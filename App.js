/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createAppContainer } from "react-navigation";
import { View } from "react-native";
import React, { Component, I18nManager } from "react";
import firebase from "react-native-firebase";
import { StyleProvider, Root, Text } from "native-base";
import AppNavigator from "./app/navigator";
import getTheme from "./theme/components";
import theme from "./theme/variables/ThemeD2D";
import { Adjust, AdjustConfig } from "react-native-adjust";
import { AdjustTrackerConfig } from "./app/libs/AdjustTracker";
import Config from "react-native-config";
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize"; // Use for caching/memoize for better performance
import { setI18nConfig } from "./app/libs/localize/LocalizeHelper";
import AsyncStorage from "@react-native-community/async-storage";
import { STORAGE_TABLE_NAME } from "./app/libs/Common";
const AppContainer = createAppContainer(AppNavigator);
console.disableYellowBox = true;

export default class App extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); // set initial config
    this.initAdjust();
  }

  initAdjust = async () => {
    let adjustConfig = new AdjustConfig(
      AdjustTrackerConfig.AppToken,
      AdjustTrackerConfig.Environment
    );
    adjustConfig.setAppSecret(2, 851215944, 1906984558, 1962284368, 466648495);
    Adjust.create(adjustConfig);
    const token = await AdjustTrackerConfig.PushToken();
    console.log("AdjustTrackerConfig.PushToken()", token);
    console.log("cekENV Config.ENV", Config.ENV);
    Adjust.setPushToken(await AdjustTrackerConfig.PushToken());
  };

  componentDidMount() {
    RNLocalize.addEventListener("change", this.handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener("change", this.handleLocalizationChange);
    Adjust.componentWillUnmount();
  }

  handleLocalizationChange = async () => {
    setI18nConfig();
    this.forceUpdate();
  };

  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Root>
          <AppContainer />
        </Root>
      </StyleProvider>
    );
  }
}
