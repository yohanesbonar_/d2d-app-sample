/**
 * @format
 */

import { AppRegistry } from "react-native";
import App from "./App";
import BgMessaging from "./app/libs/BgMessaging";
import FloatingWidget from "./src/components/molecules/FloatingWidget/FloatingWidget";

AppRegistry.registerComponent("D2D", () => App);
AppRegistry.registerHeadlessTask(
  "RNFirebaseBackgroundMessage",
  () => BgMessaging
);

// AppRegistry.registerHeadlessTask("FloatingHeadlessTask", () => require("./src/components/molecules/FloatingWidget/FloatingWidget"));
AppRegistry.registerHeadlessTask("FloatingWidget", () => FloatingWidget);
