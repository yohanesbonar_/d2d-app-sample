import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import Router from './router';
import {StyleProvider} from 'native-base';
import theme from '../theme/variables/ThemeD2D';
import getTheme from '../theme/components';

const App = () => {
  return (
    <StyleProvider style={getTheme(theme)}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </StyleProvider>
  );
};

export default App;
