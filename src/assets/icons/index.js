import IconAlbumP2KBOff from "./ic-album-p2kb-off.svg";
import IconFeedOff from "./ic-feed-off.svg";
import IconHomeLogo from "./ic-home-logo.svg";
import IconDoctor from "./ic-doctor.svg";
import IconNotification from "./ic-notification.svg";
import IconHomeEvent from "./ic-event.svg";
import IconHomeWebinar from "./ic-webinar.svg";
import IconHomeCme from "./ic-cme.svg";
import IconHomeJob from "./ic-job.svg";
import IconHomeChannel from "./ic-channel.svg";
import IconHomeLiteratur from "./ic-literatur.svg";
import IconHomeObatatoz from "./ic-obatatoz.svg";
import IconHomePolling from "./ic-polling.svg";
import IconLocation from "./ic-location.svg";
import IconSpecialist from "./ic-specialist.svg";
import IconSkp from "./ic-skp.svg";
import IconQrscan from "./ic-qrscan.svg";
import IconVerified from "./ic-verified.svg";
import IconSearch from "./ic-search.svg";
import IconBack from "./ic-back.svg";
import IconShare from "./ic-share.svg";
import IconJob from "./ic-job.svg";
import IconBookmark from "./ic-bookmark.svg";
import IconDownload from "./ic-download.svg";
import IconSyaratKetentuan from "./ic-syaratdanketentuan.svg";
import IconKebijakanPrivasi from "./ic-kebijakanprivasi.svg";
import IconKontakKami from "./ic-kontakkami.svg";
import IconRating from "./ic-rating.svg";
import IconJobStatus from "./ic-jobstatus.svg";
import IconArrowRight from "./ic-arrowright.svg";
import IconClose from "./ic-close.svg";
import IconCloseRoundedWhite from "./ic-close-rounded-white.svg";
import IconUpcomingWebinar from "./ic-upcomingwebinar.svg";
import IconRecordedWebinar from "./ic-recordedwebinar.svg";
import IconJurnal from "./ic-jurnal.svg";
import IconEventSearch from "./ic-eventsearch.svg";
import IconNotificationSearch from "./ic-notificationsearch.svg";
import IconBookmarkedSearch from "./ic-bookmarked-search.svg";
import IconShareSearch from "./ic-shareSearch.svg";
import IconBookmarkSearch from "./ic-bookmarkSearch.svg";
import IconNotFound from "./ic-notfound.svg";
import IconJobSearch from "./ic-jobSearch.svg";
import IconCmeSearch from "./ic-cmeSearch.svg";
import IconAtozSearch from "./ic-obatatozSearch.svg";
import IconFilterSearch from "./ic-filterSearch.svg";
import IconEdit from "./ic-edit.svg";
import IconCalendar from "./ic-calendar.svg";
import IconMoreVertial from "./ic-more-vertical.svg";
import IconNavigateRight from "./ic-navigate-right.svg";
import IconForumChannel from "./ic-forum-channel.svg";
import IconKonferensiChannel from "./ic-konferensi-channel.svg";
import IconRequestJournalChannel from "./ic-request-journal-channel.svg";
import IconJobsChannel from "./ic-job-channel.svg";
import IconProdukChannel from "./ic-produk-channel.svg";
import IconExhibitionChannel from "./ic-exhibition-channel.svg";
import IconKuisChannel from "./ic-exhibition-channel.svg";
import IconPollingChannel from "./ic-polling-channel.svg";
import IconElearningChannel from "./ic-elearning-channel.svg";
import IconStripBottomSheet from "./ic-strip-top-bottomsheet.svg";
import IconExitChannel from "./ic-exit-channel.svg";
import IconCloseHistorySearch from "./ic-close-history-search.svg";
import IconNavigateBefore from "./ic-navigate-before.svg";
import IconNavigateNext from "./ic-navigate-next.svg";
import IconLocationJobDetail from "./ic-location-job-detail.svg";
import IconTimeJobDetail from "./ic-time-job-detail.svg";
import IconCalendarJobDetail from "./ic-calendar-job-detail.svg";
import IconFacebookJobDetail from "./ic-facebook-job-detail.svg";
import IconInstagramJobDetail from "./ic-instagram-job-detail.svg";
import IconTwitterJobDetail from "./ic-twitter-job-detail.svg";
import IconAlertWithStar from "./ic-alert-with-star-icon.svg";
import IconFilePdfWord from "./ic-file-pdf-doc.svg";
import IconPublishReviewerJob from "./ic-publish-reviewer.svg";
import IconFiltered from "./ic-filtered.svg";
import IconNotificationOff from "./ic-notification-off.svg";
import IconNotificationOn from "./ic-notification-on.svg";
import IconEmptyData from "./ic-empty-data.svg";
import IconLiveNowWebinar from "./ic-live-now-webinar.svg";
import Icon360LiveWebinar from "./ic-360-live-webinar.svg";
import IconBookmarkedRound from "./ic-bookmarked-round.svg";
import IconUnbookmarkRound from "./ic-unbookmark-round.svg";
import IconThumbUpOn from "./ic-thumb-up-on.svg";
import IconThumbUpOff from "./ic-thumb-up-off.svg";
import IconShareForum from "./ic-share-forum.svg";
import IconCommentForum from "./ic-comment-forum.svg";
import IconComingSoon from "./ic-coming-soon.svg";
import IconMoreVerticalGrey from "./ic-more-vertical-grey.svg";
import IconEmailBlack from "./ic-email-black.svg";
import IconDkonsulChannel from "./ic-dkonsul-channel.svg";
import IconBlueArrow from "./ic-blue-arrow.svg";
import IconLightBlueArrow from "./ic-light-blue-arrow.svg";
import IconTemanBumilChannel from "./ic-teman-bumil-channel.svg";
import IconTemanDiabetesChannel from "./ic-teman-diabetes-channel.svg";
import IconOrangeArrow from "./ic-orange-arrow.svg";
import IconPurpleArrow from "./ic-purple-arrow.svg";
import IconTemanBumilHeaderProductDetail from "./ic-teman-bumil-header-banner-product-detail.svg";
import IconTemanDiabetesHeaderProductDetail from "./ic-teman-diabetes-header-banner-product-detail.svg";
import IconDkonsulHeaderProductDetail from "./ic-dkonsul-header-banner-detail-product.svg";
import IconInstagram from "./ic-instagram.svg";
import IconKelolaTD from "./ic-kelola-td.svg";
import IconSendBlue from "./ic-send-blue.svg";
import IconSendGrey from "./ic-send-grey.svg";
import IconCloseReplyTo from "./ic-close-reply-comment.svg";
import IconProfileUnfinished from "./ic-profile-unfinished.svg";
import IconAlertCompatibilityIOS from "./ic-alert-compatibilty-ios.svg";
import IconFeedOn from "./ic-feed-on.svg";
import IconAlbumP2KBOn from "./ic-album-p2kb-on.svg";
import IconEmptyNotification from "./ic-empty-notification.svg";
import IconEmptyBookmark from "./ic-empty-bookmark.svg";
import IconEmptyDownload from "./ic-empty-download.svg";
import IconHomeLogoOff from "./ic-home-logo-off.svg";
import IconBottomSheetNotFound from "./ic-not-found.svg";
import IconExpandMoreWhite from "./ic-expand-more-white.svg";
import IconWarningIconRed from "./ic-warning-icon-red.svg";
import IconVirtualExhibition from "./ic-pameran-virtual.svg";
import IconVirtualExhibitionDisabled from "./ic-pameran-virtual-disabled.svg";
import IconNavigateNextDisabled from "./ic-navigate-next-disabled.svg";
import IconChatActive from "./ic-chat-active.svg";
import IconChatInactive from "./ic-chat-inactive.svg";
import IconBrosurActive from "./ic-brosur-active.svg";
import IconBrosurInactive from "./ic-brosur-inactive.svg";
import IconKontakKamiActive from "./ic-kontak-kami-active.svg";
import IconKontakKamiInactive from "./ic-kontak-kami-inactive.svg";
import IconCommentDisable from "./ic-comment-disable.svg";
import IconSingaporeFlag from "./ic-singapore-flag.svg";
import IconIndonesiaFlag from "./ic-indonesia-flag.svg";
import IconMyanmarFlag from "./ic-myanmar-flag.svg";
import IconCambodiaFlag from "./ic-cambodia-flag.svg";
import IconPhilippinesFlag from "./ic-philippines-flag.svg";
import IconRecommendationData from "./ic-recommendation-data.svg";
import IconScrollUp from "./ic-scroll-up.svg";
import IconCharactersDisabled from "./ic-characters-disabled.svg";
import IconCharactersEnabled from "./ic-characters-enabled.svg";
import IconLowerCaseEnabled from "./ic-lowercase-enabled.svg";
import IconLowerCaseDisabled from "./ic-lowercase-disabled.svg";
import IconUpperCaseEnabled from "./ic-uppercase-enabled.svg";
import IconUpperCaseDisabled from "./ic-uppercase-disabled.svg";
import IconNumbersEnabled from "./ic-numbers-enabled.svg";
import IconNumbersDisabled from "./ic-numbers-disabled.svg";
import IconSymbolsEnabled from "./ic-symbols-enabled.svg";
import IconSymbolsDisabled from "./ic-symbols-disabled.svg";
export {
  IconAlbumP2KBOff,
  IconFeedOff,
  IconHomeLogo,
  IconDoctor,
  IconNotification,
  IconHomeCme,
  IconHomeEvent,
  IconHomeWebinar,
  IconHomeJob,
  IconHomeChannel,
  IconHomeLiteratur,
  IconHomeObatatoz,
  IconHomePolling,
  IconLocation,
  IconSpecialist,
  IconSkp,
  IconQrscan,
  IconVerified,
  IconSearch,
  IconBack,
  IconShare,
  IconJob,
  IconBookmark,
  IconDownload,
  IconSyaratKetentuan,
  IconKebijakanPrivasi,
  IconKontakKami,
  IconRating,
  IconJobStatus,
  IconArrowRight,
  IconClose,
  IconCloseRoundedWhite,
  IconUpcomingWebinar,
  IconRecordedWebinar,
  IconJurnal,
  IconEventSearch,
  IconNotificationSearch,
  IconBookmarkedSearch,
  IconShareSearch,
  IconBookmarkSearch,
  IconNotFound,
  IconEdit,
  IconJobSearch,
  IconAtozSearch,
  IconCmeSearch,
  IconFilterSearch,
  IconCalendar,
  IconMoreVertial,
  IconNavigateRight,
  IconForumChannel,
  IconExhibitionChannel,
  IconKonferensiChannel,
  IconRequestJournalChannel,
  IconJobsChannel,
  IconProdukChannel,
  IconKuisChannel,
  IconPollingChannel,
  IconElearningChannel,
  IconStripBottomSheet,
  IconExitChannel,
  IconCloseHistorySearch,
  IconNavigateBefore,
  IconNavigateNext,
  IconCalendarJobDetail,
  IconLocationJobDetail,
  IconTimeJobDetail,
  IconFacebookJobDetail,
  IconInstagramJobDetail,
  IconTwitterJobDetail,
  IconAlertWithStar,
  IconFilePdfWord,
  IconPublishReviewerJob,
  IconFiltered,
  IconNotificationOff,
  IconNotificationOn,
  IconEmptyData,
  IconLiveNowWebinar,
  Icon360LiveWebinar,
  IconBookmarkedRound,
  IconUnbookmarkRound,
  IconThumbUpOff,
  IconThumbUpOn,
  IconCommentForum,
  IconShareForum,
  IconComingSoon,
  IconMoreVerticalGrey,
  IconEmailBlack,
  IconDkonsulChannel,
  IconBlueArrow,
  IconLightBlueArrow,
  IconTemanDiabetesChannel,
  IconTemanBumilChannel,
  IconPurpleArrow,
  IconOrangeArrow,
  IconDkonsulHeaderProductDetail,
  IconTemanBumilHeaderProductDetail,
  IconTemanDiabetesHeaderProductDetail,
  IconInstagram,
  IconKelolaTD,
  IconSendBlue,
  IconSendGrey,
  IconCloseReplyTo,
  IconProfileUnfinished,
  IconAlertCompatibilityIOS,
  IconFeedOn,
  IconAlbumP2KBOn,
  IconEmptyNotification,
  IconEmptyBookmark,
  IconEmptyDownload,
  IconHomeLogoOff,
  IconBottomSheetNotFound,
  IconExpandMoreWhite,
  IconWarningIconRed,
  IconVirtualExhibition,
  IconVirtualExhibitionDisabled,
  IconNavigateNextDisabled,
  IconChatActive,
  IconChatInactive,
  IconBrosurActive,
  IconBrosurInactive,
  IconKontakKamiActive,
  IconKontakKamiInactive,
  IconCommentDisable,
  IconSingaporeFlag,
  IconIndonesiaFlag,
  IconCambodiaFlag,
  IconMyanmarFlag,
  IconPhilippinesFlag,
  IconRecommendationData,
  IconScrollUp,
  IconCharactersDisabled,
  IconCharactersEnabled,
  IconLowerCaseDisabled,
  IconLowerCaseEnabled,
  IconNumbersDisabled,
  IconNumbersEnabled,
  IconSymbolsDisabled,
  IconSymbolsEnabled,
  IconUpperCaseDisabled,
  IconUpperCaseEnabled,
};
