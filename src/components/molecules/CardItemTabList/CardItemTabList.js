import { Card, Text } from "native-base";
import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import {
  TouchableOpacity,
  TouchableHighlight,
} from "react-native-gesture-handler";
import { Avatar, Button } from "../../atoms";

const CarditemTabList = ({ item, selectedTabList, onPress, title }) => {
  return (
    <TouchableHighlight
      style={styles.containerList(item, selectedTabList)}
      onPress={onPress}
      underlayColor={item != selectedTabList ? "#D7D7D7" : "#0360BB"}
    >
      <Text style={styles.textList(item, selectedTabList)}>{title}</Text>
    </TouchableHighlight>
  );
};

export default CarditemTabList;

const styles = StyleSheet.create({
  containerList: (item, selectedTabList) => ({
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: item != selectedTabList ? "#EBEBEB" : "#0771CD",
    marginRight: 16,
    borderRadius: 4,
  }),
  textList: (item, selectedTabList) => ({
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: item != selectedTabList ? "#000000" : "#FFFFFF",
    textTransform: "uppercase",
    letterSpacing: 0.9,
    lineHeight: 16,
  }),
});
