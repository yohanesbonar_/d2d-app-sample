import _ from "lodash";
import { Card, Text, Toast } from "native-base";
import React, { useState, useEffect } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import CardAvatarTitle from "../CardAvatarTitle";
import moment from "moment";
import { likeCommentReply } from "../../../utils/network/forum/likeCommentReply";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../../app/libs/AdjustTracker";

const CardItemCommentInComment = ({
  data,
  onPressCard,
  onPressAmountLikes,
  onPressLike,
  type,
  titleReply,
  rolesReply,
  currentDateReply,
  serverDateReply,
  imageUserReply,
  commentReply,
  totalLikeReply,
  flagLikeReply,
  typeCard,
  openReadMore,
  xid,
  sbcChannel,
}) => {
  const [isLike, setIsLike] = useState(
    flagLikeReply != null ? flagLikeReply : false
  );
  const [totalLikesState, setTotalLikesState] = useState(-1);
  // const onPressCardLike = () => {
  //   setIsLike(isLike ? false : true);
  // };

  useEffect(() => {
    setIsLike(flagLikeReply);
  }, [flagLikeReply]);

  const renderLikeButton = () => {
    if (isLike) {
      return (
        <TouchableOpacity
          style={{ paddingRight: 10 }}
          // onPress={onPressLike}
          onPress={() => {
            onPressLikeButtonComment();
            AdjustTracker(
              AdjustTrackerConfig.Forum_Diskusi_Komentar_Reply_Like
            );
          }}
        >
          <Text style={styles.textLikeReplyLeftRed}>SUKA</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={{ paddingRight: 10 }}
          // onPress={onPressLike}
          onPress={() => {
            onPressLikeButtonComment();
            AdjustTracker(
              AdjustTrackerConfig.Forum_Diskusi_Komentar_Reply_Like
            );
          }}
        >
          <Text style={styles.textLikeReplyLeft}>SUKA</Text>
        </TouchableOpacity>
      );
    }
  };
  const renderComment = () => {
    if (commentReply != null && commentReply != "") {
      if (typeCard == "ForumDetail") {
        if (commentReply.length > 240) {
          return (
            <TouchableOpacity
              style={styles.containerDescription}
              onPress={openReadMore}
            >
              <Text style={styles.textDesc}>
                {commentReply.length > 240
                  ? commentReply.substring(0, 240 - 3) + "..."
                  : commentReply}
              </Text>
              {commentReply.length > 240 ? (
                <Text style={styles.readMore}>Baca selengkapnya</Text>
              ) : null}
            </TouchableOpacity>
          );
        } else {
          return (
            <View style={styles.containerDescription}>
              <Text style={styles.textDesc}>{commentReply}</Text>
            </View>
          );
        }
      } else {
        return (
          <View style={styles.containerDescription}>
            <Text style={styles.textDesc}>{commentReply}</Text>
          </View>
        );
      }
    } else {
      return null;
    }
  };

  const onPressLikeButtonComment = async () => {
    let paramLike = "";
    console.log("log isLike - > ", isLike);
    if (isLike == true) {
      paramLike = "unlike";
    } else {
      paramLike = "like";
    }
    console.log("use paramLike - > ", paramLike);
    setIsLike(paramLike == "unlike" ? false : true);
    setTotalLikesState(
      paramLike == "unlike"
        ? totalLikesState > -1 != 0
          ? totalLikesState - 1
          : totalLikeReply - 1
        : totalLikesState > -1
        ? totalLikesState + 1
        : totalLikeReply + 1
    );

    try {
      response = await likeCommentReply(xid, paramLike, sbcChannel);
      console.log("response like Button Comment", response);
      if (response.header.message == "Success") {
        // onRefreshdata();
      } else {
        Toast.show({
          text: "Something went wrong!! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  return (
    <View>
      <View style={styles.container(type)}>
        <CardAvatarTitle
          title={titleReply}
          roles={rolesReply}
          current_date={currentDateReply}
          server_date={serverDateReply}
          image_user={imageUserReply}
        />
        {renderComment()}
        <View style={styles.containerLikeReply}>
          <View style={{ flexDirection: "row" }}>{renderLikeButton()}</View>
          <TouchableOpacity onPress={onPressAmountLikes}>
            <Text style={styles.textLikeKomentar}>
              {totalLikesState > -1 ? totalLikesState : totalLikeReply} Suka
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CardItemCommentInComment;

const styles = StyleSheet.create({
  container: (type) => ({
    paddingLeft: 32,
    paddingRight: type == "ReplyComment" ? 16 : 0,
    borderRadius: 8,
  }),
  lineBottomContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
    marginTop: 17,
  },
  textName: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 6,
    marginTop: 0,
  },
  containerNameSpecialist: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 16,
    alignItems: "flex-start",
  },
  textSpecialist: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  imageSizeAvatar: { height: 40, width: 40 },
  containerNameAll: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 20,
  },
  textDesc: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    letterSpacing: 0.15,
    lineHeight: 24,
  },
  containerLikeReply: {
    flexDirection: "row",
    alignItems: "baseline",
    flex: 1,
    justifyContent: "space-between",
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  containerDescription: {
    backgroundColor: "#EBEBEB",
    borderRadius: 8,
    padding: 16,
    marginTop: 20,
  },
  textLikeKomentar: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textLikeReplyLeft: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#666666",
    letterSpacing: 0.9,
    lineHeight: 16,
  },
  textAnotherReply: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#0771CD",
    letterSpacing: 0.9,
    lineHeight: 16,
    textTransform: "uppercase",
  },
  containerAnotherReply: { paddingHorizontal: 16, paddingTop: 17 },
  textLikeReplyLeftRed: {
    fontFamily: "Roboto-Medium",
    fontWeight: "bold",
    fontSize: 15,
    color: "#D01E53",
    letterSpacing: 0.9,
    lineHeight: 16,
  },
  readMore: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#0771CD",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
});
