import React, { useState, useEffect } from "react";
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
} from "react-native";
import moment from "moment";
import platform from "../../../../theme/variables/platform";
import {
  IconBookmarkedSearch,
  IconNotificationSearch,
  IconShareSearch,
  IconBookmarkSearch,
  Icon360LiveWebinar,
  IconBookmarkedRound,
  IconUnbookmarkRound,
} from "../../../assets";
import { CardLikeCommentShare } from "..";
import { Toast } from "native-base";
import { share } from "../../../../app/libs/Common";
import { DiffTimeCount } from "../../../utils/commons";
import {
  ParamContent,
  doActionUserActivity,
} from "../../../../app/libs/NetworkUtility";
import "moment/locale/id"; // without this line it didn't work
import _ from "lodash";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const CardItemSearch = ({
  type,
  data,
  onPress,
  serverDate,
  onPressBookmark = (isBookmark) => {},
  onPressShareProps,
  component,
  onLayout,
}) => {
  const [isBookmark, setIsBookmark] = useState(
    data.flag_bookmark != null && data.flag_bookmark == true ? true : false
  );
  let defaultCoverWebinar =
    "https://static.d2d.co.id/image/cover/old-2e4e1534e8fbbf39988781a9cdb69116.png";
  let defaultCoverEvent =
    "https://static.d2d.co.id/image/cover/2e4e1534e8fbbf39988781a9cdb69116.png";

  const [webinarImageSource, setWebinarImageSource] = useState(
    data.cover != null ? data.cover : defaultCoverWebinar
  );
  const [eventImageCover, setEventImageCover] = useState(
    data.cover != null ? data.cover : defaultCoverEvent
  );

  const onWebinarImageError = () => {
    setWebinarImageSource(defaultCoverWebinar);
  };

  const onEventImageError = () => {
    setEventImageCover(defaultCoverEvent);
  };

  const shownDate = (startDate, endDate, showDiff, type) => {
    let result = "Coming Soon";
    let formatDate = "YYYY-MM-DD HH:mm:ss";

    if (startDate != null && startDate != "") {
      if (type == "Event") {
        result = moment(startDate, formatDate).format("DD MMMM YYYY");
      } else {
        result =
          moment(startDate, formatDate).format("DD MMMM YYYY HH:mm") + " WIB";
      }

      if (moment().isSameOrAfter(moment(startDate, formatDate))) {
        if (endDate != null && endDate != "") {
          if (moment().isAfter(moment(endDate, formatDate))) {
            if (showDiff) {
              result = DiffTimeCount(endDate, serverDate);
            } else {
              if (type == "Event") {
                result = moment(startDate, formatDate).format("DD MMMM YYYY");
              } else {
                result =
                  moment(startDate, formatDate).format("DD MMMM YYYY HH:mm") +
                  " WIB";
              }
            }
          } else if (showDiff) {
            result = "LIVE";
          }
        }
      } else {
        if (showDiff) {
          result = DiffTimeCount(startDate, serverDate);
        }
      }
    }

    return result;
  };

  const renderBookmark = (data) => {
    if (isBookmark) {
      return (
        <TouchableOpacity
          style={styles.touchable}
          onPress={() => {
            _toggleBookmark(data);
            onPressBookmark(!isBookmark);
          }}
        >
          <IconBookmarkedSearch />
          <Text style={styles.textBookmarked}>Bookmark</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.touchable}
          onPress={() => {
            _toggleBookmark(data);
            onPressBookmark(!isBookmark);
          }}
        >
          <IconBookmarkSearch />
          <Text style={styles.textBookmark}>Bookmark</Text>
        </TouchableOpacity>
      );
    }

    return <View> </View>;
  };

  const renderBookmarkEvent = (data) => {
    if (isBookmark) {
      return (
        <TouchableOpacity
          style={styles.iconNotif}
          onPress={() => {
            _toggleBookmark(data);
            onPressBookmark(!isBookmark);
          }}
        >
          <IconBookmarkedRound />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.iconNotif}
          onPress={() => {
            _toggleBookmark(data);
            onPressBookmark(!isBookmark);
          }}
        >
          <IconUnbookmarkRound />
        </TouchableOpacity>
      );
    }

    return <View> </View>;
  };

  const _toggleBookmark = async (data) => {
    let contentType =
      type == "Jurnal"
        ? ParamContent.PDF_JOURNAL
        : type == "Guideline"
        ? ParamContent.PDF_GUIDELINE
        : type == "Event"
        ? ParamContent.EVENT
        : null;

    doBookmarkContent(contentType, data.id);
  };

  const doBookmarkContent = async (contentType, contentId) => {
    let action = isBookmark == true ? "unbookmark" : "bookmark";
    let response = await doActionUserActivity(action, contentType, contentId);
    if (response.isSuccess == true) {
      setIsBookmark(!isBookmark);
      Toast.show({
        text: action + " succesfull",
        position: "top",
        duration: 3000,
      });
    } else {
      Toast.show({ text: response.message, position: "top", duration: 3000 });
    }
  };

  const onPressShare = (data) => {
    console.log("URL SHARE", data);
    share(data != "" ? data : null);
  };

  if (type != "") {
    if (type == "UpcomingWebinar") {
      let uriCover = { uri: webinarImageSource };
      let date = shownDate(data.start_date, data.end_date, false, "");
      let checkLive = shownDate(data.start_date, data.end_date, true, "");
      let diff = shownDate(data.start_date, data.end_date, true, "");

      return (
        <TouchableOpacity onPress={onPress}>
          <View
            style={
              component != null && component == "homeslider"
                ? {
                    ...styles.cardWebinarContainerSliderHome,
                    ...styles.cardWebinarContainer,
                  }
                : styles.cardWebinarContainer
            }
            onLayout={onLayout}
          >
            <ImageBackground
              style={styles.imageBackgroundContainer}
              source={uriCover}
              imageStyle={{ borderRadius: 8 }}
              onError={onWebinarImageError}
            >
              {/* <TouchableOpacity style={styles.iconNotif}>
                <IconNotificationSearch />
              </TouchableOpacity> */}
            </ImageBackground>

            <Text style={styles.timeMinutesWebinar}>
              {checkLive == "LIVE" ? "LIVE SEKARANG" : null}
              {diff != "LIVE" ? "UPCOMING • " + diff : null}
              {data.skp != 0 && data.skp != null ? " • SKP" : null}
              {data.pretest != 0 && data.pretest != null ? " • PRE TEST" : null}
              {data.posttest != 0 && data.posttest != null
                ? " • POST TEST"
                : null}
            </Text>

            <Text
              style={styles.titleWebinar}
              numberOfLines={
                component != null && component == "homeslider" ? 1 : 3
              }
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            <Text style={styles.dateWebinar}>{date}</Text>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "LiveWebinar") {
      let uriCover = { uri: webinarImageSource };
      let date = shownDate(data.start_date, data.end_date, false, "");
      let checkLive = shownDate(data.start_date, data.end_date, true, "");
      let diff = shownDate(data.start_date, data.end_date, true, "");
      let is360 = data.video_360 != null && data.video_360 > 0 ? true : false;
      let views = data.views != null ? data.views : 0;

      return (
        <TouchableOpacity onPress={onPress}>
          <View
            style={
              component != null && component == "homeslider"
                ? {
                    ...styles.cardWebinarContainerSliderHome,
                    ...styles.cardWebinarContainer,
                  }
                : styles.cardWebinarContainer
            }
            onLayout={onLayout}
          >
            <ImageBackground
              style={styles.imageBackgroundContainer}
              source={uriCover}
              imageStyle={{ borderRadius: 8 }}
              onError={onWebinarImageError}
            >
              {is360 ? (
                <View style={styles.viewContainerBadgeLive}>
                  <View opacity={0.8} style={styles.containerbadge360}>
                    <Icon360LiveWebinar />
                    <Text style={styles.textStyle360}>LIVE 360°</Text>
                  </View>
                </View>
              ) : (
                <View style={styles.viewContainerBadgeLive}>
                  <View opacity={0.8} style={styles.containerbadgelive}>
                    <Text style={styles.textStyleLive}>LIVE</Text>
                  </View>
                </View>
              )}
            </ImageBackground>

            <Text style={styles.timeMinutesWebinar}>
              {/* {checkLive == "LIVE" ? "LIVE SEKARANG" : null} */}
              LIVE SEKARANG
              {data.skp != 0 && data.skp != null ? " • SKP" : null}
              {data.pretest != 0 && data.pretest != null ? " • PRE TEST" : null}
              {data.posttest != 0 && data.posttest != null
                ? " • POST TEST"
                : null}
            </Text>

            <Text
              style={styles.titleWebinar}
              numberOfLines={
                component != null && component == "homeslider" ? 1 : 3
              }
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            <View style={styles.containerdatewithview}>
              <Text style={styles.dateRecordedWebinar}>{views} Dilihat</Text>
              <Text style={styles.dateRecordedWebinar}> • {date}</Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "RecordedWebinar") {
      let uriCover = { uri: webinarImageSource };
      let date = shownDate(data.start_date, data.end_date, false, "");
      let diff = shownDate(data.start_date, data.end_date, true, "");
      let is360 = data.video_360 != null && data.video_360 > 0 ? true : false;
      let views = data.views != null ? data.views : 0;

      return (
        <TouchableOpacity onPress={onPress}>
          <View
            style={
              component != null && component == "homeslider"
                ? {
                    ...styles.cardWebinarContainerSliderHome,
                    ...styles.cardWebinarContainer,
                  }
                : styles.cardWebinarContainer
            }
            onLayout={onLayout}
          >
            <ImageBackground
              style={styles.imageBackgroundContainer}
              source={uriCover}
              imageStyle={{ borderRadius: 8 }}
              onError={onWebinarImageError}
            >
              {is360 ? (
                <View style={styles.viewContainerBadgeLive}>
                  <View opacity={0.8} style={styles.containerbadge360Black}>
                    <Icon360LiveWebinar />
                    <Text style={styles.textStyle360}>360°</Text>
                  </View>
                </View>
              ) : null}
            </ImageBackground>

            <Text style={styles.timeMinutesWebinar}>
              REKAMAN {diff != "LIVE" ? "• " + diff : null}
              {data.skp != 0 && data.skp != null ? " • SKP" : null}
              {data.pretest != 0 && data.pretest != null ? " • PRE TEST" : null}
              {data.posttest != 0 && data.posttest != null
                ? " • POST TEST"
                : null}
            </Text>

            <Text
              style={styles.titleWebinar}
              numberOfLines={
                component != null && component == "homeslider" ? 1 : 3
              }
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            <View style={styles.containerdatewithview}>
              <Text style={styles.dateRecordedWebinar}>{views} Dilihat</Text>
              <Text style={styles.dateRecordedWebinar}> • {date}</Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "Jurnal") {
      let date = moment(data.created).format("DD MMMM YYYY");
      let regex = /(<([^>]+)>)/gi;
      let desc =
        data.description != null
          ? data.description.replace(regex, "")
          : data.description;
      // console.log(" result desc before regex", data.description);
      // console.log(" result desc after regex", desc);
      return (
        <TouchableOpacity onPress={onPress}>
          <View
            style={
              component != null && component == "homeslider"
                ? {
                    ...styles.cardJournalContainerSliderHome,
                    ...styles.cardJournalContainer,
                  }
                : styles.cardJournalContainer
            }
            onLayout={onLayout}
          >
            <Text style={styles.jurnalText}>JURNAL</Text>
            <Text
              style={styles.titleJurnal}
              numberOfLines={
                component != null && component == "homeslider" ? 1 : 3
              }
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            {desc != "" && component != "homeslider" && (
              <Text
                style={styles.descriptionJurnal}
                numberOfLines={
                  component != null && component == "homeslider" ? 1 : 2
                }
                ellipsizeMode="tail"
              >
                {desc}
              </Text>
            )}
            <Text style={styles.dateJurnal}>{date}</Text>
            <View style={styles.lineContainer} />
            <View style={styles.bookmarkandsharecontainer}>
              {renderBookmark(data)}
              <TouchableOpacity
                style={styles.touchable}
                onPress={() => onPressShare(data.url.app_link)}
              >
                <IconShareSearch />
                <Text style={styles.textShare}>Share</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "Event") {
      let uriCover = { uri: eventImageCover };
      let paidEvent = data.is_paid == true ? " •  BERBAYAR" : null;
      let date = shownDate(data.start_date, data.end_date, false, "Event");
      let checkLive = shownDate(data.start_date, data.end_date, true, "");
      return (
        <TouchableOpacity onPress={onPress}>
          <View
            style={
              component != null && component == "homeslider"
                ? {
                    ...styles.cardWebinarContainerSliderHome,
                    ...styles.cardWebinarContainer,
                  }
                : styles.cardWebinarContainer
            }
            onLayout={onLayout}
          >
            <ImageBackground
              style={styles.imageBackgroundContainer}
              source={uriCover}
              imageStyle={{ borderRadius: 8 }}
              onError={onEventImageError}
            >
              {renderBookmarkEvent(data)}
            </ImageBackground>
            <Text style={styles.timeEvent}>
              EVENT{paidEvent}
              {data.location_type == "online"
                ? " • ONLINE"
                : data.location_type == "offline"
                ? " • OFFLINE"
                : ""}
            </Text>
            <Text
              style={styles.titleEvent}
              numberOfLines={
                component != null && component == "homeslider" ? 1 : 3
              }
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            <Text style={styles.dateEvent}>{date}</Text>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "Guideline") {
      let date = moment(data.created).format("DD MMMM YYYY");
      return (
        <TouchableOpacity onPress={onPress}>
          <View
            style={
              component != null && component == "homeslider"
                ? {
                    ...styles.cardGuidelineContainerSliderHome,
                    ...styles.cardJournalContainer,
                  }
                : styles.cardJournalContainer
            }
            onLayout={onLayout}
          >
            <Text style={styles.jurnalText}>GUIDELINE</Text>
            <Text
              style={styles.titleJurnal}
              numberOfLines={
                component != null && component == "homeslider" ? 1 : 2
              }
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            <Text style={styles.dateGuideline}>{date}</Text>
            <View style={styles.lineContainer} />
            <View style={styles.bookmarkandsharecontainer}>
              {renderBookmark(data)}
              <TouchableOpacity
                style={styles.touchable}
                onPress={() => onPressShare(data.url.app_link)}
              >
                <IconShareSearch />
                <Text style={styles.textShare}>Share</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "Cme") {
      let date = moment(data.available_end).format("DD MMMM YYYY");
      let defaultCover = require("../../../../src/assets/images/blank.jpg");
      let uriLogo = data.logo != null ? { uri: data.logo } : defaultCover;
      let dataSKP = data.skp != null ? "• " + data.skp + " SKP" : "";
      return (
        <TouchableOpacity
          onPress={onPress}
          style={
            component != null && component == "homeslider"
              ? {
                  ...styles.cardWebinarContainerSliderHome,
                  ...styles.cardJournalContainer,
                }
              : styles.cardJournalContainer
          }
          onLayout={onLayout}
        >
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1, flexDirection: "column", marginRight: 24 }}>
              <Text style={styles.jurnalText}>CME {dataSKP}</Text>
              <Text
                style={{
                  ...styles.titleCme,
                  height: 48,
                }}
                numberOfLines={2}
                ellipsizeMode="tail"
              >
                {data.title}
              </Text>
            </View>
            <ImageBackground
              style={{
                aspectRatio: 1 / 1,
                justifyContent: "center",
                alignItems: "center",
                maxWidth: 80,
              }}
              source={uriLogo}
              imageStyle={{ borderRadius: 8 }}
            />
          </View>

          <Text style={styles.dateCme}>Tersedia sampai {date}</Text>
        </TouchableOpacity>
      );
    } else if (type == "Job") {
      let date = moment(data.created).format("DD MMMM YYYY");
      return (
        <TouchableOpacity onPress={onPress}>
          <View style={styles.cardJournalContainer}>
            <Text style={styles.jurnalText}>TULIS ARTIKEL</Text>
            <Text
              style={styles.titleJob}
              numberOfLines={3}
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            <Text style={styles.dateCme}>{date}</Text>
          </View>
        </TouchableOpacity>
      );
    } else if (type == "ObatAtoz") {
      let date = moment(data.created).format("DD MMMM YYYY");
      return (
        <TouchableOpacity
          onPress={onPress}
          style={
            component != null && component == "homeslider"
              ? {
                  ...styles.cardGuidelineContainerSliderHome,
                  ...styles.cardJournalContainer,
                }
              : styles.cardJournalContainer
          }
          onLayout={onLayout}
        >
          <Text style={styles.jurnalText}>OBAT A-Z</Text>
          <Text
            style={styles.titleJob}
            numberOfLines={
              component != null && component == "homeslider" ? 1 : 3
            }
            ellipsizeMode="tail"
          >
            {data.title}
          </Text>
          <Text style={styles.typeObat}>{data.type}</Text>
        </TouchableOpacity>
      );
    } else if (type == "JobArtikel") {
      let date = moment(data.due_date).format("DD MMMM YYYY");
      return (
        <TouchableOpacity onPress={onPress}>
          <View style={styles.cardJobArtikel}>
            <Text style={styles.categoryText}>{data.type.name}</Text>
            <Text
              style={styles.titleJobArtikel}
              numberOfLines={3}
              ellipsizeMode="tail"
            >
              {data.title}
            </Text>
            {data.channel ? (
              data.channel.channel_name ? (
                <Text style={styles.textNote}>
                  By {data.channel.channel_name}
                </Text>
              ) : null
            ) : null}
            <Text style={styles.dateJobArtikel}>{date}</Text>
            <View style={styles.lineContainer} />
            <CardLikeCommentShare onPressShare={() => onPressShareProps()} />
          </View>
        </TouchableOpacity>
      );
    } else if (type == "shimmer") {
      return (
        <View>
          <SkeletonPlaceholder
            highlightColor={"#c5c5c5"}
            backgroundColor={"#ebebeb"}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 20,
                marginHorizontal: 16,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ height: 24, width: 24, borderRadius: 12 }} />
                <View
                  style={{
                    marginLeft: 16,
                    height: 24,
                    width: 100,
                    borderRadius: 100 / 2,
                  }}
                />
              </View>
              <View style={{ height: 24, width: 100, borderRadius: 100 / 2 }} />
            </View>
          </SkeletonPlaceholder>
          <View
            style={{
              borderRadius: 8,
              marginHorizontal: 16,
              marginBottom: 32,
              backgroundColor: "#FFFFFF",
              borderColor: "#0000001F",
              elevation: 2,
              shadowColor: "#000000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.08,
              shadowRadius: 4,
            }}
          >
            <SkeletonPlaceholder
              highlightColor={"#c5c5c5"}
              backgroundColor={"#ebebeb"}
            >
              <View style={{}}>
                <View
                  style={{
                    aspectRatio: 16 / 9,
                    borderRadius: 8,
                  }}
                />

                <View
                  style={{
                    marginLeft: 16,
                    marginTop: 16,
                    marginBottom: 8,
                    height: 16,
                    width: 160,
                    borderRadius: 160 / 2,
                  }}
                />
                <View
                  style={{
                    marginLeft: 16,
                    height: 24,
                    width: 160,
                    borderRadius: 160 / 2,
                  }}
                />
                <View
                  style={{
                    marginLeft: 16,
                    height: 16,
                    marginVertical: 16,
                    width: 160,
                    borderRadius: 160 / 2,
                  }}
                />
              </View>
            </SkeletonPlaceholder>
          </View>
        </View>
      );
    } else if (type == "shimmerFeeds") {
      return (
        <View>
          <View
            style={{
              borderRadius: 8,
              // marginHorizontal: 16,
              marginBottom: 20,
              backgroundColor: "#FFFFFF",
              borderColor: "#0000001F",
              elevation: 2,
              shadowColor: "#000000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.08,
              shadowRadius: 4,
            }}
          >
            <SkeletonPlaceholder
              highlightColor={"#c5c5c5"}
              backgroundColor={"#ebebeb"}
            >
              <View style={{}}>
                <View
                  style={{
                    aspectRatio: 16 / 9,
                    borderRadius: 8,
                  }}
                />

                <View
                  style={{
                    marginLeft: 16,
                    marginTop: 16,
                    marginBottom: 8,
                    height: 16,
                    width: 120,
                    borderRadius: 160 / 2,
                  }}
                />
                <View
                  style={{
                    marginLeft: 16,
                    height: 24,
                    width: 200,
                    borderRadius: 160 / 2,
                  }}
                />
                <View
                  style={{
                    marginLeft: 16,
                    height: 16,
                    marginVertical: 16,
                    width: 160,
                    borderRadius: 160 / 2,
                  }}
                />
              </View>
            </SkeletonPlaceholder>
          </View>
          <View
            style={{
              borderRadius: 8,
              // marginHorizontal: 16,
              marginBottom: 20,
              backgroundColor: "#FFFFFF",
              borderColor: "#0000001F",
              elevation: 2,
              shadowColor: "#000000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.08,
              shadowRadius: 4,
            }}
          >
            <SkeletonPlaceholder
              highlightColor={"#c5c5c5"}
              backgroundColor={"#ebebeb"}
            >
              <View style={{}}>
                <View
                  style={{
                    marginLeft: 16,
                    marginTop: 16,
                    marginBottom: 8,
                    height: 16,
                    width: 120,
                    borderRadius: 160 / 2,
                  }}
                />
                <View
                  style={{
                    marginLeft: 16,
                    height: 24,
                    width: 200,
                    borderRadius: 160 / 2,
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{
                      marginLeft: 16,
                      height: 16,
                      marginVertical: 16,
                      width: 160,
                      borderRadius: 160 / 2,
                    }}
                  />
                  <View
                    style={{
                      marginRight: 16,
                      height: 16,
                      marginVertical: 16,
                      width: 100,
                      borderRadius: 160 / 2,
                    }}
                  />
                </View>
              </View>
            </SkeletonPlaceholder>
          </View>
        </View>
      );
    }
  }
};

export default CardItemSearch;

const styles = StyleSheet.create({
  cardWebinarContainerSliderHome: {
    width: platform.deviceWidth - 64,
    marginLeft: 16,
  },
  cardJournalContainerSliderHome: {
    width: platform.deviceWidth - 64,
    marginLeft: 16,
  },
  cardGuidelineContainerSliderHome: {
    width: platform.deviceWidth - 64,
    marginLeft: 16,
  },
  cardWebinarContainer: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#FFFFFF",
    marginBottom: 20,
    borderRadius: 8,
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  cardJournalContainer: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#FFFFFF",
    borderRadius: 8,
    marginBottom: 32,
    padding: 16,
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  cardJobArtikel: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#FFFFFF",
    marginTop: 4,
    borderRadius: 8,
    marginBottom: 16,
    paddingTop: 16,
    paddingHorizontal: 16,
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  titleWebinar: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginHorizontal: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  timeMinutesWebinar: {
    fontFamily: "Roboto-Medium",
    fontSize: 10,
    color: "#D01E53",
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 8,
    textTransform: "uppercase",
    lineHeight: 16,
    letterSpacing: 1,
  },
  dateWebinar: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    marginHorizontal: 16,
    marginVertical: 16,
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  dateRecordedWebinar: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  imageBackgroundContainer: {
    aspectRatio: 16 / 9,
    justifyContent: "center",
    alignItems: "center",
  },
  iconNotif: {
    position: "absolute",
    top: 0,
    right: 0,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  jurnalText: {
    fontFamily: "Roboto-Medium",
    fontSize: 10,
    color: "#D01E53",
    marginBottom: 8,
    lineHeight: 16,
    letterSpacing: 1,
  },
  categoryText: {
    fontFamily: "Roboto-Medium",
    fontSize: 10,
    color: "#D01E53",
    marginBottom: 8,
    textTransform: "uppercase",
    letterSpacing: 1,
    lineHeight: 16,
  },

  titleJurnal: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  titleJob: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  titleJobArtikel: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 12,
    letterSpacing: 0.15,
    lineHeight: 24,
  },
  titleCme: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  descriptionJurnal: {
    marginTop: 16,
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#666666",
    marginBottom: 8,
    lineHeight: 20,
    letterSpacing: 0.25,
  },
  dateJurnal: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    marginVertical: 16,
    lineHeight: 20,
    letterSpacing: 0.19,
  },
  dateGuideline: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    marginVertical: 16,
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  dateCme: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  dateJobArtikel: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
    marginBottom: 16,
  },
  typeObat: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    textTransform: "uppercase",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  titleEvent: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginHorizontal: 16,
  },
  timeEvent: {
    fontFamily: "Roboto-Medium",
    fontSize: 10,
    color: "#D01E53",
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 8,
  },
  dateEvent: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    marginHorizontal: 16,
    marginVertical: 16,
  },
  lineContainer: {
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
  },
  bookmarkandsharecontainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingTop: 12,
  },
  textBookmarked: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#0771CD",
    alignItems: "center",
    marginLeft: 6,
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textBookmark: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    alignItems: "center",
    marginLeft: 6,
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textShare: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    alignItems: "center",
    paddingLeft: 6,
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textNote: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    marginBottom: 16,
    letterSpacing: 0.19,
  },
  touchable: { flexDirection: "row", alignItems: "center" },
  viewContainerBadgeLive: {
    position: "absolute",
    top: 0,
    left: 0,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  containerbadge360: {
    paddingVertical: 4,
    paddingHorizontal: 6,
    backgroundColor: "#FF0000",
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
  },
  containerbadge360Black: {
    paddingVertical: 4,
    paddingHorizontal: 6,
    backgroundColor: "#000000",
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
  },
  containerbadgelive: {
    paddingVertical: 4,
    paddingHorizontal: 6,
    backgroundColor: "#FF0000",
    borderRadius: 5,
  },
  textStyle360: {
    paddingLeft: 6,
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#FFFFFF",
  },
  textStyleLive: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#FFFFFF",
  },
  containerdatewithview: {
    marginHorizontal: 16,
    marginVertical: 16,
    flexDirection: "row",
  },
});
