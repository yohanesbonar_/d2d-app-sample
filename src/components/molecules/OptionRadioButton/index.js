import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import platform from "../../../../theme/variables/d2dColor";
import { IconBookmark } from "../../../assets";
import { testID } from "../../../../app/libs/Common";

const OptionRadioButton = ({ onPress, categoryName, selected }) => {
  const renderRadioButton = () => {
    return (
      <View
        style={[
          styles.circleRadioButton,
          {
            borderColor: selected ? platform.toolbarDefaultBg : "#454F63",
          },
        ]}
      >
        {selected ? (
          <View
            style={[
              styles.circleSelected,
              {
                backgroundColor: platform.toolbarDefaultBg,
              },
            ]}
          />
        ) : null}
      </View>
    );
  };
  return (
    <View style={[styles.itemContainer]}>
      <TouchableOpacity
        {...testID("select_radio_button")}
        style={styles.itemTextContainer}
        onPress={onPress}
      >
        <Text numberOfLines={1} style={styles.itemText}>
          {categoryName}
        </Text>
        {renderRadioButton()}
      </TouchableOpacity>
    </View>
  );
};

export default OptionRadioButton;

const styles = StyleSheet.create({
  container: {
    //marginVertical:16,
    flex: 1,
    justifyContent: "flex-end",
  },
  itemTextContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  itemText: {
    flex: 1,
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  circleSelected: {
    height: 12,
    width: 12,
    borderRadius: 6,
  },
  circleRadioButton: {
    height: 24,
    width: 24,
    marginHorizontal: 12,
    borderRadius: 12,
    borderWidth: 2,
    alignItems: "center",
    justifyContent: "center",
  },
  imageFlag: {
    width: 24,
    height: 24,
    marginRight: 12,
  },
  itemContainer: {
    paddingVertical: 18,
    borderRadius: 8,
  },
});
