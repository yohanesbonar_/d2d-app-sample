import React, { useState } from "react";
import {
  Dimensions,
  Image,
  Linking,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import _ from "lodash";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const ImageCarousel = ({
  dataBanner,
  onPress = (link) => {},
  type,
  isShimmer,
}) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const clickBanner = (link) => {
    if (!_.isEmpty(link)) {
      onPress(link);
    }
  };

  const _renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        opacityContent={0.9}
        onPress={() => {
          clickBanner(item.link);
        }}
        style={styles.buttonImage}
      >
        <Image
          style={styles.image}
          resizeMode="cover"
          source={{ uri: type == "BannerChannel" ? item.image : item.url }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.containerImageBannerAds}>
      {isShimmer != true && !_.isEmpty(dataBanner) && (
        <View>
          <Carousel
            ref={(c) => {
              this._carousel = c;
            }}
            activeSlideAlignment="start"
            layout="default"
            loop={true}
            autoplay={true}
            autoplayInterval={3000}
            data={dataBanner}
            renderItem={_renderItem}
            sliderWidth={Dimensions.get("window").width}
            itemWidth={Dimensions.get("window").width - 56}
            onSnapToItem={(index) => setActiveIndex(index)}
          />
          <Pagination
            containerStyle={styles.containerPagination}
            dotsLength={dataBanner.length}
            activeDotIndex={activeIndex}
            dotContainerStyle={styles.containerDot}
            dotStyle={styles.dot}
            dotColor="#FFFFFF"
            inactiveDotColor="#FFFFFFA3"
          />
        </View>
      )}

      {isShimmer == true && (
        <SkeletonPlaceholder
          highlightColor={"#c5c5c5"}
          backgroundColor={"#ebebeb"}
        >
          <View
            style={{
              aspectRatio: 16 / 9,
              borderRadius: 8,
            }}
          />
        </SkeletonPlaceholder>
      )}
    </View>
  );
};

export default ImageCarousel;

const styles = StyleSheet.create({
  containerImageBannerAds: {
    // height: Dimensions.get("screen").width * (9 / 16),
    // width: "100%",
    // aspectRatio: 16 / 9,
    // borderRadius: 8,
    marginHorizontal: 16,
  },

  containerSwiper: {
    marginHorizontal: 16,
    borderRadius: 8,
    paddingRight: 30,
  },
  containerDot: {
    marginHorizontal: 0,
    borderRadius: 8,
  },
  dot: {
    width: 6,
    height: 6,
    borderRadius: 3,
  },
  containerPagination: {
    paddingTop: 0,
    paddingBottom: 16,
    position: "absolute",
    bottom: 0,
    left: 0,
    justifyContent: "flex-end",
  },
  image: {
    // height: Dimensions.get("screen").width * (9 / 16),
    // width: "100%",
    aspectRatio: 16 / 9,
    borderRadius: 8,
    backgroundColor: "#EBEBEB",
  },
});
