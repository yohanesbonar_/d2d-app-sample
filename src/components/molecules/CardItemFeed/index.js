import React, { useState, useEffect } from "react";
import { Text, Toast } from "native-base";
import { StyleSheet, View, FlatList, TouchableOpacity } from "react-native";
import { getData, getFeedsNew, KEY_ASYNC_STORAGE } from "../../../utils";
import CardItemForum from "../CardItemForum";
import {
  checkNpaIdiEmpty,
  isStarted,
  share,
  STORAGE_TABLE_NAME,
} from "../../../../app/libs/Common";
import _ from "lodash";
import moment from "moment";
import CardItemSearch from "../CardItemSearch";
import { Loader } from "../../../../app/components";
import platform from "../../../../theme/variables/platform";
import { IconRecommendationData } from "../../../assets";
import AsyncStorage from "@react-native-community/async-storage";

const CardItemFeed = ({
  navigation,
  paramsData,
  serverDate,
  onRefreshData,
}) => {
  const goToScreenBaseOnCategory = async (data, type, boolForum) => {
    if (
      type == "UpcomingWebinar" ||
      type == "RecordedWebinar" ||
      type == "LiveWebinar"
    ) {
      goToWebinarDetail(data);
    } else if (type == "Jurnal") {
      goToJournalDetail(data);
    } else if (type == "Event") {
      goToEventDetail(data);
    } else if (type == "Cme") {
      goToCMEDetail(data);
    } else if (type == "Guideline") {
      goToPDFView(data);
    } else if (type == "Job") {
      goToJobDetail(data);
    } else if (type == "ObatAtoz") {
      goToObatAtoz(data);
    } else if (type == "Forum") {
      goToForumDetail(data, boolForum);
    }
  };

  const goToForumDetail = (data, temp) => {
    let from = "";
    if (temp) {
      from = "CommentForumButton";
    } else {
      from = "";
    }
    navigation.navigate("ForumDetail", {
      data: data,
      sbcChannel: params.sbcChannel,
      onRefresh: onRefreshdata,
      from: from,
    });
  };

  const goToJobDetail = (item) => {
    // setShareUrl(null);
    // if (params.from && params.from == "home") {
    //   if (item.channel && !item.channel.subscribed) {
    //     setChannel(item);
    //     onOpen();
    //   } else {
    //     getChannelSbc(false, item, item.channel.xid);
    //   }
    // } else {
    navigateToJobDetail(false, item, null);
    // }
  };

  const navigateToJobDetail = (isNewSubscribe, item, sbcChannel) => {
    // let paramsData;
    // if (params.from && params.from == "home") {
    //   paramsData = {
    //     item: item,
    //     sbcChannel: sbcChannel,
    //     isNewSubscribe: isNewSubscribe,
    //     onBack: onRefreshListJob,
    //   };
    // } else {
    paramsData = {
      item: item,
      // sbcChannel: params.sbcChannel,
      sbcChannel: "",
      isNewSubscribe: isNewSubscribe,
      // onBack: onRefreshData,
      from: "Feeds",
    };
    // }

    if (item.type.name == "TULIS ARTIKEL") {
      navigation.navigate("JobTulisArtikel", paramsData);
    } else if (item.type.name == "REVIEW ARTIKEL") {
      navigation.navigate("JobReviewArtikel", paramsData);
    } else {
      navigation.navigate("JobDetail", paramsData);
    }
  };

  const goToWebinarDetail = async (data) => {
    let tempData = {
      isPaid: data.exclusive == 1 || data.is_paid == true ? true : false,
      from: "Feeds",
      ...data,
    };

    let dateShowTNC = moment(data.start_date)
      .add(-1, "hours")
      .format("YYYY-MM-DD HH:mm:ss");

    // let isStartShowTNC = isStarted(data.server_date, dateShowTNC)
    let isStartShowTNC = isStarted(serverDate, dateShowTNC);
    let routeNameDestination =
      data.has_tnc == true && isStartShowTNC == true
        ? "WebinarTNC"
        : "WebinarVideo";
    navigation.navigate({
      routeName: routeNameDestination,
      params: tempData,
      key: `webinar-${data.id}`,
    });
  };

  const goToJournalDetail = (data) => {
    navigation.navigate("LearningJournalDetail", {
      data: data,
      fromDeeplink: true,
      onRefreshDataSearchKonten: onRefreshData,
      from: "Home",
    });
  };

  const goToEventDetail = async (data) => {
    data.paid = data.is_paid;
    navigation.navigate({
      routeName: "EventDetail",
      params: {
        data: data,
        from: "home",
        onRefreshDataSearchKonten: onRefreshData,
      },
      key: `event-${data.id}`,
    });
  };

  const goToCMEDetail = async (data) => {
    if (!_.isEmpty(data.attachment)) {
      data.filename = data.attachment;
    }
    isDone = data.is_done == true || data.is_done == 1 ? true : false;
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataSubmitCmeQuiz = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ
    );
    if (checkNpaIdiEmpty(profile) && !_.isEmpty(dataSubmitCmeQuiz) && !isDone) {
      //if (checkNpaIdiEmpty(profile) && !isDone) {
      showAlertFillNpaIDI_searchContent(data);
    } else {
      gotoMedicinusDetail_searchContent(isDone, data);
    }
  };

  const gotoMedicinusDetail_searchContent = async (isDone, data) => {
    console.log("gotoMedicinusDetail data", data);
    let from = "SearchKonten";
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    // this.sendAdjust(from.DETAIL)
    // AdjustTracker(AdjustTrackerConfig.CME_SKP_Quiz);
    let paramCertificate = {
      title: data.title,
      certificate: data.certificate,
      isRead: data.status != "invalid" ? true : false,
      // isFromHistory: true,
      from: from,
      isManualSkp: data.type == "offline" ? true : false,
      typeCertificate: data.type,
      //isCertificateAvailable: true,
    };

    if (data.type == "offline") {
      gotoCertificate(paramCertificate, data);
    } else {
      if (isDone) {
        let type = data.type != null ? data.type : "Cme";

        if (type == "event" || type == "webinar") {
          gotoCertificate(paramCertificate, data);
        } else {
          if (checkNpaIdiEmpty(profile) == false) {
            gotoCertificate(paramCertificate, data);
          } else if (checkNpaIdiEmpty(profile) == true) {
            let params = {
              from: from,
              ...data,
              backFromCertificate: from,
              onRefreshDataSearchKonten: onRefreshData,
            };

            navigation.navigate({
              routeName: "CmeQuiz",
              params: params,
              key: "goto",
            });
          }
        }
      } else {
        console.log("TO THIS OPTION", data);
        let params = {
          from: from,
          ...data,
          backFromCertificate: from,
          onRefreshDataSearchKonten: onRefreshData,
        };
        navigation.navigate({
          routeName: "CmeDetail",
          params: params,
          key: `webinar-${data.id}`,
        });
      }
    }
  };

  const gotoCertificate = (paramCertificate, data) => {
    navigation.navigate({
      routeName: "CmeCertificate",
      params: paramCertificate,
      key: `webinar-${data.id}`,
    });
  };

  const showAlertFillNpaIDI_searchContent = (data) => {
    Alert.alert(
      "Reminder",
      "Please input your Medical ID first before you attempt your next CME Quiz",
      [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: "OK",
          onPress: () => gotoProfile(data),
        },
      ],
      { cancelable: true }
    );
  };

  const gotoProfile = (data) => {
    let from = "home";

    let params = {
      from: from,
      ...data,
      backFromCertificate: from,
      resubmitQuiz: true,
      isFromAlertSearchPage: true,
      //onRefreshDataSearchKonten: onRefreshdata,
    };

    navigation.navigate({
      routeName: "ChangeProfile",
      params: params,
    });
  };

  const goToPDFView = async (data) => {
    const PdfView = { type: "Navigate", routeName: "PdfView", params: data };
    let availableAttachment = data.attachment != null ? true : false;

    availableAttachment === true
      ? navigation.navigate(PdfView)
      : showErrorMessage();
  };

  const showErrorMessage = () => {
    Toast.show({
      text: "Data not found",
      position: "bottom",
      duration: 3000,
      type: "danger",
    });
  };

  const goToObatAtoz = (data) => {
    navigation.navigate("DetailAtoZ", {
      dataObat: data,
    });
  };

  const checkWebinarLive = (startDate, endDate, showDiff) => {
    let result = "Coming Soon";
    let formatDate = "YYYY-MM-DD HH:mm:ss";

    if (moment().isSameOrAfter(moment(startDate, formatDate))) {
      if (endDate != null && endDate != "") {
        if (moment().isAfter(moment(endDate, formatDate))) {
          if (showDiff) {
            result = "Recorded";
          }
        } else if (showDiff) {
          result = "Live";
        }
      }
    } else {
      if (showDiff) {
        result = "Upcoming";
      }
    }
    return result;
  };

  let data = paramsData;
  if (data.category == "webinar") {
    let typeWebinar = checkWebinarLive(data.start_date, data.end_date, true);
    let tempWebinar =
      typeWebinar == "Live"
        ? "LiveWebinar"
        : typeWebinar == "Upcoming"
        ? "UpcomingWebinar"
        : "RecordedWebinar";

    if (data.video_360 == "true" || data.video_360 == true) {
      data.video_360 = 1;
    } else if (data.video_360 == "false" || data.video_360 == false) {
      data.video_360 = 0;
    }

    return (
      <CardItemSearch
        type={tempWebinar}
        data={data}
        onPress={() => {
          goToScreenBaseOnCategory(data, tempWebinar);
        }}
        serverDate={serverDate}
      />
    );
  } else if (data.category == "event") {
    return (
      <CardItemSearch
        type="Event"
        data={data}
        onPress={() => goToScreenBaseOnCategory(data, "Event")}
        serverDate={serverDate}
        onPressBookmark={(isBookmark) => {
          data.bookmark = isBookmark;
        }}
      />
    );
  } else if (data.category == "pdf_journal") {
    return (
      <View style={{ marginBottom: -12 }}>
        <CardItemSearch
          type="Jurnal"
          data={data}
          onPress={() => goToScreenBaseOnCategory(data, "Jurnal")}
          serverDate={serverDate}
          onPressBookmark={(isBookmark) => {
            data.flag_bookmark = isBookmark;
          }}
        />
      </View>
    );
  } else if (data.category == "cme") {
    return (
      <View style={{ marginBottom: -12 }}>
        <CardItemSearch
          type="Cme"
          data={data}
          onPress={() => goToScreenBaseOnCategory(data, "Cme")}
          serverDate={serverDate}
        />
      </View>
    );
  } else if (data.category == "pdf_guideline") {
    return (
      <View style={{ marginBottom: -12 }}>
        <CardItemSearch
          type="Guideline"
          data={data}
          onPress={() => goToScreenBaseOnCategory(data, "Guideline")}
          serverDate={serverDate}
          onPressBookmark={(isBookmark) => {
            data.flag_bookmark = isBookmark;
          }}
        />
      </View>
    );
  } else if (data.category == "obatatoz" || data.category == "atoz") {
    if (!_.isEmpty(data.id)) {
      data.obat_id = data.id;
    }
    if (!_.isEmpty(data.obat_type)) {
      data.type = data.obat_type;
    }
    return (
      <View style={{ marginBottom: -12 }}>
        <CardItemSearch
          type="ObatAtoz"
          data={data}
          onPress={() => goToScreenBaseOnCategory(data, "ObatAtoz")}
          serverDate={serverDate}
        />
      </View>
    );
  }
  // else if (data.category == "job") {
  //   let dataJob = {};
  //   if (!_.isEmpty(data.other_info)) {
  //     let otherInfoData = data.other_info;
  //     dataJob = { ...data, ...otherInfoData };
  //     if (!_.isEmpty(otherInfoData.job_type)) {
  //       dataJob.type = otherInfoData.job_type;
  //     }
  //   } else {
  //     dataJob = data;
  //   }

  //   return (
  //     <CardItemSearch
  //       type="JobArtikel"
  //       data={dataJob}
  //       onPress={() => {
  //         goToScreenBaseOnCategory(dataJob, "Job");
  //       }}
  //       onPressShareProps={() => {
  //         // if (params.from && params.from == "home") {
  //         //   if (item.channel && !item.channel.subscribed) {
  //         //     setShareUrl(item.share_link_app ? item.share_link_app : null);
  //         //     setChannel(item);
  //         //     onOpen();
  //         //   } else {
  //         //     share(item.share_link_app ? item.share_link_app : null);
  //         //   }
  //         // } else {
  //         share(dataJob.share_link_app ? dataJob.share_link_app : null);
  //         // }
  //       }}
  //     />
  //   );
  // } else if (data.category == "forum") {
  //   let dataForum = {};
  //   if (!_.isEmpty(data.other_info)) {
  //     let otherInfoData = data.other_info;
  //     dataForum = { ...data, ...otherInfoData };
  //   } else {
  //     dataForumn = data;
  //   }

  //   let image = "";
  //   {
  //     if (!_.isEmpty(dataForum.attachments)) {
  //       dataForum.attachments.map((bb, i) => {
  //         if (i == 0) {
  //           image = bb.attachment_url;
  //         }
  //       });
  //     }
  //   }

  //   return (
  //     <View>
  //       <CardItemForum
  //         onPressCard={() => {
  //           goToScreenBaseOnCategory(dataForum, "Event", false);
  //           // onPressCardItemForum(dataForum, false);
  //           // AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Open);
  //         }}
  //         onPressComment={() => {
  //           goToScreenBaseOnCategory(dataForum, "Event", true);
  //           // onPressCardItemForum(dataForum, true);
  //           // AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar);
  //         }}
  //         onPressTotalComments={() => {
  //           goToScreenBaseOnCategory(dataForum, "Event", false);
  //           // onPressCardItemForum(dataForum, false);
  //           // AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar);
  //         }}
  //         onPressShare={() => onPressShareButton(dataForum)}
  //         title={dataForum.title}
  //         roles="Admin"
  //         description={dataForum.description}
  //         current_date={dataForum.created_at}
  //         server_date={serverDate}
  //         image_user={null}
  //         thumbnail_image={image != "" ? image : null}
  //         totalComments={dataForum.total_comments}
  //         totalLikes={dataForum.total_likes}
  //         flagLike={dataForum.liked}
  //         xid={dataForum.xid}
  //         sbcChannel={""}
  //         short_description={dataForum.short_description}
  //         isCommentEnable={dataForum.is_comment_enable ? true : false}
  //         from="infiniteScroll"
  //       />
  //     </View>
  //   );
  // }  else if (data.category == "Konferensi") {
  //   console.log("DATA KONFERENSI -> ", data);
  // }
  else {
    console.log("ELSE ", data.category);
    return <View />;
  }
};

export default CardItemFeed;

const styles = StyleSheet.create({
  notFoundContainer: {
    justifyContent: "center",
  },
  title: {
    marginTop: 32,
  },
  subtitle: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
  },
  containerEmptyFeeds: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 3 - 80,
  },
  textEmptyFeeds: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    lineHeight: 24,
    letterSpacing: 0.15,
    color: "#000000",
    marginTop: 32,
  },
  containerInner: {
    justifyContent: "center",
    alignItems: "center",
  },
  containerItemFooter: (isFetching) => [
    {
      justifyContent: isFetching ? null : "center",
      alignItems: isFetching ? null : "center",
    },
  ],
  textRecommendation: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  containerTextRecommendation: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    marginBottom: 21,
  },
});
