import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  Dimensions,
  Platform,
  Linking,
} from "react-native";
import { Avatar, Button } from "../../atoms";
import platform from "../../../../theme/variables/d2dColor";
import { Item, Row } from "native-base";
import {
  IconAlertCompatibilityIOS,
  IconAlertWithStar,
  IconBottomSheetNotFound,
  IconExitChannel,
  IconProfileUnfinished,
  IconStripBottomSheet,
  IconWarningIconRed,
} from "../../../assets";
import HTML from "react-native-render-html";
import { ScrollView } from "react-native-gesture-handler";
import _ from "lodash";

const BottomSheet = ({
  onPressOk,
  onPressCancel,
  wordingCancel,
  wordingOk,
  wordingClose,
  onPressClose,
  onPressExitChannel,
  title,
  desc,
  type,
  imageSource,
  screen,
  firstDesc,
  secondDesc,
}) => {
  const onPressExitChannelBS = () => { };

  const countStatusBarHeight = () => {
    const listOfNotchedIphone = [
      { w: 428, h: 926 },
      { w: 390, h: 844 },
      { w: 375, h: 812 },
      { w: 414, h: 896 },
    ];

    // const height = platform.deviceHeight; //Dimensions.get("window");
    // const width = platform.deviceWidth;

    const { height, width } = Dimensions.get("window");

    var isIPhoneX = false;

    listOfNotchedIphone.forEach((element) => {
      if (
        Platform.OS === "ios" &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        width === element.w &&
        height === element.h
      ) {
        isIPhoneX = true;
      }
    });
    console.log("isiphonex", height, width);

    const StatusBarHeight = Platform.select({
      ios: isIPhoneX ? 85 : Platform.isPad ? 60 : 20,
      android: StatusBar.currentHeight,
      default: 20,
    });

    return StatusBarHeight;
  };

  const countBottomSoftNavigationAndroid = () => {
    const deviceHeight = Dimensions.get("screen").height;
    const windowHeiight = Dimensions.get("window").height;
    console.log("height --", deviceHeight, windowHeiight);
    let bottomSoftNav = deviceHeight - windowHeiight;
    return bottomSoftNav;
  };

  const countMaxHeight = (additionalMargin) => {
    let maxHeight =
      platform.deviceHeight -
      countStatusBarHeight() -
      additionalMargin -
      (screen != null && screen == "home"
        ? platform.platform == "ios"
          ? countStatusBarHeight() == 20
            ? 60
            : countStatusBarHeight() + 20
          : 70 + 0 //(Platform.OS == "android" ? countBottomSoftNavigationAndroid() : 0)
        : Platform.OS == "android"
          ? 10 + 0 //countBottomSoftNavigationAndroid()
          : 0);
    return maxHeight;
  };

  if (type != "" && type == "bottomSheetLogout") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <Text bold style={styles.title}>
          {title}
        </Text>
        <Text regular style={styles.desc}>
          {desc}
        </Text>

        <View style={styles.viewButton}>
          <View style={{ marginRight: 8, flex: 1 }}>
            <Button
              text={wordingCancel}
              onPress={onPressCancel}
              type="secondary"
              marginRig={8}
            />
          </View>

          <View style={{ marginLeft: 8, flex: 1 }}>
            <Button text={wordingOk} onPress={onPressOk} marginSize="small" />
          </View>
        </View>
      </View>
    );
  } else if (type != "" && (type == "BottomSheetDeleteAccountConfirmation")) {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? type == "BottomSheetDeleteAccountConfirmation" ? 10 : 40
              : 16,
        }}
      >
        <Text bold style={styles.title}>
          {title}
        </Text>
        {!_.isEmpty(firstDesc) ? <Text regular style={styles.firstDescText}>
          {firstDesc}
        </Text> : null}
        {!_.isEmpty(secondDesc) ? <Text regular style={styles.secondDescText}>
          {secondDesc}
        </Text> : null}


        <View style={styles.viewButton}>
          <View style={{ marginRight: 8, flex: 1 }}>
            <Button
              text={wordingCancel}
              onPress={onPressCancel}
              type="secondary"
              marginRig={8}
            />
          </View>

          <View style={{ marginLeft: 8, flex: 1 }}>
            <Button text={wordingOk} onPress={onPressOk} marginSize="small" />
          </View>
        </View>
      </View>
    );
  } else if (type != "" && type == "BottomSheetRegisterConfirmation") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? type == "BottomSheetDeleteAccountConfirmation" ? 10 : 40
              : 16,
        }}
      >
        <Text bold style={styles.titleRegistration}>
          {title}
        </Text>
        {!_.isEmpty(desc) ? <HTML
          baseFontStyle={styles.descChannel}
          html={desc}
          ignoredStyles={[
            "font-family",
            "text-decoration-style",
            "text-decoration-color",
          ]}
          onLinkPress={(e, href, htmlAttrib) => Linking.openURL(href)}
        /> : null}


        <View style={styles.viewButtonRegisterConfirmation}>
          <View style={{ marginRight: 8, flex: 1 }}>
            <Button
              text={wordingCancel}
              onPress={onPressCancel}
              type="secondary"
              marginRig={8}
            />
          </View>

          <View style={{ marginLeft: 8, flex: 1 }}>
            <Button text={wordingOk} onPress={onPressOk} marginSize="small" />
          </View>
        </View>
      </View>
    );
  } else if (type != "" && type == "bottomSheetAboutChannel") {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column-reverse",
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
          paddingTop: 12,
        }}
      >
        <View style={{ flex: 1, paddingTop: 12 }}>
          <Button text={wordingClose} onPress={onPressClose} marginRig={8} />
        </View>
        <ScrollView
          style={{
            maxHeight: countMaxHeight(132),
            flex: 1,
          }}
        >
          <View style={styles.avatarContainerAboutChannel}>
            <Avatar
              name={title}
              imageSource={imageSource}
              fullWidth={true}
              imageSize={72}
              isShapeNotRounded={true}
              noBackground={true}
            />
          </View>

          <Text bold style={styles.titleChannel}>
            Tentang {title}
          </Text>
          <HTML
            baseFontStyle={styles.descChannel}
            html={desc}
            ignoredStyles={[
              "font-family",
              "text-decoration-style",
              "text-decoration-color",
            ]}
            onLinkPress={(e, href, htmlAttrib) => Linking.openURL(href)}
          />
        </ScrollView>
      </View>
    );
  } else if (type != "" && type == "bottomSheetJobSubscribeChannel") {
    return (
      <View
        style={{
          flex: 1,
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? screen != null && screen == "home"
                ? 10
                : 40
              : 10,
          flexDirection: "column-reverse",
        }}
      >
        <View
          style={{
            paddingHorizontal: 16,
            paddingTop: 8,
          }}
        >
          <Button text={wordingClose} onPress={onPressClose} marginRig={8} />
        </View>
        <View style={{ flex: 1 }}>
          <View
            style={{
              backgroundColor: "#FCE3EA",
              borderTopLeftRadius: 16,
              borderTopRightRadius: 16,
            }}
          >
            <Row
              style={{
                marginHorizontal: 16,
                paddingVertical: 16,
                display: "flex",
              }}
            >
              <View
                style={{
                  height: 20,
                  width: 20,
                  marginRight: 16,
                  marginTop: "auto",
                  marginBottom: "auto",
                }}
              >
                <IconWarningIconRed />
              </View>
              <Text
                style={{
                  marginTop: "auto",
                  marginBottom: "auto",
                  flex: 1,
                  fontSize: 16,
                  lineHeight: 24,
                  letterSpacing: 0.15,
                }}
              >
                Sebelum lanjut, Anda harus bergabung ke forum ini terlebih
                dahulu.
              </Text>
            </Row>
          </View>
          <ScrollView
            style={{
              maxHeight: countMaxHeight(198),
            }}
          >
            <View style={{ marginHorizontal: 16 }}>
              <View
                style={{
                  width: "100%",
                  height: 72,
                  marginTop: 24,
                  marginBottom: 8,
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Avatar
                  name={title}
                  imageSource={imageSource}
                  fullWidth={true}
                  imageSize={72}
                  isShapeNotRounded={true}
                  noBackground={true}
                />
              </View>
              <Text bold style={styles.titleChannelSubscribe}>
                Tentang {title}
              </Text>
              <HTML
                baseFontStyle={styles.descChannel}
                html={desc}
                ignoredStyles={[
                  "font-family",
                  "text-decoration-style",
                  "text-decoration-color",
                ]}
                onLinkPress={(e, href, htmlAttrib) => Linking.openURL(href)}
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  } else if (type != "" && type == "bottomSheetAboutOrganization") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <Text bold style={styles.titleOrganization}>
          Tentang {title}
        </Text>

        <View style={styles.avatarContainerOrganization}>
          <Avatar
            name={title}
            imageSource={imageSource}
            fullWidth={true}
            imageSize={100}
            isShapeNotRounded={true}
            noBackground={true}
          />
        </View>
        <HTML
          baseFontStyle={styles.descOrganization}
          html={desc}
          renderers={{
            p: (_, children) => <Text>{children}</Text>,
          }}
        />
      </View>
    );
  } else if (type != "" && type == "bottomSheetPageNotFound") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <View style={styles.iconWrapper}>
          <IconBottomSheetNotFound />
        </View>

        <Text bold style={styles.titleCenter}>
          {title}
        </Text>
        <Text regular style={styles.descCenter}>
          {desc}
        </Text>

        <View style={{ flex: 1, marginTop: 24 }}>
          <Button text={wordingClose} onPress={onPressClose} marginRig={8} />
        </View>
      </View>
    );
  } else if (type != "" && type == "BottomSheetRegister") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <View style={styles.avatarRegister}>
          <IconAlertWithStar />
        </View>

        <Text bold style={styles.titleRegister}>
          {title}
        </Text>
        <Text regular style={styles.descRegister}>
          {desc}
        </Text>

        <View style={{ flex: 1 }}>
          <Button text={wordingClose} onPress={onPressClose} marginRig={8} />
        </View>
      </View>
    );
  } else if (type != "" && type == "BottomSheetProfileUnfinished") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <View style={styles.avatarRegister}>
          <IconProfileUnfinished />
        </View>

        <Text bold style={styles.titleRegister}>
          {title}
        </Text>
        <Text regular style={styles.descRegister}>
          {desc}
        </Text>

        <View style={{ flex: 1 }}>
          <Button text={wordingClose} onPress={onPressClose} marginRig={8} />
        </View>
      </View>
    );
  } else if (type != "" && type == "BottomSheetExitChannel") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <TouchableOpacity
          style={styles.avatarContainer}
          onPress={onPressExitChannel}
        >
          <IconExitChannel />
          <Text bold style={styles.textExitChannel}>
            {title}
          </Text>
        </TouchableOpacity>

        <View style={{ flex: 1 }}>
          <Button text={wordingClose} onPress={onPressClose} marginRig={8} />
        </View>
      </View>
    );
  } else if (type != "" && type == "compatibility") {
    return (
      <View
        style={{
          marginBottom:
            platform.platform == "ios" &&
              platform.deviceHeight >= 812 &&
              platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <View style={styles.iconWrapper}>
          <IconAlertCompatibilityIOS />
        </View>
        <Text style={styles.titleCenter}>{title}</Text>
        <Text regular style={styles.descCenter}>
          {desc}
        </Text>

        <View style={styles.viewButton}>
          <View style={{ marginRight: 8, flex: 1 }}>
            <Button
              text={wordingCancel}
              onPress={onPressCancel}
              type="secondary"
              marginRig={8}
            />
          </View>

          <View style={{ marginLeft: 8, flex: 1 }}>
            <Button text={wordingOk} onPress={onPressOk} marginSize="small" />
          </View>
        </View>
      </View>
    );
  }
};

export default BottomSheet;

const styles = StyleSheet.create({
  title: {
    marginTop: 24,
    marginBottom: 16,
    textAlign: "left",
    fontSize: 20,
    color: "#1E1E20",
    fontFamily: "Roboto-Medium",
    lineHeight: 32,
    letterSpacing: 0.26,
  },
  desc: {
    textAlign: "left",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 26,
  },
  firstDescText: {
    textAlign: "left",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 26,
  },
  secondDescText: {
    textAlign: "left",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 26,
    marginTop: 16,
  },
  titleChannel: {
    marginTop: 24,
    textAlign: "left",
    fontSize: 20,
    color: "#1E1E20",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  titleChannelSubscribe: {
    marginTop: 24,
    textAlign: "left",
    fontSize: 20,
    color: "#1E1E20",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  titleOrganization: {
    marginVertical: 24,
    textAlign: "left",
    fontSize: 20,
    color: "#1E1E20",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  titleRegister: {
    marginTop: 24,
    marginBottom: 16,
    textAlign: "center",
    fontSize: 20,
    color: "#1E1E20",
    fontFamily: "Roboto-Medium",
    lineHeight: 32,
    letterSpacing: 0.26,
  },
  descChannel: {
    textAlign: "left",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    marginBottom: 24,
  },
  descOrganization: {
    textAlign: "left",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    marginBottom: 24,
  },
  descRegister: {
    textAlign: "center",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 26,
    marginBottom: 24,
  },
  viewButton: {
    marginTop: 24,
    flex: 1,
    flexDirection: "row",
  },
  avatarContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 24,
  },
  avatarContainerOrganization: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  avatarRegister: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 24,
  },
  stripContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 8,
  },
  textExitChannel: {
    marginBottom: 36,
    textAlign: "center",
    fontSize: 14,
    color: "#000000",
    fontFamily: "Roboto-Regular",
    lineHeight: 20,
    letterSpacing: 0.25,
  },
  iconWrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 24,
  },
  titleCenter: {
    marginBottom: 16,
    textAlign: "center",
    fontSize: 20,
    color: "#000000",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  descCenter: {
    textAlign: "center",
    color: "#1E1E20",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  avatarContainerAboutChannel: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 12,
  },
  titleRegistration: {
    marginTop: 24,
    textAlign: "left",
    fontSize: 20,
    color: "#1E1E20",
    fontFamily: "Roboto-Medium",
    lineHeight: 32,
    letterSpacing: 0.26,
  },
  viewButtonRegisterConfirmation: {
    marginTop: 8,
    flex: 1,
    flexDirection: "row",
  },
});
