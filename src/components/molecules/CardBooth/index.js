import { Card, Text } from "native-base";
import React from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
} from "react-native";
import { Avatar, Button } from "../../atoms";

const CardBooth = ({ data, onPressCard }) => {
  console.log("CardBooth ->", data);
  return (
    <View>
      {data && data.length
        ? data.map((item, idx) =>
            idx == 0 ? (
              <TouchableOpacity
                onPress={() => onPressCard(item)}
                style={styles.container}
              >
                <ImageBackground
                  source={{
                    uri: item.organization_cover,
                  }}
                  style={styles.containerImageBackground}
                  resizeMode="contain"
                >
                  <View stye={styles.containerImageBadge}>
                    <Image
                      source={{
                        uri: item.priority_icon,
                      }}
                      style={styles.ImageBadge}
                    />
                  </View>
                </ImageBackground>

                <View>
                  <Text style={styles.name}>{item.organization_name}</Text>
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => onPressCard(item)}
                style={styles.container}
              >
                <ImageBackground
                  source={{
                    uri: item.organization_cover,
                  }}
                  style={styles.containerImageBackground}
                  resizeMode="contain"
                >
                  <View stye={styles.containerImageBadge}>
                    <Image
                      source={{
                        uri: item.priority_icon,
                      }}
                      style={styles.ImageBadge}
                    />
                  </View>
                </ImageBackground>
                <View>
                  <Text style={styles.name}>{item.organization_name}</Text>
                </View>
              </TouchableOpacity>
            )
          )
        : null}
    </View>
  );
};

export default CardBooth;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    marginBottom: 0,
    marginHorizontal: 16,
    borderRadius: 8,
    backgroundColor: "#FFFFFF",
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  name: {
    marginBottom: 16,
    marginTop: 8,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    color: "#000000",
    textAlign: "center",
    marginHorizontal: 16,
  },
  containerImageBackground: {
    marginTop: 8,
    height: 72,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  ImageBadge: {
    marginTop: -8,
    height: 28,
    width: 87,
    borderBottomRightRadius: 14,
    borderTopLeftRadius: 8,
    opacity: 100,
  },
  containerImageBadge: {
    position: "absolute",
    top: 0,
    left: 0,
    //   paddingVertical: 16,
    //   paddingHorizontal: 16,
  },
});
