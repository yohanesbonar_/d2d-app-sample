import _ from "lodash";
import { Card, Text, Toast } from "native-base";
import React, { useState, useEffect } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Linking,
} from "react-native";
import { likeForum } from "../../../utils/network/forum/likeForum";
import CardAvatarTitle from "../CardAvatarTitle";
import CardLikeCommentShare from "../CardLikeCommentShare";
import HTML from "react-native-render-html";
import { testID } from "../../../utils";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../../app/libs/AdjustTracker";

const CardItemForum = ({
  data,
  onPressCard,
  onPressTotalLikes,
  onPressTotalComments,
  onPressShare,
  onPressComment,
  type,
  description,
  title,
  roles,
  current_date,
  server_date,
  image_user,
  thumbnail_image,
  totalComments,
  totalLikes,
  flagLike,
  xid,
  sbcChannel,
  short_description,
  isCommentEnable,
  from,
}) => {
  const [isLike, setIsLike] = useState(flagLike != null ? flagLike : false);
  const [totalLikesState, setTotalLikesState] = useState(-1);

  useEffect(() => {
    setIsLike(flagLike);
  }, [flagLike]);

  const renderDescription = () => {
    if (description != null && description != "") {
      if (type == "ForumDetail") {
        return (
          <View>
            <HTML
              {...testID("desc_forum")}
              baseFontStyle={styles.textDesc}
              html={description}
              onLinkPress={(e, href, htmlAttrib) => Linking.openURL(href)}
              ignoredStyles={[
                "font-family",
                "text-decoration-style",
                "text-decoration-color",
              ]}
            />
          </View>
        );
      } else {
        return (
          <View>
            <Text style={styles.textDesc}>
              {_.size(short_description) > 240
                ? short_description.substring(0, 240 - 3) + "..."
                : short_description}
            </Text>
            {_.size(short_description) > 240 ? (
              <Text style={styles.readMore}>Baca selengkapnya</Text>
            ) : null}
          </View>
        );
      }
    } else {
      return null;
    }
  };

  const renderMainCard = () => {
    if (type == "ForumDetail") {
      return (
        <View>
          <CardAvatarTitle
            title={title}
            roles={roles}
            current_date={current_date}
            server_date={server_date}
            image_user={image_user}
            type="Forum"
          />
          {thumbnail_image != null && thumbnail_image != "" ? (
            <View style={styles.containerThumbnailForumDetail}>
              <Image
                source={{ uri: thumbnail_image }}
                style={styles.imageThumbnail}
              />
            </View>
          ) : null}
          {/* {!_.isEmpty(description) ? (
            <View style={{ marginVertical: 2 }} />
          ) : null} */}

          {renderDescription()}
        </View>
      );
    } else {
      return (
        <TouchableOpacity onPress={onPressCard}>
          <CardAvatarTitle
            title={title}
            roles={roles}
            current_date={current_date}
            server_date={server_date}
            image_user={image_user}
            type="Forum"
            from={from}
          />
          {!_.isEmpty(short_description) ? (
            <View style={{ marginVertical: 8 }} />
          ) : null}
          {renderDescription()}
          {thumbnail_image != null && thumbnail_image != "" ? (
            <View style={styles.containerThumbnail}>
              <Image
                source={{ uri: thumbnail_image }}
                style={styles.imageThumbnail}
              />
            </View>
          ) : null}
        </TouchableOpacity>
      );
    }
  };

  const onPressLikeButton = async () => {
    let paramLike = "";
    console.log("log flagLike - > ", flagLike);
    console.log("log isLike - > ", isLike);

    if (isLike == true) {
      paramLike = "unlike";
    } else {
      paramLike = "like";
    }
    console.log("use paramLike - > ", paramLike);
    try {
      response = await likeForum(xid, paramLike, sbcChannel);
      setIsLike(paramLike == "unlike" ? false : true);
      setTotalLikesState(
        paramLike == "unlike"
          ? totalLikesState > -1
            ? totalLikesState - 1
            : totalLikes - 1
          : paramLike == "like"
          ? totalLikesState > -1
            ? totalLikesState + 1
            : totalLikes + 1
          : null
      );

      console.log("response like FORUM", response);
      if (response.header.message == "Success") {
        // await getDataForumDetail();
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  return (
    <View>
      <View
        style={
          from != null && from == "infiniteScroll"
            ? styles.containerFromInfinite
            : styles.container
        }
      >
        {renderMainCard()}
        <View style={styles.containerLikeComment(type)}>
          <View>
            <Text style={styles.textLikeKomentar}>
              {totalLikesState > -1 ? totalLikesState : totalLikes} Suka
            </Text>
          </View>
          {type == "ForumDetail" ? (
            <View>
              <Text style={styles.textLikeKomentar}>
                {totalComments} Komentar
              </Text>
            </View>
          ) : (
            <TouchableOpacity onPress={onPressTotalComments}>
              <Text style={styles.textLikeKomentar}>
                {totalComments} Komentar
              </Text>
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.lineContainer} />
        <CardLikeCommentShare
          onPressShare={onPressShare}
          onPressComment={isCommentEnable ? onPressComment : null}
          onPressLike={() => {
            AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Like);
            onPressLikeButton();
          }}
          flagLike={flagLike}
        />
      </View>
      {(from != "infiniteScroll" && type != "ForumDetail") ||
      (from != "infiniteScroll" && type == null) ? (
        <View style={styles.lineBottomContainer} />
      ) : null}
    </View>
  );
};

export default CardItemForum;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    borderRadius: 8,
  },
  containerFromInfinite: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#FFFFFF",
    marginBottom: 20,
    borderRadius: 8,
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
    paddingHorizontal: 16,
  },
  containerNameForum: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 20,
  },
  containerRightNameForum: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 16,
    alignItems: "flex-start",
  },
  textTitleForum: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 6,
    marginTop: 0,
  },
  textAdmin: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
  },
  textLikeKomentar: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 14,
    letterSpacing: 0.19,
  },
  containerLikeComment: (type) => ({
    flexDirection: "row",
    alignItems: "baseline",
    flex: 1,
    justifyContent: "space-between",
    paddingTop: type == "ForumDetail" ? 0 : 20,
  }),
  lineContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
    marginTop: 17,
  },
  lineBottomContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 8,
    borderWidth: 0.1,
    borderEndColor: "black",
  },
  textDesc: {
    // paddingTop: 16,
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    letterSpacing: 0.15,
    lineHeight: 24,
  },
  imageSizeAvatar: { height: 40, width: 40 },
  textLikeCommentShare: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
  },
  textLikeCommentShareRed: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#D01E53",
  },
  buttonLikeCommentShare: {
    flexDirection: "row",
    alignItems: "center",
  },
  containerLikeCommentShare: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    height: 48,
  },
  readMore: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#0771CD",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  containerLike: { flexDirection: "row", alignItems: "center" },
  imageThumbnail: { aspectRatio: 16 / 9, borderRadius: 8 },
  containerThumbnail: { marginTop: 20 },
  containerThumbnailForumDetail: { marginTop: 20 },
});
