import React from "react";
import { SafeAreaView, TouchableOpacity } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import _ from "lodash";
import {
  IconCharactersDisabled,
  IconCharactersEnabled,
  IconLowerCaseDisabled,
  IconLowerCaseEnabled,
  IconMoreVertial,
  IconNumbersDisabled,
  IconNumbersEnabled,
  IconSymbolsDisabled,
  IconSymbolsEnabled,
  IconUpperCaseDisabled,
  IconUpperCaseEnabled,
} from "../../../assets";
import platform from "../../../../theme/variables/d2dColor";
const CardValidationPassword = ({
  isCharactersMinimum,
  isLowercase,
  isUppercase,
  isNumber,
  isSpecialCharacters,
}) => {
  return (
    <View style={{}}>
      <Text style={styles.textNeedAtLeast}>Password needs at least:</Text>
      <View style={styles.containerValidation}>
        <View style={styles.cardItemVal}>
          {isCharactersMinimum ? (
            <IconCharactersEnabled />
          ) : (
            <IconCharactersDisabled />
          )}
          <Text style={styles.textNotes}>Characters</Text>
        </View>
        <View style={styles.cardItemVal}>
          {isLowercase ? <IconLowerCaseEnabled /> : <IconLowerCaseDisabled />}
          <Text style={styles.textNotes}>Lowercase</Text>
        </View>
        <View style={styles.cardItemVal}>
          {isUppercase ? <IconUpperCaseEnabled /> : <IconUpperCaseDisabled />}
          <Text style={styles.textNotes}>Uppercase</Text>
        </View>
        <View style={styles.cardItemVal}>
          {isNumber ? <IconNumbersEnabled /> : <IconNumbersDisabled />}
          <Text style={styles.textNotes}>Numbers</Text>
        </View>
        <View style={styles.cardItemVal}>
          {isSpecialCharacters ? (
            <IconSymbolsEnabled />
          ) : (
            <IconSymbolsDisabled />
          )}
          <Text style={styles.textNotes}>Symbols</Text>
        </View>
      </View>
    </View>
  );
};

export default CardValidationPassword;

const styles = StyleSheet.create({
  textNeedAtLeast: {
    fontFamily: "Roboto-Regular",
    lineHeight: 20,
    letterSpacing: 0.25,
    fontSize: 14,
    color: "#000000",
  },
  containerValidation: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 16,
  },
  cardItemVal: {
    flexDirection: "column",
    alignItems: "center",
  },
  textNotes: {
    fontFamily: "Roboto-Regular",
    lineHeight: 16,
    letterSpacing: 0.19,
    fontSize: 12,
    color: "#000000",
    marginTop: 8,
  },
});
