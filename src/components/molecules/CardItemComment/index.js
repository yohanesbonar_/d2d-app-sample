import _ from "lodash";
import { Card, Text, Toast } from "native-base";
import React, { useState, useEffect } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../../app/libs/AdjustTracker";
import { likeCommentReply } from "../../../utils/network/forum/likeCommentReply";
import CardAvatarTitle from "../CardAvatarTitle";
import CardItemCommentInComment from "../CardItemCommentInComment";

const CardItemComment = ({
  data,
  onPressCard,
  onPressAmountLikes,
  onPressAmountComments,
  onPressShare,
  onPressComment,
  type,
  onPressAnotherComment,
  name,
  speacialistName,
  currentDdate,
  serverDate,
  imageUser,
  commentDescription,
  totalLikes,
  totalReplies,
  dataReplies,
  flagLikeComment,
  onPressReply,
  xid,
  sbcChannel,
}) => {
  const [isLike, setIsLike] = useState(
    flagLikeComment != null ? flagLikeComment : false
  );
  const [totalLikesState, setTotalLikesState] = useState(-1);

  useEffect(() => {
    setIsLike(flagLikeComment);
  }, [flagLikeComment]);

  // const onPressCardLike = () => {
  //   setIsLike(isLike ? false : true);
  // };

  const renderLikeButton = () => {
    if (isLike) {
      return (
        <TouchableOpacity
          style={{ paddingRight: 10 }}
          // onPress={onPressLike}
          onPress={() => {
            onPressLikeButtonComment();
            AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar_Like);
          }}
        >
          <Text style={styles.textLikeReplyLeftRed}>SUKA</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={{ paddingRight: 10 }}
          // onPress={onPressLike}
          onPress={() => {
            onPressLikeButtonComment();
            AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar_Like);
          }}
        >
          <Text style={styles.textLikeReplyLeft}>SUKA</Text>
        </TouchableOpacity>
      );
    }
  };

  const renderDesc = () => {
    if (commentDescription != null && commentDescription != "") {
      if (type == "ForumDetail") {
        if (commentDescription.length > 240) {
          return (
            <TouchableOpacity
              style={styles.containerDescription}
              onPress={onPressAnotherComment}
            >
              <Text style={styles.textDesc}>
                {commentDescription.length > 240
                  ? commentDescription.substring(0, 240 - 3) + "..."
                  : commentDescription}
              </Text>
              {commentDescription.length > 240 ? (
                <Text style={styles.readMore}>Baca selengkapnya</Text>
              ) : null}
            </TouchableOpacity>
          );
        } else {
          return (
            <View style={styles.containerDescription}>
              <Text style={styles.textDesc}>{commentDescription}</Text>
            </View>
          );
        }
      } else {
        return (
          <View style={styles.containerDescription}>
            <Text style={styles.textDesc}>{commentDescription}</Text>
          </View>
        );
      }
    } else {
      return null;
    }
  };

  const onPressLikeButtonComment = async () => {
    let paramLike = "";
    console.log("log isLike - > ", isLike);
    if (isLike == true) {
      paramLike = "unlike";
    } else {
      paramLike = "like";
    }
    console.log("use paramLike - > ", paramLike);
    try {
      response = await likeCommentReply(xid, paramLike, sbcChannel);
      console.log("response like Button Comment", response);

      setIsLike(paramLike == "unlike" ? false : true);
      setTotalLikesState(
        paramLike == "unlike"
          ? totalLikesState > -1
            ? totalLikesState - 1
            : totalLikes - 1
          : paramLike == "like"
          ? totalLikesState > -1
            ? totalLikesState + 1
            : totalLikes + 1
          : null
      );

      if (response.header.message == "Success") {
        // onRefreshdata();
      } else {
        Toast.show({
          text: "Something went wrong!! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  return (
    <View>
      <View style={styles.container}>
        <CardAvatarTitle
          title={name}
          roles={speacialistName}
          current_date={currentDdate}
          server_date={serverDate}
          image_user={imageUser}
        />
        {renderDesc()}
        <View style={styles.containerLikeReply}>
          <View style={{ flexDirection: "row" }}>
            {renderLikeButton()}

            <TouchableOpacity
              style={{ paddingLeft: 10 }}
              onPress={onPressReply}
            >
              <Text style={styles.textLikeReplyLeft}>BALAS</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <Text style={styles.textLikeKomentar}>
              {totalLikesState > -1 ? totalLikesState : totalLikes} Suka
            </Text>
          </TouchableOpacity>
        </View>
        {!_.isEmpty(dataReplies) &&
        totalReplies > 1 &&
        type == "ForumDetail" ? (
          <TouchableOpacity
            style={styles.containerAnotherReply}
            onPress={onPressAnotherComment}
          >
            <Text style={styles.textAnotherReply}>Lihat balasan lainnya…</Text>
          </TouchableOpacity>
        ) : null}
        {!_.isEmpty(dataReplies) && type == "ForumDetail" ? (
          <CardItemCommentInComment
            titleReply={dataReplies.by.name}
            rolesReply={dataReplies.by.spesialization}
            currentDateReply={dataReplies.created_at}
            serverDateReply={serverDate}
            imageUserReply={dataReplies.by.display_picture}
            commentReply={dataReplies.comment}
            totalLikeReply={dataReplies.total_likes}
            flagLikeReply={dataReplies.liked}
            typeCard={type}
            openReadMore={onPressAnotherComment}
            xid={dataReplies.xid}
            sbcChannel={sbcChannel}
          />
        ) : null}
      </View>
      {type == "ReplyComment" ? null : (
        <View style={styles.lineBottomContainer} />
      )}
    </View>
  );
};

export default CardItemComment;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    borderRadius: 8,
  },
  lineBottomContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
    marginTop: 17,
  },
  textName: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 6,
    marginTop: 0,
  },
  containerNameSpecialist: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 16,
    alignItems: "flex-start",
  },
  textSpecialist: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  imageSizeAvatar: { height: 40, width: 40 },
  containerNameAll: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 20,
  },
  textDesc: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    letterSpacing: 0.15,
    lineHeight: 24,
  },
  readMore: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#0771CD",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  containerLikeReply: {
    flexDirection: "row",
    alignItems: "baseline",
    flex: 1,
    justifyContent: "space-between",
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  containerDescription: {
    backgroundColor: "#EBEBEB",
    borderRadius: 8,
    padding: 16,
    marginTop: 20,
  },
  textLikeKomentar: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textLikeReplyLeft: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#666666",
    letterSpacing: 0.9,
    lineHeight: 16,
  },
  textLikeReplyLeftRed: {
    fontFamily: "Roboto-Medium",
    fontWeight: "bold",
    fontSize: 15,
    color: "#D01E53",
    letterSpacing: 0.9,
    lineHeight: 16,
  },
  textAnotherReply: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#0771CD",
    letterSpacing: 0.9,
    lineHeight: 16,
    textTransform: "uppercase",
  },
  containerAnotherReply: { paddingHorizontal: 16, paddingTop: 17 },
});
