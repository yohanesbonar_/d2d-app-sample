import { Input, Item, Text } from "native-base";
import React from "react";
import { StyleSheet, View } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import { ThemeD2D } from "../../../../theme";
import {
  IconArrowRight,
  IconClose,
  IconMoreVerticalGrey,
} from "../../../assets";
import _ from "lodash";
const InputField = ({
  label,
  placeholder,
  onChangeText,
  value,
  isError,
  messageError,
  isEditable,
  keyboardType,
  maxLength,
  typeIconRight,
  onPress,
  subLabel,
  rightMenu,
  onPressRightMenu,
  size,
  onPressIconRight,
  subData,
  onFocus
}) => {
  const TextLabel = () => {
    if (size == "medium") {
      return (
        <Text H7 style={styles.label}>
          {label}
        </Text>
      );
    }

    return (
      <Text SubtitleMedium style={styles.label}>
        {label}
      </Text>
    );
  };
  const ButtonRightMenu = () => {
    return (
      <TouchableOpacity onPress={onPressRightMenu}>
        <IconMoreVerticalGrey />
      </TouchableOpacity>
    );
  };

  const Input = () => {
    if (onPress) {
      return (
        <View style={{
          borderColor: _.isEmpty(subData) ? undefined : "#EBEBEB",
          borderWidth: _.isEmpty(subData) ? undefined : 1,
          borderRadius: _.isEmpty(subData) ? undefined : 4,
          marginBottom: _.isEmpty(subData) ? undefined : 20,
        }}>
          <TouchableOpacity
            style={styles.textInputWrapper(isError)}
            onPress={onPress}
          >
            <Text style={styles.textInput}>{value}</Text>
            {typeIconRight == "arrow" && <IconArrowRight />}
          </TouchableOpacity>

          {!_.isEmpty(subData) && (
            subData.map((value, index) => {
              if (value.id != 0) {
                return (
                  <View key={value.id} style={styles.subDataWrapper(index)}>
                    <Text style={styles.textSubData}>{value.description}</Text>
                  </View>
                )
              }
            })
          )}
        </View>

      );
    } else if (onPressIconRight) {
      return (
        <View style={styles.textInputWrapper(isError)}>
          <TextInput
            style={styles.textInput}
            value={value}
            placeholder={placeholder}
            onChangeText={onChangeText}
            editable={isEditable}
            keyboardType={keyboardType}
            maxLength={maxLength}
            placeholderTextColor={"#666666"}
          />
          {typeIconRight == "close" && (
            <TouchableOpacity
              onPress={onPressIconRight}
              style={{ marginRight: -14 }}
            >
              <IconClose />
            </TouchableOpacity>
          )}
        </View>
      );
    }
    return (
      <View style={{
        borderColor: _.isEmpty(subData) ? undefined : "#EBEBEB",
        borderWidth: _.isEmpty(subData) ? undefined : 1,
        borderRadius: _.isEmpty(subData) ? undefined : 4,
        marginBottom: _.isEmpty(subData) ? undefined : 20,
      }}>
        <View style={styles.textInputWrapper(isError)}>
          <TextInput
            style={styles.textInput}
            value={value}
            placeholder={placeholder}
            onChangeText={onChangeText}
            editable={isEditable}
            keyboardType={keyboardType}
            maxLength={maxLength}
            placeholderTextColor={"#666666"}
            onFocus={onFocus}
          />

        </View>
        {!_.isEmpty(subData) && (
          subData.map((value, index) => {
            if (value.id != 0) {
              return (
                <View key={value.id} style={styles.subDataWrapper(index)}>
                  <Text style={styles.textSubData}>{value.description}</Text>
                </View>
              )
            }
          })
        )}
      </View>
    );
  };
  return (
    <View>
      {!_.isEmpty(label) && (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: size == "medium" ? 20 : 12,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <TextLabel />
            {!_.isEmpty(subLabel) && (
              <View style={styles.textSubLabelContainer}>
                <Text style={styles.textSubLabel}>{subLabel}</Text>
              </View>
            )}
          </View>
          {rightMenu == "moreVertical" && <ButtonRightMenu />}
        </View>
      )}

      {Input()}
      {isError == true && (
        <Text textError style={styles.textError}>
          {messageError}
        </Text>
      )}
    </View>
  );
};

export default InputField;

const styles = StyleSheet.create({
  label: {},
  textInput: {
    color: "#000000",
    fontFamily: "Roboto-Regular",
    //lineHeight: 24, //commented because to support ellpisize 
    fontSize: 16,
    letterSpacing: 0.15,
    flex: 1,
  },
  textInputWrapper: (isError) => ({
    backgroundColor: "#EBEBEB",
    borderRadius: 4,
    paddingHorizontal: 16,
    height: 56,
    marginBottom: 20,
    borderWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: isError ? ThemeD2D.textErrorColor : "#EBEBEB",
  }),
  textError: {
    marginTop: -10,
    marginBottom: 10,
  },
  textSubLabel: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    letterSpacing: 0.1,
    lineHeight: 24,
    color: "#FFFFFF",
  },
  textSubLabelContainer: {
    backgroundColor: "#178038",
    borderRadius: 4,
    paddingHorizontal: 8,
    marginLeft: 16,
  },
  textSubData: {
    color: "#000000",
    fontFamily: "Roboto-Regular",
    lineHeight: 24,
    fontSize: 16,
    letterSpacing: 0.15,
  },
  subDataWrapper: (index) => ({
    justifyContent: 'center',
    height: 56,
    marginTop: index == 0 ? -20 : undefined,
    marginLeft: 16
  })
});
