import { Text } from "native-base";
import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { IconEmailBlack, IconInstagram } from "../../../assets";

const CardContact = ({ onPress, contact, icon }) => {

  const Icon = () => {
    if (icon == "instagram") {
      return <IconInstagram />
    }
    return <IconEmailBlack />
  }

  return (
    <View style={styles.card}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.contentWrapper}>
          <Icon />
          <Text SubtitleMedium style={styles.textContact}>
            {contact}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CardContact;

const styles = StyleSheet.create({
  card: {
    padding: 0,
    justifyContent: "center",
    marginTop: 0,
    marginBottom: 20,
    marginRight: 0,
    marginLeft: 0,
    flex: 0,
    borderRadius: 8,
    backgroundColor: "#FFFFFF",
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  contentWrapper: {
    flexDirection: "row",
    marginLeft: 16,
    height: 56,
    alignItems: "center",
  },
  textContact: {
    marginLeft: 16,
  },
});
