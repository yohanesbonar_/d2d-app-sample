import { Text } from "native-base";
import React from "react";
import { StyleSheet, View } from "react-native";
import { IconEmptyData } from "../../../assets";

const EmptyDataKonferensi = ({ title, subtitle }) => {
  return (
    <View style={styles.container}>
      <IconEmptyData />
      <Text H6 style={styles.titleMessage}>
        {title}
      </Text>
      <Text style={styles.subtitle}>{subtitle}</Text>
    </View>
  );
};

export default EmptyDataKonferensi;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  titleMessage: {
    marginTop: 32,
    marginBottom: 16,
  },
  subtitle: {
    color: "#000000",
    textAlign: "center",
  },
});
