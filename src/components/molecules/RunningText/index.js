import React, { useEffect, useState, useRef } from "react";
import {
  StyleSheet,
  ScrollView,
  Animated,
  UIManager,
  findNodeHandle,
} from "react-native";

const RunningText = ({ text, textStyle, delay, speed }) => {
  const [distance, setDistance] = useState(0);
  const [isScrolling, setScrolling] = useState(false);
  const [textDelay, setTextDelay] = useState(delay ? delay : 1000);
  const [textSpeed, setTextSpeed] = useState(speed ? speed : 20);
  const textRef = useRef();
  const scrollViewRef = useRef();
  const textScrollPosition = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    if (!isScrolling && distance && distance > 0) {
      startTextScrolling();
    }
  }, [isScrolling, distance]);

  const measureWidth = async (component) => {
    return new Promise((resolve) => {
      UIManager.measure(findNodeHandle(component), (x, y, w) => {
        resolve(w);
      });
    });
  };

  const calculateWidth = async () => {
    if (!scrollViewRef.current || !textRef.current) {
      return;
    }
    let [containerWidth, textWidth] = await Promise.all([
      measureWidth(scrollViewRef.current),
      measureWidth(textRef.current),
    ]);
    setDistance(textWidth - containerWidth);
  };

  const startTextScrolling = () => {
    setScrolling(true);
    setTimeout(() => {
      Animated.timing(textScrollPosition, {
        toValue: -distance,
        duration: distance * textSpeed,
      }).start((finished) => {
        if (finished) {
          resetTextScrolling();
        }
      });
    }, textDelay);
  };

  const resetTextScrolling = () => {
    setTimeout(() => {
      textScrollPosition.setValue(0);
      setScrolling(false);
    }, 1000);
  };

  return (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      scrollEnabled={false}
      horizontal={true}
      ref={scrollViewRef}
      onContentSizeChange={calculateWidth}
    >
      <Animated.Text
        ref={textRef}
        style={[textStyle, { transform: [{ translateX: textScrollPosition }] }]}
      >
        {text}
      </Animated.Text>
    </ScrollView>
  );
};

export default RunningText;

const styles = StyleSheet.create({});
