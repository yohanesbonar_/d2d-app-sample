import React from "react";
import { Text } from "native-base";
import { StyleSheet, View } from "react-native";
import { IconEmptyBookmark, IconEmptyDownload, IconEmptyNotification, IconNotFound } from "../../../assets";

const SearchDataNotFound = ({ title, subtitle, type }) => {
  const Icon = () => {
    if (type == "notification") {
      return <IconEmptyNotification />;
    }
    else if (type == "bookmark") {
      return <IconEmptyBookmark />;
    }

    else if (type == "download") {
      return <IconEmptyDownload />;
    }

    return <IconNotFound />;
  };
  return (
    <View>
      <View style={styles.notFoundContainer}>
        <Icon />
        <Text H6 style={styles.title}>
          {title}
        </Text>
      </View>
      <Text style={styles.subtitle}>{subtitle}</Text>
    </View>
  );
};

export default SearchDataNotFound;

const styles = StyleSheet.create({
  notFoundContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    marginTop: 32,
  },
  subtitle: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
  },
});
