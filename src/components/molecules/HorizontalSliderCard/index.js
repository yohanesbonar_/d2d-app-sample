import React, { useState, useEffect, useRef } from "react";
import { View, Text, Icon, Toast, Container } from "native-base";
import { StyleSheet, TouchableOpacity, FlatList } from "react-native";
import {
  IconAtozSearch,
  IconCmeSearch,
  IconEventSearch,
  IconHomeObatatoz,
  IconJurnal,
  IconLightBlueArrow,
  IconLiveNowWebinar,
  IconRecordedWebinar,
  IconUpcomingWebinar,
} from "../../../assets";
import { BottomSheet, CardItemSearch } from "../../molecules";
import _ from "lodash";
import platform from "../../../../theme/variables/platform";

const HorizontalSliderCard = ({
  title,
  type,
  data,
  serverDate,
  limit,
  goToScreenSearchAll,
  goToScreenBaseOnCategory,
}) => {
  const [heightFooter, setHeightFooter] = useState(0);
  const _renderItemRecordedWebinar = ({ item, index, separators }) => {
    let limitIndex = limit ? limit : 3;
    if (index < limitIndex) {
      return (
        <View
          style={{
            marginRight:
              data != null && _.size(data) <= limit && _.size(data) - 1 == index
                ? 16
                : 0,
          }}
        >
          <CardItemSearch
            type={type}
            data={item}
            onPress={() => goToScreenBaseOnCategory(item)}
            serverDate={serverDate}
            component="homeslider"
            onPressBookmark={(isBookmark) => {
              item.flag_bookmark = isBookmark;
            }}
            onLayout={onLayout}
          />
        </View>
      );
    } else {
      return null;
    }
  };

  const onLayout = ({
    nativeEvent: {
      layout: { x, y, width, height },
    },
  }) => {
    setHeightFooter(height);
  };

  const _renderItemFooter = () => {
    return (
      <TouchableOpacity onPress={() => goToScreenSearchAll()}>
        <View style={styles.cardFooter(heightFooter, type)}>
          <IconLightBlueArrow />
          <Text style={styles.textLihatSemuaCenter(type)}>LIHAT SEMUA</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderIcon = () => {
    if (type != null) {
      switch (type) {
        case "UpcomingWebinar":
          return <IconUpcomingWebinar style={{ marginRight: 8 }} />;
        case "RecordedWebinar":
          return <IconRecordedWebinar style={{ marginRight: 8 }} />;
        case "LiveWebinar":
          return <IconLiveNowWebinar style={{ marginRight: 8 }} />;
        case "Event":
          return <IconEventSearch style={{ marginRight: 8 }} />;
        case "Jurnal":
          return <IconJurnal style={{ marginRight: 8 }} />;
        case "Guideline":
          return <IconJurnal style={{ marginRight: 8 }} />;
        case "ObatAtoz":
          return <IconAtozSearch style={{ marginRight: 8 }} />;
        case "Cme":
          return <IconCmeSearch style={{ marginRight: 8 }} />;
        default:
          return <IconRecordedWebinar style={{ marginRight: 8 }} />;
      }
    }
    return <IconRecordedWebinar style={{ marginRight: 8 }} />;
  };

  return data != null && data.length ? (
    <View>
      <View style={{ flexDirection: "column", marginTop: 10 }}>
        <View style={styles.containerGroup}>
          <View style={styles.containerTitle}>
            {renderIcon()}
            <Text style={styles.textGroup}>{title != null ? title : ""}</Text>
          </View>
          <TouchableOpacity onPress={() => goToScreenSearchAll()}>
            <Text style={styles.textLihatSemua}>
              {data != null && _.size(data) > limit && data.length > limit
                ? "LIHAT SEMUA"
                : ""}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{ paddingTop: 20 }}>
          {data != null && _.size(data) > 1 && (
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={data}
              renderItem={_renderItemRecordedWebinar}
              style={{}}
              ListFooterComponent={
                data != null && _.size(data) > limit && data.length > limit
                  ? _renderItemFooter
                  : null
              }
            />
          )}
          {data != null && _.size(data) <= 1 && _.size(data) != 0 && (
            <View style={{ paddingHorizontal: 16 }}>
              <CardItemSearch
                type={type}
                data={data[0]}
                onPress={() => goToScreenBaseOnCategory(data[0])}
                serverDate={serverDate}
                onPressBookmark={(isBookmark) => {
                  data[0].flag_bookmark = isBookmark;
                }}
                onLayout={onLayout}
              />
            </View>
          )}
        </View>
      </View>
    </View>
  ) : null;
};

export default HorizontalSliderCard;

const styles = StyleSheet.create({
  containerGroup: {
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 16,
    justifyContent: "space-between",
  },
  containerTitle: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
  },
  textGroup: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textLihatSemua: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#0771CD",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    lineHeight: 16,
    letterSpacing: 0.34,
  },
  textLihatSemuaCenter: (type) => [
    {
      fontSize: 14,
      fontFamily: "Roboto-Medium",
      color: "#0771CD",
      lineHeight: 16,
      letterSpacing: 0.34,
      marginTop: type == "ObatAtoz" || type == "Guideline" ? 20 : 32,
      marginRight: type == "ObatAtoz" ? 0 : 0,
    },
  ],
  cardFooter: (heightFooter, type) => [
    {
      height: heightFooter,
      marginHorizontal: 16,
      flexDirection: type == "ObatAtoz" ? "column" : "column",
      backgroundColor: "#FFFFFF",
      marginBottom: 20,
      borderRadius: 8,
      borderColor: "#0000001F",
      elevation: 2,
      shadowColor: "#000000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.08,
      shadowRadius: 4,
      alignItems: "center",
      justifyContent: "center",
      paddingHorizontal: 32,
    },
  ],
});
