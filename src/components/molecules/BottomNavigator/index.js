import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../../app/libs/AdjustTracker";
import { ThemeD2D } from "../../../../theme";
//import {colors} from '../../../utils';
import { TabItem } from "../../atoms";

const BottomNavigator = ({ navigation }) => {
  const { state } = navigation;
  const [isHideButtonNav, setIsHideButtonNav] = useState(false);
  const [pageIndex, setPageIndex] = useState(0);
  const [refreshPage, setRefreshPage] = useState(null);

  useEffect(() => {
    console.log("state", state);
    if (state.routes != null && state.routes.length) {
      let isHideTab = false;
      let refreshPage = null;
      state.routes.forEach((element) => {
        console.log("element --", element);
        if (element.key != null && element.key == "Home") {
          if (
            pageIndex == 0 &&
            element.params != null &&
            element.params.hideTab != null &&
            element.params.hideTab
          ) {
            isHideTab = true;
          }
        }
      });
      setIsHideButtonNav(isHideTab);
    }
  }, [navigation]);

  useEffect(() => {
    console.log("pageindex", pageIndex);
    if (pageIndex != 0 && isHideButtonNav) {
      setIsHideButtonNav(false);
    }
  }, [isHideButtonNav, pageIndex]);

  const setAdjustToken = (index) => {
    if (index == 0) {
      AdjustTracker(AdjustTrackerConfig.Homepage_Home);
    } else if (index == 1) {
      AdjustTracker(AdjustTrackerConfig.Homepage_Feed);
    } else if (index == 2) {
      AdjustTracker(AdjustTrackerConfig.Homepage_Album_P2KB);
    }
  };

  const checkRefreshPage = (index) => {
    if (state.routes != null && state.routes.length) {
      state.routes.forEach((element) => {
        if (element.key != null && element.key == "Feed" && index == 1) {
          if (
            element.params != null &&
            element.params.onRefreshData != null &&
            element.params.onRefreshData
          ) {
            element.params.onRefreshData();
          }
        } else if (element.key != null && element.key == "Home" && index == 0) {
          if (
            element.params != null &&
            element.params.onRefreshData != null &&
            element.params.onRefreshData
          ) {
            element.params.onRefreshData();
          }
        }
      });
    }
  };

  return isHideButtonNav ? null : (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        let label = route.routeName;

        if (index == 2) {
          label = "Album P2KB";
        }
        const isFocused = state.index === index;

        const onPress = () => {
          setAdjustToken(index);
          if (!isFocused) {
            if (index == 1 || index == 2) {
              let params = {
                isFromBottomNav: true,
                title: label,
              };

              if (index == 1) {
                checkRefreshPage(index);
              }
              navigation.navigate(route.routeName, params);
            } else {
              if (index == 0) {
                checkRefreshPage(index);
              }
              navigation.navigate(route.routeName);
            }
            setPageIndex(index);
          }
        };

        return (
          <TabItem
            key={index}
            title={label}
            active={isFocused}
            onPress={onPress}
          />
        );
      })}
    </View>
  );
};

export default BottomNavigator;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: ThemeD2D.bottomNavHeight,
    paddingTop: 6,
    backgroundColor: ThemeD2D.bottomNavBackground,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.02,
    shadowRadius: 4,
  },
});
