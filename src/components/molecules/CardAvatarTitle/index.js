import { Text } from "native-base";
import React, { useState } from "react";
import { StyleSheet, View, Image } from "react-native";
import { DiffTimeCount } from "../../../utils/commons";

const CardAvatarTitle = ({
  title,
  roles,
  current_date,
  server_date,
  image_user,
  type,
  from,
}) => {
  const renderDiffTime = () => {
    let textTime = DiffTimeCount(current_date, server_date);
    return textTime;
  };

  let defaultCover = require("../../../../src/assets/images/blank.jpg");
  let uriAvatar = image_user != null ? { uri: image_user } : defaultCover;
  if (type == "Forum") {
    return (
      <View>
        <View style={styles.containerNameForum}>
          <Text style={styles.textDiffTimeTop}>
            {from == "infiniteScroll" ? "DISKUSI • " : null}
            {current_date != null ? renderDiffTime() : null}
          </Text>
          <Text style={styles.textTitleForum}>{title}</Text>
        </View>
      </View>
    );
  } else {
    return (
      <View>
        <View style={styles.containerName}>
          <Image
            source={uriAvatar}
            style={styles.imageSizeAvatar}
            resizeMode="cover"
          />
          <View style={styles.containerRightName}>
            <Text style={styles.textTitle}>{title}</Text>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textRole}>
                {roles != "" && roles != null
                  ? roles.length > 33
                    ? roles.substring(0, 33 - 3) + "..." + " • "
                    : roles + " • "
                  : null}
              </Text>
              <Text style={styles.textDiffTime}>
                {current_date != null ? renderDiffTime() : null}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
};

export default CardAvatarTitle;

const styles = StyleSheet.create({
  containerName: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 20,
  },
  containerNameForum: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    paddingTop: 20,
  },
  containerRightName: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 16,
    alignItems: "flex-start",
  },
  textTitle: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 6,
    marginTop: 0,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textTitleForum: {
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    color: "#000000",
    marginTop: 8,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textRole: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textDiffTime: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textDiffTimeTop: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  imageSizeAvatar: { height: 40, width: 40, borderRadius: 20 },
});
