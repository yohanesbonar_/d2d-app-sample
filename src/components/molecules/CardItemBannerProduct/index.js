import { Text } from "native-base";
import React, { useState } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import {
  IconBlueArrow,
  IconDkonsulChannel,
  IconOrangeArrow,
  IconPurpleArrow,
  IconTemanBumilChannel,
  IconTemanDiabetesChannel,
} from "../../../assets";

const CardItemBannerProduct = ({ name, onPress }) => {
  return (
    <TouchableOpacity style={styles.containerBanner(name)} onPress={onPress}>
      <View style={styles.containerLeftBannerInside(name)}>
        {name == "Dkonsul" ? (
          <IconDkonsulChannel />
        ) : name == "Teman Bumil" ? (
          <IconTemanBumilChannel />
        ) : (
          <IconTemanDiabetesChannel />
        )}
        <Text style={styles.textBanner(name)}>Daftar {name}</Text>
      </View>
      {name == "Dkonsul" ? (
        <IconBlueArrow />
      ) : name == "Teman Bumil" ? (
        <IconPurpleArrow />
      ) : (
        <IconOrangeArrow />
      )}
    </TouchableOpacity>
  );
};

export default CardItemBannerProduct;

const styles = StyleSheet.create({
  containerBanner: (name) => ({
    backgroundColor:
      name == "Dkonsul"
        ? "#E2F1FC"
        : name == "Teman Bumil"
        ? "#EEE5EF"
        : name == "Teman Diabetes"
        ? "#FCE3EA"
        : null,
    padding: 16,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 8,
    marginRight: 16,
  }),
  containerLeftBannerInside: (name) => ({
    flexDirection: "column",
    marginRight: name == "Dkonsul" ? 88 : 53,
  }),
  textBanner: (name) => ({
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color:
      name == "Dkonsul"
        ? "#0771CD"
        : name == "Teman Bumil"
        ? "#A16DA9"
        : name == "Teman Diabetes"
        ? "#EE424E"
        : null,
    paddingTop: 12,
  }),
});
