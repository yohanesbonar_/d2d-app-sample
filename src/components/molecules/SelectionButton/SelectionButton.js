import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "../../atoms";

const SelectionButton = ({ onPress, keySelected, data }) => {
  let buttons = [];

  {
    data.map((res, i) => {
      buttons.push(
        <View
          key={i}
          style={[styles.buttonWrapper, { marginLeft: i > 0 ? 16 : undefined }]}
        >
          <Button
            text={res.text}
            size="small"
            type="buttonSelections"
            onPress={() => onPress(res, i)}
            isSelected={res.key == keySelected ? true : false}
          />
        </View>
      );
    });
  }
  return <View style={styles.container}>{buttons}</View>;
};

export default SelectionButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },

  buttonWrapper: {
    flex: 1,
  },
});
