import React, { useState, useEffect } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import {
  IconLocation,
  IconSkp,
  IconSpecialist,
  IconVerified,
} from "../../../assets";
import { getData, KEY_ASYNC_STORAGE } from "../../../utils";
import { Button, PhotoProfileHome } from "../../atoms";

const HomeProfile = ({
  name,
  practiceLocation,
  specialist,
  pointSKP,
  photo,
  onPress,
  textButton,
  isVerified,
  maxLineSpecialist,
  maxLinePracticeLocation,
}) => {
  return (
    <View style={{ flex: 1, flexDirection: "column", marginBottom: 12 }}>
      <View style={styles.profileMenu}>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <PhotoProfileHome imageURL={photo} />
        </View>
        <View style={styles.infoContainer}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.textNamaDokter} numberOfLines={1}>
              {name}
            </Text>
            {isVerified && <IconVerified style={{ marginLeft: 5 }} />}
          </View>

          <View style={styles.viewPoinLocation}>
            <IconLocation />
            <Text
              numberOfLines={maxLinePracticeLocation}
              style={styles.textRumahSakit}
            >
              {practiceLocation}
            </Text>
          </View>
          <View style={styles.viewPoinSpecialist}>
            <IconSpecialist />
            <Text
              numberOfLines={maxLineSpecialist}
              style={styles.textSpecialist}
            >
              {specialist}
            </Text>
          </View>
          <View style={styles.viewPoinSKP}>
            <IconSkp />
            <Text style={styles.textPoinSKP}>
              {pointSKP > 0 ? pointSKP : 0} Poin SKP
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.buttonWrapper}>
        <Button text={textButton} onPress={onPress} size="small" />
      </View>
    </View>
  );
};

export default HomeProfile;

const styles = StyleSheet.create({
  layoutPhoto: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  profileMenu: {
    marginVertical: 16,
    flex: 1,
    flexDirection: "row",
    marginHorizontal: 16,
  },
  textRumahSakit: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#666666",
    marginLeft: 10,
  },
  textSpecialist: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#666666",
    marginLeft: 8,
  },
  textPoinSKP: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#666666",
    marginLeft: 8,
  },
  viewPoinPoin: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 4,
  },
  viewPoinLocation: {
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 2,
    marginVertical: 4,
  },
  viewPoinSpecialist: {
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 0,
    marginVertical: 4,
  },
  viewPoinSKP: {
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 0,
    marginVertical: 4,
  },
  textNamaDokter: {
    fontSize: 16,
    fontFamily: "Roboto-Medium",
    color: "#000000",
    marginVertical: 4,
  },
  textLihatProfil: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#FFFFFF",
    marginVertical: 10,
  },
  buttonLihatProfile: {
    backgroundColor: "#0771CD",
    alignItems: "center",
    marginHorizontal: 16,
    borderRadius: 5,
    marginTop: 16,
  },
  infoContainer: {
    flex: 1,
    marginLeft: 20,
    marginRight: 8,
  },
  buttonWrapper: {
    marginHorizontal: 16,
  },
});
