import React, { useState } from "react";
import _ from "lodash";
import {
  BackHandler,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
} from "react-native";
import { IconClose, IconSearch } from "../../../assets";

const SearchBarField = ({
  inputText,
  type,
  onPress,
  textplaceholder,
  button,
  onChange,
  onFocus,
  onButtonClose,
  onBlur,
  inputReff,
  onChangeText,
  isCloseButton,
  onPressCloseButton,
}) => {
  // console.log("inputText: ", inputText);
  const [close, setClose] = useState(false);
  const [textInput, setTextInput] = useState(
    _.isEmpty(inputText) ? "" : inputText
  );
  const [isFocus, setIsFocus] = useState(false);
  const checkClose = () => {};

  if (
    (type == "searchHome" && button == "true") ||
    (type == "searchChannel" && button == "true")
  ) {
    return (
      <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
        <TouchableOpacity
          style={styles.layoutSearchButtonHome}
          onPress={onPress}
        >
          <IconSearch />
          <Text style={styles.textCariKontenD2D}>{textplaceholder}</Text>
        </TouchableOpacity>
      </View>
    );
  } else if (type == "searchKonten") {
    return (
      <View style={styles.layoutSearchKonten}>
        <TextInput
          placeholder={textplaceholder}
          placeholderTextColor="#989898"
          style={styles.textInputSearchKonten}
          onChangeText={onChangeText}
          onFocus={onFocus}
          value={inputText}
          ref={inputReff}
          keyboardType={
            Platform.OS === "ios" ? "ascii-capable" : "visible-password"
          }
        />
        {isCloseButton && (
          <TouchableOpacity onPress={onPressCloseButton}>
            <IconClose />
          </TouchableOpacity>
        )}
      </View>
    );
  } else if (type == "searchChannel") {
    const makeFocus = (temp) => {
      if (temp) {
        onFocus();
        setIsFocus(true);
      } else {
        setIsFocus(false);
        onBlur();
      }
    };
    return (
      <View style={styles.layoutSearchChannel}>
        {!_.isEmpty(inputText) ||
        textplaceholder != "Cari Forum" ||
        isFocus ? null : (
          <IconSearch />
        )}

        <TextInput
          placeholder={textplaceholder}
          placeholderTextColor="#989898"
          style={styles.textInputSearchChannel}
          onChangeText={onChangeText}
          // onFocus={onFocus}
          onFocus={() => makeFocus(true)}
          value={inputText}
          // onBlur={onBlur}
          onBlur={() => makeFocus(false)}
          ref={inputReff}
          keyboardType={
            Platform.OS === "ios" ? "ascii-capable" : "visible-password"
          }
        />
        {isCloseButton && (
          <TouchableOpacity onPress={onPressCloseButton}>
            <IconClose />
          </TouchableOpacity>
        )}
      </View>
    );
  } else if (type != "") {
    return (
      <View style={styles.viewSearchNonButton}>
        <IconSearch />
        <TextInput
          placeholder={textplaceholder}
          style={styles.textInputNonButton}
        />
      </View>
    );
  }
};

export default SearchBarField;

const styles = StyleSheet.create({
  layoutSearchButtonHome: {
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    paddingHorizontal: 8,
  },
  layoutSearchKonten: {
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
  },
  layoutSearchChannel: {
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    paddingLeft: 8,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
  },
  textCariKontenD2D: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    color: "#989898",
    marginLeft: 8,
    paddingVertical: 10,
  },
  viewSearchNonButton: {
    flexDirection: "row",
    alignItems: "center",
    width: 287,
    height: 40,
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  textInputNonButton: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    color: "#989898",
    flex: 1,
    paddingVertical: 10,
    marginHorizontal: 16,
  },
  textInputSearchKonten: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    color: "#000000",
    flex: 1,
    paddingVertical: 10,
    marginHorizontal: 16,
  },
  textInputSearchChannel: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    // lineHeight: 24,
    // letterSpacing: 0.15,
    color: "#000000",
    flex: 1,
    paddingVertical: 10,
    marginHorizontal: 8,
  },
});
