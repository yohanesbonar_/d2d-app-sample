import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { IconNavigateBefore, IconNavigateNext } from "../../../assets";

const ArrowCalendar = ({ type, previousMonthName, nextMonthName }) => {
  if (type == "left") {
    return (
      <View style={styles.wrapperPreviousMonth}>
        <IconNavigateBefore />
        <Text Subtitle2Medium style={styles.textMonth}>
          {previousMonthName}
        </Text>
      </View>
    );
  } else {
    return (
      <View style={styles.wrapperNextMonth}>
        <Text Subtitle2Medium style={styles.textMonth}>
          {nextMonthName}
        </Text>
        <IconNavigateNext />
      </View>
    );
  }
};

export default ArrowCalendar;

const styles = StyleSheet.create({
  wrapperPreviousMonth: {
    marginLeft: -12,
    flexDirection: "row",
    alignItems: "center",
  },
  wrapperNextMonth: {
    marginRight: -12,
    flexDirection: "row",
    alignItems: "center",
  },
  textMonth: {
    color: "#666666",
  },
});
