import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
} from "react-native";

const MenuImage = ({ imageSource, title, onPress, size }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View>
        <Image
          source={{ uri: imageSource || "" }}
          style={{ width: size, height: size }}
        />
        <Text
          style={{
            fontSize: 14,
            fontFamily: "Roboto-Regular",
            color: "#000000",
            marginBottom: 12,
            marginTop: 2,
          }}
        >
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default MenuImage;

const styles = StyleSheet.create({
  menu: {
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    height: 100,
    margin: 5,
    padding: 10,
    backgroundColor: "#00BCD4",
    // width: Dimensions.get("window").width / 3,
  },
});
