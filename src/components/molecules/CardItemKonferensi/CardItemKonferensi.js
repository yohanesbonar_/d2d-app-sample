import { min } from "moment";
import { Card, CardItem, Text } from "native-base";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { ThemeD2D } from "../../../../theme";
import { IconNotificationOff, IconNotificationOn } from "../../../assets";
import { getTime } from "../../../utils";

const CardItemKonferensi = ({
  type,
  isNotify,
  title,
  startDate,
  endDate,
  onPressButtonRight,
  textButton,
  isTextButtonDisable,
}) => {
  const RigthSide = () => {
    // if (type == "upcoming") {
    //   if (isNotify) {
    //     return <IconNotificationOn />;
    //   } else {
    //     return <IconNotificationOff />;
    //   }
    // }
    return (
      <TouchableOpacity
        onPress={onPressButtonRight}
        disabled={isTextButtonDisable}
      >
        <Text
          textButtonMediumBlue
          style={styles.textButton(isTextButtonDisable)}
        >
          {textButton}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.containerCard}>
      <View style={styles.contentContainer}>
        <View style={{ flex: 1 }}>
          <Text SubtitleMedium numberOfLines={2} style={styles.title}>
            {title}
          </Text>
          <Text style={styles.textTime}>{`${getTime(startDate)} - ${getTime(
            endDate
          )}`}</Text>
        </View>
        <View style={styles.buttonRightWrapper}>
          <RigthSide />
        </View>
      </View>
    </View>
  );
};

export default CardItemKonferensi;

const styles = StyleSheet.create({
  containerCard: {
    padding: 16,
    marginBottom: 20,
    marginTop: 0,
    borderRadius: 8,
    backgroundColor: "#FFFFFF",
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  contentContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  textTime: {
    marginTop: 8,
    marginRight: 16,
  },
  title: {
    marginRight: 16,
  },
  buttonRightWrapper: {
    flex: 0.4,
    justifyContent: "flex-end",
  },
  textButton: (isDisable) => ({
    textAlign: "center",
    color: isDisable ? "#989898" : "#0771CD",
  }),
});
