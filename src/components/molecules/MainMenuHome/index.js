import React from "react";
import { StyleSheet, View, TouchableOpacity, Dimensions } from "react-native";
import { Text } from "native-base";

import {
  IconElearningChannel,
  IconExhibitionChannel,
  IconForumChannel,
  IconHomeChannel,
  IconHomeCme,
  IconHomeEvent,
  IconHomeJob,
  IconHomeLiteratur,
  IconHomeObatatoz,
  IconHomePolling,
  IconHomeWebinar,
  IconJobsChannel,
  IconKonferensiChannel,
  IconKuisChannel,
  IconPollingChannel,
  IconProdukChannel,
  IconRequestJournalChannel,
} from "../../../assets";

const MainMenuHome = ({ icon, title, onPress, grid }) => {
  const Icon = () => {
    if (icon === "home-cme") {
      return <IconHomeCme />;
    }
    if (icon === "home-event") {
      return <IconHomeEvent />;
    }
    if (icon === "home-webinar") {
      return <IconHomeWebinar />;
    }
    if (icon === "home-job") {
      return <IconHomeJob />;
    }
    if (icon === "home-channel") {
      return <IconHomeChannel />;
    }
    if (icon === "home-literatur") {
      return <IconHomeLiteratur />;
    }
    if (icon === "home-obatatoz") {
      return <IconHomeObatatoz />;
    }
    if (icon === "home-polling") {
      return <IconHomePolling />;
    }

    if (icon === "channel-forum") {
      return <IconForumChannel />;
    }
    if (icon === "channel-konferensi") {
      return <IconKonferensiChannel />;
    }
    if (icon === "channel-request-journal") {
      return <IconRequestJournalChannel />;
    }
    if (icon === "channel-job") {
      return <IconJobsChannel />;
    }
    if (icon === "channel-produk") {
      return <IconProdukChannel />;
    }
    if (icon === "channel-exhibition") {
      return <IconExhibitionChannel />;
    }
    if (icon === "channel-kuis") {
      return <IconKuisChannel />;
    }
    if (icon === "channel-polling") {
      return <IconPollingChannel />;
    }
    if (icon === "channel-elearning") {
      return <IconElearningChannel />;
    }
    return <IconEditProfile />;
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.menu(grid)}>
        <Icon />

        <Text Body2>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default MainMenuHome;

const styles = StyleSheet.create({
  menu: (grid) => ({
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width:
      grid == 4
        ? Dimensions.get("window").width / 3
        : Dimensions.get("window").width / 4,
  }),
});
