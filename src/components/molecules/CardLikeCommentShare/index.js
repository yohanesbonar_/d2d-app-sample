import { Text } from "native-base";
import React, { useState, useEffect } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import {
  IconCommentForum,
  IconShareForum,
  IconThumbUpOff,
  IconThumbUpOn,
} from "../../../assets";

const CardLikeCommentShare = ({
  onPressLike,
  onPressComment,
  onPressShare,
  flagLike,
}) => {
  const [isLike, setIsLike] = useState(flagLike != null ? flagLike : false);
  const renderLikeButton = () => {
    if (isLike) {
      return (
        <View style={styles.containerLike}>
          <IconThumbUpOn style={{ marginRight: 6 }} />
          <Text style={styles.textLikeCommentShareRed}>Suka</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.containerLike}>
          <IconThumbUpOff style={{ marginRight: 6 }} />
          <Text style={styles.textLikeCommentShare}>Suka</Text>
        </View>
      );
    }
  };

  useEffect(() => {
    setIsLike(flagLike);
  }, [flagLike]);

  const onPressCard = () => {
    setIsLike(isLike ? false : true);
  };

  return (
    <View style={styles.containerLikeCommentShare}>
      {onPressLike && (
        <TouchableOpacity
          style={styles.buttonLikeCommentShare}
          onPress={onPressLike}
          onPressIn={() => onPressCard()}
        >
          {renderLikeButton()}
        </TouchableOpacity>
      )}
      {onPressComment && (
        <TouchableOpacity
          style={styles.buttonLikeCommentShare}
          onPress={onPressComment}
        >
          <IconCommentForum style={{ marginRight: 6 }} />
          <Text style={styles.textLikeCommentShare}>Komentar</Text>
        </TouchableOpacity>
      )}
      {onPressShare && (
        <TouchableOpacity
          style={styles.buttonLikeCommentShare}
          onPress={onPressShare}
        >
          <IconShareForum style={{ marginRight: 6 }} />
          <Text style={styles.textLikeCommentShare}>Bagikan</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CardLikeCommentShare;

const styles = StyleSheet.create({
  textLikeCommentShare: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  textLikeCommentShareRed: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#D01E53",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  buttonLikeCommentShare: {
    flexDirection: "row",
    alignItems: "center",
  },
  containerLikeCommentShare: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    height: 48,
  },
  containerLike: { flexDirection: "row", alignItems: "center" },
});
