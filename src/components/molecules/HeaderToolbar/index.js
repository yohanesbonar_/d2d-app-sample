import React from "react";
import { SafeAreaView, TouchableOpacity } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { Body, Header, Right, Title } from "native-base";
import { ArrowBackButton } from "../../atoms";
import { ThemeD2D } from "../../../../theme";
import _ from "lodash";
import { IconMoreVertial, IconShare } from "../../../assets";
const HeaderToolbar = ({
  onPress,
  title,
  onPressRightMenu,
  titleRightMenu,
  iconRightMenu,
}) => {
  const Icon = () => {
    if (!_.isEmpty(iconRightMenu)) {
      if (iconRightMenu == "iconMoreVertical") {
        return <IconMoreVertial />;
      } else if (iconRightMenu == "IconShare") {
        return <IconShare />;
      }
    }
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header noShadow style={styles.header}>
        <Body style={styles.bodyContainer}>
          {onPress && <ArrowBackButton onPress={onPress} />}
          <View style={{ flex: 1 }}>
            <Title style={styles.textTitle(onPress)}>{title}</Title>
          </View>
        </Body>

        {!_.isEmpty(titleRightMenu) && (
          <Right>
            <TouchableOpacity
              style={styles.buttonRightMenu}
              onPress={onPressRightMenu}
            >
              <Text style={styles.titleRightMenu}>{titleRightMenu}</Text>
            </TouchableOpacity>
          </Right>
        )}
        {!_.isEmpty(iconRightMenu) && (
          <TouchableOpacity
            style={styles.buttonRightMenuIcon}
            onPress={onPressRightMenu}
          >
            <Icon />
          </TouchableOpacity>
        )}
      </Header>
    </SafeAreaView>
  );
};

export default HeaderToolbar;

const styles = StyleSheet.create({
  header: {
    paddingRight: 0,
    paddingLeft: 0,
  },
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 4,
    marginVertical: 8,
  },
  textTitle: (onPress) => ({
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    marginLeft: typeof onPress != "undefined" ? 20 : 22,
    alignItems: "center",
    lineHeight: 24,
    letterSpacing: 0.15,
    textAlign: "left",
  }),
  titleRightMenu: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    letterSpacing: 1.25,
    lineHeight: 16,
    alignItems: "center",
    color: "#FFFFFF",
    textAlignVertical: "center",
  },
  buttonRightMenu: {
    paddingVertical: 16,
    paddingLeft: 16,
    paddingRight: 8,
  },
  buttonRightMenuIcon: {
    marginLeft: 4,
    paddingTop: 4,
    marginRight: 4,
  },
});
