import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import {
  IconBookmark,
  IconDownload,
  IconJob,
  IconKebijakanPrivasi,
  IconKontakKami,
  IconSyaratKetentuan,
  IconEditProfile,
  IconRating,
  IconJobStatus,
  IconArrowRight,
} from "../../../assets";

const ListMenuProfile = ({ icon, title, onPress, version }) => {
  const Icon = () => {
    if (icon === "icon-jobstatus") {
      return <IconJobStatus />;
    }
    if (icon === "icon-bookmark") {
      return <IconBookmark />;
    }
    if (icon === "icon-download") {
      return <IconDownload />;
    }
    if (icon === "icon-syaratdanketentuan") {
      return <IconSyaratKetentuan />;
    }
    if (icon === "icon-kebijakanprivasi") {
      return <IconKebijakanPrivasi />;
    }
    if (icon === "icon-kontakkami") {
      return <IconKontakKami />;
    }
    if (icon === "icon-berirating") {
      return <IconRating />;
    }
    return <IconEditProfile />;
  };
  return (
    <TouchableOpacity onPress={onPress} style={styles.menu}>
      <View style={styles.viewContainerMenu}>
        <Icon />
        <Text style={styles.textTitle}>{title}</Text>
      </View>

      <View style={styles.viewArrow}>
        <IconArrowRight />
        {version && <Text style={styles.textVersion}>Versi {version}</Text>}
      </View>
    </TouchableOpacity>
  );
};

export default ListMenuProfile;

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    marginBottom: 8,
    height: 56,
  },
  viewContainerMenu: {
    flexDirection: "row",
    flex: 0.5,
    alignItems: "flex-start",
  },
  textTitle: {
    fontSize: 16,
    marginLeft: 16,
    fontFamily: "Roboto-Medium",
    color: "#000000",
  },
  viewArrow: {
    flexDirection: "row-reverse",
    flex: 0.5,
    alignItems: "center",
  },
  textVersion: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    color: "#000000",
    marginRight: 16,
  },
});
