import { Card, Text } from "native-base";
import React from "react";
import { Dimensions, StyleSheet, View, TouchableOpacity } from "react-native";
import { Avatar, Button } from "../../atoms";

const CardItemChannel = ({
  name,
  onPressButton,
  onPressCard,
  isJoin,
  imageSource,
}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPressCard}>
        <View style={{ alignItems: "center" }}>
          <Avatar name={name} imageSource={imageSource} />
          <Text SubtitleMedium style={styles.name} numberOfLines={1}>
            {name}
          </Text>
        </View>
      </TouchableOpacity>

      <Button
        text={isJoin ? "MASUK" : "GABUNG"}
        size="small"
        marginHor={0}
        onPress={isJoin ? onPressCard : onPressButton}
        type={isJoin ? "secondary" : undefined}
      />
    </View>
  );
};

export default CardItemChannel;

const styles = StyleSheet.create({
  container: {
    padding: 16,
    marginTop: 0,
    marginBottom: 0,
    marginRight: 0,
    marginLeft: 0,
    borderRadius: 8,
    backgroundColor: "#FFFFFF",
    borderColor: "#0000001F",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 4,
  },
  name: {
    marginVertical: 16,
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    color: "#000000",
  },
});
