import React, { useState, useEffect, useRef } from "react";
import { View, Text, Icon, Toast } from "native-base";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Modalize } from "react-native-modalize";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import { IconCloseRoundedWhite } from "../../../assets";
import { BottomSheet } from "../../molecules";
import { subscribeChannel } from "../../../utils/network/channel/subscribeChannel";
import { getChannelDetail } from "../../../utils/network/channel/channelDetail";
import _ from "lodash";

const ChannelModalJoin = ({
  navigation,
  deeplinkChannelObj,
  isShowLoader,
  isLoadingBanner,
  isLoadingSearchContent,
  isNotificationOpenCheckCompleted,
  screen,
  // channelJoinModalRef,
  onClosedParam,
  onOpenedParam,
}) => {
  const [isModalHasOpen, setIsModalHasOpen] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const [channel, setChannel] = useState(null);
  const channelJoinModalRef = useRef();

  useEffect(() => {
    console.log("onopen channelXid", deeplinkChannelObj);
    if (
      deeplinkChannelObj != null &&
      deeplinkChannelObj.channelXid &&
      !_.isEmpty(deeplinkChannelObj.channelXid)
    ) {
      getChannelSbc(false, deeplinkChannelObj.channelXid);
    }
  }, [deeplinkChannelObj]);

  useEffect(() => {
    if (
      !isShowLoader &&
      !isLoadingBanner &&
      !isLoadingSearchContent &&
      isNotificationOpenCheckCompleted &&
      isShowModal
    ) {
      onOpen();
    }
  }, [
    isShowLoader,
    isLoadingBanner,
    isLoadingSearchContent,
    isNotificationOpenCheckCompleted,
    isShowModal,
  ]);

  useEffect(() => {
    console.log("element ismodalhasopen ------------------", isModalHasOpen);
  }, [isModalHasOpen]);

  const onOpen = () => {
    if (deeplinkChannelObj != null && deeplinkChannelObj.channelXid != null) {
      onOpenedParam();
      channelJoinModalRef.current?.open();
    }
  };

  const onClose = () => {
    channelJoinModalRef.current?.close();
  };

  //check if subscribed

  const onPressButtonJoinChannel = async () => {
    if (deeplinkChannelObj != null && deeplinkChannelObj.channelXid != null) {
      try {
        let params = {
          xid: deeplinkChannelObj.channelXid,
          referral_type:
            deeplinkChannelObj.navigateTo == "ChannelDetail"
              ? deeplinkChannelObj.paramsXid.data.referral_type != ""
                ? deeplinkChannelObj.paramsXid.data.referral_type
                : ""
              : "",
          referral_admin_id:
            deeplinkChannelObj.navigateTo == "ChannelDetail"
              ? deeplinkChannelObj.paramsXid.data.referral_admin_id != ""
                ? deeplinkChannelObj.paramsXid.data.referral_admin_id
                : ""
              : "",
        };
        console.log(" params subscribeChannel-> ", params);
        response = await subscribeChannel(params);
        console.log("join channel", response);
        if (response.header.message == "Success") {
          getChannelSbc(true, deeplinkChannelObj.channelXid);
        } else {
          Toast.show({
            text: "Something went wrong!! " + response.header.reason.id,
            position: "top",
            duration: 3000,
          });
        }
      } catch (error) {
        Toast.show({
          text: "Something went wrong!! " + error,
          position: "top",
          duration: 3000,
        });
      }
    } else {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const getChannelSbc = async (isNewSubscribe, channelXid) => {
    if (channelXid != null) {
      try {
        response = await getChannelDetail(channelXid);
        console.log(
          "onopen channel modal join --",
          response,
          response.header.message
        );
        let result = response.data;
        if (response.header.message == "Success") {
          navigateToDetail(isNewSubscribe, result.sbc, response.data);
        } else {
          setIsShowModal(false);
          if (response.data != null && !response.data.is_subscribed) {
            setChannel(response.data);
            console.log("channel modal must join --");
            setIsShowModal(true);
            // onOpen();
          } else {
            Toast.show({
              text: "Maaf, halaman yang Anda tuju tidak ditemukan.",
              buttonText: "TUTUP",
              position: "bottom",
              duration: 3000,
              buttonStyle: { marginLeft: 16 },
            });
            // navigation.popToTop();
            // setTypeBottomSheet("BottomSheetPageNotFound");
            // setDetailBottomSheet();
          }
        }
      } catch (error) {
        Toast.show({
          text: "Something went wrong!! " + error,
          position: "top",
          duration: 3000,
        });
      }
    } else {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const doNothing = () => {};

  const navigateToDetail = (isNewSubscribe, sbcChannel, dataChannel) => {
    if (
      deeplinkChannelObj != null &&
      deeplinkChannelObj.channelXid != null &&
      deeplinkChannelObj.navigateTo != null &&
      deeplinkChannelObj.paramsXid != null
    ) {
      let paramDetail = deeplinkChannelObj.paramsXid;

      if (
        deeplinkChannelObj.navigateTo == "ChannelDetail" &&
        deeplinkChannelObj.navigateTo != null
      ) {
        let tempChannelID =
          deeplinkChannelObj.navigateTo == "ChannelDetail"
            ? deeplinkChannelObj.channelXid
            : null;
        let temp = {
          xid: tempChannelID,
        };
        let paramsData = {
          ...paramDetail,
          sbcChannel: sbcChannel,
          isNewSubscribe: isNewSubscribe,
          titleChannel: dataChannel.channel_name,
          onBack: doNothing,
          onRefresh: doNothing,
          item: temp,
        };
        navigation.navigate(deeplinkChannelObj.navigateTo, paramsData);
      } else {
        let paramsData = {
          ...paramDetail,
          sbcChannel: sbcChannel,
          isNewSubscribe: isNewSubscribe,
          onBack: doNothing,
          onRefresh: doNothing,
        };
        navigation.navigate(deeplinkChannelObj.navigateTo, paramsData);
      }
    } else {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  return (
    <Modalize
      withOverlay={true}
      withHandle={false}
      adjustToContentHeight={true}
      closeOnOverlayTap={true}
      panGestureEnabled={true}
      ref={channelJoinModalRef}
      modalStyle={{
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        zIndex: 1,
      }}
      overlayStyle={{
        backgroundColor: "#000000CC",
      }}
      // onOverlayPress={onClose()}
      // onClose={onClose()}
      HeaderComponent={
        <View
          style={{
            height: 48,
            width: 48,
            position: "absolute",
            right: 16,
            top: -64,
          }}
        >
          {Platform.OS === "android" ? (
            <TouchableNativeFeedback
              onPress={() => onClose()}
              style={{ borderRadius: 48 }}
            >
              <IconCloseRoundedWhite />
            </TouchableNativeFeedback>
          ) : (
            <TouchableOpacity
              onPress={() => onClose()}
              style={{ borderRadius: 48 }}
            >
              <IconCloseRoundedWhite />
            </TouchableOpacity>
          )}
        </View>
      }
      onOpened={() => {
        setIsModalHasOpen(true);
      }}
      onClosed={() => {
        console.log("onopen onClosed --");
        onClosedParam();
      }}
    >
      <BottomSheet
        onPressClose={() => {
          onPressButtonJoinChannel();
          onClose();
        }}
        title={channel ? channel.title : ""}
        desc={channel ? channel.description : ""}
        wordingClose="GABUNG KE FORUM INI"
        type="bottomSheetJobSubscribeChannel"
        imageSource={channel ? channel.logo : ""}
        screen={screen}
      />
    </Modalize>
  );
};

export default ChannelModalJoin;

const styles = StyleSheet.create({});
