import React from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";

const PhotoProfileHome = ({ imageURL, onPress }) => {
  if (imageURL == "") {
    return (
      <TouchableOpacity style={styles.imagePhoto} onPress={onPress}>
        <Image
          source={require("../../../assets/images/photo-profile-avatar.png")}
          style={{ height: 85, width: 85 }}
        />
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity style={styles.imagePhoto} onPress={onPress}>
        <Image source={{ uri: imageURL }} style={styles.imagePhoto} />
      </TouchableOpacity>
    );
  }
};

export default PhotoProfileHome;

const styles = StyleSheet.create({
  imagePhoto: {
    height: 85,
    width: 85,
    borderRadius: 85 / 2,
  },
});
