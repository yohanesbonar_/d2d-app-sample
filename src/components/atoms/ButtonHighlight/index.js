import React from "react";
import { StyleSheet, Text, TouchableHighlight } from "react-native";

const ButtonHighlight = ({
  text,
  onPress,
  textColor,
  buttonColor,
  buttonUnderlayColor,
  additionalContainerStyle,
  additionalTextStyle,
  accessibilityLabel,
  disabled
}) => {
  return (
    <TouchableHighlight
      style={styles.containerStyle(buttonColor, additionalContainerStyle)}
      onPress={onPress}
      underlayColor={buttonUnderlayColor ? buttonUnderlayColor : "#0360BB"}
      accessibilityLabel={
        accessibilityLabel ? accessibilityLabel : "ButtonHightlight"
      }
      disabled={disabled}
    >
      <Text style={styles.textStyle(textColor, additionalTextStyle)}>
        {text}
      </Text>
    </TouchableHighlight>
  );
};

export default ButtonHighlight;

const styles = StyleSheet.create({
  containerStyle: (buttonColor, additionalContainerStyle) => ({
    flex: 1,
    height: 48,
    elevation: 2,
    justifyContent: "center",
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: buttonColor ? buttonColor : "#0771CD",
    ...additionalContainerStyle,
  }),
  textStyle: (textColor, additionalTextStyle) => ({
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: textColor ? textColor : "#FFFFFF",
    letterSpacing: 0.9,
    lineHeight: 16,
    textAlign: "center",
    ...additionalTextStyle,
  }),
});
