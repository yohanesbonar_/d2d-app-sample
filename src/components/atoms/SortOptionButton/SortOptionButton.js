import { Text } from "native-base";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";

const SortOptionButton = ({ choosed, text, onPress }) => {
  if (choosed) {
    return (
      <View style={{}}>
        <TouchableOpacity style={styles.touchAbleEnable} onPress={onPress}>
          <Text textButtonRegulerWhite>{text}</Text>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View style={{}}>
        <TouchableOpacity style={styles.touchAble} onPress={onPress}>
          <Text textButtonRegulerBlack>{text}</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

export default SortOptionButton;

const styles = StyleSheet.create({
  touchAbleEnable: {
    paddingHorizontal: 16,
    paddingVertical: 9,
    backgroundColor: "#D01E53",
    borderRadius: 4,
    marginRight: 20,
  },
  touchAble: {
    paddingHorizontal: 16,
    paddingVertical: 9,
    backgroundColor: "#EBEBEB",
    borderRadius: 4,
    marginRight: 20,
  },
});
