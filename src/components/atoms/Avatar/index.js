import React, { useEffect, useState } from "react";
import { StyleSheet, View, Image } from "react-native";
import _ from "lodash";
import { Text } from "native-base";
import { getInitialName, isValidateNpaIDI } from "../../../utils";
const Avatar = ({
  name,
  imageSource,
  fullWidth,
  imageSize,
  isShapeNotRounded,
  noBackground,
}) => {
  const [imgsrc, setImgsrc] = useState(imageSource);
  const [isNotRounded, setIsNotRounded] = useState(isShapeNotRounded);
  const [isWithoutBackground, setIsWithoutBackground] = useState(noBackground);
  const [isFullWidth, setIsFullWidth] = useState(fullWidth);

  const propsStyle = {
    imageSize: imageSize,
    isNotRounded: isNotRounded,
    isWithoutBackground: isWithoutBackground,
    isFullWidth: isFullWidth,
  };

  useEffect(() => {
    if (_.isEmpty(imageSource)) {
      onImageError();
    } else {
      setImgsrc(imageSource);
    }
  }, [imageSource]);

  const onImageError = () => {
    setImgsrc("");
    setIsNotRounded(false);
    setIsWithoutBackground(false);
    setIsFullWidth(false);
  };

  return (
    <View style={styles(propsStyle).container}>
      {!_.isEmpty(imgsrc) && (
        <Image
          source={{ uri: imgsrc }}
          style={styles(propsStyle).image}
          resizeMode="contain"
          accessibilityLabel={"image_avatar"}
          onError={(e) => onImageError()}
        />
      )}

      {_.isEmpty(imgsrc) && (
        <View style={styles(propsStyle).container}>
          <Text H6 accessibilityLabel={"initial_name"}>
            {getInitialName(name)}
          </Text>
        </View>
      )}
    </View>
  );
};

export default Avatar;

const styles = (propsStyle) =>
  StyleSheet.create({
    container: {
      width: propsStyle.isFullWidth
        ? "100%"
        : propsStyle.imageSize
        ? propsStyle.imageSize
        : 56,
      height: propsStyle.imageSize ? propsStyle.imageSize : 56,
      borderRadius: propsStyle.isNotRounded
        ? 0
        : propsStyle.imageSize
        ? propsStyle.imageSize / 2
        : 56 / 2,
      backgroundColor: propsStyle.isWithoutBackground
        ? "transparent"
        : "#EBEBEB",
      justifyContent: "center",
      alignItems: "center",
    },
    image: {
      width: propsStyle.isFullWidth
        ? "100%"
        : propsStyle.imageSize
        ? propsStyle.imageSize
        : 56,
      height: propsStyle.imageSize ? propsStyle.imageSize : 56,
      borderRadius: propsStyle.isNotRounded
        ? 0
        : propsStyle.imageSize
        ? propsStyle.imageSize / 2
        : 56 / 2,
    },
  });
