import { size } from "lodash";
import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { ThemeD2D } from "../../../../theme";

const Button = ({
  text,
  onPress,
  type,
  size,
  marginRig,
  marginLef,
  isSelected,
}) => {
  if (type == "buttonSelections") {
    return (
      <TouchableOpacity
        style={styles.buttonSelection(size, isSelected)}
        onPress={onPress}
      >
        <Text style={styles.textSelection(isSelected)}>{text}</Text>
      </TouchableOpacity>
    );
  }

  return (
    <TouchableOpacity
      style={styles.container(type, size, marginRig, marginLef)}
      onPress={onPress}
    >
      <Text style={styles.text(type)}>{text}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: (type, size, marginRig, marginLef) => ({
    backgroundColor:
      type == "secondary"
        ? "#EBEBEB"
        : type == "elevationButtonGray"
        ? "#D7D7D7"
        : "#0771CD",
    height: size == "small" ? 36 : 48,
    borderRadius: 4,
    justifyContent: "center",
    flex:
      type == "elevationButton" ||
      type == "elevationButtonBlue" ||
      type == "elevationButtonGray"
        ? null
        : 1,
  }),
  text: (type) => ({
    color:
      type == "secondary"
        ? "#000000"
        : type == "elevationButtonGray"
        ? "#989898"
        : "#FFFFFF",
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    lineHeight: 16,
    letterSpacing: 0.9,
  }),

  buttonSelection: (size, isSelected) => ({
    backgroundColor: isSelected ? "#0771CD" : "#EBEBEB",
    height: size == "small" ? 36 : 48,
    borderRadius: 4,
    justifyContent: "center",
    flex: 1,
  }),
  textSelection: (isSelected) => ({
    color: isSelected ? "#FFFFFF" : "#000000",
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Roboto-Medium",
  }),
});
