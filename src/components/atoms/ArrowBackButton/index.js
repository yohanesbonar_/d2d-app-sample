import React from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { IconBack } from "../../../assets";

const ArrowBackButton = ({ onPress }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        height: 48,
        width: 48,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <IconBack />
    </TouchableOpacity>
  );
};

export default ArrowBackButton;

const styles = StyleSheet.create({
  layoutPhoto: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
