import { Text } from "native-base";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { ThemeD2D } from "../../../../theme";
import {
  IconAlbumP2KBOff,
  IconHomeLogo,
  IconFeedOn,
  IconFeedOff,
  IconAlbumP2KBOn,
  IconHomeLogoOff,
} from "../../../assets";
//import {colors, fonts} from '../../../utils';

const TabItem = ({ title, active, onPress, onLongPress }) => {
  const Icon = () => {
    console.log(title);
    if (title === "Home") {
      return active ? <IconHomeLogo /> : <IconHomeLogoOff />;
    } else if (title === "Feed") {
      return active ? <IconFeedOn /> : <IconFeedOff />;
    } else if (title === "Album P2KB") {
      return active ? <IconAlbumP2KBOn /> : <IconAlbumP2KBOff />;
    }
    return <IconHomeLogo />;
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={onPress}
        onLongPress={onLongPress}
      >
        <Icon />
        <Text caption style={styles.text(active)}>
          {title}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  menu: {
    justifyContent: "center",
    alignItems: "center",
  },
  text: (active) => ({
    marginTop: 6,
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: active
      ? ThemeD2D.menuBottomNavActive
      : ThemeD2D.menuBottomNavInactive,
  }),
});
