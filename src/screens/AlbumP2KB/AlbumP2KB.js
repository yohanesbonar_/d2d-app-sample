import { Body, Container, Header, Title } from "native-base";
import React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import { ThemeD2D } from "../../../theme";

const AlbumP2KB = () => {
  return (
    <Container>
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow>
          <Body>
            <Title style={{ fontSize: 30 }}>AlbumP2KB</Title>
          </Body>
        </Header>
      </SafeAreaView>
    </Container>
  );
};

export default AlbumP2KB;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
});
