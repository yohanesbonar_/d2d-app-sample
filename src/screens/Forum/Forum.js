import { Container, Content, View, Text, Toast, Icon } from "native-base";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  BackHandler,
  FlatList,
  RefreshControl,
} from "react-native";
import _, { result } from "lodash";
import { Loader } from "../../../app/components";
import {
  CardItemForum,
  EmptyDataKonferensi,
  HeaderToolbar,
} from "../../components";
import { getAllForum, getData } from "../../utils";
import moment from "moment";
import { likeForum } from "../../utils/network/forum/likeForum";
import { share } from "../../../app/libs/Common";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const Forum = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [ListDataForum, setListDataForum] = useState([]);
  const [serverDate, setServerDate] = useState("");
  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isRefreshFromDetail, setIsRefreshFromDetail] = useState(false);
  const [isEmptyData, setIsEmptyData] = useState(false);
  const [pageForum, setPageForum] = useState(1);

  useEffect(() => {
    initData();
    AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Start);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const initData = () => {
    let temp = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    setServerDate(temp);
    getDataForum();
  };

  const getDataForum = async () => {
    setIsRefresh(false);
    setIsFailed(false);
    setIsFetching(true);

    let response = {};
    let result = [];
    let paramss = {
      paginate: pageForum,
      limit: 10,
    };
    console.log("PAGE  -> ", pageForum);
    try {
      response = await getAllForum(paramss, params.sbcChannel);
      console.log("response FORUM", response);
      result = response.data.data;
      if (response.header.message == "Success") {
        if (pageForum == 1 && _.isEmpty(result)) {
          setIsDataNotFound(true);
          console.log("notfound");
        }
        setIsEmptyData(_.isEmpty(result) ? true : false);
        setListDataForum(
          pageForum == 1 ? [...result] : [...ListDataForum, ...result]
        );
        setIsFailed(false);
        setIsFetching(false);
        setPageForum(pageForum + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const onRefreshdata = () => {
    setListDataForum([]);
    setPageForum(1);
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      getDataForum();
    }
  }, [isRefresh]);

  const handleLoadMore = () => {
    if (isEmptyData == true || isFetching == true) {
      return;
    }

    getDataForum();
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailed == true && pageForum > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetching} transparent />;
  };

  const onPressCardItemForum = (data, temp) => {
    let from = "";
    if (temp) {
      from = "CommentForumButton";
    } else {
      from = "";
    }
    navigation.navigate("ForumDetail", {
      data: data,
      sbcChannel: params.sbcChannel,
      onRefresh: onRefreshdata,
      from: from,
    });
  };

  const onPressShareButton = (item) => {
    share(item.share.link != "" ? item.share.link : null);
  };

  const renderData = ({ item }) => {
    let image = "";
    {
      if (!_.isEmpty(item.attachments)) {
        item.attachments.map((bb, i) => {
          if (i == 0) {
            image = bb.attachment_url;
          }
        });
      }
    }

    return (
      <View>
        <CardItemForum
          onPressCard={() => {
            onPressCardItemForum(item, false);
            AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Open);
          }}
          onPressComment={() => {
            onPressCardItemForum(item, true);
            AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar);
          }}
          onPressTotalComments={() => {
            onPressCardItemForum(item, false);
            AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar);
          }}
          onPressShare={() => onPressShareButton(item)}
          title={item.title}
          roles="Admin"
          description={item.description}
          current_date={item.created_at}
          server_date={serverDate}
          image_user={null}
          thumbnail_image={image != "" ? image : null}
          totalComments={item.total_comments}
          totalLikes={item.total_likes}
          flagLike={item.liked}
          xid={item.xid}
          sbcChannel={params.sbcChannel}
          short_description={item.short_description}
          isCommentEnable={item.is_comment_enable ? true : false}
        />
      </View>
    );
  };

  const _renderEmptyItemForum = () => {
    if (isEmptyData == true && isFetching == false) {
      return (
        <View style={{ marginHorizontal: 40 }}>
          <EmptyDataKonferensi
            title="Belum Ada Pembahasan"
            subtitle="Selalu ada pembahasan yang menarik disini. Tetap selalu update dan coba datang kembali nanti."
          />
        </View>
      );
    }

    return null;
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <HeaderToolbar onPress={handleBackButton} title="Diskusi" />

      <View style={styles.contentContainer}>
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={
            isEmptyData && !isFetching && isDataNotFound
              ? styles.centerEmptyList
              : undefined
          }
          data={ListDataForum}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderData}
          onEndReachedThreshold={0.5}
          onEndReached={handleLoadMore}
          ListFooterComponent={_renderItemFooter()}
          ListEmptyComponent={_renderEmptyItemForum()}
          onRefresh={() => {
            onRefreshdata();
          }}
          refreshing={false}
        />
      </View>
    </Container>
  );
};

export default Forum;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  centerEmptyList: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
});
