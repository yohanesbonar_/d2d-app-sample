import React from 'react';
import {useEffect} from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import {ImgBackgroundSplash, ImgLogoSplash} from '../../assets';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 3000);
  }, [navigation]);

  return (
    <ImageBackground source={ImgBackgroundSplash} style={styles.page}>
      <View style={styles.container}>
        <Image
          source={ImgLogoSplash}
          style={styles.imageLogoSplash}
          resizeMode="contain"
        />
        <Text style={styles.titleAppSplash}>Doctor to Doctor</Text>
      </View>
    </ImageBackground>
  );
};

export default Splash;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageLogoSplash: {
    width: 72,
    height: 72,
  },
  titleAppSplash: {
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
    textAlign: 'center',
    color: '#fff',
    padding: 20,
  },
});
