import {
  Body,
  Container,
  Content,
  Header,
  Toast,
  Right,
  Title,
} from "native-base";
import React, { useRef } from "react";
import { useEffect } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  Text,
  SafeAreaView,
  View,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack, IconShare } from "../../assets";
import { Button } from "../../components/atoms";
import {
  BottomSheet,
  HeaderToolbar,
  HomeProfile,
} from "../../components/molecules";
import ListMenuProfile from "../../components/molecules/ListMenuProfile";
import {
  ParamSetting,
  getSetting,
  deleteFIrebaseToken,
} from "../../../app/libs/NetworkUtility";

import firebase from "react-native-firebase";
const gueloginAuth = firebase.app("guelogin");
import _ from "lodash";
import platform from "../../../theme/variables/d2dColor";
import {
  openOtherApp,
  subscribeFcm,
  checkTopicsBySubscription,
  STORAGE_TABLE_NAME,
  unsubscribeTopicWebinarSubmit,
} from "../../../app/libs/Common";

import DeviceInfo from "react-native-device-info";
import { useState } from "react";
import {
  getData,
  KEY_ASYNC_STORAGE,
  removeData,
  storeData,
} from "../../utils/localStorage";
import { Loader } from "../../../app/components";
import { Modalize } from "react-native-modalize";
import { NavigationActions, StackActions } from "react-navigation";
import {
  unsubscribeTopicEventAttendee,
  unsubscribeTopicWebinarAttendee,
} from "../../utils";
import { ScrollView } from "react-native-gesture-handler";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const ProfileNew = ({ navigation }) => {
  const params = navigation.state.params;
  console.log("profileNew params: ", params);
  console.log("profileNew navigation: ", navigation);
  //console.log("profileNew : ", navigation.dangerouslyGetParent().routes.length);
  const initialProfileSummary = {
    name: "",
    specialist: "",
    photo: "",
    practiceLocation: "",
    typePracticeLocation: "",
    skp_point: 0,
  };
  const { profileSummary } = params;
  const [isShowLoader, setIsShowLoader] = useState(false);

  const [dataProfileSummary, setDataProfileSummary] = useState(profileSummary);

  useEffect(() => {
    setupDataProfile();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const setupDataProfile = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((res) => {
      let profileSummary = {};
      profileSummary.skp_point = res.skp_point;
      profileSummary.name = res.name;
      profileSummary.specialist = setDataSpecialist(res);

      profileSummary.typePracticeLocation = _.isEmpty(res.clinic_type_1)
        ? ""
        : res.clinic_type_1 + " ";

      profileSummary.practiceLocation = res.clinic_location_1.trim();
      profileSummary.photo = res.photo;
      profileSummary.nonspesialization = _.isEmpty(res.nonspesialization)
        ? "Dokter Umum"
        : res.nonspesialization[0].description;
      setDataProfileSummary(profileSummary);
    });
  };

  const setDataSpecialist = (result) => {
    let specialist = [];
    result.spesialization.map((value, i) => specialist.push(value.description));
    return specialist.join(", ");
  };

  const handleBackButton = () => {
    if (params.from == "CmeQuiz" || params.from == "pushNotifCertificate") {
      resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "HomeNavigation" })],
      });
      navigation.dispatch(this.resetAction);
      navigation.navigate("Cme");
    } else {
      navigation.state.params.onRefreshDataProfile();
      navigation.goBack();
    }
    return true;
  };

  const unsubscribeTopicAttendee = async () => {
    await unsubscribeTopicWebinarAttendee();
    await unsubscribeTopicEventAttendee();

    await removeData(KEY_ASYNC_STORAGE.FCM_TOPIC_EVENT_ATTENDEE);
    await removeData(KEY_ASYNC_STORAGE.FCM_TOPIC_WEBINAR_ATTENDEE);
  };

  const logout = async () => {
    const user = gueloginAuth.auth().currentUser;
    // await AsyncStorage.removeItem('totalShowPopup');
    // await AsyncStorage.removeItem('showPopup');
    let topicFCM = await subscribeFcm();

    if (user) {
      // this.removeToken();
      setIsShowLoader(true);
      try {
        let response = await deleteFIrebaseToken();
        if (
          response.isSuccess == true ||
          response.message == "No available device token to be removed"
        ) {
          await storeData(KEY_ASYNC_STORAGE.FCM_TOKEN, "");
          await storeData(KEY_ASYNC_STORAGE.IS_UPDATE_PROFILE, "");
          await removeData(KEY_ASYNC_STORAGE.FILTER_WEBINAR);
          await storeData(KEY_ASYNC_STORAGE.FCM_TOPICS_SUBSCRIBTIONS, "");
          await checkTopicsBySubscription(null);

          // const resetAction = NavigationActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login' })] });
          // const resetAction = NavigationActions.reset({ index: 0, actions: [NavigationActions.navigate({routeName: 'Splash'})] });

          gueloginAuth
            .auth()
            .signOut()
            .then(() => {
              let setLogin = async () =>
                await storeData(KEY_ASYNC_STORAGE.IS_LOGIN, "N");
              let uid = async () =>
                await storeData(KEY_ASYNC_STORAGE.UID, null);

              let profile = async () =>
                await storeData(KEY_ASYNC_STORAGE.PROFILE, null);
              let auth = async () =>
                await storeData(KEY_ASYNC_STORAGE.AUTHORIZATION, null);

              let fcm = async () =>
                await firebase.messaging().unsubscribeFromTopic(topicFCM);

              let unsubscribeFromTopicWebinarSubmit = async () =>
                await unsubscribeTopicWebinarSubmit();

              unsubscribeFromTopicWebinarSubmit();

              let unsubscribeAttendee = async () =>
                await unsubscribeTopicAttendee();

              unsubscribeAttendee();

              let removeTopicWebinarSubmit = async () =>
                await removeData(KEY_ASYNC_STORAGE.FCM_TOPICS_WEBINAR_SUBMIT);

              removeTopicWebinarSubmit();

              setLogin(); //should be called, if not async storage not updated

              fcm();

              let removeNotif = async () =>
                await firebase.notifications.removeAllDeliveredNotifications;
              removeNotif();

              setTimeout(() => {
                setIsShowLoader(false);
                let resetAction = StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({ routeName: "ChooseCountry" }),
                  ],
                });
                navigation.dispatch(resetAction);
                //prevent bugs: blink open reg hello before to login
                //this.props.navigation.replace('Login');
              }, 300);
            })
            .catch((error) => {
              console.log(error);
              setIsShowLoader(false);
              Toast.show({ text: error, position: "top", duration: 3000 });
            });
        } else {
          Toast.show({
            text: response.message,
            position: "top",
            duration: 3000,
          });
          setIsShowLoader(false);
        }
      } catch (error) {
        console.log(error);
        setIsShowLoader(false);
      }
    }
  };
  const renderList = () => {
    let webViewSK = {
      type: "Navigate",
      routeName: "GeneralWebview",
      params: ParamSetting.TERMS_CONDITIONS,
    };
    let webViewKP = {
      type: "Navigate",
      routeName: "GeneralWebview",
      params: ParamSetting.PRIVACY_POLICY,
    };
    let webViewKK = {
      type: "Navigate",
      routeName: "GeneralWebview",
      params: ParamSetting.CONTACT_US,
    };
    return (
      <View style={styles.listContainer}>
        {/* temporary hide */}
        {/* <ListMenuProfile title="Job Status" icon="icon-jobstatus" /> */}
        <ListMenuProfile
          title="Bookmark"
          icon="icon-bookmark"
          onPress={() =>
            navigation.navigate("ProfileBookmark") +
            AdjustTracker(AdjustTrackerConfig.Profile_Bookmark)
          }
        />
        <ListMenuProfile
          title="Download"
          icon="icon-download"
          onPress={() =>
            navigation.navigate("ProfileDownload") +
            AdjustTracker(AdjustTrackerConfig.Profile_Download)
          }
        />
        <ListMenuProfile
          title="Syarat & Ketentuan"
          icon="icon-syaratdanketentuan"
          onPress={() =>
            navigation.navigate(webViewSK) +
            AdjustTracker(AdjustTrackerConfig.Profile_Syarat_Ketentuan)
          }
        />
        <ListMenuProfile
          title="Kebijakan Privasi"
          icon="icon-kebijakanprivasi"
          onPress={() =>
            navigation.navigate(webViewKP) +
            AdjustTracker(AdjustTrackerConfig.Profile_Kebijakan_Privasi)
          }
        />
        <ListMenuProfile
          title="Kontak Kami"
          icon="icon-kontakkami"
          onPress={() =>
            navigation.navigate("ContactUs") +
            AdjustTracker(AdjustTrackerConfig.Profile_Kontak_Kami)
          }
        />
        <ListMenuProfile
          title="Beri Rating & Review"
          icon="icon-berirating"
          version="2.1.3"
          onPress={() => openStore()}
          version={DeviceInfo.getVersion()}
        />
      </View>
    );
  };

  const openStore = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Beri_Rating_Review);

    let url =
      platform.platform == "ios"
        ? "https://apps.apple.com/in/app/apple-store/id1411171451"
        : "https://play.google.com/store/apps/details?id=com.d2d.android";
    openOtherApp(url, {});
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        <BottomSheet
          onPressCancel={() => onClose()}
          onPressOk={() => {
            logout();
            onClose();
          }}
          title="Ingin Keluar?"
          desc="Sampai jumpa dan semoga datang kembali."
          wordingCancel="BATAL"
          wordingOk="KELUAR"
          type="bottomSheetLogout"
        />
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const onPressLogout = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Keluar);
    onOpen();
  };

  const onPressSeeProfile = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya);
    navigation.navigate("Biodata", {
      ...dataProfileSummary,
      onRefreshDataProfile: setupDataProfile,
    });
  };
  return (
    <Container>
      {renderBottomSheet()}
      <Loader visible={isShowLoader} />

      <HeaderToolbar onPress={handleBackButton} title="Profil" />

      <SafeAreaView style={{ flex: 1, backgroundColor: "transparent" }}>
        <View style={{ flex: 1 }}>
          <Content showsVerticalScrollIndicator={false}>
            <HomeProfile
              onPress={() => onPressSeeProfile()}
              textButton="LIHAT PROFIL SAYA"
              name={dataProfileSummary.name}
              practiceLocation={`${dataProfileSummary.typePracticeLocation} ${
                dataProfileSummary.practiceLocation
              }`}
              specialist={
                !_.isEmpty(dataProfileSummary.specialist)
                  ? dataProfileSummary.specialist
                  : dataProfileSummary.nonspesialization
              }
              pointSKP={dataProfileSummary.skp_point}
              photo={dataProfileSummary.photo}
              maxLinePracticeLocation={2}
              maxLineSpecialist={2}
            />
            {renderList()}
          </Content>
          <View style={styles.buttonWrapper}>
            <Button text="KELUAR" type="secondary" onPress={onPressLogout} />
          </View>
        </View>
      </SafeAreaView>
    </Container>
  );
};

export default ProfileNew;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    marginLeft: 32,
  },
  listContainer: {
    marginHorizontal: 16,
    flexDirection: "column",
    marginTop: 5,
    marginBottom: 80, //height button + margin bottom button + margin bottom menu profile
  },
  bodyContainer: { flexDirection: "row", alignItems: "center" },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },

  buttonWrapper: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    marginHorizontal: 16,
    paddingBottom: 16,
    backgroundColor: "#FFFFFF",
  },
});
