import { Container, Text, Button, Toast } from "native-base";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  BackHandler,
  Dimensions,
  PixelRatio,
  NativeModules,
} from "react-native";
import _ from "lodash";
import YoutubePlayer from "react-native-youtube-iframe";
const { WebinarModule } = NativeModules;

const Logger = NativeModules.Logger;
const PictureInPictureManager = NativeModules.PictureInPictureManager;

const WebinarDetail = ({ navigation }) => {
  const params = navigation.state.params;
  const [videoId, setVideoId] = useState("_dPlkFPowCc");
  const [isRefresh, setRefresh] = useState(false);
  const [data, setData] = useState(null);
  const [isPlaying, setPlaying] = useState(false);
  const [callbacksParam, setCallbacksParam] = useState("-");
  useEffect(() => {
    getData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const callBacks = (param) => {
    console.log("params callback", param);
    Toast.show({
      text: "callback : " + param,
      position: "top",
      duration: 3000,
    });
    setCallbacksParam(param);
  };

  const getData = async () => {};

  const widthVideo = Dimensions.get("window").width;
  const heightVideo = PixelRatio.roundToNearestPixel(widthVideo / (16 / 9));

  return (
    <Container>
      <YoutubePlayer
        height={heightVideo}
        width={widthVideo}
        videoId={videoId}
        isPlaying={isPlaying}
        onReady={() => console.log("video ready")}
        onError={(e) => console.log(e)}
        onPlaybackQualityChange={(q) => console.log(q)}
        playbackRate={1}
      />
      <Text>test floating webinar</Text>
      <Button
        onPress={() => {
          console.log("-----", WebinarModule);
          WebinarModule != null
            ? WebinarModule.minimizeWebinar(
                videoId,
                "Chicken Head Tracking - Smarter Every Day",
                57,
                "https://d2d.co.id/?type=webinar&title=webinar-menjaga-fluida-dalam-tubuh-agar-terhindar-dari-batu-guinjal&hash=MTU3NQ__",
                (param) => {
                  Toast.show({
                    text: "callback : " + param,
                    position: "top",
                    duration: 3000,
                  });
                  console.log("------callback", param);
                }
              )
            : {};
        }}
      >
        <Text>trigger native android</Text>
      </Button>
      <Button
        onPress={() => {
          let that = this;
          console.log("-----", WebinarModule);
          WebinarModule != null
            ? WebinarModule.stopAndBackToAppWebinar((type, time, videoId) => {
                Toast.show({
                  text:
                    "callback : " + type + " --- " + time + " --- " + videoId,
                  position: "top",
                  duration: 3000,
                });
                console.log("------callback", time);
                that.setState({ seekTo: time });
              })
            : {};
        }}
      >
        <Text>trigger native android - Stop Webinar</Text>
      </Button>
      <Button
        onPress={() => {
          console.log("check native iOS ");
          Logger != null ? Logger.log("testName", videoId) : null;
          PictureInPictureManager != null
            ? PictureInPictureManager.addEvent("testName", videoId)
            : null;
          console.log("NativeModules -> ", NativeModules);
        }}
      >
        <Text>trigger native IOS</Text>
      </Button>
    </Container>
  );
};

export default WebinarDetail;

const styles = StyleSheet.create({});
