import { Container, Content } from "native-base";
import React from "react";
import { useEffect } from "react";
import { BackHandler, Linking, StyleSheet, Text, View } from "react-native";
import { CardContact, CardItemChannel, HeaderToolbar } from "../../components";
import { sendByEmailApp } from "../../utils";

const ContactUs = ({ navigation }) => {
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const onPressContactEmail = () => {
    sendByEmailApp((type = "contactUs"));
  };

  const onPressContactInstagram = () => {
    Linking.openURL(
      `https://www.instagram.com/d2d.id/?igshid=82xkrnol33kt`
    );
  };

  return (
    <Container>
      <HeaderToolbar onPress={handleBackButton} title="Kontak Kami" />
      <Content contentContainerStyle={styles.contentContainer}>
        <CardContact contact="Info@d2d.co.id" onPress={onPressContactEmail} />

        <CardContact icon="instagram" contact="@d2d.id" onPress={onPressContactInstagram} />
      </Content>
    </Container>
  );
};

export default ContactUs;

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  cardContactWrapper: {
    marginTop: 20,
  }
});
