import { Container, Content } from "native-base";
import React from "react";
import { useEffect } from "react";
import {
  BackHandler,
  StyleSheet,
  Text,
  View,
  PermissionsAndroid,
} from "react-native";
import { HeaderToolbar } from "../../components";
import Webview from "react-native-webview";
import {
  check,
  request,
  PERMISSIONS,
  RESULTS,
  checkMultiple,
  requestMultiple,
} from "react-native-permissions";
import { ThemeD2D } from "../../../theme";
import { useState } from "react";
import Permissions from "react-native-permissions";

const ConferenceRoom = ({ navigation }) => {
  const { params } = navigation.state;
  console.log(params);

  useEffect(() => {
    setTimeout(() => {
      checkPermission();
    }, 1000);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const checkPermission = async () => {
    Permissions.checkMultiple(["camera", "microphone"]).then((response) => {
      //response is an object mapping type to permission
      if (
        response.camera != "authorized" ||
        response.microphone != "authorized"
      ) {
        requestPermission();
      }
    });
  };

  const requestPermission = async () => {
    console.log("requestPermission");

    Permissions.request("camera").then((response) => {
      // Returns once the user has chosen to 'allow' or to 'not allow' access
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      Permissions.request("microphone").then((response) => {
        webviewRef.current.reload();
      });
      webviewRef.current.reload();
    });
  };

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const webviewRef = React.createRef(null);

  return (
    <Container>
      <HeaderToolbar onPress={handleBackButton} title="Ruang Konferensi" />

      <Webview
        userAgent={
          ThemeD2D.platform == "android"
            ? "Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3714.0 Mobile Safari/537.36"
            : undefined
        }
        originWhitelist={["*"]}
        allowsInlineMediaPlayback
        source={{
          uri: params.dmeet, //URL of your redirect site
        }}
        startInLoadingState
        javaScriptEnabled={true}
        mediaPlaybackRequiresUserAction={false}
        ref={webviewRef}
      />
    </Container>
  );
};

export default ConferenceRoom;

const styles = StyleSheet.create({});
