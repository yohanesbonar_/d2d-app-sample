import _ from "lodash";
import { Container, Content, Text, Toast } from "native-base";
import React, { useEffect, useState } from "react";
import {
  BackHandler,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  _ScrollView,
} from "react-native";
import ImagePicker from "react-native-image-picker";
import { NavigationActions, StackActions } from "react-navigation";
import { Loader } from "../../../app/components";
import { AdjustTracker, AdjustTrackerConfig } from "../../../app/libs/AdjustTracker";
import {
  Button,
  HeaderToolbar,
  InputField,
  PhotoProfileHome,
} from "../../components";
import {
  getData,
  isValidateNpaIDI,
  isValidNameByRegex,
  KEY_ASYNC_STORAGE,
  removeData,
  removeEmojis,
  storeData,
  submitQuiz,
  testID,
  updatePhotoProfile,
} from "../../utils";

const ChangeProfile = ({ navigation }) => {
  const params = navigation.state.params;
  const [fullname, setFullname] = useState(params.name);
  const [medicalID, setMedicalID] = useState(_.isEmpty(params.npa_idi) ? '' : params.npa_idi);
  const [uriPhoto, setUriPhoto] = useState(params.photo);
  const [photo, setPhoto] = useState(params.photo);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isErrorField, setIsErrorField] = useState(false);
  const [messageErrorField, setMessageErrorField] = useState("");

  const [isErrorFieldMedicalID, setIsErrorFieldMedicalID] = useState(false);
  const [messageErrorFieldMedicalID, setMessageErrorFieldMedicalID] = useState(
    ""
  );
  console.log(params);

  useEffect(() => {
    AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Start)
    if (
      params.from == "Cme" ||
      params.from == "CmeQuiz" ||
      params.from == "notification" ||
      params.from == "pushNotifCertificate" ||
      params.from == "SearchKonten" ||
      params.from == "home"
    ) {
      setupDataProfile();
    }

    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  gotoCme = async () => {
    resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "HomeNavigation" })],
    });
    await navigation.dispatch(resetAction);
    navigation.navigate("Cme");
  };

  const handleBackButton = () => {
    if (params.from == "CmeQuiz" || params.from == "pushNotifCertificate") {
      gotoCme();
    } else if (params.from == "SearchKonten") {
      if (params.isFromAlertSearchPage == true) {
        navigation.goBack();
      }
      else {
        if (params.is_done == 1) {
          navigation.pop(2);
        } else {
          navigation.pop(3);
        }
        navigation.state.params.onRefreshDataSearchKonten();
      }
    } else if (params.from == "biodata") {
      backToBiodata()
    }

    else {
      navigation.goBack();
      if (params.from != "notification" && params.from != "Cme") {
        navigation.state.params.onRefreshDataProfile();
      }
    }
    return true;
  };

  const backToBiodata = () => {
    navigation.goBack();
    navigation.state.params.onRefreshDataProfile();
  }
  const setupDataProfile = () => {
    console.log("setupDataProfile")
    getData(KEY_ASYNC_STORAGE.PROFILE).then((res) => {
      setFullname(res.name);
      setMedicalID(res.npa_idi);
      setPhoto(res.photo);
    });
  };

  const onPressChangePhoto = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Ubah_Foto)
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log("Response = ", response);
      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        setPhoto(response.uri);
        setUriPhoto(response.uri);
      }
    });
  };

  const gotoCmeCertificate = async () => {
    let requestParams = await getData(KEY_ASYNC_STORAGE.SUBMIT_CME_QUIZ);
    console.log("SUBMIT_CME_QUIZ: ", requestParams);

    if (!_.isEmpty(requestParams) && params.isFromAlertSearchPage != true) {
      let responseSubmitAnswerQuiz = await submitQuiz(requestParams);
      console.log("responseSubmitAnswerQuiz: ", responseSubmitAnswerQuiz);
      setIsShowLoader(false);

      if (responseSubmitAnswerQuiz.acknowledge) {
        if (params.from == "CmeQuiz") {
          params.from = "profile";
        } else if (params.from == "notification") {
          params.isFromListNotifProfile = true;
        }
        await removeData(KEY_ASYNC_STORAGE.SUBMIT_CME_QUIZ);

        params.certificate = responseSubmitAnswerQuiz.result.certificate;
        navigation.replace("CmeCertificate", params);
      } else {
        Toast.show({
          text: "Something went wrong! " + responseSubmitAnswerQuiz.message,
          position: "bottom",
          duration: 3000,
        });
      }
    }
    else {
      setIsShowLoader(false);
    }
  };

  const doUpdateProfile = async (uri) => {
    try {
      setIsShowLoader(true);
      let response = await updatePhotoProfile(uri, fullname, medicalID);
      console.log("response: ", response);
      if (response.acknowledge) {
        let existProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
        (existProfile.name = fullname), (existProfile.photo = photo);
        existProfile.npa_idi = medicalID;

        storeData(KEY_ASYNC_STORAGE.PROFILE, existProfile);
        if (!_.isEmpty(medicalID)) {
          if (
            params.from == "CmeQuiz" ||
            params.from == "notification" ||
            params.from == "pushNotifCertificate" ||
            params.from == "SearchKonten" ||
            params.from == "home"
          ) {
            gotoCmeCertificate();
          } else {
            setIsShowLoader(false);
          }
        } else {
          setIsShowLoader(false);
        }

        if (params.from == "biodata") {
          backToBiodata()
        }
        Toast.show({
          text: response.message,
          position: "bottom",
          duration: 3000,
        });
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "bottom",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      setIsShowLoader(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "bottom",
        duration: 3000,
      });
      console.log("error: ", error);
    }
  };

  const onChangeTextFullName = (text) => {
    setFullname(text);
    if (text.length < 1) {
      setIsErrorField(true);
      setMessageErrorField("Nama Lengkap wajib diisi");
    } else if (text.length > 0 && text.length < 3) {
      setIsErrorField(true);
      setMessageErrorField("Nama Lengkap terlalu pendek minimal 3 karakter");
    } else {
      if (isValidNameByRegex(text)) {
        setIsErrorField(false);
        setMessageErrorField("");
      } else {
        setIsErrorField(true);
        setMessageErrorField("Nama Lengkap hanya boleh berupa huruf dan spasi");
      }
    }
  };

  onPressButtonSave = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Finish)
    if (fullname.length < 3) {
    } else {
      if (!isErrorField && !isErrorFieldMedicalID) {
        doUpdateProfile(uriPhoto);
      }
    }
  };

  const onChangeTextMedicalID = (text) => {
    setMedicalID(text);
    setIsErrorFieldMedicalID(false);

    if (isValidateNpaIDI(text)) {
      if (text.length > 0 && text.length < 3) {
        setMessageErrorFieldMedicalID(
          "Medical ID terlalu pendek minimal 3 karakter"
        );
        setIsErrorFieldMedicalID(true);
      } else {
        setIsErrorFieldMedicalID(false);
      }
    } else {
      setIsErrorFieldMedicalID(true);
      setMessageErrorFieldMedicalID("Medical ID hanya boleh berupa angka");
    }
  };
  return (
    <Container>
      <Loader visible={isShowLoader} />
      <HeaderToolbar title="Ubah Profil" onPress={handleBackButton} />
      <SafeAreaView style={{ flex: 1 }}>
        <Content>
          <View style={styles.contentContainer}>
            <View>
              <View style={styles.photoProfileContainer}>
                <PhotoProfileHome imageURL={photo} />
              </View>
              <TouchableOpacity
                style={styles.buttonTextUbahFoto}
                onPress={onPressChangePhoto}
                {...testID("button_change_photo")}
              >
                <Text textButtonMediumBlue>UBAH FOTO</Text>
              </TouchableOpacity>

              <InputField
                label="Nama Lengkap"
                placeholder="Masukkan Nama Lengkap"
                onChangeText={(text) =>
                  onChangeTextFullName(removeEmojis(text))
                }
                value={fullname}
                isError={isErrorField}
                messageError={messageErrorField}
                maxLength={200}
                {...testID("input_fullname")}
                onFocus={() => AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Nama_Lengkap)}
              />

              <InputField
                {...testID("input_medical_id")}
                label="Medical ID"
                placeholder="Masukkan Medical ID"
                onChangeText={(text) =>
                  onChangeTextMedicalID(removeEmojis(text))
                }
                value={medicalID}
                isError={isErrorFieldMedicalID}
                messageError={messageErrorFieldMedicalID}
                keyboardType={"number-pad"}
                maxLength={50}
                onFocus={() => AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Profil_Medical_ID)}
              />
            </View>
          </View>
        </Content>
        <View style={styles.buttonSaveWrapper}>
          <Button
            {...testID("button_save")}
            text="SIMPAN"
            onPress={onPressButtonSave}
          />
        </View>
      </SafeAreaView>
    </Container>
  );
};

export default ChangeProfile;

const styles = StyleSheet.create({
  photoProfileContainer: {
    alignSelf: "center",
    marginTop: 20,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingBottom: 16,
    flex: 1,
    justifyContent: "space-between",
  },
  buttonTextUbahFoto: {
    alignSelf: "center",
    marginVertical: 20,
  },
  buttonSaveWrapper: {
    height: 48,
    marginHorizontal: 16,
    marginBottom: 16,
  },
});
