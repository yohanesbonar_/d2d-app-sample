import {
  Body,
  Container,
  Content,
  Header,
  Right,
  Title,
  View,
  Text,
  Toast,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Dimensions,
  Image,
  Linking,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack, IconMoreVertial, IconNavigateRight } from "../../assets";
import _, { result } from "lodash";
import {
  BottomSheet,
  EmptyDataKonferensi,
  HeaderToolbar,
  ImageCarousel,
  MainMenuHome,
} from "../../components/molecules";
import { Loader } from "../../../app/components";
import { ScrollView } from "react-native-gesture-handler";
import { Avatar } from "../../components/atoms";
import { Modalize } from "react-native-modalize";
import { unSubscribeChannel } from "../../utils/network/channel/unSubscribeChannel";
import { getChannelDetail } from "../../utils/network/channel/channelDetail";
import HTML from "react-native-render-html";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const ChannelDetail = ({ navigation }) => {
  const params = navigation.state.params;
  console.log("params channel detail", params);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [titleChannel, setTitleChannel] = useState("");
  const [descChannel, setDescChannel] = useState("");
  const [xidChannel, setXidChannel] = useState("");
  const [pictureSource, setPictureSource] = useState("");
  const [dataBanner, setDataBanner] = useState([]);
  const [typeBottomSheet, setTypeBottomSheet] = useState([]);
  const [dataChannelDetail, setDataChannelDetail] = useState({});
  const [listMenu, setListMenu] = useState([]);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isNewSubscribe, setIsNewSubscribe] = useState(
    params.isNewSubscribe ? true : false
  );

  useEffect(() => {
    getData();
    AdjustTracker(AdjustTrackerConfig.Forum_Start);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const getData = async () => {
    await getDetail();

    if (isNewSubscribe) {
      setIsNewSubscribe(false);
      Toast.show({
        text:
          "Berhasil gabung ke forum " +
          (params.titleChannel != "" ? params.titleChannel : ""),
        position: "bottom",
        buttonText: "TUTUP",
        duration: 3000,
      });
    }
  };

  const onRefreshData = () => {
    setDataChannelDetail({});
    setListMenu([]);
    setDataBanner([]);
    setXidChannel("");
    setPictureSource("");
    setTitleChannel("");
    setDescChannel("");
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      getData();
    }
  }, [isRefresh]);

  const getDetail = async () => {
    setIsRefresh(false);
    let response = {};
    setIsShowLoader(true);
    try {
      response = await getChannelDetail(params.item.xid);
      if (response.header.message == "Success") {
        let result = response.data;
        setDataChannelDetail(result);
        setListMenu(result.menus);
        setDataBanner(result.banners);
        console.log("log CHANNEL DETAIL", result);
        setXidChannel(result.xid);
        setPictureSource(result.logo);
        setTitleChannel(result.channel_name);
        setDescChannel(result.description);
        setIsShowLoader(false);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const renderMenuListImage = () => {
    console.log("list menu", listMenu);
    return (
      <View style={styles.containerMenuImage}>
        <FlatList
          data={listMenu}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => onClickMenuImage(item)}
              style={styles.GridViewBlockStyle}
            >
              <Image
                source={{ uri: item.icon || "" }}
                style={{ width: 48, height: 48 }}
              />
              <Text style={styles.textTitleImage}>{item.name}</Text>
            </TouchableOpacity>
          )}
          numColumns={3}
        />
      </View>
    );
  };

  const onClickMenuImage = (item) => {
    if (item.disabled == false && item.link != "FORBIDEN") {
      console.log("menu name -> ", item.name);
      console.log("menu link -> ", item.link);
      console.log("menu sbcChannel -> ", dataChannelDetail.sbc);
      if (item.link == "ComingSoon") {
        navigation.navigate(item.link, {
          title: item.name,
        });
      } else {
        navigation.navigate(item.link, {
          item: item,
          sbcChannel: dataChannelDetail.sbc,
          from: "Forum",
        });
        if (item.link == "Forum") {
          AdjustTracker(AdjustTrackerConfig.Forum_Diskusi);
        } else if (item.link == "Request Jurnal") {
          AdjustTracker(AdjustTrackerConfig.Forum_Request_Jurnal);
        } else if (item.link == "ChannelKonferensi") {
          AdjustTracker(AdjustTrackerConfig.Forum_Konferensi);
        } else if (item.link == "Job") {
          AdjustTracker(AdjustTrackerConfig.Forum_Job);
        }
      }
    } else {
      Toast.show({
        text:
          "Menu " +
          item.name +
          " belum tersubscribe oleh Channel " +
          titleChannel,
        position: "bottom",
        duration: 2000,
      });
    }
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        {typeBottomSheet == "BottomSheetAbout" && (
          <BottomSheet
            onPressClose={() => onClose()}
            title={titleChannel}
            desc={descChannel}
            wordingClose="TUTUP"
            type="bottomSheetAboutChannel"
            imageSource={pictureSource}
          />
        )}

        {typeBottomSheet == "BottomSheetExitChannel" && (
          <BottomSheet
            onPressClose={() => {
              onClose();
              AdjustTracker(AdjustTrackerConfig.Forum_Tinggalkan_Forum_Batal);
            }}
            title="Tinggalkan Forum"
            wordingClose="BATAL"
            type="BottomSheetExitChannel"
            onPressExitChannel={() => {
              onPressUnSubcribe();
              AdjustTracker(AdjustTrackerConfig.Forum_Tinggalkan_Forum);
            }}
          />
        )}
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const onPressAboutPrincipal = () => {
    onOpen();
    setTypeBottomSheet("BottomSheetAbout");
  };

  const onPressMoreRight = () => {
    onOpen();
    setTypeBottomSheet("BottomSheetExitChannel");
  };

  const onPressUnSubcribe = async () => {
    let response = {};
    setIsShowLoader(true);
    try {
      let paramsGetAllChannel = {
        xid: xidChannel,
      };
      response = await unSubscribeChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        onClose();
        if (params.from == "Channel" || params.from == "SearchChannel") {
          navigation.state.params.onRefresh();
        }
        navigation.goBack();
        return true;
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const excludeHtmlImage = (htmlParam) => {
    let splitHtmlString = htmlParam.split("<p");
    var correctHtml = "";
    var isCompleted = false;
    splitHtmlString.forEach((element) => {
      console.log("element--", element);
      if (element.replace(" ", "") !== "") {
        let imageHtmlComponentIndex = element.indexOf("<img");
        let iframeHtmlComponentIndex = element.indexOf("<iframe");
        if (
          imageHtmlComponentIndex < 0 &&
          iframeHtmlComponentIndex < 0 &&
          !isCompleted
        ) {
          correctHtml = "<p" + element;
          isCompleted = true;
        }
      }
    });
    return correctHtml;
  };

  const onPressBanner = (link) => {
    let regex = /d2d.co.id/;
    if (link != null) {
      if (
        link.includes("menu") &&
        link.includes("channel") &&
        link.includes("forum") &&
        regex.test(link)
      ) {
        navigation.popToTop();
        navigation.state.params.onPressBanner(link);
      } else if (regex.test(link)) {
        navigation.popToTop();
        navigation.state.params.onPressBanner(link);
      } else {
        Linking.openURL(link);
      }
    }
  };

  return (
    <Container>
      {renderBottomSheet()}
      <Loader visible={isShowLoader} />

      <HeaderToolbar
        title={titleChannel}
        onPress={handleBackButton}
        onPressRightMenu={onPressMoreRight}
        iconRightMenu="iconMoreVertical"
      />
      <Content
        style={{
          backgroundColor: "#FFFFFF",
        }}
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={onRefreshData} />
        }
      >
        <View style={styles.viewAllContent}>
          <TouchableOpacity
            onPress={() => {
              onPressAboutPrincipal();
              AdjustTracker(AdjustTrackerConfig.Forum_Tentang);
            }}
          >
            <View style={styles.avatarAboutContainer}>
              <Avatar name={titleChannel} imageSource={pictureSource} />
              <View style={styles.viewRightSideAboutContainer}>
                {titleChannel != "" ? (
                  <Text
                    style={styles.textAboutChannel}
                    numberOfLines={1}
                    ellipsizeMode={"tail"}
                  >
                    Tentang {titleChannel}
                  </Text>
                ) : (
                  <View />
                )}
                {descChannel != "" ? (
                  <HTML
                    baseFontStyle={styles.descChannel}
                    html={excludeHtmlImage(descChannel)}
                    ignoredStyles={[
                      "font-family",
                      "text-decoration-style",
                      "text-decoration-color",
                    ]}
                    renderers={{
                      p: (_, children) => (
                        <Text numberOfLines={1} ellipsizeMode={"tail"}>
                          {children}
                        </Text>
                      ),
                      img: (_, children) => (
                        <View style={{ display: "none" }} />
                      ),
                      iframe: (_, children) => (
                        <View style={{ display: "none" }} />
                      ),
                    }}
                  />
                ) : (
                  <View />
                )}
              </View>
              {_.isEmpty(dataChannelDetail) ? null : <IconNavigateRight />}
            </View>
          </TouchableOpacity>
          {_.isEmpty(dataBanner) && !_.isEmpty(listMenu) ? (
            <View style={styles.nullBannerView} />
          ) : (
            <View style={styles.containerBanner}>
              <ImageCarousel
                dataBanner={dataBanner}
                onPress={(link) => {
                  // Linking.openURL(link);
                  onPressBanner(link);
                }}
                type="BannerChannel"
              />
            </View>
          )}

          {renderMenuListImage()}
          {_.isEmpty(dataBanner) && _.isEmpty(listMenu) && !isShowLoader ? (
            <View style={styles.containerEmpty}>
              <EmptyDataKonferensi
                title="Belum Ada Fitur Forum"
                subtitle="Selalu ada fitur yang menarik disini. Tetap selalu update dan coba datang kembali nanti."
              />
            </View>
          ) : null}
        </View>
      </Content>
    </Container>
  );
};

export default ChannelDetail;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 10,
    marginVertical: 8,
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
  },
  avatarAboutContainer: {
    flex: 1,
    flexDirection: "row",
    marginTop: 20,
    alignItems: "center",
  },
  textAboutChannel: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginVertical: 8,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  descChannel: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    marginBottom: 7,
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  viewAllContent: { marginHorizontal: 16, flex: 1 },
  viewRightSideAboutContainer: {
    flex: 1,
    flexDirection: "column",
    paddingLeft: 16,
    paddingRight: 24,
  },
  containerBanner: { marginTop: 20, marginLeft: -16 },
  menuGrid: {
    flexDirection: "row",
    flex: 0.5,
    width: Dimensions.get("window").width,
    justifyContent: "space-evenly",
    marginVertical: 16,
  },
  lastMenuGrid: {
    flexDirection: "row",
    flex: 0.5,
    width: Dimensions.get("window").width,
    justifyContent: "flex-start",
    marginVertical: 16,
  },
  GridViewBlockStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    width: Dimensions.get("window").width / 3 - 18,
    // height: 90,
    paddingVertical: 12,
    marginBottom: 16,
  },

  textTitleImage: {
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    color: "#000000",
    marginTop: 2,
    lineHeight: 20,
    letterSpacing: 0.25,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  containerMenuImage: {
    marginTop: 20,
  },
  nullBannerView: { marginVertical: 10 },
  containerEmpty: {
    flex: 1,
    alignSelf: "center",
    marginTop: 150,
  },
});
