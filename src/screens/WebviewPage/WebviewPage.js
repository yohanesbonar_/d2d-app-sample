import { Container } from "native-base";
import React from "react";
import { useEffect } from "react";
import { BackHandler, StyleSheet } from "react-native";
import { HeaderToolbar } from "../../components";
import Webview from "react-native-webview";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const WebviewPage = ({ navigation }) => {
  const { params } = navigation.state;

  useEffect(() => {
    initData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const initData = () => {
    if (params.name == "Dkonsul") {
      AdjustTracker(
        AdjustTrackerConfig.Forum_Produk_Dkonsul_Registration_Form_Start
      );
    } else if (params.name == "Teman Bumil") {
    } else if (params.name == "Teman Diabetes") {
      AdjustTracker(
        AdjustTrackerConfig.Forum_Produk_Teman_Diabetes_Registration_Form_Start
      );
    }
  };

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  return (
    <Container>
      <HeaderToolbar title={params.title} onPress={handleBackButton} />
      <Webview
        source={{
          uri: params.url, //URL of your redirect site
        }}
        startInLoadingState
        javaScriptEnabled={true}
        showsVerticalScrollIndicator={false}
      />
    </Container>
  );
};

export default WebviewPage;

const styles = StyleSheet.create({});
