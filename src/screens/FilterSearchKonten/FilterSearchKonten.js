import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack } from "../../assets";
import _, { initial } from "lodash";
import { Loader } from "../../../app/components";
import OptionRadioButton from "../../components/molecules/OptionRadioButton";
import { ArrowBackButton, Button } from "../../components/atoms";

const FilterSearchKonten = ({ navigation }) => {
  const params = navigation.state.params;
  const [checkedValue, setCheckedValue] = useState(params.dataFilter);
  const [isShowLoader, setIsShowLoader] = useState(false);
  let value = "";
  useEffect(() => {
    // console.log("allbykonten params", params);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const renderRadioButton = () => {
    return (
      <View style={{}}>
        <OptionRadioButton
          onPress={() => setCheckedValue("", () => console.log("hi callback"))}
          categoryName={"Semua"}
          selected={checkedValue == "" ? true : false}
        />

        {(params.listCategory.upcomingwebinar == false ||
          params.listCategory.webinar == false ||
          params.listCategory.livewebinar == false) && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("Webinar", () => console.log("hi callback"))
            }
            categoryName={"Webinar"}
            selected={checkedValue == "Webinar" ? true : false}
          />
        )}
        {params.listCategory.event == false && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("Event", () => console.log("hi callback"))
            }
            categoryName={"Event"}
            selected={checkedValue == "Event" ? true : false}
          />
        )}
        {params.listCategory.jurnal == false && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("Journal", () => console.log("hi callback"))
            }
            categoryName={"Jurnal"}
            selected={checkedValue == "Journal" ? true : false}
          />
        )}
        {params.listCategory.guideline == false && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("Guideline", () => console.log("hi callback"))
            }
            categoryName={"Guideline"}
            selected={checkedValue == "Guideline" ? true : false}
          />
        )}
        {params.listCategory.cme == false && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("Cme", () => console.log("hi callback"))
            }
            categoryName={"CME"}
            selected={checkedValue == "Cme" ? true : false}
          />
        )}
        {params.listCategory.job == false && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("Job", () => console.log("hi callback"))
            }
            categoryName={"Job"}
            selected={checkedValue == "Job" ? true : false}
          />
        )}
        {params.listCategory.obatatoz == false && (
          <OptionRadioButton
            onPress={() =>
              setCheckedValue("ObatAtoz", () => console.log("hi callback"))
            }
            categoryName={"Obat A - Z"}
            selected={checkedValue == "ObatAtoz" ? true : false}
          />
        )}
      </View>
    );
  };

  const renderButtonApply = () => {
    return (
      <View style={styles.containerButtonApply}>
        <Button
          text="TERAPKAN FILTER"
          type={"elevationButton"}
          onPress={() => {
            navigation.state.params.onSelect({ checkedValue });
            navigation.goBack();
            return true;
          }}
        />
      </View>
    );
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.containerLeftHeader}>
              <View style={styles.viewArrowBackButton}>
                <ArrowBackButton onPress={() => handleBackButton()} />
              </View>

              <Title style={styles.textTitle}>Filter</Title>
            </View>
          </Body>
          <TouchableOpacity
            onPress={() => setCheckedValue("")}
            style={styles.buttonReset}
          >
            <Text style={styles.textReset}> RESET </Text>
          </TouchableOpacity>
        </Header>
      </SafeAreaView>
      <View style={styles.viewContainer}>
        {renderButtonApply()}
        <Content style={styles.contentContainer}>
          <View style={styles.viewMainContainer}>
            <Text style={styles.textKategori}>Kategori</Text>
            {renderRadioButton()}
          </View>
        </Content>
      </View>
    </Container>
  );
};

export default FilterSearchKonten;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
    marginRight: 16,
    justifyContent: "space-between",
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
  },
  textReset: {
    fontSize: 14,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    alignItems: "center",
  },
  textKategori: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 16,
    marginTop: 20,
  },
  viewContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  containerButtonApply: {
    marginVertical: 16,
    marginHorizontal: 16,
    elevation: 4,
  },
  containerRightHeader: {
    flex: 1,
    flexDirection: "row-reverse",
    justifyContent: "center",
  },
  containerLeftHeader: { flex: 1, flexDirection: "row", alignItems: "center" },
  contentContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  buttonReset: {
    marginRight: 16,
    justifyContent: "center",
  },
  viewMainContainer: {
    marginHorizontal: 16,
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 20 },
});
