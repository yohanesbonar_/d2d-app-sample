import { Container, Content, Text, Toast } from "native-base";
import React, { useEffect, useState } from "react";
import { BackHandler, StyleSheet, TouchableOpacity, View, Keyboard } from "react-native";
import {
  Button,
  HeaderToolbar,
  InputField,
  SelectionButton,
  BottomSheet
} from "../../components";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment-timezone";
import {
  deleteSpecialist,
  getData,
  getDateFormatIndonesia,
  getMonthShortNameIndonesia,
  getProfile,
  KEY_ASYNC_STORAGE,
  removeEmojis,
  storeData,
  updateDataProfile,
  validatePhoneNumber,
} from "../../utils";
import _ from "lodash";
import { Modalize } from "react-native-modalize";
import { ThemeD2D } from "../../../theme";
import OptionRadioButton from "../../components/molecules/OptionRadioButton";
import { doSendCompetence, ParamCompetence } from "../../../app/libs/NetworkUtility";
import { Loader } from "../../../app/components";
import { checkTopicsBySubscription } from "../../../app/libs/Common";
import DeviceInfo from 'react-native-device-info';

const ChangeDetailProfile = ({ navigation }) => {
  const { params } = navigation.state;
  console.log("params: ", params);
  const [bornDate, setBornDate] = useState(params.born_date);
  const [phone, setPhone] = useState(params.phone);
  const [specialization, setSpecialization] = useState(params.spesialization);
  const [nonSpecialization, setNonSpecialization] = useState(params.nonspesialization);
  const [nonSpecialistId, setNonSpecialistId] = useState(_.isEmpty(params.nonspesialization) ? 0 : params.nonspesialization[0].id);
  const [selectedSpesialization, setSelectedSpecialization] = useState({});
  const [peminatanStudi, setPeminatanStudi] = useState(params.subscription);

  const [typePracticeLocation_1, setTypePracticeLocation_1] = useState(params.clinic_type_1)
  const [typePracticeLocation_2, setTypePracticeLocation_2] = useState(params.clinic_type_2)
  const [typePracticeLocation_3, setTypePracticeLocation_3] = useState(params.clinic_type_3)

  const [countPracticeLocation, setCountPracticeLocation] = useState(1)
  const [countEducation, setCountEducation] = useState(1)

  const [selectedPositionLocation, setSelectedPositionLocation] = useState(1)
  const [selectedPositionSpecialist, setSelectedPositionSpecialist] = useState(1)

  const [practiceLocation_1, setPracticeLocation_1] = useState(params.clinic_location_1)
  const [practiceLocation_2, setPracticeLocation_2] = useState(params.clinic_location_2)
  const [practiceLocation_3, setPracticeLocation_3] = useState(params.clinic_location_3)

  const [messageResponseUpdateProfile, setMessageResponseUpdateProfile] = useState("")

  const [education_1, setEducation_1] = useState(params.education_1)
  const [education_2, setEducation_2] = useState(params.education_2)
  const [education_3, setEducation_3] = useState(params.education_3)

  const [positionInputPhoneNumber, setPositionInputPhoneNumber] = useState(0)
  const [positionInputDomicileCity, setPositionInputDomicileCity] = useState(0)
  const [positionInputSingleSpecialist, setPositionInputSingleSpecialist] = useState(0)
  const [positionInputSpecialist, setPositionInputSpecialist] = useState(0)
  const [positionInputNonSpecialist, setPositionInputNonSpecialist] = useState(0)
  const [positionInputPeminatanStudi, setPositionInputPeminatanStudi] = useState(0)

  const [isErrorFieldTypePracticeLocation1, setIsErrorFieldTypePracticeLocation1] = useState(false);
  const [isErrorFieldTypePracticeLocation2, setIsErrorFieldTypePracticeLocation2] = useState(false);
  const [isErrorFieldTypePracticeLocation3, setIsErrorFieldTypePracticeLocation3] = useState(false);

  const [isThereChanges, setIsThereChanges] = useState(false);

  const [isSendSpesialzation, setIsSendSpesialization] = useState(false)
  const [isSendSubscription, setIsSendSubscription] = useState(false)
  const [isSendNonSpesialization, setIsSendNonSpesialization] = useState(false)
  const [isShowLoader, setIsShowLoader] = useState(false)

  const [isErrorFieldPracticeLocation1, setIsErrorFieldPracticeLocation1] = useState(false);
  const [isErrorFieldPracticeLocation2, setIsErrorFieldPracticeLocation2] = useState(false);
  const [isErrorFieldPracticeLocation3, setIsErrorFieldPracticeLocation3] = useState(false);

  const [errorMessageFieldPracticeLocation1, setErrorMessageFieldPracticeLocation1] = useState("");
  const [errorMessageFieldPracticeLocation2, setErrorMessageFieldPracticeLocation2] = useState("");
  const [errorMessageFieldPracticeLocation3, setErrorMessageFieldPracticeLocation3] = useState("");

  const [errorMessageFieldTypePracticeLocation1, setErrorMessageFieldTypePracticeLocation1] = useState("");
  const [errorMessageFieldTypePracticeLocation2, setErrorMessageFieldTypePracticeLocation2] = useState("");
  const [errorMessageFieldTypePracticeLocation3, setErrorMessageFieldTypePracticeLocation3] = useState("");

  const [positionInputTypePracticeLocation1, setPositionInputTypePracticeLocation1] = useState(0)
  const [positionInputTypePracticeLocation2, setPositionInputTypePracticeLocation2] = useState(0)
  const [positionInputTypePracticeLocation3, setPositionInputTypePracticeLocation3] = useState(0)

  const [positionInputEducation1, setPositionInputEducation1] = useState(0)
  const [positionInputEducation2, setPositionInputEducation2] = useState(0)
  const [positionInputEducation3, setPositionInputEducation3] = useState(0)

  const [shownBornDate, setShownBornDate] = useState(
    getDateFormatIndonesia(params.born_date)
  );

  const [typeBottomsheet, setTypeBottomsheet] = useState("")
  const [isShowDatePicker, setIsShowDatePicker] = useState(false);
  const [keyStatusPegawai, setKeyStatusPegawai] = useState(null);
  const [domicileCity, setDomicileCity] = useState(params.home_location);

  const [isErrorFieldSpecialist, setIsErrorFieldSpecialist] = useState(false);
  const [isErrorFieldBornDate, setIsErrorFieldBornDate] = useState(false);
  const [isErrorFieldPhone, setIsErrorFieldPhone] = useState(false);
  const [isErrorFieldDomicileCity, setIsErrorFieldDomicileCity] = useState(
    false
  );

  const [isErrorFieldPeminatanStudi, setIsErrorFieldPeminatanStudi] = useState(
    false
  );

  const [isErrorFieldEducation, setIsErrorFieldEducation] = useState(false);

  const [errorMessageFieldBornDate, setErrorMessageFieldBornDate] = useState(
    ""
  );
  const [errorMessageFieldPhone, setErrorMessageFieldPhone] = useState("");
  const [errorMessageFieldSpecialist, setErrorMessageFieldSpecialist] = useState("");
  const [
    errorMessageFieldDomicileCity,
    setErrorMessageFieldDomicileCity,
  ] = useState("");
  const [
    errorMessageFieldPeminatanStudi,
    setErrorMessageFieldPeminatanStudi,
  ] = useState("");

  const [
    errorMessageFieldPracticeEducation,
    setErrorMessageFieldEducation,
  ] = useState("");

  useEffect(() => {
    setDataStatusPegawai();
    setDataCountPracticeLocation();
    setDataCountEducation();
    if (params.from != "" && params.from == "JobDetail") {
      validateBeforeSave();
    }
  }, []);

  useEffect(() => {
    if (typeBottomsheet === 'back') {
      console.log("typeBottomsheet", typeBottomsheet)
      openBottomsheet();
    }
  }, [typeBottomsheet]);


  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    }
  })


  useEffect(() => {
    if (messageResponseUpdateProfile != "") {
      getDataProfile()
    }
  }, [messageResponseUpdateProfile]);
  const setDataCountPracticeLocation = () => {
    if (!_.isEmpty(practiceLocation_1)) {
      setCountPracticeLocation(1)
    }
    if (!_.isEmpty(practiceLocation_2)) {
      setCountPracticeLocation(2)
    }
    if (!_.isEmpty(practiceLocation_3)) {
      setCountPracticeLocation(3)
    }
  }

  const setDataCountEducation = () => {
    if (!_.isEmpty(education_1)) {
      setCountEducation(1)
    }
    if (!_.isEmpty(education_2)) {
      setCountEducation(2)
    }
    if (!_.isEmpty(education_3)) {
      setCountEducation(3)
    }
  }

  const setDataStatusPegawai = () => {
    if (params.pns == false || params.pns == null) {
      setKeyStatusPegawai("swasta");
    } else if (params.pns == true) {
      setKeyStatusPegawai("pns");
    }
  };

  const handleBackButton = () => {
    Keyboard.dismiss();

    if (isThereChanges) {
      setTypeBottomsheet("")
      setTypeBottomsheet("back")
      openBottomsheet();
    }
    else {
      navigation.goBack();
    }
    return true;

  };



  const backToBiodata = () => {
    navigation.goBack()
    navigation.state.params.onRefreshDataProfile();
  }

  const handleDatePicked = (date) => {
    let shownDatestring =
      date.getDate() +
      " " +
      getMonthShortNameIndonesia(date.getMonth()) +
      " " +
      date.getFullYear();
    let bornDate =
      date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
    setShownBornDate(shownDatestring);
    setBornDate(bornDate);
    hideDatePicker();
  };

  const hideDatePicker = () => setIsShowDatePicker(false);

  const onChangeTextPhone = (text) => {
    setPhone(text.replace(/[^0-9]/g, ''));
    if (text.length < 1) {
      setIsErrorFieldPhone(true);
      setErrorMessageFieldPhone("Nomor Handphone wajib diisi");
    }
    else if (text.length >= 1 && text.length <= 8) {
      setIsErrorFieldPhone(true);
      setErrorMessageFieldPhone("Nomor Handphone minimal 8 karakter");
    }
    else {
      if (validatePhoneNumber(text)) {
        setIsErrorFieldPhone(false);
        setErrorMessageFieldPhone("");
      }
      else {
        setIsErrorFieldPhone(true);
        setErrorMessageFieldPhone("Nomor Handphone tidak valid");
      }
    }
    setIsThereChanges(true)
  };

  const onLayoutPhoneNumber = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputPhoneNumber(y)
  };

  const onLayoutDomicileCity = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputDomicileCity(y)
  };

  const onLayoutSingleSpecialist = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputSingleSpecialist(y)
  };

  const onLayoutNonSpecialist = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputNonSpecialist(y)
  };

  // const onLayoutSpecialist = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
  //   setPositionInputSingleSpecialist(y)
  // };

  const onLayoutPeminatanStudi = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputPeminatanStudi(y)
  };



  const onLayoutTypePracticeLocation1 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputTypePracticeLocation1(y)
  };

  const onLayoutTypePracticeLocation2 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputTypePracticeLocation2(y)
  };

  const onLayoutTypePracticeLocation3 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputTypePracticeLocation3(y)
  };


  const onLayoutEducation1 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputEducation1(y)
  };

  const onLayoutEducation2 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputEducation2(y)
  };

  const onLayoutEducation3 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
    setPositionInputEducation3(y)
  };



  const doUpdateDataSpesialization = (spesializationList) => {
    spesializationList.map((value, i) => {
      let index = value.subspecialist.findIndex(x => x.id === 0);

      if (index != -1) {
        value.subspecialist.splice(index, 1)
      }
    })

    setSpecialization(spesializationList)
    setIsSendSpesialization(true)
    setIsThereChanges(true)
  };

  const gotoSelectSpecialist = (params) => {
    navigation.navigate("SelectSpecialist", params);
  };


  const selectionButtons = [
    {
      key: "pns",
      text: "PNS",
    },
    {
      key: "swasta",
      text: "SWASTA",
    },
  ];

  const doChooseCity = () => {
    navigation.navigate("SearchCity", {
      onSelectedCity: onSelectedCity,
    });
  };

  const onSelectedCity = (city) => {
    setDomicileCity(city);
    setErrorMessageFieldDomicileCity("")
    setIsErrorFieldDomicileCity(false)
    setIsThereChanges(true)
  };

  const validateBeforeSave = () => {
    let isValid = true
    if (countPracticeLocation >= 3) {
      if (_.isEmpty(typePracticeLocation_3)) {
        setIsErrorFieldTypePracticeLocation3(true)
        setErrorMessageFieldTypePracticeLocation3("Tipe Praktek wajib diisi");
        contentRef.current._root.scrollToPosition(0, positionInputTypePracticeLocation3)
        isValid = false
      }
      if (_.isEmpty(practiceLocation_3)) {
        setIsErrorFieldPracticeLocation3(true)
        setErrorMessageFieldPracticeLocation3("Lokasi Praktek wajib diisi");
        contentRef.current._root.scrollToPosition(0, positionInputTypePracticeLocation3)
        isValid = false
      }
    }

    if (countPracticeLocation >= 2) {
      if (_.isEmpty(typePracticeLocation_2)) {
        setIsErrorFieldTypePracticeLocation2(true)
        setErrorMessageFieldTypePracticeLocation2("Tipe Praktek wajib diisi");
        contentRef.current._root.scrollToPosition(0, positionInputTypePracticeLocation2)
        isValid = false
      }
      if (_.isEmpty(practiceLocation_2)) {
        setIsErrorFieldPracticeLocation2(true)
        setErrorMessageFieldPracticeLocation2("Lokasi Praktek wajib diisi");
        contentRef.current._root.scrollToPosition(0, positionInputTypePracticeLocation2)
        isValid = false
      }
    }

    if (countPracticeLocation >= 1) {
      if (_.isEmpty(typePracticeLocation_1)) {
        setIsErrorFieldTypePracticeLocation1(true)
        setErrorMessageFieldTypePracticeLocation1("Tipe Praktek wajib diisi");
        contentRef.current._root.scrollToPosition(0, positionInputTypePracticeLocation1)
        isValid = false
      }

      if (_.isEmpty(practiceLocation_1)) {
        setIsErrorFieldPracticeLocation1(true)
        setErrorMessageFieldPracticeLocation1("Lokasi Praktek wajib diisi");
        contentRef.current._root.scrollToPosition(0, positionInputTypePracticeLocation1)
        isValid = false
      }
    }

    if (_.isEmpty(peminatanStudi)) {
      setIsErrorFieldPeminatanStudi(true)
      setErrorMessageFieldPeminatanStudi("Peminatan Studi wajib diisi");
      contentRef.current._root.scrollToPosition(0, positionInputPeminatanStudi)
      isValid = false
    }

    if (peminatanStudi.length > 3) {
      setIsErrorFieldPeminatanStudi(true)
      setErrorMessageFieldPeminatanStudi("Maksimal 3 peminatan studi yang dipilih")
      isValid = false
      contentRef.current._root.scrollToPosition(0, positionInputPeminatanStudi)
    }

    if (specialization.length > 2) {
      setIsErrorFieldSpecialist(true)
      setErrorMessageFieldSpecialist("Maksimal 2 spesialis yang dipilih")
      isValid = false
      contentRef.current._root.scrollToPosition(0, positionInputNonSpecialist)
    }

    if (_.isEmpty(domicileCity)) {
      setIsErrorFieldDomicileCity(true)
      setErrorMessageFieldDomicileCity("Kota Domisili wajib diisi");
      contentRef.current._root.scrollToPosition(0, positionInputDomicileCity)
      isValid = false
    }

    if (phone.length < 8) {
      setIsErrorFieldPhone(true);
      setErrorMessageFieldPhone("Nomor Handphone minimal 8 karakter");
      contentRef.current._root.scrollToPosition(0, positionInputPhoneNumber)
      isValid = false
    }

    if (_.isEmpty(phone)) {
      setIsErrorFieldPhone(true)
      setErrorMessageFieldPhone("Nomor Handphone wajib diisi");
      contentRef.current._root.scrollToPosition(0, positionInputPhoneNumber)
      isValid = false
    }

    return isValid

  }

  const onPressSaveButton = () => {
    console.log("onPressSaveButton", countPracticeLocation)

    let isValid = validateBeforeSave()


    if (isValid) {
      console.log("onPressSaveButton isValid: ", isValid)
      setIsShowLoader(true)

      if (isSendNonSpesialization === true) {
        doSendNonSpesialization()
      }
      else if (isSendSpesialzation === true) {
        if (_.isEmpty(specialization)) {
          deleteDataSpesialization()
        }
        else {
          doSendSpesialization()
        }
      } else if (isSendSubscription === true) {
        doSendSubscribtion();
      } else {
        doUpdateProfile();
      }
      //hit api
    }
    else {
      console.log("onPressSaveButton isValid: ", isValid)
    }

  };

  const deleteDataSpesialization = async () => {

    let response = await deleteSpecialist(ParamCompetence.SPESIALIZATION);
    console.log("deleteDataSpesialization response: ", response)

    if (response.acknowledge == true) {
      if (isSendSubscription === true) {
        setIsSendSpesialization(false)
        doSendSubscribtion();
      } else {
        doUpdateProfile();
      }
    } else {
      setIsShowLoader(false)
      Toast.show({
        text: response.message,
        position: "bottom",
        duration: 3000,
      });
    }
  }
  const doSendSpesialization = async () => {
    let response = await doSendCompetence(specialization, ParamCompetence.SPESIALIZATION);
    console.log("doSendSpesialization response: ", response)

    if (response.isSuccess == true) {
      setIsSendSpesialization(false)
      if (isSendSubscription) {
        doSendSubscribtion();
      } else {
        doUpdateProfile();
      }
    } else {
      setIsShowLoader(false)
      Toast.show({
        text: response.message,
        position: "bottom",
        duration: 3000,
      });
    }
  }

  const doSendNonSpesialization = async () => {
    let response = await doSendCompetence(nonSpecialization, ParamCompetence.NON_SPESIALIZATION);
    console.log("doSendNonSpesialization response: ", response)
    if (response.isSuccess == true) {
      setIsSendSpesialization(false)
      if (isSendSpesialzation) {
        if (_.isEmpty(specialization)) {
          deleteDataSpesialization()
        }
        else {
          doSendSpesialization()
        }
      } else {
        doUpdateProfile();
      }
    } else {
      setIsShowLoader(false)
      Toast.show({
        text: response.message,
        position: "bottom",
        duration: 3000,
      });
    }
  }

  const doSendSubscribtion = async () => {
    let response = await doSendCompetence(peminatanStudi, ParamCompetence.SUBSCRIPTION);
    console.log("doSendSubscribtion response: ", response)

    if (response.isSuccess == true) {
      await checkTopicsBySubscription(peminatanStudi)
      setIsSendSubscription(false)
      doUpdateProfile();
    } else {
      setIsShowLoader(false)
      Toast.show({
        text: response.message,
        position: "bottom",
        duration: 3000,
      });
    }
  }

  const getDataProfile = async () => {
    try {
      let response = await getProfile();
      console.log("response: ", response);
      if (response.acknowledge) {
        let result = response.result;
        await storeData(KEY_ASYNC_STORAGE.PROFILE, result);
        setIsShowLoader(false);
        Toast.show({ text: messageResponseUpdateProfile, position: 'bottom', buttonText: "TUTUP", duration: 3000 });
        backToBiodata()
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const doUpdateProfile = async () => {
    let fcm_token = await getData(KEY_ASYNC_STORAGE.FCM_TOKEN)


    let profile = await getData(KEY_ASYNC_STORAGE.PROFILE)

    let npa_idi = profile.npa_idi

    let params = {
      country_code: profile.country_code,
      npa_idi: npa_idi,
      name: profile.name,
      email: profile.email,
      phone: phone,
      born_date: moment(bornDate, 'YYYY-MM-DD').format('YYYY-MM-DD'),
      //this.state.bornDate,
      education_1: education_1,
      education_2: _.isEmpty(education_2) ? '' : education_2,
      education_3: _.isEmpty(education_3) ? '' : education_3,
      home_location: domicileCity,
      clinic_location_1: practiceLocation_1,
      clinic_location_2: _.isEmpty(practiceLocation_2) ? '' : practiceLocation_2,
      clinic_location_3: _.isEmpty(practiceLocation_3) ? '' : practiceLocation_3,
      pns: keyStatusPegawai == "pns" ? true : false,

      clinic_type_1: typePracticeLocation_1,
      clinic_type_2: typePracticeLocation_2,
      clinic_type_3: typePracticeLocation_3,

      //device info
      app_version: DeviceInfo.getVersion(),
      device_unique_id: DeviceInfo.getUniqueId(),
      fcm_token: fcm_token,
      device_platform: DeviceInfo.getSystemName(),
      system_version: DeviceInfo.getSystemVersion(),
      os_build_version: await DeviceInfo.getBuildId(), //should be await, because it was promise,
      device_brand: DeviceInfo.getBrand()
    }


    console.log("updateDataProfile params: ", params)

    let response = await updateDataProfile(params);
    console.log('doUpdateProfile response', response)

    if (response.acknowledge == true) {
      setMessageResponseUpdateProfile(response.message)
      setIsThereChanges(false)
    } else {
      setIsShowLoader(false)
      Toast.show({ text: response.message, position: 'bottom', duration: 3000 });
    }

  }
  const renderBottomSheet = () => {
    let marginBottomContent = undefined
    marginBottomContent = ThemeD2D.platform == "ios" &&
      ThemeD2D.deviceHeight >= 812 &&
      ThemeD2D.deviceWidth >= 375
      ? 40
      : 16
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onBackButtonPress={() => null}
        onClose={closeBottomSheet()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        <View
          style={{
            marginBottom: typeBottomsheet == "back" ? undefined : marginBottomContent
          }}
        >
          {typeBottomsheet == "back" && (
            renderContentBottomsheetBack()
          )}
          {typeBottomsheet == "specialist" && (
            renderContentBottomsheetSpecialist()
          )}

          {typeBottomsheet == "practiceLocation" && (
            renderContenBottomsheetPracticeLocation()
          )}

          {typeBottomsheet == "setMainPracticeLocation" && (
            renderContentBottomsheetSetMainPracticeLocation()
          )}
        </View>
      </Modalize>
    );
  };

  const renderContentBottomsheetBack = () => {
    return (
      <View>
        <BottomSheet
          onPressCancel={() => closeBottomSheet()}
          onPressOk={() => {
            closeBottomSheet()
            navigation.goBack()
          }}
          title="Ingin Kembali?"
          desc="Segala perubahan data tidak akan disimpan."
          wordingCancel="BATAL"
          wordingOk="KEMBALI"
          type="bottomSheetLogout"
        />
      </View>
    )
  }
  const onPressRadioButtonTypePracticeLocation = async (type) => {
    if (selectedPositionLocation == 1) {
      setTypePracticeLocation_1(type)
      setIsErrorFieldTypePracticeLocation1(false)
      setErrorMessageFieldPracticeLocation1("")
    }
    else if (selectedPositionLocation == 2) {
      setTypePracticeLocation_2(type)
      setIsErrorFieldTypePracticeLocation2(false)
      setErrorMessageFieldPracticeLocation2("")
    }
    else if (selectedPositionLocation == 3) {
      setTypePracticeLocation_3(type)
      setIsErrorFieldTypePracticeLocation3(false)
      setErrorMessageFieldPracticeLocation3("")
    }
    setIsThereChanges(true)
    closeBottomSheet()
  };

  const renderContenBottomsheetPracticeLocation = () => {
    const typePracticeLocation = ["Rumah Sakit", "Institusi", "Klinik"]

    let currentType = ""
    if (selectedPositionLocation == 1) {
      currentType = typePracticeLocation.indexOf(typePracticeLocation_1)
    }
    else if (selectedPositionLocation == 2) {
      currentType = typePracticeLocation.indexOf(typePracticeLocation_2)
    }
    else if (selectedPositionLocation == 3) {
      currentType = typePracticeLocation.indexOf(typePracticeLocation_3)
    }

    return (
      <View>
        <Text style={styles.titleBottomsheetSpecialist}>
          Pilih Tipe Lokasi Praktek
          </Text>
        {typePracticeLocation.map((value, index) => {
          return (
            <OptionRadioButton
              key={index}
              onPress={() => onPressRadioButtonTypePracticeLocation(value)}
              categoryName={value}
              selected={currentType == index ? true : false}
            />
          )
        })}
      </View>
    )
  }

  const renderContentBottomsheetSpecialist = () => {
    return (
      <View>
        <Text style={styles.titleBottomsheetSpecialist}>
          Atur Spesialisasi {selectedPositionSpecialist}
        </Text>
        {selectedPositionSpecialist != 1 && (
          <TouchableOpacity
            style={styles.buttonBottomsheetSpecialistWrapper}
            onPress={onPressSetAsMainSpesialization}
          >
            <Text style={styles.textButtonBottomsheetSpecialist}>
              Atur Sebagai Spesialisasi Utama
            </Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={[
            styles.buttonBottomsheetSpecialistWrapper,
            { marginVertical: 8 },
          ]}
          onPress={() => onPressChangeSpecialization()}
        >
          <Text style={[styles.textButtonBottomsheetSpecialist]}>Ubah</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => onPressDeleteSpesialization()}
          style={styles.buttonBottomsheetSpecialistWrapper}
        >
          <Text style={styles.textButtonBottomsheetSpecialist}>Hapus</Text>
        </TouchableOpacity>

        <View style={styles.buttonCancelBottomsheetSpecialist}>
          <Button text="BATAL" onPress={closeBottomSheet} />
        </View>
      </View>
    )
  }

  const onPressSetAsMainPracticeLocation = () => {
    let location1 = practiceLocation_1
    let location2 = practiceLocation_2
    let location3 = practiceLocation_3

    let type1 = typePracticeLocation_1
    let type2 = typePracticeLocation_2
    let type3 = typePracticeLocation_3

    if (selectedPositionLocation == 2) {
      setPracticeLocation_1(location2)
      setPracticeLocation_2(location1)
      setTypePracticeLocation_1(type2)
      setTypePracticeLocation_2(type1)
    }
    else if (selectedPositionLocation == 3) {
      setPracticeLocation_1(location3)
      setPracticeLocation_2(location1)
      setPracticeLocation_3(location2)

      setTypePracticeLocation_1(type3)
      setTypePracticeLocation_2(type1)
      setTypePracticeLocation_3(type2)
    }
    closeBottomSheet();
  }

  const onPressDeletePracticeLocation = () => {
    let location1 = practiceLocation_1
    let location2 = practiceLocation_2
    let location3 = practiceLocation_3

    let type1 = typePracticeLocation_1
    let type2 = typePracticeLocation_2
    let type3 = typePracticeLocation_3

    if (countPracticeLocation > 1) {
      setCountPracticeLocation(countPracticeLocation - 1)
    }
    if (selectedPositionLocation == 1) {
      setPracticeLocation_1(location2)
      setTypePracticeLocation_1(type2)
      setPracticeLocation_2(location3)
      setTypePracticeLocation_2(type3)
      setPracticeLocation_3("")
      setTypePracticeLocation_3("")
    }
    else if (selectedPositionLocation == 2) {
      setPracticeLocation_2(location3)
      setTypePracticeLocation_2(type3)
      setPracticeLocation_3("")
      setTypePracticeLocation_3("")
    }
    else if (selectedPositionLocation == 3) {
      setPracticeLocation_3("")
      setTypePracticeLocation_3("")
    }
    closeBottomSheet()
  }

  const onSelect = async (data) => {
    if (!_.isEmpty(data.item)) {
      console.log("onSelect data: ", data)
      setNonSpecialistId(data.item.id);
      let temp = []
      temp.push(data.item)
      setNonSpecialization(temp);
      setIsSendNonSpesialization(true)
      setIsThereChanges(true)
    }
  };

  const renderContentBottomsheetSetMainPracticeLocation = () => {
    return (
      <View>
        <Text style={styles.titleBottomsheetSpecialist}>
          Atur Lokasi Praktek {selectedPositionLocation}
        </Text>
        {selectedPositionLocation != 1 && (
          <TouchableOpacity
            style={styles.buttonBottomsheetSpecialistWrapper}
            onPress={() => onPressSetAsMainPracticeLocation()}
          >
            <Text style={styles.textButtonBottomsheetSpecialist}>
              Atur Sebagai Lokasi Praktek Utama
            </Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() => onPressDeletePracticeLocation()}
          style={styles.buttonBottomsheetSpecialistWrapper}
        >
          <Text style={styles.textButtonBottomsheetSpecialist}>Hapus</Text>
        </TouchableOpacity>

        <View style={styles.buttonCancelBottomsheetSpecialist}>
          <Button text="BATAL" onPress={closeBottomSheet} />
        </View>
      </View>
    )
  }


  const modalizeRef = React.createRef(null);
  const contentRef = React.createRef(null);

  const openBottomsheet = () => {
    modalizeRef.current?.open();
  };

  const closeBottomSheet = () => {
    modalizeRef.current?.close();
  };

  const onPressChangeSpecialization = () => {
    closeBottomSheet()
    let params = {
      specialistList: specialization,
      saveDataSp: doUpdateDataSpesialization,
      from: "SPECIALIST",
      isChangeSpecialist: true,
      selectedSpesialization: selectedSpesialization
    };
    gotoSelectSpecialist(params)
  };

  const onPressSelectTypePracticeLocation = (position) => {
    setTypeBottomsheet("practiceLocation")
    setSelectedPositionLocation(position)
    openBottomsheet();
  }

  const onPressRightMenuDotVerticalSpecialist = (itemSpesialization, position) => {
    setTypeBottomsheet("specialist")
    setSelectedSpecialization(itemSpesialization);
    setSelectedPositionSpecialist(position)
    openBottomsheet();
  };

  const onPressRightMenuDotVerticalPracticeLocation = (position) => {
    setSelectedPositionLocation(position)
    setTypeBottomsheet("setMainPracticeLocation")
    openBottomsheet();
  };

  const onPressSelectNonSpecialist = () => {
    let params = {
      nonSpecialistId: nonSpecialistId,
      onSelect: onSelect,
    };
    navigation.navigate("NonSpecialist", params);

  }

  const onPressSelectSpecialist = () => {
    let params = {
      specialistList: specialization,
      saveDataSp: doUpdateDataSpesialization,
      from: "SPECIALIST",
    };
    gotoSelectSpecialist(params)
  }


  const onPressDeleteSpesialization = () => {
    let index = specialization.findIndex(
      (value) => value.id === selectedSpesialization.id
    );

    specialization.splice(index, 1);

    let tempSpecialization = [...specialization];
    if (tempSpecialization.length <= 2) {
      setIsErrorFieldSpecialist(false)
      setErrorMessageFieldSpecialist("")
    }
    setSpecialization(tempSpecialization);
    setIsSendSpesialization(true)
    closeBottomSheet();
  };

  const onPressSetAsMainSpesialization = () => {
    let index = specialization.findIndex(
      (value) => value.id === selectedSpesialization.id
    );

    specialization.splice(index, 1);
    specialization.unshift(selectedSpesialization);

    let tempSpecialization = [...specialization];

    setSpecialization(tempSpecialization);
    setIsSendSpesialization(true)
    setIsThereChanges(true)
    closeBottomSheet();
  };

  const onPressRemovePeminatanStudi = (index) => {
    let tempPeminatanStudi = [...peminatanStudi];
    tempPeminatanStudi.splice(index, 1);
    if (tempPeminatanStudi.length <= 3) {
      setIsErrorFieldPeminatanStudi(false)
      setErrorMessageFieldPeminatanStudi("")
    }
    setPeminatanStudi(tempPeminatanStudi);
    setIsSendSubscription(true)
    setIsThereChanges(true)
  };

  const onPressRemoveEducation = (position) => {
    let edu1 = education_1
    let edu2 = education_2
    let edu3 = education_3

    if (countEducation > 1) {
      setCountEducation(countEducation - 1)
    }
    if (position == 1) {
      setEducation_1(edu2)
      setEducation_2(edu3)
      setEducation_3("")
    }
    else if (position == 2) {
      setEducation_2(edu3)
      setEducation_3("")
    }
    else if (position == 3) {
      setEducation_3("")
    }
    setIsThereChanges(true)
  };

  onPressAddEducation = () => {
    setCountEducation(countEducation + 1)
    setIsThereChanges(true)
  }

  onPressAddPracticeLocation = () => {
    setCountPracticeLocation(countPracticeLocation + 1)
    setIsThereChanges(true)
  }

  doUpdateDataSubscribtion = (subscribtionList) => {
    setPeminatanStudi(subscribtionList)
    setIsSendSubscription(true)
    setIsErrorFieldPeminatanStudi(false)
    setErrorMessageFieldPeminatanStudi("")
    setIsThereChanges(true)
  };

  gotoSelectSubscribtions = () => {

    let dataInterest = peminatanStudi

    if (dataInterest == 1) {
      if (dataInterest[0].id == "0") {
        dataInterest = []
      }
    }

    let params = {
      specialistList: dataInterest,
      saveDataSp: doUpdateDataSubscribtion,
      from: 'SUBSCRIBTIONS',
      isPeminatanStudi: true
    }
    navigation.navigate("SelectSpecialist", params)

  }

  const onChangeTextPracticeLocation = (text, position) => {
    if (position == 1) {
      setPracticeLocation_1(text)
      if (text.length < 1) {
        setIsErrorFieldPracticeLocation1(true)
        setErrorMessageFieldPracticeLocation1("Tipe Praktek wajib diisi")
      }
      else {
        setIsErrorFieldPracticeLocation1(false)
        setErrorMessageFieldPracticeLocation1("");
      }
    }

    else if (position == 2) {
      setPracticeLocation_2(text)
      if (text.length < 1) {
        setIsErrorFieldPracticeLocation2(true)
        setErrorMessageFieldPracticeLocation2("Tipe Praktek wajib diisi")
      }
      else {
        setIsErrorFieldPracticeLocation2(false)
        setErrorMessageFieldPracticeLocation2("");
      }
    }
    else if (position == 3) {
      setPracticeLocation_3(text)
      if (text.length < 1) {
        setIsErrorFieldPracticeLocation3(true)
        setErrorMessageFieldPracticeLocation3("Tipe Praktek wajib diisi")
      }
      else {
        setIsErrorFieldPracticeLocation3(false)
        setErrorMessageFieldPracticeLocation3("");
      }
    }
    setIsThereChanges(true)
  }

  const onChangeTextEducation = (text, position) => {
    if (position == 1) {
      setEducation_1(text)
    }
    else if (position == 2) {
      setEducation_2(text)
    }
    else if (position == 3) {
      setEducation_3(text)
    }
    setIsThereChanges(true)
  }
  return (
    <Container>
      {renderBottomSheet()}
      <Loader visible={isShowLoader} />
      <HeaderToolbar onPress={handleBackButton} title="Ubah Profil" />
      <DateTimePicker
        isVisible={isShowDatePicker}
        onConfirm={handleDatePicked}
        onCancel={hideDatePicker}
        maximumDate={moment().toDate()}
        date={moment(bornDate, 'YYYY-MM-DD').toDate()}
      />
      <Content enableResetScrollToCoords={false}
        ref={contentRef}
      //ref={contentRef}
      >
        <View style={styles.container}>
          <InputField
            label="Tanggal Lahir"
            placeholder="Pilih Tanggal Lahir"
            value={shownBornDate}
            maxLength={200}
            isEditable={false}
            typeIconRight="arrow"
            onPress={() => setIsShowDatePicker(true)}
            isError={isErrorFieldBornDate}
            messageError={errorMessageFieldBornDate}
          />
          <View onLayout={onLayoutPhoneNumber}>
            <InputField
              label="No Handphone"
              placeholder="081XXXXXXXXX"
              onChangeText={(text) => onChangeTextPhone(removeEmojis(text))}
              value={phone}
              maxLength={200}
              keyboardType={"number-pad"}
              isError={isErrorFieldPhone}
              messageError={errorMessageFieldPhone}
            />
          </View>
          <InputField
            label="Email"
            placeholder="Contoh@email.com"
            value={params.email}
            maxLength={200}
            isEditable={false}
          />

          <View onLayout={onLayoutDomicileCity}>
            <InputField
              label="Kota Domisili"
              placeholder=""
              typeIconRight="arrow"
              onPress={() => doChooseCity()}
              value={
                _.isEmpty(domicileCity) ? "Pilih Kota Domisili Anda" : domicileCity
              }
              maxLength={200}
              isError={isErrorFieldDomicileCity}
              messageError={errorMessageFieldDomicileCity}
            />
          </View>
          <Text SubtitleMedium style={styles.label}>
            Status Pegawai
          </Text>
          <SelectionButton
            data={selectionButtons}
            onPress={(data, i) => {
              console.log(data);
              setKeyStatusPegawai(data.key);
              setIsThereChanges(true)
            }}
            keySelected={keyStatusPegawai}
          />
          <View style={[styles.lineDivider]} />

          <View onLayout={onLayoutNonSpecialist}>
            <InputField
              label="Non Spesialis"
              placeholder="Dokter Umum"
              maxLength={200}
              isEditable={false}
              typeIconRight="arrow"
              value={_.isEmpty(nonSpecialization) ? "Dokter Umum" : nonSpecialization[0].description}
              onPress={() => onPressSelectNonSpecialist()}
              size="medium"
            />
          </View>


          {specialization.map((value, i) => {
            return (
              <View
                key={value.id}
              >
                <InputField
                  label={`Spesialis ${i + 1}`}
                  placeholder="Pilih Spesialisasi"
                  maxLength={200}
                  isEditable={false}
                  value={value.description}
                  subLabel={i == 0 ? "Utama" : undefined}
                  rightMenu="moreVertical"
                  typeIconRight="arrow"
                  onPressRightMenu={() =>
                    onPressRightMenuDotVerticalSpecialist(value, i + 1)
                  }
                  onPress={() => onPressChangeSpecialization()}
                  size="medium"
                  isError={i > 1 ? isErrorFieldSpecialist : undefined}
                  messageError={i > 1 ? errorMessageFieldSpecialist : undefined}
                  subData={value.subspecialist}
                />
              </View>
            );
          })}

          {specialization.length > 0 && specialization.length < 2 && (
            <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => onPressSelectSpecialist()}>
              <Text textButtonMediumBlue>TAMBAHKAN SPESIALISASI</Text>
            </TouchableOpacity>
          )}

          {_.isEmpty(specialization) && (
            <View onLayout={onLayoutSingleSpecialist}>
              <InputField
                label="Spesialis 1"
                placeholder="Pilih Spesialisasi"
                maxLength={200}
                isEditable={false}
                typeIconRight="arrow"
                value="Pilih Spesialisasi"
                onPress={() => onPressSelectSpecialist()}
                size="medium"
                isError={isErrorFieldSpecialist}
                messageError={errorMessageFieldSpecialist}
              />
            </View>
          )}
          <View onLayout={onLayoutPeminatanStudi} style={[styles.lineDivider, { marginTop: specialization.length == 0 || specialization.length > 1 ? 0 : 20 }]} />

          {peminatanStudi.map((value, i) => {
            return (
              <InputField
                key={i}
                label={i == 0 ? "Peminatan Studi" : undefined}
                placeholder="Pilih Peminatan Studi"
                maxLength={200}
                isEditable={false}
                typeIconRight="close"
                value={value.description}
                onPressIconRight={() => onPressRemovePeminatanStudi(i)}
                size="medium"
                isError={i > 2 ? isErrorFieldPeminatanStudi : undefined}
                messageError={i > 2 ? errorMessageFieldPeminatanStudi : undefined}
              />
            );
          })}

          {_.isEmpty(peminatanStudi) && (
            <View>
              <InputField
                label="Peminatan Studi"
                placeholder="Pilih Peminatan Studi"
                maxLength={200}
                isEditable={false}
                typeIconRight="arrow"
                value="Pilih Peminatan Studi"
                onPress={() => gotoSelectSubscribtions()}
                size="medium"
                isError={isErrorFieldPeminatanStudi}
                messageError={errorMessageFieldPeminatanStudi}
              />
            </View>

          )}
          {peminatanStudi.length > 0 && peminatanStudi.length < 3 && (
            <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => gotoSelectSubscribtions()}>
              <Text textButtonMediumBlue>TAMBAHKAN PEMINATAN STUDI</Text>
            </TouchableOpacity>
          )}


          <View style={[styles.lineDivider, { marginTop: peminatanStudi.length == 0 || peminatanStudi.length > 2 ? 0 : 20 }]} />

          <View onLayout={onLayoutTypePracticeLocation1} >
            <InputField
              label={`Lokasi Praktek 1`}
              placeholder="Pilih Tipe Lokasi Praktek"
              maxLength={200}
              isEditable={false}
              typeIconRight="arrow"
              value={_.isEmpty(typePracticeLocation_1) ? "Pilih Tipe Lokasi Praktek" : typePracticeLocation_1}
              onPress={() => onPressSelectTypePracticeLocation(1)}
              size="medium"
              subLabel={"Utama"}
              rightMenu="moreVertical"
              onPressRightMenu={() => onPressRightMenuDotVerticalPracticeLocation(1)}
              isError={isErrorFieldTypePracticeLocation1}
              messageError={errorMessageFieldTypePracticeLocation1}
            />
          </View>

          <InputField
            placeholder="Nama Rumah Sakit/Institusi/Klinik"
            value={practiceLocation_1}
            onChangeText={(text) => onChangeTextPracticeLocation(removeEmojis(text), 1)}
            maxLength={200}
            isError={isErrorFieldPracticeLocation1}
            messageError={errorMessageFieldPracticeLocation1}
          />

          {countPracticeLocation > 1 && (
            <View onLayout={onLayoutTypePracticeLocation2}>
              <InputField
                label={`Lokasi Praktek 2`}
                placeholder="Pilih Tipe Lokasi Praktek"
                maxLength={200}
                isEditable={false}
                typeIconRight="arrow"
                value={_.isEmpty(typePracticeLocation_2) ? "Pilih Tipe Lokasi Praktek" : typePracticeLocation_2}
                onPress={() => onPressSelectTypePracticeLocation(2)}
                size="medium"
                rightMenu="moreVertical"
                onPressRightMenu={() => onPressRightMenuDotVerticalPracticeLocation(2)}
                isError={isErrorFieldTypePracticeLocation2}
                messageError={errorMessageFieldTypePracticeLocation2}
              />
              <InputField
                placeholder="Nama Rumah Sakit/Institusi/Klinik"
                value={practiceLocation_2}
                onChangeText={(text) => onChangeTextPracticeLocation(removeEmojis(text), 2)}
                maxLength={200}
                isError={isErrorFieldPracticeLocation2}
                messageError={errorMessageFieldPracticeLocation2}
              />
            </View>
          )}

          {countPracticeLocation > 2 && (
            <View onLayout={onLayoutTypePracticeLocation3}>
              <InputField
                label={`Lokasi Praktek 3`}
                placeholder="Pilih Tipe Lokasi Praktek"
                maxLength={200}
                isEditable={false}
                typeIconRight="arrow"
                value={_.isEmpty(typePracticeLocation_3) ? "Pilih Tipe Lokasi Praktek" : typePracticeLocation_3}
                onPress={() => onPressSelectTypePracticeLocation(3)}
                size="medium"
                rightMenu="moreVertical"
                onPressRightMenu={() => onPressRightMenuDotVerticalPracticeLocation(3)}
                isError={isErrorFieldTypePracticeLocation3}
                messageError={errorMessageFieldTypePracticeLocation3}
              />
              <InputField
                placeholder="Nama Rumah Sakit/Institusi/Klinik"
                value={practiceLocation_3}
                onChangeText={(text) => onChangeTextPracticeLocation(removeEmojis(text), 3)}
                maxLength={200}
                isError={isErrorFieldPracticeLocation3}
                messageError={errorMessageFieldPracticeLocation3}
              />
            </View>
          )}

          {countPracticeLocation < 3 && (
            <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => onPressAddPracticeLocation()}>
              <Text textButtonMediumBlue>TAMBAHKAN LOKASI PRAKTEK</Text>
            </TouchableOpacity>
          )}
          <View style={[styles.lineDivider, { marginTop: countPracticeLocation > 2 ? 0 : 20 }]} />

          <View onLayout={onLayoutEducation1}>
            <InputField
              label={"Pendidikan"}
              placeholder="Program Sarjana dan Nama Universitas"
              maxLength={200}
              value={education_1}
              typeIconRight={_.isEmpty(education_1) ? undefined : "close"}
              onPressIconRight={() => onPressRemoveEducation(1)}
              size="medium"
              onChangeText={(text) => onChangeTextEducation(removeEmojis(text), 1)}
            />
          </View>

          {countEducation > 1 && (
            <View onLayout={onLayoutEducation2}>
              <InputField
                placeholder="Program Sarjana dan Nama Universitas"
                maxLength={200}
                value={education_2}
                typeIconRight={_.isEmpty(education_2) ? undefined : "close"}
                onPressIconRight={() => onPressRemoveEducation(2)}
                size="medium"
                onChangeText={(text) => onChangeTextEducation(removeEmojis(text), 2)}
              />
            </View>
          )}


          {countEducation > 2 && (
            <View onLayout={onLayoutEducation3}>
              <InputField
                placeholder="Program Sarjana dan Nama Universitas"
                maxLength={200}
                value={education_3}
                typeIconRight={_.isEmpty(education_3) ? undefined : "close"}
                onPressIconRight={() => onPressRemoveEducation(3)}
                size="medium"
                onChangeText={(text) => onChangeTextEducation(removeEmojis(text), 3)}
              />
            </View>
          )}

          {countEducation < 3 && (
            <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => onPressAddEducation()}>
              <Text textButtonMediumBlue>TAMBAHKAN PENDIDIKAN</Text>
            </TouchableOpacity>
          )}

          <View style={[styles.lineDivider, { marginTop: countEducation > 2 ? 0 : 20 }]} />

          <Button text="SIMPAN" onPress={() => onPressSaveButton()} />
        </View>
      </Content>
    </Container>
  );
};

export default ChangeDetailProfile;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 40,
  },
  label: {
    marginBottom: 12,
  },
  lineDivider: {
    height: 8,
    backgroundColor: "#000000",
    opacity: 0.12,
    marginHorizontal: -16,
    marginBottom: 20,
    marginTop: 20,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },

  textButtonBottomsheetSpecialist: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    lineHeight: 24,
    letterSpacing: 0.15,
  },

  buttonBottomsheetSpecialistWrapper: {
    height: 56,
    justifyContent: "center",
  },
  titleBottomsheetSpecialist: {
    fontFamily: "Roboto-Medium",
    fontSize: 20,
    color: "#000000",
    lineHeight: 24,
    letterSpacing: 0.15,
    marginBottom: 16,
    marginTop: 20,
  },

  buttonCancelBottomsheetSpecialist: {
    marginTop: 20,
  },
});
