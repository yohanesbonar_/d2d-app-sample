import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
  Icon,
} from "native-base";
import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Animated,
  Alert,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import {
  IconBack,
  IconJurnal,
  IconRecordedWebinar,
  IconUpcomingWebinar,
  IconEventSearch,
  IconNotFound,
  IconCmeSearch,
  IconJobSearch,
  IconAtozSearch,
  IconFilterSearch,
  IconCalendar,
  IconLiveNowWebinar,
} from "../../assets";
import _, { initial } from "lodash";
import { CardItemSearch, SearchDataNotFound } from "../../components/molecules";
import SearchBarField from "../../components/molecules/SearchBarField";
import {
  getFeedsDataWebinarHomepageWithPage,
  getSearch,
} from "../../utils/network/";
import platform from "../../../theme/variables/platform";
import { HistorySearch, Loader } from "../../../app/components";
import moment from "moment";
import {
  isStarted,
  checkNpaIdiEmpty,
  STORAGE_TABLE_NAME,
  saveHistorySearch,
  removeHistorySearch,
  EnumTypeHistorySearch,
} from "../../../app/libs/Common";
import AsyncStorage from "@react-native-community/async-storage";
import Orientation from "react-native-orientation";
import { sendTopicWebinarAttendee, useDebouncedEffect } from "../../utils";
import { ArrowBackButton } from "../../components/atoms";

const SearchAllByKonten = ({ navigation }) => {
  const params = navigation.state.params;
  const [searchValue, setSearchValue] = useState("");
  const [searchCategory, setSearchCategory] = useState(
    _.isEmpty(params.data.category) ? "" : params.data.category
  );
  const [dataList, setDataList] = useState([]);
  const [isEmptyData, setIsEmptyData] = useState(false);

  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isFocusSearch, setIsFocusSearch] = useState(false);

  const [serverDate, setServerDate] = useState("");

  const [isRefresh, setIsRefresh] = useState(false);
  const [page, setPage] = useState(1);

  const [triggerChange, setTriggerChange] = useState(false);
  const [isTriggerBookmark, setIsTriggerBookmark] = useState(false);

  const [historySearchList, setHistorySearchList] = useState([]);

  const [
    onEndReachedCalledDuringMomentum,
    setOnEndReachedCalledDuringMomentum,
  ] = useState(true);
  useEffect(() => {
    initData();
    Orientation.lockToPortrait();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  useEffect(() => {
    if (isTriggerBookmark) {
      BackHandler.addEventListener("hardwareBackPress", handleBackButton);
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
      };
    }
  }, [isTriggerBookmark]);

  const handleBackButton = () => {
    if (isFocusSearch) {
      setIsFocusSearch(false);
      inputSearchRef.current.blur();
    } else {
      navigation.goBack();
      if (params.from == "home" || params.from == "SearchKonten") {
        console.log("isTriggerBookmark: ", isTriggerBookmark);
        if (isTriggerBookmark) {
          if (navigation.state.params.triggerBookmark != null) {
            navigation.state.params.triggerBookmark();
          } else {
            navigation.state.params.onRefreshDataSearchKonten();
          }
        }
      } else {
        navigation.state.params.onRefreshDataSearchKonten();
      }
      return true;
    }
  };

  const inputSearchRef = useRef(null);

  useDebouncedEffect(
    () => {
      console.log("useDebouncedEffect", searchValue); // debounced 1sec
      // call search api ...
      if (searchValue.trim().length >= 3) {
        setIsDataNotFound(false);
        getData(searchValue);
      } else if (
        searchValue.trim().length < 3 &&
        searchValue.trim().length > 0
      ) {
        Toast.show({
          text: "Pencarian harus spesifik min. 3 karakter",
          position: "top",
          duration: 2000,
        });
        // setSearchValue("");
        setIsDataNotFound(false);
      }
    },
    1000,
    [searchValue]
  );

  useEffect(() => {
    if (isRefresh) {
      getData(searchValue);
    }
  }, [isRefresh]);

  const initData = async () => {
    if (params.data.keyword != searchValue) {
      setSearchValue(params.data.keyword);
    }
    if (params.from == "home") {
      setIsDataNotFound(false);
      getData(searchValue);
    }
    loadHistorySearch(false);
  };

  const getData = (inputValue) => {
    inputSearchRef.current.blur();
    setIsRefresh(false);
    params.from == "home" &&
    (searchCategory == "UpcomingWebinar" ||
      searchCategory == "RecordedWebinar" ||
      searchCategory == "LiveWebinar")
      ? getDataWebinarHomePage(inputValue)
      : getDataFromAPI(inputValue);
  };

  const getDataWebinarHomePage = async (inputValue) => {
    setIsFailed(false);
    setIsFetching(true);
    setIsRefresh(false);

    try {
      let params = {
        limit: 10,
        page: page,
        keyword: inputValue,
        type:
          searchCategory == "LiveWebinar"
            ? "live"
            : searchCategory == "RecordedWebinar"
            ? "recorded"
            : "upcoming",
      };
      let response = await getFeedsDataWebinarHomepageWithPage(params);
      console.log("response SEARCH getDataWebinarHomePage: ", response);
      if (response.acknowledge) {
        var result = {
          upcoming_webinar: [],
          live_webinar: [],
          webinar: [],
        };
        if (searchCategory == "LiveWebinar") {
          result.live_webinar =
            !_.isEmpty(response.result.result.liveWebinar) &&
            response.result.result.liveWebinar != null
              ? response.result.result.liveWebinar
              : [];
        } else if (searchCategory == "RecordedWebinar") {
          result.webinar =
            !_.isEmpty(response.result.result.recordedWebinar) &&
            response.result.result.recordedWebinar != null
              ? response.result.result.recordedWebinar
              : [];
        } else {
          result.upcoming_webinar =
            !_.isEmpty(response.result.result.upcomingWebinar) &&
            response.result.result.upcomingWebinar != null
              ? response.result.result.upcomingWebinar
              : [];
        }
        await setData(result);

        setServerDate(response.result.server_date);
        setIsFailed(false);
        setIsFetching(false);
        setPage(page + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const getDataFromAPI = async (inputValue, limit = 10) => {
    console.log("PAGE load LOG", page);
    setIsFailed(false);
    setIsFetching(true);
    setIsRefresh(false);

    try {
      if (
        searchCategory == "UpcomingWebinar" ||
        searchCategory == "RecordedWebinar" ||
        searchCategory == "LiveWebinar"
      ) {
        response = await getSearch(
          page,
          limit,
          inputValue,
          1,
          1,
          0,
          0,
          0,
          0,
          0,
          0,
          1
        );
      } else if (searchCategory == "Event") {
        response = await getSearch(
          page,
          limit,
          inputValue,
          0,
          0,
          1,
          0,
          0,
          0,
          0,
          0,
          0
        );
      } else if (searchCategory == "Journal") {
        response = await getSearch(
          page,
          limit,
          inputValue,
          0,
          0,
          0,
          1,
          0,
          0,
          0,
          0,
          0
        );
      } else if (searchCategory == "Guideline") {
        response = await getSearch(
          page,
          limit,
          inputValue,
          0,
          0,
          0,
          0,
          1,
          0,
          0,
          0,
          0
        );
      } else if (searchCategory == "Cme") {
        response = await getSearch(
          page,
          limit,
          inputValue,
          0,
          0,
          0,
          0,
          0,
          1,
          0,
          0,
          0
        );
      } else if (searchCategory == "Job") {
        response = await getSearch(
          page,
          limit,
          inputValue,
          0,
          0,
          0,
          0,
          0,
          0,
          1,
          0,
          0
        );
      } else if (searchCategory == "ObatAtoz") {
        response = await getSearch(
          page,
          limit,
          inputValue,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          1,
          0
        );
      } else {
        response = await getSearch(
          page,
          limit,
          inputValue,
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          1
        );
      }
      console.log("response SEARCH getDataFromAPI: ", response);
      if (response.acknowledge) {
        let result = response.result.result;
        await setData(result);

        setServerDate(response.result.server_date);
        setIsFailed(false);
        setIsFetching(false);
        setPage(page + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const doShownDataWebinar = async (webinarList) => {
    let result = [];
    console.log("doShownData webinarList: ", webinarList);

    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    if (webinarList != null && webinarList.length > 0) {
      webinarList.forEach((element) => {
        if (element.exclusive == 1) {
          if (
            dataProfile != null &&
            dataProfile.webinar != null &&
            dataProfile.webinar.length > 0
          ) {
            let isShow = false;
            dataProfile.webinar.forEach((slug_hash) => {
              sendTopicWebinarAttendee(slug_hash);
              if (slug_hash == element.slug_hash) {
                isShow = true;
              }
            });

            if (isShow) {
              result.push(element);
            }
          }
        } else {
          result.push(element);
        }
      });
      console.log("webinar list", webinarList);
    }

    console.log("result", result);

    return result;
  };

  const setData = async (result) => {
    if (searchCategory == "UpcomingWebinar") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.upcoming_webinar)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          let resultAfterFilter = await doShownDataWebinar(
            result.upcoming_webinar
          );
          setDataList(
            page == 1
              ? [...resultAfterFilter]
              : [...dataList, ...resultAfterFilter]
          );
          setIsEmptyData(
            resultAfterFilter != null && resultAfterFilter.length > 0
              ? false
              : true
          );

          setIsDataNotFound(
            resultAfterFilter != null && resultAfterFilter.length > 0
              ? false
              : true
          );
        }
      } else {
        var resultAfterFilter = [];
        if (params.from != "home") {
          resultAfterFilter = await doShownDataWebinar(result.upcoming_webinar);
        } else {
          resultAfterFilter = result.upcoming_webinar;
        }

        setDataList(
          page == 1
            ? [...resultAfterFilter]
            : [...dataList, ...resultAfterFilter]
        );
        setIsEmptyData(
          resultAfterFilter != null && resultAfterFilter.length > 0
            ? false
            : true
        );
      }
    } else if (searchCategory == "LiveWebinar") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.live_webinar)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          let resultAfterFilter = await doShownDataWebinar(result.live_webinar);
          setDataList(
            page == 1
              ? [...resultAfterFilter]
              : [...dataList, ...resultAfterFilter]
          );
          setIsEmptyData(
            resultAfterFilter != null && resultAfterFilter.length > 0
              ? false
              : true
          );
          setIsDataNotFound(
            resultAfterFilter != null && resultAfterFilter.length > 0
              ? false
              : true
          );
        }
      } else {
        var resultAfterFilter = [];
        if (params.from != "home") {
          resultAfterFilter = await doShownDataWebinar(result.live_webinar);
        } else {
          resultAfterFilter = result.live_webinar;
        }

        setDataList(
          page == 1
            ? [...resultAfterFilter]
            : [...dataList, ...resultAfterFilter]
        );
        setIsEmptyData(
          resultAfterFilter != null && resultAfterFilter.length > 0
            ? false
            : true
        );
      }
    } else if (searchCategory == "RecordedWebinar") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.webinar)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          let resultAfterFilter = await doShownDataWebinar(result.webinar);
          setDataList(
            page == 1
              ? [...resultAfterFilter]
              : [...dataList, ...resultAfterFilter]
          );
          setIsEmptyData(
            resultAfterFilter != null && resultAfterFilter.length > 0
              ? false
              : true
          );
          setIsDataNotFound(
            resultAfterFilter != null && resultAfterFilter.length > 0
              ? false
              : true
          );
        }
      } else {
        var resultAfterFilter = [];
        if (params.from != "home") {
          resultAfterFilter = await doShownDataWebinar(result.webinar);
        } else {
          resultAfterFilter = result.webinar;
        }
        setDataList(
          page == 1
            ? [...resultAfterFilter]
            : [...dataList, ...resultAfterFilter]
        );
        setIsEmptyData(
          resultAfterFilter != null && resultAfterFilter.length > 0
            ? false
            : true
        );
      }
    } else if (searchCategory == "Jurnal") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.journal)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          setDataList(
            page == 1 ? [...result.journal] : [...dataList, ...result.journal]
          );
          setIsEmptyData(
            result.journal != null && result.journal.length > 0 ? false : true
          );
        }
      } else {
        setDataList(
          page == 1 ? [...result.journal] : [...dataList, ...result.journal]
        );
        setIsEmptyData(
          result.journal != null && result.journal.length > 0 ? false : true
        );
      }
    } else if (searchCategory == "Event") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.event)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          setDataList(
            page == 1 ? [...result.event] : [...dataList, ...result.event]
          );
          setIsEmptyData(
            result.event != null && result.event.length > 0 ? false : true
          );
        }
      } else {
        setDataList(
          page == 1 ? [...result.event] : [...dataList, ...result.event]
        );
        setIsEmptyData(
          result.event != null && result.event.length > 0 ? false : true
        );
      }
    } else if (searchCategory == "Guideline") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.guideline)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          setDataList(
            page == 1
              ? [...result.guideline]
              : [...dataList, ...result.guideline]
          );
          setIsEmptyData(
            result.guideline != null && result.guideline.length > 0
              ? false
              : true
          );
        }
      } else {
        setDataList(
          page == 1 ? [...result.guideline] : [...dataList, ...result.guideline]
        );
        setIsEmptyData(
          result.guideline != null && result.guideline.length > 0 ? false : true
        );
      }
    } else if (searchCategory == "Cme") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.cme)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          setDataList(
            page == 1 ? [...result.cme] : [...dataList, ...result.cme]
          );
          setIsEmptyData(
            result.cme != null && result.cme.length > 0 ? false : true
          );
        }
      } else {
        setDataList(page == 1 ? [...result.cme] : [...dataList, ...result.cme]);
        setIsEmptyData(
          result.cme != null && result.cme.length > 0 ? false : true
        );
      }
    } else if (searchCategory == "Job") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.job)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          setDataList(
            page == 1 ? [...result.job] : [...dataList, ...result.job]
          );
          setIsEmptyData(
            result.job != null && result.job.length > 0 ? false : true
          );
        }
      } else {
        setDataList(page == 1 ? [...result.job] : [...dataList, ...result.job]);
        setIsEmptyData(
          result.job != null && result.job.length > 0 ? false : true
        );
      }
    } else if (searchCategory == "ObatAtoz") {
      if (_.isEmpty(dataList)) {
        if (_.isEmpty(result.obatatoz)) {
          setIsDataNotFound(true);
          setIsEmptyData(true);
        } else {
          setDataList(
            page == 1 ? [...result.obatatoz] : [...dataList, ...result.obatatoz]
          );
          setIsEmptyData(
            result.obatatoz != null && result.obatatoz.length > 0 ? false : true
          );
        }
      } else {
        setDataList(
          page == 1 ? [...result.obatatoz] : [...dataList, ...result.obatatoz]
        );
        setIsEmptyData(
          result.obatatoz != null && result.obatatoz.length > 0 ? false : true
        );
      }
    }
  };

  const pressBookmark = () => {
    console.log("onPressBookmark");
    setIsTriggerBookmark(true);
  };

  const _renderData = ({ item }) => {
    return (
      <CardItemSearch
        type={searchCategory}
        data={item}
        onPress={() => {
          goToScreenBaseOnCategory(item, searchCategory);
        }}
        onPressBookmark={(isBookmark) => {
          pressBookmark();
          item.flag_bookmark = isBookmark;
        }}
        serverDate={serverDate}
      />
    );
  };

  const goToScreenBaseOnCategory = async (data, type) => {
    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(EnumTypeHistorySearch.ALL_CONTENT, searchValue);
    }
    await loadHistorySearch(false);
    if (
      type == "UpcomingWebinar" ||
      type == "RecordedWebinar" ||
      type == "LiveWebinar"
    ) {
      goToWebinarDetail(data);
    } else if (type == "Jurnal") {
      goToJournalDetail(data);
    } else if (type == "Event") {
      goToEventDetail(data);
    } else if (type == "Cme") {
      goToCMEDetail(data);
    } else if (type == "Guideline") {
      goToPDFView(data);
    } else if (type == "Job") {
    } else if (type == "ObatAtoz") {
      goToObatAtoz(data);
    }
  };

  const goToJournalDetail = (data) => {
    navigation.navigate("LearningJournalDetail", {
      data: data,
      fromDeeplink: true,
      onRefreshDataSearchKonten: onRefreshdata,
      from: "SearchAllByKonten",
    });
  };

  const goToWebinarDetail = async (data) => {
    let tempData = {
      isPaid: data.exclusive == 1 || data.is_paid == true ? true : false,
      from: "Feeds",
      ...data,
    };

    let dateShowTNC = moment(data.start_date)
      .add(-1, "hours")
      .format("YYYY-MM-DD HH:mm:ss");

    // let isStartShowTNC = isStarted(data.server_date, dateShowTNC)
    let isStartShowTNC = isStarted(serverDate, dateShowTNC);
    let routeNameDestination =
      data.has_tnc == true && isStartShowTNC == true
        ? "WebinarTNC"
        : "WebinarVideo";
    navigation.navigate({
      routeName: routeNameDestination,
      params: tempData,
      key: `webinar-${data.id}`,
    });
  };

  const goToPDFView = async (data) => {
    const PdfView = { type: "Navigate", routeName: "PdfView", params: data };
    let availableAttachment = data.attachment != null ? true : false;

    availableAttachment === true
      ? navigation.navigate(PdfView)
      : showErrorMessage();
  };

  const showErrorMessage = () => {
    Toast.show({
      text: "Data not found",
      position: "bottom",
      duration: 3000,
      type: "danger",
    });
  };

  const goToEventDetail = async (data) => {
    // data.flag_bookmark = this.state.bookmark;
    // data.flag_reserve = this.state.reserved;
    data.paid = data.is_paid;
    navigation.navigate({
      routeName: "EventDetail",
      params: {
        // updateDataBookmark: this.doUpdateDataBookmark,
        data: data,
        // joined: this.state.reserved,
        // //reserve
        // manualUpdateReserve: this.props.manualUpdateReserve == null ? false : this.props.manualUpdateReserve,
        // updateDataReserve: this.doUpdateDataReserved,
        from: "SearchAllByKonten",
        onRefreshDataSearchKonten: onRefreshdata,
        triggerRefreshHome: triggerRefreshHome,
      },
      key: `event-${data.id}`,
    });
  };

  const triggerRefreshHome = () => {
    setIsTriggerBookmark(true);
  };

  const goToCMEDetail = async (data) => {
    console.log("goToCMEDetail");
    isDone = data.is_done == true || data.is_done == 1 ? true : false;
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataSubmitCmeQuiz = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ
    );
    if (checkNpaIdiEmpty(profile) && !_.isEmpty(dataSubmitCmeQuiz) && !isDone) {
      //if (checkNpaIdiEmpty(profile) && !isDone) {
      showAlertFillNpaIDI(data);
    } else {
      gotoMedicinusDetail(isDone, data);
    }
  };

  const showAlertFillNpaIDI = (data) => {
    Alert.alert(
      "Reminder",
      "Please input your Medical ID first before you attempt your next CME Quiz",
      [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: "OK",
          onPress: () => gotoProfile(data),
        },
      ],
      { cancelable: true }
    );
  };

  gotoProfile = (data) => {
    let from = "SearchKonten";

    let params = {
      from: from,
      ...data,
      backFromCertificate: from,
      resubmitQuiz: true,
      isFromAlertSearchPage: true,
      onRefreshDataSearchKonten: onRefreshdata,
    };

    navigation.navigate({
      routeName: "ChangeProfile",
      params: params,
    });
  };

  const gotoMedicinusDetail = async (isDone, data) => {
    console.log("gotoMedicinusDetail data", data);
    let from = "SearchKonten";
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    // this.sendAdjust(from.DETAIL)
    // AdjustTracker(AdjustTrackerConfig.CME_SKP_Quiz);
    let paramCertificate = {
      title: data.title,
      certificate: data.certificate,
      isRead: data.status != "invalid" ? true : false,
      // isFromHistory: true,
      from: from,
      isManualSkp: data.type == "offline" ? true : false,
      typeCertificate: data.type,
      //isCertificateAvailable: true,
    };

    if (data.type == "offline") {
      gotoCertificate(paramCertificate, data);
    } else {
      if (isDone) {
        let type = data.type != null ? data.type : "Cme";

        if (type == "event" || type == "webinar") {
          gotoCertificate(paramCertificate, data);
        } else {
          if (checkNpaIdiEmpty(profile) == false) {
            gotoCertificate(paramCertificate, data);
          } else if (checkNpaIdiEmpty(profile) == true) {
            let params = {
              from: from,
              ...data,
              backFromCertificate: from,
              onRefreshDataSearchKonten: onRefreshdata,
            };

            navigation.navigate({
              routeName: "CmeQuiz",
              params: params,
              key: "goto",
            });
          }
        }
      } else {
        console.log("TO THIS OPTION", data);
        let params = {
          from: from,
          ...data,
          backFromCertificate: from,
          onRefreshDataSearchKonten: onRefreshdata,
        };
        navigation.navigate({
          routeName: "CmeDetail",
          params: params,
          key: `webinar-${data.id}`,
        });
      }
    }
  };

  const gotoCertificate = (paramCertificate, data) => {
    navigation.navigate({
      routeName: "CmeCertificate",
      params: paramCertificate,
      key: `webinar-${data.id}`,
    });
  };

  const goToObatAtoz = (data) => {
    navigation.navigate("DetailAtoZ", {
      dataObat: data,
    });
  };

  const renderDataNotFound = () => {
    return (
      <SearchDataNotFound
        title="Pencarian Tidak Ditemukan"
        subtitle="Maaf kami tidak bisa menemukan hasil yang Anda cari. Coba gunakan kata yang lain."
      />
    );
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailed == true && page > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetching} transparent />;
  };

  const onRefreshdata = () => {
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    setDataList([]);
    setPage(1);
    setIsRefresh(true);
  };

  const handleLoadMore = async () => {
    if (isEmptyData == true || isFetching == true) {
      return;
    }
    await getData(searchValue);
  };

  const clearResultSearch = () => {
    setDataList([]);
    setIsEmptyData(true);
  };

  const onChange = (text) => {
    setDataList([]);
    setIsEmptyData(true);
    setPage(1);
    if (text.length > 0) {
      setSearchValue(text);
    } else {
      setIsDataNotFound(false);
      if (_.isEmpty(historySearchList)) {
        setSearchValue(params.data.keyword);
      } else {
        setSearchValue("");
      }

      // setSearchValue("");
    }
  };

  const loadHistorySearch = async (bool) => {
    let dataHistorySearch = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CONTENT
    );
    dataHistorySearch =
      JSON.parse(dataHistorySearch) != null
        ? JSON.parse(dataHistorySearch).historySearch
        : [];
    if (bool && _.isEmpty(dataHistorySearch)) {
      setSearchValue(params.data.keyword);
    }
    setHistorySearchList(dataHistorySearch);
  };

  const onFocusTextSearch = () => {
    setIsFocusSearch(true);
  };

  const onPressRemoveItemHistory = async (item) => {
    await removeHistorySearch(EnumTypeHistorySearch.ALL_CONTENT, item.index);
    await loadHistorySearch(true);
  };

  const onPressButtonClose = () => {
    // if (value == true && _.isEmpty(historySearchList)) {
    //   // await loadHistorySearch();
    //   setSearchValue(params.data.keyword);
    // }
    setIsDataNotFound(false);
    setSearchValue("");
    inputSearchRef.current.focus();
    loadHistorySearch();
  };

  const renderHeaderList = () => {
    return (
      <View>
        {searchCategory == "UpcomingWebinar" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconUpcomingWebinar style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Upcoming Webinar</Text>
            </View>
          </View>
        )}

        {searchCategory == "LiveWebinar" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconLiveNowWebinar style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Semua Live Webinar</Text>
            </View>
          </View>
        )}
        {searchCategory == "RecordedWebinar" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconRecordedWebinar style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Rekaman Webinar</Text>
            </View>
          </View>
        )}
        {searchCategory == "Jurnal" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconJurnal style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Jurnal</Text>
            </View>
          </View>
        )}
        {searchCategory == "Event" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconEventSearch style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Event</Text>
            </View>
          </View>
        )}
        {searchCategory == "Guideline" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconJurnal style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Guideline</Text>
            </View>
          </View>
        )}
        {searchCategory == "Cme" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconCmeSearch style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>CME</Text>
            </View>
          </View>
        )}
        {searchCategory == "Job" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconJobSearch style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>JOB</Text>
            </View>
          </View>
        )}
        {searchCategory == "ObatAtoz" && (
          <View style={styles.containerGroup}>
            <View style={styles.containerTitle}>
              <IconAtozSearch style={{ marginRight: 8 }} />
              <Text style={styles.textGroup}>Obat A-Z</Text>
            </View>
          </View>
        )}
      </View>
    );
  };

  const renderContent = () => {
    if (isDataNotFound) {
      return (
        <View style={{ justifyContent: "center", flex: 1 }}>
          {renderDataNotFound()}
        </View>
      );
    } else if (
      isFocusSearch &&
      // _.isEmpty(dataList) &&
      (searchValue == "" ||
        (searchValue.trim().length > 0 && searchValue.trim().length <= 2)) &&
      historySearchList.length > 0 &&
      !isDataNotFound
    ) {
      return (
        <View style={{ marginTop: 4 }}>
          <HistorySearch
            onPressRemoveItem={(item) => onPressRemoveItemHistory(item)}
            onPressItem={(txt) => onChange(txt)}
            data={historySearchList}
          />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{
              paddingHorizontal: 16,
            }}
            data={dataList}
            onEndReachedThreshold={0.5}
            onEndReached={handleLoadMore}
            renderItem={_renderData}
            ListHeaderComponent={renderHeaderList()}
            ListFooterComponent={_renderItemFooter()}
            showsVerticalScrollIndicator={false}
            onRefresh={() => {
              onRefreshdata();
            }}
            refreshing={false}
          />
        </View>
      );
    }
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <SearchBarField
              type="searchKonten"
              textplaceholder={"Cari Konten D2D"}
              onChangeText={(text) => {
                onChange(text);
              }}
              onFocus={() => onFocusTextSearch()}
              onButtonClose={() => onPressButtonClose()}
              inputReff={inputSearchRef}
              inputText={searchValue}
              isCloseButton={searchValue.trim() == "" ? false : true}
              onPressCloseButton={() => onPressButtonClose()}
            />

            {/* <TouchableOpacity
              style={{ marginLeft: 12, marginRight: -16 }}
              onPress={() => goTo("CalendarWebinar")}
            >
              <IconCalendar />
            </TouchableOpacity> */}
          </Body>
        </Header>
      </SafeAreaView>
      {renderContent()}
    </Container>
  );
};

export default SearchAllByKonten;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  containerGroup: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginTop: 16,
    marginBottom: 20,
    justifyContent: "space-between",
  },
  textGroup: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  notFoundContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 4,
    backgroundColor: "#FFFFFF",
  },
  notFoundText: {
    fontFamily: "Roboto-Medium",
    fontSize: 20,
    color: "#000000",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 32,
    // textAlignVertical: "center",
  },
  anotherKeywordText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    // textAlignVertical: "center",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 8,
    marginRight: 16,
  },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  containerTitle: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 8 },
});
