import { Container, Content } from "native-base";
import React, { useEffect } from "react";
import {
  BackHandler,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import {
  ArrowCalendar,
  BottomSheet,
  Button,
  HeaderToolbar,
} from "../../components";
import { useState } from "react";
import { ThemeD2D } from "../../../app";
import _ from "lodash";
import { Modalize } from "react-native-modalize";
import {
  IconCalendarJobDetail,
  IconFacebookJobDetail,
  IconFilePdfWord,
  IconInstagramJobDetail,
  IconLocationJobDetail,
  IconTimeJobDetail,
  IconTwitterJobDetail,
} from "../../assets";

const JobTulisArtikel = ({ navigation }) => {
  const params = navigation.state.params.item;
  const [title, setTitle] = useState("");
  const [isRegister, setIsRegister] = useState(false);
  const [typeBottomSheet, setTypeBottomSheet] = useState("");
  useEffect(() => {
    initData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const initData = async () => {};

  const renderButtonAbsolute = () => {
    if (!isRegister) {
      return (
        <View style={styles.viewButtonAbsolute}>
          <Button
            text="SUBMIT ARTIKEL ANDA"
            type={"elevationButtonBlue"}
            onPress={() => {
              // setIsRegister(isRegister ? false : true);
              onOpenBottomsheet();
            }}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.viewButtonAbsolute}>
          <Button
            text="ARTIKEL ANDA TELAH TERSUBMIT"
            type={"elevationButtonGray"}
            onPress={() => {
              setIsRegister(false);
            }}
          />
        </View>
      );
    }
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={styles.handleStyleBottomSheet}
        adjustToContentHeight={true}
        closeOnOverlayTap={false}
        panGestureEnabled={false}
        ref={modalizeRef}
        modalStyle={styles.modalStyleBottomSheet}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        // onClose={() => onPressClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        {typeBottomSheet == "BottomSheetRegister" && (
          <BottomSheet
            onPressClose={() => onPressClose()}
            title="Terima Kasih Sudah Daftar"
            desc="Anda akan dihubungi admin secepatnya."
            wordingClose="OKE, SAMA-SAMA"
            type="BottomSheetRegister"
          />
        )}
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onPressClose = () => {
    onClose();
    setIsRegister(true);
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const onOpenBottomsheet = () => {
    setTypeBottomSheet("BottomSheetRegister");
    onOpen();
  };

  let uriCover = params.cover != null ? { uri: params.cover } : null;
  return (
    <Container>
      {renderBottomSheet()}
      <HeaderToolbar onPress={handleBackButton} title="Tulis Artikel" />
      <View style={styles.viewOuterContainer}>
        {renderButtonAbsolute()}
        <Content style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <ImageBackground
              style={styles.imageBackgroundContainer}
              source={uriCover}
              imageStyle={styles.imageStyleImageBackground}
              defaultSource={require("../../../app/assets/images/preloader.png")}
            />
            <View style={styles.mainContainer}>
              <Text style={styles.textTitle}>{params.title}</Text>
              <View style={styles.viewSmallText}>
                <IconCalendarJobDetail />
                <Text style={[styles.smallText, { marginLeft: 8 }]}>
                  10 Oktober 2020
                </Text>
              </View>
              <View style={styles.lineGrayContainer} />
              <View style={styles.viewTitleDesc}>
                <View style={styles.viewLeftTitleDesc} />
                <Text style={styles.textTitleDesc}>Upload Artikel</Text>
              </View>
              <TouchableOpacity style={styles.containerUploadFile}>
                <IconFilePdfWord />
                <Text style={styles.textFilename}>Buat_Artikel.docx</Text>
              </TouchableOpacity>
              <Text style={styles.textFormatFile}>
                Format harus berbentuk Word/Docx
              </Text>
              <View style={styles.lineGrayContainer} />
              <View style={styles.viewTitleDesc}>
                <View style={styles.viewLeftTitleDesc} />
                <Text style={styles.textTitleDesc}>Deskripsi Artikel</Text>
              </View>
              <Text style={styles.textDesc}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam
              </Text>
              <View style={styles.viewTitleDesc}>
                <View style={styles.viewLeftTitleDesc} />
                <Text style={styles.textTitleDesc}>Kualifikasi Penulis</Text>
              </View>
              <Text style={styles.textDesc}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam
              </Text>
              <View style={styles.viewTitleDesc}>
                <View style={styles.viewLeftTitleDesc} />
                <Text style={styles.textTitleDesc}>Benefit Jadi Penulis</Text>
              </View>
              <Text style={styles.textDesc}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam
              </Text>

              <View style={styles.viewTitleDesc}>
                <View style={styles.viewLeftTitleDesc} />
                <Text style={styles.textTitleDesc}>Ikuti Kami</Text>
              </View>
              <View style={styles.viewOptionIkutiKami}>
                <TouchableOpacity style={styles.viewPerOption}>
                  <IconInstagramJobDetail />
                  <Text style={styles.textOption}>Instagram</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewPerOption}>
                  <IconFacebookJobDetail />
                  <Text style={styles.textOption}>Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewPerOption}>
                  <IconTwitterJobDetail />
                  <Text style={styles.textOption}>Twitter</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Content>
      </View>
    </Container>
  );
};

export default JobTulisArtikel;

const styles = StyleSheet.create({
  imageBackgroundContainer: {
    aspectRatio: 16 / 9,
    justifyContent: "center",
    alignItems: "center",
  },
  mainContainer: { padding: 16, flex: 1, flexDirection: "column" },
  textTitle: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
  },
  smallText: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
  },
  viewSmallText: {
    flex: 1,
    marginTop: 12,
    flexDirection: "row",
    alignItems: "center",
  },
  lineGrayContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 8,
    borderWidth: 0.1,
    borderEndColor: "#EBEBEB",
    marginHorizontal: -16,
    marginVertical: 20,
  },
  textTitleDesc: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    paddingLeft: 8,
  },
  viewTitleDesc: {
    flex: 1,
    marginBottom: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  viewLeftTitleDesc: {
    backgroundColor: "#D01E53",
    height: 16,
    width: 4,
    borderWidth: 0.1,
    borderEndColor: "#D01E53",
    borderRadius: 3,
  },
  textDesc: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    marginBottom: 20,
  },
  viewButtonAbsolute: {
    marginTop: 16,
    marginBottom: 20,
    marginHorizontal: 16,
    elevation: 4,
  },
  viewOuterContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  imageStyleImageBackground: { borderRadius: 0 },
  viewOptionIkutiKami: {
    flex: 1,
    flexDirection: "row",
    alignContent: "center",
    justifyContent: "space-between",
    marginVertical: 16,
  },
  viewPerOption: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
  },
  textOption: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    color: "#000000",
    paddingTop: 16,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  modalStyleBottomSheet: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    paddingHorizontal: 16,
  },
  handleStyleBottomSheet: {
    backgroundColor: "transparent",
  },
  containerUploadFile: {
    height: 56,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#EBEBEB",
    borderRadius: 5,
  },
  textFilename: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  textFormatFile: {
    fontFamily: "Roboto-Regular",
    fontSize: 12,
    color: "#666666",
    textAlign: "left",
    marginBottom: -4,
    marginTop: 8,
  },
});
