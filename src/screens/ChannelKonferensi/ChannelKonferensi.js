import { Container, Content, Toast, Text } from "native-base";
import React, { useEffect } from "react";
import {
  BackHandler,
  Dimensions,
  SectionList,
  StyleSheet,
  View,
} from "react-native";
import {
  ArrowCalendar,
  BottomSheet,
  CardItemKonferensi,
  EmptyDataKonferensi,
  HeaderToolbar,
} from "../../components";
import { Calendar } from "react-native-calendars";
import {
  configLocaleCalendarsIndonesia,
  getMonthShortNameIndonesia,
  getAllSundaysDate,
  getDateFormatIndonesia,
  getCalendarConferenceByMonth,
  joinConference,
  registerConference,
  getTodayDate,
  getDeviceInfo,
} from "../../utils";
import { useState } from "react";
import { ThemeD2D } from "../../../theme";
import _ from "lodash";
import { Modalize } from "react-native-modalize";
import { Loader } from "../../../app/components";
import { IconAlertCompatibilityIOS } from "../../assets";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const ChannelKonferensi = ({ navigation }) => {
  const { params } = navigation.state;
  console.log(params);
  const [nextMonthName, setNextMonthName] = useState(
    getMonthShortNameIndonesia(new Date().getMonth() + 1)
  );
  const [previousMonthName, setPreviousMonthName] = useState(
    getMonthShortNameIndonesia(new Date().getMonth() - 1)
  );

  const [sbcChannel, setSbcChannel] = useState(params.sbcChannel);
  const [selectedDay, setSelectedDay] = useState("");
  const [currentDateInSelectedMonth, setCurrentDateInSelectedMonth] = useState(
    null
  );
  const [currentDate, setCurrentDate] = useState(getTodayDate());
  const [marked, setMarked] = useState({});
  const [isEmptyConferenceInDate, setIsEmptyConferenceInDate] = useState(false);
  const [dataConferencePerDate, setDataConferencePerDate] = useState([]);

  const [selectedMonth, setSelectedMonth] = useState(new Date().getMonth() + 1);
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [dataConference, setDataConference] = useState([]);
  const [heightHeader, setHeightHeader] = useState(0);
  const [positionHeader, setPositionHeader] = useState(0);
  const [heightCalendar, setHeightCalendar] = useState(0);

  const [xid, setXid] = useState(null);

  useEffect(() => {
    AdjustTracker(AdjustTrackerConfig.Forum_Konferensi_Start);
    configLocaleCalendarsIndonesia();
    onOpen();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  useEffect(() => {
    getDataCalendarConference();
  }, [selectedMonth]);

  useEffect(() => {
    setDataMarkedSunday(currentDateInSelectedMonth, selectedDay);
    if (!_.isEmpty(selectedDay)) {
      onPressSelectedDate(selectedDay);
    }
  }, [dataConference]);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const getDataCalendarConference = async () => {
    try {
      setIsShowLoader(true);
      let params = {
        by: "month",
        month: selectedMonth,
        year: selectedYear,
      };
      let response = await getCalendarConferenceByMonth(params, sbcChannel);
      console.log("response: ", response);
      if (response.header.message == "Success") {
        // let result = response.result;
        setIsShowLoader(false);
        setDataConference(response.data);
        //Toast.show({ text: response.message, position: 'bottom', duration: 3000 });
      } else {
        Toast.show({
          text: response.header.reason.id,
          position: "bottom",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "bottom",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const setDataMarkedSunday = (date, paramsSelectedDate) => {
    let datesMarked = {};
    // getAllSundaysDate(date).forEach((val) => {
    //   datesMarked[val] = {
    //     customStyles: {
    //       container: {
    //         backgroundColor: "transparent",
    //         // elevation: 2,
    //       },
    //       text: {
    //         color: ThemeD2D.brandPrimary,
    //       },
    //     },
    //   };
    // });

    dataConference.map((value) => {
      var newDate = new Date(value.date);

      datesMarked[value.date] = {
        marked: true,
        dotColor: "#0771CD",
        customStyles: {
          container: {
            backgroundColor: "transparent",
            // elevation: 2,
          },
          // text: {
          //   color:
          //     newDate.getDay() == 0
          //       ? ThemeD2D.brandPrimary
          //       : value.date == currentDate
          //       ? "#0771CD"
          //       : undefined,
          // },
        },
      };
    });

    if (!_.isEmpty(paramsSelectedDate)) {
      datesMarked[paramsSelectedDate] = {
        selected: true,
        selectedColor: "#0771CD",
      };
    }
    setMarked(datesMarked);
  };

  const setDataNameNextMonth = (month) => {
    if (month == 12) {
      month = 0;
    }
    setNextMonthName(getMonthShortNameIndonesia(month));
  };

  const setDataNamePreviousMonth = (month) => {
    if (month == 1) {
      month = 13;
    }
    setPreviousMonthName(getMonthShortNameIndonesia(month - 2));
  };

  const onPressButtonRightCardConference = (item) => {
    if (item.action.join === true) {
      if (ThemeD2D.platform == "ios") {
        let iosVersion = getDeviceInfo().getSystemVersion();
        let splitVersion = iosVersion.split(".");
        if (splitVersion[0] == 14) {
          if (splitVersion[1] >= 3) {
            doJoinConference(item);
          } else {
            setXid(item.xid);
            onOpenWarningIOS();
          }
        } else if (splitVersion[0] > 14) {
          doJoinConference(item);
        } else {
          setXid(item.xid);
          onOpenWarningIOS();
        }
      } else {
        doJoinConference(item);
      }
    } else if (item.action.register === true) {
      doRegisterConference(item);
    }
  };

  const doRegisterConference = async (item) => {
    try {
      setIsShowLoader(true);
      let params = {
        xid: item.xid,
      };
      let response = await registerConference(params, sbcChannel);
      console.log("response: ", response);
      if (response.header.message == "Success") {
        //setIsShowLoader(false);
        getDataCalendarConference();
        Toast.show({
          text: response.header.reason.id,
          position: "bottom",
          buttonText: "TUTUP",
          duration: 3000,
        });
      } else {
        Toast.show({
          text: response.header.reason.id,
          position: "bottom",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "bottom",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const doJoinConference = async (item) => {
    try {
      setIsShowLoader(true);
      let params = {
        xid: !_.isEmpty(item) ? item.xid : xid,
      };
      let response = await joinConference(params, sbcChannel);
      console.log("response: ", response);
      if (response.header.message == "Success") {
        setIsShowLoader(false);
        let params = response.data;

        navigation.navigate("ConferenceRoom", params);
      } else {
        Toast.show({
          text: response.header.reason.id,
          position: "bottom",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "bottom",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const modalizeRef = React.createRef(null);
  const modalizeWarningIOSRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const onOpenWarningIOS = () => {
    modalizeWarningIOSRef.current?.open();
  };

  const onCloseWarningIOS = () => {
    modalizeWarningIOSRef.current?.close();
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        ref={modalizeRef}
        withOverlay={false}
        withHandle={false}
        alwaysOpen={
          Dimensions.get("window").height - (heightCalendar + heightHeader)
        }
        modalHeight={
          Dimensions.get("window").height - (positionHeader + heightHeader)
        }
        HeaderComponent={<View style={styles.headerComponentBottomsheet} />}
      >
        <View>
          {_.isEmpty(selectedDay) && (
            <SectionList
              scrollEnabled={false}
              sections={dataConference}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item }) => (
                <View style={{ paddingHorizontal: 16 }}>
                  <CardItemKonferensi
                    title={item.title}
                    startDate={item.start_at}
                    textButton={item.current}
                    endDate={item.end_at}
                    onPressButtonRight={() => {
                      onPressButtonRightCardConference(item);
                      AdjustTracker(AdjustTrackerConfig.Forum_Konferensi_Pilih);
                    }}
                    isTextButtonDisable={
                      !item.action.register && !item.action.join
                    }
                  />
                </View>
              )}
              renderSectionHeader={({ section: { date } }) => (
                <Text style={styles.headerDateSection}>
                  {getDateFormatIndonesia(date)}
                </Text>
              )}
            />
          )}

          {(isEmptyConferenceInDate || _.isEmpty(dataConference)) && (
            <View style={{ marginHorizontal: 16 }}>
              <EmptyDataKonferensi
                title="Belum Ada Acara"
                subtitle="Selalu ada acara yang menarik disini. Coba pilih tanggal lain yang bertanda titik biru."
              />
            </View>
          )}

          {!_.isEmpty(selectedDay) && !isEmptyConferenceInDate && (
            <SectionList
              scrollEnabled={false}
              sections={dataConferencePerDate}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item }) => (
                <View style={{ paddingHorizontal: 16 }}>
                  <CardItemKonferensi
                    title={item.title}
                    startDate={item.start_at}
                    textButton={item.current}
                    endDate={item.end_at}
                    onPressButtonRight={() => {
                      onPressButtonRightCardConference(item);
                      AdjustTracker(AdjustTrackerConfig.Forum_Konferensi_Pilih);
                    }}
                    isTextButtonDisable={
                      !item.action.register && !item.action.join
                    }
                  />
                </View>
              )}
              renderSectionHeader={({ section: { date } }) => (
                <Text style={styles.headerDateSection}>
                  {getDateFormatIndonesia(date)}
                </Text>
              )}
            />
          )}
        </View>
      </Modalize>
    );
  };

  const renderBottomSheetWarningIOS = () => {
    return (
      <Modalize
        ref={modalizeWarningIOSRef}
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        modalStyle={styles.modalStyleBottomSheet}
        HeaderComponent={
          <View style={styles.headerComponentBottomsheetWarningIOS} />
        }
      >
        <BottomSheet
          onPressCancel={() => onCloseWarningIOS()}
          onPressOk={() => {
            onCloseWarningIOS();
            doJoinConference();
          }}
          title="Versi iOS Tidak Mendukung"
          desc="Anda harus memperbarui ke iOS 14.3 dan setelahnya untuk menggunakan fitur ini atau tekan lanjut dengan kondisi mikropon dan kamera mati."
          type="compatibility"
          wordingCancel="KEMBALI"
          wordingOk="LANJUT"
        />
      </Modalize>
    );
  };

  const onPressSelectedDate = (selectedDate) => {
    const dataPerDate = dataConference.filter(
      (data) => data.date == selectedDate
    );

    setDataConferencePerDate(dataPerDate);
    if (dataPerDate.length > 0) {
      setIsEmptyConferenceInDate(false);
    } else {
      setIsEmptyConferenceInDate(true);
    }
  };
  const onLayoutCalendar = ({
    nativeEvent: {
      layout: { x, y, width, height },
    },
  }) => {
    setHeightCalendar(height);
  };

  const onLayoutHeader = ({
    nativeEvent: {
      layout: { x, y, width, height },
    },
  }) => {
    setPositionHeader(y);
    setHeightHeader(height);
  };
  return (
    <Container>
      <Loader visible={isShowLoader} />

      {isShowLoader == false && renderBottomSheet()}
      {renderBottomSheetWarningIOS()}
      <View onLayout={onLayoutHeader}>
        <HeaderToolbar onPress={handleBackButton} title="Konferensi" />
      </View>

      <Content>
        <View onLayout={onLayoutCalendar}>
          <Calendar
            firstDay={1}
            // Specify style for calendar container element. Default = {}
            style={
              {
                // height: Dimensions.get("window").height / 2.5,
              }
            }
            // Specify theme properties to override specific styles for calendar parts. Default = {}
            theme={{
              backgroundColor: "#ffffff",
              calendarBackground: "#ffffff",
              textSectionTitleColor: "#000000",
              textSectionTitleDisabledColor: "#d9e1e8",
              selectedDayBackgroundColor: "#00adf5",
              selectedDayTextColor: "#ffffff",
              todayTextColor: "#0771CD",
              dayTextColor: "#000000",
              textDisabledColor: "#989898",
              dotColor: "#00adf5",
              selectedDotColor: "green",
              arrowColor: "#666666",
              disabledArrowColor: "#d9e1e8",
              monthTextColor: "#000000",
              indicatorColor: "blue",
              textDayFontFamily: "Roboto-Medium",
              textMonthFontFamily: "Roboto-Medium",
              textDayHeaderFontFamily: "Roboto-Medium",
              textDayFontWeight: "bold",
              textMonthFontWeight: "bold",
              textDayHeaderFontWeight: "bold",
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 14,
            }}
            onDayPress={(day) => {
              console.log("onDayPress: ", day);
              setSelectedDay(day.dateString);
              setDataMarkedSunday(currentDateInSelectedMonth, day.dateString);
              onPressSelectedDate(day.dateString);
              AdjustTracker(AdjustTrackerConfig.Forum_Konferensi_Tanggal);
            }}
            renderArrow={(direction) => {
              {
                if (direction == "left")
                  return (
                    <ArrowCalendar
                      type="left"
                      previousMonthName={previousMonthName}
                    />
                  );
                if (direction == "right")
                  return (
                    <ArrowCalendar type="right" nextMonthName={nextMonthName} />
                  );
              }
            }}
            markingType={"custom"}
            markedDates={marked}
            onMonthChange={(data) => {
              console.log("onMonthChange data: ", data);
              setSelectedDay("");
              setSelectedMonth(data.month);
              setSelectedYear(data.year);
              setDataNameNextMonth(data.month);
              setDataNamePreviousMonth(data.month);
              setDataMarkedSunday(data.dateString);
              setCurrentDateInSelectedMonth(data.dateString);
              setIsEmptyConferenceInDate(false);
            }}
          />
        </View>
      </Content>
    </Container>
  );
};

export default ChannelKonferensi;

const styles = StyleSheet.create({
  headerComponentBottomsheet: {
    backgroundColor: "#EBEBEB",
    alignSelf: "center",
    height: 4,
    width: 48,
    marginVertical: 16,
    borderRadius: 2,
  },
  headerComponentBottomsheetWarningIOS: {
    backgroundColor: "#EBEBEB",
    alignSelf: "center",
    height: 4,
    width: 48,
    marginTop: 16,
    borderRadius: 2,
  },
  headerDateSection: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    letterSpacing: 0.15,
    color: "#666666",
    lineHeight: 24,
    textAlign: "center",
    marginBottom: 20,
  },
  modalStyleBottomSheet: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    paddingHorizontal: 16,
  },
});
