import S from "./Splash/Splash";
import L from "./Login/Login";
import H from "./Home/Home";
import F from "./Feed/Feed";
import AP from "./AlbumP2KB/AlbumP2KB";
import SK from "./Search/SearchKonten";
import PN from "./ProfileNew/ProfileNew";
import N from "./Notification";
import SAB from "./SearchAllByKonten/SearchAllByKonten";
import B from "./Biodata/Biodata";
import FSK from "./FilterSearchKonten/FilterSearchKonten";
import CP from "./ChangeProfile/ChangeProfile";
import CW from "./CalendarWebinar/CalendarWebinar";
import CH from "./Channel/Channel";
import CD from "./ChannelDetail/ChannelDetail";
import J from "./Job/Job";
import CK from "./ChannelKonferensi/ChannelKonferensi";
import JD from "./JobDetail/JobDetail";
import JRA from "./JobReviewArtikel/JobReviewArtikel";
import JTA from "./JobTulisArtikel/JobTulisArtikel";
import CSA from "./ChannelSeeAll/ChannelSeeAll";
import CS from "./ComingSoon/ComingSoon";
import JF from "./JobFilter/JobFilter";
import JLS from "./JobListSpecialization/JobListSpecialization";
import CDP from "./ChangeDetailProfile/ChangeDetailProfile";
import FO from "./Forum/Forum";
import FD from "./ForumDetail/ForumDetail";
import RC from "./ReplyComment/ReplyComment";
import CU from "./ContactUs/ContactUs";
import NS from "./NonSpecialist/NonSpecialist";
import BPD from "./BannerProductDetail/BannerProductDetail";
import CR from "./ConferenceRoom/ConferenceRoom";
import WP from "./WebviewPage/WebviewPage";
import BO from "./Exhibition/Booth";
import BD from "./Exhibition/BoothDetail";
import WD from "./Webinar/WebinarDetail";
import FN from "./FeedsNew/FeedsNew";
const Splash = S;
const Login = L;
const Home = H;
const Feed = F;
const AlbumP2KB = AP;
const SearchKonten = SK;
const ProfileNew = PN;
const Notification = N;
const SearchAllByKonten = SAB;
const Biodata = B;
const FilterSearchKonten = FSK;
const ChangeProfile = CP;
const CalendarWebinar = CW;
const Channel = CH;
const ChannelDetail = CD;
const Job = J;
const ChannelKonferensi = CK;
const JobDetail = JD;
const JobReviewArtikel = JRA;
const JobTulisArtikel = JTA;
const ChannelSeeAll = CSA;
const ComingSoon = CS;
const JobFilter = JF;
const JobListSpecialization = JLS;
const ChangeDetailProfile = CDP;
const Forum = FO;
const ForumDetail = FD;
const ReplyComment = RC;
const ContactUs = CU;
const NonSpecialist = NS;
const BannerProductDetail = BPD;
const ConferenceRoom = CR;
const WebviewPage = WP;
const Booth = BO;
const BoothDetail = BD;
const WebinarDetail = WD;
const FeedsNew = FN;

export {
  Splash,
  Login,
  Home,
  Feed,
  AlbumP2KB,
  SearchKonten,
  ProfileNew,
  Notification,
  SearchAllByKonten,
  Biodata,
  FilterSearchKonten,
  ChangeProfile,
  CalendarWebinar,
  Channel,
  ChannelDetail,
  Job,
  ChannelKonferensi,
  JobDetail,
  JobReviewArtikel,
  JobTulisArtikel,
  ChannelSeeAll,
  ComingSoon,
  JobFilter,
  JobListSpecialization,
  ChangeDetailProfile,
  Forum,
  ForumDetail,
  ReplyComment,
  ContactUs,
  NonSpecialist,
  BannerProductDetail,
  ConferenceRoom,
  WebviewPage,
  Booth,
  BoothDetail,
  WebinarDetail,
  FeedsNew,
};
