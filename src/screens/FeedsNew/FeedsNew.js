import { CardItemFeed, CardItemSearch, HeaderToolbar } from "../../components";
import React, { useState, useEffect, useRef } from "react";
import { Text, Toast, Container, Content, Icon } from "native-base";
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import { share } from "../../../app/libs/Common";
import _ from "lodash";
import moment from "moment";
import { Loader } from "../../../app/components";
import platform from "../../../theme/variables/platform";
import { IconRecommendationData, IconScrollUp } from "../../assets";
import { getFeedsNew } from "../../utils";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const FeedsNew = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const [pageFeeds, setPageFeeds] = useState(1);
  const [limitFeeds, setLimitFeeds] = useState(10);
  const [isEmptyFeedsData, setisEmptyFeedsData] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [dataFeeds, setDataFeeds] = useState([]);
  const [serverDate, setServerDate] = useState(
    moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
  );
  const [isShowButtonScroll, setIsShowButtonScroll] = useState(false);
  const [isDataNotFound, setIsDataNotFound] = useState(false);

  useEffect(() => {
    navigation.setParams({ onRefreshData: onRefreshData });
    getDataFeeds();
  }, []);

  useEffect(() => {
    if (params.isFromBottomNav != true) {
      BackHandler.addEventListener("hardwareBackPress", handleBackButton);
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
      };
    }
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    if (isRefresh == true) {
      setIsRefresh(false);
      getDataFeeds();
    }
  }, [isRefresh == true]);

  const handleLoadMore = () => {
    if (isEmptyFeedsData == true || isFetching == true) {
      return;
    }
    getDataFeeds();
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter(isFetching),
        // { height: isFetching == true ? 80 : 0 },
        { width: isFetching == true ? "100%" : null },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailed == true && pageFeeds > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    if (isFetching == true) {
      return (
        <View>
          <CardItemSearch
            type="shimmerFeeds"
            data={{
              flag_bookmark: "false",
            }}
          />
        </View>
      );
    }
  };

  const onRefreshData = () => {
    console.log("IsRefreshing");
    setDataFeeds([]);
    setPageFeeds(1);
    setIsFetching(true);
    setisEmptyFeedsData(false);
    setIsRefresh(true);
    setIsDataNotFound(false);
  };

  const renderEmptyFeeds = () => {
    if (!isFetching) {
      return (
        <View style={styles.containerEmptyFeeds}>
          <View style={styles.containerInner}>
            <Text style={styles.textEmptyFeeds}>Belum Ada Feeds</Text>
          </View>
        </View>
      );
    }
  };

  const getDataFeeds = async () => {
    if (pageFeeds == 1) {
      AdjustTracker(AdjustTrackerConfig.Feed_Onload);
    }
    setIsFetching(true);
    setIsRefresh(false);
    setIsFailed(false);
    console.log("pageFeeds -> ", pageFeeds);
    try {
      let params = {
        page: pageFeeds,
        limit: limitFeeds,
      };
      let response = await getFeedsNew(params);
      console.log("response getFeedsNew: ", response);

      if (response.acknowledge) {
        let dataResult = response.result.data;
        if (pageFeeds == 1 && _.isEmpty(dataResult)) {
          setIsDataNotFound(true);
          console.log("notfound");
        }
        setisEmptyFeedsData(_.isEmpty(dataResult) ? true : false);
        setDataFeeds(
          pageFeeds == 1 ? dataResult : [...dataFeeds, ...dataResult]
        );
        setPageFeeds(pageFeeds + 1);
        setIsFailed(false);
        setIsFetching(false);
      } else {
        setIsFailed(true);
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
      setIsFetching(false);
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const _renderData = (item) => {
    // console.log(item.index, " data type ->>>> ", item.item.category);
    return (
      <CardItemFeed
        navigation={navigation}
        paramsData={item.item}
        serverDate={serverDate}
        onRefreshData={onRefreshData}
      />
    );
  };

  const scrollToTop = () => {
    flatlistRef.current.scrollToOffset({ animated: true, offset: 0 });
  };

  const flatlistRef = React.useRef();

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <HeaderToolbar
        onPress={params.isFromBottomNav ? undefined : handleBackButton}
        title="Feed"
      />
      <View style={{ flex: 1 }}>
        <FlatList
          contentContainerStyle={{
            paddingHorizontal: 16,
            paddingTop: 20,
            paddingBottom: 100,
          }}
          onScroll={(e) => {
            e.nativeEvent.contentOffset.y == 0 ||
            e.nativeEvent.contentOffset.y <= 1500
              ? setIsShowButtonScroll(false)
              : e.nativeEvent.contentOffset.y > 1500
              ? setIsShowButtonScroll(true)
              : null;
          }}
          ref={flatlistRef}
          data={dataFeeds}
          keyExtractor={(item, index) => index.toString()}
          renderItem={_renderData}
          onEndReachedThreshold={0.5}
          onEndReached={handleLoadMore}
          ListFooterComponent={_renderItemFooter()}
          onRefresh={() => onRefreshData()}
          refreshing={false}
          ListEmptyComponent={renderEmptyFeeds()}
        />
        {isShowButtonScroll ? (
          <TouchableOpacity
            style={styles.containerScrollUp}
            onPress={() => scrollToTop()}
          >
            <IconScrollUp />
          </TouchableOpacity>
        ) : null}
      </View>
    </Container>
  );
};

export default FeedsNew;

const styles = StyleSheet.create({
  notFoundContainer: {
    justifyContent: "center",
  },
  title: {
    marginTop: 32,
  },
  subtitle: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
  },
  containerEmptyFeeds: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 3 - 80,
  },
  textEmptyFeeds: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    lineHeight: 24,
    letterSpacing: 0.15,
    color: "#000000",
    marginTop: 32,
  },
  containerInner: {
    justifyContent: "center",
    alignItems: "center",
  },
  containerItemFooter: (isFetching) => [
    {
      justifyContent: isFetching ? null : "center",
      alignItems: isFetching ? null : "center",
    },
  ],
  contentContainer: {
    backgroundColor: "#FFFFFF",
    opacity: 1,
  },
  notFoundContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 4,
    backgroundColor: "#FFFFFF",
    opacity: 1,
  },
  containerScrollUp: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    right: 16,
    bottom: 25,
  },
});
