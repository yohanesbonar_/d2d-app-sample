import {
  Body,
  Container,
  Content,
  Header,
  Left,
  Right,
  Row,
  Toast,
  Icon,
} from "native-base";
import React, { useState, useEffect, useRef } from "react";
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Linking,
  StatusBar,
  Platform,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import {
  IconAtozSearch,
  IconCmeSearch,
  IconEventSearch,
  IconJobSearch,
  IconJurnal,
  IconKelolaTD,
  IconLiveNowWebinar,
  IconNotification,
  IconQrscan,
  IconRecordedWebinar,
  IconUpcomingWebinar,
  IconRecommendationData,
} from "../../assets";
import {
  BottomSheet,
  CardItemChannel,
  CardItemSearch,
  HomeProfile,
  HorizontalSliderCard,
  ImageCarousel,
  MainMenuHome,
} from "../../components/molecules";
import { Loader } from "../../../app/components";
import SearchBarField from "../../components/molecules/SearchBarField";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";
import {
  STORAGE_TABLE_NAME,
  EnumFeedsCategory,
  subscribeFcm,
  subscribeFcmSubscription,
  testID,
  checkTopicsBySubscription,
  sendTopicsWebinar,
  isStarted,
} from "../../../app/libs/Common";
import {
  doCheckVersion,
  getDetailSlug,
  sendFIrebaseToken,
  doRegisterEvent,
  getDetailCme,
  getEventDetail,
  getLinkGraphTD,
  getPopupShow,
  cappingPopup,
  submitPopup,
  updateDataProfile,
  doDetailCertificate,
  getWebinarDetail,
} from "../../../app/libs/NetworkUtility";
import { CommonActions } from "@react-navigation/native";
import {
  getBanner,
  getChannel,
  getFeedsNewCategoryList,
  getFeedsDataWebinarHomepage,
  getProfile,
  getSearch,
} from "../../utils/network";
import { getData, KEY_ASYNC_STORAGE, storeData } from "../../utils";
import firebase from "react-native-firebase";
const gueloginAuth = firebase.app("guelogin");
import _ from "lodash";
import ReactModal from "react-native-modal";
import ModalPopup from "../../../app/screens/feeds/ModalPopup";
import DeviceInfo from "react-native-device-info";
import moment from "moment-timezone";
import {
  checkNpaIdiEmpty,
  sendTopicEventAttendee,
  sendTopicWebinarAttendee,
} from "../../utils/commons";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { subscribeChannel } from "../../utils/network/channel/subscribeChannel";
import { Modalize } from "react-native-modalize";
import { getChannelDetail } from "../../utils/network/channel/channelDetail";
import Orientation from "react-native-orientation";
import { getDetailForum } from "../../utils/network/forum/detailForum";
import ChannelModalJoin from "../../components/screen_components/Channel/ChannelModalJoin";
import {
  getAgreementPopupPrivacyPolicy,
  submitPopupPrivacyPolicy,
} from "../../utils/network/agreement";
import CardItemFeed from "../../components/molecules/CardItemFeed";
import platform from "../../../theme/variables/platform";
import { getFeedsNew } from "../../utils";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import AsyncStorage from "@react-native-community/async-storage";

const Home = ({ navigation }) => {
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [dataBanner, setDataBanner] = useState([]);
  const [isCheckedPopupPrivacy, setIsCheckedPopupPrivacy] = useState(false);
  const [isShowPopupPrivacy, setIsShowPopupPrivacy] = useState(false);
  const [dataPopupPrivacy, setDataPopupPrivacy] = useState("");
  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isNotCompletedProfile, setIsNotCompletedProfile] = useState(false);
  const [dataRecommendationChannel, setDataRecommendationChannel] = useState(
    []
  );
  const [typeBottomSheet, setTypeBottomSheet] = useState("");
  const [titleBottomSheet, setTitleBottomSheet] = useState("");
  const [descBottomSheet, setDescBottomSheet] = useState("");
  const [itemChannel, setItemChannel] = useState({});
  const [deeplinkChannelObj, setDeeplinkChannelObj] = useState({
    channelXid: null,
    navigateTo: null,
    // channelXid: "77779e6b-712f-4428-b6d9-6af5c2a5af77",
    // navigateTo: "JobDetail",
  });
  const [profileSummary, setProfileSummary] = useState({
    name: "-",
    specialist: "-",
    photo: "",
    practiceLocation: "-",
    typePracticeLocation: "",
    skp_point: 0,
  });

  const [allDataCme, setAllDataCme] = useState([]);
  const [allDataEvent, setAllDataEvent] = useState([]);
  const [allDataJob, setAllDataJob] = useState([]);
  const [allDataGuideline, setAllDataGuideline] = useState([]);
  const [allDataJournal, setAllDataJournal] = useState([]);
  const [allDataObatAtoz, setAllDataObatAtoz] = useState([]);
  const [serverDate, setServerDate] = useState("");

  const [searchValue, setSearchValue] = useState("");

  const [isLoadingBanner, setIsLoadingBanner] = useState(false);
  const [isLoadingSearchContent, setIsLoadingSearchContent] = useState(false);
  const [
    isNotificationOpenCheckCompleted,
    setIsNotificationOpenCheckCompleted,
  ] = useState(false);

  const [isFetchingFeeds, setIsFetchingFeeds] = useState(false);
  const [pageFeeds, setPageFeeds] = useState(1);
  const [limitFeeds, setLimitFeeds] = useState(10);
  const [isEmptyFeedsData, setisEmptyFeedsData] = useState(false);
  const [isFailedFeeds, setIsFailedFeeds] = useState(false);
  const [dataFeeds, setDataFeeds] = useState([]);
  const [isRefreshFeeds, setIsRefreshFeeds] = useState(false);
  const [isTriggerUpdate, setIsTriggerUpdate] = useState(false);
  const [isDataNotFoundFeeds, setIsDataNotFoundFeeds] = useState(false);

  const [categoryListData, setCategoryListData] = useState([]);
  const [liveWebinarHomapageList, setLiveWebinarHomePageList] = useState([]);
  const [
    upcomingWebinarHomapageList,
    setUpcomingWebinarHomePageList,
  ] = useState([]);
  const [
    recordedWebinarHomapageList,
    setRecordedWebinarHomePageList,
  ] = useState([]);

  const [limitSlider, setLimitSlider] = useState(5);

  const channelJoinModalRef = useRef();
  const triggerUpdateRef = useRef(isTriggerUpdate);

  useEffect(() => {
    if (isTriggerUpdate) {
      navigation.addListener("didFocus", () => {
        if (triggerUpdateRef.current) {
          onPullRefresh();
          flatlistRef.current.scrollToOffset({ animated: true, offset: 0 });
          setIsTriggerUpdate(false);
          triggerUpdateRef.current = false;
        }
      });
    }
  }, [isTriggerUpdate]);

  useEffect(() => {
    navigation.setParams({ onRefreshData: onRefreshData });
    Orientation.lockToPortrait();

    const onUnsubscribeNotificationDisplayedListener = firebase
      .notifications()
      .onNotificationDisplayed((notification) => {
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
      });

    const onUnsubscribeNotificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen) => {
        // Toast.show({ text: '3', position: 'bottom', duration: 3000 });
        const notification = notificationOpen.notification;
        console.log("notificationOpenedListener: ", notification);

        if (notification != null && notification.data != null) {
          onReceivePushNotif(notification);
        }

        if (ThemeD2D.platform == "android") {
          //notification.android.setAutoCancel(true)
          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
        }
      });

    const onUnsubscribeNotificaitonListener = firebase
      .notifications()
      .onNotification((notification) => {
        //firebase.notifications().displayNotification(notification);
        if (notification !== null && notification.data != null)
          displayNotification(notification.data);
      });

    const user = gueloginAuth.auth().currentUser;

    if (user) {
      if (
        user.emailVerified ||
        (user.emailVerified === false &&
          (user.providerData[0].providerId == "facebook.com" ||
            user.providerData[0].providerId == "gmail"))
      ) {
        checkDeepLink();
        StatusBar.setHidden(false);
        getDataMain();

        getDataProfile();

        initFcm();
        checkModalPopup();
        getDataBannerHome();
        getDataFeeds();
        // getDataRecomendationChannel();
        messageListener = firebase.messaging().onMessage((message) => {
          // receiving message
          if (_.isEmpty(message._data) == true) {
            console.warn("silent push adjust");
          }
        });
      }
    }

    Linking.addEventListener("url", handleOpenURL);

    return () => {
      onUnsubscribeNotificaitonListener();
      onUnsubscribeNotificationDisplayedListener();
      onUnsubscribeNotificationOpenedListener();
      Linking.removeEventListener("url", handleOpenURL);
      setDeeplinkChannelObj({ channelXid: null, navigateTo: null });
    };
  }, []);

  useEffect(() => {
    if (isRefresh) {
      setDeeplinkChannelObj({ channelXid: null, navigateTo: null });
      doGetProfile();
      getDataBannerHome();
      getDataMain();
      getDataFeeds();
    }
    //getDataRecomendationChannel();
  }, [isRefresh]);

  const flatlistRef = React.useRef();

  useEffect(() => {
    if (isRefreshFeeds == true) {
      setIsRefreshFeeds(false);
      getDataFeeds();
    }
  }, [isRefreshFeeds == true]);

  const doGetProfile = async () => {
    setIsRefresh(false);

    try {
      let response = await getProfile();
      console.log("response: ", response);
      if (response.acknowledge) {
        let result = response.result;
        await storeData(KEY_ASYNC_STORAGE.PROFILE, result);
        await getDataProfile();
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };

  const getDataProfile = async () => {
    let dataProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    let profileSummary = {};
    profileSummary.skp_point = dataProfile.skp_point;
    profileSummary.name = dataProfile.name;

    let specialist = [];
    dataProfile.spesialization.map((value, i) => {
      specialist.push(value.description);
    });

    profileSummary.specialist = specialist.join(", ");
    profileSummary.nonspesialization = _.isEmpty(dataProfile.nonspesialization)
      ? "Dokter Umum"
      : dataProfile.nonspesialization[0].description;

    profileSummary.typePracticeLocation = _.isEmpty(dataProfile.clinic_type_1)
      ? ""
      : dataProfile.clinic_type_1 + " ";
    profileSummary.practiceLocation = dataProfile.clinic_location_1;
    profileSummary.practiceLocation = profileSummary.practiceLocation.trim();
    profileSummary.photo = dataProfile.photo;

    setProfileSummary(profileSummary);

    let profileSubscription = null;
    let profileWebinar = null;
    if (dataProfile != null) {
      let subscriptionId = [];
      // let subscriptionTitle = [];
      let dataSubscription = "";
      profileSubscription = dataProfile.subscription;
      profileWebinar = dataProfile.webinar;

      if (profileSubscription != null && profileSubscription.length > 0) {
        let self = this;
        profileSubscription.map(function(d, i) {
          subscriptionId.push(d.id);
          // subscriptionTitle.push(d.title);
        });

        dataSubscription = subscriptionId.join(",");
      }
    }

    // cek token topics fcm

    if (profileSubscription != null && profileSubscription.length > 0) {
      checkTopicsBySubscription(profileSubscription);
    }

    sendTopicsWebinar(profileWebinar);

    let isNotCompleted = false;

    console.log(" dataProfile: ", dataProfile);
    if (dataProfile != null && dataProfile.country_code != "PH") {
      if (
        dataProfile.spesialization == "" ||
        dataProfile.spesialization == null ||
        dataProfile.spesialization.length <= 0 ||
        (dataProfile.phone == "" || dataProfile.phone == null) ||
        (dataProfile.country_code == "ID" && dataProfile.pns == null) ||
        (dataProfile.born_date == "" || dataProfile.born_date == null) ||
        (dataProfile.country_code == "ID" &&
          (dataProfile.home_location == "" ||
            dataProfile.home_location == null)) ||
        (dataProfile.clinic_location_1 == "" &&
          dataProfile.clinic_location_2 == "" &&
          dataProfile.clinic_location_3 == "") ||
        (dataProfile.education_1 == "" &&
          dataProfile.education_2 == "" &&
          dataProfile.education_3 == "")
      ) {
        isNotCompleted = true;
      }
    }

    setIsNotCompletedProfile(isNotCompleted);
  };

  const handleSubmitPopup = async (data) => {
    console.log("handleSubmitPopup =>>", data);
    try {
      let user = gueloginAuth.auth().currentUser;
      if (!_.isEmpty(user)) {
        // let params = {
        //   id: dataPopupPrivacy.id,
        //   status: isCheckedPopupPrivacy ? "A" : "D",
        //   checkboxes: [
        //     {
        //       id: dataPopupPrivacy.id,
        //       status: isCheckedPopupPrivacy ? "A" : "D",
        //     },
        //     {
        //       id: dataPopupPrivacy.id,
        //       status: isCheckedPopupPrivacy ? "A" : "D",
        //     },
        //   ],
        // };
        // let response = await submitPopup(params);
        let params = {
          checkboxes: data,
        };
        let response = await submitPopupPrivacyPolicy(params);
        console.log("submit", response);
        console.log("submit Params", params);
        if (response.acknowledge == true) {
          setIsFetching(false);
          setIsSuccess(true);
          setIsFailed(false);
          setIsShowPopupPrivacy(false);
        } else {
          if (response.message != "logout") {
            Toast.show({
              text: response.message,
              position: "top",
              duration: 3000,
            });
            setIsFetching(false);
            setIsSuccess(true);
            setIsFailed(true);
          }
        }
      }
    } catch (error) {
      setIsFetching(false);
      setIsSuccess(false);
      setIsFailed(true);

      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  };

  const checkAppVersion = async () => {
    try {
      // -- api v3 --
      let deviceBuildNumber = DeviceInfo.getBuildNumber();
      let deviceVersion = DeviceInfo.getVersion();
      let paramsData = {
        version: deviceVersion,
        build_number: deviceBuildNumber,
      };
      let response = await doCheckVersion(paramsData);
      if (response.isSuccess === true && response.docs != null) {
        if (response.docs.show == false) {
          updateProfile();
        }
      }
    } catch (error) {
      console.log("checkAppVersion error: ", error);
    }
  };

  const updateProfile = async () => {
    let fcm_token = await getData(KEY_ASYNC_STORAGE.FCM_TOKEN);
    let profile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    console.log("updateProfile profile: ", profile);

    let params = {
      uid: profile.uid,
      npa_idi: profile.npa_idi == null ? "" : profile.npa_idi,
      name: profile.name,
      email: profile.email,
      phone: profile.phone,
      born_date: profile.born_date,

      country_code: profile.country_code,
      education_1: profile.education_1,
      education_2: profile.education_2 == null ? "" : profile.education_2,
      education_3: profile.education_3 == null ? "" : profile.education_3,
      home_location: profile.home_location == null ? "" : profile.home_location,
      clinic_location_1: profile.clinic_location_1,
      clinic_location_2:
        profile.clinic_location_2 == null ? "" : profile.clinic_location_2,
      clinic_location_3:
        profile.clinic_location_3 == null ? "" : profile.clinic_location_3,
      pns: profile.pns,

      ///device info
      app_version: DeviceInfo.getVersion(),
      device_unique_id: DeviceInfo.getUniqueId(),
      fcm_token: fcm_token,
      device_platform: DeviceInfo.getSystemName(),
      system_version: DeviceInfo.getSystemVersion(),
      os_build_version: await DeviceInfo.getBuildId(), //should be await, because it was promise,
      device_brand: DeviceInfo.getBrand(),
    };
    console.log("updateProfile params: ", params);
    let response = await updateDataProfile(params);

    if (response.isSuccess == true) {
      await storeData(KEY_ASYNC_STORAGE.IS_UPDATE_PROFILE, "Y");
      console.log("updateProfile response: ", response);
    } else {
      console.log("updateProfile response: ", response);
    }

    // }
  };
  const checkModalPopup = async () => {
    try {
      let user = gueloginAuth.auth().currentUser;
      if (!_.isEmpty(user)) {
        // let response = await getPopupShow();  -> change to 3.0
        let response = await getAgreementPopupPrivacyPolicy();
        console.log("checkModalPopup response: ", response);
        let res = response.result;
        if (response.acknowledge == true && res != null && !_.isEmpty(res)) {
          // res.uid = user.uid;
          setIsShowPopupPrivacy(true);
          setDataPopupPrivacy(res);
          console.log("popup", res);
        } else {
          console.log("gagalRes", res);
        }
      }
    } catch (error) {
      console.log("gagal", error);
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  };

  handleCappingPopup = async () => {
    setIsShowPopupPrivacy(false);
    try {
      let user = gueloginAuth.auth().currentUser;
      if (!_.isEmpty(user)) {
        let response = await cappingPopup();
        if (response.isSuccess == true) {
          console.log("res", response);
          console.log("capping");
        } else {
          console.log("gagal", res);
        }
      }
    } catch (error) {
      console.log("gagalApi", res);
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  };

  const initFcm = async () => {
    console.log("initFcm");
    let topicFCM = await subscribeFcm();
    await firebase.messaging().subscribeToTopic(topicFCM);
    // console.warn(subscribeFcm)
    //case kill appsAsyncSt
    let deviceToken = await getData(KEY_ASYNC_STORAGE.FCM_TOKEN);
    let isUpdateProfile = await getData(KEY_ASYNC_STORAGE.IS_UPDATE_PROFILE);

    if (
      deviceToken == null ||
      deviceToken == "" ||
      isUpdateProfile == null ||
      isUpdateProfile == ""
    ) {
      const enabled = await firebase.messaging().hasPermission();
      if (enabled) {
        // user has permissions
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
          // user has a device toke
          await sendFCM(fcmToken);
        } else {
          // user doesn't have a device token yet
          await firebase.messaging().requestPermission();
        }
      } else {
        // user doesn't have permission
        await firebase.messaging().requestPermission();
      }
    }

    await checkAppVersion();

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    console.log("notificationOpen: ", notificationOpen);
    setIsNotificationOpenCheckCompleted(true);
    if (notificationOpen) {
      // Toast.show({ text: '1', position: 'bottom', duration: 3000 });
      const notification = notificationOpen.notification;
      if (notification != null && notification.data != null) {
        onReceivePushNotif(notification);
      }
    }

    // Create the channel
    const channel = new firebase.notifications.Android.Channel(
      "d2dpushnotif-channel",
      "D2D Channel",
      firebase.notifications.Android.Importance.Max
    ).setDescription("D2D channel");
    firebase.notifications().android.createChannel(channel);
  };
  const randomizer = () =>
    Math.floor((1 + Math.random()) * 0x100000000000).toString(16);

  const displayNotification = (data) => {
    if (Platform.OS === "ios") {
      const notificationId = `d2d_notif_${randomizer()}`;

      const notification = new firebase.notifications.Notification()
        .setNotificationId(notificationId)
        .setTitle(data.title)
        .setBody(data.body)
        .setSound("default")
        .setData({
          ...data,
          show_in_foreground: true,
          groupSummary: true,
        });

      firebase.notifications().displayNotification(notification);
    }

    if (Platform.OS === "android") {
      const notificationId = `d2d_notif_${randomizer()}`;

      const notification = new firebase.notifications.Notification()
        .setNotificationId(notificationId)
        .setTitle(data.title)
        .setBody(data.body)
        .setSound("default")
        .android.setAutoCancel(true)
        .android.setChannelId("d2dpushnotif-channel")
        .android.setSmallIcon("ic_launcher")
        .android.setPriority(firebase.notifications.Android.Priority.Max)
        .setData({
          // data: { data },
          ...data,
          show_in_foreground: true,
          groupSummary: true,
        });
      firebase.notifications().displayNotification(notification);
    }
  };

  const sendFCM = async (FCMToken) => {
    setIsFailed(false);
    setIsFetching(true);

    try {
      console.log("fcm token", FCMToken);
      let response = await sendFIrebaseToken(FCMToken);

      if (response.isSuccess == true) {
        await storeData(KEY_ASYNC_STORAGE.FCM_TOKEN, FCMToken);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  };

  const onReceivePushNotif = (notification) => {
    console.log("notification.data: ", notification.data);
    if (notification.data.type == EnumFeedsCategory.CME_CERTIFICATE) {
      let data = {
        title: notification.data.title,
        certificate: notification.data.slug,
        isRead: true,
      };
      gotoCmeCertificate(data);
    } else if (
      notification.data.type == EnumFeedsCategory.PROFILE_NOTIFICATION
    ) {
      gotoProfileNotification(notification.data);
    } else if (notification.data.type == EnumFeedsCategory.REQ_JOURNAL) {
      let data = {
        isFromPushNotification: true,
      };
      gotoReqJournal(data);
    } else if (notification.data.type == EnumFeedsCategory.JOURNAL) {
      // this.gotoReqJournal()
      onRefreshFeeds();
    } else if (notification.data.type == EnumFeedsCategory.EVENT_LIST) {
      gotoEventList();
    } else if (notification.data.type == EnumFeedsCategory.EVENT) {
      gotoEventDetailFromPushNotif(notification.data);
    } else if (notification.data.type == EnumFeedsCategory.CME) {
      doHitDetailCME(notification.data.id_cme);
    } else if (notification.data.type == EnumFeedsCategory.PMM_PATIENT) {
      gotoPMMPatient(notification.data);
    } else if (notification.data.type == EnumFeedsCategory.CME_LIST) {
      gotoCmeList(notification.data);
    } else if (notification.data.type == EnumFeedsCategory.CME_DETAIL) {
      doHitDetailCME(notification.data.quiz_id);
    } else if (notification.data.type == EnumFeedsCategory.ATOZ_LIST) {
      gotoAtozList(notification.data);
    } else if (notification.data.type == EnumFeedsCategory.ATOZ_DETAIL) {
      gotoAtozDetail(notification.data);
    } else if (notification.data.type == EnumFeedsCategory.FEEDS) {
      //gotoFeeds();
      //revamp
    } else if (
      notification.data.type == EnumFeedsCategory.CERTIFICATE_CME ||
      notification.data.type == EnumFeedsCategory.CERTIFICATE_EVENT ||
      notification.data.type == EnumFeedsCategory.CERTIFICATE_OFFLINE ||
      notification.data.type == EnumFeedsCategory.CERTIFICATE_WEBINAR
    ) {
      //goto certificate
      doHitDetailCertificate(notification.data);
    } else {
      if (notification.data.type == "webinar") {
        if (typeof notification.data.target != "undefined") {
          typeTargetNotif = notification.data.target;
        }
      }

      if (
        !_.isEmpty(notification.data.type) ||
        !_.isEmpty(notification.data.slug) ||
        !_.isEmpty(notification.data.slug_hash)
      ) {
        getDetailCategory(
          notification.data.type,
          notification.data.slug,
          notification.data.slug_hash
        );
      }
    }
  };

  const checkDeepLink = () => {
    Linking.getInitialURL()
      .then((url) => handleOpenURL({ url }))
      .catch((err) => console.error(err));
  };

  const handleOpenURL = async (deeplink) => {
    let currentUser = gueloginAuth.auth().currentUser;
    console.warn("feeds currentUser", currentUser);

    if (currentUser != null) {
      let provider = [];
      if (currentUser.providerData.length > 0) {
        currentUser.providerData.forEach((val, i) => {
          provider.push(val.providerId);
        });
      }
      currentUser.emailVerified = _.includes(provider, "password")
        ? currentUser.emailVerified
        : true;
      console.log("include: ", _.includes(provider, "password"));
      console.log("currentUser.emailVerified: ", currentUser.emailVerified);

      let emailVerified = _.includes(provider, "password")
        ? currentUser.emailVerified
        : true;
      console.log("emailVerified: ", emailVerified);
      _.assign(currentUser, { emailVerified });

      //bug fix: to prevent open deeplink when after login but email is not verified
      if (currentUser && emailVerified === true) {
        let regex = /d2d.co.id/;

        if (deeplink.url != null) {
          if (
            deeplink.url.includes("menu") &&
            deeplink.url.includes("channel") &&
            deeplink.url.includes("forum") &&
            regex.test(deeplink.url)
          ) {
            splitUrlMenu(deeplink.url);
          } else if (
            deeplink.url.includes("menu") &&
            deeplink.url.includes("channel") &&
            deeplink.url.includes("job") &&
            regex.test(deeplink.url)
          ) {
            splitUrlMenu(deeplink.url);
          } else if (
            deeplink.url.includes("type") &&
            deeplink.url.includes("id") &&
            deeplink.url.includes("referral") &&
            deeplink.url.includes("invitationType") &&
            regex.test(deeplink.url)
          ) {
            splitUrlChannelInvitation(deeplink.url);
          } else if (regex.test(deeplink.url)) {
            splitUrl(deeplink.url);
          }
        }
      }
    }
  };

  const getDataRecomendationChannel = async () => {
    setIsRefresh(false);
    try {
      let params = {
        type: "recomendation",
        paginate: 1,
        limit: 8,
      };
      response = await getChannel(params);
      if (response.header.message == "Success") {
        let result = response.data.data;
        setDataRecommendationChannel(result);
        console.log("LOG RESULT ALL CHANNEL -> ", result);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const renderRekomendasiChannel = () => {
    let data = dataRecommendationChannel.slice(0, 5);
    return (
      <View>
        <View style={styles.containerChannel}>
          <Text style={styles.textRekomendasiChannel}>Rekomendasi Forum</Text>
          {dataRecommendationChannel.length > 5 ? (
            <TouchableOpacity
              onPress={() => {
                onPressSeeAll("Rekomendasi Forum", dataRecommendationChannel);
                AdjustTracker(
                  AdjustTrackerConfig.Homepage_RekomendasiChannel_Lihat_Semua
                );
              }}
            >
              <Text style={styles.textLihatSemua}>LIHAT SEMUA</Text>
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </View>

        <View style={styles.containerListCardChannel}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{ width: 8 }} />
            <View style={styles.listChannelWrapper}>
              {data.map((item, i) => {
                return (
                  <View key={i} style={styles.itemCardChannelWrapper}>
                    <CardItemChannel
                      name={item.channel_name}
                      isJoin={item.subscribed}
                      onPressCard={() => {
                        onPressCardCondition(item);
                        AdjustTracker(
                          AdjustTrackerConfig.Homepage_Rekomendasi_Channel
                        );
                      }}
                      onPressButton={() => {
                        onPressButtonInCard(item);
                        AdjustTracker(
                          AdjustTrackerConfig.Homepage_Rekomendasi_Channel
                        );
                      }}
                      imageSource={item.logo}
                    />
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  };

  const onPressCardCondition = (item) => {
    if (item.subscribed == true) {
      onPressCardChannel(item);
    } else {
      setTypeBottomSheet("BottomSheetAboutChannel");
      setTitleBottomSheet(item.channel_name);
      setDescBottomSheet(item.description);
      setItemChannel(item);
      onOpen();
    }
  };

  const onPressCardChannel = (item) => {
    navigation.navigate("ChannelDetail", {
      item: item,
      onRefresh: getDataRecomendationChannel,
      from: "Channel",
      onPressBanner: onPressBanner,
    });
  };

  const onPressButtonInCard = (item) => {
    if (item.subscribed == false) {
      // onPressButtonJoinChannel(item);
      setTypeBottomSheet("BottomSheetAboutChannel");
      setTitleBottomSheet(item.channel_name);
      setDescBottomSheet(item.description);
      setItemChannel(item);
      onOpen();
    } else {
      onPressCardChannel();
    }
  };

  const onPressButtonJoinChannel = async (item) => {
    await postSubcribe(item);
    const timer = setTimeout(async () => {
      await getDataRecomendationChannel();
    }, 1000);
  };

  const postSubcribe = async (item) => {
    let response = {};

    try {
      let paramsGetAllChannel = {
        xid: item.xid,
      };
      response = await subscribeChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        Toast.show({
          text: "Berhasil bergabung ke forum " + item.channel_name,
          buttonText: "TUTUP",
          position: "bottom",
          duration: 2000,
          buttonStyle: { marginLeft: 16 },
        });
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 2000,
        });
      }
      console.log("response SUBSCRIBE XID->", item.xid, " -------- ", response);
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 2000,
      });
    }
  };

  const onPressSeeAll = (title, data) => {
    navigation.navigate("ChannelSeeAll", {
      title: title,
      data: data,
      onRefresh: getDataRecomendationChannel,
    });
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        // onClose={() => onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        {typeBottomSheet == "BottomSheetPageNotFound" ? (
          <BottomSheet
            onPressClose={() => {
              onClose();
            }}
            title={titleBottomSheet}
            desc={descBottomSheet}
            wordingClose="TUTUP"
            type="bottomSheetPageNotFound"
            // imageSource={itemChannel.logo}
          />
        ) : (
          <BottomSheet
            onPressClose={() => {
              onPressButtonJoinChannel(itemChannel);
              onClose();
            }}
            title={titleBottomSheet}
            desc={descBottomSheet}
            wordingClose="GABUNG KE FORUM INI"
            type="bottomSheetAboutChannel"
            imageSource={itemChannel.logo}
          />
        )}
      </Modalize>
    );
  };

  const modalizeRef = useRef(null);

  const onOpen = () => {
    modalizeRef.current.open();
  };

  const onClose = () => {
    modalizeRef.current.close();
  };

  const getDataBannerHome = async () => {
    setIsRefresh(false);
    setIsLoadingBanner(true);
    try {
      let response = await getBanner("home");
      setIsLoadingBanner(false);
      console.log("response: ", response);
      if (response.acknowledge) {
        setDataBanner(response.result);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsLoadingBanner(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const renderMainMenuHome = () => {
    return (
      <View
        style={{
          flex: 1,
          marginBottom: 24,
        }}
      >
        <View style={styles.menuGrid}>
          <MainMenuHome
            icon="home-event"
            title="Event"
            onPress={() => {
              navigation.navigate("Event");
              AdjustTracker(AdjustTrackerConfig.Homepage_Event);
            }}
          />
          <MainMenuHome
            icon="home-webinar"
            title="Webinar"
            onPress={() => {
              navigation.navigate("Webinar");
              AdjustTracker(AdjustTrackerConfig.Homepage_Webinar);
            }}
          />
          <MainMenuHome
            icon="home-cme"
            title="CME"
            onPress={() => {
              navigation.navigate("Cme");
              AdjustTracker(AdjustTrackerConfig.Homepage_CME);
            }}
          />
          <MainMenuHome
            icon="home-job"
            title="Job"
            onPress={() => {
              navigation.navigate("Job", { from: "home" });
              AdjustTracker(AdjustTrackerConfig.Homepage_Job);
            }}
          />
          {/* <MainMenuHome
            icon="home-job"
            title="Job"
            // onPress={() => navigation.navigate("Job")}
            onPress={() => {
              navigation.navigate("ComingSoon", {
                title: "Job",
              });
              AdjustTracker(AdjustTrackerConfig.Homepage_Job);
            }}
          /> */}
        </View>
        <View style={styles.menuGrid}>
          <MainMenuHome
            icon="home-channel"
            title="Forum"
            onPress={() => {
              navigation.navigate("Channel", {
                onRefresh: triggerUpdate,
                onPressBanner: onPressBanner,
              });
              AdjustTracker(AdjustTrackerConfig.Homepage_Channel);
            }}
          />
          <MainMenuHome
            icon="home-literatur"
            title="Literatur"
            onPress={() => {
              navigation.navigate("Learning");
              AdjustTracker(AdjustTrackerConfig.Homepage_Literatur);
            }}
          />
          <MainMenuHome
            icon="home-obatatoz"
            title="Obat A - Z"
            onPress={() => {
              navigation.navigate("AtoZ");
              AdjustTracker(AdjustTrackerConfig.Homepage_ObatAZ);
            }}
          />
          <MainMenuHome
            icon="home-polling"
            title="Polling"
            onPress={() => {
              navigation.navigate("ComingSoon", {
                title: "Polling",
              });
              AdjustTracker(AdjustTrackerConfig.Homepage_Polling);
            }}
          />
        </View>
      </View>
    );
  };

  const renderProfileMenuHome = () => {
    return (
      <HomeProfile
        onPress={() => {
          navigation.navigate("ProfileNew", {
            profileSummary,
            onRefreshDataProfile: getDataProfile,
          });
          AdjustTracker(AdjustTrackerConfig.Homepage_Lihat_Profil);
        }}
        textButton="LIHAT PROFIL"
        name={profileSummary.name}
        practiceLocation={`${profileSummary.typePracticeLocation}${
          profileSummary.practiceLocation
        }`}
        specialist={
          !_.isEmpty(profileSummary.specialist)
            ? profileSummary.specialist
            : profileSummary.nonspesialization
        }
        maxLineSpecialist={1}
        maxLinePracticeLocation={1}
        pointSKP={profileSummary.skp_point}
        photo={profileSummary.photo}
      />
    );
  };

  const onReceiveLinkFromScan = (url) => {
    console.log("url link QR", url);
    let regex = /d2d.co.id/;
    let regexTD = /temandiabetes/;

    if (
      url != null &&
      url.includes("type") &&
      url.includes("id") &&
      url.includes("referral") &&
      url.includes("invitationType") &&
      regex.test(url)
    ) {
      splitUrlChannelInvitation(url);
    } else if (url != null && regex.test(url)) {
      splitUrl(url);
    } else if (url != null && regexTD.test(url)) {
      getLinkGraph(url);
    } else {
      Toast.show({
        text: "QR Code Not Found",
        position: "top",
        duration: 3000,
      });
    }
  };

  const splitUrlChannelInvitation = (url) => {
    console.log("url -> ", url);
    var replaceUrl = "";
    if (url.includes("staging")) {
      replaceUrl = url.replace("https://staging.d2d.co.id/?", "");
    } else {
      replaceUrl = url.replace("http://d2d.co.id/?", "");
      replaceUrl = replaceUrl.replace("https://d2d.co.id/?", "");
    }
    let splitUrl = replaceUrl.split("&");
    console.log("=splitUrlChannelInvitation -> ", splitUrl);
    if (splitUrl != null && splitUrl.length == 4) {
      let type = splitUrl[0].replace("type=", "");
      let id = splitUrl[1].replace("id=", "");
      let referral = splitUrl[2].replace("referral=", "");
      let invitationType = splitUrl[3].replace("invitationType=", "");

      if (type == "channel-invitation") {
        setDeeplinkChannelObj({
          channelXid: id,
          navigateTo: "ChannelDetail",
          paramsXid: {
            data: {
              referral_type: invitationType,
              referral_admin_id: referral,
            },
          },
        });
      }
    }
  };

  const gotoScanScreen = () => {
    AdjustTracker(AdjustTrackerConfig.ScanQR);
    let params = {
      onScanQR: onReceiveLinkFromScan,
    };
    navigation.navigate("ScanScreen", params);
  };

  const splitUrl = (url) => {
    let replaceUrl = url.replace("http://d2d.co.id/?", "");
    replaceUrl = url.replace("https://d2d.co.id/?", "");
    let splitUrl = replaceUrl.split("&");
    if (splitUrl != null && splitUrl.length > 2) {
      let category = splitUrl[0].replace("type=", "");
      let slug = splitUrl[1].replace("title=", "");
      let hash = splitUrl[2].replace("hash=", "");

      if (
        splitUrl.length > 3 &&
        replaceUrl.includes("startat") &&
        category == EnumFeedsCategory.WEBINAR
      ) {
        setIsShowLoader(true);
        let startAt = splitUrl[3].replace("startat=", "");
        startAt = startAt != null && startAt != "" ? parseInt(startAt) : 0;
        getDetailWebinarToNavigate(slug, hash, startAt);
      } else if (category == "eventsignin") {
        doRegisterEventByQR(slug, hash);
      } else {
        getDetailCategory(category, slug, hash);
      }
    }
  };

  const splitUrlMenu = (url) => {
    console.log("url -> ", url);
    var replaceUrl = "";
    if (url.includes("staging")) {
      replaceUrl = url.replace("https://staging.d2d.co.id/?", "");
    } else {
      replaceUrl = url.replace("http://d2d.co.id/?", "");
      replaceUrl = replaceUrl.replace("https://d2d.co.id/?", "");
    }
    let splitUrl = replaceUrl.split("&");
    console.log("SPLIT URL -> ", splitUrl);
    if (splitUrl != null && splitUrl.length == 3) {
      let menu = splitUrl[0].replace("menu=", "");
      let channel = splitUrl[1].replace("channel=", "");
      let forum = splitUrl[2].replace("forum=", "");
      let job;
      if (url.includes("&job=")) {
        job = splitUrl[2].replace("job=", "");
      }

      if (menu == "Forum") {
        setDeeplinkChannelObj({
          channelXid: channel,
          navigateTo: "ForumDetail",
          paramsXid: { data: { xid: forum } },
        });
        // getChannelSBC(channel, forum);
      } else if (menu == "Job") {
        setDeeplinkChannelObj({
          channelXid: channel,
          navigateTo: "JobDetail",
          paramsXid: { item: { xid: job } },
        });
      } else {
        Toast.show({
          text: "Maaf, halaman yang Anda tuju tidak ditemukan.",
          buttonText: "TUTUP",
          position: "bottom",
          duration: 2000,
          buttonStyle: { marginLeft: 16 },
        });
        // setTypeBottomSheet("BottomSheetPageNotFound");
        // setDetailBottomSheet();
      }
    }
  };

  const getChannelSBC = async (channel, forum) => {
    console.log("Channel -> ", channel, "   forum -> ", forum);
    let response = {};
    let data = {
      xid: forum,
    };

    try {
      response = await getChannelDetail(channel);
      console.log("response get channel detail -> ", response);
      let result = response.data;
      if (response.header.message == "Success") {
        responseCheckForum = await getDetailForum(forum, result.sbc);
        if (responseCheckForum.header.message == "Success") {
          goToForumDetail(data, result.sbc);
        } else {
          Toast.show({
            text: "Maaf, halaman yang Anda tuju tidak ditemukan.",
            buttonText: "TUTUP",
            position: "bottom",
            duration: 3000,
            buttonStyle: { marginLeft: 16 },
          });
          // navigation.popToTop();
          // setTypeBottomSheet("BottomSheetPageNotFound");
          // setDetailBottomSheet();
        }
      } else {
        if (response.header.reason.id.includes("bergabung")) {
          Toast.show({
            text: response.header.reason.id,
            buttonText: "TUTUP",
            position: "bottom",
            duration: 3000,
            buttonStyle: { marginLeft: 16 },
          });
        } else {
          Toast.show({
            text: "Maaf, halaman yang Anda tuju tidak ditemukan.",
            buttonText: "TUTUP",
            position: "bottom",
            duration: 3000,
            buttonStyle: { marginLeft: 16 },
          });
          // navigation.popToTop();
          // setTypeBottomSheet("BottomSheetPageNotFound");
          // setDetailBottomSheet();
        }
      }
    } catch (error) {
      Toast.show({
        text: "Maaf, halaman yang Anda tuju tidak ditemukan.",
        buttonText: "TUTUP",
        position: "bottom",
        duration: 3000,
        buttonStyle: { marginLeft: 16 },
      });
      // navigation.popToTop();
      // setTypeBottomSheet("BottomSheetPageNotFound");
      // setDetailBottomSheet();
    }
  };

  const setDetailBottomSheet = () => {
    setTitleBottomSheet("Maaf, Halaman Tidak Ditemukan");
    setDescBottomSheet(
      "Halaman yang Anda tuju tidak ditemukan. Silakan periksa kembali link yang Anda tuju."
    );
    onOpen();
  };

  const goToForumDetail = (data, sbcChannel) => {
    navigation.navigate("ForumDetail", {
      data: data,
      sbcChannel: sbcChannel,
      from: "deeplink",
    });
  };

  const getDetailCategory = async (category, slug, hash) => {
    if (category != null && slug != null) {
      setIsShowLoader(true);

      if (category == EnumFeedsCategory.EVENT) {
        let params = {
          type: "deeplink",
          hash: hash,
        };
        let response = await getEventDetail(params);
        setIsShowLoader(false);
        if (response.isSuccess && response.docs != null) {
          gotoEventDetailByHashDeeplink(response.docs);
        } else {
          Toast.show({
            text: response.message,
            position: "bottom",
            duration: 3000,
          });
        }
      } else if (category == EnumFeedsCategory.WEBINAR) {
        getDetailWebinarToNavigate(slug, hash, 0);
      } else {
        let type =
          category == EnumFeedsCategory.REQ_JOURNAL
            ? EnumFeedsCategory.PDF_JOURNAL
            : category;

        let categoryReqJournal = category;

        let response = await getDetailSlug(type, slug, hash);
        console.log("getDetailSlug response: ", response);
        setIsShowLoader(false);
        if (response.isSuccess) {
          //category : event / pdf / video
          let category = response.data[0].category;
          if (
            category == EnumFeedsCategory.PDF_GUIDELINE ||
            category == EnumFeedsCategory.PDF_KNOWLEDGE ||
            category == EnumFeedsCategory.PDF_BROCHURE ||
            category == EnumFeedsCategory.PDF_LITERATURE ||
            category == EnumFeedsCategory.PDF_PRODUCT_GROUP
          ) {
            gotoLearningGuidelineDetail(response.data[0]);
          } else if (
            category == EnumFeedsCategory.PDF_JOURNAL &&
            categoryReqJournal == category
          ) {
            gotoLearningJournalDetail(response.data[0]);
          } else if (categoryReqJournal == EnumFeedsCategory.REQ_JOURNAL) {
            gotoReqJournal(response.data[0], true);
          } else if (
            category == EnumFeedsCategory.VIDEO_LEARNING ||
            category == EnumFeedsCategory.VIDEO_KNOWLEDGE ||
            category == EnumFeedsCategory.VIDEO_PRODUCT_GROUP ||
            category == EnumFeedsCategory.VIDEO_BROCHURE ||
            category == EnumFeedsCategory.VIDEO_GENERAL
          ) {
            gotoVideoLearning(response.data[0]);
          }
        } else {
          Toast.show({
            text: response.message,
            position: "bottom",
            duration: 3000,
          });
        }
      }
    }
  };

  const getDetailWebinarToNavigate = async (slug, hash, startAt) => {
    let params = {
      type: "deeplink",
      hash: hash,
    };
    let response = await getWebinarDetail(params);
    setIsShowLoader(false);
    console.log("response getWebinarDetail: ", response);
    console.log("response webinar", response.isSuccess, response.docs != null);
    if (response.isSuccess && response.docs != null) {
      let isShowWebinar = await checkShowWebinar(response.docs);
      if (isShowWebinar) {
        let data = response.docs;
        // data.isLive = true;
        data.isLive = moment().isSameOrAfter(
          moment(data.end_date, "YYYY-MM-DD HH:mm:ss")
        )
          ? false
          : true;
        response.docs.typeTargetNotif = this.typeTargetNotif;

        gotoWebinarDetail(false, { ...response.docs, startAt: startAt });
      } else {
        Toast.show({
          text: `Sorry, you're not allowed to access this content.`,
          position: "bottom",
          duration: 3000,
        });
      }
    } else {
      Toast.show({
        text: response.message,
        position: "bottom",
        duration: 3000,
      });
    }
  };

  const doHitDetailCME = async (id) => {
    try {
      setIsShowLoader(true);
      let response = await getDetailCme(id);
      console.log("doHitDetailCME response: ", response);
      setIsShowLoader(false);

      if (response.isSuccess && response.data != null) {
        console.warn(response);
        if (response.data.done == "true") {
          let profile = getData(KEY_ASYNC_STORAGE.PROFILE);
          if (checkNpaIdiEmpty(profile)) {
            gotoCmeQuizDetail(response.data);
          } else if (!checkNpaIdiEmpty(profile)) {
            gotoCmeCertificate(response.data);
          }
        } else if (response.data.done == "false") {
          gotoCmeDetail(response.data);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const doHitDetailCertificate = async (data) => {
    console.log(data);
    setIsShowLoader(true);
    try {
      let response = await doDetailCertificate(data);
      console.log("doHitDetailCertificate response: ", response);

      if (response.isSuccess && response.data != null) {
        if (data.type == EnumFeedsCategory.CERTIFICATE_CME) {
          let profile = await getData(KEY_ASYNC_STORAGE.PROFILE);
          if (checkNpaIdiEmpty(profile)) {
            gotoCmeQuizDetail(response.data);
          } else if (!checkNpaIdiEmpty(profile)) {
            gotoCmeCertificate(response.data);
          }
        } else {
          gotoCmeCertificate(response.data);
        }
      }
      setIsShowLoader(false);
    } catch (error) {
      setIsShowLoader(false);
      console.log(error);
    }
  };

  const gotoCmeDetail = (dataCme) => {
    console.log("gotoCmeDetail dataCme: ", dataCme);
    navigation.navigate("CmeDetail", dataCme);
  };

  const gotoEventList = () => {
    navigation.navigate("Event");
  };

  const gotoEventDetailByHashDeeplink = async (data) => {
    console.log("gotoEventDetailByHashDeeplink data: ", data);

    navigation.navigate("EventDetail", {
      data: data,
    });
  };

  const gotoEventDetailFromPushNotif = async (dataNotif) => {
    console.log("gotoEventDetailFromPushNotif dataNotif: ", dataNotif);
    dataNotif.id = dataNotif.content_id;
    navigation.navigate("EventDetail", {
      data: dataNotif,
    });
  };

  const gotoLearningJournalDetail = (data) => {
    navigation.navigate("LearningJournalDetail", {
      data: data,
      fromDeeplink: true,
    });
  };

  const gotoLearningGuidelineDetail = (data) => {
    let navGuidelineDetail = {
      type: "Navigate",
      routeName: "PdfView",
      params: data,
    };
    navigation.navigate(navGuidelineDetail);
  };

  const gotoVideoLearning = (data) => {
    let navVideo = { type: "Navigate", routeName: "VideoView", params: data };
    navigation.navigate(navVideo);
  };

  const gotoPMMPatient = async (data) => {
    console.log("gotoPMMPatient data: ", data);
    navigation.navigate("PerkenalanFitur", {
      data: data,
      isFromPushNotification: true,
    });
  };

  const gotoCmeList = (data) => {
    console.log("gotoCmeList data: ", data);
    navigation.navigate("Cme", {
      data: data,
    });
  };

  const gotoCmeQuizDetail = (data) => {
    let params = {
      id: data.quiz_id,
      ...data,
      from: "pushNotifCertificate",
    };

    navigation.navigate("CmeQuiz", params);
  };

  const gotoAtozList = (data) => {
    console.log("gotoAtozList data: ", data);
    navigation.navigate("AtoZ", {
      data: data,
    });
  };

  const gotoAtozDetail = (data) => {
    console.log("gotoAtozDetail data: ", data);
    navigation.navigate("DetailAtoZ", {
      obat_id: data.obat_id,
      title_obat: data.title_obat != null ? data.title_obat : "",
      isFromPushNotification: true,
    });
  };

  const getLinkGraph = async (barcode) => {
    if (barcode) {
      let tdUserId = barcode.replace("temandiabetes", "");

      if (tdUserId) {
        try {
          setIsShowLoader(true);
          let response = await getLinkGraphTD(tdUserId);
          setIsShowLoader(false);

          if (response.isSuccess && response.data) {
            let viewGraph = {
              type: "Navigate",
              routeName: "WebviewGrafik",
              params: response.data,
            };
            navigation.navigate(viewGraph);
          } else {
            Toast.show({
              text: response.message,
              position: "top",
              duration: 3000,
            });
          }
        } catch (error) {
          setIsShowLoader(false);
          Toast.show({
            text: "Something went wrong!",
            position: "top",
            duration: 3000,
          });
        }
      }
    }
  };

  const doRegisterEventByQR = async (slug, hash) => {
    if (hash != null) {
      setIsShowLoader(true);
      let response = await doRegisterEvent(hash);
      setIsShowLoader(false);
      if (response.isSuccess == true && response.data != null) {
        gotoEventDetail(response.data);
      } else {
        if (response.message != null) {
          let splitMsg = response.message.split(",");
          if (splitMsg != null && splitMsg.length > 1) {
            if (splitMsg[1] == "toast") {
              Toast.show({
                text: splitMsg[0],
                position: "bottom",
                duration: 3000,
              });
            } else {
              showAlertRegistration(splitMsg[0], false, null, null, null);
            }
          } else {
            Toast.show({
              text: response.message,
              position: "bottom",
              duration: 3000,
            });
          }
        }
      }
    }
  };

  const showAlertRegistration = (message, isSuccess, type, slug, hash) => {
    setTimeout(
      () =>
        Alert.alert(
          "Information",
          message,
          [
            {
              text: "OK",
              onPress: () =>
                isSuccess == true ? getDetailCategory(type, slug, hash) : null,
            },
          ],
          { cancelable: true }
        ),
      700
    );
  };

  const gotoCmeCertificate = (data) => {
    let cmeCertificate = {
      type: "Navigate",
      routeName: "CmeCertificate",
      params: data,
    };
    navigation.navigate(cmeCertificate);
  };

  const gotoProfileNotification = (data) => {
    let profileNotification = {
      type: "Navigate",
      routeName: "Profile",
      params: data,
    };
    navigation.navigate(profileNotification);
  };

  const gotoReqJournal = (data = {}, isFromDeeplink = false) => {
    console.log("gotoReqJournal data: ", data);
    console.log("gotoReqJournal isFromDeeplink: ", isFromDeeplink);
    if (isFromDeeplink) {
      data.isFromDeeplink = isFromDeeplink;
    }
    let reqJournal = {
      type: "Navigate",
      routeName: "LearningRequestJournal",
      params: data,
    };
    navigation.navigate(reqJournal);
  };
  const gotoEventDetail = async (data) => {
    console.log("gotoEventDetail data: ", data);
    let newProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    newProfile.event_attendee.push(data.slug_hash);

    //only insert if data in asnyc storage not exist.
    if (!newProfile.event_attendee.includes(data.slug_hash)) {
      sendTopicEventAttendee(data.slug_hash);
      await storeData(KEY_ASYNC_STORAGE.PROFILE, newProfile);
    }

    navigation.navigate("EventDetail", {
      data: data,
      fromDeeplink: true,
    });
  };

  const checkShowWebinar = async (item) => {
    console.log("item", item);
    let isShowWebinar = false;
    let mProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    console.log("profile", mProfile);
    if (item.exclusive != null && item.isPaid != null) {
      if (item.exclusive == 1 || item.isPaid) {
        if (mProfile != null) {
          if (mProfile.webinar != null && mProfile.webinar.length > 0) {
            mProfile.webinar.forEach((slug_hash) => {
              sendTopicWebinarAttendee(slug_hash);
              if (slug_hash == item.slug_hash) {
                isShowWebinar = true;
              }
            });
          }
        }
      } else {
        isShowWebinar = true;
      }
    }

    return isShowWebinar;
  };

  const gotoWebinarDetail = (isLive, itemWebinar) => {
    console.log("webinar deeplink  -- ", itemWebinar);
    let dataWebinar = itemWebinar;
    let dateShowTNC = moment(dataWebinar.start_date)
      .add(-1, "hours")
      .format("YYYY-MM-DD HH:mm:ss");

    if (itemWebinar != null) {
      dataWebinar.isPaid =
        itemWebinar.exclusive == 1 || itemWebinar.isPaid == true ? true : false;
    }
    let isStartShowTNC = isStarted(dataWebinar.server_date, dateShowTNC);
    if (dataWebinar.has_tnc == true && isStartShowTNC == true) {
      navigation.navigate("WebinarTNC", dataWebinar);
    } else {
      if (dataWebinar != null) {
        navigation.navigate("WebinarVideo", dataWebinar);
      } else {
        Toast.show({
          text: "Something went wrong, empty data webinar",
          position: "bottom",
          duration: 3000,
        });
      }
    }
  };

  const toggleChecked = () => {
    setIsCheckedPopupPrivacy(!isCheckedPopupPrivacy);
  };

  const gotoManageTemanDiabetes = () => {
    let navManageTemanDiabetes = {
      type: "Navigate",
      //routeName: 'PerkenalanFitur',
      routeName: "ManageTemanDiabetes",
      // params: data
    };
    console.log("navManageTemanDiabetes: ", navManageTemanDiabetes);
    navigation.navigate(navManageTemanDiabetes);
  };
  const onPullRefresh = () => {
    setDataBanner([]);
    setDataRecommendationChannel([]);
    setIsRefresh(true);
    console.log("IsRefreshing");
    setDataFeeds([]);
    setPageFeeds(1);
    setIsFetchingFeeds(true);
    setisEmptyFeedsData(false);
    setIsRefreshFeeds(true);
  };

  const refreshDataSearchKonten = () => {
    onPullRefresh();
  };

  const onPressBanner = (link) => {
    let regex = /d2d.co.id/;

    if (link != null) {
      if (
        link.includes("menu") &&
        link.includes("channel") &&
        link.includes("forum") &&
        regex.test(link)
      ) {
        splitUrlMenu(link);
      } else if (regex.test(link)) {
        splitUrl(link);
      } else {
        Linking.openURL(link);
      }
    }
  };

  const getDataMain = async () => {
    setIsLoadingSearchContent(true);
    await getDataCategoryList();
    getDataWebinarHomepage();
    getDataSearchContent();
  };

  const getDataWebinarHomepage = async () => {
    try {
      let response = {};

      let params = {
        limit: limitSlider + 1,
      };

      response = await getFeedsDataWebinarHomepage(params);

      console.log("response getDataWebinarHomepage: ", response);
      let resultLiveWebinar =
        !_.isEmpty(response.result.result.liveWebinar) &&
        response.result.result.liveWebinar != null
          ? response.result.result.liveWebinar
          : [];
      let resultRecordedWebinar =
        !_.isEmpty(response.result.result.recordedWebinar) &&
        response.result.result.recordedWebinar != null
          ? response.result.result.recordedWebinar
          : [];
      let resultUpcomingWebinar =
        !_.isEmpty(response.result.result.upcomingWebinar) &&
        response.result.result.upcomingWebinar != null
          ? response.result.result.upcomingWebinar
          : [];

      if (response.acknowledge) {
        setLiveWebinarHomePageList(resultLiveWebinar);
        setRecordedWebinarHomePageList(resultRecordedWebinar);
        setUpcomingWebinarHomePageList(resultUpcomingWebinar);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      console.log("error: ", error);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const getDataCategoryList = async () => {
    try {
      let response = {};

      response = await getFeedsNewCategoryList();

      console.log("response getFeedsNewCategoryList: ", response);
      if (response.acknowledge && response.result != null) {
        setLimitSlider(
          !_.isEmpty(response.result.limit) && response.result.limit != null
            ? response.result.limit
            : 5
        );
        setCategoryListData(
          !_.isEmpty(response.result.categories) &&
            response.result.categories != null
            ? response.result.categories
            : []
        );
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      console.log("error: ", error);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const getDataSearchContent = async () => {
    try {
      let response = {};

      response = await getSearch(
        1,
        limitSlider + 1,
        searchValue,
        0,
        0,
        1,
        1,
        1,
        1,
        1,
        1,
        0
      );

      console.log("response searchContent: ", response);
      if (response.acknowledge) {
        await setData(response.result);
        setIsLoadingSearchContent(false);

        setServerDate(response.result.server_date);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsLoadingSearchContent(false);
      console.log("error: ", error);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const setData = async (resultSearch) => {
    console.log("result search", resultSearch);
    if (
      resultSearch.result.event == "undefined" &&
      resultSearch.result.journal == "undefined" &&
      resultSearch.result.guideline == "undefined" &&
      resultSearch.result.cme == "undefined" &&
      resultSearch.result.job == "undefined" &&
      resultSearch.result.obatatoz == "undefined"
    ) {
    } else if (
      resultSearch.result.event != "undefined" &&
      resultSearch.result.journal != "undefined" &&
      resultSearch.result.guideline != "undefined" &&
      resultSearch.result.cme != "undefined" &&
      resultSearch.result.job != "undefined" &&
      resultSearch.result.obatatoz != "undefined"
    ) {
      if (
        _.isEmpty(resultSearch.result.event) == true &&
        _.isEmpty(resultSearch.result.journal) == true &&
        _.isEmpty(resultSearch.result.guideline) == true &&
        _.isEmpty(resultSearch.result.cme) == true &&
        _.isEmpty(resultSearch.result.job) == true &&
        _.isEmpty(resultSearch.result.obatatoz) == true
      ) {
      } else {
        await setDataToState(resultSearch);
      }
    } else {
      await setDataToState(resultSearch);
    }
  };

  const setDataToState = async (resultSearch) => {
    setAllDataEvent(
      !_.isEmpty(resultSearch.result.event) ? resultSearch.result.event : []
    );
    setAllDataJournal(
      !_.isEmpty(resultSearch.result.journal) ? resultSearch.result.journal : []
    );
    setAllDataGuideline(
      !_.isEmpty(resultSearch.result.guideline)
        ? resultSearch.result.guideline
        : []
    );
    setAllDataCme(
      !_.isEmpty(resultSearch.result.cme) ? resultSearch.result.cme : []
    );
    setAllDataJob(
      !_.isEmpty(resultSearch.result.job) ? resultSearch.result.job : []
    );
    setAllDataObatAtoz(
      !_.isEmpty(resultSearch.result.obatatoz)
        ? resultSearch.result.obatatoz
        : []
    );
  };

  const goToScreenSearchAll = async (category) => {
    let data = {
      category: category,
      keyword: "",
    };
    navigation.navigate("SearchAllByKonten", {
      data: data,
      from: "home",
      onRefreshDataSearchKonten: refreshDataSearchKonten,
      triggerBookmark: triggerUpdate,
    });
  };

  const goToScreenBaseOnCategory = async (data, type) => {
    if (
      type == "UpcomingWebinar" ||
      type == "RecordedWebinar" ||
      type == "LiveWebinar"
    ) {
      goToWebinarDetail_searchContent(data);
    } else if (type == "Jurnal") {
      goToJournalDetail_searchContent(data);
    } else if (type == "Event") {
      goToEventDetail_searchContent(data);
    } else if (type == "Cme") {
      goToCMEDetail_searchContent(data);
    } else if (type == "Guideline") {
      goToPDFView_searchContent(data);
    } else if (type == "Job") {
    } else if (type == "ObatAtoz") {
      goToObatAtoz_searchContent(data);
    }
  };

  const goToJournalDetail_searchContent = (data) => {
    navigation.navigate("LearningJournalDetail", {
      data: data,
      fromDeeplink: true,
      onRefreshDataSearchKonten: refreshDataSearchKonten,
      from: "Home",
    });
  };

  const goToWebinarDetail_searchContent = async (data) => {
    let tempData = {
      isPaid: data.exclusive == 1 || data.is_paid == true ? true : false,
      from: "Feeds",
      ...data,
    };

    let dateShowTNC = moment(data.start_date)
      .add(-1, "hours")
      .format("YYYY-MM-DD HH:mm:ss");

    // let isStartShowTNC = isStarted(data.server_date, dateShowTNC)
    let isStartShowTNC = isStarted(serverDate, dateShowTNC);
    let routeNameDestination =
      data.has_tnc == true && isStartShowTNC == true
        ? "WebinarTNC"
        : "WebinarVideo";
    navigation.navigate({
      routeName: routeNameDestination,
      params: tempData,
      key: `webinar-${data.id}`,
    });
  };

  const goToPDFView_searchContent = async (data) => {
    const PdfView = { type: "Navigate", routeName: "PdfView", params: data };
    let availableAttachment = data.attachment != null ? true : false;

    availableAttachment === true
      ? navigation.navigate(PdfView)
      : showErrorMessage();
  };

  const showErrorMessage = () => {
    Toast.show({
      text: "Data not found",
      position: "bottom",
      duration: 3000,
      type: "danger",
    });
  };

  const goToEventDetail_searchContent = async (data) => {
    data.paid = data.is_paid;
    navigation.navigate({
      routeName: "EventDetail",
      params: {
        data: data,
        from: "home",
        onRefreshDataSearchKonten: refreshDataSearchKonten,
      },
      key: `event-${data.id}`,
    });
  };

  const goToCMEDetail_searchContent = async (data) => {
    isDone = data.is_done == true || data.is_done == 1 ? true : false;
    // let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let profile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    let dataSubmitCmeQuiz = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ
    );
    console.log("checkNpaIdiEmpty(profile)", checkNpaIdiEmpty(profile));
    if (checkNpaIdiEmpty(profile) && !_.isEmpty(dataSubmitCmeQuiz) && !isDone) {
      //if (checkNpaIdiEmpty(profile) && !isDone) {
      showAlertFillNpaIDI_searchContent(data);
    } else {
      gotoMedicinusDetail_searchContent(isDone, data);
    }
  };

  const showAlertFillNpaIDI_searchContent = (data) => {
    Alert.alert(
      "Reminder",
      "Please input your Medical ID first before you attempt your next CME Quiz",
      [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: "OK",
          onPress: () => gotoProfile_searchContent(data),
        },
      ],
      { cancelable: true }
    );
  };

  gotoProfile_searchContent = (data) => {
    let from = "home";

    let params = {
      from: from,
      ...data,
      backFromCertificate: from,
      resubmitQuiz: true,
      isFromAlertSearchPage: true,
      onRefreshDataProfile: triggerUpdate,
    };

    navigation.navigate({
      routeName: "ChangeProfile",
      params: params,
    });
  };

  const gotoMedicinusDetail_searchContent = async (isDone, data) => {
    console.log("gotoMedicinusDetail data", data);
    let from = "SearchKonten";
    // let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let profile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    // this.sendAdjust(from.DETAIL)
    // AdjustTracker(AdjustTrackerConfig.CME_SKP_Quiz);
    let paramCertificate = {
      title: data.title,
      certificate: data.certificate,
      isRead: data.status != "invalid" ? true : false,
      // isFromHistory: true,
      from: from,
      isManualSkp: data.type == "offline" ? true : false,
      typeCertificate: data.type,
      //isCertificateAvailable: true,
    };

    if (data.type == "offline") {
      gotoCertificate(paramCertificate, data);
    } else {
      if (isDone) {
        let type = data.type != null ? data.type : "Cme";

        if (type == "event" || type == "webinar") {
          gotoCertificate(paramCertificate, data);
        } else {
          if (checkNpaIdiEmpty(profile) == false) {
            gotoCertificate(paramCertificate, data);
          } else if (checkNpaIdiEmpty(profile) == true) {
            let params = {
              from: from,
              ...data,
              backFromCertificate: from,
              onRefreshDataSearchKonten: triggerUpdate,
            };

            console.log("gotoMedicinusDetail let data", params);

            navigation.navigate({
              routeName: "CmeQuiz",
              params: params,
              key: "goto",
            });
          }
        }
      } else {
        console.log("TO THIS OPTION", data);
        let params = {
          from: from,
          ...data,
          backFromCertificate: from,
          onRefreshDataSearchKonten: triggerUpdate,
        };
        navigation.navigate({
          routeName: "CmeDetail",
          params: params,
          key: `webinar-${data.id}`,
        });
      }
    }
  };

  const gotoCertificate = (paramCertificate, data) => {
    navigation.navigate({
      routeName: "CmeCertificate",
      params: paramCertificate,
      key: `webinar-${data.id}`,
    });
  };

  const gotoCertificate_searchContent = (paramCertificate, data) => {
    navigation.navigate({
      routeName: "CmeCertificate",
      params: paramCertificate,
      key: `webinar-${data.id}`,
    });
  };

  const goToObatAtoz_searchContent = (data) => {
    navigation.navigate("DetailAtoZ", {
      dataObat: data,
    });
  };

  const triggerUpdate = () => {
    setIsTriggerUpdate(true);
    triggerUpdateRef.current = true;
  };

  const renderHeaderHome = () => {
    return (
      <View>
        {renderProfileMenuHome()}
        {renderMainMenuHome()}
        {isLoadingBanner == true || !_.isEmpty(dataBanner) ? (
          <View style={styles.containerBanner}>
            <ImageCarousel
              dataBanner={dataBanner}
              onPress={(link) => {
                onPressBanner(link);
                AdjustTracker(AdjustTrackerConfig.Homepage_Sliding_Banner);
              }}
              isShimmer={isLoadingBanner}
            />
          </View>
        ) : null}

        {isLoadingSearchContent && (
          <CardItemSearch
            type="shimmer"
            data={{
              flag_bookmark: "false",
            }}
          />
        )}
        {!isLoadingSearchContent &&
          categoryListData.map((value, i) => {
            if (value == "upcoming_webinar") {
              return (
                <HorizontalSliderCard
                  title="Upcoming Webinar"
                  type="UpcomingWebinar"
                  data={upcomingWebinarHomapageList}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() =>
                    goToScreenSearchAll("UpcomingWebinar")
                  }
                  goToScreenBaseOnCategory={(item) =>
                    goToScreenBaseOnCategory(item, "UpcomingWebinar")
                  }
                />
              );
            } else if (value == "live_webinar") {
              return (
                <HorizontalSliderCard
                  title="Live Webinar"
                  type="LiveWebinar"
                  data={liveWebinarHomapageList}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() => goToScreenSearchAll("LiveWebinar")}
                  goToScreenBaseOnCategory={(item) =>
                    goToScreenBaseOnCategory(item, "LiveWebinar")
                  }
                />
              );
            } else if (value == "webinar") {
              return (
                <HorizontalSliderCard
                  title="Rekaman Webinar"
                  type="RecordedWebinar"
                  data={recordedWebinarHomapageList}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() =>
                    goToScreenSearchAll("RecordedWebinar")
                  }
                  goToScreenBaseOnCategory={(item) =>
                    goToScreenBaseOnCategory(item, "RecordedWebinar")
                  }
                />
              );
            } else if (value == "event") {
              return (
                <HorizontalSliderCard
                  title="Event"
                  type="Event"
                  data={allDataEvent}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() => goToScreenSearchAll("Event")}
                  goToScreenBaseOnCategory={
                    (item) => {
                      item.paid = item.is_paid;
                      navigation.navigate({
                        routeName: "EventDetail",
                        params: {
                          data: item,
                          from: "home",
                          onRefreshDataSearchKonten: triggerUpdate,
                        },
                        key: `event-${item.id}`,
                      });
                    }
                    // goToScreenBaseOnCategory(item, "Event")
                  }
                />
              );
            } else if (value == "journal") {
              return (
                <HorizontalSliderCard
                  title="Jurnal"
                  type="Jurnal"
                  data={allDataJournal}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() => goToScreenSearchAll("Jurnal")}
                  goToScreenBaseOnCategory={
                    (item) =>
                      navigation.navigate("LearningJournalDetail", {
                        data: item,
                        fromDeeplink: true,
                        onRefreshDataSearchKonten: triggerUpdate,
                        from: "Home",
                      })
                    // goToScreenBaseOnCategory(item, "Jurnal")
                  }
                />
              );
            } else if (value == "guideline") {
              return (
                <HorizontalSliderCard
                  title="Guideline"
                  type="Guideline"
                  data={allDataGuideline}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() => goToScreenSearchAll("Guideline")}
                  goToScreenBaseOnCategory={(item) =>
                    goToScreenBaseOnCategory(item, "Guideline")
                  }
                />
              );
            } else if (value == "cme") {
              return (
                <HorizontalSliderCard
                  title="CME"
                  type="Cme"
                  data={allDataCme}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() => goToScreenSearchAll("Cme")}
                  goToScreenBaseOnCategory={(item) =>
                    goToScreenBaseOnCategory(item, "Cme")
                  }
                />
              );
            } else if (value == "obatatoz") {
              return (
                <HorizontalSliderCard
                  title="Obat A - Z"
                  type="ObatAtoz"
                  data={allDataObatAtoz}
                  serverDate={serverDate}
                  limit={limitSlider}
                  goToScreenSearchAll={() => goToScreenSearchAll("ObatAtoz")}
                  goToScreenBaseOnCategory={(item) =>
                    goToScreenBaseOnCategory(item, "ObatAtoz")
                  }
                />
              );
            }
          })}

        {!isFetchingFeeds ? renderGroupDataFeeds() : renderShimmerHeaderFeeds()}
      </View>
    );
  };

  const renderShimmerHeaderFeeds = () => {
    return (
      <SkeletonPlaceholder
        highlightColor={"#c5c5c5"}
        backgroundColor={"#ebebeb"}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: 20,
            marginHorizontal: 16,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <View style={{ height: 24, width: 24, borderRadius: 12 }} />
            <View
              style={{
                marginLeft: 16,
                height: 24,
                width: 180,
                borderRadius: 100 / 2,
              }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    );
  };

  const handleLoadMore = () => {
    if (isEmptyFeedsData == true || isFetchingFeeds == true) {
      return;
    }
    getDataFeeds();
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter(isFetchingFeeds),
        // { height: isFetching == true ? 80 : 0 },
        { width: isFetchingFeeds == true ? "100%" : null },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailedFeeds == true && pageFeeds > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    // return <Loader visible={isFetching} transparent />;
    if (isFetchingFeeds == true) {
      return (
        <View>
          <CardItemSearch
            type="shimmerFeeds"
            data={{
              flag_bookmark: "false",
            }}
          />
        </View>
      );
    }
  };

  const onRefreshData = () => {
    flatlistRef.current.scrollToOffset({ animated: true, offset: 0 });
    console.log("IsRefreshing");
    setDataFeeds([]);
    setPageFeeds(1);
    setIsFetchingFeeds(true);
    setisEmptyFeedsData(false);
    setIsRefresh(true);
  };

  const renderEmptyFeeds = () => {
    if (!isFetchingFeeds) {
      return (
        <View style={styles.containerEmptyFeeds}>
          <View style={styles.containerInner}>
            <Text style={styles.textEmptyFeeds}>Belum Ada Feeds</Text>
          </View>
        </View>
      );
    }
  };

  const getDataFeeds = async () => {
    if (pageFeeds == 1) {
      AdjustTracker(AdjustTrackerConfig.Infinite_Scroll_Onload);
    }
    setIsFetchingFeeds(true);
    setIsRefreshFeeds(false);
    setIsFailedFeeds(false);
    console.log("pageFeeds -> ", pageFeeds);
    try {
      let params = {
        page: pageFeeds,
        limit: limitFeeds,
      };
      let response = await getFeedsNew(params);
      console.log("response getFeedsNew: ", response);

      if (response.acknowledge) {
        let dataResult = response.result.data;
        console.log("dataResult : ", response.result.data);
        if (pageFeeds == 1 && _.isEmpty(dataResult)) {
          setIsDataNotFoundFeeds(true);
          console.log("notfound");
        }
        setisEmptyFeedsData(_.isEmpty(dataResult) ? true : false);
        setDataFeeds(
          pageFeeds == 1 ? dataResult : [...dataFeeds, ...dataResult]
        );
        setPageFeeds(pageFeeds + 1);
        setIsFailedFeeds(false);
        setIsFetchingFeeds(false);
      } else {
        setIsFailedFeeds(true);
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
        setIsFetchingFeeds(false);
      }
      setIsFetchingFeeds(false);
    } catch (error) {
      setIsFailedFeeds(true);
      setIsFetchingFeeds(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const _renderData = (item) => {
    return (
      <View style={{ paddingHorizontal: 16 }}>
        <CardItemFeed
          navigation={navigation}
          paramsData={item.item}
          serverDate={serverDate}
          onRefreshData={triggerUpdate}
        />
      </View>
    );
  };

  const renderGroupDataFeeds = () => {
    return (
      <View>
        <View style={styles.containerLineInfiniteScroll} />
        <View style={styles.containerTextRecommendation}>
          <IconRecommendationData style={{ marginRight: 8 }} />
          <Text style={styles.textRecommendation}>Rekomendasi Untuk Anda</Text>
        </View>
      </View>
    );
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow>
          <Body style={styles.bodyContainer}>
            <SearchBarField
              type="searchHome"
              textplaceholder="Cari Konten D2D"
              button="true"
              onPress={() => {
                navigation.navigate("SearchKonten", {
                  from: "home",
                  onRefreshDataSearchKonten: refreshDataSearchKonten,
                  triggerBookmark: triggerUpdate,
                });
                AdjustTracker(AdjustTrackerConfig.Homepage_Cari_Konten);
              }}
            />

            <View style={styles.viewNotifQR}>
              <TouchableOpacity
                onPress={() => {
                  gotoManageTemanDiabetes();
                  AdjustTracker(AdjustTrackerConfig.Homepage_Kelola);
                }}
                style={{}}
              >
                <IconKelolaTD />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  gotoScanScreen();
                  AdjustTracker(AdjustTrackerConfig.Homepage_Scan_QR);
                }}
                style={{}}
              >
                <IconQrscan />
              </TouchableOpacity>
              <View style={{ marginHorizontal: 4 }} />
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Notification");
                  AdjustTracker(AdjustTrackerConfig.Homepage_Notification);
                }}
                style={{}}
              >
                <IconNotification />
              </TouchableOpacity>
              <View style={{ marginRight: 5 }} />
            </View>
          </Body>
        </Header>
      </SafeAreaView>
      <View style={{ backgroundColor: "#FFFFFF", flex: 1 }}>
        <FlatList
          contentContainerStyle={{
            // paddingHorizontal: 16,
            // paddingTop: 20,
            paddingBottom: 100,
          }}
          ref={flatlistRef}
          showsVerticalScrollIndicator={false}
          data={dataFeeds}
          keyExtractor={(item, index) => index.toString()}
          renderItem={_renderData}
          onEndReachedThreshold={0.5}
          onEndReached={handleLoadMore}
          ListFooterComponent={_renderItemFooter()}
          refreshing={false}
          // onRefresh={() => onPullRefresh()}
          refreshControl={
            <RefreshControl refreshing={false} onRefresh={onPullRefresh} />
          }
          ListEmptyComponent={renderEmptyFeeds()}
          ListHeaderComponent={renderHeaderHome()}
        />
      </View>

      <ReactModal
        hasBackdrop={true}
        avoidKeyboard={true}
        backdropOpacity={0.8}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}
        isVisible={isShowPopupPrivacy}
        onBackdropPress={handleCappingPopup}
      >
        <ModalPopup
          onPressClosePopup={handleCappingPopup}
          isChecked={isCheckedPopupPrivacy}
          onChecked={() => toggleChecked()}
          data={dataPopupPrivacy}
          handleSubmitPopup={handleSubmitPopup}
          typeModal="privacy"
          from="home"
        />
      </ReactModal>
      {renderBottomSheet()}
      <ChannelModalJoin
        navigation={navigation}
        deeplinkChannelObj={deeplinkChannelObj}
        channelJoinModalRef={channelJoinModalRef}
        isShowLoader={isShowLoader}
        isLoadingBanner={isLoadingBanner}
        isLoadingSearchContent={isLoadingSearchContent}
        isNotificationOpenCheckCompleted={isNotificationOpenCheckCompleted}
        screen="home"
        onClosedParam={() => {
          setDeeplinkChannelObj({ channelXid: null, navigateTo: null });
          navigation.setParams({ hideTab: false });
        }}
        onOpenedParam={() => navigation.setParams({ hideTab: true })}
      />
    </Container>
  );
};

export default Home;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  containerChannel: {
    marginHorizontal: 16,
    marginBottom: 8,
    flexDirection: "row",
    alignItems: "baseline",
    flex: 1,
    justifyContent: "space-between",
  },
  profileMenu: {
    marginTop: 20,
    flex: 1,
    flexDirection: "row",
    marginHorizontal: 16,
  },
  menuGrid: {
    flexDirection: "row",
    flex: 0.5,
    width: Dimensions.get("window").width,
    justifyContent: "space-evenly",
    marginVertical: 12,
  },
  iconNotification: {
    justifyContent: "center",
    alignItems: "center",
  },
  textLihatProfil: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#FFFFFF",
    marginVertical: 10,
  },
  buttonLihatProfile: {
    backgroundColor: "#0771CD",
    alignItems: "center",
    marginHorizontal: 16,
    borderRadius: 5,
    marginTop: 16,
  },
  textRumahSakit: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#666666",
    marginLeft: 10,
  },
  textSpecialist: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#666666",
    marginLeft: 6,
  },
  textPoinSKP: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#666666",
    marginLeft: 6,
  },
  viewPoinPoin: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 4,
  },
  textNamaDokter: {
    fontSize: 16,
    fontFamily: "Roboto-Medium",
    color: "#000000",
    marginVertical: 4,
  },
  textRekomendasiChannel: {
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    color: "#000000",
    alignItems: "flex-start",
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textLihatSemua: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#0771CD",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    lineHeight: 16,
    letterSpacing: 0.34,
  },
  containerBanner: {
    marginBottom: 32,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 6,
  },
  viewNotifQR: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 5,
  },
  containerListCardChannel: {
    paddingLeft: 8,
    marginBottom: 16,
  },
  listChannelWrapper: {
    flexDirection: "row",
  },
  itemCardChannelWrapper: {
    paddingTop: 8,
    marginRight: 16,
    marginBottom: 8,
    width: Dimensions.get("window").width / 2.5,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  containerGroup: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 16,
    justifyContent: "space-between",
  },
  containerTitle: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
  },
  textGroup: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textLihatSemua: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#0771CD",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    lineHeight: 16,
    letterSpacing: 0.34,
  },
  containerItemFooter: (isFetching) => [
    {
      justifyContent: isFetching ? null : "center",
      alignItems: isFetching ? null : "center",
      paddingHorizontal: isFetching ? 16 : null,
    },
  ],
  textRecommendation: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  containerTextRecommendation: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    marginBottom: 21,
    paddingHorizontal: 16,
    marginTop: 0,
  },
  contentContainer: {
    backgroundColor: "#FFFFFF",
    opacity: 1,
  },
  notFoundContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 4,
    backgroundColor: "#FFFFFF",
    opacity: 1,
  },
  notFoundText: {
    fontFamily: "Roboto-Medium",
    fontSize: 20,
    color: "#000000",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    textAlign: "center",
    marginTop: 32,
    lineHeight: 24,
    letterSpacing: 0.14,
    // textAlignVertical: "center",
  },
  anotherKeywordText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    // textAlignVertical: "center",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  containerLineInfiniteScroll: {
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
    marginBottom: 32,
  },
});
