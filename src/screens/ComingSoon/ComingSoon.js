import { Container, Content, View, Text } from "native-base";
import React, { useState, useEffect } from "react";
import { StyleSheet, BackHandler } from "react-native";
import { IconComingSoon } from "../../assets";
import _, { result } from "lodash";
import { Loader } from "../../../app/components";
import platform from "../../../theme/variables/platform";
import { HeaderToolbar } from "../../components";

const ComingSoon = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);

  useEffect(() => {
    if (params.isFromBottomNav != true) {
      BackHandler.addEventListener("hardwareBackPress", handleBackButton);
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
      };
    }
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <HeaderToolbar
        onPress={params.isFromBottomNav ? undefined : handleBackButton}
        title={params.title}
      />
      <Content style={styles.contentContainer}>
        <View style={styles.notFoundContainer}>
          <IconComingSoon />
          <Text style={styles.notFoundText}>Segera Hadir</Text>
          <Text style={styles.anotherKeywordText}>
            Kami sedang mempersiapkan fitur ini untuk Anda. Tetap selalu update.
          </Text>
        </View>
      </Content>
    </Container>
  );
};

export default ComingSoon;

const styles = StyleSheet.create({
  contentContainer: {
    backgroundColor: "#FFFFFF",
    opacity: 1,
  },
  notFoundContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 4,
    backgroundColor: "#FFFFFF",
    opacity: 1,
  },
  notFoundText: {
    fontFamily: "Roboto-Medium",
    fontSize: 20,
    color: "#000000",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 32,
    lineHeight: 24,
    letterSpacing: 0.14,
    // textAlignVertical: "center",
  },
  anotherKeywordText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    // textAlignVertical: "center",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
});
