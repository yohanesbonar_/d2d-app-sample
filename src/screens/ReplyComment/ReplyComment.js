import { Container, Content, View, Text, Icon, Toast } from "native-base";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  BackHandler,
  FlatList,
  Platform,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  RefreshControl,
} from "react-native";
import _, { result } from "lodash";
import { Loader } from "../../../app/components";
import {
  CardItemComment,
  CardItemCommentInComment,
  CardItemForum,
  HeaderToolbar,
} from "../../components";
import moment from "moment";
import { getDetailComment } from "../../utils/network/forum/detailComment";
import { getDataListReply } from "../../utils/network/forum/listReplyInDetailComment";
import { likeCommentReply } from "../../utils/network/forum/likeCommentReply";
import { postReply } from "../../utils/network/forum/postReply";
import { IconSendBlue, IconSendGrey } from "../../assets";
import { removeEmojis, useDebouncedEffect } from "../../utils";
import platform from "../../../theme/variables/platform";

const ReplyComment = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [dataMainComment, setDataMainComment] = useState({});
  const [dataSbcChannel, setDataSbcChannel] = useState(params.sbcChannel);
  const [dataXid, setDataXid] = useState(params.item.xid);
  const [serverDate, setServerDate] = useState("");
  const [dataListReplies, setDataListReplies] = useState([]);
  const [mainCommentName, setMainCommentName] = useState("");
  const [mainCommentLogo, setMainCommentLogo] = useState("");
  const [mainCommentSpecialization, setMainCommentSpecialization] = useState(
    ""
  );

  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isEmptyData, setIsEmptyData] = useState(true);
  const [pageReply, setPageReply] = useState(1);
  const [likedMainComment, setLikedMainComment] = useState(false);

  const [xidComment, setXidComment] = useState("");
  const [xidReplyTo, setXidReplyTo] = useState("");
  const [commentValue, setCommentValue] = useState("");
  const [commentFrom, setCommentFrom] = useState("POST");

  const [maxLength, setMaxLength] = useState(2200);
  const [isTriggerSend, setIsTriggerSend] = useState(false);

  const [numOfLines, setNumOfLines] = useState(1);
  const [isTextInputFocused, setTextInputFocus] = useState(false);

  useEffect(() => {
    getAllData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    navigation.state.params.onRefresh();
    return true;
  };

  const getAllData = async () => {
    let temp = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    setServerDate(temp);
    await getDataMainComment();
    await getDataAllReply();
  };

  const getDataMainComment = async () => {
    let response = {};
    setIsShowLoader(true);
    try {
      response = await getDetailComment(dataXid, dataSbcChannel);

      console.log("result DETAIL MAIN COMMENT", response);
      if (response.header.message == "Success") {
        setDataMainComment(response.data);
        setMainCommentName(response.data.by.name);
        setLikedMainComment(response.data.liked);
        setXidReplyTo(response.data.xid);
        setMainCommentLogo(response.data.by.display_picture);
        setMainCommentSpecialization(response.data.by.spesialization);
        setIsShowLoader(false);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const getDataAllReply = async () => {
    setIsRefresh(false);
    setIsFailed(false);
    setIsFetching(true);

    let response = {};
    let result = [];
    let paramss = {
      paginate: pageReply,
      limit: 10,
    };
    console.log("pageReply ->", pageReply);
    try {
      response = await getDataListReply(dataXid, dataSbcChannel, paramss);
      result = response.data.data;
      console.log("result LIST COMMENT REPLY PAGE", response);
      if (response.header.message == "Success") {
        if (pageReply == 1 && _.isEmpty(result)) {
          setIsDataNotFound(true);
        }
        setIsEmptyData(_.isEmpty(result) ? true : false);
        setDataListReplies(
          pageReply == 1 ? [...result] : [...dataListReplies, ...result]
        );
        setIsFailed(false);
        setIsFetching(false);
        setPageReply(pageReply + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const onRefreshdata = () => {
    setDataMainComment({});
    setDataListReplies([]);
    setPageReply(1);
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      getAllData();
    }
  }, [isRefresh]);

  const handleLoadMore = async () => {
    if (isEmptyData == true) {
      return;
    }

    getDataAllReply();
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailed == true && pageReply > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetching} transparent />;
  };

  const onPressPostReplyTo = (item) => {
    console.log("ITEM REPLY -> ", item);
    this.textInputRef.focus();
  };

  useDebouncedEffect(
    () => {
      sendToApi();
    },
    500,
    [isTriggerSend]
  );

  const onPressSend = () => {
    if (
      !_.isEmpty(commentValue) &&
      commentValue.length <= maxLength &&
      /\S/.test(commentValue)
    ) {
      this.textInputRef.blur();
      setTextInputFocus(false);
      // sendToApi();
      setIsShowLoader(true);
      setNumOfLines(1);
      setIsTriggerSend(!isTriggerSend);
    }
  };

  const sendToApi = async () => {
    let response = {};
    try {
      response = await postReply(
        params.xidMain,
        commentValue,
        xidReplyTo,
        dataSbcChannel
      );

      console.log("result Send Comment", response);
      if (response.header.message == "Success") {
        setCommentValue("");
        await onRefreshdata();
      } else {
        setIsShowLoader(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsShowLoader(false);
      return (
        <View>
          <Text style={styles.textErrorMaxValue}>
            Anda telah melebihi limit {maxLength} karakter maksimum.
          </Text>
        </View>
      );
    }
  };

  const renderErrorMaxValue = () => {
    if (commentValue.length > maxLength) {
      return (
        <View>
          <Text style={styles.textErrorMaxValue}>
            Anda telah melebihi limit {maxLength} karakter maksimum.
          </Text>
        </View>
      );
    }
  };

  const renderData = ({ item }) => {
    return (
      <View>
        <CardItemCommentInComment
          titleReply={item.by.name}
          rolesReply={item.by.spesialization}
          currentDateReply={item.created_at}
          serverDateReply={serverDate}
          imageUserReply={item.by.display_picture}
          commentReply={item.comment}
          totalLikeReply={item.total_likes}
          flagLikeReply={item.liked}
          type="ReplyComment"
          xid={item.xid}
          sbcChannel={dataSbcChannel}
        />
      </View>
    );
  };

  const renderReplyCommentFor = () => {
    return (
      <View style={styles.containerOutReplyTo}>
        <View style={styles.containerTextReplyTo}>
          <Text style={styles.textReply}>Membalas </Text>
          <Text numberOfLines={1} style={styles.textReplyToName}>
            {mainCommentName}
          </Text>
        </View>
      </View>
    );
  };

  const changeLines = (e) => {
    let temp = e.nativeEvent.contentSize.height / 16;
    // console.log("Log ChangeLines e -> ", e);
    // console.log(
    //   "Log ChangeLines e.nativeEvent.contentSize.height -> ",
    //   e.nativeEvent.contentSize.height
    // );
    console.log("Log ChangeLines Temp -> ", temp);
    setNumOfLines(temp);
  };

  const renderFieldComment = () => {
    return (
      <View style={styles.containerOutFieldInput(isTextInputFocused)}>
        {renderReplyCommentFor()}
        <View style={styles.containerInFieldInput(commentValue, maxLength)}>
          <TextInput
            style={styles.containerField}
            placeholder="Berikan komentar Anda…"
            placeholderTextColor="#666666"
            multiline={true}
            ref={(ref) => (this.textInputRef = ref)}
            onChangeText={(text) => {
              // setCommentValue(removeEmojis(text));
              setCommentValue(text);
            }}
            numberOfLines={numOfLines}
            onContentSizeChange={(e) => changeLines(e)}
            value={commentValue}
            // keyboardType={
            //   Platform.OS === "ios" ? "ascii-capable" : "visible-password"
            // }
            onFocus={() => setTextInputFocus(true)}
            onBlur={() => setTextInputFocus(false)}
          />
          <Text style={styles.textCountDown(commentValue, maxLength)}>
            {maxLength - commentValue.length}
          </Text>
          <TouchableOpacity
            style={styles.buttonSendContainer}
            onPress={() => onPressSend()}
          >
            {commentValue != "" &&
            commentValue.length <= maxLength &&
            /\S/.test(commentValue) ? (
              <IconSendBlue />
            ) : (
              <IconSendGrey />
            )}
          </TouchableOpacity>
        </View>
        {renderErrorMaxValue()}
      </View>
    );
  };

  const _renderEmptyItemComment = () => {
    if (isEmptyData && !isFetching) {
      return (
        <Text style={{ textAlign: "center", marginTop: 25, marginBottom: 20 }}>
          Tidak ada balasan lainnya
        </Text>
      );
    }

    return null;
  };

  const renderHeaderList = () => {
    return (
      <View>
        <CardItemComment
          name={mainCommentName}
          commentDescription={dataMainComment.comment}
          speacialistName={mainCommentSpecialization}
          currentDdate={dataMainComment.created_at}
          serverDate={serverDate}
          imageUser={mainCommentLogo}
          totalLikes={dataMainComment.total_likes}
          flagLikeComment={likedMainComment}
          type="ReplyComment"
          xid={dataMainComment.xid}
          sbcChannel={dataSbcChannel}
          onPressReply={() => onPressPostReplyTo(dataMainComment)}
        />
      </View>
    );
  };
  return (
    <Container>
      <Loader visible={isShowLoader} />
      <HeaderToolbar onPress={handleBackButton} title="Balasan" />
      {!isShowLoader ? (
        <View style={styles.contentContainer}>
          <FlatList
            style={{}}
            data={dataListReplies}
            keyExtractor={(item, index) => index.toString()}
            renderItem={renderData}
            onEndReachedThreshold={0.5}
            onEndReached={handleLoadMore}
            ListFooterComponent={_renderItemFooter()}
            ListEmptyComponent={_renderEmptyItemComment()}
            ListHeaderComponent={renderHeaderList()}
            onRefresh={() => {
              onRefreshdata();
            }}
            refreshing={false}
          />
        </View>
      ) : null}

      {!isShowLoader ? (
        Platform.OS === "android" ? (
          renderFieldComment()
        ) : (
          <KeyboardAvoidingView behavior="padding">
            {renderFieldComment()}
          </KeyboardAvoidingView>
        )
      ) : null}
    </Container>
  );
};

export default ReplyComment;

const styles = StyleSheet.create({
  contentContainer: {
    backgroundColor: "#FFFFFF",
    opacity: 1,
    flex: 1,
  },
  lineContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
  },
  containerField: {
    padding: 0,
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    lineHeight: 24,
    letterSpacing: 0.15,
    flex: 1,
  },
  containerInFieldInput: (commentValue, maxLength) => ({
    justifyContent: "space-between",
    flexDirection: "row",
    marginTop: 16,
    marginHorizontal: 16,
    marginBottom: 8,
    backgroundColor: "#EBEBEB",
    borderRadius: 4,
    alignItems: "flex-end",
    padding: 16,
    maxHeight: 142,
    borderWidth:
      commentValue != "" && commentValue.length > maxLength ? 1 : null,
    borderColor:
      commentValue != "" && commentValue.length > maxLength ? "#B00020" : null,
  }),
  containerOutFieldInput: (isFocused) => ({
    backgroundColor: "#FFF",
    paddingBottom: 12,
    paddingTop: 16,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.02,
    shadowRadius: 4,
    marginBottom:
      platform.platform == "ios" &&
      platform.deviceHeight >= 812 &&
      platform.deviceWidth >= 375
        ? isFocused
          ? 0
          : 24
        : 0,
  }),
  buttonSend: { color: "#666666" },
  buttonSendContainer: { paddingLeft: 16, marginBottom: 0 },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  textErrorMaxValue: {
    fontFamily: "Roboto-Regular",
    fontSize: 12,
    color: "#D01E53",
    lineHeight: 16,
    letterSpacing: 0.19,
    marginLeft: 16,
  },
  textCountDown: (commentValue, maxLength) => ({
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color:
      commentValue != "" && commentValue.length > maxLength
        ? "#D01E53"
        : "#666666",
    lineHeight: 24,
    letterSpacing: 0.15,
    marginLeft: 8,
  }),
  containerOutReplyTo: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  containerTextReplyTo: {
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 16,
    flex: 1,
  },
  textReply: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    color: "#000000",
    lineHeight: 20,
    letterSpacing: 0.25,
  },
  textReplyToName: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#000000",
    lineHeight: 20,
    letterSpacing: 0.25,
    flex: 1,
  },
});
