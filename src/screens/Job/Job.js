import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
  Icon,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Dimensions,
  Image,
  Linking,
  Animated,
  RefreshControl,
  Platform,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import {
  IconBack,
  IconCloseRoundedWhite,
  IconFiltered,
  IconFilterSearch,
  IconInstagramJobDetail,
} from "../../assets";
import _, { filter, initial, result, sortBy } from "lodash";
import {
  BottomSheet,
  CardItemSearch,
  CarditemTabList,
  EmptyDataKonferensi,
} from "../../components/molecules";
import { Modalize } from "react-native-modalize";
import platform from "../../../theme/variables/platform";
import { Loader, Tag } from "../../../app/components";
import {
  ScrollView,
  TouchableNativeFeedback,
} from "react-native-gesture-handler";
import { getAllJob } from "../../utils";
import { getJobType } from "../../utils/network/job/getJobType";
import { ArrowBackButton } from "../../components/atoms";
import { getJobTypePublic } from "../../utils/network/job/getJobTypePublic";
import { getPublicJob } from "../../utils/network/job/publicJob";
import { getChannelDetail } from "../../utils/network/channel/channelDetail";
import { subscribeChannel } from "../../utils/network/channel/subscribeChannel";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";
import { share } from "../../../app/libs/Common";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const Job = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [selectedTabList, setSelectedTabList] = useState({});
  const [dataList, setDataList] = useState([]);
  const [filterValue, setFilterValue] = useState({});

  const [isFetching, setIsFetching] = useState(true);
  const [isFailed, setIsFailed] = useState(false);
  const [pageJob, setPageJob] = useState(1);
  const [isEmptyData, setIsEmptyData] = useState(false);
  const [mainDataList, setMainDataList] = useState([]);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [jobType, setJobType] = useState([]);
  const [channel, setChannel] = useState(null);
  const [isOnTop, setIsOnTop] = useState(true);
  const [shareUrl, setShareUrl] = useState(null);

  useEffect(() => {
    initData();
    AdjustTracker(AdjustTrackerConfig.Forum_Job_Start);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const initData = async () => {
    let item = {
      id: 0,
      name: "SEMUA",
    };
    setSelectedTabList(item);
    await getDataType();
  };

  useEffect(() => {
    onRefreshListJob();
  }, [selectedTabList]);

  const getDataType = async () => {
    try {
      if (params.from && params.from == "home") {
        response = await getJobTypePublic();
      } else {
        response = await getJobType(params.sbcChannel);
      }
      console.log("response job", response);
      if (response.header.message == "Success") {
        setJobType(response.data);
      } else {
        Toast.show({
          text: "Something went wrong!! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const getDataJob = async () => {
    setIsRefresh(false);
    setIsFailed(false);
    setIsFetching(true);
    console.log("Page JOB -> ", pageJob);
    let response = {};
    let result = [];
    let paramsTemp = {};
    if (selectedTabList.id == 0 || selectedTabList.name == "SEMUA") {
      if (filterValue.sortBy != "" && filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          specialists: filterValue.specialistID,
          sort_by: filterValue.sortBy,
        };
      } else if (filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          specialists: filterValue.specialistID,
        };
      } else if (filterValue.sortBy != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          sort_by: filterValue.sortBy,
        };
      } else {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
        };
      }
    } else if (selectedTabList.id == 1) {
      if (filterValue.sortBy != "" && filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 1,
          specialists: filterValue.specialistID,
          sort_by: filterValue.sortBy,
        };
      } else if (filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 1,
          specialists: filterValue.specialistID,
        };
      } else if (filterValue.sortBy != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 1,
          sort_by: filterValue.sortBy,
        };
      } else {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 1,
        };
      }
    } else if (selectedTabList.id == 2) {
      if (filterValue.sortBy != "" && filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 2,
          specialists: filterValue.specialistID,
          sort_by: filterValue.sortBy,
        };
      } else if (filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 2,
          specialists: filterValue.specialistID,
        };
      } else if (filterValue.sortBy != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 2,
          sort_by: filterValue.sortBy,
        };
      } else {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: 2,
        };
      }
    } else if (
      selectedTabList.id != 0 &&
      selectedTabList.id != 1 &&
      selectedTabList.id != 2
    ) {
      if (filterValue.sortBy != "" && filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: selectedTabList.id,
          specialists: filterValue.specialistID,
          sort_by: filterValue.sortBy,
        };
      } else if (filterValue.specialistID != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: selectedTabList.id,
          specialists: filterValue.specialistID,
        };
      } else if (filterValue.sortBy != "") {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: selectedTabList.id,
          sort_by: filterValue.sortBy,
        };
      } else {
        paramsTemp = {
          paginate: pageJob,
          limit: 10,
          types: selectedTabList.id,
        };
      }
    }

    console.log("paramsTemp ->>", paramsTemp);

    try {
      if (params.from && params.from == "home") {
        response = await getPublicJob(paramsTemp);
      } else {
        response = await getAllJob(paramsTemp, params.sbcChannel);
      }
      result = response.data.data;
      if (response.header.message == "Success") {
        if (pageJob == 1 && _.isEmpty(result)) {
          setIsDataNotFound(true);
        }
        console.log("result DataList", result);
        setIsEmptyData(_.isEmpty(result) ? true : false);
        setDataList(pageJob == 1 ? [...result] : [...dataList, ...result]);

        setIsFailed(false);
        setIsFetching(false);
        setPageJob(pageJob + 1);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsFetching(false);
        setIsFailed(true);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsFetching(false);
      setIsFailed(true);
    }
  };

  const tabSemua = [
    {
      id: 0,
      name: "SEMUA",
    },
  ];

  const onRefreshListJob = () => {
    setDataList([]);
    // setJobType([]);
    setPageJob(1);
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    setIsRefresh(true);
    setShareUrl(null);
  };

  useEffect(() => {
    if (isRefresh) {
      getDataJob();
    }
  }, [isRefresh]);

  const handleLoadMore = () => {
    if (isEmptyData == true || isFetching == true) {
      return;
    }
    getDataJob();
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailed == true && pageJob > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetching} transparent />;
  };

  const _renderEmptyItemJob = () => {
    if (isEmptyData && _.isEmpty(dataList)) {
      return (
        <View
          style={{
            flex: 1,
            alignSelf: "center",
            justifyContent: "center",
          }}
        >
          <EmptyDataKonferensi
            title="Belum Ada Pekerjaan"
            subtitle="Selalu ada pekerjaan yang menarik disini. Tetap selalu update dan coba datang kembali nanti."
          />
        </View>
      );
    }

    return null;
  };

  const onPressTabFilter = (item) => {
    setSelectedTabList(item);
    if (item.name == "SEMUA") {
      AdjustTracker(AdjustTrackerConfig.Forum_Job_Tab_Semua);
    } else if (item.name == "MODERATOR") {
      AdjustTracker(AdjustTrackerConfig.Forum_Job_Tab_Moderator);
    } else if (item.name == "SPEAKER") {
      AdjustTracker(AdjustTrackerConfig.Forum_Job_Tab_Speaker);
    }
  };

  const renderTabFilter = () => {
    let temp = [...tabSemua, ...jobType];

    return (
      <View style={styles.containerChannel}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.listChannelWrapper}>
            {temp.map((item) => {
              return (
                <CarditemTabList
                  item={item.id}
                  title={item.name}
                  selectedTabList={selectedTabList.id}
                  onPress={() => onPressTabFilter(item)}
                />
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  };

  const _renderData = ({ item }) => {
    return (
      <CardItemSearch
        type="JobArtikel"
        data={item}
        onPress={() => {
          onJobItemPressed(item);
          AdjustTracker(AdjustTrackerConfig.Forum_Job_Open);
        }}
        onPressShareProps={() => {
          if (params.from && params.from == "home") {
            if (item.channel && !item.channel.subscribed) {
              setShareUrl(item.share_link_app ? item.share_link_app : null);
              setChannel(item);
              onOpen();
            } else {
              share(item.share_link_app ? item.share_link_app : null);
            }
          } else {
            share(item.share_link_app ? item.share_link_app : null);
          }
        }}
      />
    );
  };

  const onJobItemPressed = (item) => {
    setShareUrl(null);
    if (params.from && params.from == "home") {
      if (item.channel && !item.channel.subscribed) {
        setChannel(item);
        onOpen();
      } else {
        getChannelSbc(false, item, item.channel.xid);
      }
    } else {
      navigateToJobDetail(false, item, null);
    }
  };

  const navigateToJobDetail = (isNewSubscribe, item, sbcChannel) => {
    let paramsData;
    if (params.from && params.from == "home") {
      paramsData = {
        item: item,
        sbcChannel: sbcChannel,
        isNewSubscribe: isNewSubscribe,
        onBack: onRefreshListJob,
      };
    } else {
      paramsData = {
        item: item,
        sbcChannel: params.sbcChannel,
        isNewSubscribe: false,
        onBack: onRefreshListJob,
      };
    }

    if (item.type.name == "TULIS ARTIKEL") {
      navigation.navigate("JobTulisArtikel", paramsData);
    } else if (item.type.name == "REVIEW ARTIKEL") {
      navigation.navigate("JobReviewArtikel", paramsData);
    } else {
      navigation.navigate("JobDetail", paramsData);
    }
  };

  const onSelect = async (data) => {
    setFilterValue(data.filterValue);
  };

  useEffect(() => {
    // onRefresh();
    console.log("data filterValue ->>>>", filterValue);
  }, [filterValue]);

  const iconFilterRender = () => {
    if (
      _.isEmpty(filterValue) ||
      (filterValue.sortBy == "" && filterValue.specialistID == "")
    ) {
      return (
        <View>
          <IconFilterSearch />
        </View>
      );
    } else {
      return (
        <View>
          <IconFiltered />
        </View>
      );
    }
  };

  const onPressButtonJoinChannel = async () => {
    try {
      let params = {
        xid: channel.channel.xid,
      };
      response = await subscribeChannel(params);
      if (response.header.message == "Success") {
        if (shareUrl != null) {
          onRefreshListJob();
          share(shareUrl);
        } else {
          getChannelSbc(true, channel, channel.channel.xid);
        }
      } else {
        Toast.show({
          text: "Something went wrong!! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const goToChannelDetail = async (
    isNewSubscribe,
    channelParam,
    channelSbc
  ) => {
    navigateToJobDetail(
      isNewSubscribe,
      channelParam ? channelParam : channel,
      channelSbc
    );
  };

  const getChannelSbc = async (isNewSubscribe, channelParam, channelXid) => {
    try {
      response = await getChannelDetail(channelXid);
      let result = response.data;
      if (response.header.message == "Success") {
        goToChannelDetail(
          isNewSubscribe,
          channelParam ? channelParam : channel,
          result.sbc
        );
      } else {
        Toast.show({
          text: "Something went wrong!! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={false}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        rootStyle={{ elevation: 4}}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={onClose()}
        HeaderComponent={
          <View
            style={{
              height: 48,
              width: 48,
              position: "absolute",
              right: 16,
              top: -64,
            }}
          >
            {Platform.OS === "android" ? (
              <TouchableNativeFeedback
                onPress={() => onClose()}
                style={{ borderRadius: 48 }}
              >
                <IconCloseRoundedWhite />
              </TouchableNativeFeedback>
            ) : (
              <TouchableOpacity
                onPress={() => onClose()}
                style={{ borderRadius: 48 }}
              >
                <IconCloseRoundedWhite />
              </TouchableOpacity>
            )}
          </View>
        }
      >
        <BottomSheet
          onPressClose={() => {
            onPressButtonJoinChannel();
            onClose();
          }}
          title={channel ? channel.channel.channel_name : ""}
          desc={channel ? channel.channel.description : ""}
          wordingClose="GABUNG KE FORUM INI"
          type="bottomSheetJobSubscribeChannel"
          imageSource={channel ? channel.channel.logo : ""}
        />
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  return (
    <Container>
      {renderBottomSheet()}
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <Title style={styles.textTitle}>Job</Title>
          </Body>
          <Right>
            <TouchableOpacity
              style={{ marginLeft: 12 }}
              onPress={() => {
                navigation.navigate("JobFilter", {
                  filterValue: filterValue,
                  onSelect: onSelect,
                  onRefresh: onRefreshListJob,
                });
                AdjustTracker(AdjustTrackerConfig.Forum_Job_Filter);
              }}
            >
              {iconFilterRender()}
            </TouchableOpacity>
          </Right>
        </Header>
      </SafeAreaView>
      {_.isEmpty(jobType) ? (
        <View />
      ) : (
        <View style={styles.cardTabOut(isOnTop)}>{renderTabFilter()}</View>
      )}
      <View
        style={[
          styles.contentContainer,
          {
            paddingHorizontal:
              isEmptyData && _.isEmpty(dataList) ? 40 : undefined,
            justifyContent:
              isEmptyData && _.isEmpty(dataList) ? "center" : undefined,
            marginBottom: isEmptyData && _.isEmpty(dataList) ? undefined : 13,
          },
        ]}
      >
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={
            (isEmptyData && _.isEmpty(dataList)
              ? styles.centerEmptyList
              : undefined,
            {
              paddingHorizontal: isEmptyData && _.isEmpty(dataList) ? null : 16,
            })
          }
          data={dataList}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.9}
          renderItem={_renderData}
          ListFooterComponent={_renderItemFooter()}
          ListEmptyComponent={_renderEmptyItemJob}
          onRefresh={() => {
            onRefreshListJob();
          }}
          refreshing={false}
          onScroll={(e) => {
            setIsOnTop(e.nativeEvent.contentOffset.y <= 0);
          }}
        />
      </View>
    </Container>
  );
};

export default Job;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
  },
  contentContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  viewAllContent: { marginHorizontal: 16, flex: 1 },
  viewTabFilter: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 10,
    flexWrap: "wrap",
  },
  containerChannel: {
    paddingVertical: 20,
    backgroundColor: "#FFFFFF",
  },
  listChannelWrapper: {
    flexDirection: "row",
    marginLeft: 16,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  containerList: (item, selectedTabList) => ({
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: item != selectedTabList ? "#EBEBEB" : "#0771CD",
    marginRight: 20,
    borderRadius: 7,
  }),
  textList: (item, selectedTabList) => ({
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: item != selectedTabList ? "#000000" : "#FFFFFF",
  }),
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  cardTabOut: (isOnTop) =>
    isOnTop
      ? { flexDirection: "row" }
      : {
          flexDirection: "row",
          shadowColor: "#0000000A",
          shadowOpacity: 1,
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowRadius: 4,
          elevation: 4,
          zIndex: 1,
        },
  layoutEmptyItem: { textAlign: "center", marginTop: 30 },
  centerEmptyList: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    paddingHorizontal: 16,
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 20 },
});
