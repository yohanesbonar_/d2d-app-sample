import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
} from "native-base";
import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Alert,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import {
  IconBack,
  IconJurnal,
  IconRecordedWebinar,
  IconUpcomingWebinar,
  IconEventSearch,
  IconNotFound,
  IconCmeSearch,
  IconJobSearch,
  IconAtozSearch,
  IconFilterSearch,
  IconFiltered,
  IconLiveNowWebinar,
} from "../../assets";
import _, { result } from "lodash";
import { CardItemSearch } from "../../components/molecules";
import SearchBarField from "../../components/molecules/SearchBarField";
import { getSearch } from "../../utils/network/";
import platform from "../../../theme/variables/platform";
import { HistorySearch, Loader } from "../../../app/components";
import moment from "moment";
import {
  isStarted,
  checkNpaIdiEmpty,
  STORAGE_TABLE_NAME,
  saveHistorySearch,
  removeHistorySearch,
  EnumTypeHistorySearch,
} from "../../../app/libs/Common";
import AsyncStorage from "@react-native-community/async-storage";
import Orientation from "react-native-orientation";
import { sendTopicWebinarAttendee, useDebouncedEffect } from "../../utils";
import { ArrowBackButton } from "../../components/atoms";

const SearchKonten = ({ navigation }) => {
  const { params } = navigation.state;

  const [searchValue, setSearchValue] = useState("");
  const [upcomingWebinarList, setUpcomingWebinarList] = useState([]);
  const [liveWebinarList, setLiveWebinarList] = useState([]);
  const [recordedWebinarList, setRecordedWebinarList] = useState([]);
  const [jurnalList, setJurnalList] = useState([]);
  const [eventList, setEventList] = useState([]);
  const [guidelineList, setGuidelineList] = useState([]);
  const [CmeList, setCmeList] = useState([]);
  const [jobList, setJobList] = useState([]);
  const [atozList, setAtozList] = useState([]);

  const [isEmptyDataEventList, setIsEmptyDataEventList] = useState(true);
  const [
    isEmptyDataUpcomingWebinarList,
    setIsEmptyDataUpcomingWebinarList,
  ] = useState(true);
  const [isEmptyDataLiveWebinarList, setIsEmptyDataLiveWebinarList] = useState(
    true
  );
  const [
    isEmptyDataRecordedWebinarList,
    setIsEmptyDataRecordedWebinarList,
  ] = useState(true);
  const [isEmptyDataJurnalList, setIsEmptyDataJurnalList] = useState(true);
  const [isEmptyDataGuidelineList, setIsEmptyDataGuidelineList] = useState(
    true
  );
  const [isEmptyDataCmeList, setIsEmptyDataCmeList] = useState(true);
  const [isEmptyDataJobList, setIsEmptyDataJobList] = useState(true);
  const [isEmptyDataAtozList, setIsEmptyDataAtozList] = useState(true);

  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isShowFilter, setIsShowFilter] = useState(false);

  const [dataFilter, setDataFilter] = useState("");

  const [dataCategoryParamsFilter, setDataCategoryParamsFilter] = useState({});

  const [serverDate, setServerDate] = useState("");
  const [isFocusSearch, setIsFocusSearch] = useState(false);
  const [historySearchList, setHistorySearchList] = useState([]);

  const [dataForShowSeeAll, setDataForShowSeeAll] = useState([]);

  const [allData, setAllData] = useState([]);

  const [allDataLiveWebinar, setAllDataLiveWebinar] = useState([]);
  const [allDataUpcomingWebinar, setAllDataUpcomingWebinar] = useState([]);
  const [allDataWebinar, setAllDataWebinar] = useState([]);
  const [allDataCme, setAllDataCme] = useState([]);
  const [allDataEvent, setAllDataEvent] = useState([]);
  const [allDataJob, setAllDataJob] = useState([]);
  const [allDataGuideline, setAllDataGuideline] = useState([]);
  const [allDataJournal, setAllDataJournal] = useState([]);
  const [allDataObatAtoz, setAllDataObatAtoz] = useState([]);

  const [isTriggerBookmark, setIsTriggerBookmark] = useState(false);

  useEffect(() => {
    checkHistorySearch();
    Orientation.lockToPortrait();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  useEffect(() => {
    if (isTriggerBookmark) {
      BackHandler.addEventListener("hardwareBackPress", handleBackButton);
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
      };
    }
  }, [isTriggerBookmark]);

  const handleBackButton = () => {
    navigation.goBack();
    if (params.from == "home") {
      if (isTriggerBookmark) {
        if(navigation.state.params.triggerBookmark !=null){
          navigation.state.params.triggerBookmark();
        }else{
          navigation.state.params.onRefreshDataSearchKonten(); 
        }
      }
    }
    return true;
  };

  const inputSearchRef = useRef(null);

  const checkHistorySearch = async () => {
    let dataHistorySearch = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT
    );
    console.log("RESULT dataHistorySearch before parse", dataHistorySearch);
    dataHistorySearch =
      JSON.parse(dataHistorySearch) != null
        ? JSON.parse(dataHistorySearch).historySearch
        : [];
    console.log("RESULT dataHistorySearch after parse", dataHistorySearch);

    setHistorySearchList(dataHistorySearch);
    inputSearchRef.current.focus();
  };

  // useEffect(() => {
  //   const timer = setTimeout(() => {
  //     getData();
  //   }, 1000);

  //   return () => clearTimeout(timer);
  // }, [searchValue]);

  useDebouncedEffect(
    () => {
      console.log(searchValue); // debounced 1sec
      // call search api ...
      getData();
    },
    1000,
    [searchValue]
  );

  const onChange = (inputText) => {
    setSearchValue(inputText);
    setDataFilter("");
  };

  const onFocusTextSearch = () => {
    // setTimeout(() => {
    setIsFocusSearch(true);
    // }, 500);
  };

  const onRefresh = async () => {
    await getData();
  };

  const getData = () => {
    if (searchValue.trim().length >= 3) {
      getDataFromAPI();
      inputSearchRef.current.blur();
    } else if (
      searchValue.trim().length > 0 &&
      searchValue.trim().length <= 2
    ) {
      Toast.show({
        text: "Pencarian harus spesifik min. 3 karakter",
        position: "top",
        duration: 2000,
      });
      setIsShowFilter(false);
      setIsDataNotFound(false);
      setDataFilter("");
      clearResultSearch();
      // setSearchValue("");
    } else if (searchValue == "") {
      loadHistorySearch();
      clearResultSearch();
      setIsDataNotFound(false);
      setIsShowFilter(false);
      setDataFilter("");
    }
  };

  const clearResultSearch = () => {
    setEventList([]);
    setIsEmptyDataEventList(true);
    setUpcomingWebinarList([]);
    setIsEmptyDataUpcomingWebinarList(true);
    setLiveWebinarList([]);
    setIsEmptyDataLiveWebinarList(true);
    setJurnalList([]);
    setIsEmptyDataJurnalList(true);
    setRecordedWebinarList([]);
    setIsEmptyDataRecordedWebinarList(true);
    setGuidelineList([]);
    setIsEmptyDataGuidelineList(true);
    setCmeList([]);
    setIsEmptyDataCmeList(true);
    setJobList([]);
    setIsEmptyDataJobList(true);
    setAtozList([]);
    setIsEmptyDataAtozList(true);
  };

  const getDataFromAPI = async () => {
    try {
      setIsShowLoader(true);
      let response = {};
      if (dataFilter == "" || dataFilter == "Semua") {
        response = await getSearch(
          1,
          20,
          searchValue,
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          1,
          1
        );
      } else if (dataFilter == "Webinar") {
        response = await getSearch(
          1,
          20,
          searchValue,
          1,
          1,
          0,
          0,
          0,
          0,
          0,
          0,
          1
        );
      } else if (dataFilter == "Event") {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          1,
          0,
          0,
          0,
          0,
          0,
          0
        );
      } else if (dataFilter == "Journal") {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          0,
          1,
          0,
          0,
          0,
          0,
          0
        );
      } else if (dataFilter == "Guideline") {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          0,
          0,
          1,
          0,
          0,
          0,
          0
        );
      } else if (dataFilter == "Cme") {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          0,
          0,
          0,
          1,
          0,
          0,
          0
        );
      } else if (dataFilter == "Job") {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          0,
          0,
          0,
          0,
          1,
          0,
          0
        );
      } else if (dataFilter == "ObatAtoz") {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          1,
          0
        );
      } else {
        response = await getSearch(
          1,
          5,
          searchValue,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          1,
          1
        );
      }

      console.log("response SEARCH: ", response);
      if (response.acknowledge) {
        await setData(response.result);
        setObjectToArray(response.result.result);
        await setDataForParamsFilter(response.result);
        setServerDate(response.result.server_date);
        setIsShowLoader(false);
      } else {
        setIsShowLoader(false);
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsShowLoader(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const setData = async (resultSearch) => {
    console.log("result search", resultSearch);
    if (
      resultSearch.result.event == "undefined" &&
      resultSearch.result.upcoming_webinar == "undefined" &&
      resultSearch.result.live_webinar == "undefined" &&
      resultSearch.result.journal == "undefined" &&
      resultSearch.result.webinar == "undefined" &&
      resultSearch.result.guideline == "undefined" &&
      resultSearch.result.cme == "undefined" &&
      resultSearch.result.job == "undefined" &&
      resultSearch.result.obatatoz == "undefined"
    ) {
      setIsDataNotFound(true);
      setIsEmptyDataEventList(true);
      setIsEmptyDataUpcomingWebinarList(true);
      setIsEmptyDataLiveWebinarList(true);
      setIsEmptyDataJurnalList(true);
      setIsEmptyDataRecordedWebinarList(true);
      setIsEmptyDataGuidelineList(true);
      setIsEmptyDataCmeList(true);
      setIsEmptyDataJobList(true);
      setIsEmptyDataAtozList(true);
      setIsShowFilter(false);
    } else if (
      resultSearch.result.event != "undefined" &&
      resultSearch.result.upcoming_webinar != "undefined" &&
      resultSearch.result.live_webinar != "undefined" &&
      resultSearch.result.journal != "undefined" &&
      resultSearch.result.webinar != "undefined" &&
      resultSearch.result.guideline != "undefined" &&
      resultSearch.result.cme != "undefined" &&
      resultSearch.result.job != "undefined" &&
      resultSearch.result.obatatoz != "undefined"
    ) {
      if (
        _.isEmpty(resultSearch.result.event) == true &&
        (_.isEmpty(resultSearch.result.upcoming_webinar) == true ||
          _.isEmpty(
            await doShownDataWebinar(resultSearch.result.upcoming_webinar)
          )) &&
        (_.isEmpty(resultSearch.result.live_webinar) == true ||
          _.isEmpty(
            await doShownDataWebinar(resultSearch.result.live_webinar)
          )) &&
        _.isEmpty(resultSearch.result.journal) == true &&
        (_.isEmpty(resultSearch.result.webinar) == true ||
          _.isEmpty(await doShownDataWebinar(resultSearch.result.webinar))) &&
        _.isEmpty(resultSearch.result.guideline) == true &&
        _.isEmpty(resultSearch.result.cme) == true &&
        _.isEmpty(resultSearch.result.job) == true &&
        _.isEmpty(resultSearch.result.obatatoz) == true
      ) {
        setIsDataNotFound(true);
        setIsEmptyDataEventList(true);
        setIsEmptyDataUpcomingWebinarList(true);
        setIsEmptyDataLiveWebinarList(true);
        setIsEmptyDataJurnalList(true);
        setIsEmptyDataRecordedWebinarList(true);
        setIsEmptyDataGuidelineList(true);
        setIsEmptyDataCmeList(true);
        setIsEmptyDataJobList(true);
        setIsEmptyDataAtozList(true);
        setIsShowFilter(false);
      } else {
        await setDataToState(resultSearch);
      }
    } else {
      await setDataToState(resultSearch);
    }
  };

  const setObjectToArray = (result) => {
    var obj = result;
    var result = Object.entries(obj);
    console.log("NEW ARRAY", result);
    setAllData(result);
  };

  const setDataToState = async (resultSearch) => {
    // setDataForShowSeeAll(resultSearch.result);
    setIsDataNotFound(false);
    setIsShowFilter(true);
    if (_.isEmpty(resultSearch.result.event)) {
      setIsEmptyDataEventList(true);
    } else {
      setAllDataEvent(resultSearch.result.event);
      let firstdataevent = resultSearch.result.event[0];
      setEventList([firstdataevent]);
      setIsEmptyDataEventList(false);
    }

    let dataResultFilterUpcomingWebinar = await doShownDataWebinar(
      resultSearch.result.upcoming_webinar
    );

    if (_.isEmpty(dataResultFilterUpcomingWebinar)) {
      setIsEmptyDataUpcomingWebinarList(true);
    } else {
      setAllDataUpcomingWebinar(dataResultFilterUpcomingWebinar);
      let firstdataupcomingwebinar = dataResultFilterUpcomingWebinar[0];
      setUpcomingWebinarList([firstdataupcomingwebinar]);
      setIsEmptyDataUpcomingWebinarList(false);
    }

    let dataResultFilterLiveWebinar = await doShownDataWebinar(
      resultSearch.result.live_webinar
    );

    if (_.isEmpty(dataResultFilterLiveWebinar)) {
      setIsEmptyDataLiveWebinarList(true);
    } else {
      setAllDataLiveWebinar(dataResultFilterLiveWebinar);
      let firstdatalivewebinar = dataResultFilterLiveWebinar[0];
      setLiveWebinarList([firstdatalivewebinar]);
      setIsEmptyDataLiveWebinarList(false);
    }

    if (_.isEmpty(resultSearch.result.journal)) {
      setIsEmptyDataJurnalList(true);
    } else {
      setAllDataJournal(resultSearch.result.journal);
      // console.log("DATA PERTAMA JOURNAL", resultSearch.result.journal[0]);
      let firstdatajournal = resultSearch.result.journal[0];
      setJurnalList([firstdatajournal]);
      setIsEmptyDataJurnalList(false);
    }

    let dataResultFilterWebinar = await doShownDataWebinar(
      resultSearch.result.webinar
    );

    if (_.isEmpty(dataResultFilterWebinar)) {
      setIsEmptyDataRecordedWebinarList(true);
    } else {
      setAllDataWebinar(dataResultFilterWebinar);
      let firstdatawebinar = dataResultFilterWebinar[0];
      setRecordedWebinarList([firstdatawebinar]);
      setIsEmptyDataRecordedWebinarList(false);
    }

    if (_.isEmpty(resultSearch.result.guideline)) {
      setIsEmptyDataGuidelineList(true);
    } else {
      setAllDataGuideline(resultSearch.result.guideline);
      let firstdataguideline = resultSearch.result.guideline[0];
      setGuidelineList([firstdataguideline]);
      setIsEmptyDataGuidelineList(false);
    }

    if (_.isEmpty(resultSearch.result.cme)) {
      setIsEmptyDataCmeList(true);
    } else {
      setAllDataCme(resultSearch.result.cme);
      let firstdatacme = resultSearch.result.cme[0];
      setCmeList([firstdatacme]);
      setIsEmptyDataCmeList(false);
    }

    if (_.isEmpty(resultSearch.result.job)) {
      setIsEmptyDataJobList(true);
    } else {
      setAllDataJob(resultSearch.result.job);
      let firstdatajob = resultSearch.result.job[0];
      setJobList([firstdatajob]);
      setIsEmptyDataJobList(false);
    }

    if (_.isEmpty(resultSearch.result.obatatoz)) {
      setIsEmptyDataAtozList(true);
    } else {
      setAllDataObatAtoz(resultSearch.result.obatatoz);
      let firstdataobatatoz = resultSearch.result.obatatoz[0];
      setAtozList([firstdataobatatoz]);
      setIsEmptyDataAtozList(false);
    }
  };

  const doShownDataWebinar = async (webinarList) => {
    let result = [];
    console.log("doShownData webinarList: ", webinarList);

    console.log("data profile", dataProfile);
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    if (webinarList != null && webinarList.length > 0) {
      webinarList.forEach((element) => {
        if (element.exclusive == 1) {
          if (
            dataProfile != null &&
            dataProfile.webinar != null &&
            dataProfile.webinar.length > 0
          ) {
            let isShow = false;
            dataProfile.webinar.forEach((slug_hash) => {
              sendTopicWebinarAttendee(slug_hash);
              if (slug_hash == element.slug_hash) {
                isShow = true;
              }
            });

            if (isShow) {
              result.push(element);
            }
          }
        } else {
          result.push(element);
        }
      });
      console.log("webinar list", webinarList);
    }

    // if (result.length == 0 && typeof webinarList != "undefined") {
    //   onRefreshdata();
    // }

    return result;
  };

  const setDataForParamsFilter = async (resultSearch) => {
    if (dataFilter == "" || dataFilter == "Semua") {
      let dataResultFilterUpcomingWebinar = await doShownDataWebinar(
        resultSearch.result.upcoming_webinar
      );
      let dataResultFilterLiveWebinar = await doShownDataWebinar(
        resultSearch.result.live_webinar
      );
      let dataResultFilterWebinar = await doShownDataWebinar(
        resultSearch.result.webinar
      );

      let list = {
        upcomingwebinar:
          _.isEmpty(dataResultFilterUpcomingWebinar) == true ? true : false,
        livewebinar:
          _.isEmpty(dataResultFilterLiveWebinar) == true ? true : false,
        webinar: _.isEmpty(dataResultFilterWebinar) == true ? true : false,
        jurnal: _.isEmpty(resultSearch.result.journal) == true ? true : false,
        event: _.isEmpty(resultSearch.result.event) == true ? true : false,
        guideline:
          _.isEmpty(resultSearch.result.guideline) == true ? true : false,
        cme: _.isEmpty(resultSearch.result.cme) == true ? true : false,
        job: _.isEmpty(resultSearch.result.job) == true ? true : false,
        obatatoz:
          _.isEmpty(resultSearch.result.obatatoz) == true ? true : false,
      };
      setDataCategoryParamsFilter(list);
    }
  };

  const _renderItemUpcomingWebinar = ({ item }) => {
    return (
      <CardItemSearch
        type="UpcomingWebinar"
        data={item}
        onPress={() => {
          goToScreenBaseOnCategory(item, "UpcomingWebinar");
        }}
        serverDate={serverDate}
      />
    );
  };

  const _renderItemLiveWebinar = ({ item }) => {
    return (
      <CardItemSearch
        type="LiveWebinar"
        data={item}
        onPress={() => {
          goToScreenBaseOnCategory(item, "LiveWebinar");
        }}
        serverDate={serverDate}
      />
    );
  };

  const _renderItemRecordedWebinar = ({ item }) => {
    return (
      <CardItemSearch
        type="RecordedWebinar"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "RecordedWebinar")}
        serverDate={serverDate}
      />
    );
  };

  const pressBookmark = () => {
    console.log("onPressBookmark");
    setIsTriggerBookmark(true);
  };

  const _renderItemJurnal = ({ item }) => {
    return (
      <CardItemSearch
        type="Jurnal"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "Jurnal")}
        serverDate={serverDate}
        onPressBookmark={(isBookmark) => {
          item.flag_bookmark = isBookmark;
          pressBookmark();
        }}
      />
    );
  };

  const _renderItemEvent = ({ item }) => {
    return (
      <CardItemSearch
        type="Event"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "Event")}
        serverDate={serverDate}
        onPressBookmark={(isBookmark) => {
          item.flag_bookmark = isBookmark;
          pressBookmark();
        }}
      />
    );
  };

  const _renderItemGuideline = ({ item }) => {
    return (
      <CardItemSearch
        type="Guideline"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "Guideline")}
        serverDate={serverDate}
        onPressBookmark={(isBookmark) => {
          item.flag_bookmark = isBookmark;
          pressBookmark();
        }}
      />
    );
  };

  const _renderItemCme = ({ item }) => {
    return (
      <CardItemSearch
        type="Cme"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "Cme")}
        serverDate={serverDate}
      />
    );
  };

  const _renderItemJob = ({ item }) => {
    return (
      <CardItemSearch
        type="Job"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "Job")}
        serverDate={serverDate}
      />
    );
  };

  const _renderItemAtoz = ({ item }) => {
    return (
      <CardItemSearch
        type="ObatAtoz"
        data={item}
        onPress={() => goToScreenBaseOnCategory(item, "ObatAtoz")}
        serverDate={serverDate}
      />
    );
  };

  const renderDataNotFound = () => {
    return (
      <View style={styles.notFoundContainer}>
        <IconNotFound />
        <Text style={styles.notFoundText}>Pencarian Tidak Ditemukan</Text>
        <Text style={styles.anotherKeywordText}>
          Maaf kami tidak bisa menemukan hasil yang Anda cari. Coba gunakan kata
          yang lain.
        </Text>
      </View>
    );
  };

  const goToScreenSearchAll = async (category) => {
    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(EnumTypeHistorySearch.CONTENT, searchValue);
    }
    let data = {
      category: category,
      keyword: searchValue,
    };
    console.log("data", data);
    navigation.navigate("SearchAllByKonten", {
      data: data,
      onRefreshDataSearchKonten: onRefreshdata,
      from: "SearchKonten",
    });
  };

  const goToFilterSearch = () => {
    console.log("dataCategoryParamsFilter -->", dataCategoryParamsFilter);
    let listCategory = {
      upcomingwebinar: dataCategoryParamsFilter.upcomingwebinar,
      livewebinar: dataCategoryParamsFilter.livewebinar,
      webinar: dataCategoryParamsFilter.webinar,
      jurnal: dataCategoryParamsFilter.jurnal,
      event: dataCategoryParamsFilter.event,
      guideline: dataCategoryParamsFilter.guideline,
      cme: dataCategoryParamsFilter.cme,
      job: dataCategoryParamsFilter.job,
      obatatoz: dataCategoryParamsFilter.obatatoz,
    };
    navigation.navigate("FilterSearchKonten", {
      listCategory: listCategory,
      dataFilter: dataFilter,
      onSelect: onSelect,
    });
  };

  const onSelect = async (data) => {
    setDataFilter(data.checkedValue);
    // await onRefresh();
    console.log("data filter ->>>>", dataFilter);
  };

  useEffect(() => {
    onRefresh();
  }, [dataFilter]);

  const goToScreenBaseOnCategory = async (data, type) => {
    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(EnumTypeHistorySearch.CONTENT, searchValue);
    }
    if (
      type == "UpcomingWebinar" ||
      type == "RecordedWebinar" ||
      type == "LiveWebinar"
    ) {
      goToWebinarDetail(data);
    } else if (type == "Jurnal") {
      goToJournalDetail(data);
    } else if (type == "Event") {
      goToEventDetail(data);
    } else if (type == "Cme") {
      goToCMEDetail(data);
    } else if (type == "Guideline") {
      goToPDFView(data);
    } else if (type == "Job") {
    } else if (type == "ObatAtoz") {
      goToObatAtoz(data);
    }
  };

  const goToJournalDetail = (data) => {
    navigation.navigate("LearningJournalDetail", {
      data: data,
      fromDeeplink: true,
      onRefreshDataSearchKonten: onRefreshdata,
      from: "SearchKonten",
    });
  };

  const goToWebinarDetail = async (data) => {
    // console.log("onTap data webinar item", data);
    let tempData = {
      isPaid: data.exclusive == 1 || data.is_paid == true ? true : false,
      from: "Feeds",
      ...data,
    };

    let dateShowTNC = moment(data.start_date)
      .add(-1, "hours")
      .format("YYYY-MM-DD HH:mm:ss");

    // let isStartShowTNC = isStarted(data.server_date, dateShowTNC)
    let isStartShowTNC = isStarted(serverDate, dateShowTNC);
    console.log("log webinar list tempData", tempData);
    // console.log("log webinar list isStartShowTNC", isStartShowTNC);
    let routeNameDestination =
      data.has_tnc == true && isStartShowTNC == true
        ? "WebinarTNC"
        : "WebinarVideo";
    navigation.navigate({
      routeName: routeNameDestination,
      params: tempData,
      key: `webinar-${data.id}`,
    });
  };

  const goToPDFView = async (data) => {
    const PdfView = { type: "Navigate", routeName: "PdfView", params: data };
    let availableAttachment = data.attachment != null ? true : false;

    availableAttachment === true
      ? navigation.navigate(PdfView)
      : showErrorMessage();
  };

  const showErrorMessage = () => {
    Toast.show({
      text: "Data not found",
      position: "bottom",
      duration: 3000,
      type: "danger",
    });
  };

  const goToEventDetail = async (data) => {
    // data.flag_bookmark = this.state.bookmark;
    // data.flag_reserve = this.state.reserved;
    data.paid = data.is_paid;
    navigation.navigate({
      routeName: "EventDetail",
      params: {
        // updateDataBookmark: this.doUpdateDataBookmark,
        data: data,
        // joined: this.state.reserved,
        // //reserve
        // manualUpdateReserve: this.props.manualUpdateReserve == null ? false : this.props.manualUpdateReserve,
        // updateDataReserve: this.doUpdateDataReserved,
        from: "SearchKonten",
        onRefreshDataSearchKonten: onRefreshdata,
      },
      key: `event-${data.id}`,
    });
  };

  const goToCMEDetail = async (data) => {
    isDone = data.is_done == true || data.is_done == 1 ? true : false;
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataSubmitCmeQuiz = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ
    );
    if (checkNpaIdiEmpty(profile) && !_.isEmpty(dataSubmitCmeQuiz) && !isDone) {
      //if (checkNpaIdiEmpty(profile) && !isDone) {
      showAlertFillNpaIDI(data);
    } else {
      gotoMedicinusDetail(isDone, data);
    }
  };

  const showAlertFillNpaIDI = (data) => {
    Alert.alert(
      "Reminder",
      "Please input your Medical ID first before you attempt your next CME Quiz",
      [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: "OK",
          onPress: () => gotoProfile(data),
        },
      ],
      { cancelable: true }
    );
  };

  gotoProfile = (data) => {
    let from = "SearchKonten";

    let params = {
      from: from,
      ...data,
      backFromCertificate: from,
      resubmitQuiz: true,
      isFromAlertSearchPage: true,
      onRefreshDataSearchKonten: onRefreshdata,
    };

    navigation.navigate({
      routeName: "ChangeProfile",
      params: params,
    });
  };

  const gotoMedicinusDetail = async (isDone, data) => {
    console.log("gotoMedicinusDetail data", data);
    let from = "SearchKonten";
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    // this.sendAdjust(from.DETAIL)
    // AdjustTracker(AdjustTrackerConfig.CME_SKP_Quiz);
    let paramCertificate = {
      title: data.title,
      certificate: data.certificate,
      isRead: data.status != "invalid" ? true : false,
      // isFromHistory: true,
      from: from,
      isManualSkp: data.type == "offline" ? true : false,
      typeCertificate: data.type,
      //isCertificateAvailable: true,
    };

    if (data.type == "offline") {
      gotoCertificate(paramCertificate, data);
    } else {
      if (isDone) {
        let type = data.type != null ? data.type : "Cme";

        if (type == "event" || type == "webinar") {
          gotoCertificate(paramCertificate, data);
        } else {
          if (checkNpaIdiEmpty(profile) == false) {
            gotoCertificate(paramCertificate, data);
          } else if (checkNpaIdiEmpty(profile) == true) {
            let params = {
              from: from,
              ...data,
              backFromCertificate: from,
              onRefreshDataSearchKonten: onRefreshdata,
            };

            console.log("gotoMedicinusDetail let data", params);

            navigation.navigate({
              routeName: "CmeQuiz",
              params: params,
              key: "goto",
            });
          }
        }
      } else {
        console.log("TO THIS OPTION", data);
        let params = {
          from: from,
          ...data,
          backFromCertificate: from,
          onRefreshDataSearchKonten: onRefreshdata,
        };
        navigation.navigate({
          routeName: "CmeDetail",
          params: params,
          key: `webinar-${data.id}`,
        });
      }
    }
  };

  const gotoCertificate = (paramCertificate, data) => {
    navigation.navigate({
      routeName: "CmeCertificate",
      params: paramCertificate,
      key: `webinar-${data.id}`,
    });
  };

  const goToObatAtoz = (data) => {
    navigation.navigate("DetailAtoZ", {
      dataObat: data,
    });
  };

  const loadHistorySearch = async () => {
    let dataHistorySearch = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT
    );
    console.log("RESULT dataHistorySearch before parse", dataHistorySearch);
    dataHistorySearch =
      JSON.parse(dataHistorySearch) != null
        ? JSON.parse(dataHistorySearch).historySearch
        : [];
    console.log("RESULT dataHistorySearch after parse", dataHistorySearch);
    setHistorySearchList(dataHistorySearch);
  };

  const onPressRemoveItemHistory = async (item) => {
    await removeHistorySearch(EnumTypeHistorySearch.CONTENT, item.index);
    await loadHistorySearch();
  };

  const doSearch = (inputText) => {
    setSearchValue(inputText);
    setDataFilter("");
  };

  const onRefreshdata = async () => {
    setUpcomingWebinarList([]);
    setLiveWebinarList([]);
    setRecordedWebinarList([]);
    setEventList([]);
    setJobList([]);
    setCmeList([]);
    setAtozList([]);
    setGuidelineList([]);
    setJurnalList([]);
    await getData();
  };

  const onPressButtonClose = async () => {
    await loadHistorySearch();
    inputSearchRef.current.focus();
  };

  const renderContent = () => {
    // console.log("ALLDATA -> ", allData);
    if (isDataNotFound) {
      return <View>{renderDataNotFound()}</View>;
    } else if (
      isFocusSearch &&
      searchValue == "" &&
      historySearchList.length > 0
    ) {
      return (
        <View style={{ marginTop: 4 }}>
          <HistorySearch
            onPressRemoveItem={(item) => onPressRemoveItemHistory(item)}
            onPressItem={(txt) => doSearch(txt)}
            data={historySearchList}
          />
        </View>
      );
    } else if (!isShowLoader) {
      return (
        <View>
          {!isDataNotFound && <View style={{ marginVertical: 10 }} />}
          {!_.isEmpty(allData) &&
            allData.map((value, i) => {
              if (value[0].toString() == "upcoming_webinar") {
                return (
                  <View>
                    {!isEmptyDataUpcomingWebinarList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconUpcomingWebinar style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Upcoming Webinar</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("UpcomingWebinar")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataUpcomingWebinar) > 1
                              ? "LIHAT SEMUA"
                              : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataUpcomingWebinarList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={upcomingWebinarList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemUpcomingWebinar}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "live_webinar") {
                return (
                  <View>
                    {!isEmptyDataLiveWebinarList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconLiveNowWebinar style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Live Webinar</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("LiveWebinar")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataLiveWebinar) > 1
                              ? "LIHAT SEMUA"
                              : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataLiveWebinarList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={liveWebinarList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemLiveWebinar}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "webinar") {
                return (
                  <View>
                    {!isEmptyDataRecordedWebinarList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconRecordedWebinar style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Rekaman Webinar</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("RecordedWebinar")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataWebinar) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataRecordedWebinarList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={recordedWebinarList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemRecordedWebinar}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "event") {
                return (
                  <View>
                    {!isEmptyDataEventList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconEventSearch style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Event</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("Event")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataEvent) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataEventList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={eventList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemEvent}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "journal") {
                return (
                  <View>
                    {!isEmptyDataJurnalList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconJurnal style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Jurnal</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("Jurnal")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataJournal) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataJurnalList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={jurnalList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemJurnal}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "guideline") {
                return (
                  <View>
                    {!isEmptyDataGuidelineList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconJurnal style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Guideline</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("Guideline")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataGuideline) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataGuidelineList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={guidelineList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemGuideline}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "cme") {
                return (
                  <View>
                    {!isEmptyDataCmeList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconCmeSearch style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>CME</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("Cme")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataCme) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataCmeList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={CmeList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemCme}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "job") {
                return (
                  <View>
                    {!isEmptyDataJobList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconJobSearch style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>JOB</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("Job")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataJob) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataJobList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={jobList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemJob}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              } else if (value[0].toString() == "obatatoz") {
                return (
                  <View>
                    {!isEmptyDataAtozList && (
                      <View style={styles.containerGroup}>
                        <View style={styles.containerTitle}>
                          <IconAtozSearch style={{ marginRight: 8 }} />
                          <Text style={styles.textGroup}>Obat A-Z</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => goToScreenSearchAll("ObatAtoz")}
                        >
                          <Text style={styles.textLihatSemua}>
                            {_.size(allDataObatAtoz) > 1 ? "LIHAT SEMUA" : ""}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {!isEmptyDataAtozList && (
                      <FlatList
                        contentContainerStyle={{
                          paddingHorizontal: 16,
                          paddingTop: 20,
                        }}
                        data={atozList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_renderItemAtoz}
                        refreshing={true}
                        onEndReachedThreshold={0.5}
                      />
                    )}
                  </View>
                );
              }
            })}
        </View>
      );
    }
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer(isShowFilter)}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <SearchBarField
              type="searchKonten"
              textplaceholder={"Cari Konten D2D"}
              onChangeText={(text) => {
                onChange(text);
              }}
              onFocus={() => onFocusTextSearch()}
              onButtonClose={() => onPressButtonClose()}
              inputReff={inputSearchRef}
              inputText={searchValue}
              isCloseButton={searchValue.trim() == "" ? false : true}
              onPressCloseButton={() => {
                setSearchValue("");
              }}
            />
            {isShowFilter && (
              <TouchableOpacity
                style={styles.iconFilter}
                onPress={() => goToFilterSearch()}
              >
                {dataFilter == "" ? <IconFilterSearch /> : <IconFiltered />}
              </TouchableOpacity>
            )}
          </Body>
        </Header>
      </SafeAreaView>
      <Content
        style={styles.contentContainer}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={onRefreshdata} />
        }
      >
        {renderContent()}
      </Content>
    </Container>
  );
};

export default SearchKonten;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  containerGroup: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginVertical: 10,
    //marginBottom: 20,
    marginHorizontal: 16,
    justifyContent: "space-between",
  },
  textGroup: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  notFoundContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: platform.deviceHeight / 4,
  },
  notFoundText: {
    fontFamily: "Roboto-Medium",
    fontSize: 20,
    color: "#000000",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 32,
    // textAlignVertical: "center",
  },
  anotherKeywordText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    // textAlignVertical: "center",
    marginTop: 16,
    marginHorizontal: 46,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  bodyContainer: (isShowFilter) => ({
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
    marginRight: isShowFilter ? 12 : 16,
  }),
  textLihatSemua: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#0771CD",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    lineHeight: 16,
    letterSpacing: 0.34,
  },
  containerTitle: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
  },
  contentContainer: {
    backgroundColor: "#FFFFFF",
  },
  iconBack: { marginRight: 20 },
  iconFilter: { marginLeft: 12 },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 8 },
});
