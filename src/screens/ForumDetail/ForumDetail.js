import { Container, Content, View, Text, Toast, Icon } from "native-base";
import React, { useState, useEffect, useCallback, useRef } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  FlatList,
  TextInput,
  KeyboardAvoidingView,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import _ from "lodash";
import { Loader } from "../../../app/components";
import platform from "../../../theme/variables/platform";
import {
  Button,
  CardItemComment,
  CardItemForum,
  HeaderToolbar,
} from "../../components";
import { Platform } from "react-native";
import { getDetailForum } from "../../utils/network/forum/detailForum";
import moment from "moment";
import { likeForum } from "../../utils/network/forum/likeForum";
import { listCommentInDetailForum } from "../../utils/network/forum/listCommentInDetailForum";
import { likeCommentReply } from "../../utils/network/forum/likeCommentReply";
import { postComment } from "../../utils/network/forum/postComment";
import { postReply } from "../../utils/network/forum/postReply";
import { share } from "../../../app/libs/Common";
import {
  IconCloseReplyTo,
  IconSendBlue,
  IconSendGrey,
  IconCommentDisable,
} from "../../assets";
import { removeEmojis, useDebouncedEffect } from "../../utils";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const ForumDetail = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [serverDate, setServerDate] = useState("");
  const [dataForumDetail, setDataForumDetail] = useState({});
  const [dataChannel, setDataChannel] = useState({});
  const [dataXid, setDataXid] = useState(params.data.xid);
  const [dataSbcChannel, setDataSbcChannel] = useState(params.sbcChannel);
  const [thumbnail, setThumbnail] = useState("");
  const [dataListCommet, setDataListComment] = useState([]);
  const [totalLikesForum, setTotalLikesForum] = useState(0);

  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isEmptyData, setIsEmptyData] = useState(true);
  const [pageComment, setPageComment] = useState(1);

  const [xidComment, setXidComment] = useState("");
  const [xidReplyTo, setXidReplyTo] = useState("");
  const [commentValue, setCommentValue] = useState("");
  const [commentFrom, setCommentFrom] = useState("POST");

  const [maxLength, setMaxLength] = useState(2200);
  const [counterChar, setCounterChar] = useState(2200);
  const [dataReplyComment, setDataReplyComment] = useState({});
  const [isTriggerSend, setIsTriggerSend] = useState(false);

  const [numOfLines, setNumOfLines] = useState(1);

  const [isTextInputFocused, setTextInputFocus] = useState(false);
  const [lastPosition, setLastPosition] = useState(null);
  const [limit, setLimit] = useState(10);
  const flatListRef = useRef();

  const [isCommentEnable, setIsCommentEnable] = useState(false);
  const [isStart, setIsStart] = useState(true);

  useEffect(() => {
    console.log("params", params);
    init();
    AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Start);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    if (params.from != "deeplink" || _.isEmpty(params.from)) {
      navigation.state.params.onRefresh();
    }
    return true;
  };

  const init = async () => {
    await initAllData();
    checkKeyboard();
  };

  const checkKeyboard = () => {
    if (params.from == "CommentForumButton") {
      this.textInputRef.focus();
    }
  };

  const getAllData = async () => {
    let temp = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    setServerDate(temp);
    await getDataForumDetail();
    // console.log("is_comment_enable I -> ", isCommentEnable);
    setIsRefresh(false);
  };

  const initAllData = async () => {
    let temp = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    setServerDate(temp);
    await getDataForumDetail();
  };

  useEffect(() => {
    if (isCommentEnable) {
      // if (isStart == true) {
      //   getDataListComment();
      //   setIsStart(false);
      // }
    } else {
      setDataListComment([]);
    }
  }, [isCommentEnable]);

  const getDataForumDetail = async () => {
    let response = {};
    setIsShowLoader(true);
    try {
      response = await getDetailForum(dataXid, dataSbcChannel);

      console.log("result DETAIL DISKUSI", response);
      if (response.header.message == "Success") {
        setDataForumDetail(response.data);
        setDataChannel(response.data.channel);
        checkImage(response.data.attachments);
        setTotalLikesForum(response.data.total_likes);
        setIsShowLoader(false);
        setXidComment(response.data.xid);
        setCounterChar(2200);
        if (response.data != null && response.data.is_comment_enable) {
          setIsCommentEnable(true);
          getDataListComment();
        } else {
          setIsCommentEnable(false);
          setDataListComment([]);
          setIsFetching(false);
        }
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const getLimit = (index) => {
    console.log("limit --", index);
    var limit = 10;
    if (index != null && index > 9) {
      limit = Math.floor(index / 10) * 10;
      if (index % 9 > 0) {
        limit += 10;
      }
    }
    console.log("limit", limit);
    return limit;
  };

  const getDataListComment = async () => {
    setIsFailed(false);
    setIsFetching(true);

    let response = {};
    let result = [];
    let paramss = {
      paginate: pageComment,
      limit: limit, //diubah limitnya untuk scroll / ditambahin dari frontendnya setelah submit
    };
    console.log("PAGE COMMENT -> ", pageComment);
    try {
      response = await listCommentInDetailForum(
        dataXid,
        dataSbcChannel,
        paramss
      );
      console.log("result COMMENT DETAIL DISKUSI", response);
      result = response.data.data;
      if (response.header.message == "Success") {
        if (pageComment == 1 && _.isEmpty(result)) {
          setIsDataNotFound(true);
        }
        setIsEmptyData(_.isEmpty(result) ? true : false);
        let data =
          pageComment == 1 ? [...result] : [...dataListCommet, ...result];
        setPageComment(pageComment + 1);
        setIsFetching(false);
        setIsFailed(false);
        setDataListComment(data);

        if (lastPosition != null) {
          flatListRef.current.scrollToIndex({
            index: lastPosition,
            viewPosition: 0,
          });
          setLastPosition(null);
        }
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const onRefreshdata = () => {
    setIsStart(true);
    setDataForumDetail({});
    // setDataListComment([]);
    setPageComment(1);
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    if (lastPosition == null) {
      setLimit(10);
    }
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      getAllData();
    }
  }, [isRefresh]);

  const handleLoadMore = () => {
    if (isEmptyData == true || isFetching == true || !isCommentEnable) {
      return;
    }

    getDataListComment();
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailed == true && pageComment > 1 && !isShowLoader) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    if (!isCommentEnable) {
      return null;
    }

    return <Loader visible={isFetching} transparent />;
  };

  const renderData = ({ item, index }) => {
    let dataReplies = {};
    {
      if (!_.isEmpty(item.preview_replies)) {
        item.preview_replies.map((bb, i) => {
          if (i == 0) {
            dataReplies = bb;
          }
        });
      }
    }
    if (isCommentEnable == true && !isShowLoader) {
      return (
        <View>
          <CardItemComment
            data={item}
            onPressCard={() => goToReplyComment(item)}
            onPressAnotherComment={() => goToReplyComment(item)}
            onPressReply={() => {
              onPressPostReplyTo(item, index);
              AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar_Reply);
            }}
            type="ForumDetail"
            name={item.by.name}
            speacialistName={item.by.spesialization}
            flagLikeComment={item.liked}
            currentDdate={item.created_at}
            serverDate={serverDate}
            imageUser={item.by.display_picture}
            commentDescription={item.comment}
            totalLikes={item.total_likes}
            dataReplies={dataReplies}
            totalReplies={item.total_replies}
            xid={item.xid}
            sbcChannel={dataSbcChannel}
          />
        </View>
      );
    } else {
      return <View />;
    }
  };

  const goToReplyComment = (item) => {
    navigation.navigate("ReplyComment", {
      item: item,
      sbcChannel: params.sbcChannel,
      onRefresh: onRefreshdata,
      xidMain: xidComment,
    });
  };

  const renderReplyCommentFor = () => {
    if (commentFrom == "REPLY") {
      return (
        <View style={styles.containerOutReplyTo}>
          <View style={styles.containerText}>
            <Text style={styles.textMembalas}>Membalas </Text>
            <Text numberOfLines={1} style={styles.textNameReplyTo}>
              {dataReplyComment.by.name}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              onPressPostForum(dataForumDetail);
              setLastPosition(null);
            }}
          >
            <IconCloseReplyTo />
          </TouchableOpacity>
        </View>
      );
    }
  };

  const onChangeTextComment = (text) => {
    console.log("text -> ", text);
    console.log("text length -> ", text.length);
    // setCounterChar(maxLength - removeEmojis(text).length);
    // setCommentValue(removeEmojis(text));
    setCounterChar(maxLength - text.length);
    setCommentValue(text);
  };

  const changeLines = (e) => {
    let temp = e.nativeEvent.contentSize.height / 16;
    // console.log("Log ChangeLines e -> ", e);
    // console.log(
    //   "Log ChangeLines e.nativeEvent.contentSize.height -> ",
    //   e.nativeEvent.contentSize.height
    // );
    console.log("Log ChangeLines Temp -> ", temp);
    setNumOfLines(temp);
  };

  const renderFieldComment = () => {
    return (
      <View style={styles.containerOutFieldInput(isTextInputFocused)}>
        {renderReplyCommentFor()}
        <View style={styles.containerInFieldInput(commentValue, maxLength)}>
          <TextInput
            style={styles.containerField}
            placeholder="Berikan komentar Anda…"
            placeholderTextColor="#666666"
            multiline={true}
            ref={(ref) => (this.textInputRef = ref)}
            onChangeText={(text) => {
              onChangeTextComment(text);
            }}
            numberOfLines={numOfLines}
            onContentSizeChange={(e) => changeLines(e)}
            value={commentValue}
            // keyboardType={
            //   Platform.OS === "ios" ? "ascii-capable" : "visible-password"
            // }
            onFocus={() => setTextInputFocus(true)}
            onBlur={() => setTextInputFocus(false)}
          />
          <Text style={styles.textCountDown(commentValue, maxLength)}>
            {counterChar}
          </Text>
          <TouchableOpacity
            style={styles.buttonSendContainer}
            onPress={() => onPressSend()}
          >
            {commentValue != "" &&
            commentValue.length <= maxLength &&
            /\S/.test(commentValue) ? (
              <IconSendBlue />
            ) : (
              <IconSendGrey />
            )}
          </TouchableOpacity>
        </View>
        {renderErrorMaxValue()}
      </View>
    );
  };
  const renderErrorMaxValue = () => {
    if (commentValue.length > maxLength) {
      return (
        <View>
          <Text style={styles.textErrorMaxValue}>
            Anda telah melebihi limit {maxLength} karakter maksimum.
          </Text>
        </View>
      );
    }
  };

  const checkImage = (attachments) => {
    {
      if (!_.isEmpty(attachments)) {
        attachments.map((bb, i) => {
          if (i == 0) {
            setThumbnail(bb.attachment_url);
          }
        });
      }
    }
  };

  const _renderEmptyItemComment = () => {
    if (
      isEmptyData &&
      !isFetching &&
      !_.isEmpty(dataForumDetail) &&
      isCommentEnable == true
    ) {
      return (
        <Text style={styles.textCommentNonActive}>Tidak ada komentar</Text>
      );
    } else if (!isCommentEnable && !_.isEmpty(dataForumDetail)) {
      console.log("non active coment");
      return (
        <View
          style={{
            paddingTop: 80,
            alignContent: "center",
            alignItems: "center",
            paddingBottom: 80,
          }}
        >
          <IconCommentDisable />
          <Text style={[styles.textCommentNonActive, { marginTop: 28 }]}>
            Komentar untuk diskusi ini dinonaktifkan.
          </Text>
        </View>
      );
    }

    return null;
  };

  const onPressPostForum = (item) => {
    console.log("ITEM POST -> ", item);
    this.textInputRef.focus();
    setCommentFrom("POST");
    setXidComment(item.xid);
    setDataReplyComment(item);
  };

  const clearToComment = (item) => {
    console.log("ITEM POST -> ", item);
    setCommentFrom("POST");
    setXidComment(item.xid);
    setDataReplyComment(item);
  };

  const onPressPostReplyTo = (item, index) => {
    console.log("ITEM REPLY -> ", item, index);
    setLimit(getLimit(index));
    this.textInputRef.focus();
    setCommentFrom("REPLY");
    setXidReplyTo(item.xid);
    setDataReplyComment(item);
    setPageComment(1);
    setLastPosition(index);
  };

  useDebouncedEffect(
    () => {
      sendToApi();
    },
    500,
    [isTriggerSend]
  );

  const onPressSend = () => {
    console.log("comment value -> ", commentValue);
    console.log("xidComment ->> ", xidComment);
    console.log("xidReplyTo ->> ", xidReplyTo);
    // if (_.isEmpty(commentValue)) {
    // } else if (commentValue.length > maxLength) {
    // }
    // else
    if (
      !_.isEmpty(commentValue) &&
      commentValue.length <= maxLength &&
      /\S/.test(commentValue)
    ) {
      this.textInputRef.blur();
      setIsShowLoader(true);
      setNumOfLines(1);
      // sendToApi()
      setIsTriggerSend(!isTriggerSend);
    }
  };

  const sendToApi = async () => {
    let response = {};
    try {
      if (commentFrom == "REPLY") {
        response = await postReply(
          xidComment,
          commentValue,
          xidReplyTo,
          dataSbcChannel
        );
      } else {
        setLastPosition(0);
        response = await postComment(xidComment, commentValue, dataSbcChannel);
      }

      console.log("result Send Comment", response);
      if (response.header.message == "Success") {
        setCommentValue("");
        clearToComment(dataForumDetail);
        await onRefreshdata();
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const onPressShareButton = (item) => {
    share(item.share.link != "" ? item.share.link : null);
  };

  const renderHeaderList = () => {
    if (!_.isEmpty(dataForumDetail)) {
      return (
        <View>
          <CardItemForum
            type="ForumDetail"
            title={dataForumDetail.title}
            roles={_.isEmpty(dataForumDetail) ? null : "Admin"}
            description={dataForumDetail.description}
            current_date={dataForumDetail.created_at}
            server_date={serverDate}
            image_user={null}
            thumbnail_image={thumbnail != "" ? thumbnail : null}
            totalComments={dataForumDetail.total_comments}
            totalLikes={totalLikesForum}
            flagLike={dataForumDetail.liked}
            onPressComment={() => {
              onPressPostForum(dataForumDetail);
              AdjustTracker(AdjustTrackerConfig.Forum_Diskusi_Komentar);
            }}
            onPressShare={() => onPressShareButton(dataForumDetail)}
            xid={dataForumDetail.xid}
            sbcChannel={dataSbcChannel}
            isCommentEnable={isCommentEnable}
          />
          <View style={styles.lineContainer} />
        </View>
      );
    }
  };
  return (
    <Container>
      <Loader visible={isShowLoader} />
      <HeaderToolbar onPress={handleBackButton} title="Detail Diskusi" />
      <FlatList
        ref={flatListRef}
        showsVerticalScrollIndicator={false}
        style={{}}
        data={dataListCommet}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderData}
        onEndReachedThreshold={0.5}
        onEndReached={handleLoadMore}
        ListHeaderComponent={renderHeaderList()}
        ListFooterComponent={_renderItemFooter()}
        ListEmptyComponent={_renderEmptyItemComment()}
        refreshing={false}
        onRefresh={() => {
          onRefreshdata();
        }}
        onScrollToIndexFailed={(info) => {
          if (lastPosition != null) {
            console.log("info --", info, lastPosition);
            setTimeout(
              () =>
                flatListRef.current.scrollToIndex({
                  index: info.index,
                  viewPosition: 0,
                }),
              500
            );
          }
        }}
        // initialScrollIndex={lastPosition}
      />
      {Platform.OS === "android" ? (
        !_.isEmpty(dataForumDetail) && isCommentEnable ? (
          renderFieldComment()
        ) : null
      ) : (
        <KeyboardAvoidingView behavior="padding">
          {!_.isEmpty(dataForumDetail) && isCommentEnable
            ? renderFieldComment()
            : null}
        </KeyboardAvoidingView>
      )}
    </Container>
  );
};

export default ForumDetail;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  lineContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 1,
    borderWidth: 0.1,
    borderEndColor: "black",
  },
  viewContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  containerField: {
    paddingTop: 0,
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    lineHeight: 24,
    letterSpacing: 0.15,
    paddingBottom: Platform.OS === "android" ? 0 : null,
    flex: 1,
  },
  containerInFieldInput: (commentValue, maxLength) => ({
    justifyContent: "space-between",
    flexDirection: "row",
    marginTop: 8,
    marginHorizontal: 16,
    marginBottom: 8,
    backgroundColor: "#EBEBEB",
    borderRadius: 4,
    alignItems: "flex-end",
    padding: 16,
    maxHeight: 142,
    borderWidth:
      commentValue != "" && commentValue.length > maxLength ? 1 : null,
    borderColor:
      commentValue != "" && commentValue.length > maxLength ? "#B00020" : null,
  }),
  containerOutFieldInput: (isFocused) => ({
    backgroundColor: "#FFF",
    // marginBottom: 16,
    marginBottom:
      platform.platform == "ios" &&
      platform.deviceHeight >= 812 &&
      platform.deviceWidth >= 375
        ? isFocused
          ? 0
          : 24
        : 0,
    paddingBottom: 16,
    paddingTop: 8,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.02,
    shadowRadius: 4,
    zIndex: 1,
  }),
  buttonSend: { color: "#666666" },
  buttonSendContainer: { paddingLeft: 16, marginBottom: 0 },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  textErrorMaxValue: {
    fontFamily: "Roboto-Regular",
    fontSize: 12,
    color: "#D01E53",
    lineHeight: 16,
    letterSpacing: 0.19,
    marginLeft: 16,
  },
  textCountDown: (commentValue, maxLength) => ({
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color:
      commentValue != "" && commentValue.length > maxLength
        ? "#D01E53"
        : "#666666",
    lineHeight: 24,
    letterSpacing: 0.15,
    marginLeft: 8,
  }),
  containerOutReplyTo: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  containerText: {
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 16,
    flex: 1,
  },
  textMembalas: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    color: "#000000",
    lineHeight: 20,
    letterSpacing: 0.25,
  },
  textNameReplyTo: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    color: "#000000",
    lineHeight: 20,
    letterSpacing: 0.25,
    flex: 1,
  },
  textCommentNonActive: {
    textAlign: "center",
    marginTop: 30,
    marginBottom: 20,
    color: "#000000",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
});
