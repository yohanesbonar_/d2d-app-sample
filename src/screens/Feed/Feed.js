import { Body, Container, Header, Title } from "native-base";
import React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import { ThemeD2D } from "../../../theme";

const Feed = () => {
  return (
    <Container>
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow>
          <Body>
            <Title style={{ fontSize: 30 }}>Feed</Title>
          </Body>
        </Header>
      </SafeAreaView>
    </Container>
  );
};

export default Feed;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
});
