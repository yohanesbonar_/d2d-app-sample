import {
  Body,
  Container,
  Header,
  View,
  Text,
  Right,
  Tabs,
  ScrollableTab,
  TabHeading,
  Tab,
} from "native-base";
import React, { useState, useEffect } from "react";
import {
  Animated,
  StyleSheet,
  BackHandler,
  SafeAreaView,
  Dimensions,
  PixelRatio,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { ArrowBackButton } from "../../components/atoms";
import { CardItemComment } from "../../components";
import _ from "lodash";
import { Loader } from "../../../app/components";
import { getBoothDetail, getBoothQuestion } from "../../utils/network/booth";
import YoutubePlayer from "react-native-youtube-iframe";
import { RefreshControl } from "react-native";
import {
  IconShare,
  IconChatActive,
  IconChatInactive,
  IconBrosurActive,
  IconBrosurInactive,
  IconKontakKamiActive,
  IconKontakKamiInactive,
} from "../../assets";
import platform from "../../../theme/variables/d2dColor";
import { Modalize } from "react-native-modalize";
import { BottomSheet, RunningText } from "../../components/molecules";

const BoothDetail = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isRefresh, setRefresh] = useState(false);
  const [isFetching, setFetching] = useState(false);
  const [data, setData] = useState(null);
  const [questionData, setQuestionData] = useState(null);
  const [isPlaying, setPlaying] = useState(false);
  useEffect(() => {
    getData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    if (isRefresh) {
      getData();
    }
  }, [isRefresh]);

  const getData = async () => {
    console.log("params ->", params);
    setFetching(true);
    setRefresh(false);
    try {
      let paramsData = {
        id: 50, //params.boothId
      };
      let response = await getBoothDetail(paramsData);
      console.log("get booth detail ->> ", response);
      if (response.status === 200) {
        if (response.result != null) {
          setData(response.result);
        }
      } else {
        console.log("error getting booth detail", error);
      }
      setFetching(false);
    } catch (error) {
      setFetching(false);
      console.log("error getting booth detail", error);
    }

    try {
      let paramsDataQuestion = {
        page: 1,
        limit: 10,
        booth_id: 16,
      };
      let responseQuestion = await getBoothQuestion(paramsDataQuestion);
      console.log("get booth question ->> ", responseQuestion);
      if (responseQuestion.status === 200) {
        if (
          responseQuestion.result != null &&
          responseQuestion.result.result != null &&
          responseQuestion.result.result.length
        ) {
          setQuestionData(responseQuestion.result.result);
        }
      } else {
        console.log("error getting booth question", error);
      }
      setFetching(false);
    } catch (error) {
      setFetching(false);
      console.log("error getting booth question", error);
    }
  };

  const renderTabIcon = (name, isActive) => {
    if (name == "Tanya Jawab") {
      return isActive ? (
        <IconChatActive style={styles.iconTabs} />
      ) : (
        <IconChatInactive style={styles.iconTabs} />
      );
    } else if (name == "Brosur") {
      return isActive ? (
        <IconBrosurActive style={styles.iconTabs} />
      ) : (
        <IconBrosurInactive style={styles.iconTabs} />
      );
    } else {
      return isActive ? (
        <IconKontakKamiActive style={styles.iconTabs} />
      ) : (
        <IconKontakKamiInactive style={styles.iconTabs} />
      );
    }
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const renderModalize = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        ref={modalizeRef}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        <BottomSheet
          onPressClose={() => {
            onClose();
          }}
          title={data && data.organization ? data.organization.name : ""}
          desc={data && data.organization ? data.organization.description : ""}
          type="bottomSheetAboutOrganization"
          imageSource={
            data && data.organization && data.organization.cover
              ? data.organization.cover
              : null
          }
        />
      </Modalize>
    );
  };

  const renderQuestionItem = ({item}) => {
    return (
      <View>
        <CardItemComment
          name={item.username}
          commentDescription={item.questions}
          speacialistName={"Spesialis Anak"}
          currentDdate={item.create_date}
          serverDate={item.create_date}
          imageUser={item.photo? item.photo:null}
          totalLikes={0}
          flagLikeComment={false}
          type="ReplyComment"
        />
      </View>
    );
  };

  const widthVideo = Dimensions.get("window").width;
  const heightVideo = PixelRatio.roundToNearestPixel(widthVideo / (16 / 9));

  return (
    <Container>
      {renderModalize()}
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <View style={{ overflow: "hidden" }}>
              <RunningText
                text={
                  data && data.organization
                    ? data.organization.name
                    : "Pameran Virtual"
                }
                textStyle={styles.textTitle}
              />
              <RunningText
                textStyle={styles.textSubtitle}
                text={"Booth " + (data ? data.priority_name : "")}
              />
            </View>
          </Body>
          <Right style={{ flex: 0.5, flexDirection: "row" }}>
            <TouchableOpacity transparent>
              <IconShare style={styles.iconHeader} />
            </TouchableOpacity>
          </Right>
        </Header>
      </SafeAreaView>
      <YoutubePlayer
        height={heightVideo}
        width={widthVideo}
        videoId={data ? data.content_url : ""}
        isPlaying={isPlaying}
        onReady={() => console.log("video ready")}
        onError={(e) => console.log(e)}
        onPlaybackQualityChange={(q) => console.log(q)}
        playbackRate={1}
      />
      <Tabs
        renderTabBar={(props) => (
          <Animated.View
            style={{
              zIndex: 1,
              width: "100%",
            }}
          >
            <ScrollableTab
              {...props}
              style={{ height: 64 }}
              renderTab={(name, page, active, onPress, onLayout) => (
                <TouchableOpacity
                  key={page}
                  onPress={() => {
                    onPress(page);
                  }}
                  onLayout={onLayout}
                  activeOpacity={0.99}
                >
                  <Animated.View style={styles.tabHeading}>
                    <TabHeading
                      style={{
                        width: platform.deviceWidth / 3,
                        paddingVertical: 9,
                      }}
                      active={active}
                    >
                      <View
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        {renderTabIcon(name, active)}
                        <Text
                          style={{
                            marginTop: 6,
                            fontSize: 14,
                            lineHeight: 16,
                            letterSpacing: 0.25,
                            fontFamily: active
                              ? "Nunito-Bold"
                              : "Nunito-SemiBold",
                            color: active
                              ? platform.topTabBarBlueActiveTextColor
                              : platform.topTabBarBlueTextColor,
                          }}
                        >
                          {name}
                        </Text>
                      </View>
                    </TabHeading>
                  </Animated.View>
                </TouchableOpacity>
              )}
              underlineStyle={{
                height: 2,
                backgroundColor: [platform.topTabBarBlueActiveBorderColor],
              }}
            />
          </Animated.View>
        )}
      >
        <Tab heading="Tanya Jawab">
          <ScrollView style={{ paddingHorizontal: 16, paddingVertical: 20 }}>
            <TouchableOpacity
              style={styles.aboutButton}
              onPress={() => onOpen()}
            >
              <Text
                numberOfLines={1}
                uppercase={true}
                style={{
                  paddingVertical: 10,
                  paddingHorizontal: 20,
                  fontSize: 14,
                  lineHeight: 16,
                  letterSpacing: 0.9,
                  fontWeight: "500",
                }}
              >
                {"Tentang " +
                  (data && data.organization ? data.organization.name : "")}
              </Text>
            </TouchableOpacity>
            <View>
              <FlatList
                data={questionData}
                keyExtractor={(item, index) => index.toString()}
                onEndReachedThreshold={0.5}
                renderItem={renderQuestionItem}
                style={{ flex: 1 }}
              />
            </View>
          </ScrollView>
        </Tab>
        <Tab heading="Brosur">
          <Text>Brosur</Text>
        </Tab>
        <Tab heading="Kontak Kami">
          <Text>Kontak Kami</Text>
        </Tab>
      </Tabs>
    </Container>
  );
};

export default BoothDetail;

const styles = StyleSheet.create({
  iconHeader: {
    height: 48,
    width: 48,
  },
  iconTabs: {
    height: 24,
    width: 24,
    alignSelf: "center",
  },
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
    textAlign: "left",
    flex: 1,
  },
  textSubtitle: {
    fontSize: 12,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    lineHeight: 16,
    letterSpacing: 0.19,
    textAlign: "left",
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  tabHeading: {
    flex: 1,
    height: 200,
    backgroundColor: platform.tabBgColor,
    justifyContent: "center",
    alignItems: "center",
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 20 },
  aboutButton: {
    backgroundColor: "#EBEBEB",
    height: 36,
    borderRadius: 4,
    justifyContent: "center",
    flex: 1,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
});
