import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
  Icon,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Dimensions,
  Image,
  Linking,
  Animated,
  RefreshControl,
  Platform,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import {
  IconBack,
  IconCloseRoundedWhite,
  IconFiltered,
  IconFilterSearch,
  IconInstagramJobDetail,
} from "../../assets";
import { ArrowBackButton } from "../../components/atoms";
import _, { filter, initial, result, sortBy } from "lodash";
import { Loader, Tag } from "../../../app/components";
import { getAllBooth } from "../../utils/network/booth";
import CardBooth from "../../components/molecules/CardBooth";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const Booth = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [boothsUngroup, setBoothsUngroup] = useState([]);
  const [booths, setBooths] = useState([]);
  const [isDataEmpty, setDataEmpty] = useState(false);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [isFetching, setIsFetching] = useState(false);
  const [isRefresh, setRefresh] = useState(false);

  useEffect(() => {
    getData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    if (boothsUngroup != null && boothsUngroup.length) {
      let sortedResponse = _.sortBy(boothsUngroup, "priority");
      let mappedResponse = _.map(_.groupBy(sortedResponse, "priority"));
      console.log("sortedResponse", sortedResponse);
      console.log("groupby", _.groupBy(sortedResponse, "priority"));
      console.log("mappedResponse", mappedResponse);
      setBooths(mappedResponse);
    } else {
      setBooths([]);
    }
  }, [boothsUngroup]);

  const onRefreshData = () => {
    setIsFetching(false);
    setPage(1);
    setRefresh(true);
    setDataEmpty(false);
    setBoothsUngroup([]);
  };

  useEffect(() => {
    if (isRefresh) {
      getData();
    }
  }, [isRefresh]);

  const getData = async () => {
    console.log("params ->", params);
    try {
      setIsFetching(true);
      setRefresh(false);
      let paramsData = {
        page: page,
        limit: limit,
        event_id: params.exhibitionId,
      };
      console.log("get booth data", paramsData);
      let response = await getAllBooth(paramsData);
      console.log("get all booth", response);
      if (response.status === 200) {
        if (
          response.result &&
          response.result.result &&
          response.result.result.length
        ) {
          let data = response.result.result;
          setPage(page + 1);
          setBoothsUngroup([...boothsUngroup, ...data]);
          setDataEmpty(false);
        } else {
          if (page == 1) {
            setDataEmpty(true);
          }
        }
      } else {
        console.log("error getting data booth", response);
      }
      setIsFetching(false);
    } catch (error) {
      setIsFetching(false);
      console.log("error getting data booth", error);
    }
  };

  const handleLoadMore = async () => {
    if (isFetching || isDataEmpty) {
      return;
    } else {
      getData();
    }
  };

  const _renderData = ({ item }) => {
    return (
      <CardBooth
        data={item}
        onPressCard={(itemData) => {
          navigation.navigate("BoothDetail", {
            boothId: itemData.id,
            item: itemData,
          });
        }}
      />
    );
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <Title style={styles.textTitle}>Pameran Virtual</Title>
          </Body>
        </Header>
      </SafeAreaView>
      <FlatList
        data={booths}
        keyExtractor={(item, idx) => idx.toString()}
        onEndReached={handleLoadMore}
        renderItem={_renderData}
        onRefresh={onRefreshData}
        refreshing={false}
        style={{ flex: 1 }}
      />
    </Container>
  );
};

export default Booth;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 20 },
});
