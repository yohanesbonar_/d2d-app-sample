import { Body, Container, Content, Text, Toast } from "native-base";
import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  SafeAreaView,
  BackHandler,
  View,
  TouchableOpacity,
} from "react-native";
import { Loader } from "../../../app/components";
import { ThemeD2D } from "../../../theme";
import { Button, HeaderToolbar, PhotoProfileHome } from "../../components";
import {
  getData,
  getDateFormatIndonesia,
  getProfile,
  KEY_ASYNC_STORAGE,
  storeData,
} from "../../utils";
import _ from "lodash";
import moment from "moment-timezone";
import { AdjustTracker, AdjustTrackerConfig } from "../../../app/libs/AdjustTracker";

const Biodata = ({ navigation }) => {
  const { params } = navigation.state;

  const [dataProfile, setDataProfile] = useState(params);
  const [educationProfile, setEducatinProfile] = useState([]);
  const [subSpecialistProfile, setSubSpecialistProfile] = useState([]);
  const [practiceLocation, setPracticeLocation] = useState([]);
  const [isShowLoader, setIsShowLoader] = useState(false);

  useEffect(() => {
    getDataProfile();

    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    navigation.state.params.onRefreshDataProfile();
    return true;
  };

  const setDataEducations = (result) => {
    let educations = [];

    if (!_.isEmpty(result.education_1)) {
      educations.push(result.education_1);
    }
    if (!_.isEmpty(result.education_2)) {
      educations.push(result.education_2);
    }
    if (!_.isEmpty(result.education_3)) {
      educations.push(result.education_3);
    }

    if (educations.length > 0) {
      setEducatinProfile(educations.join(", "));
    } else {
      setEducatinProfile("-");
    }
  };

  setDataSubSpecialist = (result) => {
    let subSpecialists = [];

    result.spesialization.map((specialistValue) => {
      specialistValue.subspecialist.map((subSpecialistValue) => {
        subSpecialists.push(subSpecialistValue.description);
      });
    });

    if (subSpecialists.length > 0) {
      setSubSpecialistProfile(subSpecialists.join(", "));
    } else {
      setSubSpecialistProfile("-");
    }
  };

  const setDataPracticeLocation = (result) => {
    let practiceLocations = [];

    if (!_.isEmpty(result.clinic_location_1)) {
      let clinic_type_1 = ""
      if (!_.isEmpty(result.clinic_type_1)) {
        clinic_type_1 = result.clinic_type_1 + " "
      }
      practiceLocations.push(clinic_type_1 + result.clinic_location_1);
    }
    if (!_.isEmpty(result.clinic_location_2)) {
      let clinic_type_2 = ""

      if (!_.isEmpty(result.clinic_type_2)) {
        clinic_type_2 = result.clinic_type_2 + " "
      }
      practiceLocations.push(clinic_type_2 + result.clinic_location_2);
    }
    if (!_.isEmpty(result.clinic_location_3)) {
      let clinic_type_3 = ""
      if (!_.isEmpty(result.clinic_type_3)) {
        clinic_type_3 = result.clinic_type_3 + " "
      }
      practiceLocations.push(clinic_type_3 + result.clinic_location_3);
    }

    setPracticeLocation(practiceLocations.join(", "));
  };

  const getDataProfile = async () => {
    setIsShowLoader(true);
    try {
      let response = await getProfile();
      console.log("response: ", response);
      if (response.acknowledge) {
        let result = response.result;
        storeData(KEY_ASYNC_STORAGE.PROFILE, result);
        setDataProfile(result);

        setDataEducations(result);
        setDataPracticeLocation(result);
        setDataSubSpecialist(result);

        setIsShowLoader(false);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.message,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const getDataProfileFromLocal = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then(result => {
      setDataProfile(result);
      setDataEducations(result);
      setDataPracticeLocation(result);
      setDataSubSpecialist(result);
    })
  }

  const gotoChangeProfile = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Profil_Medical)
    navigation.navigate("ChangeProfile", {
      from: 'biodata',
      ...dataProfile,
      onRefreshDataProfile: getDataProfileFromLocal,
    });
  };
  const gotoChangeDetailProfile = () => {
    AdjustTracker(AdjustTrackerConfig.Profile_Lihat_Profil_Saya_Edit_Informasi_Akun)
    navigation.navigate("ChangeDetailProfile", {
      ...dataProfile,
      onRefreshDataProfile: getDataProfileFromLocal,
    });
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />

      <HeaderToolbar onPress={handleBackButton} title="Profil Saya" />

      <Content showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.buttonEditAbsoluteContainer}>
            <TouchableOpacity onPress={gotoChangeProfile}>
              <View>
                <Text style={styles.textChangeData}>UBAH</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.photoProfileContainer}>
            <PhotoProfileHome imageURL={dataProfile.photo} />
          </View>
          <Text style={styles.name}>{dataProfile.name}</Text>

          <View style={styles.medicalIdContainer}>
            <Text style={styles.labelMedicalID}>Medical ID</Text>
            <Text style={styles.numberMedicalID}>
              {_.isEmpty(dataProfile.npa_idi)
                ? "Belum Diisi"
                : dataProfile.npa_idi}
            </Text>

            {_.isEmpty(dataProfile.npa_idi) && (
              <TouchableOpacity
                onPress={gotoChangeProfile}
                style={styles.buttonEnterMedicalID}
              >
                <Text textButtonMediumBlue>MASUKKAN MEDICAL ID</Text>
              </TouchableOpacity>
            )}

            {/* <Text style={styles.textVerification}>VERIFIKASI</Text> */}
          </View>

          {/* <Button text="DOWNLOAD RESUME" size="small" marginHor={0} /> */}
        </View>
        <View style={styles.lineDivider} />

        <View style={styles.container}>
          <View style={styles.titleInfoAccountContainer}>
            <Text style={styles.labelInfoAccount}>Informasi Akun</Text>
            <TouchableOpacity
              style={styles.iconEditPhotoProfileWrapper}
              onPress={gotoChangeDetailProfile}
            >
              <Text style={styles.textChangeData}>UBAH</Text>
            </TouchableOpacity>
          </View>

          <View>
            <Text style={styles.labelDataAcoount}>Tanggal Lahir</Text>
            <Text style={styles.dataAccount}>
              {_.isEmpty(dataProfile.born_date)
                ? "-"
                : getDateFormatIndonesia(dataProfile.born_date)}
            </Text>
            <Text style={styles.labelDataAcoount}>Nomor Handphone</Text>
            <Text style={styles.dataAccount}>
              {_.isEmpty(dataProfile.phone) ? "-" : dataProfile.phone}
            </Text>
            <Text style={styles.labelDataAcoount}>Email</Text>
            <Text style={styles.dataAccount}>{dataProfile.email}</Text>

            <Text style={styles.labelDataAcoount}>Kota Domisili</Text>
            <Text style={styles.dataAccount}>{_.isEmpty(dataProfile.home_location) ? "-" : dataProfile.home_location}</Text>

            <Text style={styles.labelDataAcoount}>Status Pegawai</Text>
            <Text style={styles.dataAccount}>
              {dataProfile.pns != true ? "Swasta" : "PNS"}
            </Text>
            <Text style={styles.labelDataAcoount}>Spesialisasi</Text>
            {!_.isEmpty(dataProfile.spesialization) && (
              <Text style={styles.dataAccount}>
                {dataProfile.spesialization.map((value, i) => (
                  <Text key={i}>
                    {value.description}
                    {dataProfile.spesialization.length > 1 &&
                      i != dataProfile.spesialization.length - 1
                      ? ", "
                      : ""}
                  </Text>
                ))}
              </Text>
            )}
            {_.isEmpty(dataProfile.spesialization) && (
              <Text style={styles.dataAccount}>{_.isEmpty(dataProfile.nonspesialization) ? "Dokter Umum" : dataProfile.nonspesialization[0].description}</Text>
            )}
            <Text style={styles.labelDataAcoount}>Sub Spesialisasi</Text>
            <Text style={styles.dataAccount}>{subSpecialistProfile}</Text>
            <Text style={styles.labelDataAcoount}>Peminatan Studi</Text>
            {dataProfile.subscription && (
              <Text style={styles.dataAccount}>
                {dataProfile.subscription.map((value, i) => (
                  <Text key={i}>
                    {value.description}
                    {dataProfile.subscription.length > 1 &&
                      i != dataProfile.subscription.length - 1
                      ? ", "
                      : ""}
                  </Text>
                ))}
              </Text>
            )}
            <Text style={styles.labelDataAcoount}>Pendidikan</Text>
            <Text style={styles.dataAccount}>{educationProfile}</Text>
            <Text style={styles.labelDataAcoount}>Lokasi Praktek</Text>
            <Text style={styles.dataAccount}>{practiceLocation}</Text>
          </View>
        </View>
      </Content>
    </Container>
  );
};

export default Biodata;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: { flexDirection: "row", alignItems: "center" },
  container: {
    backgroundColor: "#FFFFFF",
    paddingBottom: 20,
    paddingHorizontal: 16,
  },
  titleInfoAccountContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  profileContainer: {
    flex: 0,
    justifyContent: "center",
    marginTop: 20,
  },
  lineDivider: {
    height: 8,
    backgroundColor: "#000000",
    opacity: 0.12,
  },
  name: {
    color: "#000000",
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    marginVertical: 20,
    textAlign: "center",
  },
  medicalIdContainer: {
    backgroundColor: "#FFF8E1",
    borderRadius: 8,
    paddingVertical: 12,
    //marginBottom: 20,
    justifyContent: "center",
    alignContent: "center",
  },
  textVerification: {
    color: "#0771CD",
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0.9,
    marginTop: 12,
    textAlign: "center",
  },
  textChangeData: {
    color: "#0771CD",
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0.9,
    marginVertical: 16,
    marginLeft: 8,
    marginRight: 16,
    textAlign: "center",
  },
  labelMedicalID: {
    fontFamily: "Roboto-Bold",
    color: "#000000",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    textAlign: "center",
  },
  numberMedicalID: {
    fontFamily: "Roboto-Regular",
    color: "#000000",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    textAlign: "center",
    marginTop: 8,
  },
  labelInfoAccount: {
    color: "#000000",
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  buttonEditAbsoluteContainer: {
    position: "absolute",
    right: 0,
    top: 0,
  },
  photoProfileContainer: {
    alignSelf: "center",
    marginTop: 20,
  },
  labelDataAcoount: {
    fontFamily: "Roboto-Medium",
    color: "#000000",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    marginBottom: 8,
  },
  dataAccount: {
    fontFamily: "Roboto-Regular",
    color: "#000000",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    marginBottom: 20,
  },
  iconEditPhotoProfileWrapper: {
    marginRight: -16,
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    marginLeft: 32,
  },
  buttonEnterMedicalID: {
    marginTop: 16,
    justifyContent: "center",
    alignSelf: "center",
  },
});
