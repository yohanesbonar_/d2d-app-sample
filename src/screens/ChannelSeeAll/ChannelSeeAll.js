import {
  Body,
  Container,
  Content,
  Header,
  View,
  Text,
  Toast,
  Icon,
} from "native-base";
import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Dimensions,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack, IconFilterSearch } from "../../assets";
import _ from "lodash";
import {
  CardItemSearch,
  CardItemChannel,
  SearchDataNotFound,
  BottomSheet,
} from "../../components/molecules";
import SearchBarField from "../../components/molecules/SearchBarField";
import { getChannel, getSearch } from "../../utils/network";
import platform from "../../../theme/variables/platform";
import { HistorySearch, Loader } from "../../../app/components";
import { ScrollView } from "react-native-gesture-handler";
import { subscribeChannel } from "../../utils/network/channel/subscribeChannel";
import { Modalize } from "react-native-modalize";
import AsyncStorage from "@react-native-community/async-storage";
import {
  STORAGE_TABLE_NAME,
  saveHistorySearch,
  removeHistorySearch,
  EnumTypeHistorySearch,
} from "../../../app/libs/Common";
import { useDebouncedEffect } from "../../utils";
import { ArrowBackButton } from "../../components/atoms";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const ChannelSeeAll = ({ navigation }) => {
  const params = navigation.state.params;
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [dataChannel, setDataChannel] = useState([]);
  const [dataFilterChannel, setDataFilterChannel] = useState([]);
  const [titleBottomSheet, setTitleBottomSheet] = useState("");
  const [descBottomSheet, setDescBottomSheet] = useState("");

  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isEmptyData, setIsEmptyData] = useState(true);
  const [pageChannel, setPageChannel] = useState(1);
  const [pageSearchChannel, setPageSearchChannel] = useState(1);
  const [itemChannel, setItemChannel] = useState({});
  const [historySearchList, setHistorySearchList] = useState([]);
  const [isFocusSearch, setIsFocusSearch] = useState(false);

  useEffect(() => {
    getData();
    loadHistorySearch();
    params.title == "Forum Saya"
      ? AdjustTracker(AdjustTrackerConfig.Forum_Start)
      : null;
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    if (isFocusSearch) {
      inputSearchRef.current.blur();
      setIsFocusSearch(false);
    }
    // else if (!_.isEmpty(searchValue)) {
    //   setSearchValue("");
    // }
    else {
      navigation.goBack();
      navigation.state.params.onRefresh();
      return true;
    }
  };

  const getData = async () => {
    setIsFailed(false);
    setIsFetching(true);
    setIsRefresh(false);
    console.log("page Channel -> ", pageChannel);
    let response = {};
    let paramsGetAllChannel = {
      type: "subscribed",
      paginate: pageChannel,
      limit: 10,
    };

    if (params.title == "Forum Saya") {
      paramsGetAllChannel.type = "subscribed";
    } else if (params.title == "Rekomendasi Forum") {
      paramsGetAllChannel.type = "recomendation";
    }
    console.log("paramsGetAllChannel", paramsGetAllChannel);

    try {
      response = await getChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        let result = response.data.data;
        setDataFromResponse(result, true);
        setIsFailed(false);
        setIsFetching(false);
        setPageChannel(pageChannel + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const setDataFromResponse = (data, temp) => {
    setIsEmptyData(_.isEmpty(data) ? true : false);
    if (temp) {
      setDataChannel(pageChannel == 1 ? [...data] : [...dataChannel, ...data]);
    } else {
      console.log("data Filter", data);
      if (pageSearchChannel == 1 && _.isEmpty(data)) {
        setIsDataNotFound(true);
      }
      setDataFilterChannel(
        pageSearchChannel == 1 ? [...data] : [...dataFilterChannel, ...data]
      );
    }
  };

  const onRefreshdata = () => {
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    setPageChannel(1);
    setPageSearchChannel(1);
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      setDataChannel([]);
      setDataFilterChannel([]);
      if (searchValue == "") {
        getData();
      } else {
        filterSet(searchValue);
      }
    }
  }, [isRefresh]);

  const onChangeTextSearch = (text) => {
    setPageSearchChannel(1);
    setDataFilterChannel([]);
    if (text.length > 0) {
      setSearchValue(text);
      setIsDataNotFound(false);
    } else {
      setIsDataNotFound(false);
      setSearchValue("");
      onRefreshdata();
    }
  };

  const inputSearchRef = useRef(null);

  useDebouncedEffect(
    () => {
      console.log(searchValue); // debounced 1sec
      // call search api ...
      if (searchValue.trim().length >= 3) {
        filterSet(searchValue);
        inputSearchRef.current.blur();
      } else if (
        searchValue.trim().length < 3 &&
        searchValue.trim().length > 0
      ) {
        Toast.show({
          text: "Pencarian harus spesifik min. 3 karakter",
          position: "top",
          duration: 2000,
        });
        setSearchValue("");
        loadHistorySearch();
      }
    },
    1000,
    [searchValue]
  );

  const filterSet = (input) => {
    setIsRefresh(false);
    filterChannel(input);
    setSearchValue(input);
  };

  const filterChannel = async (input) => {
    setIsFocusSearch(false);
    console.log("page Search Channel -> ", pageSearchChannel);
    setIsFailed(false);
    setIsFetching(true);
    try {
      let paramsGetAllChannel = {
        terms: input,
        type: "subscribed",
        paginate: pageSearchChannel,
        limit: 10,
      };
      if (params.title == "Forum Saya") {
        paramsGetAllChannel.type = "subscribed";
      } else if (params.title == "Rekomendasi Forum") {
        paramsGetAllChannel.type = "recomendation";
      }

      response = await getChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        let result = response.data.data;
        setDataFromResponse(result, false);
        setIsFailed(false);
        setIsFetching(false);
        setPageSearchChannel(pageSearchChannel + 1);
        console.log("LOG RESULT FILTER CHANNEL -> ", result);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const renderSearchNotFound = () => {
    return (
      <SearchDataNotFound
        title="Pencarian Tidak Ditemukan"
        subtitle="Maaf kami tidak bisa menemukan hasil yang Anda cari. Coba gunakan kata yang lain."
      />
    );
  };

  const handleLoadMore = async () => {
    if (isEmptyData == true || isFetching == true) {
      return;
    }
    if (searchValue == "") {
      await getData();
    } else {
      filterChannel(searchValue);
    }
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    let page = searchValue == "" ? pageChannel : pageSearchChannel;
    if (isFailed == true && page > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetching} transparent />;
  };

  const renderChannel = (title, data) => {
    if (isDataNotFound && !_.isEmpty(searchValue)) {
      return renderSearchNotFound();
    } else if (isFocusSearch && !_.isEmpty(historySearchList)) {
      return <View>{renderHistorySearch()}</View>;
    } else {
      return (
        <View>
          <View style={styles.containerChannel}>
            <Text style={styles.textRekomendasiChannel}>{title}</Text>
          </View>

          <View style={styles.containerListCardChannel}>
            <FlatList
              columnWrapperStyle={{
                justifyContent: "space-between",
                marginHorizontal: 16,
              }}
              data={data}
              numColumns={2}
              renderItem={({ item }) => {
                return (
                  <View key={item.id} style={styles.itemCardChannelWrapper}>
                    <CardItemChannel
                      name={item.channel_name}
                      isJoin={item.subscribed}
                      onPressCard={() => {
                        onPressCardCondition(item);
                        params.title == "Forum Saya"
                          ? AdjustTracker(AdjustTrackerConfig.Forum_Masuk)
                          : null;
                      }}
                      onPressButton={() => {
                        onPressButtonInCard(item);
                        params.title == "Forum Saya"
                          ? AdjustTracker(AdjustTrackerConfig.Forum_Saya_Open)
                          : null;
                      }}
                      imageSource={item.logo}
                    />
                  </View>
                );
              }}
              onEndReachedThreshold={0.5}
              onEndReached={handleLoadMore}
              ListFooterComponent={_renderItemFooter()}
            />
          </View>
        </View>
      );
    }
  };

  const onPressCardCondition = async (item) => {
    if (item.subscribed == true) {
      onPressCardChannel(item);
    } else {
      setTitleBottomSheet(item.channel_name);
      setDescBottomSheet(item.description);
      setItemChannel(item);
      onOpen();
    }

    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(
        EnumTypeHistorySearch.MY_REC_CHANNEL,
        searchValue
      );
      console.log(STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL);
    }
  };

  const onPressCardChannel = (item) => {
    console.log("on Press Card");
    let temp = null;
    if (searchValue != "") {
      navigation.navigate("ChannelDetail", {
        item: item,
        onRefresh: onRefreshdata,
        from: "SearchChannel",
        onPressBanner: navigation.state.params.onPressBanner,
      });
    } else {
      navigation.navigate("ChannelDetail", {
        item: item,
        onRefresh: onRefreshdata,
        from: "Channel",
        onPressBanner: navigation.state.params.onPressBanner,
      });
    }
  };

  const onPressButtonInCard = async (item) => {
    if (item.subscribed == false) {
      // onPressButtonJoinChannel(item);
      setTitleBottomSheet(item.channel_name);
      setDescBottomSheet(item.description);
      setItemChannel(item);
      onOpen();
    } else {
      onPressCardChannel();
    }

    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(
        EnumTypeHistorySearch.MY_REC_CHANNEL,
        searchValue
      );
      console.log(STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL);
    }
  };

  const onPressButtonJoinChannel = async (item) => {
    console.log("on Button Join Channel");
    await postSubcribe(item);
    const timer = setTimeout(async () => {
      onRefreshdata();
    }, 1000);
  };

  const postSubcribe = async (item) => {
    let response = {};

    try {
      let paramsGetAllChannel = {
        xid: item.xid,
      };
      response = await subscribeChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        Toast.show({
          text: "Berhasil bergabung ke forum " + item.channel_name,
          buttonText: "TUTUP",
          position: "bottom",
          duration: 2000,
          buttonStyle: { marginLeft: 16 },
        });
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 2000,
        });
      }
      console.log("response SUBSCRIBE XID->", item.xid, " -------- ", response);
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 2000,
      });
    }
  };

  const renderHistorySearch = () => {
    return (
      <View style={{ marginTop: 4 }}>
        <HistorySearch
          onPressRemoveItem={(item) => onPressRemoveItemHistory(item)}
          onPressItem={(txt) => {
            onChangeTextSearch(txt);
            AdjustTracker(AdjustTrackerConfig.Forum_Cari_Terakhir);
          }}
          data={historySearchList}
        />
      </View>
    );
  };

  const onPressRemoveItemHistory = async (item) => {
    await removeHistorySearch(EnumTypeHistorySearch.MY_REC_CHANNEL, item.index);
    await loadHistorySearch();
  };

  const loadHistorySearch = async () => {
    let dataHistorySearch = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL
    );
    console.log("RESULT dataHistorySearch before parse", dataHistorySearch);
    dataHistorySearch =
      JSON.parse(dataHistorySearch) != null
        ? JSON.parse(dataHistorySearch).historySearch
        : [];
    console.log("RESULT dataHistorySearch after parse", dataHistorySearch);
    setHistorySearchList(dataHistorySearch);
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        <BottomSheet
          onPressClose={() => {
            onPressButtonJoinChannel(itemChannel);
            onClose();
          }}
          title={titleBottomSheet}
          desc={descBottomSheet}
          wordingClose="GABUNG KE FORUM INI"
          type="bottomSheetAboutChannel"
          imageSource={itemChannel.logo}
        />
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const onPressCloseButton = () => {
    inputSearchRef.current.focus();
    setSearchValue("");
  };

  return (
    <Container>
      {renderBottomSheet()}
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <SearchBarField
              type="searchChannel"
              textplaceholder={"Cari Forum"}
              onFocus={() => {
                setIsFocusSearch(true);
                loadHistorySearch();
                AdjustTracker(AdjustTrackerConfig.Forum_Cari);
              }}
              onBlur={() => {
                _.isEmpty(historySearchList) ? setIsFocusSearch(false) : null;
              }}
              onChangeText={(text) => {
                onChangeTextSearch(text);
                AdjustTracker(AdjustTrackerConfig.Forum_Cari_Start);
              }}
              onButtonClose={() => onPressButtonClose()}
              inputReff={inputSearchRef}
              inputText={searchValue}
              isCloseButton={searchValue.trim() == "" ? false : true}
              onPressCloseButton={() => onPressCloseButton()}
            />
          </Body>
        </Header>
      </SafeAreaView>
      <Content
        style={{
          backgroundColor: "#FFFFFF",
        }}
        contentContainerStyle={{
          justifyContent:
            isDataNotFound && !_.isEmpty(searchValue) ? "center" : undefined,
          flex: isDataNotFound && !_.isEmpty(searchValue) ? 1 : undefined,
        }}
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={onRefreshdata} />
        }
      >
        <View style={styles.viewContainer}>
          {renderChannel(
            params.title,
            searchValue != "" ? dataFilterChannel : dataChannel
          )}
        </View>
      </Content>
    </Container>
  );
};

export default ChannelSeeAll;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },

  containerChannel: {
    marginVertical: 16,
  },

  textRekomendasiChannel: {
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    color: "#000000",
    alignItems: "flex-start",
    marginHorizontal: 16,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textLihatSemua: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#0771CD",
    alignItems: "flex-end",
    alignSelf: "flex-end",
  },
  containerListCardChannel: {},

  itemCardChannelWrapper: {
    marginBottom: 12,
    paddingTop: 4,
    width: Dimensions.get("window").width / 2 - 24,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
    marginRight: 16,
  },
  viewContainer: {
    flex: 1,
    justifyContent: "center",
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 8 },
});
