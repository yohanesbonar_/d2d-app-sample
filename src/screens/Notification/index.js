import _ from "lodash";
import { Container, Icon, Toast } from "native-base";
import React, { useEffect, useState } from "react";
import {
  Alert,
  BackHandler,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Loader } from "../../../app/components";
import { AdjustTracker, AdjustTrackerConfig } from "../../../app/libs/AdjustTracker";
import {
  doDetailCertificate,
  getListNotification,
} from "../../../app/libs/NetworkUtility";
import { ThemeD2D } from "../../../theme";
import { HeaderToolbar, SearchDataNotFound } from "../../components";
import { getData, KEY_ASYNC_STORAGE } from "../../utils";
import { checkNpaIdiEmpty } from "../../utils/commons";
const Notification = ({ navigation }) => {
  const [dataListNotification, setDataListNotification] = useState([]);
  const [isFetchingNotif, setIsFetchNotif] = useState(true);
  const [isFailedNotif, setIsFailedNotif] = useState(false);
  const [pageNotif, setPageNotif] = useState(1);
  const [isEmptyDataNotif, setIsEmptyDataNotif] = useState(false);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isDataNotFound, setIsDataNotFound] = useState(false);

  useEffect(() => {
    const didFocusListener = navigation.addListener("didFocus", () => {
      getListNotif(pageNotif);
    });
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      didFocusListener;
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    if (isRefresh) {
      setDataListNotification([]);
      setPageNotif(1);
      getListNotif(1);
    }
  }, [isRefresh]);

  const getListNotif = (pageNotif, limit = 10, type = "") => {
    setIsFailedNotif(false);
    setIsFetchNotif(true);
    setIsRefresh(false);

    try {
      getListNotification(pageNotif, limit, type).then((response) => {
        if (response.isSuccess) {
          setDataListNotification([
            ...dataListNotification,
            ...response.data.data.docs,
          ]);

          if (pageNotif == 1 && _.isEmpty(response.data.data.docs)) {
            setIsDataNotFound(true);
            console.log("notfound");
          }
          setIsEmptyDataNotif(
            response.data.data.docs != null &&
              response.data.data.docs.length > 0
              ? false
              : true
          );
          setIsFetchNotif(false);
          setIsFailedNotif(false);
          setPageNotif(pageNotif + 1);
        } else {
          Toast.show({
            text: response.message,
            position: "top",
            duration: 3000,
          });
          setIsFetchNotif(false);
          setIsFailedNotif(true);
        }
      });
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsFetchNotif(false);
      setIsFailedNotif(true);
    }
  };

  const handleLoadMore = () => {
    console.log("page:", pageNotif);

    if (isEmptyDataNotif == true || isFetchingNotif == true) {
      return;
    }

    getListNotif(pageNotif);
  };

  const _renderItem = ({ item }) => {
    isViewed = item.status == "unread" ? true : false;

    centerDotColor = isViewed ? "#DD2C2C" : "#78849E";
    backgroundColorItem = isViewed ? "#BBDEFB" : "#FFFFFF";

    let data = item;
    data.isFromListNotif = true;

    return (
      <TouchableOpacity
        accessibilityLabel="item_notification"
        activeOpacity={0.7}
        onPress={() => onPressitemNotification(data)}
      >
        <View
          style={[
            styles.itemViewNotif,
            { backgroundColor: backgroundColorItem },
          ]}
        >
          <View style={styles.itemViewDots}>
            <Icon
              name={"dots-three-vertical"}
              type="Entypo"
              style={[styles.iconDot, { color: centerDotColor }]}
            />
          </View>
          <View style={styles.itemViewText}>
            {/* <Text style={styles.textItemTime}>{item.title}</Text> */}
            <Text style={styles.textItemDesc}>{item.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetchingNotif == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    if (isFailedNotif == true && pageNotif > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetchingNotif} transparent />;
  };

  const _renderEmptyItemNotification = () => {
    if (isEmptyDataNotif) {
      return (
        <SearchDataNotFound
          type="notification"
          title="Belum Ada Notifikasi"
          subtitle="Tempat pemberitahuan Anda akan ditampilkan disini."
        />
      );
    }

    return null;
  };

  const onRefreshListNotif = () => {
    setDataListNotification([]);
    setIsFetchNotif(true);
    setIsEmptyDataNotif(false);
    setIsRefresh(true);
    setIsDataNotFound(false);
    console.log("onRefreshListNotif");
  };

  const onPressitemNotification = async (dataNotif) => {
    AdjustTracker(AdjustTrackerConfig.Notification_Select);
    console.log("onPressitemNotification: ", dataNotif);

    if (dataNotif.type.includes("certificate_")) {
      if (dataNotif.type.includes("cme")) {
        let profile = await getData(KEY_ASYNC_STORAGE.PROFILE);
        if (checkNpaIdiEmpty(profile)) {
          showAlertCompleteProfile(dataNotif);
        } else {
          doHitDetailCertificate(dataNotif);
        }
      } else {
        doHitDetailCertificate(dataNotif);
      }
    } else if (dataNotif.type == "event") {
      let data = JSON.parse(dataNotif.data);
      data.id = data.content_id;
      data.isFromListNotifProfile = true;

      navigation.navigate("EventDetail", {
        data: data,
        dataNotif: dataNotif,
      });
    }
  };

  const showAlertCompleteProfile = (dataNotif) => {
    let params = {
      from: "notification",
      dataNotif,
    };
    Alert.alert(
      "Warning",
      "Please input Medical ID first",
      [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: "OK",
          onPress: () => navigation.navigate("ChangeProfile", params),
        },
      ],
      { cancelable: true }
    );
  };
  const doHitDetailCertificate = async (dataNotif) => {
    let data = JSON.parse(dataNotif.data);
    setIsShowLoader(true);

    try {
      let response = await doDetailCertificate(data);
      console.log("doHitDetailCertificate response: ", response);
      setIsShowLoader(false);
      if (response.isSuccess && response.data != null) {
        gotoCertificate(response.data, dataNotif);
      }
    } catch (error) {
      setIsShowLoader(false);
      console.log(error);
    }
  };

  const gotoCertificate = (data, dataNotif) => {
    let params = {
      title: data.title,
      certificate: data.certificate,
      typeCertificate: data.type,
      status: data.status,
      dataNotif,
      isFromListNotifProfile: true,
    };
    if ((data.type = "webinar")) {
      params.isCertificateAvailable = true;
    }

    navigation.navigate({
      routeName: "CmeCertificate",
      params: params,
    });
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />

      <HeaderToolbar onPress={handleBackButton} title="Notifikasi" />

      <View style={styles.contentContainer}>
        <FlatList
          contentContainerStyle={
            isEmptyDataNotif && !isFetchingNotif && isDataNotFound
              ? styles.centerEmptyList
              : undefined
          }
          showsVerticalScrollIndicator={false}
          data={dataListNotification}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          renderItem={_renderItem}
          // onRefresh={() => onRefreshListNotif}
          // refreshing={true}
          ListEmptyComponent={_renderEmptyItemNotification}
          ListFooterComponent={_renderItemFooter()}
          keyExtractor={(item, index) => index.toString()}
          refreshing={false}
          onRefresh={() => {
            onRefreshListNotif();
          }}
        />
      </View>
    </Container>
  );
};

export default Notification;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: { flexDirection: "row", alignItems: "center" },
  itemViewNotif: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#ffff",
    borderRadius: 4,
  },
  itemViewDots: {
    flex: 0.15,
    justifyContent: "center",
    alignItems: "center",
  },
  iconDot: {
    fontSize: 30,
    textAlign: "center",
  },
  itemViewText: {
    flex: 0.85,
    paddingVertical: 15,
    marginRight: 5,
    justifyContent: "center",
    alignItems: "flex-start",
    borderBottomWidth: 1,
    borderBottomColor: "#F4F4F6",
  },
  textItemDesc: {
    fontFamily: "Nunito-SemiBold",
    color: "#454F63",
    fontSize: 16,
  },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    marginLeft: 32,
  },
  centerEmptyList: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
  },
  contentContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
});
