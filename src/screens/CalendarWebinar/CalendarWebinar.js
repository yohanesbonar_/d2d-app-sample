import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
  Radio,
  ListItem,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack } from "../../assets";
import _, { initial } from "lodash";
import { CardItemSearch } from "../../components/molecules";
import platform from "../../../theme/variables/platform";
import { Loader } from "../../../app/components";
import OptionRadioButton from "../../components/molecules/OptionRadioButton";
import { Button } from "../../components/atoms";
import {
  Calendar,
  CalendarList,
  Agenda,
  LocaleConfig,
} from "react-native-calendars";

LocaleConfig.locales["id"] = {
  monthNames: [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ],
  monthNamesShort: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun",
    "Jul",
    "Agst",
    "Sept",
    "Okt.",
    "Nov",
    "Des",
  ],
  dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
  dayNamesShort: ["Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
  today: "Hari ini",
};
LocaleConfig.defaultLocale = "id";

const CalendarWebinar = ({ navigation }) => {
  const [isShowLoader, setIsShowLoader] = useState(false);
  let value = "";
  useEffect(() => {
    // console.log("allbykonten params", params);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow>
          <Body style={styles.bodyContainer}>
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <TouchableOpacity
                onPress={handleBackButton}
                style={{ marginRight: 32 }}
              >
                <IconBack />
              </TouchableOpacity>

              <Title style={styles.textTitle}>Kalender Webinar</Title>
            </View>
          </Body>
        </Header>
      </SafeAreaView>
      <Content
        style={{
          flex: 1,
          backgroundColor: "#FFFFFF",
        }}
      >
        <View
          style={{
            marginHorizontal: 16,
          }}
        >
          <Calendar
            onMonthChange={(month) => {}}
            onDayPress={(day) => {}}
            style={styles.calendar}
            markingType={"multi-dot"}
            hideExtraDays
            theme={{ textDisabledColor: "red" }}
            // markedDates={this.isMarkedDates()}
          />
          <Text style={styles.textKategori}>test</Text>
        </View>
      </Content>
    </Container>
  );
};

export default CalendarWebinar;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 10,
    marginVertical: 8,
    marginRight: 16,
    justifyContent: "space-between",
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
  },
  textReset: {
    fontSize: 14,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    alignItems: "center",
  },
  textKategori: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 16,
    marginTop: 20,
  },
  viewContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  calendar: {
    flex: 1,
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: "#eee",
  },
});
