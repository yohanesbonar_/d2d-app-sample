import {
  Body,
  Container,
  Content,
  Header,
  View,
  Text,
  Toast,
  Icon,
} from "native-base";
import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
  Dimensions,
  ScrollView,
  RefreshControl,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack } from "../../assets";
import _, { filter, result } from "lodash";
import {
  CardItemSearch,
  CardItemChannel,
  SearchDataNotFound,
  BottomSheet,
  CardItemBannerProduct,
} from "../../components/molecules";
import SearchBarField from "../../components/molecules/SearchBarField";
import { getChannel, getSearch } from "../../utils/network";
import platform from "../../../theme/variables/platform";
import { HistorySearch, Loader } from "../../../app/components";
import { subscribeChannel } from "../../utils/network/channel/subscribeChannel";
import { Modalize } from "react-native-modalize";
import AsyncStorage from "@react-native-community/async-storage";
import {
  STORAGE_TABLE_NAME,
  saveHistorySearch,
  removeHistorySearch,
  EnumTypeHistorySearch,
} from "../../../app/libs/Common";
import { useDebouncedEffect } from "../../utils";
import { ArrowBackButton } from "../../components/atoms";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const Channel = ({ navigation }) => {
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isFocusSearch, setIsFocusSearch] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [isDataNotFound, setIsDataNotFound] = useState(false);
  const [dataAllChannel, setDataAllChannel] = useState([]);
  const [dataMyChannel, setDataMyChannel] = useState([]);
  const [titleBottomSheet, setTitleBottomSheet] = useState("");
  const [descBottomSheet, setDescBottomSheet] = useState("");
  const [dataFilterChannel, setDataFilterChannel] = useState([]);
  const [isFetching, setIsFetching] = useState(false);
  const [isFailed, setIsFailed] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [isEmptyData, setIsEmptyData] = useState(true);
  const [pageChannel, setPageChannel] = useState(1);
  const [pageSearchChannel, setPageSearchChannel] = useState(1);
  const [itemChannel, setItemChannel] = useState({});
  const [historySearchList, setHistorySearchList] = useState([]);
  const [isTriggerSearch, setIsTriggerSearch] = useState(false);

  useEffect(() => {
    AdjustTracker(AdjustTrackerConfig.Forum_Start);
    loadHistorySearch();
    getData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    if (isFocusSearch) {
      setIsFocusSearch(false);
      inputSearchRef.current.blur();
      onRefreshdata();
    } else {
      navigation.goBack();
      // navigation.state.params.onRefresh();
      return true;
    }
  };

  const inputSearchRef = useRef(null);

  const getData = async () => {
    setIsRefresh(false);
    setIsShowLoader(true);
    await getDataMyChannelOnly();
    await getDataAllChannel();
  };

  const getDataMyChannelOnly = async () => {
    try {
      let params = {
        type: "subscribed",
        paginate: 1,
        limit: 10,
      };
      response = await getChannel(params);
      if (response.header.message == "Success") {
        let result = response.data.data;
        setDataMyChannel(result);
        console.log("LOG RESULT mychannel -> ", result);
        setIsShowLoader(false);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const getDataAllChannel = async () => {
    setIsFailed(false);
    setIsFetching(true);

    console.log("pageChannel -> ", pageChannel);
    try {
      let params = {
        type: "fresh",
        paginate: pageChannel,
        limit: 10,
      };
      response = await getChannel(params);
      if (response.header.message == "Success") {
        let result = response.data.data;
        setDataFromResponse(result, true);
        console.log("LOG RESULT ALL CHANNEL -> ", result);
        setIsFailed(false);
        setIsFetching(false);
        setPageChannel(pageChannel + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const setDataFromResponse = (data, temp) => {
    setIsEmptyData(_.isEmpty(data) ? true : false);
    if (temp) {
      setDataAllChannel(
        pageChannel == 1 ? [...data] : [...dataAllChannel, ...data]
      );
    } else {
      console.log("data Filter", data);

      if (pageSearchChannel == 1 && _.isEmpty(data)) {
        setIsDataNotFound(true);
      } else {
        setIsDataNotFound(false);
      }
      setDataFilterChannel(
        pageSearchChannel == 1 ? [...data] : [...dataFilterChannel, ...data]
      );
    }
  };

  const onRefreshdata = () => {
    setPageChannel(1);
    setPageSearchChannel(1);
    setIsFetching(true);
    setIsEmptyData(false);
    setIsDataNotFound(false);
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      setPageChannel(1);
      setPageSearchChannel(1);
      setDataAllChannel([]);
      setDataMyChannel([]);
      setDataFilterChannel([]);
      if (searchValue == "") {
        getData();
      } else {
        filterSet(searchValue);
      }
    }
  }, [isRefresh]);

  const handleLoadMore = async () => {
    if (isEmptyData == true || isFetching == true) {
      return;
    }
    if (searchValue == "") {
      await getDataAllChannel();
    } else {
      filterChannel(searchValue);
    }
  };

  const _renderItemFooter = () => (
    <View
      style={[
        styles.containerItemFooter,
        { height: isFetching == true ? 80 : 0 },
      ]}
    >
      {_renderItemFooterLoader()}
    </View>
  );

  const _renderItemFooterLoader = () => {
    let page = searchValue == "" ? pageChannel : pageSearchChannel;
    if (isFailed == true && page > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={isFetching} transparent />;
  };

  const renderChannel = (title, data) => {
    let dataAfterSlice = data.slice(0, 5);
    return (
      <View>
        <View style={[styles.containerChannel, { marginHorizontal: 16 }]}>
          <Text style={styles.textRekomendasiChannel}>{title}</Text>

          {data.length > 5 ? (
            <TouchableOpacity onPress={() => onPressSeeAll(title, data)}>
              <Text style={styles.textLihatSemua}>LIHAT SEMUA</Text>
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </View>

        <View style={styles.containerListCardChannel}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{ width: 8 }} />

            <View style={styles.listChannelWrapper}>
              {dataAfterSlice.map((item) => {
                return (
                  <View key={item.id} style={styles.itemCardMyChannelWrapper}>
                    <CardItemChannel
                      name={item.channel_name}
                      isJoin={item.subscribed}
                      onPressCard={() => {
                        onPressCardCondition(item);
                        AdjustTracker(AdjustTrackerConfig.Forum_Masuk);
                      }}
                      onPressButton={() => {
                        onPressButtonInCard(item);
                        AdjustTracker(AdjustTrackerConfig.Forum_Saya_Open);
                      }}
                      imageSource={item.logo}
                    />
                  </View>
                );
              })}
            </View>
            <View style={{ width: 32 }} />
          </ScrollView>
        </View>
      </View>
    );
  };

  const onPressCardCondition = async (item) => {
    if (item.subscribed == true) {
      onPressCardChannel(item);
    } else {
      setTitleBottomSheet(item.channel_name);
      setDescBottomSheet(item.description);
      setItemChannel(item);
      onOpen();
    }

    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(EnumTypeHistorySearch.ALL_CHANNEL, searchValue);
      loadHistorySearch();
      console.log(STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL);
    }
  };

  const onPressButtonInCard = async (item) => {
    if (item.subscribed == false) {
      // onPressButtonJoinChannel(item);
      setTitleBottomSheet(item.channel_name);
      setDescBottomSheet(item.description);
      setItemChannel(item);
      onOpen();
    } else {
      onPressCardChannel();
    }

    if (searchValue != "" && searchValue.trim().length >= 3) {
      await saveHistorySearch(EnumTypeHistorySearch.ALL_CHANNEL, searchValue);
      loadHistorySearch();
    }
  };

  const onPressSeeAll = (title, data) => {
    navigation.navigate("ChannelSeeAll", {
      title: title,
      data: data,
      onRefresh: onRefreshdata,
      from: "Channel",
      onPressBanner: navigation.state.params.onPressBanner,
    });
  };

  const onPressCardChannel = (item) => {
    navigation.navigate("ChannelDetail", {
      item: item,
      onRefresh: onRefreshdata,
      from: "Channel",
      onPressBanner: navigation.state.params.onPressBanner,
    });
  };

  const onPressButtonJoinChannel = async (item) => {
    await postSubcribe(item);
    setTimeout(async () => {
      await onRefreshdata();
    }, 1000);
  };

  const postSubcribe = async (item) => {
    let response = {};

    try {
      let paramsGetAllChannel = {
        xid: item.xid,
      };
      response = await subscribeChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        Toast.show({
          text: "Berhasil bergabung ke forum " + item.channel_name,
          buttonText: "TUTUP",
          position: "bottom",
          duration: 2000,
          buttonStyle: { marginLeft: 16 },
        });
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 2000,
        });
      }
      console.log("response SUBSCRIBE XID->", item.xid, " -------- ", response);
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 2000,
      });
    }
  };

  const renderHeaderList = () => {
    if (!isShowLoader) {
      return (
        <View>
          {_.isEmpty(searchValue) &&
            !isShowLoader &&
            !_.isEmpty(dataMyChannel) &&
            renderChannel("Forum Saya", dataMyChannel)}
          {_.isEmpty(searchValue) ? (
            <View style={styles.containerListCardBanner}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <View style={styles.listBannerWrapper}>
                  <View style={{ width: 16 }} />

                  {dataBanner.map((item) => {
                    return (
                      <View key={item.id} style={styles.itemBannerWrapper}>
                        <CardItemBannerProduct
                          name={item.name}
                          onPress={() => {
                            onPressBannerProduct(item);
                            AdjustTracker(AdjustTrackerConfig.Forum_Banner);
                          }}
                        />
                      </View>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
          ) : null}

          <Text H7 style={styles.textAllChannel}>
            Semua Forum
          </Text>
        </View>
      );
    } else {
      return null;
    }
  };

  useDebouncedEffect(
    () => {
      console.log(searchValue); // debounced 1sec
      // call search api ...
      if (searchValue.trim().length >= 3) {
        filterSet(searchValue);
        inputSearchRef.current.blur();
      } else if (
        searchValue.trim().length < 3 &&
        searchValue.trim().length > 0
      ) {
        Toast.show({
          text: "Pencarian harus spesifik min. 3 karakter",
          position: "top",
          duration: 2000,
        });
        setSearchValue("");
      }
    },
    1000,
    [searchValue, isTriggerSearch]
  );

  const onChangeTextSearch = (text) => {
    setPageSearchChannel(1);
    setDataFilterChannel([]);
    if (text.length > 0) {
      setSearchValue(text);
      setIsTriggerSearch(!isTriggerSearch);
    } else {
      setIsDataNotFound(false);
      setSearchValue("");
      onRefreshdata();
    }
  };

  const filterSet = (input) => {
    setIsRefresh(false);
    filterChannel(input);
    setSearchValue(input);
  };

  const filterChannel = async (input) => {
    setIsFailed(false);
    setIsFetching(true);
    setIsFocusSearch(false);
    console.log("pageSearchChannel ->", pageSearchChannel);
    try {
      let paramsGetAllChannel = {
        terms: input,
        paginate: pageSearchChannel,
        limit: 10,
      };
      response = await getChannel(paramsGetAllChannel);
      if (response.header.message == "Success") {
        let result = response.data.data;
        setDataFromResponse(result, false);
        console.log("LOG RESULT FILTER CHANNEL -> ", result);
        setIsFailed(false);
        setIsFetching(false);
        setPageSearchChannel(pageSearchChannel + 1);
      } else {
        setIsFailed(true);
        setIsFetching(false);
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      setIsFailed(true);
      setIsFetching(false);
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  const renderHistorySearch = () => {
    return (
      <View style={{ marginTop: 4 }}>
        <HistorySearch
          onPressRemoveItem={(item) => onPressRemoveItemHistory(item)}
          onPressItem={(txt) => {
            onChangeTextSearch(txt);
            AdjustTracker(AdjustTrackerConfig.Forum_Cari_Terakhir);
          }}
          data={historySearchList}
        />
      </View>
    );
  };

  const onPressRemoveItemHistory = async (item) => {
    await removeHistorySearch(EnumTypeHistorySearch.ALL_CHANNEL, item.index);
    await loadHistorySearch();
  };

  const loadHistorySearch = async () => {
    let dataHistorySearch = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL
    );
    dataHistorySearch =
      JSON.parse(dataHistorySearch) != null
        ? JSON.parse(dataHistorySearch).historySearch
        : [];
    setHistorySearchList(dataHistorySearch);
  };

  const renderSearchNotFound = () => {
    return (
      <SearchDataNotFound
        title="Pencarian Tidak Ditemukan"
        subtitle="Maaf kami tidak bisa menemukan hasil yang Anda cari. Coba gunakan kata yang lain."
      />
    );
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={modalizeRef}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        <BottomSheet
          onPressClose={() => {
            onPressButtonJoinChannel(itemChannel);
            onClose();
            AdjustTracker(AdjustTrackerConfig.Forum_Gabung_Finish);
          }}
          title={titleBottomSheet}
          desc={descBottomSheet}
          wordingClose="GABUNG KE FORUM INI"
          type="bottomSheetAboutChannel"
          imageSource={itemChannel.logo}
        />
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const dataBanner = [
    {
      name: "Dkonsul",
      url:
        "https://docs.google.com/forms/d/e/1FAIpQLSd8eJrjKEjxzPietkJZeHOdy4Mhwr-GQVjmVj3TAMu3M5rJeA/viewform",
    },
    {
      name: "Teman Bumil",
      url: "",
    },
    {
      name: "Teman Diabetes",
      url:
        "https://docs.google.com/forms/d/e/1FAIpQLSeLhMdzmdLi2ny-Ft2-figMT4ImmQUzwPfo3MyWPjE3lawCvA/viewform",
    },
  ];

  const onPressBannerProduct = (item) => {
    let params = {
      name: item.name,
      title: item.name,
      url: item.url,
    };

    if (item.name != "Teman Bumil") {
      navigation.navigate("BannerProductDetail", params);
    }
  };

  const renderContent = () => {
    if (isDataNotFound && !_.isEmpty(searchValue)) {
      return (
        <View style={styles.searchNotFoundWrapper}>
          {renderSearchNotFound()}
        </View>
      );
    } else if (isFocusSearch && !_.isEmpty(historySearchList)) {
      return <View>{renderHistorySearch()}</View>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <FlatList
            columnWrapperStyle={{
              justifyContent: "space-between",
              marginHorizontal: 16,
            }}
            data={_.isEmpty(searchValue) ? dataAllChannel : dataFilterChannel}
            numColumns={2}
            renderItem={({ item }) => {
              return (
                <View key={item.id} style={styles.itemCardAllChannelWrapper}>
                  <CardItemChannel
                    name={item.channel_name}
                    isJoin={item.subscribed}
                    onPressCard={() => {
                      onPressCardCondition(item);
                      item.subscribed == true && searchValue != ""
                        ? AdjustTracker(
                            AdjustTrackerConfig.Forum_Cari_Masuk_Forum
                          )
                        : null;
                    }}
                    onPressButton={() => {
                      onPressButtonInCard(item);
                      AdjustTracker(AdjustTrackerConfig.Forum_Gabung_Start);
                    }}
                    imageSource={item.logo}
                  />
                </View>
              );
            }}
            onEndReachedThreshold={0.5}
            onEndReached={handleLoadMore}
            showsVerticalScrollIndicator={false}
            ListFooterComponent={_renderItemFooter()}
            ListHeaderComponent={renderHeaderList()}
            onRefresh={() => {
              onRefreshdata();
            }}
            refreshing={false}
          />
        </View>
      );
    }
  };

  const onPressCloseButton = () => {
    inputSearchRef.current.focus();
    setSearchValue("");
  };

  return (
    <Container>
      {renderBottomSheet()}
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.viewArrowBackButton}>
              <ArrowBackButton onPress={() => handleBackButton()} />
            </View>
            <SearchBarField
              type="searchChannel"
              textplaceholder={"Cari Forum"}
              onFocus={() => {
                setIsFocusSearch(true);
                AdjustTracker(AdjustTrackerConfig.Forum_Cari);
              }}
              onBlur={() => {
                _.isEmpty(historySearchList) ? setIsFocusSearch(false) : null;
              }}
              onChangeText={(text) => {
                onChangeTextSearch(text);
                AdjustTracker(AdjustTrackerConfig.Forum_Cari_Start);
              }}
              onButtonClose={() => onPressButtonClose()}
              inputReff={inputSearchRef}
              inputText={searchValue}
              isCloseButton={searchValue.trim() == "" ? false : true}
              onPressCloseButton={() => onPressCloseButton()}
            />
          </Body>
        </Header>
      </SafeAreaView>

      {renderContent()}
    </Container>
  );
};

export default Channel;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },

  containerChannel: {
    marginTop: 22,
    marginBottom: 10, //set 10 because listChannelWrapper has padding 10
    flexDirection: "row",
    alignItems: "baseline",
    flex: 1,
    justifyContent: "space-between",
  },
  textRekomendasiChannel: {
    fontSize: 18,
    fontFamily: "Roboto-Medium",
    color: "#000000",
    alignItems: "flex-start",
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  textLihatSemua: {
    fontSize: 14,
    fontFamily: "Roboto-Medium",
    color: "#0771CD",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    lineHeight: 16,
    letterSpacing: 0.34,
  },
  containerListCardChannel: {
    // paddingLeft: 16,
    marginRight: -16,
    marginLeft: 8,
  },

  containerListCardBanner: {
    // paddingLeft: 16,
    marginRight: -16,
  },
  listChannelWrapper: {
    flexDirection: "row",
    marginRight: -16,
  },
  listBannerWrapper: {
    flexDirection: "row",
    paddingBottom: 10,
    paddingHorizontal: -16,
  },
  itemCardMyChannelWrapper: {
    marginRight: 16,
    width: Dimensions.get("window").width / 2.5,
    paddingTop: 8,
    paddingBottom: 10, //set 14 because containerChannel has margin 10
    paddingHorizontal: 1,
  },
  itemBannerWrapper: {
    marginTop: 24,
  },
  itemCardAllChannelWrapper: {
    marginBottom: 16,
    width: Dimensions.get("window").width / 2 - 24,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
    marginRight: 16,
  },
  textAllChannel: {
    marginTop: 14, //set 14 because listChannelWrapper has padding 10
    marginBottom: 20,
    marginLeft: 16,
    lineHeight: 24,
    letterSpacing: 0.17,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  containerItemFooter: {
    justifyContent: "center",
    alignItems: "center",
  },
  centerEmptyList: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  searchNotFoundWrapper: {
    justifyContent: "center",
    flex: 1,
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 8 },
});
