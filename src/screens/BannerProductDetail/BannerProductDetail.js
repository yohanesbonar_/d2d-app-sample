import { Container, Content } from "native-base";
import React, { useEffect } from "react";
import {
  BackHandler,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Linking,
  SafeAreaView,
} from "react-native";
import {
  ArrowCalendar,
  BottomSheet,
  Button,
  HeaderToolbar,
} from "../../components";
import _ from "lodash";
import {
  IconDkonsulChannel,
  IconDkonsulHeaderProductDetail,
  IconTemanBumilHeaderProductDetail,
  IconTemanDiabetesHeaderProductDetail,
} from "../../assets";
import { ThemeD2D } from "../../../theme";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const BannerProductDetail = ({ navigation }) => {
  const params = navigation.state.params;
  console.log("params -> ", params);
  useEffect(() => {
    initData();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const initData = async () => {
    if (params.name == "Dkonsul") {
      AdjustTracker(
        AdjustTrackerConfig.Forum_Produk_Dkonsul_Registration_Start
      );
    } else if (params.name == "Teman Bumil") {
      AdjustTracker(
        AdjustTrackerConfig.Forum_Produk_Teman_Bumil_Registration_Start
      );
    } else if (params.name == "Teman Diabetes") {
      AdjustTracker(
        AdjustTrackerConfig.Forum_Produk_Teman_Diabetes_Registration_Start
      );
    }
  };

  const onPressButton = () => {
    if (params.name == "Teman Bumil") {
      const APP_STORE_LINK =
        "https://itunes.apple.com/id/app/teman-bumil/id1298915495?mt=8";
      const PLAY_STORE_LINK =
        "https://play.google.com/store/apps/details?id=com.temanbumil.android";
      if (ThemeD2D.platform == "ios") {
        Linking.openURL(APP_STORE_LINK).catch((err) =>
          console.error("An error occurred", err)
        );
      } else {
        Linking.openURL(PLAY_STORE_LINK).catch((err) =>
          console.error("An error occurred", err)
        );
      }
    } else {
      if (params.name == "Dkonsul") {
        AdjustTracker(
          AdjustTrackerConfig.Forum_Produk_Dkonsul_Registration_Register
        );
      } else if (params.name == "Teman Diabetes") {
        AdjustTracker(
          AdjustTrackerConfig.Forum_Produk_Teman_Diabetes_Registration_Register
        );
      } else if (params.name == "Teman Bumil") {
        AdjustTracker(
          AdjustTrackerConfig.Forum_Produk_Teman_Bumil_Registration__Register
        );
      }
      navigation.navigate("WebviewPage", params);
    }
  };

  const renderButtonAbsolute = () => {
    let temp = "";
    if (params.name == "Dkonsul") {
      temp = "DKONSUL";
    } else if (params.name == "Teman Diabetes") {
      temp = "TEMAN DIABETES";
    } else if (params.name == "Teman Bumil") {
      temp = "TEMAN BUMIL";
    }
    return (
      <View style={styles.buttonWrapper}>
        <Button
          text={"DAFTAR " + temp}
          type={"elevationButtonBlue"}
          onPress={() => {
            onPressButton();
            // setIsRegister(isRegister ? false : true);
          }}
        />
      </View>
    );
  };

  let whatDesc = "";
  let howWork = "";
  let benefit = "";
  let benefitHospital = "";
  if (params.name == "Dkonsul") {
    whatDesc =
      "DKonsul adalah sebuah platform telekonsultasi di mana pasien dapat mendapatkan layanan konsultasi kesehatan dengan dokter dari berbagai macam spesialisasi yang terdaftar di RS/Klinik terpercaya.";
    howWork =
      "Pasien dapat memilih rentang waktu konsultasi 15, 30 & 60 menit sesuai kebutuhan. Berdasarkan hasil konsultasi, dokter dapat memberikan resep obat elektronik yang dapat langsung digunakan pasien untuk membeli obat di GoApotik, platform e-commerce apotik yang bekerjasama dengan DKonsul.";
    benefit =
      "- Dapat menentukan sendiri jadwal praktek serta harga per konsultasi yang diberikan kepada pasien \n- Dapat melayani beberapa konsultasi pasien dalam satu waktu secara bersamaan \n- Dapat melayani pasien yang tidak dapat melakukan kunjungan ke RS/Klinik \n- Dapat memberikan resep obat secara elektronik";
    benefitHospital =
      "- Dapat menentukan sendiri jadwal praktek serta harga per konsultasi yang diberikan kepada pasien \n- Dapat melayani beberapa konsultasi pasien dalam satu waktu secara bersamaan - Dapat melayani pasien yang tidak dapat melakukan kunjungan ke RS/Klinik \n- Dapat memberikan resep obat secara elektronik";
  } else if (params.name == "Teman Diabetes") {
    whatDesc =
      "Kelola adalah program pengelolaan diabetes berkelanjutan di mana pasien (menggunakan aplikasi TD) dihubungkan dengan 1 dokter yang dapat memantau data kesehatan pasien secara real-time. Data kesehatan di catat secara mandiri oleh pasien dan dokter yang terhubung dapat menggunakan data tersebut untuk membarikan personalized feedback.";
    howWork =
      "Pasien mencatat data kesehatan berupa gula darah, asupan makanan, aktivitas fisik dan data lainnya pada aplkasi Teman Diabetes. Dokter yang terhubung dapat melihat data ini secara real-time dan memberikan personalized feedback melalui konsultasi online (DKonsul) serta rujukan kepada edukator in-house Teman Diabetes untuk mendapatkan edukasi terkait lifestyle manegement secara menyeluruh.";
    benefit =
      "- Dapat menentukan sendiri jadwal praktek serta harga per konsultasi yang diberikan kepada pasien \n- Dapat melayani beberapa konsultasi pasien dalam satu waktu secara bersamaan \n- Dapat melayani pasien yang tidak dapat melakukan kunjungan ke RS/Klinik \n- Dapat memberikan resep obat secara elektronik";
    benefitHospital = "";
  } else if (params.name == "Teman Bumil") {
    whatDesc =
      "Teman Bumil adalah aplikasi Teman Ibu Milenial yang ditujukan untuk calon ibu dalam masa perencanaan kehamilan, ibu yang sedang hamil, dan ibu dengan anak dalam fase tumbuh kembang usia 0-5 tahun. Teman Bumil hadir sebagai bentuk dukungan untuk ibu Indonesia, dengan menyediakan informasi-informasi terpercaya dan berbagai fitur menarik lainnya.";
    howWork =
      "Dokter bisa turut berpartisipasi untuk mendukung ibu di Indonesia dengan menjadi narasumber pada event online Teman Bumil. Ada berbagai macam event online, seperti #TanyaDokter, Kelas Online, #CurhatYuk, #NgobrolBarengBubid, Instagram Live, dan YouTube Live. Selain itu, dokter juga dapat berpartisipasi dengan memberikan review pada artikel Teman Bumil.";
    benefit =
      "Dokter dapat membantu para ibu Indonesia dari mana pun dan kapan pun! Pekerjaan ini bersifat remote dan waktu pelaksanaannya sangat fleksibel.";
    benefitHospital = "";
  }

  return (
    <Container>
      <HeaderToolbar onPress={handleBackButton} title={params.name} />
      <SafeAreaView style={{ flex: 1, backgroundColor: "transparent" }}>
        <View style={{ flex: 1 }}>
          <Content showsVerticalScrollIndicator={false}>
            <View style={styles.containerContent}>
              <View style={styles.containerBannerHeader(params.name)}>
                {params.name == "Dkonsul" ? (
                  <IconDkonsulHeaderProductDetail />
                ) : params.name == "Teman Bumil" ? (
                  <IconTemanBumilHeaderProductDetail />
                ) : (
                  <IconTemanDiabetesHeaderProductDetail />
                )}

                <Text style={styles.textGabung(params.name)}>
                  {params.name == "Teman Diabetes"
                    ? "Mari Bergabung Bersama Kelola"
                    : "Mari Bergabung Bersama " + params.name}
                </Text>
              </View>
              <View style={styles.mainContainer}>
                <View style={styles.viewTitleDesc}>
                  <View style={styles.viewLeftTitleDesc} />
                  <Text style={styles.textTitleDesc}>
                    {params.name == "Teman Diabetes"
                      ? "Apa itu kelola?"
                      : "Apa itu " + params.name + "?"}
                  </Text>
                </View>
                <Text style={styles.textDesc}>{whatDesc}</Text>
                <View style={styles.viewTitleDesc}>
                  <View style={styles.viewLeftTitleDesc} />
                  <Text style={styles.textTitleDesc}>
                    Bagaimana cara kerjanya?
                  </Text>
                </View>
                <Text style={styles.textDesc}>{howWork}</Text>
                <View style={styles.viewTitleDesc}>
                  <View style={styles.viewLeftTitleDesc} />
                  <Text style={styles.textTitleDesc}>
                    {params.name == "Teman Diabetes"
                      ? "Apa keuntungannya?"
                      : "Apa keuntungannya untuk dokter?"}
                  </Text>
                </View>
                <Text style={styles.textDesc}>{benefit}</Text>
                {benefitHospital != "" ? (
                  <View>
                    <View style={styles.viewTitleDesc}>
                      <View style={styles.viewLeftTitleDesc} />
                      <Text style={styles.textTitleDesc}>
                        Apa keuntungannya untuk rumah sakit / klinik?
                      </Text>
                    </View>
                    <Text style={styles.textDesc}>{benefitHospital}</Text>
                  </View>
                ) : null}
              </View>
            </View>
          </Content>
          {renderButtonAbsolute()}
        </View>
      </SafeAreaView>
    </Container>
  );
};

export default BannerProductDetail;

const styles = StyleSheet.create({
  imageBackgroundContainer: {
    aspectRatio: 16 / 9,
    justifyContent: "center",
    alignItems: "center",
  },
  mainContainer: { padding: 16, flex: 1, flexDirection: "column" },
  textTitle: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
  },
  smallText: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
  },
  viewSmallText: {
    flex: 1,
    marginTop: 12,
    flexDirection: "row",
    alignItems: "center",
  },
  lineGrayContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 8,
    borderWidth: 0.1,
    borderEndColor: "#EBEBEB",
    marginHorizontal: -16,
    marginVertical: 20,
  },
  textTitleDesc: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    paddingLeft: 8,
  },
  viewTitleDesc: {
    flex: 1,
    marginBottom: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  viewLeftTitleDesc: {
    backgroundColor: "#D01E53",
    height: 16,
    width: 4,
    borderWidth: 0.1,
    borderEndColor: "#D01E53",
    borderRadius: 3,
  },
  textDesc: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    marginBottom: 20,
  },

  viewOuterContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  imageStyleImageBackground: { borderRadius: 0 },
  viewOptionIkutiKami: {
    flex: 1,
    flexDirection: "row",
    alignContent: "center",
    justifyContent: "space-between",
    marginVertical: 16,
  },
  viewPerOption: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
  },
  textOption: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    color: "#000000",
    paddingTop: 16,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  modalStyleBottomSheet: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    paddingHorizontal: 16,
  },
  handleStyleBottomSheet: {
    backgroundColor: "transparent",
  },
  containerBannerHeader: (name) => ({
    marginHorizontal: 16,
    marginVertical: 20,
    backgroundColor:
      name == "Dkonsul"
        ? "#E2F1FC"
        : name == "Teman Bumil"
        ? "#EEE5EF"
        : name == "Teman Diabetes"
        ? "#FCE3EA"
        : null,
    borderRadius: 8,
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: 20,
  }),
  textGabung: (name) => ({
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color:
      name == "Dkonsul"
        ? "#0771CD"
        : name == "Teman Bumil"
        ? "#A16DA9"
        : name == "Teman Diabetes"
        ? "#EE424E"
        : null,
    paddingTop: 12,
    marginTop: 20,
  }),
  buttonWrapper: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    paddingTop: 16,
    paddingHorizontal: 16,
    paddingBottom: 16,
    backgroundColor: "#FFFFFF",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.02,
    shadowRadius: 4,
  },
  containerContent: {
    marginBottom: 60, //margin vertical button wrapper + height button - margin bottom textDesc
  },
});
