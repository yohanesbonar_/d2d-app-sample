import { Container, Content, Toast } from "native-base";
import React, { useEffect } from "react";
import {
  BackHandler,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  RefreshControl,
  Image,
  SafeAreaView,
  Linking,
} from "react-native";
import { BottomSheet, Button, HeaderToolbar } from "../../components";
import { useState } from "react";
import { ThemeD2D } from "../../../app";
import _ from "lodash";
import { Modalize } from "react-native-modalize";
import { jobDetail } from "../../utils/network/job/jobDetail";
import { Loader } from "../../../app/components";
import { postApplyJob } from "../../utils/network/job/postApplyJob";
import { getData, KEY_ASYNC_STORAGE, testID } from "../../utils";
import HTML from "react-native-render-html";
import { share } from "../../../app/libs/Common";

const JobDetail = ({ navigation }) => {
  const params = navigation.state.params;
  const [isRegister, setIsRegister] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [data, setData] = useState({});
  const [title, setTitle] = useState("");
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [arraySpec, setArraySpec] = useState([]);
  const [arrayHeader, setArrayHeader] = useState([]);
  const [xid, setXid] = useState("");
  const [isApplied, setIsApplied] = useState(false);
  const [isCompletedProfileData, setIsCompleteProfileData] = useState(false);
  const [typeBottomsheet, setTypeBottomsheet] = useState("");
  const [dataLocalProfile, setDataLocalProfile] = useState({});
  const [isSuccessDaftar, setIsSuccessDaftar] = useState(false);
  const [wordingButtonAbsolute, setWordingButtonAbsolute] = useState("");
  const [isJoinWithDirectLink, setJoinWithDirectLink] = useState(false);
  const [registrationUrl, setRegistrationUrl] = useState("");
  const [isNewSubscribe, setIsNewSubscribe] = useState(
    params.isNewSubscribe ? true : false
  );

  const [isImageError, setIsImageError] = useState(false);

  useEffect(() => {
    console.log("params", params);
    getAllData();

    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    if (params.from && params.from != "Feeds") {
      params.onBack();
    }
    navigation.goBack();
    return true;
  };

  const getAllData = () => {
    setIsRefresh(false);
    getDataProfileFromLocal();
    initData();

    if (isNewSubscribe) {
      setIsNewSubscribe(false);
      Toast.show({
        text: "Berhasil gabung ke forum.",
        position: "bottom",
        buttonText: "TUTUP",
        duration: 3000,
      });
    }
  };

  const getDataProfileFromLocal = async () => {
    let dataProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    let isCompletedProfile = true;
    setDataLocalProfile(dataProfile);
    console.log("data profile ->", dataProfile);
    if (dataProfile != null) {
      if (
        _.isEmpty(dataProfile.subscription) ||
        _.isEmpty(dataProfile.home_location) ||
        _.isEmpty(dataProfile.phone) ||
        (_.isEmpty(dataProfile.clinic_location_1) &&
          _.isEmpty(dataProfile.clinic_location_2) &&
          _.isEmpty(dataProfile.clinic_location_3)) ||
        (_.isEmpty(dataProfile.clinic_type_1) &&
          _.isEmpty(dataProfile.clinic_type_2) &&
          _.isEmpty(dataProfile.clinic_type_3))
      ) {
        isCompletedProfile = false;
      }

      setIsCompleteProfileData(isCompletedProfile);
    }
  };

  const initData = async () => {
    let response = {};
    setIsShowLoader(true);
    try {
      response = await jobDetail(params.item.xid, params.sbcChannel);

      console.log("result JOB detail", response);
      if (response.header.message == "Success") {
        setData(response.data);
        setTitle(response.data.type.name);
        setArraySpec(response.data.spec);
        setArrayHeader(response.data.header_attributes);
        setXid(response.data.xid);
        setIsApplied(response.data.applied);
        setWordingButtonAbsolute(response.data.apply_button);
        setIsShowLoader(false);

        if (response.data != null && response.data.registration_url) {
          setJoinWithDirectLink(true);
          setRegistrationUrl(response.data.registration_url);
          setWordingButtonAbsolute(response.data.registration_button_caption);
        }
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
        setIsShowLoader(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }
  };

  const onRefreshData = () => {
    setIsImageError(false);
    setData({});
    setTitle("");
    setArraySpec([]);
    setArrayHeader([]);
    setXid("");
    setIsApplied(false);
    setIsRefresh(true);
  };

  useEffect(() => {
    if (isRefresh) {
      getAllData();
    }
  }, [isRefresh]);

  const renderButtonAbsolute = (item) => {
    if (!isApplied) {
      return isJoinWithDirectLink ? (
        <Button
          text={wordingButtonAbsolute.toUpperCase()}
          type={"elevationButtonBlue"}
          onPress={() => {
            onPressButtonApplyByLink();
          }}
        />
      ) : (
        <Button
          text={wordingButtonAbsolute.toUpperCase()}
          type={"elevationButtonBlue"}
          onPress={() => {
            onPressButtonApply();
          }}
        />
      );
    } else {
      return (
        <Button
          text={wordingButtonAbsolute.toUpperCase()}
          type={"elevationButtonGray"}
        />
      );
    }
  };

  const renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={styles.handleStyleBottomSheet}
        adjustToContentHeight={true}
        closeOnOverlayTap={
          typeBottomsheet == "BottomSheetProfileUnfinished" ? true : false
        }
        panGestureEnabled={
          typeBottomsheet == "BottomSheetProfileUnfinished" ? true : false
        }
        ref={modalizeRef}
        modalStyle={styles.modalStyleBottomSheet}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        // onClose={onClose}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        {typeBottomsheet == "BottomSheetRegister" ? (
          <BottomSheet
            onPressClose={() => onPressClose()}
            title="Terima Kasih Sudah Daftar"
            desc="Anda akan dihubungi admin secepatnya."
            wordingClose="OKE"
            type="BottomSheetRegister"
          />
        ) : null}
        {typeBottomsheet == "BottomSheetProfileUnfinished" ? (
          <BottomSheet
            onPressClose={() => gotoProfile()}
            title="Profil Belum Lengkap"
            desc="Mohon lengkapi profil Anda terlebih dahulu sebelum mendaftarkan job ini."
            wordingClose="LENGKAPI PROFIL SEKARANG"
            type="BottomSheetProfileUnfinished"
          />
        ) : null}
      </Modalize>
    );
  };

  const modalizeRef = React.createRef(null);

  const onOpen = () => {
    console.log("openBottomsheet");
    modalizeRef.current?.open();
  };

  const gotoProfile = async () => {
    onClose();
    navigation.navigate("ChangeDetailProfile", {
      ...dataLocalProfile,
      onRefreshDataProfile: postDataDaftarJob,
      from: "JobDetail",
    });
  };

  const onPressClose = async () => {
    onRefreshData();
    onClose();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const onPressButtonApply = () => {
    if (isCompletedProfileData) {
      postDataDaftarJob();
    } else {
      setTypeBottomsheet("BottomSheetProfileUnfinished");
      onOpen();
    }
  };

  const onPressButtonApplyByLink = () => {
    if (isCompletedProfileData) {
      if (registrationUrl) {
        Linking.openURL(registrationUrl);
      } else {
        Toast.show({
          text: "Something went wrong! ",
          position: "top",
          duration: 3000,
        });
      }
    } else {
      setTypeBottomsheet("BottomSheetProfileUnfinished");
      onOpen();
    }
  };

  const postDataDaftarJob = async () => {
    if (registrationUrl) {
      setIsCompleteProfileData(true);
      return;
    }
    let response = {};
    try {
      response = await postApplyJob(xid, params.sbcChannel);

      console.log("result JOB APPLY", response);
      if (response.header.message == "Success") {
        console.log("success apply");
        setIsSuccessDaftar(true);
      } else {
        Toast.show({
          text: "Something went wrong! " + response.header.reason.id,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  useEffect(() => {
    if (isSuccessDaftar) {
      onOpenBottomSheetSuccessDaftar();
    }
  }, [isSuccessDaftar]);

  const onOpenBottomSheetSuccessDaftar = () => {
    setIsSuccessDaftar(false);
    setTypeBottomsheet("BottomSheetRegister");
    onOpen();
  };

  const renderSpec = () => {
    return (
      <View style={{ flexDirection: "column" }}>
        {arraySpec.map((item, index) => {
          return (
            <View key={index}>
              {!_.isEmpty(item.icon) ? (
                <View style={styles.viewTitleDesc}>
                  <Image
                    source={{ uri: item.icon }}
                    style={styles.iconImageSpec}
                  />

                  <Text style={styles.textTitleDesc}>{item.title}</Text>
                </View>
              ) : (
                <View style={styles.viewTitleDesc}>
                  <View style={styles.viewLeftTitleDesc} />
                  <Text style={styles.textTitleDesc}>{item.title}</Text>
                  {/* <Text style={styles.textTitleDesc2}>{item.title}</Text> */}
                </View>
              )}

              <HTML
                {...testID("desc_spec_job_detail")}
                baseFontStyle={styles.textDesc}
                html={item.description}
                ignoredStyles={[
                  "font-family",
                  "text-decoration-style",
                  "text-decoration-color",
                ]}
                onLinkPress={(e, href, htmlAttrib) => Linking.openURL(href)}
              />
            </View>
          );
        })}
      </View>
    );
  };

  const onImageError = () => {
    setIsImageError(true);
  };

  const renderHeader = () => {
    return (
      <View style={{ flexDirection: "column" }}>
        {arrayHeader.map((item, index) => {
          return (
            <View key={index} style={styles.viewSmallText}>
              <Image
                source={{ uri: item.icon }}
                style={styles.iconImageHeader}
              />
              <Text style={[styles.smallText, { marginLeft: 8 }]}>
                {item.value}
              </Text>
            </View>
          );
        })}
      </View>
    );
  };

  let defaultCover = require("../../../src/assets/images/blank.jpg");
  let uriCover = data.image != null ? { uri: data.image } : null;
  return (
    <Container>
      <Loader visible={isShowLoader} />
      {renderBottomSheet()}
      <HeaderToolbar
        onPress={handleBackButton}
        title={
          "Job " + title.charAt(0).toUpperCase() + title.slice(1).toLowerCase()
        }
        onPressRightMenu={() =>
          share(data.share_link_app ? data.share_link_app : null)
        }
        iconRightMenu="IconShare"
      />
      <SafeAreaView style={{ flex: 1, backgroundColor: "transparent" }}>
        {!isShowLoader ? (
          <View style={{ flex: 1 }}>
            <Content
              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl refreshing={false} onRefresh={onRefreshData} />
              }
            >
              <View style={styles.containerContent}>
                {!_.isEmpty(data.image) &&
                  data.image != null &&
                  !isImageError && (
                    <ImageBackground
                      style={styles.imageBackgroundContainer}
                      source={uriCover}
                      imageStyle={styles.imageStyleImageBackground}
                      defaultSource={require("../../../app/assets/images/preloader.png")}
                      onError={onImageError}
                    />
                  )}

                <View style={styles.mainContainer}>
                  <Text style={styles.textTitle}>{data.title}</Text>
                  {renderHeader()}
                  <View style={styles.lineGrayContainer} />
                  {renderSpec()}
                </View>
              </View>
            </Content>
            <View style={styles.buttonWrapper}>
              {renderButtonAbsolute(data)}
            </View>
          </View>
        ) : null}
      </SafeAreaView>
    </Container>
  );
};

export default JobDetail;

const styles = StyleSheet.create({
  imageBackgroundContainer: {
    aspectRatio: 16 / 9,
    justifyContent: "center",
    alignItems: "center",
  },
  mainContainer: { padding: 16, flex: 1, flexDirection: "column" },
  textTitle: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  smallText: {
    fontFamily: "Roboto-Medium",
    fontSize: 12,
    color: "#666666",
    lineHeight: 16,
    letterSpacing: 0.19,
  },
  viewSmallText: {
    flex: 1,
    marginTop: 12,
    flexDirection: "row",
    alignItems: "center",
  },
  lineGrayContainer: {
    flex: 1,
    backgroundColor: "#EBEBEB",
    height: 8,
    borderWidth: 0.1,
    borderEndColor: "#EBEBEB",
    marginHorizontal: -16,
    marginTop: 20,
    marginBottom: 16,
  },
  textTitleDesc: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    paddingLeft: 8,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  textTitleDesc2: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
  },
  viewTitleDesc: {
    flex: 1,
    marginTop: 4,
    flexDirection: "row",
    alignItems: "center",
  },
  viewLeftTitleDesc: {
    backgroundColor: "#D01E53",
    height: 16,
    width: 4,
    borderWidth: 0.1,
    borderEndColor: "#D01E53",
    borderRadius: 3,
  },
  textDesc: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    // marginBottom: 20,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  viewButtonAbsolute: {
    // marginTop: 16,
    // marginHorizontal: 16,
    // elevation: 4,
  },
  viewOuterContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  imageStyleImageBackground: { borderRadius: 0 },
  viewOptionIkutiKami: {
    flex: 1,
    flexDirection: "row",
    alignContent: "center",
    justifyContent: "space-between",
    marginVertical: 16,
  },
  viewPerOption: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
  },
  textOption: {
    fontFamily: "Roboto-Regular",
    fontSize: 14,
    color: "#000000",
    paddingTop: 16,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
  modalStyleBottomSheet: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    paddingHorizontal: 16,
  },
  handleStyleBottomSheet: {
    backgroundColor: "transparent",
  },
  iconImageHeader: { height: 12, width: 12 },
  iconImageSpec: { height: 16, width: 16 },
  buttonWrapper: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    padding: 16,
    backgroundColor: "#FFFFFF",
  },
  containerContent: {
    marginBottom: 60, //margin vertical button wrapper + height button - margin bottom textDesc
  },
});
