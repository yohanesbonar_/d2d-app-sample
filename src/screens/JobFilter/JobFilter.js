import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
  Radio,
  ListItem,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconArrowRight, IconBack, IconExpandMoreWhite } from "../../assets";
import _, { initial } from "lodash";
import platform from "../../../theme/variables/platform";
import { Loader } from "../../../app/components";
import {
  ArrowBackButton,
  Button,
  SortOptionButton,
} from "../../components/atoms";
import { HeaderToolbar } from "../../components";
import {
  AdjustTracker,
  AdjustTrackerConfig,
} from "../../../app/libs/AdjustTracker";

const JobFilter = ({ navigation }) => {
  const params = navigation.state.params;
  const [filterValue, setFilterValue] = useState({});
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [isSortAtoZ, setIsSortAtoZ] = useState(false);
  const [isSortZtoA, setIsSortZtoA] = useState(false);
  const [isEarlier, setIsEarlier] = useState(false);
  const [isLatest, setIsLatest] = useState(false);
  const [specialistID, setSpecialistID] = useState("");
  const [specialistName, setSpecialistName] = useState("");

  useEffect(() => {
    initData();
    AdjustTracker(AdjustTrackerConfig.Forum_Job_Filter_Start);
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  const initData = () => {
    if (!_.isEmpty(params.filterValue)) {
      if (params.filterValue.sortBy == "alphabetic") {
        setIsSortAtoZ(true);
        setIsSortZtoA(false);
        setIsLatest(false);
        setIsEarlier(false);
      } else if (params.filterValue.sortBy == "reversealphabetic") {
        setIsSortAtoZ(false);
        setIsSortZtoA(true);
        setIsLatest(false);
        setIsEarlier(false);
      } else if (params.filterValue.sortBy == "added") {
        setIsLatest(false);
        setIsEarlier(true);
        setIsSortAtoZ(false);
        setIsSortZtoA(false);
      } else if (params.filterValue.sortBy == "oldest") {
        setIsLatest(true);
        setIsEarlier(false);
        setIsSortAtoZ(false);
        setIsSortZtoA(false);
      } else {
        setIsLatest(false);
        setIsEarlier(false);
        setIsSortAtoZ(false);
        setIsSortZtoA(false);
      }

      setSpecialistID(params.filterValue.specialistID);
      setSpecialistName(params.filterValue.specialistName);
    }
  };

  const renderButtonApply = () => {
    let sortBy = "";

    if (
      isSortAtoZ == true &&
      isSortZtoA == false &&
      isEarlier == false &&
      isLatest == false
    ) {
      sortBy = "alphabetic";
    } else if (
      isSortAtoZ == false &&
      isSortZtoA == true &&
      isEarlier == false &&
      isLatest == false
    ) {
      sortBy = "reversealphabetic";
    } else if (
      isEarlier == true &&
      isLatest == false &&
      isSortAtoZ == false &&
      isSortZtoA == false
    ) {
      sortBy = "added";
    } else if (
      isEarlier == false &&
      isLatest == true &&
      isSortAtoZ == false &&
      isSortZtoA == false
    ) {
      sortBy = "oldest";
    }

    let filterValue = {
      sortBy: sortBy,
      specialistID: specialistID,
      specialistName: specialistName,
    };

    return (
      <View style={styles.containerButtonApply}>
        <Button
          text="TERAPKAN FILTER"
          type={"elevationButton"}
          onPress={() => {
            AdjustTracker(AdjustTrackerConfig.Forum_Job_Filter_Terapkan);
            navigation.state.params.onSelect({ filterValue });
            navigation.goBack();
            navigation.state.params.onRefresh();
            return true;
          }}
        />
      </View>
    );
  };

  const onPressReset = () => {
    setIsSortAtoZ(false);
    setIsSortZtoA(false);
    setIsEarlier(false);
    setIsLatest(false);
    setSpecialistID("");
    setSpecialistName("");
    AdjustTracker(AdjustTrackerConfig.Forum_Job_Filter_Reset);
  };

  const onPressSort = (type) => {
    if (type == "A ke Z") {
      setIsSortAtoZ(isSortAtoZ ? false : true);
      if (isSortZtoA) {
        setIsSortZtoA(false);
      }
      if (isEarlier) {
        setIsEarlier(false);
      }
      if (isLatest) {
        setIsLatest(false);
      }
    } else if (type == "Z ke A") {
      setIsSortZtoA(isSortZtoA ? false : true);
      if (isSortAtoZ) {
        setIsSortAtoZ(false);
      }
      if (isEarlier) {
        setIsEarlier(false);
      }
      if (isLatest) {
        setIsLatest(false);
      }
    } else if (type == "Terdekat") {
      setIsLatest(isLatest ? false : true);
      if (isSortZtoA) {
        setIsSortZtoA(false);
      }
      if (isSortAtoZ) {
        setIsSortAtoZ(false);
      }
      if (isEarlier) {
        setIsEarlier(false);
      }
    } else if (type == "Terjauh") {
      setIsEarlier(isEarlier ? false : true);
      if (isSortZtoA) {
        setIsSortZtoA(false);
      }
      if (isSortAtoZ) {
        setIsSortAtoZ(false);
      }
      if (isLatest) {
        setIsLatest(false);
      }
    }
  };

  const renderJudul = () => {
    return (
      <View>
        <Text style={styles.subCategory}>Judul</Text>
        <View style={styles.viewTouchAbleRow}>
          {isSortAtoZ
            ? renderCard(true, "A ke Z")
            : renderCard(false, "A ke Z")}
          {isSortZtoA
            ? renderCard(true, "Z ke A")
            : renderCard(false, "Z ke A")}
        </View>
      </View>
    );
  };

  const renderWaktu = () => {
    return (
      <View>
        <Text style={styles.subCategory}>Waktu</Text>
        <View style={styles.viewTouchAbleRow}>
          {isLatest
            ? renderCard(true, "Terdekat")
            : renderCard(false, "Terdekat")}
          {isEarlier
            ? renderCard(true, "Terjauh")
            : renderCard(false, "Terjauh")}
        </View>
      </View>
    );
  };

  const renderBulan = () => {
    return (
      <View>
        <Text style={styles.subCategory}>Bulan</Text>
        <TouchableOpacity style={styles.buttonChoose}>
          <Text style={styles.textChoose}>Pilih Bulan</Text>
          <IconArrowRight />
        </TouchableOpacity>
      </View>
    );
  };

  const renderSpesialisasi = () => {
    if (specialistID == "" && specialistName == "") {
      return (
        <View>
          <Text style={styles.subCategory}>Spesialisasi</Text>
          <TouchableOpacity
            style={styles.buttonChoose}
            onPress={() =>
              navigation.navigate("JobListSpecialization", {
                specialistID: specialistID,
                onSelect: onSelect,
              })
            }
          >
            <Text style={styles.textChoose}>Pilih spesialisasi</Text>
            <IconArrowRight />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View>
          <Text style={styles.subCategory}>Spesialisasi</Text>
          <TouchableOpacity
            style={styles.buttonChoosed}
            onPress={() =>
              navigation.navigate("JobListSpecialization", {
                specialistID: specialistID,
                onSelect: onSelect,
              })
            }
          >
            <Text style={styles.textChoosed}>{specialistName}</Text>
            <IconExpandMoreWhite width={24} height={24} />
          </TouchableOpacity>
        </View>
      );
    }
  };

  const onSelect = async (data) => {
    if (!_.isEmpty(data.item)) {
      setSpecialistID(data.item.id.toString());
      setSpecialistName(data.item.description);
    } else {
      setSpecialistID("");
      setSpecialistName("");
    }
  };

  useEffect(() => {
    console.log(
      "data specialistID JOB FILTER from LIST SPECIALIST ->>>>",
      specialistID
    );
  }, [specialistID]);

  const renderCard = (bool, text) => {
    return (
      <SortOptionButton
        text={text}
        choosed={bool}
        onPress={() => onPressSort(text)}
      />
    );
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.containerLeftHeader}>
              <View style={styles.viewArrowBackButton}>
                <ArrowBackButton onPress={() => handleBackButton()} />
              </View>
              <Title style={styles.textTitle}>Filter</Title>
            </View>
          </Body>
          <TouchableOpacity
            onPress={() => onPressReset()}
            style={styles.buttonReset}
          >
            <Text style={styles.textReset}> RESET </Text>
          </TouchableOpacity>
        </Header>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 1, backgroundColor: "transparent" }}>
        <View style={{ flex: 1 }}>
          <View style={styles.viewContainer}>
            <Content style={styles.contentContainer}>
              <View style={styles.viewContainerMain}>
                {renderJudul()}
                {renderWaktu()}
                {/* {renderBulan()} */}
                {renderSpesialisasi()}
              </View>
            </Content>
          </View>
          {renderButtonApply()}
        </View>
      </SafeAreaView>
    </Container>
  );
};

export default JobFilter;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
    marginRight: 16,
    justifyContent: "space-between",
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  textReset: {
    fontSize: 14,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    alignItems: "center",
    lineHeight: 16,
    letterSpacing: 1.25,
  },
  subCategory: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: "#000000",
    marginBottom: 12,
    marginTop: 20,
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  viewContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  contentContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  viewContainerMain: {
    marginHorizontal: 16,
    marginBottom: 60, //margin vertical button wrapper + height button - margin bottom textDesc
  },
  viewRightHeader: {
    flex: 1,
    flexDirection: "row-reverse",
  },
  touchAble: {
    paddingHorizontal: 16,
    paddingVertical: 9,
    backgroundColor: "#EBEBEB",
    borderRadius: 4,
    marginRight: 20,
  },
  containerButtonApply: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    paddingTop: 16,
    paddingHorizontal: 16,
    paddingBottom: 16,
    backgroundColor: "#FFFFFF",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: -4,
    },
    shadowOpacity: 0.02,
    shadowRadius: 4,
  },
  touchAbleEnable: {
    paddingHorizontal: 16,
    paddingVertical: 9,
    backgroundColor: "#D01E53",
    borderRadius: 4,
    marginRight: 20,
  },
  text: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
  },
  textEnable: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#FFFFFF",
  },
  viewTouchAbleRow: { flexDirection: "row", flex: 1 },
  containerMidBody: { flex: 1, flexDirection: "row", alignItems: "center" },
  buttonChoose: {
    justifyContent: "space-between",
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#EBEBEB",
    paddingHorizontal: 16,
    paddingTop: 19,
    paddingBottom: 18,
    borderRadius: 4,
  },
  textChoose: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#000000",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  buttonChoosed: {
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#D01E53",
    paddingHorizontal: 16,
    paddingTop: 19,
    paddingBottom: 18,
    borderRadius: 4,
  },
  textChoosed: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#FFFFFF",
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  containerRightHeader: {
    flex: 1,
    flexDirection: "row-reverse",
    justifyContent: "center",
  },
  containerLeftHeader: { flex: 1, flexDirection: "row", alignItems: "center" },
  buttonReset: {
    marginRight: 16,
    justifyContent: "center",
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 20 },
});
