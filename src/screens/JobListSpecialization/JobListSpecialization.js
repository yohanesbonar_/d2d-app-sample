import {
  Body,
  CardItem,
  Container,
  Content,
  Header,
  Left,
  Right,
  Title,
  View,
  Text,
  Toast,
  Radio,
  ListItem,
} from "native-base";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  FlatList,
} from "react-native";
import { ThemeD2D } from "../../../theme";
import { IconBack } from "../../assets";
import _, { initial } from "lodash";
import { Loader } from "../../../app/components";
import OptionRadioButton from "../../components/molecules/OptionRadioButton";
import { ArrowBackButton, Button } from "../../components/atoms";
import { getDataSpesializations } from "../../../app/libs/NetworkUtility";
import { HeaderToolbar } from "../../components";

const JobListSpecialization = ({ navigation }) => {
  const params = navigation.state.params;
  const [specialistID, setSpecialistID] = useState(params.specialistID);
  const [isShowLoader, setIsShowLoader] = useState(false);
  const [dataSpecialist, setDataSpecialist] = useState([]);

  let value = "";
  useEffect(() => {
    getDataAllSpecialist();
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButton);
    };
  }, []);

  const handleBackButton = () => {
    navigation.goBack();
    return true;
  };

  getDataAllSpecialist = async () => {
    let masterDataSp = [];

    setIsShowLoader(true);

    let params = {
      type: "SPECIALIST",
      page: 1,
      limit: 100,
      keyword: "",
      nesting: true,
    };

    try {
      let response = await getDataSpesializations(params);
      console.log("get all data specialist response: ", response);

      setIsShowLoader(false);

      if (response.isSuccess == true) {
        masterDataSp = response.docs;
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
      setIsShowLoader(false);
    }

    if (masterDataSp != null && masterDataSp.length > 0) {
      setDataSpecialist(masterDataSp);
    }
  };

  const onPressRadioButton = async (item) => {
    setSpecialistID(item.id.toString());
    await getDataAllSpecialist();
    setTimeout(() => {
      navigation.state.params.onSelect({ item });
      navigation.goBack();
      return true;
    }, 500);
  };

  const renderData = (item) => {
    let id = item.item.id.toString();
    let desc = item.item.description;
    return (
      <View style={{}}>
        <OptionRadioButton
          onPress={() => onPressRadioButton(item.item)}
          categoryName={desc}
          selected={specialistID == id ? true : false}
        />
      </View>
    );
  };

  const onPressReset = async () => {
    let item = {};
    setSpecialistID("");
    await getDataAllSpecialist();
    setTimeout(() => {
      navigation.state.params.onSelect({ item });
      navigation.goBack();
      return true;
    }, 500);
  };

  return (
    <Container>
      <Loader visible={isShowLoader} />
      <SafeAreaView style={styles.safeArea}>
        <Header noShadow style={styles.header}>
          <Body style={styles.bodyContainer}>
            <View style={styles.containerLeftHeader}>
              <View style={styles.viewArrowBackButton}>
                <ArrowBackButton onPress={() => handleBackButton()} />
              </View>
              <Title style={styles.textTitle}>Daftar Specialist</Title>
            </View>
          </Body>
          <TouchableOpacity
            onPress={() => onPressReset()}
            style={styles.buttonReset}
          >
            <Text style={styles.textReset}> RESET </Text>
          </TouchableOpacity>
        </Header>
      </SafeAreaView>
      <View style={styles.viewContainer}>
        <Content
          style={{
            flex: 1,
            backgroundColor: "#FFFFFF",
          }}
        >
          <View
            style={{
              marginHorizontal: 16,
            }}
          >
            <FlatList
              data={dataSpecialist}
              keyExtractor={(item, index) => index.toString()}
              renderItem={renderData}
            />
          </View>
        </Content>
      </View>
    </Container>
  );
};

export default JobListSpecialization;

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: ThemeD2D.brandPrimary,
  },
  bodyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // marginLeft: 16,
    marginVertical: 8,
    marginRight: 16,
    justifyContent: "space-between",
  },
  textTitle: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    lineHeight: 24,
    letterSpacing: 0.15,
  },
  textReset: {
    fontSize: 14,
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    alignItems: "center",
    lineHeight: 16,
    letterSpacing: 1.25,
  },
  viewContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    backgroundColor: "#FFFFFF",
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  containerRightHeader: {
    flex: 1,
    flexDirection: "row-reverse",
    justifyContent: "center",
  },
  containerLeftHeader: { flex: 1, flexDirection: "row", alignItems: "center" },
  buttonReset: {
    marginRight: 16,
    justifyContent: "center",
  },
  viewArrowBackButton: { marginLeft: 4, marginRight: 20 },
});
