import { API_CALL_CHANNEL } from "../requestHelper";
import { API } from "../Api";
import { errorHanlder } from "../Handler";
import moment from "moment";

export const getCalendarConferenceByMonth = async (params, sbc) => {
  const tz = moment().format("Z");
  try {
    const option = {
      method: "get",
      url: `${API.CONFERENCE}calendar/${tz}`,
      params: params,
    };
    let response = await API_CALL_CHANNEL(option, sbc);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const joinConference = async (params, sbc) => {
  const tz = moment().format("Z");
  const data = { tz: tz };

  try {
    const option = {
      method: "post",
      url: `${API.CONFERENCE}${params.xid}/join`,
      data: data,
    };
    let response = await API_CALL_CHANNEL(option, sbc);
    console.log("log response", response);

    return response;
  } catch (error) {
    console.log("log error", error);
    return errorHanlder(error);
  }
};

export const registerConference = async (params, sbc) => {
  const data = { source: "mobile" };

  try {
    const option = {
      method: "post",
      url: `${API.CONFERENCE}${params.xid}/register`,
      data: data,
    };
    let response = await API_CALL_CHANNEL(option, sbc);
    console.log("log response", response);

    return response;
  } catch (error) {
    console.log("log error", error);
    return errorHanlder(error);
  }
};
