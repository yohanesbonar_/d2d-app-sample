import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";

export const getBanner = async (screen_name) => {
  try {
    const option = {
      method: "get",
      url: API.BANNER + `/${screen_name}`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
