import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const subscribeChannel = async (params) => {
  const data = {
    source: "mobile",
    referral_type: params.referral_type ? params.referral_type : "",
    referral_admin_id: params.referral_admin_id ? params.referral_admin_id : "",
  };
  try {
    const option = {
      method: "post",
      url: API.ALL_CHANNEL + "/" + params.xid + "/subscription",
      data: data,
    };
    let response = await API_CALL_CHANNEL(option);
    console.log("log response", response);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
