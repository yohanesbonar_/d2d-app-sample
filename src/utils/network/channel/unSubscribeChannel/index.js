import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const unSubscribeChannel = async (params
) => {
    try {
        const option = {
            method: "delete",
            url:
                API.ALL_CHANNEL + "/" + params.xid + "/subscription",
        };
        let response = await API_CALL_CHANNEL(option);
        console.log("log response", response);

        return response;
    } catch (error) {
        return errorHanlder(error);
    }
};
