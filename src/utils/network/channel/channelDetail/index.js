import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const getChannelDetail = async (xid) => {
  try {
    const option = {
      method: "get",
      url: API.ALL_CHANNEL + "/" + xid,
    };
    let response = await API_CALL_CHANNEL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
