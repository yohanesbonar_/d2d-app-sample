import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const getJobType = async (sbc) => {
  try {
    const option = {
      method: "get",
      url: API.ALL_CHANNEL + "/master/jobtype",
    };
    let response = await API_CALL_CHANNEL(option, sbc);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
