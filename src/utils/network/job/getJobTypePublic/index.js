import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const getJobTypePublic = async () => {
  try {
    const option = {
      method: "get",
      url: API.ALL_CHANNEL + "/master/jobtype",
    };
    let response = await API_CALL_CHANNEL(option);
    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
