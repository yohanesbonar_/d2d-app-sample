import { API } from "../../Api";
import { errorHanlder } from "../../Handler";
import { API_CALL_CHANNEL } from "../../requestHelper";

export const getPublicJob = async (params) => {
    const moment = require("moment");
    const tf = moment().format("Z");
    try {
        const option = {
            method: "get",
            url: API.ALL_CHANNEL + "/public/job/"+ tf,
            params: params
        }
        let response = API_CALL_CHANNEL(option)
        return response
    } catch (error) {
        errorHanlder(error)
    }
}