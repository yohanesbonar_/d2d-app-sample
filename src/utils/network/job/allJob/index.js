import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const getAllJob = async (params, sbc) => {
  const moment = require("moment");
  const tf = moment().format("Z");

  try {
    const option = {
      method: "get",
      url: API.ALL_JOB + tf,
      params: params,
    };
    let response = await API_CALL_CHANNEL(option, sbc);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
