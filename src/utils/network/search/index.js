import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";

export const getSearch = async (
  page,
  limit,
  keyword,
  searchUpcomingWebinar,
  searchWebinar,
  searchEvent,
  searchJournal,
  searchGuideline,
  searchCme,
  searchJob,
  searchObatAtoz,
  searchLiveWebinar
) => {
  try {
    const option = {
      method: "get",
      url:
        API.SEARCH +
        `?page=${page}&limit=${limit}&keyword=${keyword}&upcomingWebinar=${searchUpcomingWebinar}&webinar=${searchWebinar}&event=${searchEvent}&journal=${searchJournal}&guideline=${searchGuideline}&cme=${searchCme}&job=${searchJob}&obatatoz=${searchObatAtoz}&liveWebinar=${searchLiveWebinar}`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
