import { API_CALL } from "../requestHelper";
import { API } from "../Api";
import { errorHanlder } from "../Handler";
import moment from "moment";

export const getAllBooth = async (params) => {
  try {
    const option = {
      method: "get",
      url: API.BOOTH,
      params: params,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getBoothDetail = async (params) => {
  try {
    const option = {
      method: "get",
      url: API.BOOTH + "/" + params.id,
    };
    let response = await API_CALL(option);
    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getBoothQuestion = async (params) => {
  try {
    const option = {
      method: "get",
      url: API.BOOTH_QUESTION,
      params: params
    };
    let response = await API_CALL(option);
    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
