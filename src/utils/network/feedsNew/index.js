import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";

export const getFeedsNew = async (params) => {
  try {
    const option = {
      method: "get",
      url: API.FEEDSNEW,
      params: params,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getFeedsNewCategoryList = async () => {
  try {
    const option = {
      method: "get",
      url: API.FEEDSNEW + `/category-list`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getFeedsDataWebinarHomepage = async (params) => {
  let limit = params.limit;
  try {
    const option = {
      method: "get",
      url: API.FEEDSNEW + `/webinar-homepage?limit=${limit}`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getFeedsDataWebinarHomepageWithPage = async (params) => {
  try {
    const option = {
      method: "get",
      url:
        API.FEEDSNEW +
        `/webinar-homepage?type=${params.type}&page=${params.page}&limit=${
          params.limit
        }&keyword=${params.keyword}`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
