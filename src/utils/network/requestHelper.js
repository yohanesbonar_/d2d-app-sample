import axios from "axios";
import { BASE_URL, BASE_URL_CHANNEL } from "../network/Api";
import { errorHanlder, responseHandler } from "./Handler";
import firebase from "react-native-firebase";
import { ThemeD2D } from "../../../theme/index";
import { getData, KEY_ASYNC_STORAGE, storeData } from "../localStorage";
import guelogin from "../../../app/libs/GueLogin";
const gueloginAuth = firebase.app("guelogin");
import _ from "lodash";

/*
  ONLY USE THIS AXIOS WRAPPER FOR AUTH REQUIRED API
  FOR STANDARDIZATION USE PROMISE INSTEAD OF ASYNC AWAIT ON THE WRAPPER

  option should be
  {
    url: 'url',
    method: 'get'/'post'/'put',
    data: {}
  }
*/

export const API_CALL = async (
  option,
  contentType = "application/x-www-form-urlencoded; charset=UTF-8"
) => {
  try {
    let idToken = null;
    const currentUser = gueloginAuth.auth().currentUser;

    try {
      idToken = await currentUser.getIdToken(true);
    } catch (error) {
      console.log("error: ", error);
    }

    if (idToken != null) {
      await storeData(KEY_ASYNC_STORAGE.AUTHORIZATION, idToken);
    } else {
      idToken = await getData(KEY_ASYNC_STORAGE.AUTHORIZATION);
    }

    let dataProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    let countryCode = null;
    if (dataProfile != null) {
      countryCode = dataProfile.country_code;
    }

    // adding the authentication token
    const API_OPTION = {
      baseURL: BASE_URL,
      headers: {
        "Content-Type": contentType,
        // 'dataType': 'json',
        // 'X-Requested-With': 'XMLHttpRequest',
        authorization: idToken,
        platform: ThemeD2D.platform,
        countrycode: countryCode,
      },
      ...option,
    };
    console.log("API OPTION API CALL -> ", API_OPTION);
    const res = await axios.request(API_OPTION);

    console.log("res API CALL -> ", res);
    return responseHandler(res);
  } catch (error) {
    console.log("error API CALL -> ", error);
    console.log("API_OPTION API CALL error response -> ", error.response);
    if (
      error.response != null &&
      error.response.status != null &&
      error.response.status >= 400 &&
      error.response.status < 500
    ) {
      if (
        error.response.data != undefined &&
        typeof error.response.data != "undefined" &&
        typeof error.response.data != "string"
      ) {
        let error_temp = error.response.data;
        return error_temp;
      } else {
        return errorHanlder(error);
      }
    } else {
      return errorHanlder(error);
    }
  }
};

export const API_CALL_JSON = async (
  option,
  contentType = "application/json; charset=UTF-8"
) => {
  try {
    let idToken = null;
    const currentUser = gueloginAuth.auth().currentUser;

    try {
      idToken = await currentUser.getIdToken(true);
    } catch (error) {
      console.log("error: ", error);
    }

    if (idToken != null) {
      await storeData(KEY_ASYNC_STORAGE.AUTHORIZATION, idToken);
    } else {
      idToken = await getData(KEY_ASYNC_STORAGE.AUTHORIZATION);
    }

    let dataProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    let countryCode = null;
    if (dataProfile != null) {
      countryCode = dataProfile.country_code;
    }

    // adding the authentication token
    const API_OPTION = {
      baseURL: BASE_URL,
      headers: {
        "Content-Type": contentType,
        // 'dataType': 'json',
        // 'X-Requested-With': 'XMLHttpRequest',
        authorization: idToken,
        platform: ThemeD2D.platform,
        countrycode: countryCode,
      },
      ...option,
    };
    console.log("API OPTION API CALL -> ", API_OPTION);
    const res = await axios.request(API_OPTION);

    console.log("res API CALL -> ", res);
    return responseHandler(res);
  } catch (error) {
    console.log("error API CALL -> ", error);
    return errorHanlder(error);
  }
};

export const API_CALL_CHANNEL = async (option, sbc) => {
  try {
    let idToken = null;
    const currentUser = gueloginAuth.auth().currentUser;

    try {
      idToken = await currentUser.getIdToken(true);
    } catch (error) {
      console.log("error: ", error);
    }

    if (idToken != null) {
      await storeData(KEY_ASYNC_STORAGE.AUTHORIZATION, idToken);
    } else {
      idToken = await getData(KEY_ASYNC_STORAGE.AUTHORIZATION);
    }

    let dataProfile = await getData(KEY_ASYNC_STORAGE.PROFILE);
    let countryCode = null;
    if (dataProfile != null) {
      countryCode = dataProfile.country_code;
    }

    let dataHeader = {
      headers: {
        // "Content-Type": contentType,
        "X-DCHANNEL-KEY": "Ha1XU73ZHCKGsh6lH4jGSL5wg80J2PE7",
        // 'dataType': 'json',
        // 'X-Requested-With': 'XMLHttpRequest',
        Authorization: "Bearer " + idToken,
      },
    };

    try {
      if (!_.isEmpty(sbc)) {
        dataHeader.headers["X-SBC"] = sbc;
      }
    } catch (error) {
      console.log("error", error);
    }
    // adding the authentication token
    const API_OPTION = {
      baseURL: BASE_URL_CHANNEL,
      ...dataHeader,
      ...option,
    };
    console.log("API_OPTION API_CALL_CHANNEL -> ", API_OPTION);
    const res = await axios.request(API_OPTION);
    return responseHandler(res);
  } catch (error) {
    console.log("error", error.response.data);
    let error_temp = error.response.data;
    return error_temp;
  }
};
