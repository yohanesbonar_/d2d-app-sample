import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";
import qs from "qs";

export const postLogin = async (email) => {
  const data = { email: email };
  try {
    const option = {
      method: "post",
      url: API.LOGIN,
      data: qs.stringify(data),
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
