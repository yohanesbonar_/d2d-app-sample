import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";
import qs from "qs";

export const submitQuiz = async (params) => {
  const data = params;
  try {
    const option = {
      method: "post",
      url: API.CME + "submit-quiz",
      data: qs.stringify(data),
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
