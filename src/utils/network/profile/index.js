import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";
import _ from "lodash";
import qs from "qs";

export const getProfile = async () => {
  try {
    const option = {
      method: "get",
      url: API.PROFILE,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const updatePhotoProfile = async (uri, fullname, medicalID) => {
  console.log("updatePhotoProfile uri: ", uri);

  const data = new FormData();

  if (!_.isEmpty(uri)) {

    data.append("filename", {
      uri: uri,
      type: "image/jpeg", // or photo.type
      //type: "multipart/form-data",
      name: ".jpg",
    });
  }

  data.append("name", fullname);
  data.append("medical_id", medicalID);

  try {
    const option = {
      method: "post",
      url: API.PROFILE_PICTURE,
      data: data,
    };

    const contentType = "multipart/form-data";
    let response = await API_CALL(option, contentType);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const updateDataProfile = async (params) => {
  const data = params
  try {
    const option = {
      method: "put",
      url: API.PROFILE,
      data: qs.stringify(data),
    };

    let response = await API_CALL(option);
    console.log("response: ", response)
    return response;
  } catch (error) {
    console.log("error: ", error)

    return errorHanlder(error);
  }
};
