import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";
import qs from "qs";

export const forgotPassword = async (params) => {
  const data = { email: params.email };
  try {
    const option = {
      method: "post",
      url: API.FORGOT_PASSWORD,
      data: qs.stringify(data),
    };
    let response = await API_CALL(option);
    console.log("response forgotPassword-> ", response);
    return response;
  } catch (error) {
    console.log("forgotPassword error-> ", error);
    return errorHanlder(error);
  }
};

export const resendForgotPassword = async (params) => {
  const data = { email: params.email };
  try {
    const option = {
      method: "post",
      url: API.RESEND_FORGOT_PASSWORD,
      data: qs.stringify(data),
    };
    let response = await API_CALL(option);
    console.log("response forgotPassword-> ", response);
    return response;
  } catch (error) {
    console.log("forgotPassword error-> ", error);
    return errorHanlder(error);
  }
};
