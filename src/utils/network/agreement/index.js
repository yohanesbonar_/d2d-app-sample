import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL, API_CALL_JSON } from "../requestHelper";

export const getAgreementWithType = async (typeAgreement) => {
  try {
    const option = {
      method: "get",
      url: API.AGREEMENT + `/active?type=${typeAgreement}`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getAgreementPopupPrivacyPolicy = async () => {
  try {
    const option = {
      method: "get",
      url: API.AGREEMENT + `/active`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const submitPopupPrivacyPolicy = async (params) => {
  try {
    const option = {
      method: "post",
      data: params,
      url: API.AGREEMENT,
    };
    let response = await API_CALL_JSON(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
