import { API } from "../Api";
import { errorHanlder } from "../Handler";
import { API_CALL } from "../requestHelper";
import _ from "lodash";

export const deleteSpecialist = async (type) => {
    try {
        const option = {
            method: "delete",
            url: `${API.SPECIALIST}/type/${type}`
        };
        let response = await API_CALL(option);

        return response;
    } catch (error) {
        return errorHanlder(error);
    }
};
