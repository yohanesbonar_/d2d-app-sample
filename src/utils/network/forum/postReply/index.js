import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const postReply = async (xid, comment, xidReplyTo, sbc) => {
  const data = { comment: comment, reply_to: xidReplyTo };
  try {
    const option = {
      method: "post",
      url: API.ALL_FORUM + xid + "/comment",
      data: data,
    };
    let response = await API_CALL_CHANNEL(option, sbc);
    console.log("log response", response);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
