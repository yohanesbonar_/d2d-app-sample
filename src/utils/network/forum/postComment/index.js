import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const postComment = async (xid, comment, sbc) => {
  const data = { comment: comment };
  try {
    const option = {
      method: "post",
      url: API.ALL_FORUM + xid + "/comment",
      data: data,
    };
    let response = await API_CALL_CHANNEL(option, sbc);
    console.log("log response", response);

    return response;
  } catch (error) {
    console.log("log error", error);
    return errorHanlder(error);
  }
};
