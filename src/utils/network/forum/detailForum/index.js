import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";
import moment from "moment";

export const getDetailForum = async (xid, sbc) => {
  const moment = require("moment");
  const tf = moment().format("Z");
  console.log("timezone -> ", tf);
  try {
    const option = {
      method: "get",
      url: API.ALL_FORUM + xid + "/" + tf,
    };
    let response = await API_CALL_CHANNEL(option, sbc);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
