import { API_CALL_CHANNEL } from "../../requestHelper";
import { API } from "../../Api";
import { errorHanlder } from "../../Handler";

export const likeForum = async (xid, paramLike, sbc) => {
  try {
    const option = {
      method: "put",
      url: API.ALL_FORUM + xid + "/" + paramLike,
    };
    let response = await API_CALL_CHANNEL(option, sbc);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};
