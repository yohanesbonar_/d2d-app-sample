import Config from "react-native-config";
export const BASE_URL = Config.BASE_URL;
export const BASE_URL_CHANNEL = Config.BASE_URL_CHANNEL;

export const API_URL = BASE_URL;
export const API = {
  SPECIALIST: `v3.1/specialist`,
  BANNER: `v3.1/banner`,
  PROFILE: `v3.1/profile/`,
  PROFILE_PICTURE: `v3.1/profile/picture/`,
  SEARCH: `v3.1/search`,
  LOGIN: `v3.1/login`,
  CME: `v3.1/cme/`,
  EXHIBITION: `v3.1/exhibition/`,
  BOOTH: `v3.1/booth/`,
  BOOTH_QUESTION: `v3.1/boothquestion/`,
  ALL_CHANNEL: `open-api/v1/channel`,
  ALL_FORUM: `open-api/v1/channel/forum/`,
  ALL_JOB: `open-api/v1/channel/job/`,
  ALL_COMMENT: `open-api/v1/channel/forum/comment/`,
  CONFERENCE: `open-api/v1/channel/conference/`,
  AGREEMENT: `v3.0/agreement`,
  FEEDSNEW: "v3.1/feeds",
  FORGOT_PASSWORD: "v3.0/forgotpassword",
  RESEND_FORGOT_PASSWORD: "v3.0/forgotpassword/resend",
};
