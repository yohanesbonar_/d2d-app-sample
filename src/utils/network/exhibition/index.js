import { API_CALL } from "../requestHelper";
import { API } from "../Api";
import { errorHanlder } from "../Handler";
import moment from "moment";

export const getAllExhibition = async (params) => {
  try {
    const option = {
      method: "get",
      url: `${API.EXHIBITION}`,
      params: params,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};

export const getExhibitionDetail = async (params) => {
  try {
    let url
    if(params.type && params.type === 'deeplink'){
      url = `${API.EXHIBITION}/hash/${params.hash}`
    }else{
      url = `${API.EXHIBITION}${params.id}`;
    }

    const option = {
      method: "get",
      url: `${API.EXHIBITION}${params.id}`,
    };
    let response = await API_CALL(option);

    return response;
  } catch (error) {
    return errorHanlder(error);
  }
};