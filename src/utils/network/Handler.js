import { HttpStatusCode } from "./HttpStatusCode";

// convert to minimal response from axios
export const responseHandler = (res) => {
  return res.data;
};

export const responseNoConnection = () => {
  return responseBase(false, "No network connected", null);
};

// conver error response from axios
export const errorHanlder = (error) => {
  let response = { code: null, message: null };

  if (!error.response) {
    // response = { code: 00, message: 'No network connected' };
    response = responseBase(false, "No network connected", null);
  } else {
    response = statusCode(error.response.status);
  }

  return response;
};

// get message status code
const statusCode = (code) => {
  let responseError = responseBase(false, "Something went wrong", null);

  HttpStatusCode.find(function(element) {
    if (element.code == code) {
      responseError = responseBase(false, element.message, null);
    }
  });

  return responseError;
};

const responseBase = (acknowledge, message, result) => {
  let response = {
    acknowledge: acknowledge,
    message: message,
    result: result,
  };
  return response;
};
