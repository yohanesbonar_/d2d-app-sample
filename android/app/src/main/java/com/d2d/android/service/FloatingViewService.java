package com.d2d.android.service;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.d2d.android.MainActivity;
import com.d2d.android.R;
import com.d2d.android.WebinarModule;
import com.d2d.android.util.Constant;
import com.d2d.android.util.Utils;
import com.facebook.react.bridge.Callback;

public class FloatingViewService extends Service implements View.OnTouchListener {

    private WindowManager windowManager;
    private View floatingView;
    private View collapsedView;
    private View expandedView;
    private WebView webView;
    private ImageView closeWebView;
    private ImageView backToAppButton;
    private ImageView arrowCollapsed;
    private String videoId = "";
    private String videoTitle = "";
    private int startat = 0;
    private String webinarLink;
    private WindowManager.LayoutParams params;
    private int initialX = 0, initialY = 0;
    private float initialTouchX = 0, initialTouchY = 0;
    private Point szWindow = new Point();
    private boolean isMinimized = false;
    private IBinder binder = new LocalBinder();
    private WebinarModule.WebinarModuleInterface webinarModuleInterface;
    private Callback stopAndBackToAppWebinarCallback;

    public FloatingViewService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        videoId = intent.getStringExtra(Constant.VIDEO_ID_EXTRA);
        videoTitle = intent.getStringExtra(Constant.VIDEO_TITLE_EXTRA);
        startat = intent.getIntExtra(Constant.VIDEO_START_AT_EXTRA, 0);
        webinarLink = intent.getStringExtra(Constant.WEBINAR_LINK);

        renderIframe();
        return binder;
    }

    public class LocalBinder extends Binder {
        public FloatingViewService getService() {
            return FloatingViewService.this;
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        videoId = intent.getStringExtra(Constant.VIDEO_ID_EXTRA);
        videoTitle = intent.getStringExtra(Constant.VIDEO_TITLE_EXTRA);
        startat = intent.getIntExtra(Constant.VIDEO_START_AT_EXTRA, 0);
        webinarLink = intent.getStringExtra(Constant.WEBINAR_LINK);

        renderIframe();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        videoId = intent.getStringExtra(Constant.VIDEO_ID_EXTRA);
        videoTitle = intent.getStringExtra(Constant.VIDEO_TITLE_EXTRA);
        startat = intent.getIntExtra(Constant.VIDEO_START_AT_EXTRA, 0);
        webinarLink = intent.getStringExtra(Constant.WEBINAR_LINK);

        renderIframe();
        return super.onStartCommand(intent, flags, startId);
    }

    private void renderIframe() {
        float radius = new Utils(this).getPx(this.getResources().getDimension(R.dimen.rounded_medium));
        float height = new Utils(this).getPx(this.getResources().getDimension(R.dimen.video_height));
        float width = new Utils(this).getPx(this.getResources().getDimension(R.dimen.video_width));
        String path = "<body style=\"margin: 0; padding: 0\">\n" +
                "    <div\n" +
                "      style=\"\n" +
                "        position: relative;\n" +
                "        border-radius: 8px;\n" +
                "        width: 303px;\n" +
                "        height: 157.5px;\n" +
                "        display: flex;\n" +
                "        justify-items: center;\n" +
                "      \"\n" +
                "    >\n" +
                "      <div\n" +
                "        style=\"\n" +
                "          position: absolute;\n" +
                "          top: 0px;\n" +
                "          left: 0px;\n" +
                "          border-radius: 0.5rem;\n" +
                "          width: 100%;\n" +
                "          height: 57px;\n" +
                "          color: transparent;\n" +
                "          z-index: 1;\n" +
                "        \"\n" +
                "      ></div>\n" +
                "      <iframe\n" +
                "        id=\"webinar-iframe\"\n" +
                "        src=\"https://www.youtube.com/embed/" + videoId + "?enablejsapi=1&autoplay=1&start=" + startat + "&modestbranding=1&rel=0&fs=0\"\n" +
                "        frameborder=\"0\"\n" +
                "        allow=\"autoplay\"\n" +
                "        style=\"background-color: black; border: none; width: 100%; height: 100%\"\n" +
                "      ></iframe>\n" +
                "    </div>\n" +
                "    <script type=\"text/javascript\">\n" +
                "      var tag = document.createElement(\"script\");\n" +
                "      tag.id = \"iframe-demo\";\n" +
                "      tag.src = \"https://www.youtube.com/iframe_api\";\n" +
                "      var firstScriptTag = document.getElementsByTagName(\"script\")[0];\n" +
                "      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\n" +
                "\n" +
                "      var player;\n" +
                "      function onYouTubeIframeAPIReady() {\n" +
                "        player = new YT.Player(\"webinar-iframe\", {\n" +
                "          playerVars: { autoplay: 1, controls: 0 }," +
                "          events: {\n" +
                "            onStateChange: onPlayerStateChange,\n" +
                "          },\n" +
                "        });\n" +
                "      }" +
                "      function onPlayerStateChange(event) {\n" +
                "        if (event.data == 0) {\n" +
                "          player.seekTo(0);\n" +
                "        }\n" +
                "      }\n" +
                "      function getTime() {\n" +
                "        var time =\n" +
                "          player.playerInfo.currentTime != null\n" +
                "            ? player.playerInfo.currentTime\n" +
                "            : 0;\n" +
                "        window.JsInterface.backToApp(time);\n" +
                "      }\n" +
                "    </script>\n" +
                "  </body>";
//        webView.loadData(path, "text/html", "UTF-8");
        webView.loadDataWithBaseURL("https://www.youtube.com", path, "text/html", "UTF-8", null);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setBackgroundColor(Color.BLACK);
        webView.setBackgroundResource(R.drawable.layout_rounded);
        webView.addJavascriptInterface(new JsInterface(this), "JsInterface");

        closeWebView.bringToFront();
        backToAppButton.bringToFront();
    }

    public class JsInterface {
        private Context context;

        public JsInterface(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void backToApp(float currentTime) {
            int currTime = (int) Math.floor(currentTime);
            if(stopAndBackToAppWebinarCallback!=null){
                stopAndBackToAppWebinarCallback.invoke("seekTo", currTime, videoId);
            }
            stopFloatingWebinar();
//            String url = webinarLink != null && !webinarLink.isEmpty() ? webinarLink + "&startat=" + currTime : "https://d2d.co.id/";
//            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
//            intentUrl.setData(Uri.parse(url));
//            intentUrl.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intentUrl);
//            stopSelf();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        floatingView = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null);

        int LAYOUT_FLAG = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_PHONE;

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );

        params.x = -500;
        params.y = 0;

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.addView(floatingView, params);
        windowManager.getDefaultDisplay().getSize(szWindow);

        collapsedView = floatingView.findViewById(R.id.rl_collapsed);
        expandedView = floatingView.findViewById(R.id.ll_expanded);
        webView = floatingView.findViewById(R.id.wv_video);
        closeWebView = floatingView.findViewById(R.id.iv_close_webview);
        backToAppButton = floatingView.findViewById(R.id.iv_back_to_app);
        arrowCollapsed = floatingView.findViewById(R.id.iv_collapsed);

        floatingView.findViewById(R.id.rl_parent).setOnTouchListener(this);
        webView.setOnTouchListener(this);

        backToAppButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
//            triggerGetTime(null);
        });

        closeWebView.setOnClickListener(view -> {
            stopFloatingWebinar();
        });
    }

    public void stopFloatingWebinar() {
        stopSelf();
        webinarModuleInterface.stopBindingService();
    }

    public void registerInterface(WebinarModule.WebinarModuleInterface webinarModuleInterface) {
        this.webinarModuleInterface = webinarModuleInterface;
    }

    public void triggerGetTime(Activity activity, Callback callback) {
        //trigger javascript function to get last secondss
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    stopAndBackToAppWebinarCallback = callback;
                    webView.evaluateJavascript("javascript:getTime();", null);
                }
            });
        } else {
            webView.evaluateJavascript("javascript:getTime();", null);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (floatingView != null) {
            windowManager.removeView(floatingView);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Long timeStart = 0L, timeEnd = 0L;

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //remember initial position
                initialX = params.x;
                initialY = params.y;

                //get touch location
                initialTouchX = motionEvent.getRawX();
                initialTouchY = motionEvent.getRawY();
                return view != webView;

            case MotionEvent.ACTION_UP:
                int xDiff = 0, yDiff = 0;
                xDiff = (int) (motionEvent.getRawX() - initialTouchX);
                yDiff = (int) (motionEvent.getRawY() - initialTouchY);

//                resetPosition(motionEvent.getRawX());

                //sometimes element move a little when clicking
                if (xDiff < 5 && yDiff < 5) {
                    if (view != webView) {
                        collapsedView.setVisibility(View.GONE);
                        expandedView.setVisibility(View.VISIBLE);
                    } else {
                        return false;
                    }
                }
                return true;


            case MotionEvent.ACTION_MOVE:
                params.x = initialX + (int) (motionEvent.getRawX() - initialTouchX);
                params.y = initialY + (int) (motionEvent.getRawY() - initialTouchY);
                windowManager.updateViewLayout(floatingView, params);
                return view != webView;

            case MotionEvent.EDGE_RIGHT:
                return true;
            case MotionEvent.EDGE_LEFT:
                return true;
        }
        return false;
    }

    private void resetPosition(float rawX) {
        if (rawX <= szWindow.x / 2) {
            arrowCollapsed.setImageResource(R.drawable.ic_baseline_keyboard_arrow_right_24);
            moveToLeft();
        } else {
            arrowCollapsed.setImageResource(R.drawable.ic_baseline_keyboard_arrow_left_24);
            moveToRight();
        }

    }


    private void moveToLeft() {
        params.x = -szWindow.x / 2;
        if (windowManager != null) {
            windowManager.updateViewLayout(floatingView, params);
        }
    }

    private void moveToRight() {
        params.x = szWindow.x / 2;
        if (windowManager != null) {
            windowManager.updateViewLayout(floatingView, params);
        }
    }
}
