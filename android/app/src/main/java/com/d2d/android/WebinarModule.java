package com.d2d.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.d2d.android.service.FloatingViewService;
import com.d2d.android.util.Constant;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

public class WebinarModule extends ReactContextBaseJavaModule {
    ReactApplicationContext reactApplicationContext;
    Activity activity;
    FloatingViewService mService;
    boolean mBound = false;

    public WebinarModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactApplicationContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return "WebinarModule";
    }

    //define callbacks for service binding, passed to bindservice()
    public ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            //we've bound to FloatingViewService, cast the Ibinder and get FloatingViewService instance
            FloatingViewService.LocalBinder binder = (FloatingViewService.LocalBinder) iBinder;
            mService = binder.getService();
            mBound = true;
            mService.registerInterface(new WebinarModuleInterface());
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    @ReactMethod(isBlockingSynchronousMethod = true)
    public void minimizeWebinar(String videoId, String videoTitle, int startAt, String webinarUrl, Callback callback) {
        activity = getCurrentActivity();
        callback.invoke("test callback from native android");
    //    Toast.makeText(activity, "triggered" + videoId + "------" + startAt, Toast.LENGTH_LONG).show();
        triggerFloating(videoId, videoTitle, startAt, webinarUrl);
    }
    @ReactMethod(isBlockingSynchronousMethod = true)
    public void stopAndBackToAppWebinar(Callback callback) {
        if(mBound){
            mService.triggerGetTime(activity, callback);
        }
    }

    public void minimizeApp () {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        reactApplicationContext.startActivity(startMain);

    }

    private void triggerFloating(String videoId, String videoTitle, int startAt, String webinarUrl) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(activity)) {
                askPermission();
            } else {
                Intent serviceIntent = new Intent(activity, FloatingViewService.class);
                serviceIntent.putExtra(Constant.VIDEO_ID_EXTRA, videoId);
                serviceIntent.putExtra(Constant.VIDEO_TITLE_EXTRA, videoTitle);
                serviceIntent.putExtra(Constant.VIDEO_START_AT_EXTRA, startAt);
                serviceIntent.putExtra(Constant.WEBINAR_LINK, webinarUrl);
                activity.bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);
                minimizeApp();

//                activity.startService(serviceIntent);
//                activity.finish();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void askPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + activity.getPackageName()));
        activity.startActivityForResult(intent, Constant.SYSTEM_ALERT_WINDOW_PERMISSION);
    }

    public class WebinarModuleInterface {
        public WebinarModuleInterface(){}
        public void stopBindingService(){
            activity.unbindService(connection);
            mBound = false;
        }
    }
}
