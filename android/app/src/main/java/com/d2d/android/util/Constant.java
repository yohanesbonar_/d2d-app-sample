package com.d2d.android.util;

public class Constant {
    public static final int SYSTEM_ALERT_WINDOW_PERMISSION = 2084;
    public static final String FLOATING_INTENT_EXTRA = "FLOATING_INTENT_EXTRA";
    public static final String VIDEO_ID_EXTRA = "VIDEO_ID_EXTRA";
    public static final String VIDEO_TITLE_EXTRA = "VIDEO_TITLE_EXTRA";
    public static final String VIDEO_START_AT_EXTRA = "VIDEO_START_AT_EXTRA";
    public static final String WEBINAR_LINK = "WEBINAR_LINK";
}
