package com.d2d.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.d2d.android.service.FloatingViewService;
import com.d2d.android.service.FloatingWidgetService;
import com.d2d.android.util.Constant;
import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

public class MainActivity extends ReactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("----------- main activity -------------------");
        // Intent serviceIntent = new Intent(
        //         MainActivity.this,
        //         FloatingWidgetService.class
        // );
        // Bundle bundle = new Bundle();

        // bundle.putString("foo", "bar");
        // serviceIntent.putExtras(bundle);
        // this.startService(serviceIntent);

        // HeadlessJsTaskService.acquireWakeLockNow(MainActivity.this);

//        new CountDownTimer(10000, 1000) {
//
//            @Override
//            public void onTick(long l) {
//                Toast.makeText(MainActivity.this, "tick tock" + l, Toast.LENGTH_SHORT).show();
//            }
//            @Override
//            public void onFinish() {
//                triggerFloating();
//            }
//        }.start();
    }

    private void triggerFloating() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                askPermission();
            } else {
                startService((new Intent(MainActivity.this, FloatingViewService.class)));
                finish();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void askPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, Constant.SYSTEM_ALERT_WINDOW_PERMISSION);
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "D2D";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
                return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }
}
