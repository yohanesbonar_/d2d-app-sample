package com.d2d.android.util;

import android.content.Context;

public class Utils {
    Context context;

    public Utils(Context context) {
        this.context = context;
    }

    public float getDensity() {
        return context.getResources().getDisplayMetrics().density;
    }

    public float getPx(float dp) {
        return dp * getDensity();
    }

    public float getDp(float px) {
        return px / getDensity();
    }
}
