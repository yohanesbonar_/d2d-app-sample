//
//  RCTPictureInPicture.h
//  D2D
//
//  Created by Yohanes Bonar H on 19/07/21.
//  Copyright © 2021 Facebook. All rights reserved.
//

//#ifndef RCTPictureInPicture_h
//#define RCTPictureInPicture_h
//
//
//#endif /* RCTPictureInPicture_h */
//
////  RCTPictureInPicture.h

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface PictureInPicture : NSObject <RCTBridgeModule>
@end

