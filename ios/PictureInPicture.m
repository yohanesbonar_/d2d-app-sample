//
//  RCTPictureInPicture.m
//  D2D
//
//  Created by Yohanes Bonar H on 19/07/21.
//  Copyright © 2021 Facebook. All rights reserved.
//

//#import <Foundation/Foundation.h>
//  RCTLogger.m

#import "PictureInPicture.h"
#import <React/RCTLog.h>
#import <AVKit/AVKit.h>

@implementation PictureInPicture

RCT_EXPORT_MODULE(Logger);

RCT_EXPORT_METHOD(log:(NSString *)message videoID:(NSString *)videoID ) {

    RCTLogInfo(@"Message in Native iOS =>>>  %@", message);
  RCTLogInfo(@"VideoID in Native iOS =>>>  %@", videoID);
  
 }


@end
