/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
// rahmat1929 - 29 Jun 19
import React, {Component} from 'react';
import { StyleProvider, Root } from 'native-base';
import { createAppContainer } from 'react-navigation';

import RootNavigator from './src/navigations';
import getTheme from './src/theme/components';
import d2dColor from './src/theme/variables/d2dColor';

const AppContainer  = createAppContainer(RootNavigator);

export default class App extends Component {
  render() {
    return (
        <StyleProvider style={getTheme(d2dColor)}>
            <Root>
                <AppContainer />
            </Root>
        </StyleProvider>
    );
  }
}
// import React, {Fragment, Component} from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
//   TouchableOpacity,
//   Dimensions
// } from 'react-native';

// // const App = () => {
// class App extends Component {

//     constructor() {
//         super();
//     }

//     render() {
//         return (
//             <View style={styles.container}>
//                 <Text>APPS</Text>
//             </View>
//         )
//     }
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
// });

// export default App;
