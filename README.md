Copyright (c) 2018-2021 PT Global Urban Esensial

Dokumen ini merupakan dokumen rahasia PT Global Urban Esensial. Dilarang memodifikasi maupun mendistribusikan dokumen ini kepada pihak manapun di luar organisasi kecuali terdapat NDA atau perjanjian khusus untuk keperluan join development. Akses kepada dokumen ini terbatas hanya untuk lingkungan internal tim tech PT Global Urban Esensial.

Barang siapa dengan sengaja memodifikasi maupun mendistribusikan dokumen ini dengan cara ilegal, maka akan diproses sesuai peraturan dan hukum yang berlaku.
