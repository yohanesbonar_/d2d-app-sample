import React, { Component } from 'react';
import { StyleSheet, View, WebView, Image, TouchableHighlight, Dimensions, SafeAreaView } from 'react-native';
import Pdf from 'react-native-pdf';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Toast } from 'native-base';
import { Loader } from './../components';
import platform from '../../theme/variables/d2dColor';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';

export default class GeneralPdfView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            numberOfPages: 0,
            showLoader: false
        }
    }

    componentDidMount() {
        lor(this);
    }

    componentWillUnMount() {
        rol();
    }

    _renderPdf(attachment) {
        if (platform.platform == 'android') {
            const source = { uri: attachment, cache: false };
            return (
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages, filePath) => {
                        // this.state.page = numberOfPages;
                        console.log(numberOfPages)
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        // this.state.page = page;
                        if (page && numberOfPages)
                            Toast.show({ text: `Page : ${page}/${numberOfPages}`, position: 'bottom', duration: 1000 })
                    }}
                    onError={(error) => {
                        if (error.message != 'cancelled')
                            Toast.show({ text: error.message, position: 'top', duration: 3000 })
                    }}
                    style={{ flex: 1, height: hp('100%') - platform.toolbarHeight, width: wp('100%') }} />
            );
        } else {
            const jsCode = "window.postMessage(document.getElementByTagName('body').innerHTML)"
            return (
                <WebView
                    javaScriptEnabled={true}
                    injectedJavaScript={jsCode}
                    onMessage={event => console.log('Received: ', event.nativeEvent.data)}
                    source={{ uri: attachment }}
                    onLoadStart={this.onloadStart}
                    onLoadEnd={this.onloadEnd}
                    style={{ flex: 1, flexDirection: 'column', width: wp('100%') - 20, backgroundColor: '#F3F3F3' }} />
            )
        }
    }

    onloadStart = () => {
        this.setState({ showLoader: true })
    }

    onloadEnd = () => {
        this.setState({ showLoader: false })
    }

    render() {
        const nav = this.props.navigation;
        const { params } = nav.state;

        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header>
                        <Left style={{ flex: 0.15 }}>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon name='md-arrow-back' style={{ fontSize: 28 }} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 0.7, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>{params.title}</Title>
                        </Body>
                        <Right style={{ flex: 0.15 }} />
                    </Header>
                </SafeAreaView>
                <View style={styles.content}>
                    {this._renderPdf(params.pdf)}
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex: 1,
        height: hp('100%') - platform.toolbarHeight,
        width: wp('100%')
    },
    pagingWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        zIndex: 1000,
        minHeight: 40,
        width: Dimensions.get('window').width,
        padding: 10,
        backgroundColor: 'rgba(36, 41, 46, 0.7)',
    },
    paging: {
        color: '#fff',
        textAlign: 'center'
    },
    iconDownload: {
        flex: 0,
        color: '#FFFFFF',
        fontSize: 22
    },
});
