import React, { Component } from 'react'
import { BackHandler, Linking, View, AppState } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Toast, Col, Row } from 'native-base';
import { WebView } from 'react-native-webview';
import { testID } from './../../libs/Common';
import { set } from 'lodash';
import { SafeAreaView } from 'react-native';
import platform from '../../../theme/variables/d2dColor'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Loader } from '../../components'
import { cancelPayment, startPayment } from '../../libs/NetworkUtility'
import { Modalize } from 'react-native-modalize';
import { AppInstalledChecker, CheckPackageInstallation } from 'react-native-check-app-install';
import firebase from 'react-native-firebase';
const gueloginAuth = firebase.app('guelogin');

export default class SelectPayment extends Component {

    constructor(props) {
        super(props);

        this.state = {
            urlSelectPayment: this.props.navigation.state.params.urlSelectPayment,
            isHome: false,
            title: "",
            isBackToEventDetail: false,
            order_id: this.props.navigation.state.params.order_id,
            injectJS: '',
            showLoader: false,
            type: this.props.navigation.state.params.type,
            amount: this.props.navigation.state.params.amount,
            eventId: this.props.navigation.state.params.id_event,
            showHeader: true,
            appState: AppState.currentState,
            isButtonUbah: false,
            isBackConfirmation: false,
            isGopay: false,
            isShowGojekNotAvailable: false,
            isReloadCheckGojekApps: false
        }
    }

    componentDidMount() {
        console.log("this.props: ", this.props)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

        //  this.checkDeepLink()
        this.handlerPushNotif()
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/background/) && nextAppState === "active") {
            console.log("App has come to the foreground!");
            //handle user minimize app, finisih payment reload/refresh webview
            //this.webview.reload()
            if (this.state.isGopay == true && this.state.isReloadCheckGojekApps == true) {
                this.checkGojekAppAvailable();
            } else {
                this.props.navigation.goBack()
            }
        }
        this.setState({ appState: nextAppState });
    };

    handlerPushNotif = () => {
        console.log('handlerPushNotif')
        //case open apps
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('notificationListener SelectPayment: ', notification)
            if (notification != null && notification.data != null) {
                if (notification.data.type == 'event' && typeof (notification.data.order_code) != "undefined" && typeof (notification.data.status)) {
                    if (this.state.order_id == notification.data.order_code && notification.data.status == "success") {
                        //handle user receive push notification after finisih payment reload/refresh webview
                        //this.webview.reload()
                        this.props.navigation.goBack()
                    }
                }
            }
        });
    }

    checkDeepLink = () => {
        Linking.getInitialURL()
            .then(url => this.handleCallbackGojek({ url }))
            .catch(err => console.error(err));

        //for app running in background 
        Linking.addEventListener('url', this.handleCallbackGojek);
    }

    handleCallbackGojek = async (url) => {
        this.webview.reload()
    }

    removeNotificationListener = () => {
        this.notificationListener()
    }

    componentWillUnmount() {
        this.backHandler.remove();
        this.removeNotificationListener()
        AppState.removeEventListener("change", this._handleAppStateChange);
        Linking.removeEventListener('url', this.handleCallbackGojek);
    }

    onBackPressed() {
        // this.props.navigation.goBack();
        if (this.state.isBackToEventDetail == true && this.state.isBackConfirmation == true) {
            this.modalizeBottomSheet.open();
        } else if (this.state.isBackToEventDetail == true) {
            this.props.navigation.goBack();
        } else if (this.state.isShowGojekNotAvailable == true) {

        } else {
            this.webview.goBack();
        }
    }

    onCheckURL(url) {

        if (url.includes('d2d.co.id/payment/finish')) {
            if (url.includes('status_code=200')) {
                this.setState({ isBackToEventDetail: true, title: "Success Payment", showHeader: false, isBackConfirmation: false, isGopay: false });
                setTimeout(() => {
                    this.props.navigation.goBack()
                }, 5000);
            } else if (url.includes('status_code=201') && url.includes('transaction_status=pending') && this.state.isGopay == true) {
                this.props.navigation.goBack();
            } else {
                this.setState({ isBackToEventDetail: true, title: "Success Payment", showHeader: false, isBackConfirmation: false, isGopay: false });
            }

        } else if (url.includes('gopay/ui/checkout')) {
            this.setState({ isBackToEventDetail: true, title: "Gopay", showHeader: true, isButtonUbah: false, isBackConfirmation: true, isGopay: true });
        } else if (url.includes('gopay/ui/payment')) {
            this.setState({ isBackToEventDetail: true, title: "Complete Payment", showHeader: true, isButtonUbah: false, isBackConfirmation: false, isGopay: true });
        } else if ((url.includes('/payment'))) {
            console.log("LOG SUCCESS")
            // setTimeout(() => {
            //     this.props.navigation.goBack()
            // }, 3000);
            this.setState({ isBackToEventDetail: true, title: "Complete Payment", showHeader: true, isButtonUbah: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('select-payment')) {
            this.setState({ isBackToEventDetail: true, title: "Select Payment", showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('number')) {
            this.setState({ isBackToEventDetail: true, title: "Complete Payment", showHeader: true, isButtonUbah: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('credit-card')) {
            this.setState({ title: "Kartu Kredit/Debit", isBackToEventDetail: true, showHeader: true, isBackConfirmation: true, isButtonUbah: false, isGopay: false });
        } else if (url.includes('credit')) {
            this.setState({ title: "Kartu Kredit/Debit", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('bca-klikpay')) {
            this.setState({ title: "BCA KlikPay", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('cimb-clicks')) {
            this.setState({ title: "Octo Clicks", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('danamon-online')) {
            this.setState({ title: "Danamon Online Banking", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('bank-transfer')) {
            this.setState({ title: "ATM/Bank Transfer", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('bca-va')) {
            this.setState({ title: "BCA Virtual Account", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('permata-va')) {
            this.setState({ title: "Permata Virtual Account", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('all-bank-va')) {
            this.setState({ title: "Jaringan ATM", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('mandiri-bill')) {
            this.setState({ title: "Mandiri", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('bni-va')) {
            this.setState({ title: "BNI Virtual Account", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('bri-va')) {
            this.setState({ title: "BRI Virtual Account", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('indomaret')) {
            this.setState({ title: "Indomaret", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('gopay')) {
            this.checkGojekAppAvailable();
            this.setState({ title: "Gopay", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: true });
        } else if (url.includes('akulaku')) {
            this.setState({ title: "Akulaku", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('alfamart/payment-code')) {
            this.setState({ title: "Complete Payment", isBackToEventDetail: true, showHeader: true, isButtonUbah: true, isBackConfirmation: false, isGopay: false });
        } else if (url.includes('alfamart') && (!url.includes('payment-code'))) {
            this.setState({ title: "Alfa Group", isBackToEventDetail: false, showHeader: true, isBackConfirmation: false, isGopay: false });
        } else if ((url.includes('redirect')) || (url.includes('index'))) {
            this.setState({ title: "Complete Payment", isBackToEventDetail: true, showHeader: true, isButtonUbah: true, isBackConfirmation: false, isGopay: false });
        } else {
            this.setState({ isBackToEventDetail: true, showHeader: true, isBackConfirmation: false, isGopay: false });
        }
    }

    checkGojekAppAvailable = () => {
        AppInstalledChecker
            .checkURLScheme('gojek') // omit the :// suffix
            .then((isInstalled) => {
                // isInstalled is true if the app is installed or false if not
                if (isInstalled) {
                    this.setState({ isShowGojekNotAvailable: false, isReloadCheckGojekApps: false });
                    this.modalizeBottomSheet.close();
                } else {
                    this.setState({ isShowGojekNotAvailable: true });
                    this.modalizeBottomSheet.open();
                }
            })
    }

    onCheckInjectJS = (url) => {
        if (url.includes('sandbox')) {
            if (url.includes('cimb/clicks/index') || url.includes('bca/klikpay/index') || url.includes('danamon/online/bdiecommerce') || url.includes('akulaku/ui')) {
                this.setState({
                    injectJS: `
                window.ReactNativeWebView.postMessage;
                document.querySelector('.navbar').style.display = 'none'
                document.querySelector('.page-header').style.margin = "0px 0px 0px"
                document.querySelector('#wrap > .container').style.padding = "0px 15px 0px 15px"
                document.querySelector('.container').style.margin = "0px 0px 0px 0px";
                true
              ` });
            } else {
                this.setState({
                    injectJS: `
                window.ReactNativeWebView.postMessage;
                document.querySelector('.header').style.display = 'none'
                document.querySelector('.test-notice').style.display = 'none'
                document.querySelector('.app-container').style.padding = "0px 0px 0px 0px";
                true
              `})
            }
        } else {
            this.setState({
                injectJS: `
                window.ReactNativeWebView.postMessage;
                document.querySelector('.header').style.display = 'none'
                document.querySelector('.app-container').style.marginTop = '-52px';
            true
          `})
        }
    }

    onPressUbah = () => {
        console.log("onPressUbah")
        this.modalizeBottomSheet.open()
    }
    onPressConfirmUbah = async () => {
        this.modalizeBottomSheet.close()

        let params = {
            order_id: this.state.order_id
        }
        this.setState({ showLoader: true });

        try {
            let response = await cancelPayment(params)
            console.log("new start payment response", response)

            if (response.isSuccess) {

                let params = {
                    id: this.state.eventId,
                    type: this.state.type,
                    amount: this.state.amount
                }

                try {
                    let response = await startPayment(params);
                    console.log(response)

                    if (response.isSuccess) {
                        this.setState({
                            showLoader: false,
                            urlSelectPayment: response.docs.redirect_url + ("#/select-payment"),
                            order_id: response.docs.order_id
                        });
                    }
                    else {
                        this.setState({ showLoader: false });
                        Toast.show({ text: response.message, position: 'top', duration: 3000 })
                    }

                } catch (error) {
                    this.setState({ showLoader: false });
                    Toast.show({ text: error, position: 'top', duration: 3000 })
                }

            }
            else {
                this.setState({ showLoader: false });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }

        } catch (error) {
            this.setState({ showLoader: false });
            Toast.show({ text: error, position: 'top', duration: 3000 })
        }

        // this.setState({ showLoader: true });

        // let params = {
        //     id: this.props.navigation.state.params.id_event,
        //     type: this.props.navigation.state.params.type,
        //     amount: this.props.navigation.state.params.amount,
        // }

        // try {
        //     let response = await startPayment(params);

        //     if (response.isSuccess) {
        //         this.setState({ showLoader: false });
        //             //url buat testing dapet dari sini
        //             //https://app.sandbox.midtrans.com/snap/v1/transactions
        //         this.setState({ urlSelectPayment: response.docs.redirect_url });
        //     }
        //     else {
        //         this.setState({ showLoader: false });
        //         Toast.show({ text: response.message, position: 'top', duration: 3000 })
        //     }

        // } catch (error) {
        //     this.setState({ showLoader: false });
        //     Toast.show({ text: error, position: 'top', duration: 3000 })
        // }
    }


    onShouldStartLoadWithRequest = (request) => {
        // HACK: allow some urls to be embedded, and reject others
        // https://github.com/react-native-community/react-native-webview/issues/381

        if (request.url.includes("pdf")) {
            let data = {
                from: "SelectPayment",
                title: "Payment Instructions",
                attachment: request.url + '.pdf',
                type: "paymentInstructions",
                isCanDownload: true,
            }
            let pdfView = { type: "Navigate", routeName: "PdfView", params: data };
            this.props.navigation.navigate(pdfView);
            return false;
        }
        else if (request.url.includes("https://d2d.co.id/?type=event")) {
            this.props.navigation.goBack()
        }
        else {
            return true
        }
    }

    _renderHeader = () => {
        if (this.state.showHeader) {
            return (
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >
                    <Header noShadow hasTabs>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                {...testID('button_back')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title style={{ color: "#FFFFFF", fontFamily: "Nunito", fontSize: 20, fontWeight: "bold" }}>{this.state.title}</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }}>
                            {this.state.title == "Complete Payment" && this.state.isButtonUbah == true && (
                                <Button
                                    {...testID('button_back')}
                                    accessibilityLabel="button_back"
                                    transparent
                                    style={{
                                        marginRight: -10,
                                        paddingRight: 0,
                                        paddingLeft: 0,
                                        // paddingBottom: 0,
                                        // paddingTop: 0,
                                    }}
                                    onPress={() => this.onPressUbah()} >
                                    <Text style={{ color: "#FFFFFF", fontFamily: "Nunito-Bold", fontSize: 16, marginTop: 4 }}>Ubah</Text>
                                </Button>

                            )}
                        </Right>
                    </Header>
                </SafeAreaView>
            )
        } else {
            return (
                <SafeAreaView style={{ backgroundColor: "#fff" }} >
                    <Header androidStatusBarColor={'#fff'} iosBarStyle={'dark-content'} style={{ height: 0, backgroundColor: "#fff" }} />
                </SafeAreaView>
            )
        }
    }

    renderBottomSheet = () => {
        return (
            <Modalize
                withOverlay={true}
                withHandle={true}
                handleStyle={{
                    backgroundColor: 'transparent'
                }}

                closeOnOverlayTap={false}
                tapGestureEnabled={false}
                panGestureEnabled={false}
                closeSnapPointStraightEnabled={false}
                onBackButtonPress={() => null}

                adjustToContentHeight={true}
                closeOnOverlayTap={false}
                panGestureComponentEnabled={false}
                ref={(ref) => {
                    this.modalizeBottomSheet = ref;
                }}
                modalStyle={{
                    borderTopLeftRadius: 16,
                    borderTopRightRadius: 16,
                    paddingHorizontal: 16,
                }}

                overlayStyle={{
                    backgroundColor: "#000000CC",
                }}

            >
                {this.state.isButtonUbah == true && (
                    this._renderContentBottomSheetUbah()
                )}
                {this.state.isBackConfirmation == true && (
                    this._renderContentBottomSheetBackConfirmation()
                )}
                {this.state.isShowGojekNotAvailable == true && (
                    this._renderGojekAppNotAvailable()
                )}
            </Modalize>
        )

    }

    _renderGojekAppNotAvailable = () => {
        return (
            <Col style={{
                // alignItems: 'center',
                marginBottom: platform.platform == "ios" && platform.deviceHeight >= 812 && platform.deviceWidth >= 375 ? 40 : 16
            }}>

                <Text bold style={{
                    marginTop: 16,
                    marginBottom: 4,
                    textAlign: 'left',
                    fontSize: 20,
                    lineHeight: 28,
                    fontFamily: "Nunito-Bold",
                    color: "#1E201F"
                }}>Aplikasi Gojek Belum Terinstall</Text>
                <Text bold style={{
                    marginBottom: 16,
                    marginTop: 4,
                    textAlign: 'left',
                    fontSize: 16,
                    lineHeight: 28,
                    fontFamily: "Nunito",
                    color: "#1E201F"
                }}>Sistem mendeteksi perangkat Anda belum memiliki aplikasi gojek. Tekan download untuk install aplikasi gojek dalam melanjutkan transaksi.</Text>

                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                    <Button
                        {...testID('button_metode_lain')}
                        block style={{ flex: 1, marginRight: 12, height: 48, backgroundColor: "#F6F6F7", borderRadius: 8 }} onPress={() => {
                            this.webview.goBack();
                            this.modalizeBottomSheet.close();
                            this.setState({ isShowGojekNotAvailable: false });
                        }}>
                        <Text style={{ color: "#1E1E20", fontSize: 16, fontFamily: "Nunito-Bold", lineHeight: 26 }}>Metode Lain</Text>
                    </Button>
                    <Button
                        {...testID('button_download')}
                        block style={{ flex: 1, marginLeft: 12, height: 48, backgroundColor: "#D01E53", borderRadius: 8 }}
                        onPress={() => {
                            this._openAppStore();
                            this.setState({ isReloadCheckGojekApps: true });
                        }
                        }
                    >
                        <Text style={{ color: "#FFFFFF", fontSize: 16, fontFamily: "Nunito-Bold", lineHeight: 26 }}>Download</Text>
                    </Button>
                </Row>

            </Col>
        )
    }

    _openAppStore = () => {
        // const APP_STORE_LINK = 'itms://itunes.apple.com/us/app/apple-store/id944875099?mt=8';
        const APP_STORE_LINK = 'https://itunes.apple.com/id/app/d2d/id1411171451?mt=8';
        // const PLAY_STORE_LINK = 'market://details?id=com.gojek.app';
        const PLAY_STORE_LINK = 'https://play.google.com/store/apps/details?id=com.gojek.app';
        if (platform.platform == 'ios') {
            Linking.openURL(APP_STORE_LINK).catch(err => console.error('An error occurred', err));
        }
        else {
            Linking.openURL(PLAY_STORE_LINK).catch(err => console.error('An error occurred', err));
        }
    }

    _renderContentBottomSheetUbah = () => {
        return (
            <Col style={{
                // alignItems: 'center',
                marginBottom: platform.platform == "ios" && platform.deviceHeight >= 812 && platform.deviceWidth >= 375 ? 40 : 16
            }}>

                <Text bold style={{
                    marginVertical: 16,
                    textAlign: 'left',
                    fontSize: 20,
                    lineHeight: 28,
                    fontFamily: "Nunito-Bold",
                    color: "#1E201F"
                }}>Apakah Anda Akan Mengubah Metode Pembayaran?</Text>

                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>

                    <Button
                        {...testID('button_cancel_ubah')}
                        block style={{ flex: 1, marginRight: 12, height: 48, backgroundColor: "#F6F6F7", borderRadius: 8 }} onPress={() => this.modalizeBottomSheet.close()}>
                        <Text style={{ color: "#1E1E20", fontSize: 16, fontFamily: "Nunito-Bold", lineHeight: 26 }}>Tidak</Text>
                    </Button>
                    <Button
                        {...testID('button_ya_ubah')}
                        block style={{ flex: 1, marginLeft: 12, height: 48, backgroundColor: "#D01E53", borderRadius: 8 }}
                        onPress={() =>
                            this.onPressConfirmUbah()
                        }
                    >
                        <Text style={{ color: "#FFFFFF", fontSize: 16, fontFamily: "Nunito-Bold", lineHeight: 26 }}>Ya</Text>
                    </Button>
                </Row>

            </Col>
        )
    }

    _renderContentBottomSheetBackConfirmation = () => {
        return (
            <Col style={{
                // alignItems: 'center',
                marginBottom: platform.platform == "ios" && platform.deviceHeight >= 812 && platform.deviceWidth >= 375 ? 40 : 16
            }}>

                <Text style={{
                    marginTop: 16,
                    marginBottom: 8,
                    textAlign: 'left',
                    fontSize: 20,
                    lineHeight: 28,
                    fontFamily: "Nunito-Bold",
                    color: "#1E201F"
                }}>Ingin keluar halaman?</Text>

                <Text style={{
                    marginBottom: 16,
                    textAlign: 'left',
                    fontSize: 16,
                    lineHeight: 28,
                    fontFamily: "Nunito",
                    color: "#1E201F"
                }}>Pembayaran saat ini otomatis akan dibatalkan.</Text>

                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>

                    <Button
                        {...testID('button_cancel_confirmation')}
                        block style={{ flex: 1, marginRight: 8, height: 48, backgroundColor: "#F6F6F7", borderRadius: 8 }} onPress={() => this.props.navigation.goBack()}>
                        <Text style={{ color: "#1E1E20", fontSize: 16, fontFamily: "Nunito-Bold", lineHeight: 26 }}>Keluar Halaman</Text>
                    </Button>
                    <Button
                        {...testID('button_ya_confirmation')}
                        block style={{ flex: 1, marginLeft: 8, height: 48, backgroundColor: "#D01E53", borderRadius: 8 }}
                        onPress={() =>
                            this.modalizeBottomSheet.close()
                        }
                    >
                        <Text style={{ color: "#FFFFFF", fontSize: 16, fontFamily: "Nunito-Bold", lineHeight: 26 }}>Lanjut Bayar</Text>
                    </Button>
                </Row>

            </Col>
        )
    }

    render() {
        console.log("LOG URL in Render()", this.state.urlSelectPayment);
        return (
            <Container style={{ backgroundColor: '#F3F3F3' }}>
                {this._renderHeader()}
                {this.renderBottomSheet()}

                <SafeAreaView style={{ flex: 1 }}>
                    <WebView
                        onMessage={event => console.log('Received: ', event.nativeEvent.data)}
                        javaScriptEnabled={true}
                        injectedJavaScript={this.state.injectJS}
                        userAgent={"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3714.0 Mobile Safari/537.36"}
                        startInLoadingState={true}
                        source={{ uri: this.state.urlSelectPayment }}
                        style={{ flex: 1 }}
                        ref={ref => (this.webview = ref)}
                        onLoadEnd={syntheticEvent => {
                            const { nativeEvent } = syntheticEvent;
                            const _url = nativeEvent.url;
                            this.onCheckURL(_url);
                            this.onCheckInjectJS(_url);
                            console.log("LOG ISI URL", _url)
                        }}
                        onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
                        onNavigationStateChange={navState => {
                            const _url = navState.url;
                            this.onCheckURL(_url);
                            this.onCheckInjectJS(_url);
                        }} />
                </SafeAreaView>

                <Loader visible={this.state.showLoader} />

            </Container>
        )
    }

}
