import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Button,
  Icon,
  Text,
  Item,
  Input,
} from "native-base";
import _ from "lodash";
import platform from "../../../theme/variables/d2dColor";
import { Loader } from "./../../components";
import { getDataAtoZ } from "./../../libs/ObatAtoZ";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { testID } from "../../libs/Common";
export default class SearchAtoZ extends Component {
  page = 1;
  timer = null;
  state = {
    searchValue: "",
    // tabActive: 0,
    // menuTab: ['SEMUA', 'GENERIK', 'BRAND'],
    obat: [],
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    isEmptyData: false,
    isFirstTime: true,
    //data: []
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    this.getObatAtoZ(this.page);
    this.setState({
      isFirstTime: false,
    });
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  doSearch = (inputText) => {
    clearTimeout(this.timer);
    this.setState({ obat: [], searchValue: inputText, isEmptyData: false });
    if (inputText.length >= 3) {
      this.page = 1;
      this.timer = setTimeout(() => {
        this.getObatAtoZ(this.page);
      }, 1000);
    }
  };

  async getObatAtoZ(page) {
    if (this.state.searchValue == "") {
      this.setState({ isFetching: false, isSuccess: true });
      return;
    }

    this.setState({ isFailed: false, isFetching: true });

    let response = await getDataAtoZ("", this.state.searchValue, page, "");

    try {
      if (response.isSuccess) {
        this.setState({
          //data: [...this.state.data, response],
          obat: [...this.state.obat, ...response.data.data],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isEmptyData:
            response.data.data != null && response.data.data.length > 0
              ? false
              : true,
        });
      } else {
        this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMore = () => {
    if (this.state.isEmptyData == true) {
      return;
    }
    this.page = this.state.isFailed == true ? this.page : this.page + 1;
    this.getObatAtoZ(this.page);
  };

  renderItemObat = ({ item }) => {
    return (
      <TouchableOpacity
        {...testID("list_item_obat")}
        activeOpacity={0.8}
        style={styles.typeObatContainer}
        key={item.obat_id}
        onPress={() => {
          this.props.navigation.navigate("DetailAtoZ", {
            dataObat: item,
          });
        }}
      >
        <Text style={styles.itemObat}>{item.title}</Text>
        <Text style={styles.itemTypeObat}>{item.type}</Text>
      </TouchableOpacity>
    );
  };

  _renderItemFooter = () => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader()}
    </View>
  );

  _renderItemFooterLoader() {
    if (this.state.isFailed == true) {
      return (
        <TouchableOpacity onPress={() => this.handleLoadMore()}>
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = () => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
  );

  _renderEmptyItemLoader() {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.handleLoadMore()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (
      this.state.isFirstTime == true ||
      (this.state.isFetching == false && this.state.isSuccess == true)
    ) {
      let messageNotFound = "We are really sad we could not find anything";
      if (this.state.searchValue != "") {
        messageNotFound += " for " + this.state.searchValue;
      }
      return (
        <View style={styles.viewNotFound}>
          <Icon name="ios-search" style={styles.iconNotFound} />
          <Text style={styles.textNotFound}>{messageNotFound}</Text>
        </View>
      );
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  renderList() {
    return (
      <FlatList
        contentContainerStyle={{ paddingTop: 5, paddingBottom: 5 }}
        scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
        data={this.state.obat}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.01}
        renderItem={this.renderItemObat}
        ListEmptyComponent={this._renderEmptyItem}
        ListFooterComponent={this._renderItemFooter}
        keyExtractor={(item, index) => index.toString()}
      />

      // <SectionList
      //     renderItem={({ item, index, section }) => this.renderItemObat(item)}
      //     // renderSectionHeader={({ section: { title } }) => this.renderSectionHeader(title)}
      //     sections={this.state.data}
      //     keyExtractor={(item, index) => item + index}
      // />
      // null
    );
  }

  render() {
    const nav = this.props.navigation;
    const inlineStyleIconClose = { marginTop: 7 };

    return (
      <Container>
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header noShadow searchBar>
            <Item nopadding>
              <Icon name="ios-search" style={{ color: "#D7DEE6" }} />
              <Input
                {...testID("input_search")}
                accessibilityLabel="input_search"
                placeholder="Cari obat atau penyakit terkait"
                onChangeText={(text) => this.doSearch(text)}
                onTouchStart={() =>
                  AdjustTracker(AdjustTrackerConfig.Obat_Search)
                }
                value={this.state.searchValue}
                style={styles.searchField}
              />
              <Icon
                {...testID("button_clear_input")}
                style={{ color: "#D7DEE6" }}
                name="ios-close-circle"
                onPress={() => this.doSearch("")}
              />
            </Item>
            <TouchableOpacity
              {...testID("button_close")}
              accessibilityLabel="button_close"
              transparent
              style={{ marginRight: 5, marginLeft: 10, alignSelf: "center" }}
              onPress={() => this.onBackPressed()}
            >
              <Icon
                name="md-close"
                type="Ionicons"
                style={[styles.iconClose]}
              />
            </TouchableOpacity>
          </Header>
        </SafeAreaView>
        {/* <View style={styles.containerTabs}>
                    {this.renderButtonTab()}

                </View> */}
        {this.renderList()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  searchField: {
    fontSize: 14,
    lineHeight: 15,
  },
  viewNotFound: {
    marginVertical: 20,
    marginHorizontal: 10,
  },
  iconNotFound: {
    fontSize: 80,
    textAlign: "center",
  },
  textNotFound: {
    textAlign: "center",
    fontSize: 20,
  },

  itemObat: {
    fontFamily: "Nunito-Regular",
    fontSize: 16,
    color: "#454F63",
    marginLeft: 12,
  },
  itemTypeObat: {
    fontFamily: "Nunito-Regular",
    fontSize: 16,
    color: "#959DAD",
    marginRight: 12,
  },
  typeObatContainer: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#fff",
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: "#EAF4FF",
    justifyContent: "space-between",
  },

  buttonTab: {
    width: 100 / 3 + "%",
    borderRadius: 0,
    paddingHorizontal: 10,
    backgroundColor: "#fff",
    borderBottomWidth: 3,
    borderBottomColor: "transparent",
  },
  textButtonTab: {
    fontFamily: "Nunito-Bold",
    fontSize: 13,
    color: "#959DAD",
  },
  containerTabs: {
    flexDirection: "row",
    width: "100%",
  },
  iconClose: {
    fontSize: 28,
    color: "#fff",
  },
  // emptyItem: {
  //     backgroundColor: '#fff',
  //     borderBottomLeftRadius: 7,
  //     borderBottomRightRadius: 7,
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     height: (platform.deviceHeight / 2) - 80
  // },
  viewNotFound: {
    marginVertical: 20,
    marginHorizontal: 10,
  },
  iconNotFound: {
    fontSize: 80,
    textAlign: "center",
  },
  textNotFound: {
    textAlign: "center",
    fontSize: 20,
  },
});
