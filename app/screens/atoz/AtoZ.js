import React, { Component, Fragment } from "react";
import {
  Linking,
  StatusBar,
  StyleSheet,
  Animated,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Image,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Icon,
  Text,
  Body,
  ListItem,
  Toast,
  Right,
  Button,
} from "native-base";
import platform from "../../../theme/variables/d2dColor";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import _ from "lodash";
import { getDataAtoZ, getDataBannerObatAtoZ } from "./../../libs/ObatAtoZ";
import { Loader } from "./../../components";
import { AdjustTrackerConfig, AdjustTracker } from "../../libs/AdjustTracker";
import Swiper from "react-native-swiper";
import { testID } from "../../libs/Common";
import { ArrowBackButton } from "../../../src/components/atoms";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;
export default class AtoZ extends Component {
  page = 1;
  _refSlider = null;

  constructor(props) {
    super(props);
    this.state = {
      screen: Dimensions.get("window"),
      animatedValue: new Animated.Value(0),
      obat: [],
      filterAlphabet: [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
      ],
      selectedAlphabet: "-",
      showLoader: false,
      imageBannerAds: [],
      showBannerAds: false,
      onRefresh: false,
      isEmptyData: false,
      isFetching: false,
      isSuccess: false,
      isFailed: false,
      isFirstTime: true,
      width: Dimensions.get("window").width,
      currentInterval: 200,
      backButtonAvailable: false,
    };
  }

  onLayout = (e) => {
    this.setState({
      width: e.nativeEvent.layout.width,
      screen: Dimensions.get("window"),
    });
  };

  getOrientation = () => {
    if (this.state.screen.width > this.state.screen.height) {
      return "LANDSCAPE";
    } else {
      return "PORTRAIT";
    }
  };

  componentDidMount() {
    StatusBar.setHidden(false);
    this.getData();
    this.setState({ showLoader: true });
    lor(this);
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.setState({ backButtonAvailable: true });
      } else {
        this.setState({ backButtonAvailable: false });
      }
    });
  };

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }
  async getData() {
    this.page = 1;
    this.setState({
      obat: [],
    });

    this.onPressAlphabet("A");
    this.getBannerObatAtoZ();
    this.getDataCountryCode();
  }

  async getBannerObatAtoZ() {
    try {
      let response = await getDataBannerObatAtoZ();
      if (response.isSuccess) {
        this.setState({
          imageBannerAds: [...this.state.imageBannerAds, ...response.data],
        });

        if (response.data.length > 0) {
          this.setState({ showBannerAds: true });
        } else {
          this.setState({ showBannerAds: false });
        }
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  }

  async getObatAtoZ(prefix, keyword, page, limit) {
    this.setState({
      isFailed: false,
      isFetching: true,
    });

    try {
      let response = await getDataAtoZ(prefix, keyword, page, limit);
      console.log("getObatAtoZ response: ", response);
      if (response.isSuccess) {
        this.setState({
          obat: [...this.state.obat, ...response.data.data],
          showLoader: false,
          isEmptyData:
            response.data.data != null && response.data.data.length > 0
              ? false
              : true,
          isFetching: false,
          isSuccess: true,
          isFailed: false,
        });
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
        this.setState({
          showLoader: false,
          isFetching: false,
          isSuccess: false,
          isFailed: true,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
      this.setState({
        showLoader: false,
        isFetching: false,
        isSuccess: false,
        isFailed: true,
      });
    }
  }

  onPressAlphabet = (prefix) => {
    AdjustTracker(AdjustTrackerConfig.ObatAZ_Alfabet);

    if (
      this.state.selectedAlphabet != prefix &&
      this.state.isFetching == false
    ) {
      this.page = 1;
      this.setState(
        {
          selectedAlphabet: prefix,
          obat: [],
          showLoader: false,
          isFetching: true,
        },
        async () => {
          await this.getObatAtoZ(prefix, "", this.page, "");
        }
      );
    }
  };

  renderFilterAlphabet() {
    const alphabet = this.state.filterAlphabet;
    const inlineStyle = {
      Button: { backgroundColor: "#CE1E52" },
      Text: { color: "#fff" },
    };

    return alphabet.map((prefix, i) => {
      return (
        <TouchableOpacity
          {...testID("button_alphabet")}
          key={i}
          onPress={() => this.onPressAlphabet(prefix)}
          style={[
            styles.buttonAlphabet,
            this.state.selectedAlphabet == prefix ? inlineStyle.Button : {},
          ]}
        >
          <Text
            style={[
              styles.textButtonAlphabet,
              this.state.selectedAlphabet == prefix ? inlineStyle.Text : {},
            ]}
          >
            {prefix}
          </Text>
        </TouchableOpacity>
      );
    });
  }

  renderHeaderItem = () => {
    return (
      <ListItem
        itemDivider
        style={{
          borderTopLeftRadius: 7,
          borderTopRightRadius: 7,
          backgroundColor: "#E6E6E6",
        }}
      >
        <Text style={{ fontFamily: "Nunito-Bold", fontSize: 16 }}>
          {this.state.selectedAlphabet}
        </Text>
      </ListItem>
    );
  };

  renderEmptyItem = () => {
    if (this.state.obat.length == 0) {
      return (
        <View style={styles.emptyItem}>
          <Text>
            {this.state.isFetching == true
              ? "Loading..."
              : "Obat Tidak Ditemukan"}
          </Text>
        </View>
      );
    }
  };

  renderDataBannerAds = (data) => {
    const bannerAds = data;

    return bannerAds.map((value, i) => {
      return (
        <TouchableOpacity
          opacityContent={0.9}
          style={{ height: 100 }}
          {...testID("image_banner")}
          onPress={() => {
            AdjustTracker(AdjustTrackerConfig.ObatAZ_Banner);

            if (!_.isEmpty(value.link)) {
              Linking.openURL(value.link);
            }
          }}
        >
          <Image
            style={{ height: "100%", width: "100%", borderRadius: 7 }}
            source={{ uri: value.url }}
          />
        </TouchableOpacity>
      );
    });
  };

  renderDot = (active = false) => {
    return (
      <View
        style={{
          backgroundColor: active == true ? "rgba(0,0,0,.2)" : "#007aff",
          width: 8,
          height: 8,
          borderRadius: 4,
          marginLeft: 3,
          marginRight: 3,
          marginTop: 13,
          marginBottom: -10,
        }}
      />
    );
  };

  renderImageBannerAds = () => {
    const { imageBannerAds } = this.state;

    return (
      <View style={styles.containerImageBannerAds} onLayout={this.onLayout}>
        <Swiper
          showsPagination={true}
          index={imageBannerAds.length - 1}
          loop={true}
          autoplay={true}
          dot={this.renderDot(false)}
          activeDot={this.renderDot(true)}
          onIndexChanged={(index) => console.warn(index)}
        >
          {this.renderDataBannerAds(imageBannerAds)}
        </Swiper>
      </View>
    );
  };

  renderItemObat = ({ item }) => {
    return (
      <TouchableOpacity
        {...testID("list_item_obat")}
        activeOpacity={0.8}
        style={styles.typeObatContainer}
        key={item.obat_id}
        onPress={() => {
          AdjustTracker(AdjustTrackerConfig.ObatAZ_Alfabet_Obat);

          this.props.navigation.navigate("DetailAtoZ", {
            dataObat: item,
          });
        }}
      >
        <Text style={styles.itemObat}>{item.title}</Text>
        <Text style={styles.itemTypeObat}>{item.type}</Text>
      </TouchableOpacity>
    );
  };

  handleLoadMore = () => {
    if (this.state.isEmptyData == true) {
      return;
    }

    this.page = this.state.isFailed == true ? this.page : this.page + 1;

    this.getObatAtoZ(this.state.selectedAlphabet, "", this.page, "");
  };

  _renderItemFooter = () => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader()}
    </View>
  );

  _renderItemFooterLoader() {
    if (this.state.isFailed == true) {
      return (
        <TouchableOpacity onPress={() => this.handleLoadMore()}>
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  render() {
    const nav = this.props.navigation;
    const inlineStyleIconCloseScreen = { marginTop: 7 };

    let heightHeaderSpan = this.state.showBannerAds == true ? 400 : 300;
    if (this.getOrientation() == "LANDSCAPE") {
      heightHeaderSpan = this.state.showBannerAds == true ? 250 : 150;
    }

    let translateY = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
      extrapolate: "clamp",
    });
    let opacityContent = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [1, 0],
    });

    let heightTopContainer =
      this.state.showBannerAds == true ? wp("215%") : wp("200%");

    return (
      <Fragment>
        <SafeAreaView
          style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }}
        />

        <Container>
          <View
            style={{
              width: wp("200%"),
              height: heightTopContainer,
              borderRadius: wp("100%"),
              backgroundColor: platform.toolbarDefaultBg,
              position: "absolute",
              left: -wp("50%"),
              top: -wp("90%"),
            }}
          />

          <View>
            <AnimatedFlatList
              contentContainerStyle={{
                paddingTop: heightHeaderSpan + 5,
                paddingHorizontal: 10,
              }}
              // scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
              onScroll={Animated.event(
                [
                  {
                    nativeEvent: {
                      contentOffset: { y: this.state.animatedValue },
                    },
                  },
                ],
                { useNativeDriver: true } // <-- Add this
              )}
              data={this.state.obat}
              ListFooterComponent={this._renderItemFooter}
              onEndReachedThreshold={0.01}
              onEndReached={this.handleLoadMore}
              renderItem={this.renderItemObat}
              ListEmptyComponent={this.renderEmptyItem}
              ListHeaderComponent={this.renderHeaderItem}
              keyExtractor={(item, index) => index.toString()}
            />

            <Animated.View
              style={[
                styles.animHeaderSpan,
                { transform: [{ translateY }], height: heightHeaderSpan },
              ]}
            >
              <Animated.View
                style={[styles.wrapperHeader, { opacity: opacityContent }]}
              >
                {this.getOrientation() == "PORTRAIT" && (
                  <Text style={{ color: "#fff" }}>
                    Dapatkan informasi obat dengan mendapatkan nama generik atau
                    penyakit terkait
                  </Text>
                )}

                {this.getOrientation() == "PORTRAIT" && (
                  <TouchableOpacity
                    {...testID("button_input_cari_obat")}
                    onPress={() => {
                      AdjustTracker(AdjustTrackerConfig.ObatAZ_Search);
                      this.props.navigation.navigate("SearchAtoZ");
                    }}
                    activeOpacity={0.8}
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      borderRadius: 10,
                      backgroundColor: "#fff",
                      paddingHorizontal: 7,
                      paddingVertical: 10,
                      marginVertical: 15,
                    }}
                  >
                    <Icon name="ios-search" style={styles.iconSearch} />
                    <Text
                      style={{
                        flex: 0.9,
                        color: "#6C6C6C",
                        fontFamily: "Nunito-Regular",
                        fontSize: 14,
                      }}
                    >
                      Cari obat atau penyakit terkait
                    </Text>
                    <Icon name="ios-close-circle" style={styles.iconClose} />
                  </TouchableOpacity>
                )}

                {this.state.showBannerAds == true
                  ? this.renderImageBannerAds()
                  : null}

                <View>
                  {this.getOrientation() == "PORTRAIT" && (
                    <Text style={{ color: "#fff", marginTop: 10 }}>
                      Alfabet
                    </Text>
                  )}
                  <ScrollView
                    horizontal={true}
                    style={styles.containerScrollAlphabet}
                  >
                    {this.renderFilterAlphabet()}
                  </ScrollView>
                </View>
              </Animated.View>
            </Animated.View>
          </View>

          <Animated.View style={styles.animHeader}>
            <Header noShadow>
              <Body
                style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
              >
                {this.state.backButtonAvailable == true && (
                  <View style={{ marginLeft: -6 }}>
                    <ArrowBackButton onPress={() => this.onBackPressed()} />
                  </View>
                )}
                <Title style={styles.textTitle(this.state.backButtonAvailable)}>
                  Obat A to Z
                </Title>
              </Body>
              {this.getOrientation() != "PORTRAIT" && (
                <Right style={{ flex: 0.5 }}>
                  <Button
                    {...testID("button_search_atoz")}
                    onPress={() => this.props.navigation.navigate("SearchAtoZ")}
                    transparent
                  >
                    <Icon
                      type="EvilIcons"
                      name="search"
                      style={{ fontSize: 28 }}
                    />
                  </Button>
                </Right>
              )}
            </Header>
          </Animated.View>

          <Loader visible={this.state.showLoader} />
        </Container>
      </Fragment>
    );
  }
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
//const heightHeaderSpan = 300;
//const imageSliderHeight = 100;
const styles = StyleSheet.create({
  animHeader: {
    position: "absolute",
    width: "100%",
    zIndex: 2,
  },
  animHeaderSpan: {
    position: "absolute",
    paddingTop: platform.toolbarHeight,
    backgroundColor: "transparent",
    // height: heightHeaderSpan + imageSliderHeight,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: "flex-end",
    alignItems: "flex-start",
  },

  wrapperHeader: {
    position: "absolute",
    left: 0,
    right: 0,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },

  emptyItem: {
    backgroundColor: "#fff",
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    justifyContent: "center",
    alignItems: "center",
    height: platform.deviceHeight / 2 - 80,
  },

  itemObat: {
    fontFamily: "Nunito-Regular",
    fontSize: 16,
    color: "#454F63",
  },
  itemTypeObat: {
    fontFamily: "Nunito-Regular",
    fontSize: 12,
    color: "#959DAD",
  },

  buttonAlphabet: {
    marginRight: 10,
    borderRadius: 25,
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  },

  textButtonAlphabet: {
    color: "#fff",
    fontFamily: "Nunito-Bold",
    fontSize: 20,
    lineHeight: 40,
  },

  typeObatContainer: {
    backgroundColor: "#fff",
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: "#EAF4FF",
  },
  iconSearch: {
    fontSize: 20,
    marginLeft: -7,
    marginRight: 10,
    color: "#D7DEE6",
  },

  iconClose: {
    fontSize: 20,
    marginLeft: 10,
    marginRight: -7,
    color: "#D7DEE6",
  },

  containerScrollAlphabet: {
    marginTop: 10,
    backgroundColor: "rgba(255, 255, 255, 0.1)",
    padding: 10,
    marginHorizontal: -10,
    borderRadius: 0,
  },

  containerImageBannerAds: {
    height: 100,
    width: "100%",
    backgroundColor: "rgba(255, 255, 255, 0.5)",
    borderRadius: 7,
  },
  iconCloseScreen: {
    marginTop: 15,
    fontSize: 28,
    color: "#fff",
  },
  paginationBoxStyle: {
    position: "absolute",
    bottom: 0,
    padding: 0,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    paddingVertical: 10,
  },
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    padding: 0,
    margin: 0,
    backgroundColor: "rgba(128, 128, 128, 0.92)",
  },
  textTitle: (backButtonAvailable) => ({
    fontSize: 30,
    marginLeft: backButtonAvailable == true ? 20 : null,
  }),
});
