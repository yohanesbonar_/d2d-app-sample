import React, { Component } from 'react';
import ViewMoreText from 'react-native-view-more-text';
import {
    ListView,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity, TouchableWithoutFeedback, TouchableHighlight,
    View,
    TextInput,
    FlatList,
    Dimensions,
    ScrollView,
    Linking, SafeAreaView,
    BackHandler
} from 'react-native';
import {
    Icon, Button, Toast, CardItem, Container, Header, Content, Left, Right, Body, Title,
    Card, Col
} from 'native-base';
import platform from '../../../theme/variables/d2dColor';
import { getDetailDataAtoZ } from './../../libs/ObatAtoZ';
import { Loader, ContentDetailAtoZ, Tag } from './../../components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { AdjustTrackerConfig, AdjustTracker } from '../../libs/AdjustTracker';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

import { escapeHtml } from '../../libs/Common';
import { testID } from '../../libs/Common'

let from = {
    BACK_BUTTON: "BACK_BUTTON",
};
class DetailAtoZ extends Component {
    i = 1;
    constructor(props) {
        super(props);

        this.state = {
            showLoader: false,
            activeTab: 1,
            yIndex: 0,
            tabActive: 0,
            obat: {},
            componentObat: [],
            tag: [],
            dummyRelated: ['Endokrin', 'Obgyn', 'Diabetes Militus'],
            relatedObat: [],
            loadWebView: true
        }
    }

    stateLoadWebView = () => {
        this.setState({
            loadWebView: false
        });
    }

    componentDidMount() {

        let idObat = ''
        let titleObat = ''

        let dataProps = this.props.navigation.state.params;
        console.log("dataProps: ", dataProps)
        if (dataProps.isFromPushNotification == true) {
            idObat = dataProps.obat_id
            titleObat = dataProps.title_obat
        }
        else {
            if (dataProps.dataObat.obat_id != undefined) {
                idObat = dataProps.dataObat.obat_id;
                titleObat = dataProps.dataObat.title
            } else if (dataProps.dataObat.idObat != undefined) {
                //when open after click tag obat related
                idObat = dataProps.dataObat.idObat
                titleObat = dataProps.dataObat.value
            }
        }
        this.getDetailObatAtoZ(idObat)

        AdjustTracker(AdjustTrackerConfig.Obat_Infodetail, titleObat, idObat);

        lor(this)

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        rol();
        this.backHandler.remove();
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }


    sendAdjust = (action) => {

        let token = "";

        switch (action) {
            case from.BACK_BUTTON:
                token = AdjustTrackerConfig.ObatAZ_Detail_BackButton;
                break;
            default:
                break;
        }
        if (token != "") {
            AdjustTracker(token);
        }
    };

    async getDetailObatAtoZ(obat_id) {
        this.setState({
            showLoader: true
        })

        let response = await getDetailDataAtoZ(obat_id);
        console.log('getDetailObatAtoZ response: ', response)
        try {
            if (response.isSuccess) {

                let data = response.data.component;
                data.push({
                    'title': 'Nama Paten',
                    'description': response.data.obat.patent_name
                })
                this.setState({
                    obat: response.data.obat,
                    componentObat: [...this.state.componentObat, ...data],
                    relatedObat: [...this.state.relatedObat, ...response.data.related_obat.data],
                    showLoader: false
                })
            } else {
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
                this.setState({
                    showLoader: false
                })
            }
        } catch (error) {
            Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
            this.setState({
                showLoader: false
            })
        }
    }

    onButtonTabPress = (tabType) => {
        this.setState({
            tabActive: tabType
        })
    }

    removeAllHtmlTag = (textHtml) => {
        let text = textHtml.replace(/<(.|\n)*?>/g, '');

        return text;
    }

    renderButtonTab = () => {
        const tab = this.state.componentObat;
        const styleButtonTabActive = { borderBottomColor: '#EDCE4A' };
        const styleTextTabActive = { color: '#454F63' }
        return tab.map((value, i) => {
            return (
                <Button
                    key={i}
                    style={[styles.buttonTab
                        , this.state.tabActive == i ? styleButtonTabActive : styles.buttonTab]}
                    light full
                    onPress={() => { this.onButtonTabPress(i) }}>
                    <Text
                        style={[styles.textButtonTab, this.state.tabActive == i ? styleTextTabActive : styles.textButtonTab]}
                    >{value.title}</Text>
                </Button>
            )
        })
    }

    render() {
        const { navigation } = this.props;
        const { goBack } = navigation;
        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                <View style={{ width: wp('200%'), height: wp('200%'), borderRadius: wp('100%'), backgroundColor: platform.toolbarDefaultBg, position: 'absolute', left: -wp('50%'), top: -wp('100%') }}></View>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >
                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                {...testID('button_back')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed() + this.sendAdjust(from.BACK_BUTTON)}>
                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>{this.state.obat.title}</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }}>

                        </Right>
                    </Header>
                </SafeAreaView>
                <Content>

                    {/* <ScrollView horizontal={true} style={styles.containerTabs}>
                        {this.renderButtonTab()}
                    </ScrollView> */
                    }

                    <View style={{ padding: 10 }}>
                        {this.state.componentObat.length > 0 ? this.renderContent() : null}
                        {this.state.relatedObat != null && this.state.relatedObat.length > 0 ? this.renderRelated() : null}
                    </View>


                </Content>
                {/* <View style={styles.background}> */}

                {/* {this.renderTopHeader()} */}

                {/* <ScrollView
                    ref={s => this.scrollView = s}
                    showsVerticalScrollIndicator={false}
                    style={{
                        backgroundColor: 'transparent',
                        // marginTop: -(HEIGHT / 2.7), no tab header margin
                        marginTop: -(HEIGHT / 2.0),
                        marginBottom: 16,
                        borderRadius: 15,
                    }}
                >

                    <View style={{
                        width: WIDTH * 0.95,
                        borderRadius: 15,
                        backgroundColor: '#FFFFFF',
                        marginBottom: 16
                    }}>
                        {this.state.obat.title != undefined ? this.renderTitleObat() : null}

                        {this.state.componentObat.length > 0 ? this.renderContent() : null}
                    </View>

                    <View style={{
                        width: WIDTH * 0.95,
                        borderRadius: 15,
                        backgroundColor: '#FFFFFF',
                        marginBottom: 16
                    }}>

                        {this.state.relatedObat != null && this.state.relatedObat.length > 0 ? this.renderRelated() : null}
                    </View>
                </ScrollView> */}

                {/* </View> */}
            </Container>
        )
    }

    renderContent() {
        return (
            <Card >
                <CardItem >
                    <Col>
                        <Text style={styles.textTitle}>{this.state.obat.title}</Text>

                        {this.state.componentObat.length > 0 ? this.renderDataContent() : null}
                    </Col>

                </CardItem>
            </Card>
        )
    }

    renderRelated = () => {
        return (
            <Card style={{ flex: 0 }}>
                <Col>
                    <CardItem style={{
                        backgroundColor: '#FFFFFF',
                        borderRadius: 15,
                    }}>
                        <View style={{
                            paddingHorizontal: 0,
                        }}>
                            <Text style={styles.contentLabel} >Related</Text>
                            <View style={styles.viewRelated}>
                                {this.renderDataRelated(this.state.relatedObat)}
                            </View>
                        </View>
                    </CardItem>
                </Col>
            </Card>

        )
    }

    renderDataRelated = (dataRelated) => {
        let items = [];
        if (dataRelated != null && dataRelated.length > 0) {
            let self = this;
            dataRelated.map(function (value, i) {
                items.push(
                    <Tag
                        {...testID('tag_obat_related')}
                        key={i}
                        value={value.title}
                        idObat={value.obat_id} />
                )
            });

        }

        return items
    }

    renderTitleObat() {
        return (
            <Text
                {...testID('title_obat')}
                style={styles.contentNamaObat}>{this.state.obat.title}</Text>
        )
    }

    renderDataContent() {
        const tab = this.state.componentObat;
        return tab.map((value, index) => {
            this.state.desc = value.description;

            if (value.description != null && this.removeAllHtmlTag(value.description) != "") {
                return (
                    <ContentDetailAtoZ
                        key={index}
                        dataObat={value}
                        stateLoadWebView={this.stateLoadWebView} />
                )
            }
        })
    }

    renderTopHeader() {
        const { navigation } = this.props;
        const { goBack } = navigation;
        return (
            <View style={styles.top}>

                <View style={styles.topMargin}>
                    <TouchableWithoutFeedback
                        {...testID('button_back')}
                        onPress={() => this.onBackPressed()}
                    >
                        <Icon style={styles.back_button} name='md-arrow-round-back' />
                    </TouchableWithoutFeedback>
                    <Text style={styles.nama_obat}>{this.state.obat.title}</Text>
                </View>

                {/* temporary disable  */}

                {/* <View style={{
                    flexDirection: 'row',
                    width: WIDTH,
                    marginTop: HEIGHT * 0.05
                }}>

                    <ScrollView horizontal={true} style={styles.containerTabs}>
                        {this.renderButtonTab()}
                    </ScrollView>

                </View> */}
            </View >
        )
    }

}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F3F3F3',
        alignItems: 'center',
    },
    top: {
        backgroundColor: '#CB1D50',
        height: HEIGHT * 0.6,
        width: WIDTH * 2,
        borderBottomLeftRadius: WIDTH * 4,
        borderBottomRightRadius: WIDTH * 4,
        flexDirection: 'column',
        alignItems: 'center'
        // borderRadius: WIDTH/4,
    },
    topMargin: {
        marginLeft: 0,
        width: WIDTH,
        // alignItems: 'flex-start'
    },
    back_button: {
        color: 'white',
        alignItems: 'flex-start',
        position: 'absolute',
        left: 0,
        marginLeft: 20,
        marginTop: 20,
    },
    nama_obat: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20,
        fontFamily: 'Nunito-SemiBold',
        fontSize: 18,
        color: '#FFFFFF'
    },
    contentNamaObat: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontFamily: 'Nunito-Bold',
        fontSize: 20,
        color: '#454F63'
    },
    contentLabel: {
        alignSelf: 'flex-start',
        justifyContent: 'center',
        marginBottom: 10,
        //marginLeft: 15,
        fontFamily: 'Nunito-Bold',
        fontSize: 16,
        color: '#454F63'
    },
    text: {
        alignSelf: 'flex-start',
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        fontFamily: 'Nunito-Regular',
        fontSize: 14,
        color: '#78849E'
    },
    containerTabs: {
        flexDirection: "row",
        width: '150%',
        backgroundColor: '#fff'
    },
    buttonTab: {
        // width: '50%',
        paddingHorizontal: 10,
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 3,
        borderBottomColor: 'transparent'
    },
    textButtonTab: {
        fontFamily: 'Nunito-Bold',
        fontSize: 13,
        color: '#959DAD'
    },
    coverWebView: {
        position: 'absolute',
        backgroundColor: 'transparent',
        width: '100%',
        height: 130,
        zIndex: 1
    },

    viewRelated: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    toolbarIcon: {
        color: '#FFFFFF',
        fontSize: 25,
    },
    mainCardItem: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textTitle: {
        color: '#454F63',
        fontSize: 20,
        fontFamily: 'Nunito-Bold',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 5,
        // backgroundColor: 'yellow'
    },
})

export default DetailAtoZ;