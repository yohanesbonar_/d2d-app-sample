import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, ScrollView, Dimensions, Alert, SafeAreaView, BackHandler } from 'react-native';
import { Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right, Item, Toast } from 'native-base';
import { Loader } from './../components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { WebView } from 'react-native-webview';
import platform from '../../theme/variables/d2dColor';
import Orientation from "react-native-orientation";

export default class GeneralWebview extends Component {

    state = {
        showLoader: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        title: 'TITLE',
        content: null,
        orientation: 'portrait',
        width: 0,
        height: 0
    }

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.getOrientation();
        // this unlocks any previous locks to all Orientations
        Orientation.unlockAllOrientations();
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    onBackPressed() {
        Orientation.lockToPortrait();
        this.props.navigation.goBack();
    }

    getOrientation = (reload) => {
        if (this.rootView) {
            if (Dimensions.get('window').width < Dimensions.get('window').height) {
                this.setState({ orientation: 'portrait' });
            } else {
                this.setState({ orientation: 'landscape' });
                if (reload == true) {
                    setTimeout(() => {
                        this.webview.reload();
                    }, 500)
                }
            }
        }

        setTimeout(() => {
            if (this.state.orientation === 'portrait') {
                Alert.alert('Info', 'For better view, use your phone in landscape mode!');
            }
        }, 500)
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change')
        this.backHandler.remove();
    }

    _renderUrl = () => {
        let { params } = this.props.navigation.state;
        return (
            <Body>
                <WebView
                    // userAgent={'Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)'}
                    ref={ref => (this.webview = ref)}
                    // onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack; }}
                    // injectedJavaScript={jsCode}
                    javaScriptEnabledAndroid={true}
                    source={{ uri: params }}
                    // onLoadStart={this.onloadStart}
                    // onLoadEnd={this.onloadEnd}
                    style={{ flex: 1, flexDirection: 'column', width: this.state.width == 0 ? wp('100%') : this.state.width, backgroundColor: '#F3F3F3' }} />
            </Body>
        )
    }

    onLayout = (e) => {
        this.setState({
            width: e.nativeEvent.layout.width,
            height: e.nativeEvent.layout.height
        })
        this.webview.reload();
    }

    render() {
        const nav = this.props.navigation;

        return (
            <Container
                ref={ref => (this.rootView = ref)}
                onLayout={this.onLayout}>
                <Loader visible={this.state.showLoader} />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Grafik</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                {this._renderUrl()}
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
});
