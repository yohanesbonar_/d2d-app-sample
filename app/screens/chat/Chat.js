import React, { Component } from 'react';
import { Dimensions, Platform, StatusBar, StyleSheet, View, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native';
import Menu from 'react-native-material-menu';
import {
    Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right, Item, Toast,
} from 'native-base';
import { Loader } from './../../components';
import { escapeHtml } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { WebView } from 'react-native-webview';
import { ParamSetting, getSetting } from './../../libs/NetworkUtility';
import { testID } from './../../libs/Common';
export default class Chat extends Component {
    url = 'https://d2d.co.id/livechat.php?id=';
    validateUrl = 'https://www.youtube.com/watch?v=';
    defaultImageSpeaker = 'https://s3-ap-southeast-1.amazonaws.com/static.guesehat.com/directories_thumb/93df4a25ec23629e3377296e62132386.jpg';
    state = {
        idYoutube: '',
        urlWebview: '',
        tempUrlWebview: '',
        userAgent: 'D2D'
        // title: '',
    }
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const { params } = this.props.navigation.state;
        console.log(params)
        this.setState({
            idYoutube: params.idYoutube,
            urlWebview: params.urlWebview,
            // title : params.title
        })
    }
    _onNavigationStateChange(webViewState) {
        console.log('_onNavigationStateChange webViewState:', webViewState)
        if (webViewState.url == this.validateUrl + this.state.idYoutube || webViewState.url == 'https://www.youtube.com/oops') {
            this.setState({ urlWebview: this.url + this.state.idYoutube + '&open=true' });
        }
        if (platform.platform == 'android') {
            if (webViewState.url !== 'about:blank') {
                if (webViewState.url.includes('accounts.google.com')) {
                    this.setState({
                        userAgent: 'D2D'
                    })
                } else {
                    this.setState({
                        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
                    })
                }
            }
        } else {
            // if (!webViewState.loading) {
            if (webViewState.url.includes('accounts.google.com')) {
                this.setState({
                    userAgent: 'D2D'
                })
            } else {
                this.setState({
                    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.4 Safari/605.1.15'
                })
            }
            // }
        }

        console.log('_onNavigationStateChange user agent:', this.state.userAgent)

        this.setState({ tempUrlWebview: webViewState.url });
    }
    render() {
        const nav = this.props.navigation;
        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >
                    <Header noShadow>
                        <Left style={{ flex: 0.14 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => nav.goBack()}>
                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <View style={{
                            justifyContent: 'center', flex: 0.86,
                        }}>
                            <Text style={{ color: '#fff' }}>Back to Webinar</Text>
                        </View>
                        {/* <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>{this.state.title}</Title>
                    </Body>
                    <Right style={{ flex: 0.5 }} /> */}
                        {/* <TouchableOpacity
                            onPress={() => nav.goBack()}>
                            <View style={{flexDirection: 'row',}}>
                             <Icon name='md-arrow-back' style={{ color : '#fff'}} />
                            <Text style={{ color : '#fff'}}>Back to Webinar</Text>
                            </View>
                        </TouchableOpacity> */}
                    </Header>
                </SafeAreaView>
                <WebView
                    javaScriptEnabled={true}
                    source={{ uri: this.state.urlWebview }}
                    startInLoadingState={true}
                    onNavigationStateChange={this._onNavigationStateChange.bind(this)}
                    userAgent={this.state.f}
                    //userAgent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
                    style={{ width: wp('100%') }}
                />
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
});