import React, { Component } from 'react';
import { NavigationActions, StackActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, Text, View, ImageBackground, Image } from 'react-native';
import { Swiper } from './../../components';
import platform from '../../../theme/variables/d2dColor';

export default class Onboarding extends Component {
    resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login' })] });

    constructor(props) {
        super(props);
    }

    async startApp() {
        await AsyncStorage.setItem('ONBOARDING', 'Y');
        this.ONBOARDING = await AsyncStorage.getItem('ONBOARDING');
        this.props.navigation.replace('Login');
    }

    render() {
        return (
            <View></View>
        );
    }
}

const styles = StyleSheet.create({
    // Slide styles
    slide: {
        flex: 1,                    // Take up all screen
        justifyContent: 'center',   // Center vertically
        alignItems: 'center',       // Center horizontally
    },
    image: {
        width: 140,
        height: 140
    },
    titleApp: {
        fontFamily: 'Nunito-Bold',
        fontSize: 40,
        textAlign: 'center',
        color: '#fff',
        padding: 20,
    }
});