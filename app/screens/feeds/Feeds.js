import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
    StatusBar,
    StyleSheet,
    View,
    Modal,
    Image,
    FlatList,
    TouchableOpacity,
    Linking,
    Alert,
    Platform,
    SafeAreaView,
} from "react-native";
import firebase from "react-native-firebase";
import {
    Container,
    Header,
    Title,
    Icon,
    Text,
    Toast,
    Body,
    Right,
    Button,
} from "native-base";
import {
    Loader,
    ListItemLearningSpecialist,
    CardItemEvent,
    CardItemWebinar,
} from "./../../components";
import {
    STORAGE_TABLE_NAME,
    EnumFeedsCategory,
    subscribeFcm,
    subscribeFcmSubscription,
    testID,
    checkTopicsBySubscription,
    sendTopicsWebinar,
    checkNpaIdiEmpty,
    isStarted,
} from "./../../libs/Common";
import {
    doCheckVersion,
    getDetailSlug,
    getNewApiFeeds,
    sendFIrebaseToken,
    doRegisterEvent,
    getDetailCme,
    getEventDetail,
    getLinkGraphTD,
    getPopupShow,
    cappingPopup,
    submitPopup,
    updateDataProfile,
    doDetailCertificate,
    getWebinarDetail,
} from "./../../libs/NetworkUtility";
import platform from "../../../theme/variables/d2dColor";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as lor,
    removeOrientationListener as rol,
} from "react-native-responsive-screen";
import moment from "moment-timezone";
import { NavigationActions, StackActions } from "react-navigation";
const gueloginAuth = firebase.app("guelogin");
import _, { has } from "lodash";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import ReactModal from "react-native-modal";
import ModalPopup from "./ModalPopup";
import DeviceInfo from "react-native-device-info";
import { translate } from "../../libs/localize/LocalizeHelper";
import {
    getData,
    getLocalize,
    KEY_ASYNC_STORAGE,
    sendTopicEventAttendee,
    sendTopicWebinarAttendee,
} from "../../../src/utils";
import {
    getAgreementPopupPrivacyPolicy,
    submitPopupPrivacyPolicy,
} from "../../../src/utils/network/agreement";
export default class Feeds extends Component {
    CURRENT_VERSION_ANDROID = DeviceInfo.getVersion();
    CURRENT_VERSION_IOS = DeviceInfo.getVersion();

    typeTargetNotif = null;

    page = 1;
    state = {
        showLoader: false,
        feeds: [],
        onRefresh: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        hideFooter: true,
        subscription: null,
        uid: null,
        isEmptyDataFeeds: false,
        announcement: false,
        isNotCompletedProfile: false,
        isLiveNow: true,
        titleEventWebinar: "",
        dataWebinarLive: null,
        mProfile: null,
        showPopup: false,
        dataPopup: "",
        isChecked: false,
        isInternationalMode: false,
        server_date: "",
        country_code: "",
    };

    constructor(props) {
        super(props);
        this.getDataCountryCode();
    }

    getDataCountryCode = () => {
        getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
            this.setState({
                country_code: profile.country_code,
            });
        });
    };

    async componentDidMount() {
        const user = gueloginAuth.auth().currentUser;
        console.log("componentDidMount user: ", user);

        this.createNotificationListeners();
        this.createNotificationOpenedListener();
        this.createNotificationDisplayedListener();

        if (user) {
            if (
                user.emailVerified ||
                (user.emailVerified === false &&
                    (user.providerData[0].providerId == "facebook.com" ||
                        user.providerData[0].providerId == "gmail"))
            ) {
                let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
                profile = JSON.parse(profile);
                if (profile != null) {
                    console.log("profile.country_code: ", profile.country_code);
                    if (profile.country_code != "ID") {
                        this.setState({
                            isInternationalMode: true,
                        });
                    }
                }
                this.checkDeepLink();
                StatusBar.setHidden(false);
                this.getDataProfile();
                lor(this);
                this.initFcm();
                this.checkDataUser();

                const didFocusCheckUser = this.props.navigation.addListener(
                    "didFocus",
                    (payload) => {
                        this.checkDataUser();
                    }
                );

                this.messageListener = firebase.messaging().onMessage((message) => {
                    // receiving message
                    if (_.isEmpty(message._data) == true) {
                        console.warn("silent push adjust");
                    }
                });

                this.checkModalPopup();
            }
        }
    }

    async checkModalPopup() {
        try {
            let user = gueloginAuth.auth().currentUser;
            if (!_.isEmpty(user)) {
                // let response = await getPopupShow(); -> change to 3.0
                let response = await getAgreementPopupPrivacyPolicy();
                console.log("checkModalPopup response: ", response);
                let res = response.result;
                if (response.acknowledge == true && res != null && !_.isEmpty(res)) {
                    // res.uid = user.uid;
                    this.setState({
                        showPopup: true,
                        dataPopup: res,
                    });
                    console.log("popup", res);
                } else {
                    console.log("gagalRes", res);
                }
            }
        } catch (error) {
            console.log("gagal", error);
            Toast.show({
                text: "Something went wrong!",
                position: "top",
                duration: 3000,
            });
        }
    }

    handleCappingPopup = async () => {
        this.setState({
            showPopup: false,
        });
        try {
            let user = gueloginAuth.auth().currentUser;
            if (!_.isEmpty(user)) {
                let response = await cappingPopup();
                if (response.isSuccess == true) {
                    console.log("res", response);
                    console.log("capping");
                } else {
                    console.log("gagal", res);
                }
            }
        } catch (error) {
            console.log("gagalApi", res);
            Toast.show({
                text: "Something went wrong!",
                position: "top",
                duration: 3000,
            });
        }
    };

    async checkShowPopup() {
        let showPopup = await AsyncStorage.getItem("showPopup");
        let popup = JSON.parse(await AsyncStorage.getItem("totalShowPopup"));
        console.log("popup", popup);
        if (popup != null && popup.total_show > 0 && showPopup) {
            this.setState({
                showPopup: true,
                dataPopup: popup,
            });
        }
    }

    handleSubmitPopup = async (data) => {
        console.log("handleSubmitPopup =>>", data);
        try {
            let user = gueloginAuth.auth().currentUser;
            if (!_.isEmpty(user)) {
                // let popup = JSON.parse(await AsyncStorage.getItem('totalShowPopup'));
                let params = {
                    //   id: this.state.dataPopup.id,
                    //   status: this.state.isChecked ? "A" : "D",     // =>>>>> comment karena ganti di ke api 3.0
                    checkboxes: data,
                };
                // let response = await submitPopup(params);          // =>>>>> comment karena ganti di ke api 3.0
                let response = await submitPopupPrivacyPolicy(params);
                console.log("handleSubmitPopup submit =>>", response);
                console.log("handleSubmitPopup Params =>>", params);
                // if (response.isSuccess == true) {                /// =>>>>> comment karena ganti di ke api 3.0
                if (response.acknowledge == true) {
                    this.setState({
                        isFetching: false,
                        isSuccess: true,
                        isFailed: false,
                        hideFooter: true,
                        onRefresh: false,
                        showPopup: false,
                    });
                } else {
                    if (response.message != "logout") {
                        Toast.show({
                            text: response.message,
                            position: "top",
                            duration: 3000,
                        });
                        this.setState({
                            isFetching: false,
                            isSuccess: true,
                            isFailed: true,
                            hideFooter: true,
                            onRefresh: false,
                        });
                    }
                }
            }
        } catch (error) {
            this.setState({
                isFetching: false,
                isSuccess: false,
                isFailed: true,
                hideFooter: false,
                onRefresh: false,
            });
            Toast.show({
                text: "Something went wrong!",
                position: "top",
                duration: 3000,
            });
        }
    };

    checkDataUser = async () => {
        let isNotCompleted = false;
        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let dataProfile = JSON.parse(getJsonProfile);

        if (dataProfile != null && dataProfile.country_code != "PH") {
            if (
                dataProfile.spesialization == "" ||
                dataProfile.spesialization == null ||
                dataProfile.spesialization.length <= 0 ||
                (dataProfile.phone == "" || dataProfile.phone == null) ||
                (dataProfile.country_code == "ID" && dataProfile.pns == null) ||
                (dataProfile.born_date == "" || dataProfile.born_date == null) ||
                (dataProfile.country_code == "ID" &&
                    (dataProfile.home_location == "" ||
                        dataProfile.home_location == null)) ||
                (dataProfile.clinic_location_1 == "" &&
                    dataProfile.clinic_location_2 == "" &&
                    dataProfile.clinic_location_3 == "") ||
                (dataProfile.education_1 == "" &&
                    dataProfile.education_2 == "" &&
                    dataProfile.education_3 == "")
            ) {
                isNotCompleted = true;
            }
        }

        this.setState({
            isNotCompletedProfile: isNotCompleted,
            mProfile: dataProfile,
        });
    };

    async checkAppVersion() {
        try {
            // -- api v3 --
            let deviceBuildNumber = DeviceInfo.getBuildNumber();
            let deviceVersion = DeviceInfo.getVersion();
            let paramsData = {
                version: deviceVersion,
                build_number: deviceBuildNumber,
            };
            let response = await doCheckVersion(paramsData);
            if (response.isSuccess === true && response.docs != null) {
                let currentVersion =
                    platform.platform == "ios"
                        ? this.CURRENT_VERSION_IOS
                        : this.CURRENT_VERSION_ANDROID;
                console.log("doCheckVersion currentVersion: ", currentVersion);

                if (response.docs.show == false) {
                    this.updateProfile();
                }
            }
        } catch (error) {
            console.log("checkAppVersion error: ", error);
        }
    }

    async initFcm() {
        console.log("initFcm");
        console.log("cekTopicFCM: ", await subscribeFcm());
        let topicFCM = await subscribeFcm();
        await firebase.messaging().subscribeToTopic(topicFCM);
        // console.warn(subscribeFcm)
        //case kill apps
        let deviceToken = await AsyncStorage.getItem(STORAGE_TABLE_NAME.FCM_TOKEN);
        let isUpdateProfile = await AsyncStorage.getItem(
            STORAGE_TABLE_NAME.IS_UPDATE_PROFILE
        );

        if (
            deviceToken == null ||
            deviceToken == "" ||
            isUpdateProfile == null ||
            isUpdateProfile == ""
        ) {
            const enabled = await firebase.messaging().hasPermission();
            if (enabled) {
                // user has permissions
                const fcmToken = await firebase.messaging().getToken();
                if (fcmToken) {
                    // user has a device toke
                    await this.sendFCM(fcmToken);
                } else {
                    // user doesn't have a device token yet
                    await firebase.messaging().requestPermission();
                }
            } else {
                // user doesn't have permission
                await firebase.messaging().requestPermission();
            }
        }

        await this.checkAppVersion();

        const notificationOpen = await firebase
            .notifications()
            .getInitialNotification();
        console.log("notificationOpen: ", notificationOpen);
        if (notificationOpen) {
            // Toast.show({ text: '1', position: 'bottom', duration: 3000 });
            const notification = notificationOpen.notification;
            if (notification != null && notification.data != null) {
                this.onReceivePushNotif(notification);
            }
        }

        // Create the channel
        const channel = new firebase.notifications.Android.Channel(
            "d2dpushnotif-channel",
            "D2D Channel",
            firebase.notifications.Android.Importance.Max
        ).setDescription("D2D channel");
        firebase.notifications().android.createChannel(channel);
    }

    createNotificationDisplayedListener = () => {
        this.onUnsubscribeNotificationDisplayedListener = firebase
            .notifications()
            .onNotificationDisplayed((notification) => {
                // Process your notification as required
                // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
            });
    };

    createNotificationOpenedListener = () => {
        this.onUnsubscribeNotificationOpenedListener = firebase
            .notifications()
            .onNotificationOpened((notificationOpen) => {
                // Toast.show({ text: '3', position: 'bottom', duration: 3000 });
                const notification = notificationOpen.notification;
                console.log("notificationOpenedListener: ", notification);

                if (notification != null && notification.data != null) {
                    this.onReceivePushNotif(notification);
                }

                if (platform.platform == "android") {
                    //notification.android.setAutoCancel(true)
                    firebase
                        .notifications()
                        .removeDeliveredNotification(notification.notificationId);
                }
            });
    };

    createNotificationListeners = () => {
        this.onUnsubscribeNotificaitonListener = firebase
            .notifications()
            .onNotification((notification) => {
                //firebase.notifications().displayNotification(notification);
                if (notification !== null && notification.data != null)
                    this.displayNotification(notification.data);
            });
    };

    removeNotificationListeners = () => {
        this.onUnsubscribeNotificaitonListener();
        this.onUnsubscribeNotificationOpenedListener();
        this.onUnsubscribeNotificationDisplayedListener();
    };

    randomizer = () =>
        Math.floor((1 + Math.random()) * 0x100000000000).toString(16);

    async displayNotification(data) {
        if (Platform.OS === "ios") {
            const notificationId = `d2d_notif_${this.randomizer()}`;

            const notification = new firebase.notifications.Notification()
                .setNotificationId(notificationId)
                .setTitle(data.title)
                .setBody(data.body)
                .setSound("default")
                .setData({
                    ...data,
                    show_in_foreground: true,
                    groupSummary: true,
                });

            firebase.notifications().displayNotification(notification);
        }

        if (Platform.OS === "android") {
            const notificationId = `d2d_notif_${this.randomizer()}`;

            const notification = new firebase.notifications.Notification()
                .setNotificationId(notificationId)
                .setTitle(data.title)
                .setBody(data.body)
                .setSound("default")
                .android.setAutoCancel(true)
                .android.setChannelId("d2dpushnotif-channel")
                .android.setSmallIcon("ic_launcher")
                .android.setPriority(firebase.notifications.Android.Priority.Max)
                .setData({
                    // data: { data },
                    ...data,
                    show_in_foreground: true,
                    groupSummary: true,
                });
            firebase.notifications().displayNotification(notification);
        }
    }

    onReceivePushNotif = (notification) => {
        console.log("notification.data: ", notification.data);
        if (notification.data.type == EnumFeedsCategory.CME_CERTIFICATE) {
            let data = {
                title: notification.data.title,
                certificate: notification.data.slug,
                isRead: true,
            };
            this.gotoCmeCertificate(data);
        } else if (
            notification.data.type == EnumFeedsCategory.PROFILE_NOTIFICATION
        ) {
            this.gotoProfileNotification(notification.data);
        } else if (notification.data.type == EnumFeedsCategory.REQ_JOURNAL) {
            let data = {
                isFromPushNotification: true,
            };
            this.gotoReqJournal(data);
        } else if (notification.data.type == EnumFeedsCategory.JOURNAL) {
            // this.gotoReqJournal()
            this.onRefreshFeeds();
        } else if (notification.data.type == EnumFeedsCategory.EVENT_LIST) {
            this.gotoEventList();
        } else if (notification.data.type == EnumFeedsCategory.EVENT) {
            this.gotoEventDetailFromPushNotif(notification.data);
        } else if (notification.data.type == EnumFeedsCategory.CME) {
            this.doHitDetailCME(notification.data.id_cme);
        } else if (notification.data.type == EnumFeedsCategory.PMM_PATIENT) {
            this.gotoPMMPatient(notification.data);
        } else if (notification.data.type == EnumFeedsCategory.CME_LIST) {
            this.gotoCmeList(notification.data);
        } else if (notification.data.type == EnumFeedsCategory.CME_DETAIL) {
            this.doHitDetailCME(notification.data.quiz_id);
        } else if (notification.data.type == EnumFeedsCategory.ATOZ_LIST) {
            this.gotoAtozList(notification.data);
        } else if (notification.data.type == EnumFeedsCategory.ATOZ_DETAIL) {
            this.gotoAtozDetail(notification.data);
        } else if (notification.data.type == EnumFeedsCategory.FEEDS) {
            this.gotoFeeds();
        } else if (
            notification.data.type == EnumFeedsCategory.CERTIFICATE_CME ||
            notification.data.type == EnumFeedsCategory.CERTIFICATE_EVENT ||
            notification.data.type == EnumFeedsCategory.CERTIFICATE_OFFLINE ||
            notification.data.type == EnumFeedsCategory.CERTIFICATE_WEBINAR
        ) {
            //goto certificate
            this.doHitDetailCertificate(notification.data);
        } else {
            if (notification.data.type == "webinar") {
                if (typeof notification.data.target != "undefined") {
                    this.typeTargetNotif = notification.data.target;
                }
            }

            if (
                !_.isEmpty(notification.data.type) ||
                !_.isEmpty(notification.data.slug) ||
                !_.isEmpty(notification.data.slug_hash)
            ) {
                this.getDetailCategory(
                    notification.data.type,
                    notification.data.slug,
                    notification.data.slug_hash
                );
            }
        }
    };

    componentWillUnmount() {
        console.log("componentWillUnmount");
        this.removeNotificationListeners();
        Linking.removeEventListener("url", this.handleOpenURL);
        rol();
    }

    componentWillUnMount() {
        this.notificationDisplayedListener();
        this.notificationListener();
        this.notificationOpenedListener();
        Linking.removeEventListener("url", this.handleOpenURL);
        rol();
    }

    //---- start code deeplinking -----//

    checkDeepLink() {
        Linking.getInitialURL()
            .then((url) => this.handleOpenURL({ url }))
            .catch((err) => console.error(err));

        //for app running in background
        Linking.addEventListener("url", this.handleOpenURL);
    }

    handleOpenURL = async (deeplink) => {
        // D

        let currentUser = gueloginAuth.auth().currentUser;
        console.warn("feeds currentUser", currentUser);

        if (currentUser != null) {
            let provider = [];
            if (currentUser.providerData.length > 0) {
                currentUser.providerData.forEach((val, i) => {
                    provider.push(val.providerId);
                });
            }
            currentUser.emailVerified = _.includes(provider, "password")
                ? currentUser.emailVerified
                : true;
            console.log("include: ", _.includes(provider, "password"));
            console.log("currentUser.emailVerified: ", currentUser.emailVerified);

            let emailVerified = _.includes(provider, "password")
                ? currentUser.emailVerified
                : true;
            console.log("emailVerified: ", emailVerified);
            _.assign(currentUser, { emailVerified });

            //bug fix: to prevent open deeplink when after login but email is not verified
            if (currentUser && emailVerified === true) {
                let regex = /d2d.co.id/;

                if (deeplink.url != null) {
                    if (regex.test(deeplink.url)) {
                        this.splitUrl(deeplink.url);
                    }
                }
            }
        }
    };

    splitUrl = (url) => {
        let replaceUrl = url.replace("http://d2d.co.id/?", "");
        replaceUrl = url.replace("https://d2d.co.id/?", "");
        let splitUrl = replaceUrl.split("&");
        if (splitUrl != null && splitUrl.length == 3) {
            let category = splitUrl[0].replace("type=", "");
            let slug = splitUrl[1].replace("title=", "");
            let hash = splitUrl[2].replace("hash=", "");

            if (category == "eventsignin") {
                this.doRegisterEventByQR(slug, hash);
            } else {
                this.getDetailCategory(category, slug, hash);
            }
        }
    };

    showAlertRegistration = (message, isSuccess, type, slug, hash) => {
        setTimeout(
            () =>
                Alert.alert(
                    "Information",
                    message,
                    [
                        {
                            text: "OK",
                            onPress: () =>
                                isSuccess == true
                                    ? this.getDetailCategory(type, slug, hash)
                                    : null,
                        },
                    ],
                    { cancelable: true }
                ),
            700
        );
    };

    doRegisterEventByQR = async (slug, hash) => {
        if (hash != null) {
            this.setState({ showLoader: true });
            let response = await doRegisterEvent(hash);
            this.setState({ showLoader: false });
            if (response.isSuccess == true && response.data != null) {
                // this.showAlertRegistration(response.message, true, EnumFeedsCategory.EVENT, slug, hash);
                // console.warn('data', response.data)
                this.gotoEventDetail(response.data);
            } else {
                if (response.message != null) {
                    let splitMsg = response.message.split(",");
                    if (splitMsg != null && splitMsg.length > 1) {
                        if (splitMsg[1] == "toast") {
                            Toast.show({
                                text: splitMsg[0],
                                position: "bottom",
                                duration: 3000,
                            });
                        } else {
                            this.showAlertRegistration(splitMsg[0], false, null, null, null);
                        }
                    } else {
                        Toast.show({
                            text: response.message,
                            position: "bottom",
                            duration: 3000,
                        });
                    }
                }
            }
        }
    };

    async getDetailCategory(category, slug, hash) {
        if (category != null && slug != null) {
            this.setState({ showLoader: true });

            if (category == EnumFeedsCategory.EVENT) {
                let params = {
                    type: "deeplink",
                    hash: hash,
                };
                let response = await getEventDetail(params);
                this.setState({ showLoader: false });
                if (response.isSuccess && response.docs != null) {
                    this.gotoEventDetailByHashDeeplink(response.docs);
                } else {
                    Toast.show({
                        text: response.message,
                        position: "bottom",
                        duration: 3000,
                    });
                }
            } else if (category == EnumFeedsCategory.WEBINAR) {
                let params = {
                    type: "deeplink",
                    hash: hash,
                };
                let response = await getWebinarDetail(params);
                this.setState({ showLoader: false });
                console.log("response getWebinarDetail: ", response);
                if (response.isSuccess && response.docs != null) {
                    let isShowWebinar = this.checkShowWebinar(response.docs);
                    if (isShowWebinar) {
                        let data = response.docs;
                        // data.isLive = true;
                        data.isLive = moment().isSameOrAfter(
                            moment(data.end_date, "YYYY-MM-DD HH:mm:ss")
                        )
                            ? false
                            : true;
                        response.docs.typeTargetNotif = this.typeTargetNotif;

                        this.gotoWebinarDetail(false, response.docs);
                    } else {
                        Toast.show({
                            text: `Sorry, you're not allowed to access this content.`,
                            position: "bottom",
                            duration: 3000,
                        });
                    }
                } else {
                    Toast.show({
                        text: response.message,
                        position: "bottom",
                        duration: 3000,
                    });
                }
            } else {
                let type =
                    category == EnumFeedsCategory.REQ_JOURNAL
                        ? EnumFeedsCategory.PDF_JOURNAL
                        : category;

                let categoryReqJournal = category;

                let response = await getDetailSlug(type, slug, hash);
                console.log("getDetailSlug response: ", response);
                this.setState({ showLoader: false });
                if (response.isSuccess) {
                    //category : event / pdf / video
                    let category = response.data[0].category;

                    // if (category == EnumFeedsCategory.EVENT) {
                    //     this.gotoEventDetail(response.data[0]);
                    // } else
                    if (
                        category == EnumFeedsCategory.PDF_GUIDELINE ||
                        category == EnumFeedsCategory.PDF_KNOWLEDGE ||
                        category == EnumFeedsCategory.PDF_BROCHURE ||
                        category == EnumFeedsCategory.PDF_LITERATURE ||
                        category == EnumFeedsCategory.PDF_PRODUCT_GROUP
                    ) {
                        this.gotoLearningGuidelineDetail(response.data[0]);
                    } else if (
                        category == EnumFeedsCategory.PDF_JOURNAL &&
                        categoryReqJournal == category
                    ) {
                        this.gotoLearningJournalDetail(response.data[0]);
                    } else if (categoryReqJournal == EnumFeedsCategory.REQ_JOURNAL) {
                        this.gotoReqJournal(response.data[0], true);
                    } else if (
                        category == EnumFeedsCategory.VIDEO_LEARNING ||
                        category == EnumFeedsCategory.VIDEO_KNOWLEDGE ||
                        category == EnumFeedsCategory.VIDEO_PRODUCT_GROUP ||
                        category == EnumFeedsCategory.VIDEO_BROCHURE ||
                        category == EnumFeedsCategory.VIDEO_GENERAL
                    ) {
                        this.gotoVideoLearning(response.data[0]);
                    }
                } else {
                    Toast.show({
                        text: response.message,
                        position: "bottom",
                        duration: 3000,
                    });
                }
            }
        }
    }

    doHitDetailCME = async (id) => {
        try {
            this.setState({ showLoader: true });
            let response = await getDetailCme(id);
            console.log("doHitDetailCME response: ", response);
            this.setState({ showLoader: false });

            if (response.isSuccess && response.data != null) {
                console.warn(response);
                if (response.data.done == "true") {
                    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
                    if (checkNpaIdiEmpty(profile)) {
                        this.gotoCmeQuizDetail(response.data);
                    } else if (!checkNpaIdiEmpty(profile)) {
                        this.gotoCmeCertificate(response.data);
                    }
                } else if (response.data.done == "false") {
                    this.gotoCmeDetail(response.data);
                }
            }
        } catch (error) {
            console.log(error);
        }
    };

    doHitDetailCertificate = async (data) => {
        console.log(data);
        this.setState({ showLoader: true });

        try {
            let response = await doDetailCertificate(data);
            console.log("doHitDetailCertificate response: ", response);

            if (response.isSuccess && response.data != null) {
                if (data.type == EnumFeedsCategory.CERTIFICATE_CME) {
                    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
                    if (checkNpaIdiEmpty(profile)) {
                        this.gotoCmeQuizDetail(response.data);
                    } else if (!checkNpaIdiEmpty(profile)) {
                        this.gotoCmeCertificate(response.data);
                    }
                } else {
                    this.gotoCmeCertificate(response.data);
                }
            }
            this.setState({ showLoader: false });
        } catch (error) {
            console.log(error);
        }
    };

    gotoCmeCertificate = (data) => {
        console.log("gotoCmeCertificate data: ", data);
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: "CmeCertificate",
                params: data,
            })
        );
    };

    gotoCmeDetail = (dataCme) => {
        console.log("gotoCmeDetail dataCme: ", dataCme);
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: "CmeDetail",
                params: dataCme,
                key: `cmedetail-${dataCme.id}`,
            })
        );
    };

    gotoEventList = () => {
        this.props.navigation.navigate("Event");
    };

    async gotoEventDetail(data) {
        console.log("gotoEventDetail data: ", data);
        let getProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let newProfile = JSON.parse(getProfile);
        newProfile.event_attendee.push(data.slug_hash);

        //only insert if data in asnyc storage not exist.
        if (!newProfile.event_attendee.includes(data.slug_hash)) {
            sendTopicEventAttendee(data.slug_hash);
            newProfile = JSON.stringify(newProfile);
            await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, newProfile);
        }

        this.props.navigation.navigate("EventDetail", {
            data: data,
            fromDeeplink: true,
        });
    }

    async gotoEventDetailByHashDeeplink(data) {
        console.log("gotoEventDetailByHashDeeplink data: ", data);

        this.props.navigation.navigate("EventDetail", {
            data: data,
        });
    }

    gotoEventDetailFromPushNotif = async (dataNotif) => {
        console.log("gotoEventDetailFromPushNotif dataNotif: ", dataNotif);
        dataNotif.id = dataNotif.content_id;
        this.props.navigation.navigate("EventDetail", {
            data: dataNotif,
        });
    };

    gotoLearningJournalDetail(data) {
        this.props.navigation.navigate("LearningJournalDetail", {
            data: data,
            fromDeeplink: true,
        });
    }

    gotoLearningGuidelineDetail(data) {
        let navGuidelineDetail = {
            type: "Navigate",
            routeName: "PdfView",
            params: data,
        };
        this.props.navigation.navigate(navGuidelineDetail);
    }

    gotoVideoLearning(data) {
        let navVideo = { type: "Navigate", routeName: "VideoView", params: data };
        this.props.navigation.navigate(navVideo);
    }

    async gotoPMMPatient(data) {
        console.log("gotoPMMPatient data: ", data);
        this.props.navigation.navigate("PerkenalanFitur", {
            data: data,
            isFromPushNotification: true,
        });
    }

    gotoCmeList(data) {
        console.log("gotoCmeList data: ", data);
        this.props.navigation.navigate("Cme", {
            data: data,
        });
    }

    gotoCmeQuizDetail(data) {
        let params = {
            id: data.quiz_id,
            ...data,
            from: "pushNotifCertificate",
        };
        console.log("gotoCmeQuizDetail params", params);
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: "CmeQuiz",
                params: params,
            })
        );
    }

    gotoAtozList(data) {
        console.log("gotoAtozList data: ", data);
        this.props.navigation.navigate("AtoZ", {
            data: data,
        });
    }

    gotoAtozDetail(data) {
        console.log("gotoAtozDetail data: ", data);
        this.props.navigation.navigate("DetailAtoZ", {
            obat_id: data.obat_id,
            title_obat: data.title_obat != null ? data.title_obat : "",
            isFromPushNotification: true,
        });
    }

    gotoScanScreen() {
        AdjustTracker(AdjustTrackerConfig.ScanQR);
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: "ScanScreen",
                params: {
                    onScanQR: this.onReceiveLinkFromScan,
                },
                key: `scanQr`,
            })
        );
    }

    gotoFeeds() {
        console.log(
            "gotoFeeds currentScreen: ",
            this.props.navigation.state.routeName
        );

        this.props.navigation.dispatch(StackActions.popToTop());

        //handle click notif when current screen in profile, cme, list event, etc
        if (this.props.navigation.state.routeName == "Feeds") {
            this.props.navigation.dispatch(
                NavigationActions.navigate({
                    routeName: "Feeds",
                })
            );
        }
    }

    gotoManageTemanDiabetes() {
        AdjustTracker(AdjustTrackerConfig.Feeds_Kelola);
        let navManageTemanDiabetes = {
            type: "Navigate",
            //routeName: 'PerkenalanFitur',
            routeName: "ManageTemanDiabetes",
            // params: data
        };
        console.log("navManageTemanDiabetes: ", navManageTemanDiabetes);
        this.props.navigation.navigate(navManageTemanDiabetes);
    }

    async updateProfile() {
        // let isUpdateProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.IS_UPDATE_PROFILE);
        // console.log("isUpdateProfile: ", isUpdateProfile)
        // if (isUpdateProfile != "Y") {

        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let fcm_token = await AsyncStorage.getItem(STORAGE_TABLE_NAME.FCM_TOKEN);

        let profile = JSON.parse(getJsonProfile);

        console.log("updateProfile profile: ", profile);

        let params = {
            uid: profile.uid,
            npa_idi: profile.npa_idi == null ? "" : profile.npa_idi,
            name: profile.name,
            email: profile.email,
            phone: profile.phone,
            born_date: profile.born_date,

            country_code: profile.country_code,
            //this.state.bornDate,
            education_1: profile.education_1,
            education_2: profile.education_2 == null ? "" : profile.education_2,
            education_3: profile.education_3 == null ? "" : profile.education_3,
            home_location: profile.home_location == null ? "" : profile.home_location,
            clinic_location_1: profile.clinic_location_1,
            clinic_location_2:
                profile.clinic_location_2 == null ? "" : profile.clinic_location_2,
            clinic_location_3:
                profile.clinic_location_3 == null ? "" : profile.clinic_location_3,
            pns: profile.pns,

            ///device info
            app_version: DeviceInfo.getVersion(),
            device_unique_id: DeviceInfo.getUniqueId(),
            fcm_token: fcm_token,
            device_platform: DeviceInfo.getSystemName(),
            system_version: DeviceInfo.getSystemVersion(),
            os_build_version: await DeviceInfo.getBuildId(), //should be await, because it was promise,
            device_brand: DeviceInfo.getBrand(),
        };
        console.log("updateProfile params: ", params);
        let response = await updateDataProfile(params);

        if (response.isSuccess == true) {
            await AsyncStorage.setItem(STORAGE_TABLE_NAME.IS_UPDATE_PROFILE, "Y");
            console.log("updateProfile response: ", response);
        } else {
            console.log("updateProfile response: ", response);
        }

        // }
    }

    onReceiveLinkFromScan = (url) => {
        let regex = /d2d.co.id/;
        let regexTD = /temandiabetes/;

        if (url != null && regex.test(url)) {
            this.splitUrl(url);
        } else if (url != null && regexTD.test(url)) {
            this.getLinkGraph(url);
        } else {
            Toast.show({
                text: "QR Code Not Found",
                position: "top",
                duration: 3000,
            });
        }
    };

    getLinkGraph = async (barcode) => {
        if (barcode) {
            let tdUserId = barcode.replace("temandiabetes", "");

            if (tdUserId) {
                try {
                    this.setState({ showLoader: true });
                    let response = await getLinkGraphTD(tdUserId);
                    this.setState({ showLoader: false });

                    if (response.isSuccess && response.data) {
                        let viewGraph = {
                            type: "Navigate",
                            routeName: "WebviewGrafik",
                            params: response.data,
                        };
                        this.props.navigation.navigate(viewGraph);
                    } else {
                        Toast.show({
                            text: response.message,
                            position: "top",
                            duration: 3000,
                        });
                    }
                } catch (error) {
                    this.setState({ showLoader: false });
                    Toast.show({
                        text: "Something went wrong!",
                        position: "top",
                        duration: 3000,
                    });
                }
            }
        }
    };

    gotoCmeCertificate(data) {
        let cmeCertificate = {
            type: "Navigate",
            routeName: "CmeCertificate",
            params: data,
        };
        this.props.navigation.navigate(cmeCertificate);
    }

    gotoProfileNotification(data) {
        let profileNotification = {
            type: "Navigate",
            routeName: "Profile",
            params: data,
        };
        this.props.navigation.navigate(profileNotification);
    }

    gotoReqJournal(data = {}, isFromDeeplink = false) {
        console.log("gotoReqJournal data: ", data);
        console.log("gotoReqJournal isFromDeeplink: ", isFromDeeplink);
        if (isFromDeeplink) {
            data.isFromDeeplink = isFromDeeplink;
        }
        let reqJournal = {
            type: "Navigate",
            routeName: "LearningRequestJournal",
            params: data,
        };
        this.props.navigation.navigate(reqJournal);
    }

    //---- end code deeplinking -----//

    async getDataProfile() {
        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let dataProfile = JSON.parse(getJsonProfile);
        let profileSubscription = null;
        let profileWebinar = null;
        if (dataProfile != null) {
            let subscriptionId = [];
            // let subscriptionTitle = [];
            let dataSubscription = "";
            profileSubscription = dataProfile.subscription;
            profileWebinar = dataProfile.webinar;

            if (profileSubscription != null && profileSubscription.length > 0) {
                let self = this;
                profileSubscription.map(function (d, i) {
                    subscriptionId.push(d.id);
                    // subscriptionTitle.push(d.title);
                });

                dataSubscription = subscriptionId.join(",");
            }

            this.setState({
                subscription: dataSubscription,
                uid: dataProfile.uid,
            });
        }

        this.getFeeds(this.page);

        // cek token topics fcm

        if (profileSubscription != null && profileSubscription.length > 0) {
            checkTopicsBySubscription(profileSubscription);
        }

        sendTopicsWebinar(profileWebinar);
    }

    doCheckAnnouncement = (announcement) => {
        let isShowWebinar = false;
        if (announcement != null) {
            isShowWebinar = this.checkShowWebinar(announcement);
        }

        this.setState({ announcement: isShowWebinar });
    };

    async getFeeds(page) {
        this.setState({ isFailed: false, isFetching: true, hideFooter: false });

        try {
            let user = gueloginAuth.auth().currentUser;
            if (!_.isEmpty(user)) {
                // let response = await getApiFeeds(page, this.state.subscription, true, true, true, true, false, false, false, null);
                let response = await getNewApiFeeds(page, this.state.subscription);
                console.log("response getNewApiFeeds", response);
                if (response.isSuccess == true) {
                    if (response.server_date != "") {
                        this.setState({
                            server_date: response.server_date,
                        });
                    }
                    this.setState({
                        isFetching: false,
                        isSuccess: true,
                        isFailed: false,
                        hideFooter: true,
                        onRefresh: false,
                        feeds: [...this.state.feeds, ...response.data],
                        isEmptyDataFeeds:
                            response.data != null && response.data.length > 0 ? false : true,
                        dataWebinarLive: response.announcement,
                        titleEventWebinar:
                            response.announcement != null ? response.announcement.title : "",
                        // announcement: response.announcement != null ? true : false
                    });

                    this.doCheckAnnouncement(response.announcement);
                } else {
                    if (response.message != "logout") {
                        Toast.show({
                            text: response.message,
                            position: "top",
                            duration: 3000,
                        });
                        this.setState({
                            isFetching: false,
                            isSuccess: true,
                            isFailed: true,
                            hideFooter: true,
                            onRefresh: false,
                        });
                    }
                }
            }
        } catch (error) {
            this.setState({
                isFetching: false,
                isSuccess: false,
                isFailed: true,
                hideFooter: false,
                onRefresh: false,
            });
            Toast.show({
                text: "Something went wrong!",
                position: "top",
                duration: 3000,
            });
        }
    }

    onRefreshFeeds() {
        this.page = 1;
        this.setState({ feeds: [], onRefresh: true, isEmptyDataFeeds: false });
        this.getDataProfile();
    }

    handleLoadMore = () => {
        if (this.state.isEmptyDataFeeds == true) {
            return;
        }
        this.page = this.state.isFailed == true ? this.page : this.page + 1;
        this.getFeeds(this.page);
    };

    checkShowWebinar = (item) => {
        let isShowWebinar = false;

        if (item.exclusive == 1 || item.isPaid == true) {
            if (this.state.mProfile != null) {
                if (
                    this.state.mProfile.webinar != null &&
                    this.state.mProfile.webinar.length > 0
                ) {
                    this.state.mProfile.webinar.forEach((slug_hash) => {
                        sendTopicWebinarAttendee(slug_hash);
                        if (slug_hash == item.slug_hash) {
                            isShowWebinar = true;
                        }
                    });
                }
            }
        } else {
            isShowWebinar = true;
        }

        return isShowWebinar;
    };

    _renderItem = ({ item }) => {
        if (item.category == EnumFeedsCategory.EVENT) {
            return (
                <View style={styles.viewCard}>
                    <CardItemEvent
                        navigation={this.props.navigation}
                        data={item}
                        manualUpdateReserve={true}
                    />
                </View>
            );
        } else if (item.category == EnumFeedsCategory.WEBINAR) {
            let isShowWebinar = this.checkShowWebinar(item);
            item.server_date = this.state.server_date;

            if (isShowWebinar) {
                if (item.isAnnouncement) {
                    return (
                        <TouchableOpacity
                            onPress={() => this.gotoWebinarDetail(false, item)}
                            style={styles.bgndAnnouncement}
                        >
                            <Text style={styles.txtAnnouncement}>
                                {item.title}
                                {this.shownDateAnnouncement(item.start_date)}
                            </Text>
                        </TouchableOpacity>
                    );
                } else {
                    return (
                        <View style={styles.viewCard}>
                            <CardItemWebinar
                                navigation={this.props.navigation}
                                data={item}
                                isLive={true}
                                country_code={this.state.country_code}
                            />
                        </View>
                    );
                }
            } else {
                return null;
            }

            // if (item.isAnnouncement) {
            //     return (
            //     <TouchableOpacity onPress={() => this.gotoWebinarDetail(false, item)}
            //         style={styles.bgndAnnouncement}>
            //         <Text style={styles.txtAnnouncement}>{item.title}{this.shownDateAnnouncement(item.start_date)}</Text>
            //     </TouchableOpacity>)
            // } else {
            //     let isShowWebinar = false;
            //     if (item.exclusive == 1) {
            //         if (this.state.mProfile != null) {
            //             if (this.state.mProfile.webinar != null && this.state.mProfile.webinar.length > 0) {
            //                 this.state.mProfile.webinar.forEach(slug_hash => {
            //                     if (slug_hash == item.slug_hash) {
            //                         isShowWebinar = true
            //                     }
            //                 })
            //             }
            //         }

            //     } else {
            //         isShowWebinar = true
            //     }

            //     if (isShowWebinar) {
            //         return (
            //             <View style={styles.viewCard}>
            //                 <CardItemWebinar navigation={this.props.navigation} data={item} isLive={true} />
            //             </View>
            //         )
            //     } else {
            //         return null
            //     }
            // }
        } else {
            return (
                <View style={styles.viewCard}>
                    <ListItemLearningSpecialist
                        navigation={this.props.navigation}
                        data={item}
                    />
                </View>
            );
        }
    };

    shownDateAnnouncement = (startDate) => {
        let result = " . Will Live";
        if (startDate != null) {
            let formatDate = "YYYY-MM-DD HH:mm:ss";
            if (this.state.country_code != "ID") {
                // get device timezone eg. -> "Asia/Shanghai"

                const deviceTimeZone = getLocalize().getTimeZone();
                console.log("cardItemWebinar deviceTimeZone: ", deviceTimeZone);

                // force set startDate to timezone Asia/Jakarta because in cms only set date as WIB
                let startDateWIB = moment.tz(startDate, "Asia/Jakarta").format();
                console.log("cardItemWebinar startDateWIB: ", startDateWIB);

                let localStartDate = moment(startDateWIB)
                    .tz(deviceTimeZone)
                    .format(formatDate);
                console.log("cardItemWebinar localStartDate: ", localStartDate);
                startDate = localStartDate;
            }

            result = " . Live " + moment(startDate, formatDate).fromNow();
        }

        return result;
    };

    _renderItemFooter = () => (
        <View
            style={{
                height: this.state.isFetching == true ? 80 : 0,
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            {this._renderItemFooterLoader()}
        </View>
    );

    _renderItemFooterLoader() {
        if (this.state.isFailed == true && this.page > 1) {
            return (
                <TouchableOpacity onPress={() => this.handleLoadMore()}>
                    <Icon name="ios-sync" style={{ fontSize: 42 }} />
                </TouchableOpacity>
            );
        }

        return <Loader visible={this.state.isFetching} transparent />;
    }

    _renderEmptyItem = () => (
        <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
    );

    _renderEmptyItemLoader() {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity
                    style={{
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: 20,
                    }}
                    onPress={() => this.handleLoadMore()}
                >
                    <Image
                        source={require("./../../assets/images/noinet.png")}
                        style={{ width: 72, height: 72 }}
                    />
                    <Text style={{ textAlign: "center" }}>
                        Something went wrong,{" "}
                        <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
                    </Text>
                </TouchableOpacity>
            );
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            if (this.state.feeds.length == 0) {
                return <Text style={{ textAlign: "center" }}>Data not found</Text>;
            }

            return null;
        }

        return <Text style={{ textAlign: "center" }}>Loading...</Text>;
    }

    gotoWebinarDetail = (isLive, itemWebinar) => {
        let dataWebinar =
            !isLive && itemWebinar != null ? itemWebinar : this.state.dataWebinarLive;
        let dateShowTNC = moment(dataWebinar.start_date)
            .add(-1, "hours")
            .format("YYYY-MM-DD HH:mm:ss");
        if (itemWebinar == null) {
            dataWebinar.server_date = this.state.server_date;
        }
        if (itemWebinar != null) {
            dataWebinar.isPaid =
                itemWebinar.exclusive == 1 || itemWebinar.isPaid == true ? true : false;
        }
        let isStartShowTNC = isStarted(dataWebinar.server_date, dateShowTNC);
        if (dataWebinar.has_tnc == true && isStartShowTNC == true) {
            this.props.navigation.dispatch(
                NavigationActions.navigate({
                    routeName: "WebinarTNC",
                    params: dataWebinar,
                })
            );
        } else {
            if (dataWebinar != null) {
                this.props.navigation.dispatch(
                    NavigationActions.navigate({
                        routeName: "WebinarVideo",
                        params: dataWebinar,
                        key: `webinar-${dataWebinar.id}`,
                    })
                );
            } else {
                Toast.show({
                    text: "Something went wrong, empty data webinar",
                    position: "bottom",
                    duration: 3000,
                });
            }
        }
    };

    gotoWebinarDetailFromWebinarLive = (isLive, itemWebinar) => {
        console.log("log webinartnc feeds 'itemwebinar'", itemWebinar);
        console.log(
            "log webinartnc feeds 'this.state.dataWebinarLive'",
            this.state.dataWebinarLive
        );
        let dataWebinar =
            !isLive && itemWebinar != null ? itemWebinar : this.state.dataWebinarLive;
        let dateShowTNC = moment(dataWebinar.start_date)
            .add(-1, "hours")
            .format("YYYY-MM-DD HH:mm:ss");
        if (itemWebinar == null) {
            dataWebinar.server_date = this.state.server_date;
            dataWebinar.isPaid =
                this.state.dataWebinarLive.exclusive == 1 ? true : false;
        }
        let isStartShowTNC = isStarted(dataWebinar.server_date, dateShowTNC);
        if (dataWebinar.has_tnc == true && isStartShowTNC == true) {
            this.props.navigation.dispatch(
                NavigationActions.navigate({
                    routeName: "WebinarTNC",
                    params: dataWebinar,
                })
            );
        } else {
            if (dataWebinar != null) {
                this.props.navigation.dispatch(
                    NavigationActions.navigate({
                        routeName: "WebinarVideo",
                        params: dataWebinar,
                        key: `webinar-${dataWebinar.id}`,
                    })
                );
            } else {
                Toast.show({
                    text: "Something went wrong, empty data webinar",
                    position: "bottom",
                    duration: 3000,
                });
            }
        }
    };

    gotoProfile = () => {
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: "Profile",
                key: `profile-123`,
            })
        );
    };

    async sendFCM(FCMToken) {
        this.setState({ isFailed: false, isFetching: true, hideFooter: false });

        try {
            console.log("fcm token", FCMToken);
            let response = await sendFIrebaseToken(FCMToken);

            if (response.isSuccess == true) {
                await AsyncStorage.setItem(STORAGE_TABLE_NAME.FCM_TOKEN, FCMToken);
                //this.updateProfile()
            }
        } catch (error) {
            Toast.show({
                text: "Something went wrong!",
                position: "top",
                duration: 3000,
            });
        }
    }

    toggleChecked = () => {
        this.setState({ isChecked: !this.state.isChecked });
    };
    render() {
        return (
            <Container>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showLoader}
                    onRequestClose={() => console.log("loader closed")}
                >
                    <Loader visible={this.state.showLoader} />
                </Modal>
                <View
                    style={{
                        width: wp("200%"),
                        height: wp("200%"),
                        borderRadius: wp("100%"),
                        backgroundColor: platform.toolbarDefaultBg,
                        position: "absolute",
                        left: -wp("50%"),
                        top: -wp("100%"),
                    }}
                />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow>
                        <Body>
                            <Title style={{ fontSize: 30 }}>Feeds</Title>
                        </Body>

                        {!this.state.isInternationalMode && (
                            <Right style={{ flex: 0.5 }}>
                                <Button
                                    // {...testID('buttonQRScan')}
                                    accessibilityLabel="button_pmm"
                                    onPress={() => this.gotoManageTemanDiabetes()}
                                    transparent
                                >
                                    <Icon
                                        type="Foundation"
                                        name="folder-add"
                                        nopadding
                                        style={styles.menus}
                                    />
                                </Button>
                                <Button
                                    // {...testID('buttonQRScan')}
                                    accessibilityLabel="button_qr_scan"
                                    onPress={() => this.gotoScanScreen()}
                                    transparent
                                >
                                    <Icon name="ios-qr-scanner" nopadding style={styles.menus} />
                                </Button>
                            </Right>
                        )}
                    </Header>
                </SafeAreaView>

                <View style={{ flex: 1, flexDirection: "column" }}>
                    {this.state.announcement && (
                        <TouchableOpacity
                            onPress={() =>
                                AdjustTracker(AdjustTrackerConfig.Feeds_Live) +
                                this.gotoWebinarDetailFromWebinarLive(true, null)
                            }
                            style={styles.bgndAnnouncement}
                        >
                            <Text style={styles.txtAnnouncement}>
                                {"Webinar : " + this.state.titleEventWebinar + " . Live"}
                            </Text>
                        </TouchableOpacity>
                    )}
                    {this.state.isNotCompletedProfile && !this.state.announcement && (
                        <TouchableOpacity
                            onPress={() =>
                                this.gotoProfile() +
                                AdjustTracker(AdjustTrackerConfig.Feeds_Pitakuning_Profile)
                            }
                            style={styles.bgndAnnouncement}
                        >
                            <Text style={styles.txtAnnouncement}>
                                Please complete your profile (CLICK HERE)
                            </Text>
                        </TouchableOpacity>
                    )}
                    <FlatList
                        contentContainerStyle={styles.flatlist}
                        data={this.state.feeds}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        onRefresh={() => this.onRefreshFeeds()}
                        refreshing={this.state.onRefresh}
                        renderItem={this._renderItem}
                        ListEmptyComponent={this._renderEmptyItem}
                        ListFooterComponent={this._renderItemFooter}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                <ReactModal
                    hasBackdrop={true}
                    avoidKeyboard={true}
                    backdropOpacity={0.8}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={600}
                    animationOutTiming={600}
                    backdropTransitionInTiming={600}
                    backdropTransitionOutTiming={600}
                    isVisible={this.state.showPopup}
                    onBackdropPress={this.handleCappingPopup}
                >
                    <ModalPopup
                        onPressClosePopup={this.handleCappingPopup}
                        // onPressClosePopup={() => this.setState({showPopup:false}, async () => {
                        //     // let popup = JSON.parse(await AsyncStorage.getItem('totalShowPopup'));
                        //     // popup.total_show = popup.total_show-1;
                        //     // await AsyncStorage.multiSet([['totalShowPopup', JSON.stringify(popup)], ['showPopup', 'false']]);
                        // })}
                        isChecked={this.state.isChecked}
                        onChecked={this.toggleChecked}
                        data={this.state.dataPopup}
                        handleSubmitPopup={this.handleSubmitPopup}
                        typeModal="privacy"
                    />
                </ReactModal>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 10,
    },
    flatlist: {
        // padding: 10,
        paddingBottom: 10,
    },
    viewCard: {
        paddingHorizontal: 10,
    },
    bgndAnnouncement: {
        flexDirection: "row",
        backgroundColor: "#EDCE4A",
        padding: 10,
        // marginBottom: 5,
    },
    txtAnnouncement: {
        fontWeight: "bold",
        color: "#454F63",
    },
});
