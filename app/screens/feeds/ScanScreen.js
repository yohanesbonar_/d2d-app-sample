import React, { Component } from 'react';
import { NavigationActions, StackActions } from 'react-navigation';
import { StatusBar, StyleSheet, Dimensions, View, BackHandler, SafeAreaView } from 'react-native';
import { Container, Body, Left, Button, Header, Content, Icon, Title } from 'native-base';

import QRCodeScanner from 'react-native-qrcode-scanner';
import platform from '../../../theme/variables/d2dColor';
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';

export default class ScanScreen extends Component {

  resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Home' })] });
  deviceHeight = platform.deviceHeight - platform.toolbarHeight;

  state = {
    showLoader: false,
    email: null,
    emailFailed: false,
    password: null,
    passwordFailed: false,
    passwordVisible: false
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    AdjustTracker(AdjustTrackerConfig.Scan_QR_Start);

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }


  onSuccess(e) {
    // Linking
    //   .openURL(e.data)
    //   .catch(err => console.error('An error occured', err));
    this.props.navigation.goBack();
    this.props.navigation.state.params.onScanQR(e.data);
    AdjustTracker(AdjustTrackerConfig.Scan_QR_Finish);

  }

  onBackPressed() {
    this.props.navigation.goBack();
  }

  _renderMarker = () => {
    return (
      <View>
        {/* <View style={styles.topOverlay}></View> */}
        {/* <View style={{ flexDirection: "row" }}>
                    <View style={styles.leftAndRightOverlay} /> */}
        <Icon name='ios-qr-scanner' style={styles.marker} />
        {/* <View style={styles.leftAndRightOverlay} />
                </View> */}
        {/* <View style={styles.bottomOverlay} /> */}
      </View>
    )
  }

  render() {
    const nav = this.props.navigation;

    return (
      <Container>
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header noShadow>
            <Left style={{ flex: 0.5 }}>
              <Button transparent onPress={() => this.onBackPressed()}>
                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
              </Button>
            </Left>

            <Body>
              <Title style={{ fontSize: 30 }}>Scan QR</Title>
            </Body>
          </Header>
        </SafeAreaView>
        <QRCodeScanner
          cameraStyle={{ height: this.deviceHeight }}
          onRead={this.onSuccess.bind(this)}
          showMarker
          customMarker={this._renderMarker()} />
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  marker: {
    fontSize: 300,
    color: '#ffffff'
  }
});
