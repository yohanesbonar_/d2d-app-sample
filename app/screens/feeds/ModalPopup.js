import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  Dimensions,
  ScrollView,
  Image,
} from "react-native";

import platform from "../../../theme/variables/d2dColor";
import {
  Content,
  Button,
  Text,
  Col,
  Item,
  Input,
  Row,
  Icon,
  Toast,
} from "native-base";
import { testID } from "../../libs/Common";
import AsyncStorage from "@react-native-community/async-storage";
import Api, { errorMessage } from "../../libs/Api";
import { Loader, MenuItemChecklist } from "./../../components";
import HTML from "react-native-render-html";
import { translate } from "../../libs/localize/LocalizeHelper";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import _ from "lodash";
import ButtonHighlight from "../../../src/components/atoms/ButtonHighlight";
export default class ModalPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      isInputName: false,
      namaGelar: "",
      showLoader: false,
      isLoading: false,
      isSuccesSubmit: false,
      img: {
        url: `https://public-v2links.adobecc.com/57ce3fa7-fdda-4c46-6003-731221e62a56/component?params=component_id%3Accaeb526-acd4-4fce-8a17-adc8a0e3161f&params=version%3A0&token=1581734955_da39a3ee_72d3e26c177829395177f448b4d7f36dc604a0cc&api_key=CometServer1`,
        width: 200,
        height: 200,
      },
      dataCheckboxes: this.props.data.checkboxes ? this.props.data.checkboxes : [],
      isActiveSubmit: false,
    };
    console.log("this props", this.props);
  }

  componentDidMount() {
    if (this.props.typeModal === "promo")
      this.onCalculateImageSize(this.state.img.url);

    if (_.isEmpty(this.state.dataCheckboxes)) {
      console.log("didmount ->", this.props);
      this.setState({ dataCheckboxes: this.props.data.checkboxes });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data != this.props.data) {
      if (_.isEmpty(this.state.dataCheckboxes)) {
        this.setState({ dataCheckboxes: this.props.data.checkboxes });
      }
    }
  }

  setAdjustToken = (type) => {
    if (this.props.typeModal == "privacy") {
      if (type == "close") {
        AdjustTracker(AdjustTrackerConfig.Homepage_Popup_TnC_Close);
      } else if (type == "submit") {
        AdjustTracker(AdjustTrackerConfig.Homepage_Popup_TnC_Submit);
      } else if (type == "not_interest") {
        AdjustTracker(AdjustTrackerConfig.Homepage_Popup_TnC_tidak_tertarik);
      }
    }
  };

  render() {
    return (
      <View style={{ justifyContent: "center" }} centerContent={true}>
        <Loader transparent={true} visible={this.state.showLoader} />

        <View style={{ padding: 0, position: "absolute", left: 0, right: 0 }}>
          {this.state.isLoading ? (
            <View
              style={{
                flex: 0,
                backgroundColor: "white",
                paddingHorizontal: 20,
                paddingVertical: this.state.isLoading === false ? 20 : 100,
                borderRadius: 10,
                zIndex: 1,
              }}
            />
          ) : (
            this.renderPopup()
          )}

          {this.state.isLoading === false &&
            this.state.typeModal != "confirmation" ? (
            <TouchableOpacity
              style={styles.popupClose}
              onPress={() => {
                this.props.onPressClosePopup();
                this.setAdjustToken("close");
              }}
            >
              <Text style={{ color: "#FFFFFF", marginTop: -2, fontSize: 18 }}>
                x
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    );
  }

  onCalculateImageSize = (url) => {
    let imageURL = url;
    let deviceWidth = (Dimensions.get("window").width * 90) / 105;
    let deviceHeight = (Dimensions.get("window").height * 50) / 105;

    // if (popupType == 'info') {
    //   deviceHeight = Dimensions.get('window').height * 90 / 100;
    // }

    Image.getSize(
      imageURL,
      (srcWidth, srcHeight) => {
        let ratio = Math.min(deviceWidth / srcWidth, deviceHeight / srcHeight);
        let img = this.state.img;
        img["width"] = srcWidth * ratio;
        img["height"] = srcHeight * ratio;
        this.setState({ img });
      },
      (error) => {
        console.warn(error);
      }
    );
  };

  renderPopup() {
    if (this.props.typeModal === "promo") return this.renderModalPromo();
    else return this.renderModalPrivacy();
  }

  onCheckedArray = (agreement_id) => {
    let data = this.state.dataCheckboxes;
    data.map((item, i) => {
      if (item.agreement_id == agreement_id) {
        if (item.isChecked == true) {
          item.isChecked = false;
        } else if (item.isChecked == false) {
          item.isChecked = true;
        } else if (_.isEmpty(item.isChecked)) {
          item.isChecked = true;
        }
      }
    });
    this.setState({ dataCheckboxes: [] });
    this.setState({ dataCheckboxes: data });

    this.checkSubmitActiveOrNo(data);
  };

  checkSubmitActiveOrNo = (data) => {
    var condition = true;
    data.map((item, i) => {
      if (item.is_required == 1 && !item.isChecked) {
        condition = false;
      }
    });
    this.setState({ isActiveSubmit: condition });
  };

  checkDataToSubmit = () => {
    let data = [];
    this.state.dataCheckboxes.forEach((item) => {
      data.push({
        agreement_id: item.agreement_id,
        status: item.isChecked ? "A" : "D",
      });
    });
    this.props.handleSubmitPopup(data);
  };

  renderModalPromo() {
    if (this.state.isLoading === false) {
      return (
        <View
          style={{
            flex: 0,
            backgroundColor: "white",
            borderRadius: 10,
            zIndex: 1,
          }}
        >
          <Image
            // source={{ uri: this.props.popupInfo.content.imageURL }}
            source={{ uri: this.state.img.url }}
            style={[styles.popupCard, { height: this.state.img.height }]}
            resizeMode="contain"
          />
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: this.state.isLoading === false ? 20 : 100,
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontSize: 16,
                color: "#262626D6",
                paddingBottom: 20,
              }}
            >
              Selamat menunaikan ibadah puasa dari Diabetasol. Kalian Bisa
              mendapatkan paket berpuasa dari kami dengan klik tombol “LANJUT”
              dibawah
            </Text>

            <TouchableOpacity
              style={[styles.popupPromoButton, { backgroundColor: "#EF434F" }]}
              accessibilityLabel={"touchPressPromo"}
              onPress={() => { }}
            >
              <Text style={styles.popupButtonText}>SUBMIT</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                paddingVertical: 10,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              accessibilityLabel={"touchIgnorePromo"}
              onPress={() => { }}
            >
              <Text
                style={{ textAlign: "center", color: "#757575", fontSize: 14 }}
              >
                saya tidak tertarik
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
  renderModalPrivacy() {
    let isChecked = this.props.isChecked;
    const { data } = this.props;
    console.log("data modalpopup ->>>", data);

    if (this.state.isLoading === false) {
      return (
        <View
          style={{
            flex: 0,
            backgroundColor: "white",
            paddingHorizontal: 20,
            paddingVertical: this.state.isLoading === false ? 20 : 100,
            borderRadius: 10,
            zIndex: 1,
          }}
        >
          <Text
            style={{
              textAlign: "center",
              marginTop: 35,
              fontSize: 18,
              fontWeight: "bold",
              color: "#262626D6",
              marginBottom: 5,
            }}
          >
            {data.title}
          </Text>
          <Text
            style={{
              textAlign: "center",
              fontSize: 16,
              color: "#262626D6",
            }}
          >
            {data.description}
          </Text>

          <View
            style={{
              backgroundColor: "rgba(120, 132, 158, 0.1)",
              borderRadius: 15,
              height: Dimensions.get("window").height / 2.5,
              marginTop: 15,
            }}
          >
            <ScrollView>
              <View
                style={{
                  paddingHorizontal: 20,
                  alignItems: "center",
                }}
              >
                {/* <Text style={{fontSize:16, fontWeight:'bold'}}>Privacy Policy</Text>
                                <Text style={{fontSize:14, paddingVertical:10}}>Selamat menunaikan ibadah puasa dari Diabetasol. Kalian Bisa mendapatkan paket berpuasa dari kami dengan klik tombol “LANJUT” dibawah. Selamat menunaikan ibadah puasa dari Diabetasol. Kalian Bisa mendapatkan paket berpuasa dari kami dengan klik tombol “LANJUT” dibawah. Selamat menunaikan ibadah puasa dari Diabetasol. Kalian Bisa mendapatkan paket berpuasa dari kami dengan klik tombol “LANJUT” dibawah. Selamat menunaikan ibadah puasa dari Diabetasol. Kalian Bisa mendapatkan paket berpuasa dari kami dengan klik tombol “LANJUT” dibawah</Text> */}

                <HTML
                  html={data.agreement}
                // imagesMaxWidth={Dimensions.get('window').width}
                />
              </View>
            </ScrollView>
          </View>

          {!_.isEmpty(data.checkboxes) && !_.isEmpty(data) ? (
            <View
              style={{
                flexDirection: "column",
                marginVertical: 12,
                marginRight: 20,
              }}
            >
              {!_.isEmpty(this.state.dataCheckboxes) && this.state.dataCheckboxes.map((item, i) => {
                return (
                  <View key={i}>
                    <Row style={{ alignItems: "center", marginVertical: 4 }}>
                      <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => this.onCheckedArray(item.agreement_id)}
                      >
                        <Icon
                          type={"MaterialCommunityIcons"}
                          name={
                            item.isChecked == true
                              ? "checkbox-marked"
                              : "checkbox-blank-outline"
                          }
                          style={{
                            fontSize: 24,
                            color:
                              item.isChecked == true ? "#04A84A" : "#454F63",
                          }}
                        />
                      </TouchableOpacity>
                      <Text style={{ fontSize: 13, marginLeft: 8 }}>
                        {item.confirmation}
                      </Text>
                    </Row>
                  </View>
                );
              })}
            </View>
          ) : null}

          {/* <Row style={{ alignItems: "center", marginVertical: 16 }}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.props.onChecked()}
              >
                <Icon
                  type={"MaterialCommunityIcons"}
                  name={
                    isChecked == true
                      ? "checkbox-marked"
                      : "checkbox-blank-outline"
                  }
                  style={{
                    fontSize: 24,
                    color: isChecked == true ? "#04A84A" : "#454F63",
                  }}
                />
              </TouchableOpacity>
              <Text style={{ fontSize: 13, marginLeft: 8 }}>
                {data.confirmation}
              </Text>
            </Row> */}

          <ButtonHighlight
                // {...testID('btnLanjut')}
                accessibilityLabel="btn_lanjut"
                text="SUBMIT"
                onPress={() => {
                  // this.props.handleSubmitPopup();
                  this.checkDataToSubmit();
                  this.setAdjustToken("submit");
                }}
                disabled={!this.state.isActiveSubmit}
                buttonColor={this.state.isActiveSubmit ? null : "#BAC3BE"}
                buttonUnderlayColor={this.state.isActiveSubmit ? null : "#D7D7D7"}
                additionalContainerStyle={{...styles.popupPromoButton, flex:1, height:null}}
                additionalTextStyle={{...styles.popupButtonText, lineHeight:null}}/>

          <TouchableOpacity
            style={{ paddingVertical: 10, paddingHorizontal: 15, marginTop: 5 }}
            accessibilityLabel={"touchIgnorePromo"}
            onPress={() => {
              // this.props.handleSubmitPopup();
              this.checkDataToSubmit();
              this.setAdjustToken("not_interest");
            }}
          >
            <Text
              style={{ textAlign: "center", color: "#757575", fontSize: 14 }}
            >
              {translate("popup_saya_tidak_tertarik")}
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  ///style modal
  popupClose: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#78849E",
    width: 35,
    height: 35,
    borderRadius: 17.5,
    position: "absolute",
    right: 20,
    top: 20,
    zIndex: 2,
  },
  popupPromoButton: {
    borderRadius: 3,
    paddingVertical: 15,
    paddingHorizontal: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.11,
    shadowRadius: 4.45,

    elevation: 6,
  },
  popupPromoLabel: {
    marginBottom: 7,
    color: "#757575",
    fontWeight: "bold",
  },
  popupButtonText: {
    textAlign: "center",
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 16,
  },
  popupPromoInput: {
    marginBottom: 10,
    backgroundColor: "#F0F6FA",
    borderRadius: 5,
    paddingHorizontal: 7,
    paddingVertical: 10,
    borderWidth: 1,
  },
  popupPromoResultContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  popupPromoResultImage: {
    marginTop: 30,
    width: 80,
    height: 80,
  },
  popupPromoResultTitle: {
    marginTop: 25,
    marginBottom: 10,
    color: "#757575",
    fontWeight: "bold",
    fontSize: 24,
    textAlign: "center",
  },
  popupPromoResultBody: {
    marginBottom: 30,
    color: "#757575",
    textAlign: "center",
  },
  popupCard: {
    ...Platform.select({
      android: { elevation: 5 },
      ios: {
        shadowColor: "rgba(0,0,0, .5)",
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.1,
        shadowRadius: 2.5,
      },
    }),
    position: "relative",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    marginTop: -2,
  },

  textMessageInputCertificate: {
    color: "#454F63",
    fontSize: 16,
    fontFamily: "Nunito-Bold",
    textAlign: "center",
    marginTop: 15,
    marginHorizontal: 25,
  },

  textMessageTerimakasih: {
    color: "#454F63",
    fontSize: 18,
    fontFamily: "Nunito-Bold",
    textAlign: "center",
    marginTop: 10,
    marginHorizontal: 10,
  },

  textLabel: {
    marginTop: 25,
    marginBottom: 5,
  },
  btnSubmit: {
    marginTop: 15,
    marginBottom: 10,
    borderRadius: 10,
    height: 55,
  },

  circleIcon: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 1,
    backgroundColor: platform.brandSuccess,
    borderColor: "#FFFFFF",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    shadowColor: "#00000029",
    shadowOpacity: 0.8,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0,
    },
  },
});
