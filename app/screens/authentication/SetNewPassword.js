import React, { Component } from "react";

import {
  StatusBar,
  StyleSheet,
  View,
  Modal,
  BackHandler,
  TouchableOpacity,
  Image,
  Linking,
  KeyboardAvoidingView,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Content,
  Button,
  Text,
  Item,
  Input,
  Label,
  Toast,
} from "native-base";
import { Loader } from "../../components";
import { testID, removeEmojis, validatePassword } from "../../libs/Common";
import platform from "../../../theme/variables/d2dColor";

import { doSetNewPassword } from "../../libs/NetworkUtility";
import { SafeAreaView } from "react-navigation";
import { AdjustConfig } from "react-native-adjust";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { CardValidationPassword } from "../../../src/components";

let clickCounter = 1;
export default class SetNewPassword extends Component {
  state = {
    showLoader: false,
    email: null,
    emailFailed: false,
    errorMessageEmail: "",
    password: null,
    passwordFailed: false,
    passwordVisible: false,
    confpassword: null,
    confpasswordFailed: false,
    confpasswordVisible: false,
    errorMessagePassword: "",
    errorMessageConfPassword: "",
    uid: this.props.navigation.state.params
      ? this.props.navigation.state.params.uid
      : "",
    passcode: this.props.navigation.state.params
      ? this.props.navigation.state.params.passcode
      : "",
    dataValidatePassword: {},
    isAvoidingPassword: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });

    this.didBlurListener = this.props.navigation.addListener(
      "didBlur",
      (payload) => {
        Linking.removeEventListener("url", this.handleOpenURL);
      }
    );

    this.didFocusListener = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        this.checkDeepLink();
      }
    );
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
    this.backHandler.remove();
    this.didBlurListener.remove();
    this.didFocusListener.remove();
    Linking.removeEventListener("url", this.handleOpenURL);
  }

  checkDeepLink() {
    Linking.getInitialURL()
      .then((url) => this.handleOpenURL({ url }))
      .catch((err) => console.error(err));

    //for app running in background
    Linking.addEventListener("url", this.handleOpenURL);
  }

  handleOpenURL = async (deeplink) => {
    console.log("handleOpenURL deeplink: ", deeplink);

    if (deeplink.url != null) {
      // "https://staging.d2d.co.id/changepassword/xEg56dpMD8OCfi0cH9YnivLw2I13/dVHcPs"
      if (deeplink.url.includes("d2d.co.id/changepassword")) {
        let splittedUrl = deeplink.url.split("/");

        let uid = splittedUrl[splittedUrl.length - 2];
        let passcode = splittedUrl[splittedUrl.length - 1];

        this.setState({
          uid: uid,
          passcode: passcode,
        });
      }
    }
  };

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  sendNewPassword = async () => {
    try {
      this.setState({ showLoader: true });
      let params = {
        uid: this.state.uid,
        passcode: this.state.passcode,
        password: this.state.password,
      };
      console.log("params sendNewPassword ->>>>>>>", params);
      let response = await doSetNewPassword(params);

      console.log("response sendNewPassword ->>>>>>>", response);

      if (response.isSuccess == true) {
        this.setState({ showLoader: false });

        Toast.show({
          text: response.message,
          position: "bottom",
          duration: 5000,
        });

        this.props.navigation.pop(3); //goto login
      } else {
        this.setState({ showLoader: false });
        Toast.show({
          text: response.message,
          position: "bottom",
          duration: 5000,
        });
      }
      clickCounter = 1;
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "bottom", duration: 5000 });
      clickCounter = 1;
    }
  };

  onPressConfirmation = async () => {
    AdjustTracker(AdjustTrackerConfig.Forgot_Password_Finish);
    if (clickCounter == 1) {
      clickCounter++;
      let isValid = false;
      await this.checkValidateSetNewPassword();
      let isPasswordValid =
        this.state.dataValidatePassword.charactersminimum &&
        this.state.dataValidatePassword.lowerCase &&
        this.state.dataValidatePassword.upperCase &&
        this.state.dataValidatePassword.numbers &&
        this.state.dataValidatePassword.specialCharacters
          ? true
          : false;

      isValid =
        !this.state.passwordFailed &&
        !this.state.confpasswordFailed &&
        isPasswordValid;

      if (isValid) {
        this.sendNewPassword();
      } else {
        clickCounter = 1;
      }
    }
  };

  checkValidateSetNewPassword = () => {
    if (!this.state.password) {
      this.setState({
        passwordFailed: true,
        errorMessagePassword: "Password is required",
      });
    } else {
      if (this.state.password.length < 8) {
        this.setState({
          passwordFailed: true,
          errorMessagePassword: "Password min. 8 character",
        });
      } else {
        this.setState({ passwordFailed: false });
      }
    }

    if (!this.state.confpassword) {
      this.setState({
        confpasswordFailed: true,
        errorMessageConfPassword: "Confirm password is required",
      });
    } else {
      if (this.state.confpassword != this.state.password) {
        this.setState({
          confpasswordFailed: true,
          errorMessageConfPassword: "Confirm password doesn't match",
        });
      } else {
        this.setState({ confpasswordFailed: false });
      }
    }
  };

  _visiblePassword = () => {
    this.setState({ passwordVisible: !this.state.passwordVisible });
  };

  _visibleconfPassword = () => {
    this.setState({ confpasswordVisible: !this.state.confpasswordVisible });
  };

  _checkValidatePW = (password) => {
    let dataVP = validatePassword(password);
    this.setState({
      dataValidatePassword: dataVP,
    });
  };

  _renderValidatePW = () => {
    return (
      <View style={{ marginTop: -8 }}>
        <CardValidationPassword
          isCharactersMinimum={
            this.state.dataValidatePassword.charactersminimum
          }
          isLowercase={this.state.dataValidatePassword.lowerCase}
          isUppercase={this.state.dataValidatePassword.upperCase}
          isNumber={this.state.dataValidatePassword.numbers}
          isSpecialCharacters={
            this.state.dataValidatePassword.specialCharacters
          }
        />
      </View>
    );
  };

  renderPassword() {
    return (
      <View
        style={{
          marginTop: 24,
        }}
      >
        <Label black style={styles.label}>
          New Password
        </Label>
        <Item
          noShadowElevation
          style={[styles.itemInput, { paddingRight: 0 }]}
          last
          error={this.state.passwordFailed}
        >
          <Input
            newDesign
            // {...testID('inputPassword')}
            accessibilityLabel="input_password"
            autoCapitalize="none"
            placeholder={"Enter your new password"}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) => {
              this.setState({ password: removeEmojis(txt).replace(/\s/g, "") });
              this._checkValidatePW(removeEmojis(txt).replace(/\s/g, ""));
            }}
            value={this.state.password}
            secureTextEntry={!this.state.passwordVisible}
            onFocus={() =>
              this.setState({
                isAvoidingPassword: true,
              })
            }
            onBlur={() =>
              this.setState({
                isAvoidingPassword: false,
              })
            }
          />
          <TouchableOpacity
            {...testID("showPassword")}
            onPress={() => this._visiblePassword()}
          >
            {this.state.passwordVisible ? (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-on.png")}
                style={{ width: 48, height: 48 }}
              />
            ) : (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-off.png")}
                style={{ width: 48, height: 48 }}
              />
            )}
          </TouchableOpacity>
        </Item>

        {this.state.passwordFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessagePassword}
          </Text>
        )}
        <Label black style={styles.label}>
          Confirm New Password
        </Label>
        <Item
          noShadowElevation
          last
          error={this.state.confpasswordFailed}
          style={[styles.itemInput, { paddingRight: 0 }]}
        >
          <Input
            newDesign
            // {...testID('inputConfirmationPassword')}
            accessibilityLabel="input_confirm_password"
            autoCapitalize="none"
            placeholder={"Enter your confirm new password"}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) =>
              this.setState({
                confpassword: removeEmojis(txt).replace(/\s/g, ""),
              })
            }
            value={this.state.confpassword}
            secureTextEntry={!this.state.confpasswordVisible}
          />
          <TouchableOpacity
            {...testID("showConfirmationPassword")}
            onPress={() => this._visibleconfPassword()}
          >
            {this.state.confpasswordVisible ? (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-on.png")}
                style={{ width: 48, height: 48 }}
              />
            ) : (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-off.png")}
                style={{ width: 48, height: 48 }}
              />
            )}
          </TouchableOpacity>
        </Item>
        {this.state.confpasswordFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessageConfPassword}
          </Text>
        )}
      </View>
    );
  }

  renderContent = () => {
    return (
      <Content
        style={{
          paddingHorizontal: 16,
          paddingVertical: 24,
        }}
      >
        <View
          style={{
            borderRadius: 8,
            backgroundColor: platform.backgroundInfo,
            padding: 16,
          }}
        >
          <Text
            regular
            style={{
              lineHeight: 26,
            }}
          >
            Create your new password. Make it easy, strong and hard to guess.
            Use at least 8 characters.
          </Text>
        </View>

        {this.renderPassword()}
        {this._renderValidatePW()}

        <Button
          style={styles.btnConfirmation}
          onPress={() => {
            this.state.dataValidatePassword.charactersminimum &&
            this.state.dataValidatePassword.lowerCase &&
            this.state.dataValidatePassword.upperCase &&
            this.state.dataValidatePassword.numbers &&
            this.state.dataValidatePassword.specialCharacters
              ? this.onPressConfirmation()
              : null;
          }}
          block
          disabled={
            this.state.dataValidatePassword.charactersminimum &&
            this.state.dataValidatePassword.lowerCase &&
            this.state.dataValidatePassword.upperCase &&
            this.state.dataValidatePassword.numbers &&
            this.state.dataValidatePassword.specialCharacters
              ? false
              : true
          }
        >
          <Text textButton>Confirmation</Text>
        </Button>
      </Content>
    );
  };

  render() {
    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showLoader}
          onRequestClose={() => console.log("loader closed")}
        >
          <Loader visible={this.state.showLoader} />
        </Modal>
        <SafeAreaView>
          <Header
            newDesign
            androidStatusBarColor={"#fff"}
            iosBarStyle={"dark-content"}
          >
            <Left newDesign>
              <TouchableOpacity
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Image
                  resizeMode={"contain"}
                  source={require("./../../assets/images/back.png")}
                  style={{ width: 48, height: 48 }}
                />
              </TouchableOpacity>
            </Left>
            <Body newDesign>
              <Title newDesign>Set New Password</Title>
            </Body>
          </Header>
        </SafeAreaView>
        {platform.platform == "ios" ? (
          <KeyboardAvoidingView
            style={{
              flex: 1,
            }}
            behavior={platform.platform == "ios" ? "padding" : "height"}
            keyboardVerticalOffset={
              platform.platform == "ios" && this.state.isAvoidingPassword
                ? -50
                : 20
            }
            enabled={
              this.state.isAvoidingPassword && platform.platform == "ios"
                ? true
                : false
            }
          >
            {this.renderContent()}
          </KeyboardAvoidingView>
        ) : (
          this.renderContent()
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  btnConfirmation: {
    marginTop: 8,
    marginBottom: 10,
  },
  itemInput: {
    marginBottom: 24,
  },
  label: {
    marginBottom: 12,
  },
  textError: {
    marginTop: -16,
    marginBottom: 16,
  },
});
