import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Modal, ImageBackground, TouchableOpacity, Platform, ScrollView, SafeAreaView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Form, Item, Input, Label, Toast, Card, CardItem } from 'native-base';
import { Loader } from './../../components';
import { validateEmail, STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import guelogin from './../../libs/GueLogin';
const gueloginAuth = firebase.app('guelogin');
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { translate } from './../../libs/localize/LocalizeHelper'

export default class RegisterHomeLocation extends Component {

    state = {
        specialistList: [],
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.setDummySpecilist();
    }

    setDummySpecilist = () => {
        let resultList = [];
        for (i = 1; i <= 7; i++) {

            dummySpList = [];
            for (a = 1; a <= 3; a++) {
                let dummySubSp = {
                    subsp_id: i,
                    title: 'Sub Specialist ' + a,
                    description: 'Specialist ' + a,
                    sp_id: i,
                };

                dummySpList.push(dummySubSp);
            }

            let dummySpecialist = {
                id: i,
                title: 'Specialist ' + i,
                description: 'Specialist ' + i,
                subspecialist: dummySpList,
            };

            resultList.push(dummySpecialist);
        }

        this.setState({ specialistList: resultList })
    }

    renderEmptySpecilist = () => {
        return (<View style={styles.viewEmpty}>
            <View style={{ flexDirection: 'column' }}>
                <Text style={styles.textSpesialis}>Spesialis</Text>
                <Item onPress={() => console.log('onclick')}
                    last style={styles.itemInput}>
                    <Icon name={'ios-person'} style={{ color: '#78849E' }} />
                    <Input
                        pointerEvents="none"
                        editable={false}
                        autoCapitalize="none"
                        placeholder={translate('hint_example_specialist')}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.setState({ password: txt })} />
                </Item>
            </View>
        </View>
        )
    }

    renderData = () => {
        return <View style={{ flex: 1, marginBottom: 10, }}>
            {
                <ScrollView>
                    {this.renderSpecialist()}
                </ScrollView>

            }
            <TouchableOpacity
                // {...testID('add')} 
                accessibilityLabel="add"
                style={{ padding: 10 }}>
                <Text style={styles.textAdd}>Tambah +</Text>
            </TouchableOpacity>
        </View>;
    }

    renderSpecialist = () => {
        let items = []
        let self = this;
        this.state.specialistList.map(function (d, i) {
            items.push(
                <Card style={{ paddingVertical: 5 }} key={i}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name={'ios-person'} style={styles.iconSpesialist} />
                        <View style={styles.viewTextNoborder}>
                            <Text>{d.title}</Text>
                            <TouchableOpacity
                                // {...testID('cancel')} 
                                accessibilityLabel="cancel"
                                onPress={() => self.doDeleteSpecialist(self, i)}
                                style={{ flex: 1, alignItems: 'flex-end', }}>
                                <Icon name={'ios-close-circle'} style={styles.iconCancel} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {self.renderSubSpecialist(d, i)}
                </Card>
            )
        });

        return items;
    }

    renderSubSpecialist = (specialist, indexSp) => {
        let items = []
        let self = this;
        if (specialist.subspecialist != null && specialist.subspecialist.length > 0) {
            specialist.subspecialist.map(function (d, i) {
                items.push(
                    <View style={{ flexDirection: 'row' }} key={i}>
                        <Icon name={'ios-person'} style={styles.iconSubSpecialist} />
                        <View style={styles.viewTextBorderTop}>
                            <Text>{d.title}</Text>
                            <TouchableOpacity
                                // {...testID('cancel')}
                                accessibilityLabel="cancel"
                                onPress={() => self.deleteSubSpecialist(self, indexSp, i)}
                                style={{ flex: 1, alignItems: 'flex-end', }}>
                                <Icon name={'ios-close-circle'} style={styles.iconCancel} />
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            })
        }

        return items;
    }

    doDeleteSpecialist = (self, index) => {
        self.state.specialistList.splice(index, 1);
        self.setState({ specialist: self.state.specialist, needSendSpesialzation: true })
    }

    deleteSubSpecialist = (self, indexSp, index) => {
        self.state.specialistList[indexSp].subspecialist.splice(index, 1);
        self.setState({ specialist: self.state.specialist, needSendSpesialzation: true })
    }

    render() {
        let btnColor = this.state.specialistList.length > 0 ? '#3AE194' : '#BAC3BE';

        return (
            <Container style={styles.backgroundColor}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow style={styles.backgroundColor}>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => null}>
                                <Icon name='md-arrow-back' style={styles.contentColor} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title style={styles.contentColor}>Input Your Data</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <View style={{ flex: 1, flexDirection: 'column', paddingHorizontal: 25 }}>
                    <View style={{ flex: 0.2, justifyContent: 'center' }}>
                        <Text style={styles.textTitle}>Apakah{`\n`}Spesialis Anda?</Text>
                    </View>
                    <View style={{ flex: 0.6 }}>
                        {this.state.specialistList.length <= 0 && this.renderEmptySpecilist()}
                        {this.state.specialistList.length > 0 && this.renderData()}
                    </View>
                    <View style={{ flex: 0.2 }}>
                        <View style={styles.btnNextRegister}>
                            <TouchableOpacity
                                // {...testID('btnLanjut')}
                                accessibilityLabel="btn_lanjut"
                                style={[styles.touchBtnRegister, { backgroundColor: btnColor }]} onPress={() => null} block rounded>
                                <Text style={styles.textNext}>LANJUT</Text>
                            </TouchableOpacity>
                            <View style={styles.viewGrey} />
                            <View style={styles.viewGreen} />
                        </View>
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: '#F7F7FA'
    },
    contentColor: {
        color: '#78849E'
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
    },
    touchBtnRegister: {
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: {
        width: '20%',
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    },
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
    },
    viewEmpty: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 25,
        marginTop: 10
    },
    textSpesialis: {
        color: '#78849E',
        margin: 10,
        fontSize: 16
    },
    itemInput: {
        backgroundColor: 'white',
        alignItems: 'center'
    },
    textAdd: {
        color: '#0EA2DD',
        textAlign: 'right'
    },
    iconSpesialist: {
        color: '#78849E',
        marginHorizontal: 15,
        fontSize: 45
    },
    iconSubSpecialist: {
        color: 'white',
        marginHorizontal: 15,
        fontSize: 45
    },
    viewTextNoborder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewTextBorderTop: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#D7DEE6'
    },
    iconCancel: {
        color: '#D7DEE6',
        marginRight: 10,
    }
});
