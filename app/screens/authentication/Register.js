import React, { Component } from "react";
import firebase from "react-native-firebase";
import { NavigationActions, StackActions } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  Modal,
  TouchableOpacity,
  BackHandler,
  Image,
  SafeAreaView,
  KeyboardAvoidingView,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Col,
  Item,
  Input,
  Label,
  Toast,
  Card,
  CardItem,
} from "native-base";
import { Loader } from "./../../components";
import {
  validateEmail,
  STORAGE_TABLE_NAME,
  testID,
  removeEmojis,
  validatePassword,
} from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
import guelogin from "./../../libs/GueLogin";
const gueloginAuth = firebase.app("guelogin");
import { doLoginApi, doRegisterApi } from "./../../libs/NetworkUtility";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";

import { AdjustTrackerConfig, AdjustTracker } from "./../../libs/AdjustTracker";
import CountrySelection from "../../libs/countries/CountrySelection";
import Geocoder from "react-native-geocoder";
import RNLocation from "react-native-location";
import country from "../../libs/countries/assets/data/country.json";
import { Modalize } from "react-native-modalize";
import { postLogin } from "../../../src/utils";
import {
  IconCambodiaFlag,
  IconIndonesiaFlag,
  IconMyanmarFlag,
  IconPhilippinesFlag,
  IconSingaporeFlag,
} from "../../../src/assets";
import { ButtonHighlight } from "../../../src/components";
import {
  IconCharactersDisabled,
  IconCharactersEnabled,
  IconLowerCaseDisabled,
  IconLowerCaseEnabled,
  IconNumbersEnabled,
  IconNumbersDisabled,
  IconSymbolsDisabled,
  IconSymbolsEnabled,
  IconUpperCaseDisabled,
  IconUpperCaseEnabled,
} from "../../../src/assets";
import { CardValidationPassword } from "../../../src/components";
import _ from "lodash";

export default class Register extends Component {
  state = {
    showLoader: false,
    name: null,
    nameFailed: false,
    npa: null,
    npaFailed: false,
    email: null,
    emailFailed: false,
    password: null,
    passwordFailed: false,
    passwordVisible: false,
    confpassword: null,
    confpasswordFailed: false,
    confpasswordVisible: false,
    buttonSubmit: "REGISTER",
    uid: null,
    selectedCountry: this.props.navigation.state.params.country
      ? this.props.navigation.state.params.country
      : country[0],
    longitude: null,
    latitude: null,
    errorMessageName: "",
    errorMessageEmail: "",
    errorMessagePassword: "",
    errorMessageConfPassword: "",
    dataValidatePassword: {},
    isAvoidingPassword: false,
    isAvoidingConfirmPassword: false,
  };

  constructor(props) {
    super(props);
  }

  getCurrentNameLocation = async (coordinate) => {
    console.log("getCurrentNameLocation coordinate: ", coordinate);

    Geocoder.geocodePosition(coordinate)
      .then((res) => {
        // res is an Array of geocoding object
        console.log("getCurrentNameLocation res: ", res);

        let resultCountry = country.filter((obj) => {
          return res[0].country.includes(obj.name);
        });

        console.log("result: ", resultCountry);
        if (typeof resultCountry[0] != "undefined") {
          this.setState({
            selectedCountry: resultCountry[0],
          });
        }
      })
      .catch((err) => console.log("getCurrentNameLocation error: ", err));
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }
  componentDidMount() {
    console.log(this.props.navigation.state.params.country);
    // const verifyEmail = {
    //   type: "Navigate",
    //   routeName: "RegisterVerification",
    //   params: {
    //     success: true,
    //     email: 'tes@Yopmail.com',
    //     uid: 'response.uid',
    //     name: 'response.name',
    //   },
    // };
    // this.props.navigation.navigate(verifyEmail);

    StatusBar.setHidden(false);

    // setTimeout(() => {
    //   RNLocation.requestPermission({
    //     ios: "whenInUse",
    //     android: {
    //       detail: "coarse",
    //     },
    //   }).then((granted) => {
    //     console.log("requestPermission granted: ", granted);
    //     if (granted) {
    //       RNLocation.configure({ distanceFilter: 0 });
    //       RNLocation.getLatestLocation({ timeout: 60000 }).then(
    //         (latestLocation) => {
    //           // Use the location here
    //           console.log("latestLocation: ", latestLocation);
    //           if (latestLocation != null) {
    //             let coordinate = {
    //               lat: latestLocation.latitude,
    //               lng: latestLocation.longitude,
    //             };
    //             this.getCurrentNameLocation(coordinate);
    //           }
    //         }
    //       );
    //     }
    //   });
    // }, 1000);

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  checkValidateRegistration = () => {
    let regexName = /^(?!\s)[A-Za-z.'’\s]+$/;
    let isNameValid = regexName.test(this.state.name);

    if (!this.state.name) {
      this.setState({
        nameFailed: true,
        errorMessageName: "Full name is required",
      });
    } else {
      if (isNameValid === false) {
        this.setState({
          nameFailed: true,
          errorMessageName: "Full name is invalid",
        });
      } else {
        if (this.state.name.trim().length < 3) {
          this.setState({
            nameFailed: true,
            errorMessageName: "Full name min. 3 character",
          });
        } else {
          this.setState({ nameFailed: false });
        }
      }
    }

    if (!this.state.email) {
      this.setState({
        emailFailed: true,
        errorMessageEmail: "Email address is required",
      });
    } else {
      if (validateEmail(this.state.email) === false) {
        this.setState({
          emailFailed: true,
          errorMessageEmail: "Invalid email format",
        });
      } else {
        this.setState({ emailFailed: false });
      }
    }

    if (!this.state.password) {
      this.setState({
        passwordFailed: true,
        errorMessagePassword: "Password is required",
      });
    } else {
      if (this.state.password.length < 8) {
        this.setState({
          passwordFailed: true,
          errorMessagePassword: "Password min. 8 character",
        });
      } else {
        this.setState({ passwordFailed: false });
      }
    }

    if (!this.state.confpassword) {
      this.setState({
        confpasswordFailed: true,
        errorMessageConfPassword: "Confirm password is required",
      });
    } else {
      if (this.state.confpassword != this.state.password) {
        this.setState({
          confpasswordFailed: true,
          errorMessageConfPassword: "Confirm password doesn't match",
        });
      } else {
        this.setState({ confpasswordFailed: false });
      }
    }
  };

  async registerPress() {
    const nav = this.props.navigation;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: "RegisterVerification" }),
      ],
    });

    AdjustTracker(AdjustTrackerConfig.Register_Register);

    let isSuccess = false;

    await this.checkValidateRegistration();

    let isPasswordValid =
      this.state.dataValidatePassword.charactersminimum &&
      this.state.dataValidatePassword.lowerCase &&
      this.state.dataValidatePassword.upperCase &&
      this.state.dataValidatePassword.numbers &&
      this.state.dataValidatePassword.specialCharacters
        ? true
        : false;

    if (
      !this.state.nameFailed &&
      !this.state.emailFailed &&
      !this.state.passwordFailed &&
      isPasswordValid &&
      !this.state.confpasswordFailed
    ) {
      isSuccess = true;
    }

    if (isSuccess === true) {
      this.setState({ showLoader: true });
      this.doRegisterUser();
    }
  }

  async doRegisterUser() {
    try {
      let params = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        confirm_password: this.state.confpassword,
        country_code: this.state.selectedCountry.country_code,
      };

      let response = await doRegisterApi(params);
      console.log("doRegisterUser response: ", response);
      if (response.isSuccess == true) {
        this.setState({ showLoader: false });

        params = {
          // success: response.docs.sendEmail && response.docs.registered,
          success: true,
          email: this.state.email,
        };

        this.props.navigation.replace("RegisterVerification", { ...params });
      } else {
        this.setState({ showLoader: false });
        if (
          // response.headers.reason != null &&
          response.headers != null &&
          this.state.selectedCountry.country_code == "ID"
        ) {
          Toast.show({
            text: response.headers.reason.ID,
            position: "top",
            duration: 5000,
          });
        } else if (
          // response.headers.reason != null &&
          response.headers != null &&
          this.state.selectedCountry.country_code != "ID"
        ) {
          Toast.show({
            text: response.headers.reason.EN,
            position: "top",
            duration: 5000,
          });
        } else {
          Toast.show({
            text: response.message ? response.message : "Something went wrong!",
            position: "top",
            duration: 5000,
          });
        }
      }
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({
        text: "Something went wrong!" + error,
        position: "top",
        duration: 5000,
      });
    }
  }

  async doLogin(email) {
    try {
      let user = gueloginAuth.auth().currentUser;
      // let response = await doLoginApi(email);
      let response = null;
      if (this.state.selectedCountry == "ID") {
        response = await postLogin(email);
      } else {
        response = await doLoginApi(email);
      }

      if (
        (response.acknowledge && this.state.selectedCountry == "ID") ||
        (response.isSuccess == true && this.state.selectedCountry != "ID")
      ) {
        let uid = null;
        if (this.state.selectedCountry == "ID") {
          console.warn(response.result.uid);
          uid = response.result.uid;
        } else {
          console.warn(response.data.uid);
          uid = response.data.uid;
        }
        if (typeof uid != "undefined") {
          let profile = null;
          if (this.state.country_code == "ID") {
            profile = response.result;
          } else {
            profile = response.data;
          }

          if (profile != null) {
            let jsonProfile = JSON.stringify(profile);
            let saveProfile = await AsyncStorage.setItem(
              STORAGE_TABLE_NAME.PROFILE,
              jsonProfile
            );
            let uid = await AsyncStorage.setItem(
              STORAGE_TABLE_NAME.UID,
              profile.uid
            );

            if (profile.npa_idi == null) {
              this.resetAction = StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: "Register",
                    params: user,
                  }),
                ],
              });
            } else if (
              profile.spesialization == null ||
              profile.spesialization.length == 0
            ) {
              // this.resetAction = NavigationActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Subscribtion', params: { type: 'subscription' } })] });
              this.resetAction = StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: "RegisterHello",
                    params: { profile },
                  }),
                ],
              });
            } else if (profile.spesialization.length > 0) {
              this.resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: "Home" })],
              });
            }
          }
        } else {
          this.resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: "Register",
                params: user,
              }),
            ],
          });
        }

        this.props.navigation.dispatch(this.resetAction);
      } else {
        this.setState({ showLoader: false });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "top", duration: 3000 });
    }
  }

  _visiblePassword = () => {
    this.setState({ passwordVisible: !this.state.passwordVisible });
  };

  _visibleconfPassword = () => {
    this.setState({ confpasswordVisible: !this.state.confpasswordVisible });
  };

  _onSelectCountry = (country) => {
    this.setState({
      selectedCountry: country,
    });
  };

  _renderSelectedFlag = () => {
    let countryName =
      this.state.selectedCountry == null
        ? "Indonesia"
        : this.state.selectedCountry.name;

    if (countryName == "Cambodia") {
      return (
        // <Image
        //   resizeMode={"contain"}
        //   source={require("./../../assets/images/flags/Cambodia.png")}
        //   style={{ width: 24, height: 24, marginRight: 12 }}
        // />
        <IconCambodiaFlag />
      );
    } else if (countryName == "Myanmar") {
      return <IconMyanmarFlag />;
    } else if (countryName == "Philippines") {
      return <IconPhilippinesFlag />;
    } else if (countryName == "Singapore") {
      return <IconSingaporeFlag />;
    } else {
      return <IconIndonesiaFlag />;
    }
  };

  _renderCountries = () => {
    console.log(
      "renderCountries selectedCountry: ",
      this.state.selectedCountry.name
    );
    let countryName =
      this.state.selectedCountry == null
        ? "Indonesia"
        : this.state.selectedCountry.name;

    return (
      <Col style={{ marginTop: 0 }}>
        <Label black style={styles.label}>
          Country
        </Label>
        <Item
          noShadowElevation
          {...testID("input_select_country")}
          //onPress={() => this.modalizeBottomSheet.open()}
          style={[
            styles.itemInput,
            { height: 56, alignItems: "center", paddingRight: 0 },
          ]}
        >
          <View style={{ marginRight: 12 }}>{this._renderSelectedFlag()}</View>

          <Text regular style={[{ fontFamily: "Roboto-Regular" }]}>
            {countryName}
          </Text>

          {/* <Right>

            <Image
              resizeMode={"contain"}
              source={require("./../../assets/images/arrow-down.png")}
              style={{ width: 48, height: 48 }}
            />
          </Right> */}
        </Item>
      </Col>
    );
  };

  _renderBottomSheetCountry = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        ref={(ref) => {
          this.modalizeBottomSheet = ref;
        }}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        HeaderComponent={
          <View
            style={{
              height: 4,
              borderRadius: 2,
              width: 40,
              marginTop: 8,
              backgroundColor: "#454F6329",
              alignSelf: "center",
            }}
          />
        }
      >
        <CountrySelection
          selected={
            this.state.selectedCountry == null
              ? "Indonesia"
              : this.state.selectedCountry.name
          }
          action={(item) =>
            this._onSelectCountry(item) +
            console.log("action: ", item) +
            this.modalizeBottomSheet.close()
          }
        />
      </Modalize>
    );
  };
  _renderEmail() {
    return this.state.buttonSubmit == "REGISTER" ? (
      <View>
        <Label black style={styles.label}>
          Email
        </Label>
        <Item
          noShadowElevation
          style={styles.itemInput}
          error={this.state.emailFailed}
        >
          <Input
            newDesign
            // {...testID('inputEmail')}
            accessibilityLabel="input_email"
            autoCapitalize="none"
            keyboardType="email-address"
            placeholder={"Enter your email"}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) => this.setState({ email: txt })}
          />
        </Item>
        {this.state.emailFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessageEmail}
          </Text>
        )}
      </View>
    ) : null;
  }

  _checkValidatePW = (password) => {
    let dataVP = validatePassword(password);
    this.setState({
      dataValidatePassword: dataVP,
    });
  };

  _renderValidatePW = () => {
    return (
      <View style={{ marginTop: -8 }}>
        <CardValidationPassword
          isCharactersMinimum={
            this.state.dataValidatePassword.charactersminimum
          }
          isLowercase={this.state.dataValidatePassword.lowerCase}
          isUppercase={this.state.dataValidatePassword.upperCase}
          isNumber={this.state.dataValidatePassword.numbers}
          isSpecialCharacters={
            this.state.dataValidatePassword.specialCharacters
          }
        />
      </View>
    );
  };

  _renderPassword() {
    return this.state.buttonSubmit == "REGISTER" ? (
      <View>
        <Label black style={styles.label}>
          Password
        </Label>
        <Item
          noShadowElevation
          style={[styles.itemInput, { paddingRight: 0 }]}
          last
          error={this.state.passwordFailed}
        >
          <Input
            newDesign
            // {...testID('inputPassword')}
            accessibilityLabel="input_password"
            autoCapitalize="none"
            placeholder={"Enter your password"}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) => {
              this.setState({ password: removeEmojis(txt).replace(/\s/g, "") });
              this._checkValidatePW(removeEmojis(txt).replace(/\s/g, ""));
            }}
            value={this.state.password}
            secureTextEntry={!this.state.passwordVisible}
            onFocus={() =>
              this.setState({
                isAvoidingPassword: true,
              })
            }
            onBlur={() =>
              this.setState({
                isAvoidingPassword: false,
              })
            }
          />
          <TouchableOpacity
            {...testID("showPassword")}
            onPress={() => this._visiblePassword()}
          >
            {this.state.passwordVisible ? (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-on.png")}
                style={{ width: 48, height: 48 }}
              />
            ) : (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-off.png")}
                style={{ width: 48, height: 48 }}
              />
            )}
          </TouchableOpacity>
        </Item>

        {this.state.passwordFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessagePassword}
          </Text>
        )}
        <Label black style={styles.label}>
          Confirm Password
        </Label>
        <Item
          noShadowElevation
          last
          error={this.state.confpasswordFailed}
          style={[styles.itemInput, { paddingRight: 0 }]}
        >
          <Input
            newDesign
            // {...testID('inputConfirmationPassword')}
            accessibilityLabel="input_confirm_password"
            autoCapitalize="none"
            placeholder={"Enter your confirm password"}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) =>
              this.setState({
                confpassword: removeEmojis(txt).replace(/\s/g, ""),
              })
            }
            value={this.state.confpassword}
            secureTextEntry={!this.state.confpasswordVisible}
            onFocus={() =>
              this.setState({
                isAvoidingConfirmPassword: true,
              })
            }
            onBlur={() =>
              this.setState({
                isAvoidingConfirmPassword: false,
              })
            }
          />
          <TouchableOpacity
            {...testID("showConfirmationPassword")}
            onPress={() => this._visibleconfPassword()}
          >
            {this.state.confpasswordVisible ? (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-on.png")}
                style={{ width: 48, height: 48 }}
              />
            ) : (
              <Image
                resizeMode={"contain"}
                source={require("./../../assets/images/eye-off.png")}
                style={{ width: 48, height: 48 }}
              />
            )}
          </TouchableOpacity>
        </Item>
        {this.state.confpasswordFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessageConfPassword}
          </Text>
        )}
      </View>
    ) : null;
  }

  logout() {
    const user = gueloginAuth.auth().currentUser;

    if (user) {
      gueloginAuth
        .auth()
        .signOut()
        .then(() => {
          let setLogin = async () =>
            await AsyncStorage.setItem("IS_LOGIN", "N");
          this.gotoTologin();
        })
        .catch((error) => {
          console.log(error);
          Toast.show({ text: error, position: "top", duration: 3000 });
          this.gotoTologin();
        });
    } else {
      let setLogin = async () => await AsyncStorage.setItem("IS_LOGIN", "N");
      this.gotoTologin();
    }
  }

  gotoTologin() {
    setTimeout(() => {
      this.resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Login" })],
      });
      this.props.navigation.dispatch(this.resetAction);
    }, 500);
  }

  renderContent = () => {
    return (
      <Content
        style={{
          backgroundColor: "#fff",
        }}
      >
        <View style={styles.content}>
          <Card noShadow transparent style={styles.card}>
            <View style={styles.form}>
              <Label black style={styles.label}>
                Full Name
              </Label>
              <Item
                noShadowElevation
                style={styles.itemInput}
                error={this.state.nameFailed}
              >
                <Input
                  newDesign
                  // {...testID('inputFullName')}
                  accessibilityLabel="input_fullname"
                  autoCapitalize="none"
                  placeholder={"Enter your name"}
                  // placeholder={'name must be appropriate with IDI'} // register with npa idi
                  value={this.state.name}
                  placeholderTextColor={platform.placeholderTextColor}
                  onChangeText={(txt) =>
                    this.setState({ name: removeEmojis(txt) })
                  }
                />
              </Item>
              {this.state.nameFailed && (
                <Text textError style={styles.textError}>
                  {this.state.errorMessageName}
                </Text>
              )}
              {this._renderEmail()}
              {this._renderCountries()}
              {this._renderPassword()}
              {this._renderValidatePW()}
              {/* <Button
                // {...testID('buttonRegister')}
                accessibilityLabel="button_register"
                style={styles.btnRegister}
                onPress={() =>
                  this.state.dataValidatePassword.charactersminimum &&
                  this.state.dataValidatePassword.lowerCase &&
                  this.state.dataValidatePassword.upperCase &&
                  this.state.dataValidatePassword.numbers &&
                  this.state.dataValidatePassword.specialCharacters
                    ? this.registerPress()
                    : null
                }
                block
                disabled={
                  this.state.dataValidatePassword.charactersminimum &&
                  this.state.dataValidatePassword.lowerCase &&
                  this.state.dataValidatePassword.upperCase &&
                  this.state.dataValidatePassword.numbers &&
                  this.state.dataValidatePassword.specialCharacters
                    ? false
                    : true
                }
              >
                <Text textButton>Submit</Text>
              </Button> */}

              <ButtonHighlight
                text="Submit"
                onPress={() =>
                  this.state.dataValidatePassword.charactersminimum &&
                  this.state.dataValidatePassword.lowerCase &&
                  this.state.dataValidatePassword.upperCase &&
                  this.state.dataValidatePassword.numbers &&
                  this.state.dataValidatePassword.specialCharacters
                    ? this.registerPress()
                    : null
                }
                disabled={
                  this.state.dataValidatePassword.charactersminimum &&
                  this.state.dataValidatePassword.lowerCase &&
                  this.state.dataValidatePassword.upperCase &&
                  this.state.dataValidatePassword.numbers &&
                  this.state.dataValidatePassword.specialCharacters
                    ? false
                    : true
                }
                additionalContainerStyle={{
                  ...styles.btnRegister,
                  borderRadius: 10,
                }}
                accessibilityLabel="button_register"
              />
            </View>
          </Card>
        </View>
      </Content>
    );
  };

  render() {
    const nav = this.props.navigation;
    return (
      <Container>
        {/* {this._renderBottomSheetCountry()} */}

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showLoader}
          onRequestClose={() => console.log("loader closed")}
        >
          <Loader visible={this.state.showLoader} />
        </Modal>
        <SafeAreaView>
          <Header
            newDesign
            androidStatusBarColor={"#fff"}
            iosBarStyle={"dark-content"}
          >
            <Left newDesign>
              <TouchableOpacity
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Image
                  resizeMode={"contain"}
                  source={require("./../../assets/images/back.png")}
                  style={{ width: 48, height: 48 }}
                />
              </TouchableOpacity>
            </Left>
            <Body newDesign>
              <Title newDesign>Register</Title>
            </Body>
          </Header>
        </SafeAreaView>
        {platform.platform == "ios" ? (
          <KeyboardAvoidingView
            style={{
              flex: 1,
            }}
            behavior={platform.platform == "ios" ? "padding" : "height"}
            keyboardVerticalOffset={
              platform.platform == "ios" &&
              this.state.isAvoidingPassword &&
              !this.state.passwordFailed &&
              !this.state.confpasswordFailed
                ? -50
                : platform.platform == "ios" &&
                  this.state.isAvoidingPassword &&
                  this.state.passwordFailed
                ? -30
                : platform.platform == "ios" &&
                  this.state.isAvoidingPassword &&
                  this.state.confpasswordFailed
                ? -30
                : platform.platform == "ios" &&
                  this.state.isAvoidingPassword &&
                  this.state.passwordFailed &&
                  this.state.confpasswordFailed
                ? -10
                : 20
            }
            enabled={
              this.state.isAvoidingPassword && platform.platform == "ios"
                ? true
                : false
            }
          >
            {this.renderContent()}
          </KeyboardAvoidingView>
        ) : (
          this.renderContent()
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    flex: 0,
    paddingTop: 24,
    paddingBottom: 0,
    paddingHorizontal: 16,
    borderColor: "transparent",
  },
  btnRegister: {
    marginTop: 8,
    marginBottom: 10,
  },
  itemInput: {
    marginBottom: 24,
  },
  label: {
    marginBottom: 12,
  },
  textError: {
    marginTop: -16,
    marginBottom: 16,
  },
});
