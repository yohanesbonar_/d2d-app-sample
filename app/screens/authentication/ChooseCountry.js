import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { NavigationActions, StackActions } from 'react-navigation';
import { StatusBar, StyleSheet, View, TouchableOpacity, Image, BackHandler } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Button, Icon, Text, Label, Col, Item, Content } from 'native-base';
import { testID, STORAGE_TABLE_NAME } from './../../libs/Common';
import guelogin from './../../libs/GueLogin';
const gueloginAuth = firebase.app('guelogin');
import AsyncStorage from '@react-native-community/async-storage';
import platform from "../../../theme/variables/d2dColor";
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import Geocoder from "react-native-geocoder";
import RNLocation from "react-native-location";
import country from "../../libs/countries/assets/data/country.json";
import { Modalize } from 'react-native-modalize';
import CountrySelection from "../../libs/countries/CountrySelection";
import { SafeAreaView } from 'react-native';
import { IconCambodiaFlag, IconIndonesiaFlag, IconMyanmarFlag, IconPhilippinesFlag, IconSingaporeFlag } from '../../../src/assets';
import ButtonHighlight from '../../../src/components/atoms/ButtonHighlight';

export default class ChooseCountry extends Component {

    //profile = country[0]
    timer = null

    state = {
        name: '',
        selectedCountry: country[0],
        isChooseCountry: false
    }

    constructor(props) {
        super(props);
    }


    getCurrentNameLocation = async (coordinate) => {
        console.log("getCurrentNameLocation coordinate: ", coordinate);

        Geocoder.geocodePosition(coordinate)
            .then((res) => {
                // res is an Array of geocoding object
                console.log("getCurrentNameLocation res: ", res);

                let resultCountry = country.filter((obj) => {

                    return res[0].countryCode.includes(obj.country_code);
                });

                console.log("result: ", resultCountry);
                if (typeof (resultCountry[0]) != 'undefined') {
                    this.setState({
                        selectedCountry: resultCountry[0],
                    });
                    //this.profile.country_code = resultCountry[0].country_code
                }
            })
            .catch((err) => console.log("getCurrentNameLocation error: ", err));
    };

    requestLocationPermission = () => {
        this.timer = setTimeout(() => {
            RNLocation.requestPermission({
                ios: "whenInUse",
                android: {
                    detail: "coarse",
                },
            }).then((granted) => {
                console.log("requestPermission granted: ", granted);
                if (granted) {
                    RNLocation.configure({ distanceFilter: 0 });
                    RNLocation.getLatestLocation({ timeout: 60000 }).then(
                        (latestLocation) => {
                            // Use the location here
                            console.log("latestLocation: ", latestLocation);
                            if (latestLocation != null) {

                                let coordinate = {
                                    lat: latestLocation.latitude,
                                    lng: latestLocation.longitude,
                                };
                                this.getCurrentNameLocation(coordinate);
                            }
                        }
                    );
                }
            });
        }, 1000);
    }

    componentDidMount() {
        StatusBar.setHidden(false);

        this.requestLocationPermission()
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
        clearTimeout(this.timer);

    }

    onBackPressed = () => {
        BackHandler.exitApp()
    }

    gotologin = async () => {
        let jsonProfile = JSON.stringify(this.state.selectedCountry);
        await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, jsonProfile);
        let country = {
            country: this.state.selectedCountry
        }
        const login = {
            type: "Navigate",
            routeName: "Login",
            params: country
        }
        this.props.navigation.navigate(login);

    }

    _onSelectCountry = (country) => {
        //this.profile = country

        this.setState({
            selectedCountry: country,
        });
    };

    _renderCountries = () => {
        let countryName =
            this.state.selectedCountry == null
                ? "Indonesia"
                : this.state.selectedCountry.name;

        return (
            <View style={{ width: '100%' }} >
                <Label black style={styles.label}>Country</Label>
                <Item
                    noShadowElevation
                    {...testID('input_select_country')}
                    onPress={() => {
                        this.modalizeBottomSheet.open();
                        AdjustTracker(AdjustTrackerConfig.Onboarding_Select_Country_Start)
                        clearTimeout(this.timer);
                    }}
                    style={[styles.itemInput, { height: 56, alignItems: "center", paddingRight: 0 }]}
                >

                    <View style={{ marginRight: 12 }}>{this._renderSelectedFlag()}</View>

                    <Text regular>{countryName}</Text>

                    <Right>

                        <Image
                            resizeMode={"contain"}
                            source={require("./../../assets/images/arrow-down.png")}
                            style={{ width: 48, height: 48 }}
                        />
                    </Right>
                </Item>
            </View>
        );
    };

    _renderSelectedFlag = () => {
        let countryName =
            this.state.selectedCountry == null
                ? "Indonesia"
                : this.state.selectedCountry.name;

        if (countryName == "Cambodia") {
            return (
                // <Image
                //   resizeMode={"contain"}
                //   source={require("./../../assets/images/flags/Cambodia.png")}
                //   style={styles.imageFlag}
                // />
                <IconCambodiaFlag />
            );
        } else if (countryName == "Myanmar") {
            return (
                <IconMyanmarFlag />
            );
        } else if (countryName == "Philippines") {
            return (
                <IconPhilippinesFlag />
            );
        } else if (countryName == "Singapore") {
            return (
                <IconSingaporeFlag />
            );
        } else {
            return (
                <IconIndonesiaFlag />
            );
        }
    };

    _renderBottomSheetCountry = () => {
        return (
            <Modalize
                withOverlay={true}
                withHandle={true}
                handleStyle={{
                    backgroundColor: 'transparent'
                }}

                adjustToContentHeight={true}

                ref={(ref) => {
                    this.modalizeBottomSheet = ref;
                }}
                modalStyle={{
                    borderTopLeftRadius: 16,
                    borderTopRightRadius: 16,
                    paddingHorizontal: 16,
                }}
                overlayStyle={{
                    backgroundColor: "#000000CC",
                }}
                HeaderComponent={
                    <View style={{
                        height: 4,
                        borderRadius: 2,
                        width: 40,
                        marginTop: 8,
                        backgroundColor: '#454F6329',
                        alignSelf: 'center'
                    }}>

                    </View>
                }
            >

                <CountrySelection
                    selected={
                        this.state.selectedCountry == null
                            ? "Indonesia"
                            : this.state.selectedCountry.name
                    }
                    action={(item) =>
                        this._onSelectCountry(item) +
                        console.log("action: ", item) +
                        this.modalizeBottomSheet.close()
                    }
                />
            </Modalize>
        )
    }

    renderChooseCountry = () => {
        return (
            <Container>

                {this._renderBottomSheetCountry()}
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow iosBarStyle={'dark-content'}>
                        <Left style={{ flex: 0.5 }}>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Choose Your Country</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <Content
                    contentContainerStyle={{
                        flexGrow: 1,
                        justifyContent: 'center'
                    }}
                    style={{
                        backgroundColor: '#fff',
                        paddingHorizontal: 16
                    }}>

                    <Col style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>

                        <Image
                            resizeMode='contain'
                            source={require('./../../assets/images/D2D-logo.png')} style={{ width: 82, height: 82, marginBottom: 32 }} />
                        <Text bold textLarge style={{ marginBottom: 16 }}>Welcome</Text>
                        <Text style={{ textAlign: 'center', lineHeight: 26, marginBottom: 32 }}>
                            <Text regular>Choose your country of residence. Please note, there will be some differences in the language and content provided in the application according to the country you have been chosen.</Text>
                        </Text>


                        {this._renderCountries()}


                        {/* <Button
                            {...testID('button_next')}
                            onPress={() => {
                                this.gotologin()
                                AdjustTracker(AdjustTrackerConfig.Onboarding_Select_Country_Finish)
                            }
                            }
                            block style={{ marginTop: 8 }}>
                            <Text textButton>NEXT</Text>
                        </Button> */}
                        <ButtonHighlight
                            {...testID('button_next')}
                            text="NEXT"
                            onPress={() => {
                                this.gotologin()
                                AdjustTracker(AdjustTrackerConfig.Onboarding_Select_Country_Finish)
                            }}
                            additionalContainerStyle={{marginTop: 8, borderRadius: 8, alignSelf:"stretch", height:48, flex:0}}
                            accessibilityLabel="button_next"/>
                    </Col>


                </Content>
            </Container>
        )
    }
    render() {
        return this.renderChooseCountry()
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: '#F7F7FA'
    },
    contentColor: {
        color: '#78849E'
    },
    icond2d: {
        width: 65,
        height: 65,
        marginBottom: 10
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
    },
    touchBtnRegister: {
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
        // backgroundColor: '#3AE194',
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: {
        width: '20%',
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    },
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
        marginBottom: 10
    },
    textDescription: {
        color: '#78849E',
        fontSize: 18,
        fontFamily: 'Nunito',
        marginBottom: 10,
    },
    itemInput: {
        marginBottom: 24,
        height: 56,
        alignItems: "center"
    },
    label: {
        marginBottom: 12
    }
});
