import React, { Component } from "react";
import firebase from "react-native-firebase";
import { NavigationActions, StackActions } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  BackHandler,
  ImageBackground,
  TouchableOpacity,
  Platform,
  ScrollView,
  SafeAreaView,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Form,
  Item,
  Input,
  Label,
  Toast,
  Card,
  CardItem,
} from "native-base";
import { Loader } from "./../../components";
import { validateEmail, STORAGE_TABLE_NAME, testID } from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
import guelogin from "./../../libs/GueLogin";
const gueloginAuth = firebase.app("guelogin");
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import {
  doSendCompetence,
  updateDataProfile,
} from "./../../libs/NetworkUtility";

import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { translate } from "../../libs/localize/LocalizeHelper";
import _ from "lodash";
import { Modalize } from "react-native-modalize";
import { BottomSheet } from "../../../src/components/molecules";
import { getAgreementWithType } from "../../../src/utils/network/agreement";
import ButtonHighlight from "../../../src/components/atoms/ButtonHighlight";
export default class RegisterPracticeLocation extends Component {
  state = {
    practiceLocList: [],
    showLoader: false,
    typeBottomSheet: "",
    titleBottomsheet: "",
    descBottomsheet: "",
  };

  constructor(props) {
    super(props);
  }

  profile = null;

  componentDidMount() {
    StatusBar.setHidden(false);
    this.getDataAgreement();
    this.getDataSubs();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  getDataAgreement = async () => {
    let typeAgreement = "registration_confirmation";

    try {
      response = await getAgreementWithType(typeAgreement);
      console.log(" response agreement -> ", response);
      let dataResult = response.result;
      if (response.acknowledge && !_.isEmpty(dataResult)) {
        this.setState({
          titleBottomsheet: dataResult.description,
          descBottomsheet: dataResult.agreement,
        });
      } else {
        let temp = !_.isEmpty(response.message) ? response.message : "";
        Toast.show({
          text: "Something went wrong! " + temp,
          position: "top",
          duration: 3000,
        });
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong! " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  getDataSubs = () => {
    this.profile = this.props.navigation.state.params;
    if (this.profile != null) {
      this.setState({ practiceLocList: this.profile.practiceLocations });
    }
  };

  gotoInputPracticeLocation = () => {
    if (this.profile == null) {
      console.log("profile null");
      return;
    }

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: "InputPracticeLocations",
        key: `gotoInputPracticeLoc`,
        params: {
          practiceLocList: this.profile.practiceLocations,
          saveDataPracticeLoc: this.updatePracticeLoc,
        },
      })
    );
  };

  updatePracticeLoc = (practiceLocList) => {
    this.profile.practiceLocations = practiceLocList;
    this.setState({ practiceLocList: practiceLocList });
  };

  renderEmptyPracticeLoc = () => {
    return (
      <View style={styles.viewEmpty}>
        <View style={{ flexDirection: "column" }}>
          <Text style={styles.textSpesialis}>Doctor practice</Text>
          <Item
            onPress={() => {
              // Adjust Tracker
              AdjustTracker(AdjustTrackerConfig.Onboarding_Practiceloc);
              this.gotoInputPracticeLocation();
            }}
            last
            style={styles.itemInput}
          >
            <Icon
              name={"hospital-o"}
              type={"FontAwesome"}
              style={{ color: "#78849E" }}
            />
            <Input
              pointerEvents="none"
              editable={false}
              autoCapitalize="none"
              placeholder={translate("placeholder_short_practice_location")}
              placeholderTextColor={platform.placeholderTextColor}
              onChangeText={(txt) => this.setState({ password: txt })}
            />
          </Item>
        </View>
      </View>
    );
  };

  renderData = () => {
    return (
      <View style={{ flex: 1, marginBottom: 10 }}>
        {<ScrollView>{this.renderPracticeLoc()}</ScrollView>}
        <TouchableOpacity
          onPress={() => this.gotoInputPracticeLocation()}
          // {...testID('add')}
          accessibilityLabel="add"
          style={{ padding: 10 }}
        >
          <Text style={styles.textAdd}>Add +</Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderPracticeLoc = () => {
    let items = [];
    let self = this;
    this.state.practiceLocList.map(function(d, i) {
      items.push(
        <Card style={{ paddingVertical: 5 }} key={i}>
          <View style={{ flexDirection: "row", marginVertical: 5 }}>
            <Icon
              name={"hospital-o"}
              type={"FontAwesome"}
              style={styles.iconSpesialist}
            />
            <View style={styles.viewTextNoborder}>
              <Text
                style={{
                  flex: 0.85,
                  color: "#78849E",
                  fontSize: 16,
                  marginTop: 5,
                }}
              >
                {d}
              </Text>
              <TouchableOpacity
                // {...testID('cancel')}
                accessibilityLabel="cancel"
                onPress={() => self.doDeleteSubscribtion(self, i)}
                style={{ flex: 0.15, alignItems: "flex-end" }}
              >
                <Icon name={"ios-close-circle"} style={styles.iconCancel} />
              </TouchableOpacity>
            </View>
          </View>
        </Card>
      );
    });

    return items;
  };

  doDeleteSubscribtion = (self, index) => {
    self.state.practiceLocList.splice(index, 1);
    self.setState({ practiceLocList: self.state.practiceLocList });
  };

  openBSConfirm = () => {
    if (
      this.state.practiceLocList != null &&
      this.state.practiceLocList.length > 0
    ) {
      this.setState({ typeBottomSheet: "BottomSheetRegisterConfirmation" });
      this.onOpen();
    }
  };

  doSendSpesializations = async () => {
    try {
      this.setState({ showLoader: true });

      let response = await doSendCompetence(
        this.profile.specialist,
        "spesialization"
      );

      if (response.isSuccess) {
        this.doSendSubscriptions();
      } else {
        Toast.show({
          text: response.message,
          position: "top",
          buttonText: "Okay",
        });
        this.setState({ showLoader: false });
      }
    } catch (error) {
      Toast.show({ text: error, position: "top", buttonText: "Okay" });
      this.setState({ showLoader: false });
    }
  };

  doSendSubscriptions = async () => {
    try {
      // let response = await doSendCompetence(this.profile.subscribtions, 'subscription');
      let response = await doSendCompetence(
        this.profile.specialist,
        "subscription"
      );

      if (response.isSuccess) {
        this.doUpdateProfile();
      } else {
        Toast.show({ text: error, position: "top", buttonText: "Okay" });
        this.setState({ showLoader: false });
      }
    } catch (error) {
      this.setState({ showLoader: false });
    }
  };

  doUpdateProfile = async () => {
    try {
      let params = {
        country_code: this.profile.country_code,
        uid: this.profile.uid,
        name: this.profile.name,
        email: this.profile.email,
        npa_idi: !_.isEmpty(this.profile.npaIdi) ? this.profile.npaIdi : "",
        phone: "",
        pns: "",
        born_date: "",
        education_1: "",
        education_2: "",
        education_3: "",
        home_location: "",
        clinic_location_1: this.profile.practiceLocations[0],
        clinic_location_2:
          this.profile.practiceLocations.length > 1
            ? this.profile.practiceLocations[1]
            : "",
        clinic_location_3:
          this.profile.practiceLocations.length > 2
            ? this.profile.practiceLocations[2]
            : "",
      };

      let response = await updateDataProfile(params);
      console.log("update profile", response);
      this.setState({ showLoader: false });

      if (response.isSuccess == true) {
        this.gotoRegisterSuccess(this.profile);
      } else {
        Toast.show({
          text: response.message,
          position: "top",
          buttonText: "Okay",
        });
      }
    } catch (error) {
      Toast.show({ text: error, position: "top", buttonText: "Okay" });
      this.setState({ showLoader: false });
    }
  };

  gotoRegisterSuccess = (profile) => {
    this.resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: "RegisterSuccess",
          params: { profile },
        }),
      ],
    });
    this.props.navigation.dispatch(this.resetAction);
  };

  onOpen = () => {
    // modalizeRef.current.open();
    this.modalizeBSRef.open();
  };

  onClose = () => {
    // modalizeRef.current.close();
    this.modalizeBSRef.close();
  };

  validateDataPraocticeLoc = () => {
    if (
      this.state.practiceLocList != null &&
      this.state.practiceLocList.length > 0
    ) {
      this.doSendSpesializations();
    }
  };

  _renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        ref={(ref) => {
          this.modalizeBSRef = ref;
        }}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        // onClose={() => onClose()}
        HeaderComponent={<View style={styles.bottomSheetHeader} />}
      >
        {this.state.typeBottomSheet == "BottomSheetRegisterConfirmation" ? (
          <BottomSheet
            onPressCancel={() => this.onClose()}
            onPressOk={() => {
              this.onClose();
              this.validateDataPraocticeLoc();
            }}
            title={this.state.titleBottomsheet}
            desc={this.state.descBottomsheet}
            wordingCancel="BACK"
            wordingOk="CONFIRMATION"
            type="BottomSheetRegisterConfirmation"
          />
        ) : null}
      </Modalize>
    );
  };

  render() {
    let btnColor =
      this.state.practiceLocList.length > 0 ? "#3AE194" : "#BAC3BE";

    return (
      <Container style={styles.backgroundColor}>
        <Loader visible={this.state.showLoader} />
        <SafeAreaView>
          <Header noShadow style={styles.backgroundColor}>
            <Left style={{ flex: 0.5 }}>
              <Button
                // {...testID('buttonBack')}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={styles.contentColor} />
              </Button>
            </Left>
            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Title style={styles.contentColor}>Input Your Data</Title>
            </Body>
            <Right style={{ flex: 0.5 }} />
          </Header>
        </SafeAreaView>
        {this._renderBottomSheet()}
        <View
          style={{ 
            flex: 1, 
            flexDirection: "column", 
            paddingHorizontal: 25,
            marginBottom:
              platform.platform == "ios"
                ? Platform.isPad
                  ? 34
                  : platform.deviceHeight >= 812 &&
                    platform.deviceWidth >= 375
                  ? 34
                  : 8
                : 8,
            paddingBottom: 16, 
        }}>
          <View style={{ flex: 0.2, justifyContent: "center" }}>
            <Text style={styles.textTitle}>
              Where's your{`\n`}practice location?
            </Text>
          </View>
          <View style={{ flex: 0.8 }}>
            {this.state.practiceLocList.length <= 0 &&
              this.renderEmptyPracticeLoc()}
            {this.state.practiceLocList.length > 0 && this.renderData()}
          </View>
          <View style={{ height:86 }}>
            <View style={styles.btnNextRegister}>
              <ButtonHighlight
                  // {...testID('btnLanjut')}
                  accessibilityLabel="btn_lanjut"
                  text="NEXT"
                  buttonColor={this.state.practiceLocList.length > 0 ? null : "#BAC3BE"}
                  buttonUnderlayColor={this.state.practiceLocList.length > 0 ? null : "#D7D7D7"}
                  onPress={() => {
                    !_.isEmpty(this.state.titleBottomsheet) &&
                    !_.isEmpty(this.state.descBottomsheet)
                      ? this.openBSConfirm()
                      : null;
                  }}
                  additionalContainerStyle={{...styles.touchBtnRegister, paddingVertical:null, paddingHorizontal:null, flex:0, height:null}}
                  additionalTextStyle={{...styles.textNext, lineHeight:null}}/>
              <View style={styles.viewGrey} />
              <View
                style={styles.viewGreen(
                  this.props.navigation.state.params.country_code
                )}
              />
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  backgroundColor: {
    backgroundColor: "#F7F7FA",
  },
  contentColor: {
    color: "#78849E",
  },
  btnNextRegister: {
    backgroundColor: "#F7F7FA",
    flexDirection: "column",
    flex: 1,
    height: "100%",
  },
  touchBtnRegister: {
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    padding: 15,
  },
  textNext: {
    fontFamily: "Nunito-Bold",
    color: "white",
    fontSize: 18,
  },
  viewGrey: {
    backgroundColor: "#EAEAEA",
    height: 12,
    borderRadius: 15,
  },
  viewGreen: (country_code) => [
    {
      width: country_code != "PH" ? "83.3333333333%" : "80%",
      top: -12,
      backgroundColor: "#3AE194",
      height: 12,
      borderRadius: 15,
    },
  ],
  textTitle: {
    color: "#78849E",
    fontSize: 30,
    fontFamily: "Nunito-Bold",
  },
  viewEmpty: {
    justifyContent: "flex-end",
    flex: 1,
    marginBottom: 25,
    marginTop: 10,
  },
  textSpesialis: {
    color: "#78849E",
    margin: 10,
    fontSize: 16,
  },
  itemInput: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  textAdd: {
    color: "#0EA2DD",
    textAlign: "right",
    fontSize: 18,
    fontFamily: "Nunito-Bold",
  },
  iconSpesialist: {
    color: "#78849E",
    marginHorizontal: 15,
    marginTop: 5,
    fontSize: 22,
  },
  viewTextNoborder: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  viewTextBorderTop: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderTopWidth: 2,
    borderTopColor: "#D7DEE6",
  },
  iconCancel: {
    color: "#D7DEE6",
    marginRight: 10,
    marginTop: 5,
    fontSize: 24,
  },
  bottomSheetHeader: {
    height: 4,
    borderRadius: 2,
    width: 40,
    marginTop: 8,
    backgroundColor: "#454F6329",
    alignSelf: "center",
  },
});
