import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, BackHandler, KeyboardAvoidingView, TouchableOpacity, Platform, ScrollView, SafeAreaView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Form, Item, Input, Label, Toast, Card, CardItem } from 'native-base';
import { Loader } from './../../components';
import { validateEmail, STORAGE_TABLE_NAME, testID, removeEmojis } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import guelogin from './../../libs/GueLogin';
const gueloginAuth = firebase.app('guelogin');
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { translate } from '../../libs/localize/LocalizeHelper'
import ButtonHighlight from '../../../src/components/atoms/ButtonHighlight';

export default class InputPraticeLocations extends Component {

    state = {
        practiceLocList: [],
        showErrorInput: false,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.initPracticeLoc();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    componentWillMount() {
        this.setState({ practiceLocList: [''] })
    }

    initPracticeLoc = () => {
        let { params } = this.props.navigation.state;
        if (params != null) {
            if (params.practiceLocList != null && params.practiceLocList.length > 0) {
                let purePrctLoc = [];
                params.practiceLocList.map(function (d, i) {
                    purePrctLoc.push(d);
                });

                if (params.practiceLocList.length < 3) {
                    purePrctLoc.push('');
                }

                this.setState({ practiceLocList: purePrctLoc })
            }
        }
    }

    renderData = () => {
        return <View style={{ flex: 1, marginVertical: 10, }}>
            {

                <KeyboardAwareScrollView
                    extraScrollHeight={-100}
                >
                    {this.renderPracticeLoc()}

                    {this.state.practiceLocList.length < 3 && <TouchableOpacity onPress={() => this.addCardInput()}
                        // {...testID('add')} 
                        accessibilityLabel="add"
                        style={{ padding: 10 }} >
                        <Text style={styles.textAdd}>Add +</Text>
                    </TouchableOpacity>}

                </KeyboardAwareScrollView>
            }
        </View>;
    }

    isEmptyCard = () => {
        let isError = false;

        if (this.state.practiceLocList.length > 0) {

            for (i = 0; i < this.state.practiceLocList.length; i++) {
                if (this.state.practiceLocList[i] == '') {
                    isError = true;
                    break;
                }
            }
        }

        return isError;
    }


    addCardInput = () => {
        // Adjust Tracker
        AdjustTracker(AdjustTrackerConfig.Onboarding_Practiceloc_Add);

        this.setState({ showErrorInput: false })
        if (this.isEmptyCard() == false) {
            let temporaryPL = this.state.practiceLocList;
            temporaryPL.push('');
            this.setState({ practiceLocList: temporaryPL });
        } else {
            this.setState({ showErrorInput: true })
        }
    }

    onTextChanged = (text, index) => {
        let temporaryPL = this.state.practiceLocList;
        temporaryPL[index] = removeEmojis(text);
        this.setState({ practiceLocList: temporaryPL });
    }

    savePracticeLoc = () => {
        // Adjust Tracker
        AdjustTracker(AdjustTrackerConfig.Onboarding_Practiceloc_Save);
        this.setState({ showErrorInput: false })
        if (this.isEmptyCard() == false) {
            this.props.navigation.goBack();
            this.props.navigation.state.params.saveDataPracticeLoc(this.state.practiceLocList);
        } else {
            this.setState({ showErrorInput: true })
        }
    }

    renderPracticeLoc = () => {
        if (this.state.practiceLocList != null && this.state.practiceLocList.length > 0) {
            let items = []
            let self = this;
            let practiceLocSize = this.state.practiceLocList.length;
            this.state.practiceLocList.map(function (d, i) {
                items.push(
                    <Card style={{ paddingVertical: 20, paddingHorizontal: 20 }} key={i}>
                        <View>
                            <Label style={{ marginBottom: 10 }}>Practice Location {practiceLocSize > 1 ? i + 1 : ''}</Label>
                            <Item error={self.state.showErrorInput && d == ''}>
                                <Input
                                    multiline={true}
                                    numberOfLines={4}
                                    style={{ minHeight: platform.deviceHeight / 4.5 }}
                                    // {...testID('inputClinic1')}
                                    accessibilityLabel="input_clinic1"
                                    autoCapitalize="none"
                                    placeholder={translate("placeholder_practice_location")}
                                    value={d}
                                    placeholderTextColor={platform.placeholderTextColor}
                                    onChangeText={(txt) => self.onTextChanged(txt, i)}
                                    onFocus={() => AdjustTracker(AdjustTrackerConfig.Onboarding_Practiceloc_Freetext)} />
                            </Item>
                        </View>
                        {practiceLocSize > 1 && <TouchableOpacity style={{ position: 'absolute', right: 0, top: 0, marginTop: 10, marginRight: 5 }}
                            // {...testID('cancel')}
                            accessibilityLabel="cancel"
                            onPress={() => self.doDeleteSubscribtion(self, i)} >
                            <Icon name={'ios-close-circle'} style={styles.iconCancel} />
                        </TouchableOpacity>}
                    </Card>
                )
            });

            return items;
        }
        // else {
        //     this.setState({ practiceLocList: [''] })
        // }
    }

    doDeleteSubscribtion = (self, index) => {
        self.state.practiceLocList.splice(index, 1);
        self.setState({ practiceLocList: self.state.practiceLocList })
    }

    render() {
        let btnColor = this.state.practiceLocList.length > 0 ? '#3AE194' : '#BAC3BE';

        return (
            <Container style={styles.backgroundColor}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                <Header noShadow>
                    <Left style={{ flex: 0.5 }}>
                        <Button
                            // {...testID('buttonBack')}
                            accessibilityLabel="button_back"
                            transparent onPress={() => this.onBackPressed()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>Practice Location</Title>
                    </Body>
                    <Right style={{ flex: 0.5 }} />
                </Header>
                </SafeAreaView>

                <View style={{ 
                    flex: 1, 
                    flexDirection: 'column', 
                    paddingHorizontal: 10, 
                    marginBottom:
                        platform.platform == "ios"
                        ? Platform.isPad
                            ? 34
                            : platform.deviceHeight >= 812 &&
                            platform.deviceWidth >= 375
                            ? 34
                            : 8
                        : 8,
                    paddingBottom: 16 }}>
                    <View style={{ flex: 1 }}>
                        {this.renderData()}
                    </View>
                    <View style={{ height:55 }}>
                        <View style={styles.btnNextRegister}>
                            <ButtonHighlight
                                // {...testID('btnLanjut')}
                                accessibilityLabel="btnLanjut"
                                text="SAVE"
                                buttonColor={this.state.practiceLocList.length > 0 ? null : "#BAC3BE"}
                                buttonUnderlayColor={this.state.practiceLocList.length > 0 ? null : "#D7D7D7"}
                                onPress={() => this.savePracticeLoc()}
                                additionalContainerStyle={{...styles.touchBtnRegister, paddingVertical:null, paddingHorizontal:null, height:null}}
                                additionalTextStyle={{...styles.textNext, lineHeight:null}}/>
                            {/* <View style={styles.viewGrey} />
                            <View style={styles.viewGreen} /> */}
                        </View>
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: '#F7F7FA'
    },
    contentColor: {
        color: '#78849E'
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
    },
    touchBtnRegister: {
        // marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: {
        width: '20%',
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    },
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
    },
    viewEmpty: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 25,
        marginTop: 10
    },
    textSpesialis: {
        color: '#78849E',
        margin: 10,
        fontSize: 16
    },
    itemInput: {
        backgroundColor: 'white',
        alignItems: 'center'
    },
    textAdd: {
        color: '#0EA2DD',
        textAlign: 'right',
        fontSize: 18,
        fontFamily: 'Nunito-Bold',
    },
    iconSpesialist: {
        color: '#78849E',
        marginHorizontal: 15,
        fontSize: 45
    },
    viewTextNoborder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewTextBorderTop: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#D7DEE6'
    },
    iconCancel: {
        color: '#D7DEE6',
        marginRight: 10,
    }
});
