import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  View,
  Modal,
  Keyboard,
  BackHandler,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Content,
  Button,
  Text,
  Item,
  Input,
  Label,
  Toast,
} from "native-base";
import { Loader } from "./../../components";
import { validateEmail, testID } from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";

import { doForgotPassword } from "./../../libs/NetworkUtility";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { forgotPassword, getData, KEY_ASYNC_STORAGE } from "../../../src/utils";

export default class ForgotPassword extends Component {
  state = {
    showLoader: false,
    email: null,
    emailFailed: false,
    errorMessageEmail: "",
    country_code: "",
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getDataCountryCode();
    StatusBar.setHidden(false);
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
    AdjustTracker(AdjustTrackerConfig.Forgot_Password_Start);
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      console.log("country_code -> ", profile.country_code);
      this.setState({
        country_code: profile.country_code,
      });
    });
  };

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async forgotPassword() {
    Keyboard.dismiss();

    let emailFailed = true;

    if (this.state.email) {
      if (validateEmail(this.state.email) === true) {
        emailFailed = false;
      } else {
        this.setState({
          emailFailed: true,
          errorMessageEmail: "Email address is invalid",
        });
      }
    } else {
      this.setState({
        emailFailed: true,
        errorMessageEmail: "Email address is required",
      });
    }

    if (emailFailed == false) {
      this.sendEmailForgotPass();
    } else {
      this.setState({ emailFailed: emailFailed });
    }
  }

  async sendEmailForgotPass() {
    try {
      this.setState({ showLoader: true });
      let params = {
        email: this.state.email,
      };

      let response = await forgotPassword(params);
      console.log("response doForgotPassword ->", response);

      if (response.acknowledge) {
        this.setState({ showLoader: false, errorMessageEmail: "" });
        const forgotConfirm = {
          type: "Navigate",
          routeName: "ForgotPasswordConfirmation",
          params: { success: true, email: this.state.email },
        };
        this.props.navigation.navigate(forgotConfirm);

        Toast.show({
          buttonText: "Close",
          text: response.message,
          position: "bottom",
          duration: 5000,
        });
      } else {
        if (response.header && response.header.code && response.header.reason) {
          let message = "";
          if (this.state.country_code == "ID") {
            message = response.header.reason.id;
          } else {
            message = response.header.reason.en;
          }
          this.setState({
            showLoader: false,
            emailFailed: true,
            errorMessageEmail: message,
          });
        } else {
          this.setState({ showLoader: false, errorMessageEmail: "" });
          Toast.show({
            buttonText: "Close",
            text: response.message,
            position: "bottom",
            duration: 5000,
          });
        }

        // Toast.show({ text: response.message, position: 'bottom', duration: 5000 });
      }
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "bottom", duration: 5000 });
    }
  }

  renderEmail() {
    return (
      <View
        style={{
          marginTop: 24,
        }}
      >
        <Label black style={styles.label}>
          Email
        </Label>
        <Item
          noShadowElevation
          style={styles.itemInput}
          error={this.state.emailFailed}
        >
          <Input
            newDesign
            // {...testID('inputEmail')}
            accessibilityLabel="input_email"
            autoCapitalize="none"
            keyboardType="email-address"
            placeholder={"Enter your email"}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) => this.setState({ email: txt })}
          />
        </Item>
        {this.state.emailFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessageEmail}
          </Text>
        )}
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showLoader}
          onRequestClose={() => console.log("loader closed")}
        >
          <Loader visible={this.state.showLoader} />
        </Modal>
        <SafeAreaView>
          <Header
            newDesign
            androidStatusBarColor={"#fff"}
            iosBarStyle={"dark-content"}
          >
            <Left newDesign>
              <TouchableOpacity
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Image
                  resizeMode={"contain"}
                  source={require("./../../assets/images/back.png")}
                  style={{ width: 48, height: 48 }}
                />
              </TouchableOpacity>
            </Left>
            <Body newDesign>
              <Title newDesign>Forgot Password</Title>
            </Body>
          </Header>
        </SafeAreaView>
        <Content
          style={{
            paddingHorizontal: 16,
            paddingVertical: 24,
          }}
        >
          <View
            style={{
              borderRadius: 8,
              backgroundColor: platform.backgroundInfo,
              padding: 16,
            }}
          >
            <Text
              regular
              style={{
                lineHeight: 26,
              }}
            >
              Please enter your registered email that you want to reset the
              password
            </Text>
          </View>

          {this.renderEmail()}

          <Button
            {...testID("button_send")}
            style={styles.btnSend}
            onPress={() => this.forgotPassword()}
            block
          >
            <Text textButton>Send</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  btnSend: {
    marginTop: 8,
    marginBottom: 10,
  },
  itemInput: {
    marginBottom: 24,
  },
  label: {
    marginBottom: 12,
  },
  textError: {
    marginTop: -16,
    marginBottom: 16,
  },
});
