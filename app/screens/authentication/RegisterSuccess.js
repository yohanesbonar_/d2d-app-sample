import React, { Component } from "react";
import { NavigationActions, StackActions } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  TouchableOpacity,
  BackHandler,
  SafeAreaView,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Button,
  Icon,
  Text,
  Toast,
} from "native-base";
import { testID, STORAGE_TABLE_NAME } from "./../../libs/Common";
import { Loader } from "./../../components";
import { getProfileUser, getPopupShow } from "./../../libs/NetworkUtility";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { setI18nConfig } from "../../libs/localize/LocalizeHelper";
import platform from "../../../theme/variables/platform";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";
import ButtonHighlight from "../../../src/components/atoms/ButtonHighlight";
export default class RegisterSuccess extends Component {
  state = {
    showLoader: false,
  };

  constructor(props) {
    super(props);
  }

  getProfile = async () => {
    try {
      this.setState({ showLoader: true });
      let response = await getProfileUser();
      this.setState({ showLoader: false });

      if (response.isSuccess && response.data != null) {
        console.log(response.data);

        let jsonProfile = JSON.stringify(response.data);
        let saveProfile = await AsyncStorage.setItem(
          STORAGE_TABLE_NAME.PROFILE,
          jsonProfile
        );
        setI18nConfig(); // set initial config
      }
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "top", buttonText: "Okay" });
    }
  };

  componentDidMount() {
    StatusBar.setHidden(false);
    let { params } = this.props.navigation.state;
    console.log(params);
    if (params != null && params.profile != null) {
      this.getProfile(params.profile.uid);
    }
    // this.checkModalPopup();
    // this.getProfile();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    //do nothing, to prevent user cant go back
    //this.props.navigation.goBack()
  };

  gotoHome = () => {
    // Adjust Tracker
    AdjustTracker(AdjustTrackerConfig.Onboarding_Finish);

    // this.resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Home' })] });
    // this.props.navigation.dispatch(this.resetAction);
    this.setGotoHomeNavigation();
  };

  setGotoHomeNavigation = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code != "ID") {
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Home" })],
        });
      } else {
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "HomeNavigation" }),
          ],
        });
      }

      this.props.navigation.dispatch(this.resetAction);
    });
  };
  async checkModalPopup() {
    let { params } = this.props.navigation.state;
    try {
      await AsyncStorage.setItem("showPopup", "true");
      let response = await getPopupShow();
      if (response.isSuccess == true) {
        let res = response.data;
        res.uid = params.profile.uid;
        // let popup = JSON.parse(await AsyncStorage.getItem('totalShowPopup'));

        // if(res.id != popup.id || popup.uid != params.profile.uid)
        // {
        // if(popup.uid == user.uid)
        //     res.total_show = popup.total_show;
        await AsyncStorage.setItem("totalShowPopup", JSON.stringify(res));
        console.log("set", res);
        // }
      } else {
        console.log("gagal", res);
      }
    } catch (error) {
      // Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 });
    }
  }

  render() {
    return (
      <Container style={styles.backgroundColor}>
        <Loader visible={this.state.showLoader} />
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header noShadow>
            <Left style={{ flex: 0.5 }}>
              {/* <Button
                            {...testID('buttonBack')}
                            transparent onPress={() => this.logout()}>
                            <Icon name='md-arrow-back' style={styles.contentColor} />
                        </Button> */}
            </Left>
            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Title>Input Your Data</Title>
            </Body>
            <Right style={{ flex: 0.5 }} />
          </Header>
        </SafeAreaView>
        <View
          style={{ 
            flex: 1, 
            flexDirection: "column", 
            paddingHorizontal: 25,
            marginBottom:
              platform.platform == "ios"
                ? Platform.isPad
                  ? 34
                  : platform.deviceHeight >= 812 &&
                    platform.deviceWidth >= 375
                  ? 34
                  : 8
                : 8,
          paddingBottom: 16, }}
        >
          <View style={{ flex: 1, marginBottom: 20, marginTop: 20 }}>
            <Text style={styles.textTitle}>
              Congratulation, you have completed your registration step!
            </Text>
            <Text style={styles.textDescription}>
              Your personal information automatically saved on our system.
              Please use this application properly.
            </Text>
          </View>
          <View style={{ height:86 }}>
            <View style={styles.btnNextRegister}>

              <ButtonHighlight
                // {...testID('btnLanjut')}
                accessibilityLabel="btn_lanjut"
                text="FINISH"
                onPress={() => this.gotoHome()}
                additionalContainerStyle={{...styles.touchBtnRegister, paddingVertical:null, paddingHorizontal:null, flex:0, height:null}}
                additionalTextStyle={{...styles.textNext, lineHeight:null}}/>
              
              <View style={styles.viewGrey} />
              <View style={styles.viewGreen} />
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  backgroundColor: {
    backgroundColor: "#F7F7FA",
  },
  contentColor: {
    color: "#78849E",
  },
  icond2d: {
    width: 65,
    height: 65,
    marginBottom: 10,
  },
  btnNextRegister: {
    backgroundColor: "#F7F7FA",
    flexDirection: "column",
    flex: 1,
    height: "100%",
  },
  touchBtnRegister: {
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    padding: 15,
    // backgroundColor: '#3AE194',
  },
  textNext: {
    fontFamily: "Nunito-Bold",
    color: "white",
    fontSize: 18,
  },
  viewGrey: {
    backgroundColor: "#EAEAEA",
    height: 12,
    borderRadius: 15,
  },
  viewGreen: {
    width: "100%",
    top: -12,
    backgroundColor: "#3AE194",
    height: 12,
    borderRadius: 15,
  },
  textTitle: {
    color: "#78849E",
    fontSize: 30,
    fontFamily: "Nunito-Bold",
    marginBottom: 10,
  },
  textDescription: {
    color: "#78849E",
    fontSize: 18,
    fontFamily: "Nunito",
    marginBottom: 10,
  },
});
