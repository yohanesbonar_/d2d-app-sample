import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, BackHandler, ImageBackground, TouchableOpacity, Platform, ScrollView, SafeAreaView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Form, Item, Input, Label, Toast, Card, CardItem } from 'native-base';
import { Loader } from './../../components';
import { validateEmail, STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import guelogin from './../../libs/GueLogin';
const gueloginAuth = firebase.app('guelogin');
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';

import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { translate } from './../../libs/localize/LocalizeHelper'
import ButtonHighlight from '../../../src/components/atoms/ButtonHighlight';

export default class RegisterSpecialist extends Component {

    state = {
        specialistList: [],
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.getDataSp();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    profile = null;

    getDataSp = () => {
        this.profile = this.props.navigation.state.params;

        console.log('profile: ', this.profile)
        if (this.profile != null) {
            this.setState({ specialistList: this.profile.specialist })
        }
    }

    gotoSelectSpecialist = () => {
        if (this.profile == null) {
            console.log('profile null')
            return;
        }

        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSpecialist',
            key: `gotoSelectSpecialist`,
            params: {
                specialistList: this.profile.specialist,
                saveDataSp: this.updateDataSp,
                from: 'SPECIALIST'
            }
            ,
        }));
    }

    gotoInputSubscribtion = () => {
        if (this.state.specialistList.length > 0) {
            console.log('gotoInputSubscribtion profile: ', this.profile)
            // this.props.navigation.dispatch(NavigationActions.navigate({
            //     routeName: 'RegisterSubscribtions',
            //     key: `gotoInputSubscribtions`,
            //     params: this.profile,
            // }));
            this.props.navigation.dispatch(NavigationActions.navigate({
                routeName: 'RegisterPracticeLocation',
                key: `gotoRegPracticeLoc`,
                params: this.profile,
            }));
        }
    }


    updateDataSp = (specialistList) => {
        this.profile.specialist = specialistList;
        this.profile.subscribtions = specialistList;
        this.setState({ specialistList: specialistList })
        console.log('updateDataSp: ', this.profile)
    }

    renderEmptySpecilist = () => {
        return (<View style={styles.viewEmpty}>
            <View style={{ flexDirection: 'column' }}>
                <Text style={styles.textSpesialis}>Specialization</Text>
                <Item onPress={() => {
                    // Adjust Tracker
                    AdjustTracker(AdjustTrackerConfig.Onboarding_Spec);

                    this.gotoSelectSpecialist()
                }
                }
                    last style={styles.itemInput}>
                    <Icon name={'user-md'} type={'FontAwesome'} style={{ color: '#78849E' }} />
                    <Input
                        pointerEvents="none"
                        editable={false}
                        autoCapitalize="none"
                        placeholder={translate('hint_example_specialist')}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.setState({ password: txt })} />
                </Item>
            </View>
        </View>
        )
    }

    renderData = () => {
        return <View style={{ flex: 1, marginBottom: 10, }}>
            {
                <ScrollView>
                    {this.renderSpecialist()}
                </ScrollView>

            }
            <TouchableOpacity
                // {...testID('add')} 
                accessibilityLabel="add"
                style={{ padding: 10 }}
                onPress={() => {
                    // Adjust Tracker
                    AdjustTracker(AdjustTrackerConfig.Onboarding_Spec_Add);

                    this.gotoSelectSpecialist()
                }}>
                <Text style={styles.textAdd}>Add +</Text>
            </TouchableOpacity>
        </View>;
    }

    renderSpecialist = () => {
        let items = []
        let self = this;
        this.state.specialistList.map(function (d, i) {
            items.push(
                <Card style={{ paddingVertical: 10 }} key={i}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name={'user-md'} type={'FontAwesome'} style={styles.iconSpesialist} />
                        <View style={styles.viewTextNoborder}>
                            <TouchableOpacity
                                // {...testID('cancel')}
                                accessibilityLabel="cancel"
                                onPress={() => self.doDeleteSpecialist(self, i)}>
                                <Icon name={'ios-close-circle'} style={styles.iconCancel} />
                            </TouchableOpacity>
                            <Text style={{ color: '#78849E', fontSize: 16, flex: 1 }}>{d.description}</Text>

                        </View>
                    </View>
                    {self.renderSubSpecialist(d, i)}
                </Card>
            )
        });

        return items;
    }

    renderSubSpecialist = (specialist, indexSp) => {
        let items = []
        let self = this;
        if (specialist.subspecialist != null && specialist.subspecialist.length > 0) {
            specialist.subspecialist.map(function (d, i) {
                if (d.id > 0) {
                    items.push(
                        <View style={{ flexDirection: 'row', }} key={i}>
                            <Icon name={'user-md'} type={'FontAwesome'} style={styles.iconSubSpecialist} />
                            <View style={styles.viewTextBorderTop}>
                                <TouchableOpacity
                                    // {...testID('cancel')}
                                    accessibilityLabel="cancel"
                                    onPress={() => self.deleteSubSpecialist(self, indexSp, i)}>
                                    <Icon name={'ios-close-circle'} style={styles.iconCancel} />
                                </TouchableOpacity>
                                <Text style={{ color: '#78849E', fontSize: 16, flex: 1 }}>{d.description}</Text>
                            </View>
                        </View>
                    )
                }
            })
        }

        return items;
    }

    doDeleteSpecialist = (self, index) => {
        self.state.specialistList.splice(index, 1);
        self.setState({ specialist: self.state.specialist, needSendSpesialzation: true })
    }

    deleteSubSpecialist = (self, indexSp, index) => {
        self.state.specialistList[indexSp].subspecialist.splice(index, 1);
        self.setState({ specialist: self.state.specialist, needSendSpesialzation: true })
    }

    render() {
        let btnColor = this.state.specialistList.length > 0 ? '#3AE194' : '#BAC3BE';

        return (
            <Container style={styles.backgroundColor}>
                <SafeAreaView>

                    <Header noShadow style={styles.backgroundColor}>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' style={styles.contentColor} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title style={styles.contentColor}>Input Your Data</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>

                <View 
                    style={{ flex: 1, 
                        flexDirection: 'column', 
                        paddingHorizontal: 25, 
                        marginBottom:
                            platform.platform == "ios"
                            ? Platform.isPad
                                ? 34
                                : platform.deviceHeight >= 812 &&
                                platform.deviceWidth >= 375
                                ? 34
                                : 8
                            : 8,
                        paddingBottom: 16, 
                }}>
                    <View style={{ flex: 0.2, justifyContent: 'center' }}>
                        <Text style={styles.textTitle}>What's{`\n`}your specializations?</Text>
                    </View>
                    <View style={{ flex: 0.8 }}>
                        {this.state.specialistList.length <= 0 && this.renderEmptySpecilist()}
                        {this.state.specialistList.length > 0 && this.renderData()}
                    </View>
                    <View style={{ height:86 }}>
                        <View style={styles.btnNextRegister}>
                            <ButtonHighlight
                                // {...testID('btnLanjut')}
                                accessibilityLabel="btn_lanjut"
                                text="NEXT"
                                buttonColor={this.state.specialistList.length > 0? null: "#BAC3BE"}
                                buttonUnderlayColor={this.state.specialistList.length > 0? null: "#D7D7D7"}
                                onPress={() => this.gotoInputSubscribtion()}
                                additionalContainerStyle={{...styles.touchBtnRegister, paddingVertical:null, paddingHorizontal:null, flex:0, height:null}}
                                additionalTextStyle={{...styles.textNext, lineHeight:null}}/>
                            <View style={styles.viewGrey} />
                            <View style={styles.viewGreen(this.props.navigation.state.params.country_code)} />
                        </View>
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: '#F7F7FA'
    },
    contentColor: {
        color: '#78849E'
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
    },
    touchBtnRegister: {
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
        // backgroundColor: '#3AE194',
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: (country_code) => [{
        width: country_code != "PH" ? "50%" : "40%",
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    }],
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
    },
    viewEmpty: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 25,
        marginTop: 10
    },
    textSpesialis: {
        color: '#78849E',
        margin: 10,
        fontSize: 16
    },
    itemInput: {
        backgroundColor: 'white',
        alignItems: 'center'
    },
    textAdd: {
        color: '#0EA2DD',
        textAlign: 'right',
        fontSize: 18,
        fontFamily: 'Nunito-Bold',
    },
    iconSpesialist: {
        color: '#78849E',
        marginHorizontal: 15,
        fontSize: 28
    },
    iconSubSpecialist: {
        color: 'white',
        marginHorizontal: 15,
        fontSize: 28
    },
    viewTextNoborder: {
        flex: 1,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewTextBorderTop: {
        flex: 1,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#D7DEE6',
        paddingTop: 5,
        marginTop: 5
    },
    iconCancel: {
        color: '#D7DEE6',
        marginRight: 10,
    }
});
