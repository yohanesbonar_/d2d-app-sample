import React, { Component } from "react";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Modal,
  ScrollView,
  Image,
  BackHandler,
  Linking,
  AppState,
  SafeAreaView,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Col,
  Button,
  Icon,
  Text,
  Toast,
  Card,
  Item,
  Input,
  Label,
  Row,
} from "native-base";
import { Loader } from "./../../components";
import { validateEmail, testID } from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
import guelogin from "./../../libs/GueLogin";
const gueloginAuth = firebase.app("guelogin");
import { doResendForgotPassword } from "./../../libs/NetworkUtility";
import { openInbox } from "react-native-email-link";
import {
  getData,
  KEY_ASYNC_STORAGE,
  resendForgotPassword,
} from "../../../src/utils";

export default class ForgotPasswordConfirmation extends Component {
  totalSeconds = 120;
  countTimer = null;
  timestampBackgroundInSeconds = 0;
  timestampForegroundInSeconds = 0;

  state = {
    appState: AppState.currentState,
    showLoader: false,
    code: "",
    codeFailed: false,
    password: "",
    passwordFailed: false,
    passwordVisible: false,
    confpassword: "",
    confpasswordFailed: false,
    confpasswordVisible: false,
    email: null,
    countdownResend: "02:00",
    isResend: false,
    country_code: "",
  };

  componentDidMount() {
    this.getDataCountryCode();
    StatusBar.setHidden(false);
    const { params } = this.props.navigation.state;
    this.setState({ email: params.email });

    this.countTimer = setInterval(this.countDownTimer, 1000);

    this.didBlurListener = this.props.navigation.addListener(
      "didBlur",
      (payload) => {
        Linking.removeEventListener("url", this.handleOpenURL);
        AppState.removeEventListener("change", this._handleAppStateChange);
      }
    );

    this.didFocusListener = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        AppState.addEventListener("change", this._handleAppStateChange);
        this.checkDeepLink();
      }
    );

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      console.log("country_code -> ", profile.country_code);
      this.setState({
        country_code: profile.country_code,
      });
    });
  };

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
    Linking.removeEventListener("url", this.handleOpenURL);
    AppState.removeEventListener("change", this._handleAppStateChange);
    clearInterval(this.countTimer);
    this.didBlurListener.remove();
    this.didFocusListener.remove();
  }

  _handleAppStateChange = (nextAppState) => {
    const { appState } = this.state;
    console.log("_handleAppStateChange appState: ", appState);
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground!");
      this.timestampForegroundInSeconds = Date.parse(new Date()) / 1000;
      console.log(
        "timestampForegroundInSeconds ",
        this.timestampForegroundInSeconds
      );
      //  if (this.timestampForegroundInSeconds != 0 && this.timestampBackgroundInSeconds != 0) {
      let diffTimestamp =
        this.timestampForegroundInSeconds - this.timestampBackgroundInSeconds;
      console.log("diffTimestamp ", diffTimestamp);
      console.log("totalSeconds ", this.totalSeconds);

      if (this.totalSeconds != 0) {
        if (diffTimestamp <= this.totalSeconds) {
          this.totalSeconds = this.totalSeconds - diffTimestamp;
          this.countTimer = setInterval(this.countDownTimer, 1000);
        } else {
          this.totalSeconds = 0;
          this.setState({
            isResend: true,
          });
        }
      } else {
        this.setState({
          isResend: true,
        });
        clearInterval(this.countTimer);
      }
    } else {
      this.timestampBackgroundInSeconds = Date.parse(new Date()) / 1000;
      console.log(
        "timestampBackgroundInSeconds ",
        this.timestampBackgroundInSeconds
      );
      clearInterval(this.countTimer);
    }

    this.setState({ appState: nextAppState });
  };
  checkDeepLink() {
    Linking.getInitialURL()
      .then((url) => this.handleOpenURL({ url }))
      .catch((err) => console.error(err));

    //for app running in background
    Linking.addEventListener("url", this.handleOpenURL);
  }

  handleOpenURL = async (deeplink) => {
    console.log("handleOpenURL deeplink: ", deeplink);

    if (deeplink.url != null) {
      // "https://staging.d2d.co.id/changepassword/xEg56dpMD8OCfi0cH9YnivLw2I13/dVHcPs"
      if (deeplink.url.includes("d2d.co.id/changepassword")) {
        let splittedUrl = deeplink.url.split("/");

        let uid = splittedUrl[splittedUrl.length - 2];
        let passcode = splittedUrl[splittedUrl.length - 1];

        let data = {
          uid: uid,
          passcode: passcode,
        };
        this.gotoSetNewPassword(data);
      }
    }
  };

  gotoSetNewPassword(data) {
    console.log("gotoSetNewPassword data: ", data);
    const setNewpassword = {
      type: "Navigate",
      routeName: "SetNewPassword",
      params: data,
    };
    this.props.navigation.navigate(setNewpassword);
  }

  gotoTologin() {
    this.props.navigation.replace("Login");
  }

  _visiblePassword = () => {
    this.setState({ passwordVisible: !this.state.passwordVisible });
  };

  _visibleconfPassword = () => {
    this.setState({ confpasswordVisible: !this.state.confpasswordVisible });
  };

  validateSubmitPassword() {
    let errorMessage = "";

    if (this.state.code != "") {
      this.setState({ codeFailed: false });
    } else {
      this.setState({ codeFailed: true });
      errorMessage = "code";
    }

    if (this.state.password != "") {
      this.setState({ passwordFailed: false });
    } else {
      this.setState({ passwordFailed: true });
      if (errorMessage != "") {
        errorMessage += ", ";
      }
      errorMessage += "password";
    }

    if (this.state.confpassword != "") {
      this.setState({ confpasswordFailed: false });
    } else {
      this.setState({ confpasswordFailed: true });
      if (errorMessage != "") {
        errorMessage += ", ";
      }
      errorMessage += "password confirmation";
    }

    if (errorMessage != "") {
      errorMessage += " can not by empty";
    } else {
      if (this.state.password != this.state.confpassword) {
        errorMessage = "Password confirmation doesn't match";
      }
    }

    return errorMessage;
  }

  openEmailApp = async () => {
    try {
      await openInbox();
    } catch (error) {
      Toast.show({ text: error.message, position: "bottom", duration: 5000 });
      console.log("openEmailApp: ", error.message);
    }
  };

  countDownTimer = () => {
    --this.totalSeconds;
    let minute = Math.floor(this.totalSeconds / 60);
    let seconds = this.totalSeconds - minute * 60;

    if (minute < 10) minute = "0" + minute;
    if (seconds < 10) seconds = "0" + seconds;

    if (this.totalSeconds >= 0) {
      this.setState({
        countdownResend: minute + ":" + seconds,
      });
    } else {
      this.setState({
        isResend: true,
      });
      clearInterval(this.countTimer);
    }

    // console.log("totalSeconds: ", this.totalSeconds)
  };

  resendForgotPassword = async () => {
    try {
      this.setState({ showLoader: true });
      let params = {
        email: this.state.email,
      };

      let response = await resendForgotPassword(params);

      console.log(response);

      if (response.acknowledge) {
        this.setState({ showLoader: false, isResend: false });
        Toast.show({
          buttonText: "Close",
          text: response.message,
          position: "bottom",
          duration: 5000,
        });

        this.totalSeconds = 120;
        this.countTimer = setInterval(this.countDownTimer, 1000);
      } else {
        this.setState({ showLoader: false });
        // Toast.show({ text: response.message, position: 'bottom', duration: 5000 });
        if (response.header && response.header.code && response.header.reason) {
          let message = "";
          if (this.state.country_code == "ID") {
            message = response.header.reason.id;
          } else {
            message = response.header.reason.en;
          }
          Toast.show({ text: message, position: "bottom", duration: 5000 });
        }
      }
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "bottom", duration: 5000 });
    }
  };

  render() {
    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showLoader}
          onRequestClose={() => console.log("loader closed")}
        >
          <Loader visible={this.state.showLoader} />
        </Modal>
        <SafeAreaView>
          <Header
            newDesign
            androidStatusBarColor={"#fff"}
            iosBarStyle={"dark-content"}
          >
            <Left newDesign>
              <TouchableOpacity
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Image
                  resizeMode={"contain"}
                  source={require("./../../assets/images/back.png")}
                  style={{ width: 48, height: 48 }}
                />
              </TouchableOpacity>
            </Left>
            <Body newDesign>
              <Title newDesign>Reset Password</Title>
            </Body>
          </Header>
        </SafeAreaView>

        <View
          style={{
            flex: 1,
          }}
        >
          <Col
            style={{
              justifyContent: "center",
              alignItems: "center",
              paddingHorizontal: 16,
            }}
          >
            <Image
              resizeMode="contain"
              source={require("./../../assets/images/email-verification.png")}
              style={{ width: 106, height: 74, marginBottom: 32 }}
            />
            <Text bold textLarge style={{ marginBottom: 16 }}>
              Check Your Email
            </Text>
            <Text style={{ textAlign: "center", lineHeight: 26 }}>
              <Text regular>
                {"We have sent a link for your reset password to\n"}
              </Text>
              <Text regular bold>
                {this.state.email},
              </Text>
              <Text regular> please check.</Text>
            </Text>

            <Button
              onPress={() => this.openEmailApp()}
              block
              style={{ marginTop: 32 }}
            >
              <Text textButton>Open Email App</Text>
            </Button>
          </Col>
        </View>

        <View
          style={{
            position: "absolute",
            bottom: 0,
            right: 0,
            left: 0,
            backgroundColor: "#F6F6F7",
          }}
        >
          <Row
            style={{
              justifyContent: "space-between",
              alignItems: "center",
              padding: 16,
            }}
          >
            <Text regular>Didn’t receive verification?</Text>

            <TouchableOpacity
              disabled={!this.state.isResend}
              onPress={() => this.resendForgotPassword()}
            >
              <Text textAsButton>
                {this.state.isResend ? "Resend" : this.state.countdownResend}
              </Text>
            </TouchableOpacity>
          </Row>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 10,
    marginTop: -platform.deviceHeight / 1.5,
  },
  card: {
    flex: 0,
    paddingVertical: 30,
  },
  centerAlign: {
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "Nunito-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  body: {
    textAlign: "center",
    fontSize: 14,
    lineHeight: 18,
  },
  btnResend: {
    marginTop: -3,
  },
  textResend: {
    fontSize: 14,
    color: platform.brandInfo,
  },
  circleIcon: {
    width: 80,
    height: 80,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: "#EAEAEA",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  btnSubmit: {
    marginTop: 15,
    marginBottom: 30,
    borderRadius: 10,
    height: 55,
  },
});
