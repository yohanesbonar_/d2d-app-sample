import React, { Component } from "react";
import firebase from "react-native-firebase";
import { GoogleSignin } from "@react-native-community/google-signin";
import { NavigationActions, StackActions } from "react-navigation";
import { AccessToken, LoginManager, LoginButton } from "react-native-fbsdk";
import AsyncStorage from "@react-native-community/async-storage";
import { ButtonHighlight } from "../../../src/components";

import {
  StatusBar,
  StyleSheet,
  View,
  Modal,
  Linking,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Item,
  Input,
  Label,
  Toast,
  Card,
  CardItem,
  Title,
  Body,
  Right,
} from "native-base";
import { Loader } from "./../../components";
import {
  validateEmail,
  STORAGE_TABLE_NAME,
  openOtherApp,
  subscribeFcm,
  testID,
  subscribeFcmSubscription,
  removeEmojis,
  getCountryCodeProfile,
} from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
const gueloginAuth = firebase.app("guelogin");
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import {
  doLoginApi,
  getAllSubscribtions,
  getPopupShow,
  activateRegisterVerification,
  doRegisterApi,
} from "./../../libs/NetworkUtility";

import { AdjustTrackerConfig, AdjustTracker } from "./../../libs/AdjustTracker";
import { setI18nConfig } from "../../libs/localize/LocalizeHelper";

import appleAuth, {
  AppleButton,
  AppleAuthRequestScope,
  AppleAuthRequestOperation,
} from "@invertase/react-native-apple-authentication";
import DeviceInfo from "react-native-device-info";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";
import { postLogin } from "../../../src/utils";
export default class Login extends Component {
  resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: "Home" })],
  });

  state = {
    showLoader: false,
    email: null,
    emailFailed: false,
    password: null,
    passwordFailed: false,
    passwordVisible: false,
    country: this.props.navigation.state.params.country,
    errorMessageName: "",
    name: "",
    country_code: "",
  };

  constructor(props) {
    super(props);
    this.getDataCountryCode();
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code != null) {
        this.setState({ country_code: profile.country_code });
      }
    });
  };

  componentDidMount() {
    console.log("componentDidMount: ", this.props.navigation.state.params);

    AdjustTracker(AdjustTrackerConfig.Login_Start);

    StatusBar.setHidden(false);
    this.clearSubscribeFcm();

    this.didBlurListener = this.props.navigation.addListener(
      "didBlur",
      (payload) => {
        console.log("didBlurListener login");
        Linking.removeEventListener("url", this.handleOpenURL);
      }
    );

    this.didFocusListener = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        console.log("didFocusListener login");
        this.checkDeepLink();
      }
    );

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  onBackPressed = () => {
    ///this.props.navigation.goBack();
    this.props.navigation.replace("ChooseCountry");
  };

  componentWillUnmount() {
    console.log("componentWillUnmount login");
    this.didBlurListener.remove();
    this.didFocusListener.remove();
    this.backHandler.remove();
    Linking.removeEventListener("url", this.handleOpenURL);
  }

  checkDeepLink() {
    Linking.getInitialURL()
      .then((url) => this.handleOpenURL({ url }))
      .catch((err) => console.error(err));

    //for app running in background
    Linking.addEventListener("url", this.handleOpenURL);
  }

  handleOpenURL = async (deeplink) => {
    console.log("handleOpenURL deeplink: ", deeplink);

    if (deeplink.url != null) {
      //verification account
      if (deeplink.url.includes("d2d.co.id/verify")) {
        let splittedUrl = deeplink.url.split("/");

        let uid = splittedUrl[splittedUrl.length - 2];
        let code = splittedUrl[splittedUrl.length - 1];

        let data = {
          uid: uid,
          code: code,
        };
        this.doActivateRegisterVerification(data);
      }
    }
  };

  doActivateRegisterVerification = async (data) => {
    try {
      this.setState({ showLoader: true });
      let params = {
        uid: data.uid,
        code: data.code,
      };

      let response = await activateRegisterVerification(params);
      console.log("activateRegisterVerification response: ", response);

      if (response.isSuccess == true) {
        this.setState({ showLoader: false });
        Toast.show({
          text: response.message,
          position: "bottom",
          duration: 3000,
        });
      } else {
        Toast.show({
          text: response.message,
          position: "bottom",
          duration: 5000,
        });
        this.setState({ showLoader: false });
      }
    } catch (error) {
      console.log("catch", error);
      Toast.show({ text: error, position: "top", duration: 5000 });
      this.setState({ showLoader: false });
    }
  };

  async clearSubscribeFcm() {
    let topicFCM = await subscribeFcm();
    await firebase.messaging().unsubscribeFromTopic(topicFCM);

    this.doCheckDataTopics();
  }

  doCheckDataTopics = async () => {
    let isEmptyFcm = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.IS_EMPTY_FCM_TOPICS
    );
    if (isEmptyFcm != null && isEmptyFcm == "true") {
      return;
    }

    try {
      let response = await getAllSubscribtions();

      if (response.isSuccess == true) {
        this.clearDataTopicsFcm(response.data);
      }
    } catch (error) {}
  };

  async checkModalPopup() {
    try {
      let user = gueloginAuth.auth().currentUser;
      if (!_.isEmpty(user)) {
        await AsyncStorage.setItem("showPopup", "true");
        let response = await getPopupShow();
        if (response.isSuccess == true) {
          let res = response.data;
          res.uid = user.uid;
          let popup = JSON.parse(await AsyncStorage.getItem("totalShowPopup"));

          if (res.id != popup.id || popup.uid != user.uid) {
            if (popup.uid == user.uid) res.total_show = popup.total_show;
            await AsyncStorage.setItem("totalShowPopup", JSON.stringify(res));
            console.log("set", res);
          }
        } else {
          console.log("gagal", res);
        }
      }
    } catch (error) {
      // Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 });
    }
  }

  clearDataTopicsFcm = async (subscriptions) => {
    let country_code = await getCountryCodeProfile();
    if (subscriptions != null && subscriptions.length > 0) {
      for (let i = 0; i < subscriptions.length; i++) {
        await firebase
          .messaging()
          .unsubscribeFromTopic(
            subscribeFcmSubscription + subscriptions[i].id + "_PH"
          );
        await firebase
          .messaging()
          .unsubscribeFromTopic(
            subscribeFcmSubscription + subscriptions[i].id + "_KH"
          );
        await firebase
          .messaging()
          .unsubscribeFromTopic(
            subscribeFcmSubscription + subscriptions[i].id + "_MM"
          );
        await firebase
          .messaging()
          .unsubscribeFromTopic(subscribeFcmSubscription + subscriptions[i].id);
      }
      let needClear = await AsyncStorage.setItem(
        STORAGE_TABLE_NAME.IS_EMPTY_FCM_TOPICS,
        "true"
      );
    }
  };

  async loginEmail() {
    let isSuccess = true;
    let message = "";
    let messages = [];

    if (!this.state.email) {
      this.setState({ emailFailed: true });
      messages.push("Email address");
    } else {
      if (validateEmail(this.state.email) === false) {
        isSuccess = false;
        this.setState({ emailFailed: !isSuccess });
        messages.push("Email address is invalid");
      } else {
        isSuccess = true;
        this.setState({ emailFailed: !isSuccess });
      }
    }

    if (!this.state.password) {
      this.setState({ passwordFailed: true });
      messages.push("Password");
    } else {
      if (this.state.password.length < 6) {
        isSuccess = false;
        this.setState({ passwordFailed: !isSuccess });
        messages.push("Password min. 6 character");
      } else {
        isSuccess = true;
        this.setState({ passwordFailed: !isSuccess });
      }
    }

    if (messages.length == 2) {
      isSuccess = false;
      message = messages.join(" and ");
    } else if (messages.length > 0 && messages.length < 2) {
      message = messages.join("");
      isSuccess = false;
    } else if (messages.length > 2) {
      message = messages.join(", ");
      isSuccess = false;
    }

    if (!this.state.email || !this.state.password) {
      message += " is required";
    }

    if (isSuccess == true) {
      this.setState({ showLoader: true });

      try {
        let user = await gueloginAuth
          .auth()
          .signInAndRetrieveDataWithEmailAndPassword(
            this.state.email,
            this.state.password
          );
        console.log(user);

        if (user.user.emailVerified === false) {
          this.setState({ showLoader: false });
          Toast.show({
            text: "Your account is inactive, please check your email!",
            position: "bottom",
            duration: 3000,
          });
          this.gotoVerification();
        } else {
          // let setLogin = await AsyncStorage.setItem('IS_LOGIN', 'Y');
          // let setUID = await AsyncStorage.setItem('UID', user.user.uid);

          this.doLogin(user.user.email);
        }
      } catch (error) {
        console.log(error);
        let msg = null;
        // let gotoVerif = false;
        switch (error.code) {
          case "auth/invalid-email":
            msg = "Email address is invalid!";
            break;
          case "auth/user-disabled":
            msg = "Your account is inactive!";
            // gotoVerif = true;
            break;
          case "auth/user-not-found":
            msg = "Email address is not registered!";
            break;
          case "auth/wrong-password":
            msg = "Email address or password incorrect!";
            break;
          case "auth/network-error":
            msg = "Check your connection!";
            break;
          case "auth/too-many-requests":
            msg =
              "You've made too many attempts at logging in. Please try again later";
            break; //ios
          case "auth/unknown":
            msg =
              "You've made too many attempts at logging in. Please try again later";
            break; //android
        }

        // if(gotoVerif){
        //     this.gotoVerification();
        // }else if (msg){
        Toast.show({ text: `${msg}!`, position: "top", duration: 3000 });
        // }

        this.setState({ showLoader: false });
      }
    } else {
      Toast.show({ text: `${message}`, position: "top", duration: 3000 });
    }

    AdjustTracker(AdjustTrackerConfig.Login_Login);
  }

  loginFacebook = async () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logOut;
    LoginManager.logInWithPermissions(["public_profile", "email"]).then(
      (result) => {
        console.log("loginFacebook", result);
        if (result.isCancelled) {
          return Promise.reject(new Error("The user cancelled the request"));
        }

        return AccessToken.getCurrentAccessToken().then((data) => {
          console.log("data: " + JSON.stringify(data));
          this.setState({ showLoader: true });
          const credential = firebase.auth.FacebookAuthProvider.credential(
            data.accessToken
          );

          return gueloginAuth
            .auth()
            .signInAndRetrieveDataWithCredential(credential)
            .then((user) => {
              console.log("user: " + JSON.stringify(user));
              this.doLogin(user.user.email);
            })
            .catch((error) => {
              console.log("error ", error.message);
              this.setState({ showLoader: false });
              if (error.message != "The user cancelled the request") {
                Toast.show({
                  text: error.message,
                  position: "top",
                  duration: 3000,
                });
              }
            });
        });
      },
      function(error) {
        console.log("Login fail with error: " + error);
      }
    );

    AdjustTracker(AdjustTrackerConfig.Login_Facebook);
  };

  async loginGoogle() {
    await GoogleSignin.configure({
      iosClientId:
        "127784416281-4dik7n346f48opmidta6ueh8k7n42fvd.apps.googleusercontent.com",
      webClientId:
        "127784416281-epnvn2n12te5amjekdoeki5rbn4vkc4m.apps.googleusercontent.com",
    });

    try {
      this.setState({ showLoader: true });
      const google = await GoogleSignin.signIn();
      //console.warn('google', google)
      const credential = firebase.auth.GoogleAuthProvider.credential(
        google.idToken,
        google.accessToken
      );
      const user = await gueloginAuth
        .auth()
        .signInAndRetrieveDataWithCredential(credential);
      this.doLogin(user.user.email);
    } catch (error) {
      console.warn("loginGoogle error", error);
      this.setState({ showLoader: false });
      if (platform.platform == "ios") {
        if (error.code != -5) {
          //code = -5 //12501
          Toast.show({ text: error.message, position: "top", duration: 3000 });
        }
      } else {
        if (error.code != 12501) {
          //code = -5 //12501
          Toast.show({ text: error.message, position: "top", duration: 3000 });
        }
      }
    }

    AdjustTracker(AdjustTrackerConfig.Login_Google);
  }

  setGotoHomeNavigation = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code != "ID") {
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Home" })],
        });
      } else {
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "HomeNavigation" }),
          ],
        });
      }

      this.props.navigation.dispatch(this.resetAction);
    });
  };

  async doLogin(email) {
    try {
      let user = gueloginAuth.auth().currentUser;
      let response = null;

      if (typeof user.email != "undefined") {
        if (this.state.country_code == "ID") {
          response = await postLogin(email);
        } else {
          response = await doLoginApi(email);
        }
      }
      console.log("doLogin response: ", response);

      console.warn("login", response.acknowledge);
      if (
        (response.acknowledge && this.state.country_code == "ID") ||
        (response.isSuccess == true && this.state.country_code != "ID")
      ) {
        if (this.state.country_code == "ID") {
          console.warn(response.result.uid);
          uid = response.result.uid;
        } else {
          console.warn(response.data.uid);
          uid = response.data.uid;
        }

        if (typeof uid != "undefined") {
          let profile = null;
          if (this.state.country_code == "ID") {
            profile = response.result;
          } else {
            profile = response.data;
          }
          let jsonProfile = JSON.stringify(profile);
          let saveProfile = await AsyncStorage.setItem(
            STORAGE_TABLE_NAME.PROFILE,
            jsonProfile
          );

          setI18nConfig(); // set initial config

          prevUid = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);
          console.log("profile.uid ", profile.uid);
          console.log("prevUid ", prevUid);

          if (profile.uid != prevUid) {
            console.log("remove async storage SUBMIT_CME_QUIZ ");
            await AsyncStorage.removeItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ);
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_EVENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_WEBINAR
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_PAY_NOW
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_JOIN
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_REPLAY
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CONTENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL
            );
          }

          let uid = await AsyncStorage.setItem(
            STORAGE_TABLE_NAME.UID,
            profile.uid
          );
          let setLogin = await AsyncStorage.setItem("IS_LOGIN", "Y");

          console.warn("login", "login api berhasil");
          // await this.checkModalPopup();

          if (
            profile.subscription == null ||
            profile.subscription.length == 0
          ) {
            let country = this.state.country;

            this.resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "RegisterHello",
                  params: {
                    profile,
                    country,
                  },
                }),
              ],
            });
            this.props.navigation.dispatch(this.resetAction);
            // const registerHello = { type: "Navigate", routeName: 'RegisterHello', params: profile }
            // this.props.navigation.navigate(registerHello);
          } else if (profile.subscription.length > 0) {
            console.log("gotoHome from login");
            this.setGotoHomeNavigation();
            //this.resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Home' })] });
          }
        } else {
          console.warn("login", "login api ga ada data");
          if (this.state.country_code == "ID") {
            if (response.message != "Login Success") {
              Toast.show({
                text: response.message,
                position: "top",
                duration: 3000,
              });
            }
          } else {
            if (response.message != "Success") {
              Toast.show({
                text: response.message,
                position: "top",
                duration: 3000,
              });
            }
          }

          await gueloginAuth.auth().signOut();
          this.resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: "Login", params: user }),
            ],
          });
          this.props.navigation.dispatch(this.resetAction);
        }
      } else {
        console.warn("response.message", response.message);

        if (this.state.country_code == "ID") {
          if (response.message != "Login Success") {
            Toast.show({
              text: response.message,
              position: "top",
              duration: 3000,
            });
          }
        } else {
          if (response.message != "Success") {
            Toast.show({
              text: response.message,
              position: "top",
              duration: 3000,
            });
          }
        }
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "Login", params: user }),
          ],
        });
        this.props.navigation.dispatch(this.resetAction);
      }
    } catch (error) {
      console.warn("doLogin error", error);
    }
  }

  gotoVerification() {
    const verifyEmail = {
      type: "Navigate",
      routeName: "RegisterVerification",
      params: { success: true, email: this.state.email },
    };
    this.props.navigation.navigate(verifyEmail);
  }

  doRegister() {
    const nav = this.props.navigation;
    const { params } = this.props.navigation.state;
    let country = {
      country: params.country,
    };

    AdjustTracker(AdjustTrackerConfig.Login_Register);

    const register = {
      type: "Navigate",
      routeName: "Register",
      params: country,
    };
    console.log("doRegister country: ", country);
    nav.navigate(register);
  }

  _visiblePassword = () => {
    this.setState({ passwordVisible: !this.state.passwordVisible });
  };

  /**
   * Note the sign in request can error, e.g. if the user cancels the sign-in.
   * Use `AppleAuthError` to determine the type of error, e.g. `error.code === AppleAuthError.CANCELED`
   */
  onAppleButtonPress = async () => {
    // 1). start a apple sign-in request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGIN,
      requestedScopes: [
        AppleAuthRequestScope.EMAIL,
        AppleAuthRequestScope.FULL_NAME,
      ],
    });

    console.log("appleAuthRequestResponse: ", appleAuthRequestResponse);
    // 2). if the request was successful, extract the token and nonce
    const { identityToken, nonce } = appleAuthRequestResponse;

    // can be null in some scenarios
    if (identityToken) {
      // 3). create a Firebase `AppleAuthProvider` credential
      const appleCredential = firebase.auth.AppleAuthProvider.credential(
        identityToken,
        nonce
      );
      console.log("appleCredential: ", appleCredential);

      // 4). use the created `AppleAuthProvider` credential to start a Firebase auth request,
      //     in this example `signInWithCredential` is used, but you could also call `linkWithCredential`
      //     to link the account to an existing user
      const userCredential = await gueloginAuth
        .auth()
        .signInWithCredential(appleCredential);
      console.log("userCredential: ", userCredential);
      this.setState({ showLoader: true });

      await this.doLogin(userCredential.user.email);

      this.setState({ showLoader: false });

      // user is now signed in, any Firebase `onAuthStateChanged` listeners you have will trigger
      console.warn(
        `Firebase authenticated via Apple, UID: ${userCredential.user.uid}`
      );
    } else {
      // handle this - retry?
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  };

  renderNew() {
    return (
      <Container>
        <SafeAreaView>
          <Header
            androidStatusBarColor={"#fff"}
            left
            noShadow
            noBackArrow
            iosBarStyle={"dark-content"}
          >
            <Title leftTitle>Login</Title>
          </Header>
        </SafeAreaView>
        <Content>
          <Label style={{ marginTop: 15, marginBottom: 5 }}>Email</Label>
          <Item error={this.state.emailFailed}>
            <Input
              // {...testID('inputEmail')}
              accessibilityLabel="input_email"
              autoCapitalize="none"
              placeholder={"e-mail"}
              keyboardType="email-address"
              placeholderTextColor={platform.placeholderTextColor}
              onChangeText={(txt) => this.setState({ email: txt })}
            />
          </Item>
          <Label style={{ marginTop: 15, marginBottom: 5 }}>Password</Label>
          <Item last error={this.state.passwordFailed}>
            <Input
              // {...testID('inputPassword')}
              autoCapitalize="none"
              accessibilityLabel="input_password"
              placeholder={"password"}
              placeholderTextColor={platform.placeholderTextColor}
              onChangeText={(txt) => this.setState({ password: txt })}
              secureTextEntry={!this.state.passwordVisible}
            />
            <TouchableOpacity
              // {...testID('showPassword')}
              // accessibilityLabel="show_password"
              onPress={() => this._visiblePassword()}
            >
              <Icon
                type="FontAwesome"
                name={this.state.passwordVisible ? "eye" : "eye-slash"}
              />
            </TouchableOpacity>
          </Item>
        </Content>
      </Container>
    );
  }

  checkValidateRegistration = () => {
    let regexName = /^(?!\s)[A-Za-z.'’\s]+$/;
    let isNameValid = regexName.test(this.state.name);

    if (!this.state.name) {
      this.setState({
        nameFailed: true,
        errorMessageName: "Full name is required",
      });
    } else {
      if (isNameValid === false) {
        this.setState({
          nameFailed: true,
          errorMessageName: "Full name is invalid",
        });
      } else {
        if (this.state.name.trim().length < 3) {
          this.setState({
            nameFailed: true,
            errorMessageName: "Full name min. 3 character",
          });
        } else {
          this.setState({ nameFailed: false });
        }
      }
    }
  };
  doRegisterUser = async () => {
    let isValid = false;

    await this.checkValidateRegistration();

    if (!this.state.nameFailed) {
      isValid = true;
    }

    if (isValid === true) {
      try {
        this.setState({ showLoader: true });

        let params = {
          name: this.state.name,
          device_unique_id: DeviceInfo.getUniqueId(),
          country_code: this.state.country.country_code,
        };

        let response = await doRegisterApi(params);
        console.log("doRegisterUser response: ", response);
        if (response.isSuccess == true) {
          let email = response.docs.data.email;
          let user = await gueloginAuth
            .auth()
            .signInAndRetrieveDataWithEmailAndPassword(
              email,
              params.device_unique_id
            );
          console.log(user);

          let profile = response.docs.data;
          let jsonProfile = JSON.stringify(profile);
          let saveProfile = await AsyncStorage.setItem(
            STORAGE_TABLE_NAME.PROFILE,
            jsonProfile
          );
          setI18nConfig(); // set initial config

          prevUid = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);
          console.log("profile.uid ", profile.uid);
          console.log("prevUid ", prevUid);

          if (profile.uid != prevUid) {
            console.log("remove async storage SUBMIT_CME_QUIZ ");
            await AsyncStorage.removeItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ);
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_EVENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_WEBINAR
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_PAY_NOW
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_JOIN
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_REPLAY
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CONTENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL
            );
          }

          let uid = await AsyncStorage.setItem(
            STORAGE_TABLE_NAME.UID,
            profile.uid
          );
          let setLogin = await AsyncStorage.setItem("IS_LOGIN", "Y");

          this.setState({ showLoader: false });
          this.props.navigation.dispatch(this.resetAction);
        } else {
          Toast.show({
            text: response.message,
            position: "top",
            duration: 5000,
          });
          this.setState({ showLoader: false });
        }
      } catch (error) {
        this.setState({ showLoader: false });
        Toast.show({
          text: "Something went wrong!" + error,
          position: "top",
          duration: 5000,
        });
      }
    }
  };

  onChangeTextFullName = (value) => {
    if (value.trim().length > 0) {
      this.setState({
        name: removeEmojis(value),
        nameFailed: false,
        errorMessageName: "",
      });
    } else {
      this.setState({
        name: value,
      });
    }
  };
  renderContentLoginPhilippines = () => {
    return (
      <View style={styles.form}>
        <Label black style={{ fontSize: 20, marginBottom: 24 }}>
          Please input this to access D2D
        </Label>
        <Label black style={styles.label}>
          Full Name
        </Label>

        <Item
          noShadowElevation
          style={styles.itemInput}
          error={this.state.nameFailed}
        >
          <Input
            newDesign
            // {...testID('inputFullName')}
            accessibilityLabel="input_fullname"
            autoCapitalize="none"
            placeholder={"Enter your full name"}
            // placeholder={'name must be appropriate with IDI'} // register with npa idi
            value={this.state.name}
            placeholderTextColor={platform.placeholderTextColor}
            onChangeText={(txt) => this.onChangeTextFullName(txt)}
          />
        </Item>
        {this.state.nameFailed && (
          <Text textError style={styles.textError}>
            {this.state.errorMessageName}
          </Text>
        )}
        <Button
          {...testID("button_continue")}
          onPress={() => this.doRegisterUser()}
          block
        >
          <Text textButton>CONTINUE</Text>
        </Button>
      </View>
    );
  };

  renderContentLoginNonPhilippines = () => {
    const nav = this.props.navigation;

    return (
      <View>
        <View style={styles.form}>
          <Label style={{ marginTop: 15, marginBottom: 5 }}>Email</Label>
          <Item error={this.state.emailFailed}>
            <Input
              // {...testID('inputEmail')}
              accessibilityLabel="input_email"
              autoCapitalize="none"
              placeholder={"e-mail"}
              keyboardType="email-address"
              placeholderTextColor={platform.placeholderTextColor}
              onChangeText={(txt) => this.setState({ email: txt })}
            />
          </Item>
          <Label style={{ marginTop: 15, marginBottom: 5 }}>Password</Label>
          <Item last error={this.state.passwordFailed}>
            <Input
              // {...testID('inputPassword')}
              autoCapitalize="none"
              accessibilityLabel="input_password"
              placeholder={"password"}
              placeholderTextColor={platform.placeholderTextColor}
              onChangeText={(txt) => this.setState({ password: txt })}
              secureTextEntry={!this.state.passwordVisible}
            />
            <TouchableOpacity
              // {...testID('showPassword')}
              // accessibilityLabel="show_password"
              onPress={() => this._visiblePassword()}
            >
              <Icon
                type="FontAwesome"
                name={this.state.passwordVisible ? "eye" : "eye-slash"}
              />
            </TouchableOpacity>
          </Item>
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            <TouchableOpacity
              style={styles.btnForgot}
              onPress={() => {
                nav.navigate("ForgotPassword");
                AdjustTracker(AdjustTrackerConfig.Login_Forgot_Password);
              }}
              activeOpacity={0.7}
            >
              <Text
                // {...testID('forgotPassword')}
                accessibilityLabel="forgot_password"
                style={styles.textForgot}
              >
                Forgot password?
              </Text>
            </TouchableOpacity>
          </View>

          <ButtonHighlight
            text="LOGIN"
            onPress={() => this.loginEmail()}
            additionalContainerStyle={{marginBottom:20, borderRadius: 10}}
            accessibilityLabel="btnlogin"/>

        </View>
        <View>
          <CardItem style={styles.centerAlign}>
            <View style={styles.underlineText} />
          </CardItem>
          <CardItem
            style={[
              styles.centerAlign,
              {
                top: -10,
                position: "absolute",
                width: "100%",
                backgroundColor: "transparent",
              },
            ]}
          >
            <Text style={styles.regitserTitle}>Login with</Text>
          </CardItem>
          <CardItem style={[styles.centerAlign, { marginTop: 5 }]}>
            {appleAuth.isSupported && (
              <TouchableOpacity
                accessibilityLabel="btnLoginApple"
                style={[styles.btnSocial, styles.btnApple]}
                onPress={() => this.onAppleButtonPress()}
                block
                rounded
              >
                <Icon
                  name="apple"
                  type="MaterialCommunityIcons"
                  style={{ color: "#fff", textAlign: "center" }}
                />
              </TouchableOpacity>
            )}

            <TouchableOpacity
              // {...testID('btnLoginFB')}
              accessibilityLabel="btnloginFB"
              style={[styles.btnSocial, styles.btnFacebook]}
              onPress={() => this.loginFacebook()}
              activeOpacity={0.7}
            >
              <Icon
                name="sc-facebook"
                type="EvilIcons"
                style={{ color: "#fff", textAlign: "center" }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              // {...testID('btnLoginGoogle')}
              accessibilityLabel="btnloginGoogle"
              style={[styles.btnSocial, styles.btnGoogle]}
              onPress={() => this.loginGoogle()}
              block
              rounded
            >
              <Image
                source={require("./../../assets/images/ic_google.png")}
                style={{ width: 18, height: 18 }}
              />
            </TouchableOpacity>
          </CardItem>
          <View style={styles.centerAlign}>
            <Text style={styles.textForgot}>Do not have an account yet? </Text>
          </View>

          <CardItem style={styles.centerAlign}>
            <TouchableOpacity
              // {...testID('btnRegister')}
              accessibilityLabel="btnregister"
              style={[styles.btnSocial, styles.btnRegister]}
              onPress={() => this.doRegister()}
              block
              rounded
            >
              <Text style={styles.textRegister}>REGISTER</Text>
            </TouchableOpacity>
          </CardItem>
        </View>
      </View>
    );
  };

  render() {
    const nav = this.props.navigation;
    const country =
      this.state.country == undefined
        ? nav.state.params.country
        : this.state.country;
    console.log("this.state.country - render ()", this.state.country);
    let isPhilippines =
      typeof country.country_code != "undefined" && country.country_code == "PH"
        ? true
        : false;

    return (
      <Container>
        <View
          style={{
            width: wp("200%"),
            height: wp("200%"),
            borderRadius: wp("100%"),
            backgroundColor: platform.toolbarDefaultBg,
            position: "absolute",
            left: -wp("50%"),
            top: -wp("100%"),
          }}
        />

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showLoader}
          onRequestClose={() => console.log("loader closed")}
        >
          <Loader visible={this.state.showLoader} />
        </Modal>
        <SafeAreaView>
          <Header noShadow>
            <Left style={{ flex: 0.5 }}>
              <Button
                // {...testID('buttonBack')}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={styles.contentColor} />
              </Button>
            </Left>

            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            />
            <Right style={{ flex: 0.5 }} />
          </Header>
        </SafeAreaView>
        <ScrollView>
          <View>
            {/* <ImageBackground source={require('./../../assets/images/bg-blank.png')} 
                        style={{width: platform.deviceWidth, height: platform.deviceHeight/1.5, paddingHorizontal: 10}} /> */}

            <View style={styles.content}>
              <Text style={styles.title}>Welcome!</Text>
              <Card style={styles.card}>
                {isPhilippines
                  ? this.renderContentLoginPhilippines()
                  : this.renderContentLoginNonPhilippines()}
              </Card>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    marginTop: -10,
    padding: 10,
    // marginTop: -platform.deviceHeight/1.5
  },
  title: {
    fontFamily: "Nunito-Bold",
    fontSize: 30,
    textAlign: "center",
    // marginTop: 30,
    marginBottom: 10,
    color: "#fff",
  },
  card: {
    flex: 0,
    paddingVertical: 20,
    paddingBottom: 30,
  },
  centerAlign: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    paddingHorizontal: 20,
  },
  btnLogin: {
    marginBottom: 20,
    borderRadius: 10,
    height: 55,
  },
  regitserTitle: {
    color: "#78849E",
    paddingHorizontal: 15,
    textAlign: "center",
    backgroundColor: "#fff",
  },
  underlineText: {
    borderTopWidth: 1,
    marginHorizontal: 30,
    borderTopColor: "#E0E6ED",
    height: 1,
    flex: 1,
  },
  btnSocial: {
    flex: 1,
    marginBottom: 10,
    zIndex: 1,
    height: 50,
    width: 100,
    marginHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15,
  },
  btnFacebook: {
    backgroundColor: "#3B5998",
    borderWidth: 3,
    borderColor: "#3B5998",
  },
  btnGoogle: {
    backgroundColor: "#FFFFFF",
    borderWidth: 3,
    borderColor: "#EAEAEA",
  },
  btnRegister: {
    backgroundColor: "#FFFFFF",
    borderWidth: 2,
    borderColor: "#DB235D",
  },
  btnEmail: {
    backgroundColor: "#C91D50",
    borderWidth: 3,
    borderColor: "#C91D50",
  },
  btnForgot: {
    marginTop: 20,
    marginBottom: 10,
  },
  textForgot: {
    fontSize: 14,
    color: "#78849E",
    marginBottom: 10,
  },
  textRegister: {
    fontFamily: "Nunito-Bold",
    color: "#DB235D",
    fontSize: 16,
  },
  btnApple: {
    backgroundColor: "#000000",
    borderWidth: 3,
    borderColor: "#000000",
  },
  itemInput: {
    marginBottom: 24,
  },
  label: {
    marginBottom: 12,
  },
  contentColor: {
    color: "#FFF",
  },
  textError: {
    marginTop: -16,
    marginBottom: 16,
  },
});
