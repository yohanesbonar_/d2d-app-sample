import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, AppState, ImageBackground, Modal, BackHandler, Image, Linking, SafeAreaView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Toast, Card, CardItem, Col, Row } from 'native-base';
import { Loader } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import guelogin from './../../libs/GueLogin';
import { testID } from '../../libs/Common';
const gueloginAuth = firebase.app('guelogin');

import { doSendVerification, activateRegisterVerification } from './../../libs/NetworkUtility';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { openInbox } from 'react-native-email-link'

export default class RegisterVerification extends Component {

    totalSeconds = 120
    countTimer = null
    timestampBackgroundInSeconds = 0
    timestampForegroundInSeconds = 0

    state = {
        showLoader: false,
        emailSent: false,
        appState: AppState.currentState,
        email: null,
        countdownResend: '02:00',
        isResend: false
    }

    componentDidMount() {
        let { params } = this.props.navigation.state;

        if (params.success) {
            this.setState({ emailSent: true, email: params.email });
        }

        this.countTimer = setInterval(this.countDownTimer, 1000);

        StatusBar.setHidden(false);
        this.checkEmailVerification(false);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed()
            return true;
        });
        AppState.addEventListener('change', this._handleAppStateChange);

        this.checkDeepLink();

    }

    componentWillUnmount() {
        this.backHandler.remove();
        AppState.removeEventListener('change', this._handleAppStateChange);
        Linking.removeEventListener('url', this.handleOpenURL);
        clearInterval(this.countTimer);
    }

    checkDeepLink() {
        Linking.getInitialURL()
            .then(url => this.handleOpenURL({ url }))
            .catch(err => console.error(err));

        //for app running in background 
        Linking.addEventListener('url', this.handleOpenURL);
    }

    handleOpenURL = async (deeplink) => {
        console.log('handleOpenURL deeplink: ', deeplink)

        if (deeplink.url != null) {
            if (deeplink.url.includes("d2d.co.id/verify")) {
                let splittedUrl = deeplink.url.split("/")

                let uid = splittedUrl[splittedUrl.length - 2]
                let code = splittedUrl[splittedUrl.length - 1]

                let data = {
                    uid: uid,
                    code: code
                }
                this.doActivateRegisterVerification(data)
            }
        }

    }

    openEmailApp = async () => {
        try {
            await openInbox();
        } catch (error) {
            Toast.show({ text: error.message, position: 'bottom', duration: 3000 });
            console.log("openEmailApp: ", error.message);
        }
    }

    countDownTimer = () => {
        --this.totalSeconds;
        let minute = Math.floor((this.totalSeconds) / 60);
        let seconds = this.totalSeconds - (minute * 60);

        if (minute < 10)
            minute = "0" + minute;
        if (seconds < 10)
            seconds = "0" + seconds;

        if (this.totalSeconds >= 0) {
            this.setState({
                countdownResend: minute + ':' + seconds
            })
        } else {
            this.setState({
                isResend: true
            })
            clearInterval(this.countTimer)
        }

        console.log("totalSeconds: ", this.totalSeconds)
    }

    onBackPressed = () => {
        this.gotoTologin()
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App has come to the foreground!')
        }
        this.setState({ appState: nextAppState });
    }

    doActivateRegisterVerification = async (data) => {
        try {
            this.setState({ showLoader: true })
            let params = {
                uid: data.uid,
                code: data.code
            };

            let response = await activateRegisterVerification(params);
            console.log('activateRegisterVerification response: ', response)

            if (response.isSuccess == true) {
                this.setState({ showLoader: false });
                Toast.show({ text: response.message, position: 'bottom', duration: 3000 });
                this.onBackPressed()
            } else {
                Toast.show({ text: response.message, position: 'bottom', duration: 5000 });
                this.setState({ showLoader: false });
            }

        } catch (error) {
            console.log('catch', error);
            Toast.show({ text: error, position: 'top', duration: 5000 });
            this.setState({ showLoader: false })
        }

    }

    async checkEmailVerification(isResendEmail) {
        let { params } = this.props.navigation.state;
        console.log('checkEmailVerification params: ', params)
        if (this.isFromRegister(params)) {
            //from register
            if (params != null) {
                if (params.email != null) {
                    if (isResendEmail == true) {
                        this.sendVerification(params.email);
                    } else {
                        this.setState({ emailSent: true, });
                    }
                }
            } else {
                Toast.show({ text: 'Something went wrong', position: 'bottom', duration: 3000 });
            }

        } else {
            const currentUser = await gueloginAuth.auth().currentUser;
            if (currentUser != null && currentUser._user != null) {
                try {
                    this.sendVerification(currentUser._user.email)
                } catch (error) {
                    Toast.show({ text: error, position: 'bottom', duration: 3000 });
                }
            }

        }
    }

    isFromRegister(params) {
        if (params != null) {
            if (params.email != null) {
                return true;
            }
        }
        return false;
    }

    async sendVerification(email) {
        try {
            this.setState({ showLoader: true })
            let params = {
                email: email,
            };

            let response = await doSendVerification(params);
            console.log('sendVerification response: ', response)

            setTimeout(() => {
                if (response.isSuccess == true) {
                    this.setState({ emailSent: true, showLoader: false, isResend: false });

                    this.totalSeconds = 120
                    this.countTimer = setInterval(this.countDownTimer, 1000);

                    Toast.show({ text: response.message, position: 'bottom', duration: 3000 });
                } else {
                    console.warn('response', response.message);
                    Toast.show({ text: response.message, position: 'bottom', duration: 5000 });
                    this.setState({ emailSent: false, showLoader: false });
                }
            }, 1000)

        } catch (error) {
            console.warn('catch', error);
            Toast.show({ text: error, position: 'top', duration: 5000 });
            this.setState({ showLoader: false, emailSent: false })
        }
    }

    gotoTologin() {
        // this.props.navigation.replace('Login');
        this.props.navigation.goBack()
        //this.logout();
    }

    async logout() {

        const user = gueloginAuth.auth().currentUser;
        let topicFCM = await subscribeFcm();
        if (user) {

            try {
                let response = await deleteFIrebaseToken();
                if (response.isSuccess == true || response.message == 'No available device token to be removed') {

                    gueloginAuth.auth().signOut()
                        .then(() => {
                            let setLogin = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.IS_LOGIN, 'N');
                            let uid = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.UID, null);
                            let profile = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, null);
                            let auth = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.AUTHORIZATION, null);
                            let fcm = async () => await firebase.messaging().unsubscribeFromTopic(topicFCM);

                            setTimeout(() => {
                                // let resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login' })] });
                                // this.props.navigation.dispatch(resetAction);
                                //prevent bugs: blink open reg hello before to login
                                this.props.navigation.replace('Login');

                            }, 300);
                        })
                        .catch((error) => {
                            Toast.show({ text: error, position: 'top', duration: 3000 });
                        })
                } else {
                    Toast.show({ text: response.message, position: 'top', duration: 3000 });
                }
            } catch (error) {
                console.log(error);
            }

        }
    }

    render = () => {
        return (
            <Container>
                <Modal animationType="slide" transparent={true} visible={this.state.showLoader} onRequestClose={() => console.log('loader closed')}>
                    <Loader visible={this.state.showLoader} />
                </Modal>
                <SafeAreaView>

                    <Header newDesign androidStatusBarColor={'#fff'}
                        iosBarStyle={'dark-content'}>
                        <Left newDesign>
                            <TouchableOpacity
                                accessibilityLabel="button_back"
                                transparent
                                onPress={() => this.onBackPressed()}
                            >
                                <Image
                                    resizeMode={"contain"}
                                    source={require("./../../assets/images/back.png")}
                                    style={{ width: 48, height: 48 }}
                                />
                            </TouchableOpacity>
                        </Left>
                        <Body newDesign>
                            <Title newDesign>Account Activation</Title>
                        </Body>

                    </Header>
                </SafeAreaView>

                <View style={{
                    flex: 1,
                }} >
                    <Col style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingHorizontal: 16
                    }}>

                        <Image
                            resizeMode='contain'
                            source={require('./../../assets/images/email-verification.png')} style={{ width: 106, height: 74, marginBottom: 32 }} />
                        <Text bold textLarge style={{ marginBottom: 16 }}>Check Verification</Text>
                        <Text style={{ textAlign: 'center', lineHeight: 26 }}>
                            <Text regular>{'We have sent an activation link for you to\n'}</Text>
                            <Text regular bold >{this.state.email},</Text>
                            <Text regular> please check.</Text>
                        </Text>

                        <Button
                            onPress={() => this.openEmailApp()}
                            block style={{ marginTop: 32 }}>
                            <Text textButton>Open Email App</Text>
                        </Button>
                    </Col>

                </View>

                <View style={{ position: 'absolute', bottom: 0, right: 0, left: 0, backgroundColor: '#F6F6F7' }}>

                    <Row style={{ justifyContent: 'space-between', alignItems: 'center', padding: 16 }}>
                        <Text regular>Didn’t receive verification?</Text>

                        <TouchableOpacity
                            disabled={!this.state.isResend}
                            onPress={() =>
                                this.checkEmailVerification(true)
                            }>
                            <Text textAsButton>{this.state.isResend ? "Resend" : this.state.countdownResend}</Text>
                        </TouchableOpacity>
                    </Row>
                </View>
            </Container>
        )
    }

    renderOld() {

        return (
            <Container>
                <Modal animationType="slide" transparent={true} visible={this.state.showLoader} onRequestClose={() => console.log('loader closed')}>
                    <Loader visible={this.state.showLoader} />
                </Modal>
                <SafeAreaView>
                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.gotoTologin()}>
                                <Icon name='md-arrow-back' style={{ color: '#fff' }} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Email Verification</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <View>
                    <ImageBackground source={require('./../../assets/images/bg-blank.png')}
                        style={{ width: platform.deviceWidth, height: platform.deviceHeight / 1.5, paddingHorizontal: 10 }} />
                    <View style={styles.content}>
                        <Card style={styles.card}>
                            <Text style={styles.title}>Email Verification</Text>
                            <View style={[styles.centerAlign, { padding: 20 }]}>
                                <Text style={styles.body}>
                                    We have sent an activation link to your email address
                                </Text>
                            </View>
                            <View style={styles.centerAlign}>
                                <View style={styles.circleIcon}>
                                    <Icon
                                        name={this.state.emailSent ? 'ios-checkmark' : 'ios-close'}
                                        style={{ fontSize: 80, color: this.state.emailSent ? platform.brandSuccess : platform.brandDanger }} />
                                </View>
                            </View>
                            <View style={styles.centerAlign}>
                                <Text>
                                    You are not receiving emails?
                                    <Text
                                        // {...testID('buttonResend')}
                                        accessibilityLabel="button_resend"
                                        style={styles.textResend} onPress={() => this.checkEmailVerification(true)}> resend</Text>
                                </Text>
                            </View>
                        </Card>
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 10,
        marginTop: -platform.deviceHeight / 1.5
    },
    card: {
        flex: 0,
        paddingVertical: 30,
    },
    centerAlign: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontFamily: 'Nunito-Bold',
        fontSize: 30,
        textAlign: 'center'
    },
    body: {
        textAlign: 'center',
        fontSize: 14,
        lineHeight: 18
    },
    btnResend: {
        marginTop: -3,
    },
    textResend: {
        fontSize: 14,
        color: platform.brandInfo,
    },
    circleIcon: {
        width: 80,
        height: 80,
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#EAEAEA',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    }
});
