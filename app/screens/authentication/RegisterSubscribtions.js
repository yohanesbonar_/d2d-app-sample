import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Modal, ImageBackground, TouchableOpacity, Platform, ScrollView, SafeAreaView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Form, Item, Input, Label, Toast, Card, CardItem } from 'native-base';
import { Loader } from './../../components';
import { validateEmail, STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import guelogin from './../../libs/GueLogin';
const gueloginAuth = firebase.app('guelogin');
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { translate } from './../../libs/localize/LocalizeHelper'
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';

export default class RegisterSubscribtions extends Component {

    state = {
        subscribtionList: [],
    }

    constructor(props) {
        super(props);
    }


    profile = null;

    componentDidMount() {
        StatusBar.setHidden(false);
        this.getDataSubs()
    }

    getDataSubs = () => {
        this.profile = this.props.navigation.state.params;
        console.log('init subs', this.profile)
        if (this.profile != null) {
            this.setState({ subscribtionList: this.profile.subscribtions })
        }
    }

    gotoSelectSpecialist = () => {
        if (this.profile == null) {
            console.log('profile null')
            return;
        }

        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSpecialist',
            key: `gotoSelectSubscribtions`,
            params: {
                specialistList: this.profile.subscribtions,
                saveDataSp: this.updateDataSp,
                from: 'SUBSCRIBTIONS'
            }
            ,
        }));
    }

    updateDataSp = (subscribtionList) => {
        this.profile.subscribtions = subscribtionList;
        this.setState({ subscribtionList: subscribtionList })
        console.log('update profile', this.profile)
    }


    gotoInputPracticeLocation = () => {
        if (this.state.subscribtionList.length > 0) {
            // Adjust Tracker
            AdjustTracker(AdjustTrackerConfig.Onboarding_Subs_Next);

            this.props.navigation.dispatch(NavigationActions.navigate({
                routeName: 'RegisterPracticeLocation',
                key: `gotoRegPracticeLoc`,
                params: this.profile,
            }));
        }
    }


    renderEmptySpecilist = () => {
        return (<View style={styles.viewEmpty}>
            <View style={{ flexDirection: 'column' }}>
                <Text style={styles.textSpesialis}>Subscription</Text>
                <Item onPress={() => {
                    // Adjust Tracker
                    AdjustTracker(AdjustTrackerConfig.Onboarding_Subs);

                    this.gotoSelectSpecialist();
                }}
                    last style={styles.itemInput}>
                    <Icon name={'book'} type={'FontAwesome'} style={{ color: '#78849E', fontSize: 22 }} />
                    <Input
                        pointerEvents="none"
                        editable={false}
                        autoCapitalize="none"
                        placeholder={translate('hint_example_specialist')}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.setState({ password: txt })} />
                </Item>
            </View>
        </View>
        )
    }

    renderData = () => {
        return <View style={{ flex: 1, marginBottom: 10, }}>
            {
                <ScrollView>
                    {this.renderSubscribtions()}
                </ScrollView>

            }
            <TouchableOpacity onPress={() => {
                // Adjust Tracker
                AdjustTracker(AdjustTrackerConfig.Onboarding_Subs_Add);

                this.gotoSelectSpecialist()
            }}
                // {...testID('add')} 
                accessibilityLabel="add"
                style={{ padding: 10 }}>
                <Text style={styles.textAdd}>Add +</Text>
            </TouchableOpacity>
        </View>;
    }

    renderSubscribtions = () => {
        let items = []
        let self = this;
        this.state.subscribtionList.map(function (d, i) {
            if (d.id > 0) {
                items.push(
                    <Card style={{ paddingVertical: 10 }} key={i}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name={'book'} type={'FontAwesome'} style={styles.iconSpesialist} />
                            <View style={styles.viewTextNoborder}>
                                <TouchableOpacity
                                    // {...testID('cancel')}
                                    accessibilityLabel="cancel"
                                    onPress={() => self.doDeleteSubscribtion(self, i)}>
                                    <Icon name={'ios-close-circle'} style={styles.iconCancel} />
                                </TouchableOpacity>
                                <Text style={{ color: '#78849E', fontSize: 16, flex: 1 }}>{d.description}</Text>

                            </View>
                        </View>
                    </Card>
                )
            }

        });

        return items;
    }

    doDeleteSubscribtion = (self, index) => {
        self.state.subscribtionList.splice(index, 1);
        self.setState({ subscribtionList: self.state.subscribtionList })
    }

    render() {
        let btnColor = this.state.subscribtionList.length > 0 ? '#3AE194' : '#BAC3BE';

        return (
            <Container style={styles.backgroundColor}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >
                    <Header noShadow style={styles.backgroundColor}>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon name='md-arrow-back' style={styles.contentColor} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title style={styles.contentColor}>Input Your Data</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <View style={{ flex: 1, flexDirection: 'column', paddingHorizontal: 25 }}>
                    <View style={{ flex: 0.2, justifyContent: 'center' }}>
                        <Text style={styles.textTitle}>Select Subscription</Text>
                    </View>
                    <View style={{ flex: 0.6 }}>
                        {this.state.subscribtionList.length <= 0 && this.renderEmptySpecilist()}
                        {this.state.subscribtionList.length > 0 && this.renderData()}
                    </View>
                    <View style={{ flex: 0.2 }}>
                        <View style={styles.btnNextRegister}>
                            <TouchableOpacity
                                // {...testID('btnLanjut')}
                                accessibilityLabel="btn_lanjut"
                                style={[styles.touchBtnRegister, { backgroundColor: btnColor }]}
                                onPress={() => this.gotoInputPracticeLocation()} block rounded>
                                <Text style={styles.textNext}>NEXT</Text>
                            </TouchableOpacity>
                            <View style={styles.viewGrey} />
                            <View style={styles.viewGreen(this.props.navigation.state.params.country_code)} />
                        </View>
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: '#F7F7FA'
    },
    contentColor: {
        color: '#78849E'
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
    },
    touchBtnRegister: {
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: (country_code) => [{
        width: country_code != "PH" ? "66.666666666667%" : "60%",
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    }],
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
    },
    viewEmpty: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 25,
        marginTop: 10
    },
    textSpesialis: {
        color: '#78849E',
        margin: 10,
        fontSize: 16
    },
    itemInput: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textAdd: {
        color: '#0EA2DD',
        textAlign: 'right',
        fontSize: 18,
        fontFamily: 'Nunito-Bold',
    },
    iconSpesialist: {
        color: '#78849E',
        marginHorizontal: 15,
        fontSize: 25
    },
    viewTextNoborder: {
        flex: 1,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewTextBorderTop: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#D7DEE6'
    },
    iconCancel: {
        color: '#D7DEE6',
        marginRight: 10,
    }
});
