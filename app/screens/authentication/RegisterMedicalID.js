import React, { Component } from "react";
import firebase from "react-native-firebase";
import { NavigationActions } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  BackHandler,
  ImageBackground,
  TouchableOpacity,
  Platform,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Form,
  Item,
  Input,
  Label,
  Toast,
  Card,
  CardItem,
} from "native-base";
import { Loader } from "./../../components";
import {
  validateEmail,
  STORAGE_TABLE_NAME,
  testID,
  removeEmojis,
  onlyAlphaNumeric,
  onlyNumeric,
} from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
import guelogin from "./../../libs/GueLogin";
const gueloginAuth = firebase.app("guelogin");
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";

import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { translate } from "./../../libs/localize/LocalizeHelper";
import _ from "lodash";
import ButtonHighlight from "../../../src/components/atoms/ButtonHighlight";

export default class RegisterMedicalID extends Component {
  state = {
    specialistList: [],
    npaIDI: "",
    npaIDITemp: "",
    isFocused: false,
    errorMessageMedicalId: "",
    npaIdiFailed: false,
    activeButtonNext: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    this.getDataSp();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  profile = null;

  getDataSp = () => {
    this.profile = this.props.navigation.state.params;

    console.log("profile: ", this.profile);
    if (this.profile != null) {
      this.setState({
        npaIDI: this.profile.npaIDI,
        npaIDITemp: this.profile.npaIdi,
        npaIdiFailed:
          _.size(this.profile.npaIdi) > 0 && _.size(this.profile.npaIdi) < 3
            ? true
            : false,
        errorMessageMedicalId:
          _.size(this.profile.npaIdi) > 0 && _.size(this.profile.npaIdi) < 3
            ? "Medical ID min. 3 character"
            : "",
        activeButtonNext:
          _.size(this.profile.npaIdi) >= 0 && _.size(this.profile.npaIdi) < 3
            ? false
            : true,
      });
    }
  };

  goToRegisterSpecialist = () => {
    console.log("test123");
    if (!this.state.npaIdiFailed) {
      this.props.navigation.dispatch(
        NavigationActions.navigate({
          routeName: "RegisterSpecialist",
          key: `gotoInputSpecialist`,
          params: this.profile,
        })
      );
    }
  };

  onTextChanged = (text) => {
    let afterRemoveEmoji = removeEmojis(text);
    let textTemp = "";
    if (this.props.navigation.state.params.country_code == "ID") {
      textTemp = onlyNumeric(afterRemoveEmoji);
    } else {
      textTemp = onlyAlphaNumeric(afterRemoveEmoji);
    }

    this.setState({ npaIDITemp: textTemp });
    this.profile.npaIdi = textTemp;

    if (_.size(textTemp) >= 0 && _.size(textTemp) < 3) {
      this.setState({
        npaIdiFailed: true,
        errorMessageMedicalId: "Medical ID min. 3 character",
        activeButtonNext: false,
      });
    } else {
      this.setState({
        npaIdiFailed: false,
        errorMessageMedicalId: "",
        activeButtonNext: true,
      });
    }
  };

  render() {
    let btnColor =
      !this.state.npaIdiFailed && this.state.activeButtonNext
        ? "#3AE194"
        : "#BAC3BE";

    return (
      <Container style={styles.backgroundColor}>
        <SafeAreaView>
          <Header noShadow style={styles.backgroundColor}>
            <Left style={{ flex: 0.5 }}>
              <Button
                // {...testID('buttonBack')}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={styles.contentColor} />
              </Button>
            </Left>
            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Title style={styles.contentColor}>Input Your Data</Title>
            </Body>
            <Right style={{ flex: 0.5 }} />
          </Header>
        </SafeAreaView>
        <View
          style={{ flex: 1, flexDirection: "column", paddingHorizontal: 25 }}
        >
          <View style={{ flex: 0.3, justifyContent: "center" }}>
            <Text style={styles.textTitle}>
              What's{`\n`}your Medical ID Number?
            </Text>
          </View>
          <KeyboardAvoidingView
            behavior="padding"
            enabled={platform.platform == "ios" ? true : false}
            style={{
              flex: 1,
              justifyContent: "flex-end",
              paddingBottom: 0,
            }}
            keyboardVerticalOffset={
              platform.platform == "ios"
                ? Platform.isPad ? 76 :
                  platform.deviceHeight >= 812 && platform.deviceWidth >= 375
                    ? 100
                    : 76
                : 0
            }
          >
            <View
              style={{
                paddingBottom: 16,
                marginBottom:
                platform.platform == "ios"
                  ? Platform.isPad
                    ? 34
                    : platform.deviceHeight >= 812 &&
                      platform.deviceWidth >= 375
                    ? 34
                    : 8
                  : 8,
              }}
            >
              <View
                style={{
                  height: 100,
                  marginBottom: this.state.npaIdiFailed ? 10 : 28,
                  marginTop: 10,
                }}
              >
                <View
                  style={{
                    justifyContent: "flex-end",
                    flex: 1,
                  }}
                >
                  <View style={{ flexDirection: "column" }}>
                    <Label black style={styles.textLabel}>
                      Medical ID Number
                    </Label>
                    <Item
                      noShadowElevation
                      style={[styles.itemInput]}
                      // error={this.state.npaIdiFailed}
                    >
                      <Input
                        newDesign
                        placeholder={"Your Medical ID Number"}
                        value={this.state.npaIDITemp}
                        placeholderTextColor={platform.placeholderTextColor}
                        maxLength={
                          this.props.navigation.state.params.country_code ==
                          "SG"
                            ? 20
                            : 50
                        }
                        keyboardType={
                          this.props.navigation.state.params.country_code ==
                          "ID"
                            ? "number-pad"
                            : platform.platform === "ios"
                            ? "ascii-capable"
                            : "visible-password"
                        }
                        onChangeText={(txt) => this.onTextChanged(txt)}
                        {...testID("input_npaidi")}
                        onFocus={() => this.setState({ isFocused: true })}
                        onBlur={() => this.setState({ isFocused: false })}
                      />
                    </Item>
                    {this.state.npaIdiFailed && (
                      <Text textError style={styles.textError}>
                        {this.state.errorMessageMedicalId}
                      </Text>
                    )}
                  </View>
                </View>
              </View>
              <View style={{ height: 87 }}>
                <View style={styles.btnNextRegister}>
                  <ButtonHighlight
                    // {...testID('btnLanjut')}
                    accessibilityLabel="btn_lanjut"
                    text="NEXT"
                    buttonColor={
                      !this.state.npaIdiFailed && this.state.activeButtonNext
                        ? null
                        : "#BAC3BE"
                    }
                    buttonUnderlayColor={
                      !this.state.npaIdiFailed && this.state.activeButtonNext
                        ? null
                        : "#D7D7D7"
                    }
                    onPress={() => this.goToRegisterSpecialist()}
                    additionalContainerStyle={{
                      ...styles.touchBtnRegister,
                      paddingVertical: null,
                      paddingHorizontal: null,
                      flex: 0,
                      height: null,
                    }}
                    additionalTextStyle={{
                      ...styles.textNext,
                      lineHeight: null,
                    }}
                    disabled={
                      !this.state.npaIdiFailed && this.state.activeButtonNext
                        ? false
                        : true
                    }
                  />
                  <View style={styles.viewGrey} />
                  <View style={styles.viewGreen} />
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  backgroundColor: {
    backgroundColor: "#F7F7FA",
  },
  contentColor: {
    color: "#78849E",
  },
  btnNextRegister: {
    backgroundColor: "#F7F7FA",
    flexDirection: "column",
    flex: 1,
    height: "100%",
  },
  touchBtnRegister: {
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    padding: 15,
    // backgroundColor: '#3AE194',
  },
  textNext: {
    fontFamily: "Nunito-Bold",
    color: "white",
    fontSize: 18,
  },
  viewGrey: {
    backgroundColor: "#EAEAEA",
    height: 12,
    borderRadius: 15,
  },
  viewGreen: {
    width: "34%",
    top: -12,
    backgroundColor: "#3AE194",
    height: 12,
    borderRadius: 15,
  },
  textTitle: {
    color: "#78849E",
    fontSize: 28,
    fontFamily: "Nunito-Bold",
  },
  viewEmpty: {
    justifyContent: "flex-end",
    flex: 1,
    marginBottom: 25,
    marginTop: 10,
  },
  textSpesialis: {
    color: "#78849E",
    margin: 10,
    fontSize: 16,
  },
  itemInput: {
    backgroundColor: "white",
    alignItems: "center",
  },
  textAdd: {
    color: "#0EA2DD",
    textAlign: "right",
    fontSize: 18,
    fontFamily: "Nunito-Bold",
  },
  iconSpesialist: {
    color: "#78849E",
    marginHorizontal: 15,
    fontSize: 28,
  },
  iconSubSpecialist: {
    color: "white",
    marginHorizontal: 15,
    fontSize: 28,
  },
  viewTextNoborder: {
    flex: 1,
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center",
  },
  viewTextBorderTop: {
    flex: 1,
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center",
    borderTopWidth: 2,
    borderTopColor: "#D7DEE6",
    paddingTop: 5,
    marginTop: 5,
  },
  iconCancel: {
    color: "#D7DEE6",
    marginRight: 10,
  },
  textSpesialis: {
    color: "#78849E",
    margin: 10,
    fontSize: 16,
  },
  textLabel: {
    marginBottom: 12,
  },
  textError: {
    marginTop: 4,
    marginLeft: 4,
  },
});
