import React, { Component } from "react";
import firebase from "react-native-firebase";
import { NavigationActions, StackActions } from "react-navigation";
import {
  StatusBar,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  BackHandler,
  SafeAreaView,
  Platform,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Button,
  Icon,
  Text,
  Label,
  Col,
  Item,
  Content,
} from "native-base";
import { testID, STORAGE_TABLE_NAME } from "./../../libs/Common";
import guelogin from "./../../libs/GueLogin";
const gueloginAuth = firebase.app("guelogin");
import AsyncStorage from "@react-native-community/async-storage";

import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import Geocoder from "react-native-geocoder";
import RNLocation from "react-native-location";
import country from "../../libs/countries/assets/data/country.json";
import { Modalize } from "react-native-modalize";
import CountrySelection from "../../libs/countries/CountrySelection";
import { setI18nConfig } from "../../libs/localize/LocalizeHelper";
import {
  IconCambodiaFlag,
  IconIndonesiaFlag,
  IconMyanmarFlag,
  IconPhilippinesFlag,
  IconSingaporeFlag,
} from "../../../src/assets";
import ButtonHighlight from "../../../src/components/atoms/ButtonHighlight";
import platform from "../../../theme/variables/d2dColor";

export default class RegisterHello extends Component {
  constructor(props) {
    super(props);

    let checkSelectedCountry = null;
    let countryCode = this.props.navigation.state.params.profile.country_code;
    if (countryCode != null) {
      checkSelectedCountry = country.filter(function(element) {
        if (element.country_code == countryCode) {
          return true;
        }
      });
      checkSelectedCountry = checkSelectedCountry[0];
    } else {
      checkSelectedCountry = this.props.navigation.state.params.country;
    }

    this.state = {
      name: "",
      selectedCountry: checkSelectedCountry,
      isChooseCountry: null,
    };
  }

  profile = null;

  getProfile = async () => {
    let { params } = this.props.navigation.state;

    console.log("params: ", params);

    if (params != null && params.country != null) {
      let jsonCountry = JSON.stringify(params.country);
      await AsyncStorage.setItem(STORAGE_TABLE_NAME.COUNTRY, jsonCountry);
    }

    if (params != null && params.profile != null) {
      if (params.profile.country_code == null) {
        this.setState({
          isChooseCountry: true,
        });
        //this.requestLocationPermission()
      } else {
        this.setState({
          isChooseCountry: false,
        });
      }

      console.log(params.profile);

      let profileReg = {
        id: params.profile.id,
        uid: params.profile.uid,
        name: params.profile.name,
        npaIdi: params.profile.npa_idi,
        email: params.profile.email,
        specialist: [],
        subscribtions: [],
        homeLocation: null,
        practiceLocations: [],
        country_code:
          params.profile.country_code != null
            ? params.profile.country_code
            : this.state.selectedCountry.country_code,
        // born_date: null
        // clinic_location_1: null
        // clinic_location_2: null
        // clinic_location_3: null
        // counting: { bookmark: null, download: null }
        // education_1: null
        // education_2: null
        // education_3: null
        // email: "ekades100@yopmail.com"
        // home_location: null
        // id: 71
        // name: "asikah"
        // npa_idi: "71259"
        // phone: null
        // photo: null
        // spesialization: []
        // subscription: []
        // uid: "WADNxQRE4cW8jFB4VBtYctIExB33"
      };

      this.profile = profileReg;

      this.setConfig();
      this.setState({ name: profileReg.name });
    }
  };

  getCurrentNameLocation = async (coordinate) => {
    console.log("getCurrentNameLocation coordinate: ", coordinate);

    Geocoder.geocodePosition(coordinate)
      .then((res) => {
        // res is an Array of geocoding object
        console.log("getCurrentNameLocation res: ", res);

        let resultCountry = country.filter((obj) => {
          return res[0].country.includes(obj.name);
        });

        console.log("result: ", resultCountry);
        if (typeof resultCountry[0] != "undefined") {
          this.setState({
            selectedCountry: resultCountry[0],
          });
          this.profile.country_code = resultCountry[0].country_code;
          this.setConfig();
        }
      })
      .catch((err) => console.log("getCurrentNameLocation error: ", err));
  };

  requestLocationPermission = () => {
    setTimeout(() => {
      RNLocation.requestPermission({
        ios: "whenInUse",
        android: {
          detail: "coarse",
        },
      }).then((granted) => {
        console.log("requestPermission granted: ", granted);
        if (granted) {
          RNLocation.configure({ distanceFilter: 0 });
          RNLocation.getLatestLocation({ timeout: 60000 }).then(
            (latestLocation) => {
              // Use the location here
              console.log("latestLocation: ", latestLocation);
              if (latestLocation != null) {
                let coordinate = {
                  lat: latestLocation.latitude,
                  lng: latestLocation.longitude,
                };
                this.getCurrentNameLocation(coordinate);
              }
            }
          );
        }
      });
    }, 1000);
  };

  componentDidMount() {
    StatusBar.setHidden(false);
    this.getProfile();

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.logout();
  };

  setConfig = async () => {
    let jsonProfile = JSON.stringify(this.profile);
    await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, jsonProfile);
    await setI18nConfig(); // set initial config
  };

  gotoInputSpecialist = async () => {
    await this.setConfig();
    // Adjust Tracker
    AdjustTracker(AdjustTrackerConfig.Onboarding_Next);

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        // routeName: "RegisterSpecialist",
        routeName:
          this.profile.country_code != "PH"
            ? "RegisterMedicalID"
            : "RegisterSpecialist",
        key:
          this.profile.country_code != "PH"
            ? "gotoInputMedicalID"
            : "gotoInputSpecialist",
        params: this.profile,
      })
    );
  };

  logout() {
    const user = gueloginAuth.auth().currentUser;

    if (user) {
      gueloginAuth
        .auth()
        .signOut()
        .then(() => {
          let setLogin = async () =>
            await AsyncStorage.setItem("IS_LOGIN", "N");
          this.gotoTologin();
        })
        .catch((error) => {
          console.log(error);
          Toast.show({ text: error, position: "top", duration: 3000 });
          this.gotoTologin();
        });
    } else {
      let setLogin = async () => await AsyncStorage.setItem("IS_LOGIN", "N");
      this.gotoTologin();
    }
  }

  gotoTologin() {
    let { profile } = this.props.navigation.state.params;
    let { country } = this.props.navigation.state.params;

    let params = {
      ...profile,
      country,
    };
    console.log("gotoTologin ", params);
    setTimeout(() => {
      this.resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: "Login", params: params }),
        ],
      });
      this.props.navigation.dispatch(this.resetAction);
    }, 500);
    //  this.props.navigation.goBack()
  }

  _onSelectCountry = async (country) => {
    this.profile.country_code = country.country_code;
    this.setState({
      selectedCountry: country,
    });
    this.setConfig();
  };

  _renderCountries = () => {
    let countryName =
      this.state.selectedCountry == null
        ? "Indonesia"
        : this.state.selectedCountry.name;

    return (
      <View style={{ width: "100%" }}>
        <Label black style={styles.label}>
          Country
        </Label>
        <Item
          noShadowElevation
          {...testID("input_select_country")}
          // onPress={() => this.modalizeBottomSheet.open()}
          style={[
            styles.itemInput,
            { height: 56, alignItems: "center", paddingRight: 0 },
          ]}
        >
          <View style={{ marginRight: 12 }}>{this._renderSelectedFlag()}</View>

          <Text regular>{countryName}</Text>

          {/* <Right>

                        <Image
                            resizeMode={"contain"}
                            source={require("./../../assets/images/arrow-down.png")}
                            style={{ width: 48, height: 48 }}
                        />
                    </Right> */}
        </Item>
      </View>
    );
  };

  _renderSelectedFlag = () => {
    let countryName =
      this.state.selectedCountry == null
        ? "Indonesia"
        : this.state.selectedCountry.name;

    if (countryName == "Cambodia") {
      return (
        // <Image
        //   resizeMode={"contain"}
        //   source={require("./../../assets/images/flags/Cambodia.png")}
        //   style={{ width: 24, height: 24, marginRight: 12 }}
        // />
        <IconCambodiaFlag />
      );
    } else if (countryName == "Myanmar") {
      return <IconMyanmarFlag />;
    } else if (countryName == "Philippines") {
      return <IconPhilippinesFlag />;
    } else if (countryName == "Singapore") {
      return <IconSingaporeFlag />;
    } else {
      return <IconIndonesiaFlag />;
    }
  };

  _renderBottomSheetCountry = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        ref={(ref) => {
          this.modalizeBottomSheet = ref;
        }}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        HeaderComponent={
          <View
            style={{
              height: 4,
              borderRadius: 2,
              width: 40,
              marginTop: 8,
              backgroundColor: "#454F6329",
              alignSelf: "center",
            }}
          />
        }
      >
        <CountrySelection
          selected={
            this.state.selectedCountry == null
              ? "Indonesia"
              : this.state.selectedCountry.name
          }
          action={(item) =>
            this._onSelectCountry(item) +
            console.log("action: ", item) +
            this.modalizeBottomSheet.close()
          }
        />
      </Modalize>
    );
  };

  renderChooseCountry = () => {
    return (
      <Container>
        {this._renderBottomSheetCountry()}
        <SafeAreaView>
          <Header
            newDesign
            androidStatusBarColor={"#fff"}
            iosBarStyle={"dark-content"}
          >
            <Left newDesign>
              <TouchableOpacity
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Image
                  resizeMode={"contain"}
                  source={require("./../../assets/images/back.png")}
                  style={{ width: 48, height: 48 }}
                />
              </TouchableOpacity>
            </Left>
            <Body newDesign>
              <Title newDesign>Input Your Data</Title>
            </Body>
          </Header>
        </SafeAreaView>

        <Content
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: "center",
          }}
          style={{
            backgroundColor: "#fff",
            paddingHorizontal: 16,
          }}
        >
          <Col
            style={{
              justifyContent: "center",
              alignItems: "center",
              flex: 1,
            }}
          >
            <Image
              resizeMode="contain"
              source={require("./../../assets/images/D2D-logo.png")}
              style={{ width: 82, height: 82, marginBottom: 32 }}
            />
            <Text bold textLarge style={{ marginBottom: 16 }}>
              {this.state.name}
            </Text>
            <Text
              style={{ textAlign: "center", lineHeight: 26, marginBottom: 32 }}
            >
              <Text regular>
                Thanks for your registration. There will be several step to
                finish your profile. Please complete your data!
              </Text>
            </Text>

            {this._renderCountries()}

            <Button
              {...testID("button_next")}
              onPress={() => this.gotoInputSpecialist()}
              block
              style={{ marginTop: 8 }}
            >
              <Text textButton>Next</Text>
            </Button>
          </Col>

          <View
            style={{
              position: "absolute",
              bottom: 24,
              right: 0,
              left: 0,
            }}
          >
            <View
              style={{
                backgroundColor: "#F6F6F7",
                height: 8,
                borderRadius: 10,
              }}
            />
            <View
              style={{
                width: "20%",
                top: -8,
                backgroundColor: "#00A849",
                height: 8,
                borderRadius: 10,
              }}
            />
          </View>
        </Content>
      </Container>
    );
  };
  render() {
    if (this.state.isChooseCountry == true) {
      return this.renderChooseCountry();
    } else if (this.state.isChooseCountry == false) {
      return (
        <Container style={styles.backgroundColor}>
          <SafeAreaView>
            <Header noShadow style={styles.backgroundColor}>
              <Left style={{ flex: 0.5 }}>
                <Button
                  // {...testID('buttonBack')}
                  accessibilityLabel="button_back"
                  transparent
                  onPress={() => this.logout()}
                >
                  <Icon name="md-arrow-back" style={styles.contentColor} />
                </Button>
              </Left>
              <Body
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Title style={styles.contentColor}>Input Your Data</Title>
              </Body>
              <Right style={{ flex: 0.5 }} />
            </Header>
          </SafeAreaView>
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              paddingHorizontal: 25,
              marginBottom:
                platform.platform == "ios"
                  ? Platform.isPad
                    ? 34
                    : platform.deviceHeight >= 812 &&
                      platform.deviceWidth >= 375
                    ? 34
                    : 8
                  : 8,
              paddingBottom: 16,
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "flex-end",
                marginBottom: 20,
              }}
            >
              <Image
                source={require("./../../assets/images/new_icon.png")}
                style={styles.icond2d}
              />
              <Text style={styles.textTitle}>Hi {this.state.name}</Text>
              <Text style={styles.textDescription}>
                Thanks for your registration. There will be several step to
                finish your profile. Please complete your data!
              </Text>
            </View>
            <View
              style={{
                height: 86,
              }}
            >
              <View style={styles.btnNextRegister}>
                <ButtonHighlight
                  // {...testID('btnLanjut')}
                  accessibilityLabel="btn_lanjut"
                  text="NEXT"
                  onPress={() => this.gotoInputSpecialist()}
                  additionalContainerStyle={{
                    ...styles.touchBtnRegister,
                    paddingVertical: null,
                    paddingHorizontal: null,
                    flex: 0,
                    height: null,
                  }}
                  additionalTextStyle={{ ...styles.textNext, lineHeight: null }}
                />
                <View style={styles.viewGrey} />
                <View style={styles.viewGreen(this.state.selectedCountry)} />
              </View>
            </View>
          </View>
        </Container>
      );
    }

    return null;
  }
}

const styles = StyleSheet.create({
  backgroundColor: {
    backgroundColor: "#F7F7FA",
  },
  contentColor: {
    color: "#78849E",
  },
  icond2d: {
    width: 65,
    height: 65,
    marginBottom: 10,
  },
  btnNextRegister: {
    backgroundColor: "#F7F7FA",
    flexDirection: "column",
    flex: 1,
    height: "100%",
  },
  touchBtnRegister: {
    marginBottom: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    padding: 15,
    // backgroundColor: '#3AE194',
  },
  textNext: {
    fontFamily: "Nunito-Bold",
    color: "white",
    fontSize: 18,
  },
  viewGrey: {
    backgroundColor: "#EAEAEA",
    height: 12,
    borderRadius: 15,
  },
  viewGreen: (countryName) => [
    {
      width: countryName != "Philippines" ? "16.6666666667%" : "20%",
      top: -12,
      backgroundColor: "#3AE194",
      height: 12,
      borderRadius: 15,
    },
  ],
  textTitle: {
    color: "#78849E",
    fontSize: 30,
    fontFamily: "Nunito-Bold",
    marginBottom: 10,
  },
  textDescription: {
    color: "#78849E",
    fontSize: 18,
    fontFamily: "Nunito",
    marginBottom: 10,
  },
  itemInput: {
    marginBottom: 24,
    height: 56,
    alignItems: "center",
  },
  label: {
    marginBottom: 12,
  },
});
