import React, { Component } from 'react';
import { StyleSheet, Alert, SafeAreaView, BackHandler } from 'react-native';
import { Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right, Toast } from 'native-base';
// import { VideoPlayer } from './../components';
import VideoPlayer from 'react-native-video-controls';
import { ParamAction, ParamContent, doActionUserActivity } from './../libs/NetworkUtility';
import platform from '../../theme/variables/platform';
import { testID } from './../libs/Common';
import { NavigationActions } from 'react-navigation';

export default class App extends Component {

    videoHeight = platform.platform == 'ios' ? (platform.deviceHeight - platform.toolbarHeight) : (platform.deviceHeight - platform.toolbarHeight) - 10;

    constructor() {
        super();

        this.state = {
            title: 'Video',
            video: { width: undefined, height: undefined, duration: undefined },
            thumbnailUrl: 'https://static.skalanews.com/media/news/images/thumbs-635-425/ikan_sarden_memobee-com.jpg',
            videoUrl: 'https://vjs.zencdn.net/v/oceans.mp4',
        };
    }

    componentDidMount() {
        const nav = this.props.navigation;
        const { params } = nav.state;
        if (params != null) {
            this.setState({
                title: params.title,
                thumbnailUrl: params.cover,
                videoUrl: params.attachment,
            })

            this.doViewVideoLearning(params.id)
        }
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    onBackPressed = () => {
        this.props.navigation.goBack();
    };

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async doViewVideoLearning(videoId) {
        let viewVideoLearning = await doActionUserActivity(ParamAction.VIEW, ParamContent.VIDEO_LEARNING, videoId);
    }

    async doDownloadFile(id, category) {
        let actionDownload = await doActionUserActivity(ParamAction.DOWNLOAD, category, id);
        if (actionDownload.isSuccess) {
            if (category == ParamContent.VIDEO_MATERIS) {
                this.showAlertSuccess()
            } else {
                Toast.show({ text: 'Download succesfull', position: 'top', duration: 3000 })
            }
        }
    }

    showAlertSuccess = () => {
        Alert.alert(
            'Information',
            'File successfully downloaded. You can access this file in Profile Menu',
            [
                // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.gotoProfileDownload() },
            ],
            { cancelable: true }
        );
    }

    // gotoProfile = () => {
    //     this.props.navigation.dispatch(NavigationActions.navigate({
    //         routeName: 'Profile',
    //         key: `profile-1235`,
    //         params : {isGotoDownload : true},
    //     }));
    // }

    gotoProfileDownload = () => {
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: 'ProfileDownload',
                key: `videoProfileDownload`
            }));
    }

    render() {
        let isCanDownload = false;
        const { params } = this.props.navigation.state;

        if (params != null) {
            if (typeof params.isCanDownload != 'undefined') {
                isCanDownload = params.isCanDownload;
            }
        }

        return (
            <Container>
                {isCanDownload == true &&
                    <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                        <Header>
                            <Left style={{ flex: 0.5 }} />
                            <Body>
                                <Title>{this.state.title}</Title>
                            </Body>
                            <Right style={{ flex: 0.5 }}>
                                <Button
                                    // {...testID('buttonDownload')}
                                    accessibilityLabel="button_download"
                                    transparent onPress={() => this.doDownloadFile(params.id, params.category)}>
                                    <Icon name='md-download' nopadding style={styles.iconDownload} />
                                </Button>
                            </Right>
                        </Header>
                    </SafeAreaView>}
                {isCanDownload != true &&
                    <SafeAreaView style={{
                        // backgroundColor: platform.toolbarDefaultBg
                        backgroundColor: "#000000"
                    }}>

                    </SafeAreaView>}
                {/* <Content> */}
                {/* <VideoPlayer
                endWithThumbnail={true}
                thumbnail={{ uri: this.state.thumbnailUrl }}
                video={{ uri: this.state.videoUrl }}
                // video={require('./../../assets/oceans.mp4')}
                autoplay={false}
                videoHeight={this.videoHeight}
                videoWidth={platform.deviceWidth}
                ref={r => this.player = r}
                /> */}
                <VideoPlayer source={{ uri: this.state.videoUrl }} navigator={this.props.navigation} disableVolume />
                {/* </Content> */}
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    iconDownload: {
        flex: 0,
        color: '#FFFFFF',
        fontSize: 22
    },
});