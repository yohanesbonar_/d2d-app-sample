import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  View,
  BackHandler,
  SafeAreaView,
} from "react-native";
import Menu from "react-native-material-menu";
import {
  Container,
  Content,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Item,
  Toast,
} from "native-base";
import { Loader } from "./../components";
import { escapeHtml } from "./../libs/Common";
import platform from "../../theme/variables/d2dColor";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import MyWebView from "react-native-webview";
import { ParamSetting, getSetting } from "./../libs/NetworkUtility";
import { getData, KEY_ASYNC_STORAGE } from "../../src/utils/localStorage";
import { HeaderToolbar } from "../../src/components";

export default class GeneralWebview extends Component {
  state = {
    showLoader: false,
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    title: "TITLE",
    content: null,
    country_code: "",
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    const { params } = this.props.navigation.state;
    if (params != null) {
      this.getDetailWebview(params);
      this.getDataCountryCode(params);
    }
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };
  componentWillUnmount() {
    this.backHandler.remove();
  }
  getDataCountryCode = (params) => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      this.setState({
        country_code: profile.country_code,
      });
      if (profile.country_code == "ID") {
        let title =
          params == ParamSetting.TERMS_CONDITIONS
            ? "Syarat & Ketentuan"
            : params == ParamSetting.PRIVACY_POLICY
            ? "Kebijakan Privasi"
            : params == ParamSetting.CONTACT_US
            ? "Kontak Kami"
            : "TITLE";
        this.setState({ title: title });
      } else {
        let title =
          params == ParamSetting.TERMS_CONDITIONS
            ? "Terms & Conditions"
            : params == ParamSetting.PRIVACY_POLICY
            ? "Privacy Policy"
            : params == ParamSetting.CONTACT_US
            ? "Contact Us"
            : "TITLE";
        this.setState({ title: title });
      }
    });
  };

  async getDetailWebview(type) {
    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await getSetting(type);

      if (response.isSuccess === true) {
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          content: response.data[0].content,
        });
      } else {
        this.setState({ isFetching: false, isSuccess: false, isFailed: true });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({ text: "error", position: "top", duration: 3000 });
    }
  }

  _renderDescription = () => {
    if (this.state.content != null) {
      return (
        <View style={{ flex: 1 }}>
          <MyWebView
            startInLoadingState={true}
            source={{ html: escapeHtml(this.state.content, "") }}
          />
        </View>
      );
    }

    return null;
  };

  render() {
    const nav = this.props.navigation;

    return (
      <Container>
        <Loader visible={this.state.showLoader} />
        {this.state.country_code == "ID" && (
          <HeaderToolbar
            onPress={() => this.onBackPressed()}
            title={this.state.title}
          />
        )}
        {this.state.country_code != "ID" && (
          <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
            <Header noShadow>
              <Left style={{ flex: 0.5 }}>
                <Button transparent onPress={() => this.onBackPressed()}>
                  <Icon name="md-arrow-back" style={styles.toolbarIcon} />
                </Button>
              </Left>
              <Body
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Title>{this.state.title}</Title>
              </Body>
              <Right style={{ flex: 0.5 }} />
            </Header>
          </SafeAreaView>
        )}

        {this._renderDescription()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerBody: {
    justifyContent: "center",
    alignItems: "center",
  },
});
