import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Image, TouchableOpacity, FlatList, BackHandler } from 'react-native';
import Menu from 'react-native-material-menu';
import {
    Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right,
    Row, Col, Form, Item, Toast, Card
} from 'native-base';
import { Loader, ListItemLearningSpecialist, CardItemEvent } from './../../components';

import platform from '../../../theme/variables/platform';
import { EnumFeedsCategory, STORAGE_TABLE_NAME } from './../../libs/Common';

import { getApiFeeds } from './../../libs/NetworkUtility';
import { SafeAreaView } from 'react-native'
import { HeaderToolbar, SearchDataNotFound } from '../../../src/components';
import { getData, KEY_ASYNC_STORAGE } from '../../../src/utils';
export default class ProfileBookmark extends Component {

    page = 1;

    state = {
        showLoader: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        onRefresh: false,
        emptyDataBookmark: false,
        bookmarkList: [],
        subscription: null,
        country_code: ''
    }

    constructor(props) {
        super(props);
        this.getDataCountryCode()
    }

    getDataCountryCode = () => {

        getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
            this.setState({
                country_code: profile.country_code
            })
        });
    };
    componentDidMount() {
        StatusBar.setHidden(false);
        // this.getDataCompetence();
        this.getDataBookmark(this.page);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed()
            return true;
        });
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getDataCompetence() {
        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let dataProfile = JSON.parse(getJsonProfile);
        if (dataProfile != null) {
            let subscriptionId = [];
            // let subscriptionTitle = [];
            let dataSubscription = '';

            if (dataProfile.subscription != null && dataProfile.subscription.length > 0) {
                let self = this;
                dataProfile.subscription.map(function (d, i) {
                    subscriptionId.push(d.id);
                    // subscriptionTitle.push(d.title);
                });

                dataSubscription = subscriptionId.join(',');
            }

            this.setState({
                subscription: dataSubscription,
            })
        }

        this.getDataBookmark(this.page);
    }

    async getDataBookmark(page) {
        this.setState({ isFailed: false, isFetching: true, });

        try {
            let response = await getApiFeeds(page, null, true, true, true, true, true, false, false, null, false, false);

            if (response.isSuccess === true) {
                this.setState({
                    bookmarkList: [...this.state.bookmarkList, ...response.data],
                    isFetching: false, isSuccess: true, isFailed: false, onRefresh: false,
                    emptyDataBookmark: response.data != null && response.data.length > 0 ? false : true
                });
            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true, onRefresh: false });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }

        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true, onRefresh: false });
            Toast.show({ text: 'error', position: 'top', duration: 3000 })
        }
    }


    handleLoadMore = () => {
        if (this.state.emptyDataBookmark === true && this.state.isFetching) {
            return;
        }
        this.page = this.state.isFailed == true ? this.page : this.page + 1;
        this.getDataBookmark(this.page);
    }

    onRefreshBookmarks() {
        this.page = 1
        this.setState({ onRefresh: true, emptyDataBookmark: false, bookmarkList: [] })
        this.getDataBookmark(this.page)
    }

    _renderItem = ({ item }) => {
        if (item.category == EnumFeedsCategory.EVENT) {
            return (
                <CardItemEvent navigation={this.props.navigation} data={item} />
            )
        } else {
            return (
                <ListItemLearningSpecialist navigation={this.props.navigation} data={item} />
            )
        }
    }

    _renderItemFooter = () => (
        this.state.emptyDataBookmark?
            null
        :
            <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}>
                {this._renderItemFooterLoader()}
            </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true && (
            (this.page > 1))
        ) {
            return (
                <TouchableOpacity onPress={() => {
                    this.handleLoadMore()
                }}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItem = (type) => (
        <View style={styles.emptyItem}>
            {this._renderEmptyItemLoader()}
        </View>
    )

    _renderEmptyItemLoader() {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            if (this.state.bookmarkList.length == 0) {
                if (this.state.country_code == "ID") {
                    return (
                        <SearchDataNotFound
                            type="bookmark"
                            title="Belum Ada Bookmark"
                            subtitle="Tempat penyimpanan bookmark Anda akan ditampilkan disini."
                        />
                    )
                }
                else {
                    return (
                        <Text style={{ textAlign: 'center' }}>Data not found</Text>
                    )
                }
            }

            return null;
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }


    render() {
        const nav = this.props.navigation;
        const DownloadDetail = { type: "Navigate", routeName: "DownloadDetail" }

        return (
            <Container>
                {this.state.country_code == "ID" && (
                    <HeaderToolbar onPress={() => this.onBackPressed()} title="Bookmark" />
                )}

                {this.state.country_code != "ID" && (
                    <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>

                        <Header noShadow>
                            <Left style={{ flex: 0.5 }}>
                                <Button
                                    accessibilityLabel="button_back"
                                    transparent onPress={() => this.onBackPressed()}>
                                    <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                                </Button>
                            </Left>
                            <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Title>Bookmark</Title>
                            </Body>
                            <Right style={{ flex: 0.5 }} />
                        </Header>
                    </SafeAreaView>
                )}

                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <FlatList
                        contentContainerStyle={this.state.country_code == "ID" && this.state.isFetching == false && this.state.isSuccess == true &
                            this.state.bookmarkList.length == 0 ? styles.centerEmptyList : styles.flatlist}
                        data={this.state.bookmarkList}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        onRefresh={() => this.onRefreshBookmarks()}
                        refreshing={false}
                        renderItem={this._renderItem}
                        ListEmptyComponent={this._renderEmptyItem}
                        ListFooterComponent={this._renderItemFooter}
                        keyExtractor={(item, index) => index.toString()
                        } />
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContent: {
    },
    flatlist: {
        padding: 10,
    },
    centerEmptyList: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        width: "100%",
    }
});
