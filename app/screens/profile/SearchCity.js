import React, { Component } from 'react';
import { StatusBar, StyleSheet, TouchableOpacity, View, FlatList, BackHandler } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Right, Content, Toast, Card, Item, Input } from 'native-base';
import { Loader } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { getDataCity } from './../../libs/NetworkUtility';
import { testID } from './../../libs/Common';
import { SafeAreaView } from 'react-native';

export default class SearchCity extends Component {

    tempCityList = [];
    masterCityList = [];
    timer = null;
    state = {
        cityList: [],
        showLoader: true,
        searchValue: '',
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.getData();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        this.props.navigation.goBack();
    }

    async getData() {
        this.setState({ showLoader: true })

        try {
            let response = await getDataCity();

            if (response.isSuccess == true) {
                this.masterCityList = response.data;
                this.setState({ cityList: this.masterCityList, showLoader: false });
            } else {
                this.setState({ showLoader: false });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        } catch (error) {
            this.setState({ showLoader: false });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }

    _renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                // {...testID('ButtonSelectCity')}
                // accessibilityLabel="button_select_city"
                style={styles.container} activeOpacity={0.7} {...this.props}
                onPress={() => this.onSelectedCity(item.name)}>
                <Text style={styles.text}>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    _renderEmptyItem = () => (
        <View style={styles.emptyItem}>
            {this.renderNotFound()}
        </View>
    )

    renderNotFound() {
        if (this.state.showLoader == true) {
            return null;
        }

        let messageNotFound = "We are really sad we could not find anything";
        if (this.state.searchValue != '') {
            messageNotFound += ' for ' + this.state.searchValue;
        }
        return (
            <View style={styles.viewNotFound}>
                <Icon name="ios-search" style={styles.iconNotFound} />
                <Text style={styles.textNotFound}>{messageNotFound}</Text>
            </View>
        );
    }

    onSelectedCity(cityName) {
        this.props.navigation.goBack();
        this.props.navigation.state.params.onSelectedCity(cityName);
    }

    doSearch(value) {
        clearTimeout(this.timer);
        this.setState({ searchValue: value })
        this.tempCityList = [];

        if (this.masterCityList != null && this.masterCityList.length > 0) {
            this.timer = setTimeout(() => {
                for (let key in this.masterCityList) {
                    let obj = this.masterCityList[key];
                    let dataChecklist = null;
                    let dataCity = null;

                    if (obj.name.indexOf(value.toUpperCase()) !== -1) {
                        dataCity = { name: obj.name };

                        this.tempCityList.push(dataCity);
                    }
                }
                this.setState({ cityList: this.tempCityList });

            }, 700);
        }
    }

    render() {
        const nav = this.props.navigation;

        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow searchBar rounded>
                        <Item nopadding>
                            <Icon name="ios-search" />
                            <Input
                                // {...testID('inputSearchCity')}
                                accessibilityLabel="input_search_city"
                                placeholder="Search City"
                                onChangeText={(text) => this.doSearch(text)}
                                value={this.state.searchValue}
                                style={styles.searchField} />
                            <Icon
                                // {...testID('buttonDoSearch')}
                                // accessibilityLabel="button_do_search"
                                {...testID('button_clear')}
                                name="ios-close-circle" onPress={() => this.doSearch('')} />
                        </Item>
                        <TouchableOpacity
                            {...testID('button_close')}
                            accessibilityLabel="button_close"
                            transparent style={{ marginRight: 5, marginLeft: 10, alignSelf: 'center' }} onPress={() => this.onBackPressed()}>
                            <Icon name='md-close' type="Ionicons"
                                style={[styles.iconClose]}
                            />
                        </TouchableOpacity>
                    </Header>
                </SafeAreaView>
                <FlatList
                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                    data={this.state.cityList}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    renderItem={this._renderItem}
                    ListEmptyComponent={this._renderEmptyItem}
                    ListFooterComponent={this._renderItemFooter}
                    keyExtractor={(item, index) => index.toString()} />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    searchField: {
        fontSize: 14,
        lineHeight: 15,
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    },
    text: {
        fontFamily: 'Nunito-Bold',
        fontSize: 15,
        color: '#454F63',
        marginVertical: 10,
    },
    container: {
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 2,
        borderBottomColor: '#EEF0F5',
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingHorizontal: 10,
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    },
    iconClose: {
        fontSize: 28,
        color: '#fff',
    },
})