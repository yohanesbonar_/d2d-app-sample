import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Image, Animated, FlatList, TouchableOpacity, } from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import firebase from 'react-native-firebase';
import { NavigationActions, StackActions } from 'react-navigation';
import { convertMonth, validateEmail, validatePhoneNumber, STORAGE_TABLE_NAME, subscribeFcm, testID } from './../../libs/Common';
import { ParamSetting, ParamCompetence, doSendCompetence, doUploadProfileImage, getProfileUser, updateDataProfile } from './../../libs/NetworkUtility';
import DateTimePicker from 'react-native-modal-datetime-picker';
import guelogin from './../../libs/GueLogin';
import { deleteFIrebaseToken, getListNotification } from './../../libs/NetworkUtility';
const gueloginAuth = firebase.app('guelogin');
import moment from 'moment-timezone';

import {
    Container, Header, Title, Button, Icon, Text, Left, Body, Right, Card, CardItem,
    Item, Input, Label, Toast, Tabs, Tab, ScrollableTab, TabHeading
} from 'native-base';
import { Loader, } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import ImagePicker from 'react-native-image-picker';

export default class Register extends Component {

    page = 1;

    SPESIALIZATION = 'spesialization';
    SUBSCRIPTION = 'subscription';

    phoneNo = '';
    edu1 = '';
    edu2 = '';
    edu3 = '';
    clinicLoc1 = '';
    clinicLoc2 = '';
    clinicLoc3 = '';

    state = {
        animatedValue: new Animated.Value(0),
        showLoader: true,
        uid: null,
        provider: 'email',
        name: null,
        nameFailed: false,
        email: null,
        emailFailed: false,
        photoURL: null,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        notifications: [],
        avatarSource: null,
        fullName: null,
        email: null,
        npaIdi: null,
        spesialization: null,
        subscribtion: null,
        phoneNo: null,
        shownBornDate: null,
        bornDate: null,
        education1: null,
        education2: null,
        education3: null,
        homeLocation: null,
        clinicLocation: null,
        clinicLocation2: null,
        clinicLocation3: null,
        fullNameFailed: false,
        emailFailed: false,
        npaIdiFailed: false,
        spesializationFailed: false,
        subscribtionFailed: false,
        phoneNoFailed: false,
        bornDateFailed: false,
        education1Failed: false,
        education2Failed: false,
        education3Failed: false,
        homeLocationFailed: false,
        clinicLocationFailed: false,
        clinicLocation2Failed: false,
        clinicLocation3Failed: false,
        totalBookmark: false,
        totalDownload: false,
        needRefresh: true,
        needSendSpesialzation: false,
        needSendSubscribtion: false,
        isShowingDatePicker: false,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.getData();
        const didFocusSubscription = this.props.navigation.addListener(
            'didFocus',
            payload => {
                if (this.state.needRefresh === true) {
                    this.getListNotif();
                    this.getData();
                } else {
                    this.setState({ needRefresh: true })
                }
            }
        );


        let { params } = this.props.navigation.state;

        if (typeof params != 'undefined' && typeof params.isGotoDownload != 'undefined' &&
            params.isGotoDownload == true) {
            this.gotoProfileDownload();
        }
    }

    async getListNotif() {
        //getListNotification
        try {
            let response = await getListNotification();
            console.log(response)
            if (response.isSuccess == true) {
                this.setState({ notifications: response.data })
            }
        } catch (error) {
            console.log(error);
        }
    }

    getData() {
        let user = gueloginAuth.auth().currentUser;

        if (user) {
            this.setState({
                uid: user.uid,
                provider: user.providerId,
                name: user.displayName,
                email: user.email,
                photoURL: user.photoURL
            })

            this.getProfileDetail(user.uid);
            // this.getNotifications(this.page);
        }
    }

    setMenuRef = (ref) => {
        this.menu = ref;
    };

    async getProfileDetail(uid) {
        this.setState({ isFailed: false, isFetching: true, showLoader: true, spesialization: null, subscribtion: null });

        try {
            let response = await getProfileUser(uid);

            this.doSetProfile(response);
        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true, showLoader: false });
            Toast.show({ text: 'Something went wrong!', position: 'top', buttonText: 'Okay' })
        }
    }

    doSetProfile(response) {
        if (response.isSuccess == true) {
            if (response.data != null) {
                let profile = response.data;

                if (profile != null) {
                    let shownBornDate = null;
                    if (profile.born_date != null) {
                        shownBornDate = profile.born_date.substring(8, 10) + ' ' + convertMonth(profile.born_date.substring(5, 7)) + ' ' + profile.born_date.substring(0, 4)
                        profile.born_date = profile.born_date.substring(0, 10);
                    }

                    this.doSaveProfileUser(profile);


                    this.phoneNo = profile.phone == null ? '' : profile.phone;
                    this.edu1 = profile.education_1 == null ? '' : profile.education_1;
                    this.edu2 = profile.education_2 == null ? '' : profile.education_2;
                    this.edu3 = profile.education_3 == null ? '' : profile.education_3;
                    this.clinicLoc1 = profile.clinic_location_1 == null ? '' : profile.clinic_location_1;
                    this.clinicLoc2 = profile.clinic_location_2 == null ? '' : profile.clinic_location_2;
                    this.clinicLoc3 = profile.clinic_location_3 == null ? '' : profile.clinic_location_3;

                    this.setState({
                        fullName: profile.name,
                        email: profile.email,
                        npaIdi: profile.npa_idi,
                        spesialization: profile.spesialization,
                        subscribtion: profile.subscription,
                        phoneNo: profile.phone,
                        shownBornDate: shownBornDate,
                        bornDate: profile.born_date,
                        education1: profile.education_1,
                        education2: profile.education_2,
                        education3: profile.education_3,
                        homeLocation: profile.home_location,
                        clinicLocation: profile.clinic_location_1,
                        clinicLocation2: profile.clinic_location_2,
                        clinicLocation3: profile.clinic_location_3,
                        avatarSource: profile.photo != null ? 'https://d2doss.oss-ap-southeast-5.aliyuncs.com/image/profile/' + profile.photo : null,
                        totalBookmark: profile.counting.bookmark == null ? 0 : profile.counting.bookmark,
                        totalDownload: profile.counting.download == null ? 0 : profile.counting.download,
                        isFetching: false, isSuccess: true, isFailed: false, showLoader: false
                    });

                }
            }
        } else {
            this.setState({ isFetching: false, isSuccess: true, isFailed: false });
        }

    }


    async doSaveProfileUser(profile) {
        try {
            let jsonProfile = JSON.stringify(profile);
            let saveProfile = await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, jsonProfile);
        } catch (error) {
            console.log('failed update db local profile')
        }
    }


    //---- method  get notification phase 2 ----------//
    // async getNotifications(page) {
    //     this.setState({ isFailed: false, isFetching: true, });

    //     try {
    //         let response = await Api.get(`event?page=${page}`);

    //         this.setState({ notifications: [...this.state.notifications, ...response.result] });
    //         this.setState({ isFetching: false, isSuccess: true, isFailed: false });
    //     } catch(error) {
    //         this.setState({ isFetching: false, isSuccess: false, isFailed: true });
    //         Toast.show({ text: 'Terjadi kesalahan!', position: 'bottom', buttonText: 'Okay'})
    //     }
    // }

    handleLoadMore = () => {
        this.page = this.state.isFailed == true ? this.page : this.page + 1;

        // this.getNotifications(this.page);
    }


    _renderItem = ({ item }) => {
        isViewed = item.status == "unread" ? true : false;

        centerDotColor = isViewed ? '#DD2C2C' : '#78849E';
        backgroundColorItem = isViewed ? '#BBDEFB' : '#FFFFFF';

        let data = {
            title: item.title,
            certificate: item.slug,
            isRead: item.status == "unread" ? true : false,
        }

        return (
            <TouchableOpacity
                // {...testID('buttonCmeCertificate')}
                accessibilityLabel="button_cme_certification"
                activeOpacity={0.7} onPress={() => this.gotoCmeCertificate(data)}>
                <View style={[styles.itemViewNotif, { backgroundColor: backgroundColorItem }]}>
                    <View style={styles.itemViewDots}>
                        <Icon name={'dot-single'} type='Entypo' style={[styles.iconDot, { color: centerDotColor }]} />
                    </View>
                    <View style={styles.itemViewText}>
                        {/* <Text style={styles.textItemTime}>{item.title}</Text> */}
                        <Text style={styles.textItemDesc}>{item.title}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetching == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true && (
            (this.page > 1))
        ) {
            return (
                <TouchableOpacity onPress={() => {
                    this.handleLoadMore()
                }}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItemNotification() {
        // if (this.state.isFetching == false && this.state.isFailed == true) {
        //     return (
        //         <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={() => this.handleLoadMore()}>
        //             <Image source={require('./../../assets/images/noinet.png')} style={{width: 72, height: 72}} />
        //             <Text style={{textAlign: 'center'}}>Something went wrong, <Text style={{color: platform.brandInfo}}>tap to reload</Text></Text>
        //         </TouchableOpacity>
        //     )
        // } else if (this.state.isFetching == false && this.state.isSuccess == true) {
        //     return (
        //         <Text style={{textAlign: 'center'}}>Notification not found</Text>
        //     )
        // }

        return (<Text style={{ textAlign: 'center' }}>Notification not found</Text>);
    }

    _renderEmptyItemNotif() {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            return (
                <Text style={{ textAlign: 'center' }}>Notification not found</Text>
            )
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }
    //---- end of method  get notification phase 2 ----------//


    doValidateDataProfile() {
        let errorMessage = this.doValidateField();

        if (errorMessage == '') {
            this.setState({ showLoader: true })

            if (this.state.needSendSpesialzation === true) {
                this.doSendSpesialization();
            } else if (this.state.needSendSubscribtion === true) {
                this.doSendSubscribtion();
            } else {
                this.doUpdateProfile();
            }

        } else {
            Toast.show({ text: errorMessage, position: 'top', duration: 3000 })
        }
    }

    doValidateField() {
        let errorMessage = '';
        let messages = [];
        let msgReq = [];

        if (!this.state.fullName) {
            this.setState({ fullNameFailed: true });
            msgReq.push('Fullname');
        } else {
            this.setState({ fullNameFailed: false })
        }

        if (!this.state.email) {
            this.setState({ emailFailed: true });
            msgReq.push('Email address');
        } else {
            if (validateEmail(this.state.email) === false) {
                this.setState({ emailFailed: true })
                messages.push('Email address is invalid');
            } else {
                this.setState({ emailFailed: false })
            }
        }

        if (!this.state.npaIdi) {
            this.setState({ npaIdiFailed: true });
            msgReq.push('NPA IDI');
        } else {
            this.setState({ npaIdiFailed: false });
        }

        if (this.state.spesialization == null || this.state.spesialization.length <= 0) {
            this.setState({ spesializationFailed: true });
            messages.push('Select at least one spesialization');
        } else {
            this.setState({ spesializationFailed: false });
        }

        if (this.state.subscribtion == null || this.state.subscribtion.length <= 0) {
            this.setState({ subscribtionFailed: true });
            messages.push('Select at least one subscription');
        } else {
            this.setState({ subscribtionFailed: false });
        }

        if (this.phoneNo == '') {
            this.setState({ phoneNoFailed: true });
            msgReq.push('Phone number');
        } else {
            if (validatePhoneNumber(this.phoneNo) === false) {
                this.setState({ phoneNoFailed: true })
                messages.push('Phone number is invalid');
            } else {
                this.setState({ phoneNoFailed: false })
            }
        }

        if (!this.state.bornDate) {
            this.setState({ bornDateFailed: true });
            msgReq.push('Date of birth');
        } else {
            this.setState({ bornDateFailed: false })
        }

        if (this.edu1 == '') {
            this.setState({ education1Failed: true });
            messages.push('Please input at least one education');
        } else {
            this.setState({ education1Failed: false })
        }

        // if (!this.state.homeLocation) {
        //     this.setState({ homeLocationFailed: true });
        //     msgReq.push('Home location');
        // } else {
        //     this.setState({ homeLocationFailed: false})
        // }

        if (this.clinicLoc1 == '') {
            this.setState({ clinicLocationFailed: true });
            msgReq.push('Doctor practice');
        } else {
            this.setState({ clinicLocationFailed: false })
        }

        if (msgReq != null && msgReq.length > 0) {
            errorMessage = msgReq.join(', ') + ' is required';
        }

        if (messages != null && messages.length > 0) {
            if (msgReq != null && msgReq.length > 0) {
                errorMessage += ' and ';
            }
            errorMessage += messages.join(', ');
        }


        return errorMessage;
    }

    //--- update firebase ----//
    updateProfile() {
        const nav = this.props.navigation;
        const user = guelogin.auth().currentUser;

        if (user) {
            this.setState({ showLoader: true });
            user.updateProfile({ displayName: this.state.name })
                .then(() => {
                    this.setState({ showLoader: false });
                    Toast.show({ text: 'Profile updated!', position: 'top', duration: 3000 });
                }).catch((error) => {
                    console.log(error)
                    this.setState({ showLoader: false });
                    Toast.show({ text: error, position: 'top', duration: 3000 });
                });
        }
    }
    //--- end of update firebase ---//

    async doSendSpesialization() {
        let response = await doSendCompetence(this.state.spesialization, ParamCompetence.SPESIALIZATION);
        if (response.isSuccess == true) {
            this.setState({ needSendSpesialzation: false })
            if (this.state.needSendSubscribtion) {
                this.doSendSubscribtion();
            } else {
                this.doUpdateProfile();
            }
        } else {
            this.setState({ showLoader: false })
            alert(response.message)
        }
    }

    async doSendSubscribtion() {
        let response = await doSendCompetence(this.state.subscribtion, ParamCompetence.SUBSCRIPTION);
        if (response.isSuccess == true) {
            this.setState({ needSendSubscribtion: false })
            this.doUpdateProfile();
        } else {
            this.setState({ showLoader: false })
            alert(response.message)
        }
    }

    async doUpdateProfile() {
        let uid = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);

        this.setState({
            phoneNo: this.phoneNo,
            education1: this.edu1,
            education2: this.edu2,
            education3: this.edu3,
            clinic_location_1: this.clinicLoc1,
            clinic_location_2: this.clinicLoc2,
            clinic_location_3: this.clinicLoc3,
        })

        let params = {
            uid: uid,
            name: this.state.fullName,
            email: this.state.email,
            phone: this.phoneNo,
            born_date: this.state.bornDate,
            education_1: this.edu1,
            education_2: this.edu2 == null ? '' : this.edu2,
            education_3: this.edu2 == null ? '' : this.edu3,
            home_location: this.state.homeLocation == null ? '' : this.state.homeLocation,
            clinic_location_1: this.clinicLoc1,
            clinic_location_2: this.clinicLoc2 == null ? '' : this.clinicLoc2,
            clinic_location_3: this.clinicLoc3 == null ? '' : this.clinicLoc3,
        }

        let response = await updateDataProfile(params);

        this.setState({ showLoader: false })
        if (response.isSuccess == true) {
            Toast.show({ text: 'Profile updated!', position: 'top', duration: 3000 });
            this.getData();
        } else {
            Toast.show({ text: response.message, position: 'top', duration: 3000 });
        }

    }

    isFetchLogout = false;

    async logout() {

        if (this.isFetchLogout == true) {
            return;
        }

        this.isFetchLogout = true;
        const user = gueloginAuth.auth().currentUser;
        let topicFCM = await subscribeFcm();
        if (user) {
            // this.removeToken();
            this.setState({ showLoader: true });

            try {
                let response = await deleteFIrebaseToken();
                if (response.isSuccess == true || response.message == 'No available device token to be removed') {
                    let delFcmToken = await AsyncStorage.setItem(STORAGE_TABLE_NAME.FCM_TOKEN, "");

                    const resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login' })] });
                    // const resetAction = NavigationActions.reset({ index: 0, actions: [NavigationActions.navigate({routeName: 'Splash'})] });

                    gueloginAuth.auth().signOut()
                        .then(() => {
                            let setLogin = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.IS_LOGIN, 'N');
                            let uid = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.UID, null);
                            let profile = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, null);
                            let auth = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.AUTHORIZATION, null);
                            let fcm = async () => await firebase.messaging().unsubscribeFromTopic(topicFCM);

                            setTimeout(() => {
                                this.setState({ showLoader: false });
                                this.props.navigation.dispatch(resetAction);
                            }, 100);
                        })
                        .catch((error) => {
                            console.log(error)
                            this.setState({ showLoader: false });
                            Toast.show({ text: error, position: 'top', duration: 3000 });
                        })
                } else {
                    Toast.show({ text: response.message, position: 'top', duration: 3000 });
                }
            } catch (error) {
                console.log(error);
            }

            this.isFetchLogout = false
        }
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };


        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                // let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.doUploadImage(response.uri, this.state.fullName)
            }
        });
    }

    async doUploadImage(uri, namefile) {
        this.setState({ showLoader: true });
        let response = await doUploadProfileImage(uri, namefile);
        if (response.isSuccess === true) {
            this.setState({ avatarSource: response.data, showLoader: false })
        } else {
            this.setState({ showLoader: false })
            alert(response.message);
        }
    }

    _renderSpesialization = () => {
        let keyAdd = this.state.spesialization != null ? this.state.spesialization.length : 1;
        return (
            <View style={styles.textSpecialist}>
                {this._renderTagSpecialist()}

                <TouchableOpacity
                    // {...testID('buttonAdd')}
                    accessibilityLabel="button_add"
                    key={keyAdd} onPress={() => this.doSelectSpesialization(this.SPESIALIZATION)} activeOpacity={0.7}>
                    <View style={styles.backgroundBtnAdd}>
                        <Text style={styles.textGrey}>+ Add</Text>
                    </View>
                </TouchableOpacity>
            </View>);
    }

    _renderTagSpecialist = () => {
        let items = [];
        if (this.state.spesialization != null && this.state.spesialization.length > 0) {
            let self = this;
            this.state.spesialization.map(function (d, i) {
                items.push(
                    <TouchableOpacity
                        // {...testID('buttonDeletedSpesialization')}
                        accessibilityLabel="button_delete"
                        key={i} onPress={() => self.doDeletedSpesialization(self, i)} activeOpacity={0.7}>
                        <View style={styles.backgroundCompetence}>
                            <Text style={styles.textGrey}>{d.title}</Text>
                            <Icon name='ios-close' style={styles.tagIconX} />
                        </View>
                    </TouchableOpacity>
                )
            });
        }

        return items
    }

    doDeletedSpesialization = (self, index) => {
        self.state.spesialization.splice(index, 1);
        self.setState({ spesialization: self.state.spesialization, needSendSpesialzation: true })
    }

    _renderSubscribtions = () => {
        let keyAdd = this.state.subscribtion != null ? this.state.subscribtion.length : 1;
        return (
            <View style={styles.textSpecialist}>
                {this._renderTagSubscribtions()}

                <TouchableOpacity
                    // {...testID('buttonSelectSpesialization')}
                    accessibilityLabel="button_select"
                    key={keyAdd} onPress={() => this.doSelectSpesialization(this.SUBSCRIPTION)} activeOpacity={0.7}>
                    <View style={styles.backgroundBtnAdd}>
                        <Text style={styles.textGrey}>+ Add</Text>
                    </View>
                </TouchableOpacity>
            </View>);

    }

    _renderTagSubscribtions = () => {
        let items = [];

        if (this.state.subscribtion != null && this.state.subscribtion.length > 0) {
            let self = this;
            this.state.subscribtion.map(function (d, i) {
                items.push(
                    <TouchableOpacity
                        // {...testID('buttonDeletedSubscribtion')}
                        accessibilityLabel="button_delete"
                        key={i} onPress={() => self.doDeletedSubscribtion(self, i)} activeOpacity={0.7}>
                        <View style={styles.backgroundCompetence}>
                            <Text style={styles.textGrey}>{d.title}</Text>
                            <Icon name='ios-close' style={styles.tagIconX} />
                        </View>
                    </TouchableOpacity>
                )
            });

        }

        return items
    }

    doDeletedSubscribtion = (self, index) => {
        self.state.subscribtion.splice(index, 1);
        self.setState({ subscribtion: self.state.subscribtion, needSendSubscribtion: true })
    }

    doSelectSpesialization(type) {
        this.setState({ needRefresh: false })
        this.props.navigation.navigate('Subscribtion', {
            from: 'PROFILE',
            type: type,
            // updateData: this.doRefreshData, 
            selectedSpecialist: type == this.SPESIALIZATION ? this.state.spesialization : this.state.subscribtion,
            updateData: type == this.SPESIALIZATION ? this.doUpdateDataSpesialization : this.doUpdateDataSubscribtion,
        })
    }

    doUpdateDataSpesialization = (spesializationList) => {
        this.setState({ spesialization: spesializationList, needSendSpesialzation: true });
    };

    doUpdateDataSubscribtion = (subscribtionList) => {
        this.setState({ subscribtion: subscribtionList, needSendSubscribtion: true });
    };


    _showDatePicker = () => this.setState({ isShowingDatePicker: true });

    _hideDatePicker = () => this.setState({ isShowingDatePicker: false });

    _handleDatePicked = (date) => {
        let shownDatestring = date.getDate() + " " + convertMonth((date.getMonth() + 1)) + " " + date.getFullYear();
        let bornDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

        this.setState({ shownBornDate: shownDatestring, bornDate: bornDate })
        this._hideDatePicker();
    };

    doChooseCity = () => {
        this.setState({ needRefresh: false })
        this.props.navigation.navigate('SearchCity', {
            onSelectedCity: this.onSelectedCity,
        })
    }

    onSelectedCity = (city) => {
        this.setState({ homeLocation: city, homeLocationFailed: false })
    }

    gotoWebView(type) {
        this.menu.hide();
        this.setState({ needRefresh: false })
        let webView = { type: "Navigate", routeName: "GeneralWebview", params: type }
        this.props.navigation.navigate(webView);
    }

    gotoCmeCertificate(data) {
        let cmeCertificate = { type: "Navigate", routeName: 'CmeCertificate', params: data };
        this.props.navigation.navigate(cmeCertificate)
    }

    onChangeTextPhoneNo(value) {
        this.phoneNo = value
        if (platform.platform != 'ios') {
            this.setState({ phoneNo: value })
        }
    }

    onChangeTextEdu1(value) {
        this.edu1 = value
        if (platform.platform != 'ios') {
            this.setState({ education1: value })
        }
    }

    onChangeTextEdu2(value) {
        this.edu2 = value
        if (platform.platform != 'ios') {
            this.setState({ education2: value })
        }
    }

    onChangeTextEdu3(value) {
        this.edu3 = value
        if (platform.platform != 'ios') {
            this.setState({ education3: value })
        }
    }
    onChangeTextClinic1(value) {
        this.clinicLoc1 = value
        if (platform.platform != 'ios') {
            this.setState({ clinicLocation: value })
        }
    }

    onChangeTextClinic2(value) {
        this.clinicLoc2 = value
        if (platform.platform != 'ios') {
            this.setState({ clinicLocation2: value })
        }
    }

    onChangeTextClinic3(value) {
        this.clinicLoc3 = value
        if (platform.platform != 'ios') {
            this.setState({ clinicLocation3: value })
        }
    }

    gotoProfileDownload = () => {
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: 'ProfileDownload',
                key: `profileDownload`
            }));
    }

    gotoProfileBookmark = () => {
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: 'ProfileBookmark',
                key: `profileBookmark`
            }));
    }

    render() {
        const nav = this.props.navigation;

        let translateY = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight)],
            outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
            extrapolate: 'clamp',
        })
        let opacityContent = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 3],
            outputRange: [1, 0],
        });
        let opacityTitle = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 3],
            outputRange: [0, 1],
        });
        const imageOpacity = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight)],
            outputRange: [1, 0],
            extrapolate: 'clamp',
        });

        let imageAvatar = this.state.avatarSource === null ?
            { uri: 'https://s3-ap-southeast-1.amazonaws.com/static.guesehat.com/article/mums_ayo_belajar_memahami_si_kecil_1520934591.jpg' } :
            // null :
            { uri: this.state.avatarSource };

        let heightImage = this.state.avatarSource === null ? 0 : heightHeaderSpan - (heightHeaderSpan / 6);
        let heightEmptyImage = this.state.avatarSource === null ? heightHeaderSpan - (heightHeaderSpan / 6) : 0;

        let topValue = this.state.avatarSource === null ? heightHeaderSpan - (heightHeaderSpan / 2) - 100 :
            heightHeaderSpan - (heightHeaderSpan / 2) - 60;
        let iconName = this.state.avatarSource === null ? 'account-circle' : 'md-camera';
        let iconType = this.state.avatarSource === null ? 'MaterialIcons' : 'Ionicons';
        let sizeIcon = this.state.avatarSource === null ? 70 : 60;
        let styleBtnUploadImage = this.state.avatarSource === null ? styles.btnUploadImage : styles.invisibleBtn;
        let mrgnBottomIcon = this.state.avatarSource === null ? 0 : 20;
        let valueOpacity = this.state.avatarSource === null ? 1 : 0.8;

        console.log(imageAvatar);

        const name = this.state.mProfile != null && this.state.mProfile.doctor.name ? this.state.mProfile.doctor.name : 'noname';

        let tabPosition = 0;
        let { params } = this.props.navigation.state;
        if (params != null && params.type != null) {
            tabPosition = 1;
        }
        return (
            <Container>
                <DateTimePicker
                    isVisible={this.state.isShowingDatePicker}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDatePicker}
                    maximumDate={moment().toDate()}
                />
                <Loader visible={this.state.showLoader} />
                <Animated.View style={styles.animHeader}>
                    <Header noShadow hasTabs style={{ backgroundColor: 'transparent' }}>
                        <Left style={{ flex: 0.5 }}>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <AnimatedTitle style={{ opacity: opacityTitle, textAlign: 'center' }}>{this.state.fullName}</AnimatedTitle>
                        </Body>
                        <Right style={{ flex: 0.5 }}>
                            <Menu style={{ padding: 0 }}
                                ref={this.setMenuRef}
                                button={<Button
                                    // {...testID('buttonMore')}
                                    accessibilityLabel="button_more"
                                    transparent onPress={() => this.menu.show()}>
                                    <Icon name='md-more' nopadding style={styles.iconMore} />
                                </Button>}>
                                <MenuItem
                                    // {...testID('buttonTnC')}
                                    accessibilityLabel="button_tnc"
                                    textStyle={styles.textLogout} onPress={() => this.gotoWebView(ParamSetting.TERMS_CONDITIONS)}>Terms & Conditions</MenuItem>
                                <MenuItem
                                    // {...testID('buttonPrivacy')}
                                    accessibilityLabel="button_privacy"
                                    textStyle={styles.textLogout} onPress={() => this.gotoWebView(ParamSetting.PRIVACY_POLICY)}>Privacy Policy</MenuItem>
                                <MenuItem
                                    // {...testID('buttonContactUs')}
                                    accessibilityLabel="button_contactus"
                                    textStyle={styles.textLogout} onPress={() => this.gotoWebView(ParamSetting.CONTACT_US)}>Contact Us</MenuItem>
                                <MenuItem
                                    // {...testID('buttonLogOut')}
                                    accessibilityLabel="button_logout"
                                    textStyle={styles.textLogout} onPress={() => this.logout()}>Logout</MenuItem>
                            </Menu>
                        </Right>
                    </Header>
                </Animated.View>

                <Animated.View style={[styles.animHeaderSpan, { transform: [{ translateY }] }]}>
                    <Animated.View style={[styles.mainViewCollapsing, { opacity: imageOpacity }]}>
                        <TouchableOpacity
                            // {...testID('buttonAvatar')}
                            accessibilityLabel="button_avatar"
                            onPress={this.selectPhotoTapped.bind(this)}>
                            <Animated.Image style={{ height: heightImage, opacity: imageOpacity, backgroundColor: '#CB1D50' }}
                                source={imageAvatar}
                            // defaultSource={require('../assets/images/new_icon.png')}
                            />
                            <Animated.View style={{ height: heightEmptyImage, backgroundColor: '#CB1D50' }}
                            />
                        </TouchableOpacity>
                        <View style={[styles.viewInCollapsing, { top: topValue }]}>
                            <TouchableOpacity style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginBottom: mrgnBottomIcon }}
                                onPress={this.selectPhotoTapped.bind(this)}>
                                <Icon name={iconName} type={iconType} style={[styles.iconImage, { fontSize: sizeIcon, opacity: valueOpacity }]} />
                                <TouchableOpacity
                                    // {...testID('buttonUploadImage')}
                                    accessibilityLabel="button_upload"
                                    style={styleBtnUploadImage} onPress={this.selectPhotoTapped.bind(this)}>
                                    <Text style={styles.textWhite}>Upload Image</Text>
                                </TouchableOpacity>
                            </TouchableOpacity>
                            <Animated.Text bordered style={[styles.heading, { opacity: opacityContent }]}
                                numberOfLines={1}>{this.state.fullName}</Animated.Text>
                            <Card style={styles.mainCardCollapsing}>
                                <CardItem style={[styles.smallCardCollapsing, { borderRightWidth: 1, borderRightColor: '#F4F4F6', }]}>
                                    <TouchableOpacity
                                        // {...testID('buttonBookmark')}
                                        accessibilityLabel="button_bookmark"
                                        onPress={() => this.gotoProfileBookmark()}
                                        style={styles.touchSmallCardCollapsing}>
                                        <Text style={styles.textNumber}>{this.state.totalBookmark}</Text>
                                        <Text style={styles.textCollapsingDesc}>BOOKMARK</Text>
                                    </TouchableOpacity>
                                </CardItem>
                                <CardItem style={[styles.smallCardCollapsing, { borderLeftWidth: 1, borderLeftColor: '#F4F4F6' }]}>
                                    <TouchableOpacity
                                        // {...testID('buttonDownload')}
                                        accessibilityLabel="button_download"
                                        onPress={() => this.gotoProfileDownload()}
                                        style={styles.touchSmallCardCollapsing}>
                                        <Text style={styles.textNumber}>{this.state.totalDownload}</Text>
                                        <Text style={styles.textCollapsingDesc}>DOWNLOAD</Text>
                                    </TouchableOpacity>
                                </CardItem>
                            </Card>
                        </View>
                    </Animated.View>
                </Animated.View>

                <Tabs
                    initialPage={tabPosition}
                    renderTabBar={(props) =>
                        <Animated.View
                            style={{ transform: [{ translateY }], top: heightHeaderSpan, zIndex: 1, width: "100%" }}>
                            <ScrollableTab {...props}
                                renderTab={(name, page, active, onPress, onLayout) => (
                                    <TouchableOpacity key={page}
                                        onPress={() => { onPress(page) }}
                                        onLayout={onLayout}
                                        activeOpacity={0.99}>
                                        <Animated.View style={styles.tabHeading}>
                                            <TabHeading
                                                // {...testID('tabHeading')}
                                                accessibilityLabel="tab_heading"
                                                style={{ width: (platform.deviceWidth / 2), height: platform.toolbarHeight }}
                                                active={active}>
                                                <Text style={{
                                                    fontFamily: active ? 'Nunito-Bold' : 'Nunito-Regular',
                                                    color: active ? platform.topTabBarActiveTextColor : platform.topTabBarTextColor,
                                                    fontSize: platform.tabFontSize
                                                }}>
                                                    {name}
                                                </Text>
                                            </TabHeading>
                                        </Animated.View>
                                    </TouchableOpacity>
                                )}
                                underlineStyle={{ backgroundColor: platform.topTabBarActiveBorderColor }} />
                        </Animated.View>
                    }>
                    <Tab
                        // {...testID('tabProfile')}
                        accessibilityLabel="tab_profile"
                        heading="PROFILE">
                        <Animated.ScrollView
                            // automaticallyAdjustContentInsets={false}
                            // keyboardShouldPersistTaps = 'always'
                            contentContainerStyle={{ paddingTop: heightHeaderSpan + 5, paddingHorizontal: 10 }}
                            scrollEventThrottle={1} // <-- Use 1 here to make sure no cmespessialist are ever missed
                            onScroll={Animated.event(
                                [{ nativeEvent: { contentOffset: { y: this.state.animatedValue } } }],
                                { useNativeDriver: true } // <-- Add this
                            )}
                        >

                            <Card style={styles.card}>
                                <Label style={styles.textLabel}>Fullname</Label>
                                <Item error={this.state.nameFailed}>
                                    <Input autoCapitalize="none"
                                        placeholder={'Name'}
                                        editable={false}
                                        value={this.state.fullName}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ fullName: txt })} />
                                </Item>
                                <Label style={styles.textLabel}>Email</Label>
                                <Item error={this.state.emailFailed}>
                                    <Input autoCapitalize="none"
                                        placeholder={'Email address'}
                                        editable={false}
                                        value={this.state.email}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ email: txt })} />
                                </Item>
                                <Label style={styles.textLabel}>NPA IDI</Label>
                                <Item error={this.state.npaIdiFailed}>
                                    <Input
                                        placeholder={'ex: 0123XX'}
                                        value={this.state.npaIdi}
                                        editable={false}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ npaIdi: txt })}
                                    />
                                </Item>
                                <Label style={styles.textLabel}>Specialization</Label>
                                {this._renderSpesialization()}
                                <Label style={styles.textLabel}>Subscription</Label>
                                {this._renderSubscribtions()}
                                <Label style={styles.textLabel}>Phone Number</Label>
                                <Item error={this.state.phoneNoFailed}>
                                    <Input
                                        // {...testID('inputNumberic')}
                                        accessibilityLabel="input_numeric"
                                        autoCapitalize="none"
                                        keyboardType="numeric"
                                        placeholder={'ex: 0896xxxxxxxx'}
                                        value={this.state.phoneNo}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextPhoneNo(txt)}
                                    />
                                </Item>
                                <Label style={styles.textLabel}>Date of birth</Label>
                                <Item error={this.state.bornDateFailed} onPress={this._showDatePicker}>
                                    <Input
                                        // {...testID('inputDateOfBirth')}
                                        accessibilityLabel="input_date_birth"
                                        autoCapitalize="none"
                                        placeholder={'ex: dd mmm yyyy'}
                                        // onTouchStart={this._showDatePicker}
                                        editable={false}
                                        pointerEvents="none"
                                        value={this.state.shownBornDate}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ bornDate: txt })} />

                                    <Icon name='date-range' type='MaterialIcons' style={{ color: '#C3D1E8' }} />
                                </Item>
                                <Label style={styles.textLabel}>Educational Degree</Label>
                                <Item error={this.state.education1Failed}>
                                    <Input
                                        // {...testID('inputEducationalDegree1')}
                                        accessibilityLabel="input_educational_degree1"
                                        autoCapitalize="none"
                                        placeholder={'Sp.A from Brawijaya University'}
                                        value={this.state.education1}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextEdu1(txt)} />
                                </Item>
                                <Item style={{ marginTop: 15 }} error={this.state.education2Failed}>
                                    <Input
                                        // {...testID('inputEducationalDegree2')}
                                        accessibilityLabel="input_educational_degree2"
                                        autoCapitalize="none"
                                        placeholder={'Sp.A from Brawijaya University'}
                                        value={this.state.education2}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextEdu2(txt)} />
                                </Item>
                                <Item style={{ marginTop: 15 }} error={this.state.education3Failed}>
                                    <Input
                                        // {...testID('inputEducationalDegree3')}
                                        accessibilityLabel="input_educational_degree3"
                                        autoCapitalize="none"
                                        placeholder={'Sp.A from Brawijaya University'}
                                        value={this.state.education3}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextEdu3(txt)} />
                                </Item>
                                <Label style={styles.textLabel}>Home Location</Label>
                                <Item error={this.state.homeLocationFailed} onPress={() => this.doChooseCity()}>
                                    <Input
                                        // {...testID('inputHomeLocation')}
                                        accessibilityLabel="input_home"
                                        autoCapitalize="none"
                                        placeholder={'Home Location'}
                                        // onTouchStart={this.doChooseCity}
                                        editable={false}
                                        pointerEvents="none"
                                        value={this.state.homeLocation}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ homeLocation: txt })} />
                                </Item>
                                <Label style={styles.textLabel}>Doctor Practice</Label>
                                <Item error={this.state.clinicLocationFailed}>
                                    <Input
                                        // {...testID('inputClinic1')}
                                        accessibilityLabel="input_clinic1"
                                        autoCapitalize="none"
                                        placeholder={'ex : RS. Dr. Karyadi'}
                                        value={this.state.clinicLocation}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextClinic1(txt)} />
                                </Item>
                                <Item style={{ marginTop: 15 }} error={this.state.clinicLocation2Failed}>
                                    <Input
                                        // {...testID('inputClinic2')}
                                        accessibilityLabel="input_clinic2"
                                        autoCapitalize="none"
                                        placeholder={'ex : RS. Dr. Karyadi'}
                                        value={this.state.clinicLocation2}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextClinic2(txt)} />
                                </Item>
                                <Item style={{ marginTop: 15 }} error={this.state.clinicLocation3Failed}>
                                    <Input
                                        // {...testID('inputClinic3')}
                                        accessibilityLabel="input_clinic3"
                                        autoCapitalize="none"
                                        placeholder={'ex : RS. Dr. Karyadi'}
                                        value={this.state.clinicLocation3}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.onChangeTextClinic3(txt)} />
                                </Item>
                                <Button
                                    // {...testID('buttonSave')}
                                    accessibilityLabel="button_save"
                                    style={styles.btnSave} onPress={() => this.doValidateDataProfile()} success block>
                                    <Text style={{ fontFamily: 'Nunito-Bold' }}>SAVE</Text>
                                </Button>
                            </Card>
                        </Animated.ScrollView>
                    </Tab>
                    <Tab
                        // {...testID('inputNotification')} 
                        accessibilityLabel="input_notif"
                        heading="NOTIFICATION">
                        <AnimatedFlatList
                            contentContainerStyle={{ paddingTop: heightHeaderSpan + 5, paddingHorizontal: 10, }}
                            scrollEventThrottle={1} // <-- Use 1 here to make sure no cmespessialist are ever missed
                            onScroll={Animated.event(
                                [{ nativeEvent: { contentOffset: { y: this.state.animatedValue } } }],
                                { useNativeDriver: true } // <-- Add this
                            )}
                            data={this.state.notifications}
                            onEndReached={this.handleLoadMore}
                            onEndReachedThreshold={0.5}
                            renderItem={this._renderItem}
                            ListEmptyComponent={this._renderEmptyItemNotification}
                            ListFooterComponent={this._renderItemFooter()}
                            keyExtractor={(item, index) => index.toString()} />
                    </Tab>
                </Tabs>
            </Container>
        )
    }
}


const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const AnimatedTitle = Animated.createAnimatedComponent(Title);
const heightHeaderSpan = 320;//250
const styles = StyleSheet.create({
    headerBody: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: platform.titleFontColor,
        fontFamily: platform.titleFontfamily,
        fontSize: platform.titleFontSize,
    },
    animHeader: {
        position: "absolute",
        width: "100%",
        zIndex: 2,
    },
    animHeaderSpan: {
        position: 'absolute',
        backgroundColor: platform.toolbarDefaultBg,
        height: heightHeaderSpan,
        left: 0,
        right: 0,
        zIndex: 1,
    },
    animHeaderImageSpan: {
        height: heightHeaderSpan,
        left: 0,
        right: 0,
        zIndex: 1,
        paddingHorizontal: 20,
        paddingVertical: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },
    emptyItem: {
        justifyContent: 'center',
        alignItems: 'center',
        height: (platform.deviceHeight / 2) - 80
    },
    tabHeading: {
        flex: 1,
        height: 100,
        backgroundColor: platform.tabBgColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        padding: 10,
    },
    card: {
        // flex: 0, 
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginTop: 10,
        marginBottom: platform.platform == 'ios' ? platform.deviceHeight / 4 : 10,
    },
    form: {
        paddingHorizontal: 20,
    },
    viewInCollapsing: {
        position: 'absolute',
        // top : heightHeaderSpan - (heightHeaderSpan/2) - 30,
        left: 0,
        right: 0,
        paddingHorizontal: 10,
    },
    textNumber: {
        flex: 1,
        fontFamily: 'Nunito-Bold',
        color: '#454F63',
        fontSize: 20
    },
    textCollapsingDesc: {
        flex: 1,
        fontFamily: 'Nunito-SemiBold',
        color: '#78849E',
        fontSize: 14
    },
    textLabel: {
        marginTop: 15,
        marginBottom: 5
    },
    iconMore: {
        color: '#D0D8E6',
        fontSize: 24
    },
    textLogout: {
        fontFamily: 'Nunito-Regular',
        fontSize: 13
    },
    mainViewCollapsing: {
        flex: 1,
        // height: heightHeaderSpan - platform.toolbarHeight, 
        backgroundColor: '#F7F7FA'
    },
    heading: {
        color: '#fff',
        fontSize: 35,
        fontFamily: 'Nunito-Bold',
        flexWrap: "wrap",
        textAlign: 'center',
    },
    mainCardCollapsing: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 20,
        // marginVertical:10
    },
    smallCardCollapsing: {
        flex: 1,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center'
    },
    touchSmallCardCollapsing: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    btnSave: {
        marginTop: 30,
        marginBottom: 10,
        borderRadius: 10,
        height: 55
    },
    itemViewNotif: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffff',
        borderRadius: 4
    },
    itemViewDots: {
        flex: 0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconDot: {
        fontSize: 30,
        textAlign: 'center'
    },
    itemViewText: {
        flex: 0.85,
        paddingVertical: 15,
        marginRight: 5,
        justifyContent: 'center',
        alignItems: 'flex-start',
        borderBottomWidth: 1,
        borderBottomColor: '#F4F4F6'
    },
    textItemTime: {
        color: '#959DAD',
        fontSize: 12,
    },
    textItemDesc: {
        fontFamily: 'Nunito-SemiBold',
        color: '#454F63',
        fontSize: 16
    },
    backgroundCompetence: {
        padding: 5,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginRight: 10,
        backgroundColor: '#F0F0F0',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundBtnAdd: {
        padding: 5,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginRight: 10,
        borderRadius: 5,
    },
    textGrey: {
        fontSize: 14,
        color: '#6C6C6C'
    },
    textSpecialist: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 10,
        flexWrap: 'wrap'
    },
    tagIconX: {
        fontSize: 18,
        marginLeft: 10,
    },
    iconImage: {
        color: '#FFFFFF',
        // fontSize: 45,
        justifyContent: 'center',
        // opacity : 0.8,
        alignItems: 'center',
        flexWrap: "wrap",
        textAlign: 'center',
    },
    btnUploadImage: {
        marginTop: 5,
        marginBottom: 20,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: '#ffff',
        padding: 5,
    },
    invisibleBtn: {
        height: 0,
    },
    textWhite: {
        color: '#fff',
        flexWrap: "wrap",
        textAlign: 'center',
        fontSize: 12,
    }

});
