import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Image, TouchableOpacity, FlatList, Alert, BackHandler } from 'react-native';
import {
    Container, Header, Title, Button, Icon, Text, Left, Body, Right, Toast,
} from 'native-base';
import { Loader, } from './../../components';
import platform from '../../../theme/variables/platform';
import { EnumFeedsCategory, STORAGE_TABLE_NAME } from './../../libs/Common';
import { getApiFeeds } from './../../libs/NetworkUtility';
import { NavigationActions } from 'react-navigation';
import { ParamAction, doActionUserActivity } from './../../libs/NetworkUtility';
import { SafeAreaView } from 'react-native'
import { getData, KEY_ASYNC_STORAGE } from '../../../src/utils';
import { HeaderToolbar, SearchDataNotFound } from '../../../src/components';
export default class ProfileBookmark extends Component {

    page = 1;

    state = {
        showLoader: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        onRefresh: false,
        emptyDataDownload: false,
        downloadList: [],
        subscription: null,
        country_code: ''
    }

    constructor(props) {
        super(props);
        this.getDataCountryCode()
    }

    getDataCountryCode = () => {
        getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
            this.setState({
                country_code: profile.country_code
            })
        });
    };

    componentDidMount() {
        StatusBar.setHidden(false);
        // this.getDataCompetence();
        this.getDataDownload(this.page);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed()
            return true;
        });
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getDataCompetence() {
        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let dataProfile = JSON.parse(getJsonProfile);
        if (dataProfile != null) {
            let subscriptionId = [];
            // let subscriptionTitle = [];
            let dataSubscription = '';

            if (dataProfile.subscription != null && dataProfile.subscription.length > 0) {
                let self = this;
                dataProfile.subscription.map(function (d, i) {
                    subscriptionId.push(d.id);
                    // subscriptionTitle.push(d.title);
                });

                dataSubscription = subscriptionId.join(',');
            }

            this.setState({
                subscription: dataSubscription,
            })
        }

        this.getDataDownload(this.page);
    }

    async getDataDownload(page) {
        this.setState({ isFailed: false, isFetching: true, });

        try {
            let response = await getApiFeeds(page, null, true, true, true, false, false, true, false, null, true, true);

            if (response.isSuccess === true) {
                this.setState({
                    downloadList: [...this.state.downloadList, ...response.data],
                    isFetching: false, isSuccess: true, isFailed: false, onRefresh: false,
                    emptyDataDownload: response.data != null && response.data.length > 0 ? false : true
                });
            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true, onRefresh: false });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }

        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true, onRefresh: false });
            Toast.show({ text: 'error', position: 'top', duration: 3000 })
        }
    }


    handleLoadMore = () => {
        if (this.state.emptyDataDownload === true) {
            return;
        }
        this.page = this.state.isFailed == true ? this.page : this.page + 1;
        this.getDataDownload(this.page);
    }

    onRefreshDownloads() {
        this.page = 1
        this.setState({ onRefresh: true, emptyDataDownload: false, downloadList: [] })
        this.getDataDownload(this.page)
    }

    showNotifDelete = (item) => {
        if (item != null && item.id != null && item.category != null) {
            Alert.alert(
                'Warning',
                'Are you sure to delete this content ?',
                [
                    { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' },
                    { text: 'Yes', onPress: () => this.doDeleteContent(item.id, item.category) },
                ],
                { cancelable: true }
            )

        }
    }

    doDeleteContent = async (id, category) => {
        this.setState({ showLoader: true })
        let actionDeleted = await doActionUserActivity(ParamAction.DELETE_DOWNLOAD, category, id);
        this.setState({ showLoader: false })
        if (actionDeleted.isSuccess) {
            Toast.show({ text: 'Deleted success', position: 'top', duration: 3000 })
            this.onRefreshDownloads();
        } else {
            Toast.show({ text: 'Deleted failed', position: 'top', duration: 3000 })
        }
    }

    _renderItem = ({ item }) => {
        let iconType = item.category == EnumFeedsCategory.PDF_JOURNAL || item.category == EnumFeedsCategory.PDF_MATERI ? 'FontAwesome' :
            item.category == EnumFeedsCategory.VIDEO_LEARNING || item.category == EnumFeedsCategory.VIDEO_MATERI ? 'MaterialIcons' : 'Entypo';
        let iconName = item.category == EnumFeedsCategory.PDF_JOURNAL || item.category == EnumFeedsCategory.PDF_MATERI ? 'file-pdf-o' :
            item.category == EnumFeedsCategory.VIDEO_LEARNING || item.category == EnumFeedsCategory.VIDEO_MATERI ? 'play-circle-filled' : 'images';
        let downloadDate = item.start_date;

        return (
            // <TouchableOpacity style={{ flex: 1, backgroundColor: 'red', }} onPress={() => this.showDetail(item)}>
            //     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', }}>
            //         <View style={{ flex: 1, alignItems: 'flex-start' }}>
            //             <Text>test</Text>
            //         </View>
            //         <View style={{ flex: 1, alignItems: 'center' }}>
            //             <Text>test</Text>
            //         </View>
            //         <View style={{ flex: 1, alignItems: 'flex-end' }}>
            //             <Text>test</Text>
            //         </View>
            //     </View>
            // </TouchableOpacity>
            <TouchableOpacity onPress={() => this.showDetail(item)}>
                <View style={styles.mainItemView}>
                    <View style={styles.leftItemView}>
                        <View style={styles.leftItemViewInside}>
                            <Icon name={iconName} type={iconType} style={styles.iconItem} />
                        </View>
                    </View>
                    <View style={styles.centerItemView}>
                        <Text style={styles.textTitleItem}>{item.title}</Text>
                        <Text style={styles.textTimeItem}>{downloadDate}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => this.showNotifDelete(item)}
                        style={styles.rightItemView}>
                        <Icon name={'delete-forever'} type='MaterialIcons' style={styles.iconArrowRight} />
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }

    showDetail(data) {
        let route = null;

        if (data.category == EnumFeedsCategory.PDF_GUIDELINE || data.category == EnumFeedsCategory.PDF_JOURNAL) {
            route = "PdfView";
        } else if (data.category == EnumFeedsCategory.PDF_MATERI) {
            route = "PdfView";
            data.isCanDownload = false;
        } else if (data.category == EnumFeedsCategory.VIDEO_MATERI) {
            route = "VideoView";
            data.isCanDownload = false;
        }

        if (route != null) {
            // const showDetail = { type: "Navigate", routeName: route, params: data }
            // this.props.navigation.navigate(showDetail);
            this.props.navigation.dispatch(NavigationActions.navigate({
                routeName: route,
                params: data, key: `${data.category}-${data.id}`
            }));
        } else {
            Toast.show({ text: 'can not show detail', position: 'top', duration: 3000 })
        }
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetching == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true && (
            (this.page > 1))
        ) {
            return (
                <TouchableOpacity onPress={() => {
                    this.handleLoadMore()
                }}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItem = (type) => (
        <View style={styles.emptyItem}>
            {this._renderEmptyItemLoader()}
        </View>
    )

    _renderEmptyItemLoader() {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            if (this.state.downloadList.length == 0) {
                if (this.state.country_code == "ID") {
                    return (
                        <SearchDataNotFound
                            type="download"
                            title="Belum Ada Download"
                            subtitle="Tempat penyimpanan hasil download Anda akan ditampilkan disini."
                        />
                    )
                }
                else {
                    return (
                        <Text style={{ fontSize: 14, textAlign: 'center', flex: 1, marginVertical: 20, marginHorizontal: 10 }}>Data not found</Text>
                    )
                }

            }

            return null;
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }


    render() {
        const nav = this.props.navigation;
        const DownloadDetail = { type: "Navigate", routeName: "DownloadDetail" }

        return (
            <Container style={{ flex: 1 }}>

                {this.state.country_code == "ID" && (
                    <HeaderToolbar onPress={() => this.onBackPressed()} title="Download" />
                )}

                {this.state.country_code != "ID" && (
                    <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                        <Header noShadow>
                            <Left style={{ flex: 0.5 }}>
                                <Button
                                    accessibilityLabel="button_back"
                                    transparent onPress={() => this.onBackPressed()}>
                                    <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                                </Button>
                            </Left>
                            <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Title>Download</Title>
                            </Body>
                            <Right style={{ flex: 0.5 }} />
                        </Header>
                    </SafeAreaView>
                )}


                <Loader visible={this.state.showLoader} />
                <View style={{ flex: 1, backgroundColor: '#ffff', }}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        style={styles.flatlist}
                        contentContainerStyle={this.state.country_code == "ID" && this.state.isFetching == false && this.state.isSuccess == true &
                            this.state.downloadList.length == 0 ? styles.centerEmptyList : undefined}
                        data={this.state.downloadList}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        onRefresh={() => this.onRefreshDownloads()}
                        refreshing={this.state.onRefresh}
                        renderItem={this._renderItem}
                        ListEmptyComponent={this._renderEmptyItem}
                        ListFooterComponent={this._renderItemFooter}
                        keyExtractor={(item, index) => index.toString()
                        } />
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContent: {
    },
    flatlist: {
        flex: 1,
        paddingHorizontal: 10,
        backgroundColor: '#ffff',
    },
    mainItemView: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffff',
        borderBottomWidth: 2,
        borderBottomColor: '#F4F4F6',
        paddingVertical: 5,
        alignItems: 'center'
    },
    leftItemView: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    leftItemViewInside: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D31F55',
        padding: 10,
        borderRadius: 10
    },
    iconItem: {
        fontSize: 20,
        textAlign: 'center',
        color: '#F4F4F6'
    },
    centerItemView: {
        flex: 0.6,
        padding: 10,
        marginRight: 3,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    textTitleItem: {
        fontFamily: 'Nunito-Bold',
        color: '#454F63',
        fontSize: 16
    },
    textTimeItem: {
        color: '#959DAD',
        fontSize: 12
    },
    rightItemView: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconArrowRight: {
        fontSize: 21,
        textAlign: 'center',
        color: '#B21D1D'
    },
    centerEmptyList: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        width: "100%",
    }
});
