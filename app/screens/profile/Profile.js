import React, { Component, Fragment } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Image, Animated, FlatList, TouchableOpacity, Alert, UIManager, findNodeHandle, SafeAreaView } from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import firebase from 'react-native-firebase';
import { NavigationActions, StackActions } from 'react-navigation';
import { sendByEmailApp, convertMonth, validateEmail, validatePhoneNumber, STORAGE_TABLE_NAME, subscribeFcm, checkTopicsBySubscription, testID, validateFullName, validateNpaIDI, checkNpaIdiEmpty, unsubscribeTopicWebinarSubmit, removeEmojis, onlyAlphaNumeric } from './../../libs/Common';
import { ParamSetting, ParamCompetence, doSendCompetence, doUploadProfileImage, getProfileUser, updateDataProfile, submitAnswerQuiz } from './../../libs/NetworkUtility';
import DateTimePicker from 'react-native-modal-datetime-picker';
import guelogin from './../../libs/GueLogin';
import { deleteFIrebaseToken, getListNotification, doDetailCertificate } from './../../libs/NetworkUtility';
const gueloginAuth = firebase.app('guelogin');
import moment from 'moment-timezone';
import _ from 'lodash'
import Modal from 'react-native-modal';
import Orientation from "react-native-orientation";

import {
    Container, Header, Title, Button, Icon, Text, Left, Body, Right, Card, CardItem,
    Item, Input, Label, Toast, Tabs, Tab, ScrollableTab, TabHeading, Col, Row
} from 'native-base';
import { Loader, Popup } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import ImagePicker from 'react-native-image-picker';

import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DeviceInfo from 'react-native-device-info';
let from = {
    T_AND_C: 'T_AND_C',
    PrivacyPolicy: 'PrivacyPolicy',
    ContactUs: 'ContactUs',
    Logout: 'Logout',
    Profile_UploadPhoto: 'Profile_UploadPhoto',
    Profile_3Dots: 'Profile_3Dots',
    CHANGE_PHOTO: 'CHANGE_PHOTO'
}
import { Modalize } from 'react-native-modalize';
import country from "../../libs/countries/assets/data/country.json";
import { translate } from '../../libs/localize/LocalizeHelper'
import { BottomSheet } from '../../../src/components/molecules';
import { IconCambodiaFlag, IconIndonesiaFlag, IconMyanmarFlag, IconPhilippinesFlag, IconSingaporeFlag } from '../../../src/assets';

const initialErrorState = {
    fullNameFailed: false,
    emailFailed: false,
    npaIdiFailed: false,
    spesializationFailed: false,
    subscribtionFailed: false,
    phoneNoFailed: false,
    bornDateFailed: false,
    education1Failed: false,
    education2Failed: false,
    education3Failed: false,
    homeLocationFailed: false,
    clinicLocationFailed: false,
    clinicLocation2Failed: false,
    clinicLocation3Failed: false,

    errorMessageMedicalId: '',
    errorMessageFullname: '',
    errorMessagePhoneNumber: '',
    errorMessageEducation1: '',
    errorMessageEducation2: '',
    errorMessageEducation3: '',
    errorMessageDoctorPractice1: '',
    errorMessageDoctorPractice2: '',
    errorMessageDoctorPractice3: '',


};

export default class Register extends Component {

    page = 1;
    pageNotif = 1;

    SPESIALIZATION = 'spesialization';
    SUBSCRIPTION = 'subscription';

    phoneNo = '';
    edu1 = '';
    edu2 = '';
    edu3 = '';
    clinicLoc1 = '';
    clinicLoc2 = '';
    clinicLoc3 = '';

    state = {
        animatedValue: new Animated.Value(0),
        showLoader: true,
        uid: null,
        provider: 'email',
        name: null,
        nameFailed: false,
        email: null,
        emailFailed: false,
        photoURL: null,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        notifications: [],
        avatarSource: null,
        fullName: null,
        email: null,
        npaIdi: null,
        spesialization: null,
        subscribtion: null,
        phoneNo: null,
        shownBornDate: null,
        bornDate: null,
        education1: null,
        education2: null,
        education3: null,
        homeLocation: null,
        clinicLocation: null,
        clinicLocation2: null,
        clinicLocation3: null,

        totalBookmark: false,
        totalDownload: false,
        needRefresh: true,
        needSendSpesialzation: false,
        needSendSubscribtion: false,
        isShowingDatePicker: false,
        isPns: null,
        showErrorStatus: false,
        isEmptyDataNotif: false,
        isEmptyDataNotif: false,
        isFetchingNotif: false,
        isSuccessNotif: false,
        isFailedNotif: false,
        country_code: null,

        npaIdiFilled: false,
        fullNameFilled: false,

        positionInputPhoneNumber: 0,
        positionInputMedicalId: 0,
        positionInputEducation1: 0,
        positionInputDoctorPractice1: 0,
        positionInputFullname: 0,
        positionInputSpecialist: 0,
        positionParentSpecialistDoctorPractice: 0,
        positionInputInterest: 0,
        positionStatusPNS: 0,
        typeBottomSheet: "",

        ...initialErrorState
    }

    constructor(props) {
        super(props);
        // this.state = initialState;
    }

    resetErrorState() {
        this.setState(initialErrorState);
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        StatusBar.setHidden(false);
        AdjustTracker(AdjustTrackerConfig.Profile_Profile);
        // this.getData();
        // this.onRefreshListNotif();

        this.props.navigation.addListener(
            'didBlur',
            payload => {
                //prevent bugs flow npa idi -> always to certificate after save profile
                // and handle not to reset params if move to sub screen profile
                // such as home location, spesialist, etc.
                if (this.state.needRefresh == true) {
                    this.props.navigation.state.params = undefined
                }
            }
        )
        const didFocusSubscription = this.props.navigation.addListener(
            'didFocus',
            payload => {
                if (this.state.needRefresh === true) {
                    this.resetErrorState()
                    this.onRefreshListNotif();
                    this.getData();
                } else {
                    this.setState({ needRefresh: true })
                }
            }
        );


        let { params } = this.props.navigation.state;

        if (typeof params != 'undefined' && typeof params.isGotoDownload != 'undefined' &&
            params.isGotoDownload == true) {
            this.gotoProfileDownload();
        }
    }

    onRefreshListNotif() {
        this.pageNotif = 1;
        this.setState({ notifications: [] })
        this.getListNotif(this.pageNotif);
    }

    async getListNotif(pageNotif, limit = 10, type = '') {
        //getListNotification
        // try {
        //     let response = await getListNotification(pageNotif, limit, type);
        //     console.log('getListNotif ', response)
        //     if (response.isSuccess == true) {
        //         this.setState({ notifications: response.data.data.data })
        //     }
        // } catch (error) {
        //     console.log(error);
        // }

        this.setState({
            isFailedNotif: false,
            isFetchingNotif: true
        })

        try {
            let response = await getListNotification(pageNotif, limit, type);
            console.log('response notif: ', response)
            if (response.isSuccess) {
                this.setState({
                    notifications: [...this.state.notifications, ...response.data.data.docs],
                    isEmptyDataNotif: response.data.data.docs != null && response.data.data.docs.length > 0 ? false : true,
                    isFetchingNotif: false, isSuccessNotif: true, isFailedNotif: false,
                })
            } else {
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
                this.setState({
                    isFetchingNotif: false, isSuccessNotif: false, isFailedNotif: true
                })
            }
        } catch (error) {
            Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
            this.setState({
                isFetchingNotif: false, isSuccessNotif: false, isFailedNotif: true
            })
        }
    }

    getData() {
        let user = gueloginAuth.auth().currentUser;

        if (user) {
            this.setState({
                uid: user.uid,
                provider: user.providerId,
                name: user.displayName,
                email: user.email,
                photoURL: user.photoURL
            })
            this.getProfileDetail();
            // this.getNotifications(this.page);
        }
    }

    setMenuRef = (ref) => {
        this.menu = ref;
    };

    async getProfileDetail() {
        let dataAsync = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        console.log('profile dataAsync: ', JSON.parse(dataAsync))
        this.setState({ isFailed: false, isFetching: true, showLoader: true, spesialization: null, subscribtion: null });

        try {
            let response = await getProfileUser();
            this.doSetProfile(response);
        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true, showLoader: false });
            Toast.show({ text: 'Something went wrong!', position: 'top', buttonText: 'Okay' })
        }
    }

    doSetProfile(response) {
        if (response.isSuccess == true) {
            if (response.data != null) {
                let profile = response.data;
                console.log('doSetProfile: ', profile)
                if (profile != null) {
                    let shownBornDate = null;

                    if (profile.born_date != null && profile.born_date != '') {
                        // shownBornDate = profile.born_date.substring(8, 10) + ' ' + convertMonth(profile.born_date.substring(5, 7)) + ' ' + profile.born_date.substring(0, 4)
                        shownBornDate = moment(profile.born_date, 'YYYY-MM-DD').format('DD MMM YYYY')
                        console.warn('doSetProfile profile.born_date', profile.born_date)
                        console.warn('doSetProfile shownBornDate', shownBornDate)

                        profile.born_date = shownBornDate;
                    }

                    this.doSaveProfileUser(profile);


                    this.phoneNo = profile.phone == null ? '' : profile.phone;
                    this.edu1 = profile.education_1 == null ? '' : profile.education_1;
                    this.edu2 = profile.education_2 == null ? '' : profile.education_2;
                    this.edu3 = profile.education_3 == null ? '' : profile.education_3;
                    this.clinicLoc1 = profile.clinic_location_1 == null ? '' : profile.clinic_location_1;
                    this.clinicLoc2 = profile.clinic_location_2 == null ? '' : profile.clinic_location_2;
                    this.clinicLoc3 = profile.clinic_location_3 == null ? '' : profile.clinic_location_3;

                    this.setState({
                        country_code: profile.country_code,
                        fullName: profile.name,
                        email: profile.email,
                        npaIdi: profile.npa_idi,
                        spesialization: profile.spesialization,
                        subscribtion: profile.subscription,
                        phoneNo: profile.phone,
                        shownBornDate: shownBornDate,
                        bornDate: profile.born_date,
                        education1: profile.education_1,
                        education2: profile.education_2,
                        education3: profile.education_3,
                        homeLocation: profile.home_location,
                        clinicLocation: profile.clinic_location_1,
                        clinicLocation2: profile.clinic_location_2,
                        clinicLocation3: profile.clinic_location_3,
                        avatarSource: profile.photo != null && profile.photo != "" ? profile.photo : null,
                        totalBookmark: profile.counting.bookmark == null ? 0 : profile.counting.bookmark,
                        totalDownload: profile.counting.download == null ? 0 : profile.counting.download,
                        isFetching: false, isSuccess: true, isFailed: false, showLoader: false,
                        isPns: profile.pns,
                        npaIdiFilled: _.isEmpty(profile.npa_idi) ? true : false,
                        fullNameFilled: _.isEmpty(profile.npa_idi) ? true : false,
                    });
                }
            }
        } else {
            this.setState({ isFetching: false, isSuccess: true, isFailed: false });
        }

    }


    async doSaveProfileUser(profile) {
        try {
            let jsonProfile = JSON.stringify(profile);
            let saveProfile = await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, jsonProfile);

            console.log('saveProfile ', JSON.parse(jsonProfile).country_code)
        } catch (error) {
            console.log('failed update db local profile')
        }
    }


    //---- method  get notification phase 2 ----------//
    // async getNotifications(page) {
    //     this.setState({ isFailed: false, isFetching: true, });

    //     try {
    //         let response = await Api.get(`event?page=${page}`);

    //         this.setState({ notifications: [...this.state.notifications, ...response.result] });
    //         this.setState({ isFetching: false, isSuccess: true, isFailed: false });
    //     } catch(error) {
    //         this.setState({ isFetching: false, isSuccess: false, isFailed: true });
    //         Toast.show({ text: 'Terjadi kesalahan!', position: 'bottom', buttonText: 'Okay'})
    //     }
    // }

    handleLoadMore = () => {
        if (this.state.isEmptyDataNotif == true || this.state.isFetchingNotif == true) {
            return;
        }

        this.pageNotif = this.state.isFailedNotif == true ? this.pageNotif : this.pageNotif + 1;
        this.getListNotif(this.pageNotif);
    }

    showAlertCompleteProfile = () => {
        Alert.alert(
            'Warning',
            'Please input Medical ID first',
            [
                // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'OK', onPress: () => this.tabs.goToPage(0),
                },
            ],
            { cancelable: true }
        );
    }

    onPressitemNotification = async (dataNotif) => {
        console.log('onPressitemNotification: ', dataNotif)

        if (dataNotif.type.includes('certificate_')) {
            if (dataNotif.type.includes('cme')) {
                let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
                if (checkNpaIdiEmpty(profile)) {
                    this.showAlertCompleteProfile()
                }
                else {
                    this.doHitDetailCertificate(dataNotif)
                }
            }
            else {
                this.doHitDetailCertificate(dataNotif)
            }
        }
        else if (dataNotif.type == "event") {
            let data = JSON.parse(dataNotif.data)
            data.id = data.content_id
            data.isFromListNotifProfile = true
            this.props.navigation.navigate('EventDetail', {
                data: data,
                dataNotif: dataNotif
            })
        }
    }

    doHitDetailCertificate = async (dataNotif) => {
        let data = JSON.parse(dataNotif.data)
        this.setState({ showLoader: true });

        try {
            let response = await doDetailCertificate(data);
            console.log("doHitDetailCertificate response: ", response)
            this.setState({ showLoader: false });

            if (response.isSuccess && response.data != null) {

                this.gotoCertificate(response.data, dataNotif)
            }
        } catch (error) {
            console.log(error)
        }
    }

    gotoCertificate(data, dataNotif) {
        let params = {
            title: data.title,
            certificate: data.certificate,
            typeCertificate: data.type,
            status: data.status,
            dataNotif,
            isFromListNotifProfile: true
        }
        if (data.type = 'webinar') {
            params.isCertificateAvailable = true
        }
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'CmeCertificate',
            params: params,
        }));
    }

    _renderItem = ({ item }) => {
        isViewed = item.status == "unread" ? true : false;

        centerDotColor = isViewed ? '#DD2C2C' : '#78849E';
        backgroundColorItem = isViewed ? '#BBDEFB' : '#FFFFFF';

        let data = item
        data.isFromListNotif = true

        return (
            <TouchableOpacity
                // {...testID('buttonCmeCertificate')}
                accessibilityLabel="button_cme_certificate"
                activeOpacity={0.7} onPress={() => this.onPressitemNotification(data)}>
                <View style={[styles.itemViewNotif, { backgroundColor: backgroundColorItem }]}>
                    <View style={styles.itemViewDots}>
                        <Icon name={'dots-three-vertical'} type='Entypo' style={[styles.iconDot, { color: centerDotColor }]} />
                    </View>
                    <View style={styles.itemViewText}>
                        {/* <Text style={styles.textItemTime}>{item.title}</Text> */}
                        <Text style={styles.textItemDesc}>{item.title}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetchingNotif == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailedNotif == true && (
            (this.pageNotif > 1))
        ) {
            return (
                <TouchableOpacity onPress={() => {
                    this.handleLoadMore()
                }}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetchingNotif} transparent />);
    }

    _renderEmptyItemNotification() {
        // if (this.state.isFetching == false && this.state.isFailed == true) {
        //     return (
        //         <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={() => this.handleLoadMore()}>
        //             <Image source={require('./../../assets/images/noinet.png')} style={{width: 72, height: 72}} />
        //             <Text style={{textAlign: 'center'}}>Something went wrong, <Text style={{color: platform.brandInfo}}>tap to reload</Text></Text>
        //         </TouchableOpacity>
        //     )
        // } else if (this.state.isFetching == false && this.state.isSuccess == true) {
        //     return (
        //         <Text style={{textAlign: 'center'}}>Notification not found</Text>
        //     )
        // }

        return (<Text style={{ textAlign: 'center' }}>Notification not found</Text>);
    }

    _renderEmptyItemNotif() {
        if (this.state.isFetchingNotif == false && this.state.isFailedNotif == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetchingNotif == false && this.state.isSuccessNotif == true) {
            return (
                <Text style={{ textAlign: 'center' }}>Notification not found</Text>
            )
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }
    //---- end of method  get notification phase 2 ----------//


    doValidateDataProfile = async () => {
        let errorMessage = await this.doValidateField();
        let specialistFailed = this.state.spesialization.length > 0 || this.state.country_code == "PH" ? false : true
        let interestFailed = this.state.subscribtion.length > 0 || this.state.country_code == "PH" ? false : true

        let isValid = !this.state.npaIdiFailed && !this.state.fullNameFailed && !this.state.phoneNoFailed
            && !this.state.clinicLocationFailed && !this.state.education1Failed && !specialistFailed && !interestFailed && !this.state.showErrorStatus

        console.log("isValid ", isValid)

        if (isValid) {
            this.setState({ showLoader: true })

            if (this.state.needSendSpesialzation === true) {
                this.doSendSpesialization();
            } else if (this.state.needSendSubscribtion === true) {
                this.doSendSubscribtion();
            } else {
                this.doUpdateProfile();
            }

            // Adjust Tracker
            AdjustTracker(AdjustTrackerConfig.Profile_Save);

            this.scroll.getNode().scrollTo({
                animated: true,
                y: 0
            })

        } else {
            let focusPosition = 0
            if (this.state.npaIdiFailed) {
                focusPosition = this.state.positionInputMedicalId
            }
            else if (this.state.fullNameFailed) {
                focusPosition = this.state.positionInputFullname
            }
            else if (this.state.phoneNoFailed) {
                focusPosition = this.state.positionInputPhoneNumber
            }
            else if (this.state.education1Failed) {
                focusPosition = this.state.positionInputEducation1
            }
            else if (this.state.clinicLocationFailed) {
                focusPosition = this.state.positionInputDoctorPractice1
            }

            else if (this.state.showErrorStatus) {
                focusPosition = this.state.positionStatusPNS
            }

            else if (specialistFailed) {
                focusPosition = this.state.positionInputSpecialist
            }
            else if (interestFailed) {
                focusPosition = this.state.positionInputInterest
            }

            console.log("focusPosition: ", focusPosition)
            this.scroll.getNode().scrollTo({
                animated: true,
                y: focusPosition
            })
        }
    }



    doValidateField() {
        let errorMessage = '';
        let messages = [];
        let msgReq = [];

        let regexName = /^(?!\s)[A-Za-z.'’\s]+$/;
        let isNameValid = regexName.test(this.state.fullName)

        if (!this.state.fullName) {
            this.setState({ fullNameFailed: true, errorMessageFullname: "Full name is required" });
        }
        else if (!isNameValid) {
            this.setState({
                fullNameFailed: true,
                errorMessageFullname: "Full name is invalid"
            })
        }
        else if (this.state.fullName.length > 0 && this.state.fullName.length < 3) {
            this.setState({ fullNameFailed: true, errorMessageFullname: "Full name min. 3 character" });
        }
        else {
            this.setState({ fullNameFailed: false })
        }


        if (this.state.npaIdi) {
            if (!validateNpaIDI(this.state.npaIdi) && this.state.country_code == "ID") {
                this.setState({ npaIdiFailed: true, errorMessageMedicalId: "Medical ID is invalid" })
            }
            else if (this.state.npaIdi.length > 0 && this.state.npaIdi.length < 3) {
                this.setState({ npaIdiFailed: true, errorMessageMedicalId: "Medical ID min. 3 character" })
            }
            else {
                this.setState({ npaIdiFailed: false })
            }
        }
        else {
            this.setState({ npaIdiFailed: false })
        }

        console.log("npaIdi: ", this.state.npaIdi)
        console.log("npaIdiFailed: ", this.state.npaIdiFailed)

        if ((this.state.spesialization == null || this.state.spesialization.length <= 0) && this.state.country_code != 'PH') {
            this.setState({ spesializationFailed: true });
            messages.push('Select at least one spesialization');
        } else {
            this.setState({ spesializationFailed: false });
        }

        if ((this.state.subscribtion == null || this.state.subscribtion.length <= 0) && this.state.country_code != 'PH') {
            this.setState({ subscribtionFailed: true });
            messages.push('Select at least one your interest');
        } else {
            this.setState({ subscribtionFailed: false });
        }

        if (this.phoneNo == '' && this.state.country_code != 'PH') {
            this.setState({ phoneNoFailed: true, errorMessagePhoneNumber: "Phone number is required" });
        } else {
            if (validatePhoneNumber(this.phoneNo) === false && this.state.country_code != 'PH') {
                this.setState({ phoneNoFailed: true, errorMessagePhoneNumber: "Phone number is invalid" })
            } else {
                this.setState({ phoneNoFailed: false })
            }
        }

        if (!this.state.bornDate && this.state.country_code != 'PH') {
            this.setState({ bornDateFailed: true });
        } else {
            this.setState({ bornDateFailed: false })
        }

        if (this.edu1 == '' && this.state.country_code != 'PH') {
            this.setState({ education1Failed: true, errorMessageEducation1: "Please input at least one education" });
        } else {
            this.setState({ education1Failed: false })
        }

        if (this.state.isPns != null || this.state.country_code != 'ID') {
            this.setState({ showErrorStatus: false })
        } else {
            this.setState({ showErrorStatus: true })
        }


        if (this.clinicLoc1 == '' && this.state.country_code != 'PH') {
            this.setState({ clinicLocationFailed: true, errorMessageDoctorPractice1: "This field is required" });
        } else {
            this.setState({ clinicLocationFailed: false })
        }

        if (messages != null && messages.length > 0) {
            if (msgReq != null && msgReq.length > 0) {
                errorMessage += ' and ';
            }
            errorMessage += messages.join(', ');
        }

        return errorMessage;
    }

    //--- update firebase ----//
    updateProfile() {
        const nav = this.props.navigation;
        const user = guelogin.auth().currentUser;

        if (user) {
            this.setState({ showLoader: true });
            user.updateProfile({ displayName: this.state.name })
                .then(() => {
                    this.setState({ showLoader: false });
                    Toast.show({ text: 'Profile updated!', position: 'top', duration: 3000 });
                }).catch((error) => {
                    console.log(error)
                    this.setState({ showLoader: false });
                    Toast.show({ text: error, position: 'top', duration: 3000 });
                });
        }
    }
    //--- end of update firebase ---//

    async doSendSpesialization() {
        let response = await doSendCompetence(this.state.spesialization, ParamCompetence.SPESIALIZATION);
        if (response.isSuccess == true) {
            this.setState({ needSendSpesialzation: false })
            if (this.state.needSendSubscribtion) {
                this.doSendSubscribtion();
            } else {
                this.doUpdateProfile();
            }
        } else {
            this.setState({ showLoader: false })
            alert(response.message)
        }
    }

    async doSendSubscribtion() {
        let response = await doSendCompetence(this.state.subscribtion, ParamCompetence.SUBSCRIPTION);
        if (response.isSuccess == true) {
            await checkTopicsBySubscription(this.state.subscribtion)
            this.setState({ needSendSubscribtion: false })
            this.doUpdateProfile();
        } else {
            this.setState({ showLoader: false })
            alert(response.message)
        }
    }

    async doUpdateProfile() {
        let uid = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);
        let fcm_token = await AsyncStorage.getItem(STORAGE_TABLE_NAME.FCM_TOKEN);

        let npa_idi = ""

        let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE)
        profile = JSON.parse(profile)

        if (profile.country_code != "ID") {
            npa_idi = this.state.npaIdi
        }
        else {
            npa_idi = !this.state.npaIdiFilled ? "" : this.state.npaIdi
        }


        this.setState({
            phoneNo: this.phoneNo,
            education1: this.edu1,
            education2: this.edu2,
            education3: this.edu3,
            clinic_location_1: this.clinicLoc1,
            clinic_location_2: this.clinicLoc2,
            clinic_location_3: this.clinicLoc3,
        })


        let params = {
            country_code: this.state.country_code,
            uid: uid,
            npa_idi: npa_idi,
            name: this.state.fullName,
            email: this.state.email,
            phone: this.phoneNo,
            born_date: moment(this.state.shownBornDate, 'DD MMM YYYY').format('YYYY-MM-DD'),
            //this.state.bornDate,
            education_1: this.edu1,
            education_2: this.edu2 == null ? '' : this.edu2,
            education_3: this.edu2 == null ? '' : this.edu3,
            home_location: this.state.homeLocation == null ? '' : this.state.homeLocation,
            clinic_location_1: this.clinicLoc1,
            clinic_location_2: this.clinicLoc2 == null ? '' : this.clinicLoc2,
            clinic_location_3: this.clinicLoc3 == null ? '' : this.clinicLoc3,
            pns: this.state.isPns,

            //device info
            app_version: DeviceInfo.getVersion(),
            device_unique_id: DeviceInfo.getUniqueId(),
            fcm_token: fcm_token,
            device_platform: DeviceInfo.getSystemName(),
            system_version: DeviceInfo.getSystemVersion(),
            os_build_version: await DeviceInfo.getBuildId(), //should be await, because it was promise,
            device_brand: DeviceInfo.getBrand()
        }


        console.log("updateDataProfile params: ", params)

        let response = await updateDataProfile(params);
        console.log('doUpdateProfile response', response)

        if (response.isSuccess == true) {
            //Toast.show({ text: 'Profile updated!', position: 'top', duration: 3000 });
            Toast.show({ text: response.message, position: 'top', duration: 3000 });
            let { params } = this.props.navigation.state;
            console.log("paramsCMECertificate: ", params)

            if (params != undefined) {
                if (!_.isEmpty(response.docs.npa_idi) && params.from != undefined) {

                    // show Certificate
                    if (params.from == 'CmeQuiz') {
                        params.from = 'profile'
                    }


                    if (params.done == "true") {
                        let requestParams = await AsyncStorage.getItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ);
                        requestParams = JSON.parse(requestParams)
                        console.log("SUBMIT_CME_QUIZ: ", requestParams)

                        let responseSubmitAnswerQuiz = await submitAnswerQuiz(requestParams);
                        console.log("responseSubmitAnswerQuiz: ", responseSubmitAnswerQuiz)

                        if (responseSubmitAnswerQuiz.isSuccess) {
                            await AsyncStorage.removeItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ)

                            params.certificate = responseSubmitAnswerQuiz.data.certificate
                            this.props.navigation.dispatch(NavigationActions.navigate({
                                routeName: 'CmeCertificate',
                                params: params,
                            }));
                        }
                    } else if (params.done == "false") {
                        params.from = 'profile'
                        this.props.navigation.dispatch(NavigationActions.navigate({
                            routeName: 'CmeDetail',
                            params: params,
                        }));
                    }
                }

                //if npa idi not found 
                else {
                    this.setState({
                        npaIdiFailed: true
                    })
                    this.scroll.getNode().scrollTo({
                        animated: true,
                        y: this.state.positionInputMedicalId
                    })
                }
            }

            this.getData();
            this.setState({ showLoader: false })


        } else {
            Toast.show({ text: response.message, position: 'top', duration: 3000 });
        }

    }

    isFetchLogout = false;

    unsubscribeTopicAttendee = async () => {

        await unsubscribeTopicWebinarAttendee()
        await unsubscribeTopicEventAttendee()

        await removeData(KEY_ASYNC_STORAGE.FCM_TOPIC_EVENT_ATTENDEE)
        await removeData(KEY_ASYNC_STORAGE.FCM_TOPIC_WEBINAR_ATTENDEE)
    }

    async logout() {
        if (this.isFetchLogout == true) {
            return;
        }

        this.isFetchLogout = true;
        const user = gueloginAuth.auth().currentUser;
        // await AsyncStorage.removeItem('totalShowPopup');
        // await AsyncStorage.removeItem('showPopup');
        let topicFCM = await subscribeFcm();

        if (user) {
            // this.removeToken();
            this.setState({ showLoader: true });

            try {
                let response = await deleteFIrebaseToken();
                if (response.isSuccess == true || response.message == 'No available device token to be removed') {
                    let delFcmToken = await AsyncStorage.setItem(STORAGE_TABLE_NAME.FCM_TOKEN, "");
                    await AsyncStorage.setItem(STORAGE_TABLE_NAME.IS_UPDATE_PROFILE, "")
                    await AsyncStorage.removeItem(STORAGE_TABLE_NAME.FILTER_WEBINAR)
                    await checkTopicsBySubscription(null);
                    let delFcmTopics = await AsyncStorage.setItem(STORAGE_TABLE_NAME.FCM_TOPICS_SUBSCRIBTIONS, "");

                    // const resetAction = NavigationActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login' })] });
                    // const resetAction = NavigationActions.reset({ index: 0, actions: [NavigationActions.navigate({routeName: 'Splash'})] });

                    gueloginAuth.auth().signOut()
                        .then(() => {
                            let setLogin = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.IS_LOGIN, 'N');
                            let uid = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.UID, null);
                            let profile = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, null);
                            let auth = async () => await AsyncStorage.setItem(STORAGE_TABLE_NAME.AUTHORIZATION, null);
                            let fcm = async () => await firebase.messaging().unsubscribeFromTopic(topicFCM);

                            let unsubscribeFromTopicWebinarSubmit = async () => await unsubscribeTopicWebinarSubmit()
                            unsubscribeFromTopicWebinarSubmit()

                            let unsubscribeAttendee = async () => await this.unsubscribeTopicAttendee()
                            unsubscribeAttendee()


                            let removeTopicWebinarSubmit = async () => await AsyncStorage.removeItem(STORAGE_TABLE_NAME.FCM_TOPICS_WEBINAR_SUBMIT)
                            removeTopicWebinarSubmit()

                            setLogin() //should be called, if not async storage not updated

                            fcm()

                            let removeNotif = async () => await firebase.notifications.removeAllDeliveredNotifications
                            removeNotif()

                            // let clearDataSubmitQuiz = async () => await AsyncStorage.removeItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ)
                            // clearDataSubmitQuiz()

                            setTimeout(() => {
                                this.setState({ showLoader: false });
                                let resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'ChooseCountry' })] });
                                this.props.navigation.dispatch(resetAction);
                                //prevent bugs: blink open reg hello before to login
                                //this.props.navigation.replace('Login');

                            }, 300);
                        })
                        .catch((error) => {
                            console.log(error)
                            this.setState({ showLoader: false });
                            Toast.show({ text: error, position: 'top', duration: 3000 });
                        })
                } else {
                    Toast.show({ text: response.message, position: 'top', duration: 3000 });
                }
            } catch (error) {
                console.log(error);
            }

            this.isFetchLogout = false
        }
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        if (this.state.avatarSource != null) {
            this.sendAdjust(from.CHANGE_PHOTO)
        }
        else {
            this.sendAdjust(from.Profile_UploadPhoto)
        }

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                // let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.doUploadImage(response.uri, this.state.fullName)
            }
        });
    }

    async doUploadImage(uri, namefile) {
        this.setState({ showLoader: true });
        let response = await doUploadProfileImage(uri, namefile);
        if (response.isSuccess === true) {
            this.setState({ avatarSource: response.data, showLoader: false })
        } else {
            this.setState({ showLoader: false })
            alert(response.message);
        }
    }

    _renderSpesialization = () => {
        let keyAdd = this.state.spesialization != null ? this.state.spesialization.length : 1;
        return (
            <View style={{}}>
                {this._renderTagSpecialist()}
                <View style={{ flex: 1, }}>
                    {this.state.spesialization == null || this.state.spesialization.length <= 0 &&
                        (<Text style={{ flex: 1, color: '#CB1D50' }}>{translate("please_choose_specialization")}</Text>)}
                    <TouchableOpacity style={{ padding: 5 }}
                        {...testID('choose_spesialis')}
                        onPress={() => this.gotoSelectSpecialist()}>
                        <Text textAsButton style={styles.textAdd}>Add Specialization</Text>

                    </TouchableOpacity>
                </View>
            </View>);
    }

    _renderTagSpecialist = () => {
        let items = [];
        if (this.state.spesialization != null && this.state.spesialization.length > 0) {
            let self = this;
            console.log('renderTagSpecialist this.state.spesialization: ', this.state.spesialization)
            this.state.spesialization.map(function (d, i) {
                items.push(
                    <TouchableOpacity key={i} style={[styles.mainTouchSp, { borderWidth: d.subspecialist.length > 0 ? 2 : 0 }]}>
                        <View style={styles.viewTopSp}>
                            <TouchableOpacity
                                // {...testID('buttonX')}
                                accessibilityLabel="buttonX"
                                onPress={() => self.doDeletedSpesialization(self, i)}>
                                {/* <Icon type="MaterialCommunityIcons" name='close' style={styles.tagIconX} /> */}
                                <Image
                                    resizeMode={"contain"}
                                    source={require("./../../assets/images/delete.png")}
                                    style={{ width: 48, height: 48, margin: -16 }}
                                />
                            </TouchableOpacity>
                            <Text regular style={{ flex: 1, textAlign: 'left', textAlignVertical: 'center' }}>{d.description}</Text>
                        </View>
                        {self.renderSubSpecialist(d, i)}
                    </TouchableOpacity>
                )
            });
        }

        return items
    }


    renderSubSpecialist = (specialist, indexSp) => {
        let items = []
        let self = this;
        if (specialist.subspecialist != null && specialist.subspecialist.length > 0) {
            specialist.subspecialist.map(function (d, i) {
                if (d.id > 0) {
                    items.push(
                        <View key={i} style={styles.viewBottomSp}>
                            <TouchableOpacity key={i} onPress={() => self.deleteSubSpecialist(self, indexSp, i)}>
                                <Icon type="MaterialCommunityIcons" name='close' style={styles.tagIconX} />
                            </TouchableOpacity>
                            <Text regular style={{ flex: 1, textAlign: 'left', textAlignVertical: 'center' }}>{d.description}</Text>
                        </View>
                    )
                }
            })
        }

        return items;
    }

    deleteSubSpecialist = (self, indexSp, index) => {
        self.state.spesialization[indexSp].subspecialist.splice(index, 1);
        self.setState({ spesialization: self.state.spesialization, needSendSpesialzation: true })
    }

    doDeletedSpesialization = (self, index) => {
        self.state.spesialization.splice(index, 1);
        self.setState({ spesialization: self.state.spesialization, needSendSpesialzation: true })
    }

    _renderSubscribtions = () => {
        console.log('_renderSubscriptions this.state.subscribtion: ', this.state.subscribtion)
        let keyAdd = this.state.subscribtion != null ? this.state.subscribtion.length : 1;
        return (
            <View>
                {this._renderTagSubscribtions()}
                <View style={{ flex: 1 }}>
                    {this.state.subscribtion == null || this.state.subscribtion.length <= 0 &&
                        (<Text style={{ flex: 1, color: '#CB1D50' }}>{translate("please_choose_interest")}</Text>)}
                    <TouchableOpacity style={{ padding: 5 }}
                        {...testID('choose_subscription')}
                        onPress={() => this.gotoSelectSubscribtions()}>
                        <Text textAsButton style={styles.textAdd} >Add Interest</Text>

                    </TouchableOpacity>
                </View>
            </View>);

    }

    _renderTagSubscribtions = () => {
        let items = [];

        if (this.state.subscribtion != null && this.state.subscribtion.length > 0) {
            let isSingleInterest = this.state.subscribtion.length == 1 ? true : false
            let self = this;

            this.state.subscribtion.map(function (d, i) {

                if (d.id > 0) {
                    items.push(
                        <TouchableOpacity key={i} style={[styles.mainTouchSp]}>
                            <View style={styles.viewTopSp}>
                                <TouchableOpacity
                                    // {...testID('buttonX')}
                                    accessibilityLabel="buttonX"
                                    onPress={() => self.doDeletedSubscribtion(self, i)}>
                                    <Image
                                        resizeMode={"contain"}
                                        source={require("./../../assets/images/delete.png")}
                                        style={{ width: 48, height: 48, margin: -16 }}
                                    />
                                </TouchableOpacity>
                                <Text regular style={{ flex: 1, textAlign: 'left', textAlignVertical: 'center' }}>{d.description}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                }
                else {
                    //handle initial interest dokter umum
                    if (isSingleInterest) {
                        items.push(
                            <TouchableOpacity key={i} style={[styles.mainTouchSp, { borderWidth: 0 }]}>
                                <View style={styles.viewTopSp}>
                                    <TouchableOpacity
                                        // {...testID('buttonX')}
                                        accessibilityLabel="buttonX"
                                        onPress={() => self.doDeletedSubscribtion(self, i)}>
                                        <Image
                                            resizeMode={"contain"}
                                            source={require("./../../assets/images/delete.png")}
                                            style={{ width: 48, height: 48, margin: -16 }}
                                        />
                                    </TouchableOpacity>
                                    <Text regular style={{ flex: 1, textAlign: 'left', textAlignVertical: 'center' }}>{d.description}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }
                }
            });

        }

        return items
    }

    doDeletedSubscribtion = (self, index) => {
        self.state.subscribtion.splice(index, 1);
        if (self.state.subscribtion.length > 0 && self.state.subscribtion[0].id == 0) {
            self.state.subscribtion.splice(0, 1);
        }
        self.setState({ subscribtion: self.state.subscribtion, needSendSubscribtion: true })
    }

    doSelectSpesialization(type) {
        this.setState({ needRefresh: false })
        this.props.navigation.navigate('Subscribtion', {
            from: 'PROFILE',
            type: type,
            // updateData: this.doRefreshData, 
            selectedSpecialist: type == this.SPESIALIZATION ? this.state.spesialization : this.state.subscribtion,
            updateData: type == this.SPESIALIZATION ? this.doUpdateDataSpesialization : this.doUpdateDataSubscribtion,
        })
    }

    gotoSelectSpecialist = () => {
        console.log('gotoSelectSpecialist this.state.spesialization: ', this.state.spesialization)
        this.setState({ needRefresh: false })
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSpecialist',
            key: `gotoSelectSpecialist`,
            params: {
                specialistList: this.state.spesialization,
                // saveDataSp: this.updateDataSp,
                saveDataSp: this.doUpdateDataSpesialization,
                from: 'SPECIALIST'
            }
            ,
        }));
    }

    gotoSelectSubscribtions = () => {
        console.log('gotoSelectSubscribtions this.doUpdateDataSubscribtion', this.state.subscribtion)
        this.setState({ needRefresh: false })

        let dataInterest = this.state.subscribtion

        if (this.state.subscribtion == 1) {
            if (this.state.subscribtion[0].id == "0") {
                dataInterest = []
            }
        }

        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSpecialist',
            key: `gotoSelectSpecialist`,
            params: {

                specialistList: dataInterest,
                // saveDataSp: this.updateDataSp,
                saveDataSp: this.doUpdateDataSubscribtion,
                from: 'SUBSCRIBTIONS'
            },
        }));
    }

    doUpdateDataSpesialization = (spesializationList) => {
        this.setState({ spesialization: spesializationList, needSendSpesialzation: true });
    };

    doUpdateDataSubscribtion = (subscribtionList) => {
        this.setState({ subscribtion: subscribtionList, needSendSubscribtion: true });
    };


    _showDatePicker = () => this.setState({ isShowingDatePicker: true });

    _hideDatePicker = () => this.setState({ isShowingDatePicker: false });

    _handleDatePicked = (date) => {
        let shownDatestring = date.getDate() + " " + convertMonth((date.getMonth() + 1)) + " " + date.getFullYear();
        let bornDate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

        this.setState({ shownBornDate: shownDatestring, bornDate: bornDate })
        this._hideDatePicker();
    };

    doChooseCity = () => {
        this.setState({ needRefresh: false })
        this.props.navigation.navigate('SearchCity', {
            onSelectedCity: this.onSelectedCity,
        })
    }

    onSelectedCity = (city) => {
        this.setState({ homeLocation: city, homeLocationFailed: false })
    }

    gotoWebView(type) {
        this.menu.hide();
        this.setState({ needRefresh: false })
        let webView = { type: "Navigate", routeName: "GeneralWebview", params: type }
        this.props.navigation.navigate(webView);
    }

    gotoCmeCertificate(data) {
        let cmeCertificate = { type: "Navigate", routeName: 'CmeCertificate', params: data };
        this.props.navigation.navigate(cmeCertificate)
    }

    onChangeTextPhoneNo(value) {
        this.phoneNo = value
        // if (platform.platform != 'ios') {
        this.setState({ phoneNo: value })
        // }
    }

    onChangeTextEdu1(value) {
        this.edu1 = value
        // if (platform.platform != 'ios') {
        this.setState({ education1: value })
        // }
    }

    onChangeTextEdu2(value) {
        this.edu2 = value
        // if (platform.platform != 'ios') {
        this.setState({ education2: value })
        // }
    }

    onChangeTextEdu3(value) {
        this.edu3 = value
        // if (platform.platform != 'ios') {
        this.setState({ education3: value })
        // }
    }
    onChangeTextClinic1(value) {
        this.clinicLoc1 = value
        // if (platform.platform != 'ios') {
        this.setState({ clinicLocation: value })
        // }
    }

    onChangeTextClinic2(value) {
        this.clinicLoc2 = value
        // if (platform.platform != 'ios') {
        this.setState({ clinicLocation2: value })
        // }
    }

    onChangeTextClinic3(value) {
        this.clinicLoc3 = value
        // if (platform.platform != 'ios') {
        this.setState({ clinicLocation3: value })
        // }
    }

    onChangeTab(tab) {
        switch (tab.i) {
            case 0: AdjustTracker(AdjustTrackerConfig.Profile_Profile); break;
            case 1: AdjustTracker(AdjustTrackerConfig.Profile_Notif); break;
        }
    }

    gotoProfileDownload = () => {
        AdjustTracker(AdjustTrackerConfig.Profile_Download);
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: 'ProfileDownload',
                key: `profileDownload`
            }));
    }

    gotoProfileBookmark = () => {
        AdjustTracker(AdjustTrackerConfig.Profile_Bookmark);
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: 'ProfileBookmark',
                key: `profileBookmark`
            }));
    }

    renderEducations = () => {
        return (
            <View>
                <Item noShadowElevation style={styles.itemInput} error={this.state.education1Failed}>
                    <Input
                        newDesign
                        // {...testID('inputEducationalDegree1')}
                        accessibilityLabel="input_educational_degree1"
                        autoCapitalize="none"
                        placeholder={translate("placeholder_education")}
                        value={this.state.education1}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.onChangeTextEdu1(removeEmojis(txt))}
                    />
                </Item>
                {this.state.education1Failed && (
                    <Text textError style={styles.textError}>{this.state.errorMessageEducation1}</Text>
                )}
                <Item noShadowElevation style={styles.itemInput} error={this.state.education2Failed}>
                    <Input
                        newDesign
                        // {...testID('inputEducationalDegree2')}
                        accessibilityLabel="input_educational_degree2"
                        autoCapitalize="none"
                        placeholder={translate("placeholder_education")}
                        value={this.state.education2}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.onChangeTextEdu2(removeEmojis(txt))}
                    />
                </Item>
                <Item noShadowElevation style={styles.itemInput} error={this.state.education3Failed}>
                    <Input
                        newDesign
                        // {...testID('inputEducationalDegree3')}
                        accessibilityLabel="input_educational_degree3"
                        autoCapitalize="none"
                        placeholder={translate("placeholder_education")}
                        value={this.state.education3}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.onChangeTextEdu3(removeEmojis(txt))}
                    />
                </Item>
            </View>)
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.T_AND_C:
                token = AdjustTrackerConfig.Profile_T_AND_C
                break;
            case from.PrivacyPolicy:
                token = AdjustTrackerConfig.Profile_PrivacyPolicy
                break;
            case from.ContactUs:
                token = AdjustTrackerConfig.Profile_ContactUs
                break;
            case from.Logout:
                token = AdjustTrackerConfig.Profile_Logout
                break;
            case from.Profile_UploadPhoto:
                token = AdjustTrackerConfig.Profile_UploadPhoto
                break;
            case from.Profile_3Dots:
                token = AdjustTrackerConfig.Profile_3Dots
                break;
            case from.CHANGE_PHOTO:
                token = AdjustTrackerConfig.Profile_ChangePhoto
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }

    _renderSelectedFlag = () => {
        let country_code = this.state.country_code
        if (country_code == "KH") {
            return (
                // <Image
                //     resizeMode={"contain"}
                //     source={require("./../../assets/images/flags/Cambodia.png")}
                //     style={{ width: 24, height: 24, marginRight: 12 }}
                // />
                <IconCambodiaFlag />
            );
        } else if (country_code == "MM") {
            return (
                <IconMyanmarFlag />
            );
        } else if (country_code == "PH") {
            return (
                <IconPhilippinesFlag />
            );
        } else if (country_code == "SG") {
            return (
                <IconSingaporeFlag />
            );
        } else {
            return (
                <IconIndonesiaFlag />
            );
        }
    };

    renderBottomSheetWarningChangeCountry = () => {
        return (
            <Modalize
                withOverlay={true}
                withHandle={true}
                handleStyle={{
                    backgroundColor: 'transparent'
                }}

                adjustToContentHeight={true}
                closeOnOverlayTap={false}
                panGestureComponentEnabled={false}
                ref={(ref) => {
                    this.modalizeBottomSheet = ref;
                }}

                overlayStyle={{
                    backgroundColor: "#000000CC",
                }}

                onClose={() =>
                    this.props.navigation.setParams({ isHide: false })
                }
                modalStyle={{
                    borderTopLeftRadius: 16,
                    borderTopRightRadius: 16,
                    paddingHorizontal: 16,
                }}

                HeaderComponent={
                    <View style={{
                        height: 4,
                        borderRadius: 2,
                        width: 40,
                        marginTop: 8,
                        backgroundColor: '#454F6329',
                        alignSelf: 'center'
                    }}>

                    </View>
                }
            >


                {this._renderContentBottomSheetCountry()}

            </Modalize>
        )
    }

    _renderCountries = () => {
        let resultCountry = country.filter((obj) => {
            return this.state.country_code == obj.country_code;
        });
        console.log('resultCountry: ', resultCountry)
        let countryName = resultCountry.length > 0 ? resultCountry[0].name : ''
        return (
            <Col>
                <Label black style={{ marginBottom: 12 }}>Country</Label>

                <Item
                    noShadowElevation
                    {...testID('item_select_country')}

                    onPress={() =>
                        this.modalizeBottomSheet.open() +
                        console.log('this.props: ', this.props) +
                        this.props.navigation.setParams({ isHide: true })
                    }

                    error={this.state.emailFailed}
                    style={{ height: 56, alignItems: "center", marginBottom: this.state.country_code == 'ID' ? 24 : 0 }}
                >

                    <View style={{ marginRight: 12 }}>{this._renderSelectedFlag()}</View>

                    <Text regular>{countryName}</Text>

                    <Right>

                        <Image
                            resizeMode={"contain"}
                            source={require("./../../assets/images/arrow-down.png")}
                            style={{ width: 48, height: 48, marginRight: -16 }}
                        />
                    </Right>
                </Item>

            </Col >
        );
    };

    _renderContentBottomSheetCountry = () => {
        return (
            <Col style={{
                alignItems: 'center',
                marginBottom: 16
            }}>
                <Image
                    resizeMode='contain'
                    style={{
                        height: 82,
                        width: 82,
                        marginVertical: 32,
                    }} source={require("../../assets/images/confirmation-warning-icon.png")}>

                </Image>
                <Text bold style={{
                    marginBottom: 16,
                    textAlign: 'center',
                    fontSize: 20,
                }}>Confirmation</Text>
                <Text regular style={{
                    textAlign: 'center',
                }}>If you want to change your country, please contact cs@d2d.co.id or press send to create email contents.</Text>

                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginTop: 24
                }}>
                    <Button
                        {...testID('button_cancel_change_country')}
                        flat secondary block style={{ flex: 1, marginRight: 8 }} onPress={() => this.modalizeBottomSheet.close()}>
                        <Text textButtonCancel>CANCEL</Text>
                    </Button>
                    <Button
                        {...testID('button_send_change_country')}
                        flat block style={{ flex: 1, marginLeft: 8 }} onPress={() => this.modalizeBottomSheet.close() + sendByEmailApp('changeCountry')}>
                        <Text textButton>SEND</Text>
                    </Button>
                </Row>
            </Col>
        )
    }

    renderHomeLocation = () => {
        return (
            <View>
                <Label black style={styles.textLabel}>Home Location</Label>
                <Item noShadowElevation style={styles.itemInput} error={this.state.homeLocationFailed} onPress={() => this.doChooseCity()}>
                    <Input
                        newDesign
                        // {...testID('inputHomeLocation')}
                        accessibilityLabel="input_home_location"
                        autoCapitalize="none"
                        placeholder={'Home Location'}
                        // onTouchStart={this.doChooseCity}
                        editable={false}
                        pointerEvents="none"
                        value={this.state.homeLocation}
                        placeholderTextColor={platform.placeholderTextColor}
                        onChangeText={(txt) => this.setState({ homeLocation: txt })}
                    />
                </Item>
            </View>

        )
    }
    renderStatusPNS = () => {
        return (
            <View>
                <Label black style={styles.textLabel}>Status</Label>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 0.5, }}>
                        <TouchableOpacity style={styles.touchRadio}
                            onPress={() => this.setState({ isPns: true })}>
                            <Icon name={this.state.isPns == true ? 'md-radio-button-on' : 'md-radio-button-off'}
                                style={{
                                    color: this.state.isPns == true ? '#0EA2DD' : '#C3D1E8',
                                    margin: 5
                                }} />
                            <Text style={styles.textPns}>PNS</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 0.5, }}>
                        <TouchableOpacity style={styles.touchRadio}
                            onPress={() => this.setState({ isPns: false })}>
                            <Icon name={this.state.isPns == false ? 'md-radio-button-on' : 'md-radio-button-off'}
                                style={{
                                    color: this.state.isPns == false ? '#0EA2DD' : '#C3D1E8',
                                    margin: 5
                                }} />
                            <Text style={styles.textPns}>NON PNS</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.isPns == null && this.state.showErrorStatus &&
                    <Text style={{ flex: 1, color: '#CB1D50' }}>Please select status</Text>}
            </View>
        )
    }

    onLayoutPhoneNumber = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutPhoneNumber y: ", y)
        this.setState({
            positionInputPhoneNumber: y + heightHeaderSpan
        });
    };

    onLayoutMedicalId = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutMedicalId y: ", y)

        this.setState({
            positionInputMedicalId: y
        });
    };

    onLayoutFullname = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutFullname y: ", y)

        this.setState({
            positionInputFullname: y + heightHeaderSpan
        });
    };

    onLayoutEducation1 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutEducation1 y: ", y)
        let position = this.state.positionParentSpecialistDoctorPractice + y - heightHeaderSpan
        //+ (this.state.positionParentSpecialistDoctorPractice - y - heightHeaderSpan)
        console.log("onLayoutEducation1 position: ", position)

        this.setState({
            positionInputEducation1: position
        });

    };

    onLayoutDoctorPractice1 = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutDoctorPractice1 y: ", y)
        let position = this.state.positionParentSpecialistDoctorPractice + y - heightHeaderSpan
        // let position = this.state.positionParentSpecialistDoctorPractice + ((y + heightHeaderSpan) - this.state.positionParentSpecialistDoctorPractice)
        console.log("onLayoutDoctorPractice1 position: ", position)

        this.setState({
            positionInputDoctorPractice1: position
        });
    };

    closeScreen = () => {
        //this.props.navigation.goBack()
        console.log("closeScreen ")
        this.popup.togglePopup();
    }

    onPressLogout = () => {
        this.state.country_code == "PH" ? this.showPopupWarningLogout() : this.logout() + this.sendAdjust(from.Logout)
    }

    showPopupWarningLogout = () => {
        console.log("showPopupWarningLogout ")
        this.menu.hide();

        setTimeout(() => {

            if (this.popup) {
                this.popup.togglePopup();
            }
        }, 300);

    }

    onLayoutSpecialist = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutSpecialist y: ", y)

        this.setState({
            positionInputSpecialist: this.state.positionParentSpecialistDoctorPractice + y - heightHeaderSpan
        });
    };

    onLayoutParentSpecialistDoctorPractice = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutParentSpecialistDoctorPractice y: ", y)

        this.setState({
            positionParentSpecialistDoctorPractice: y + heightHeaderSpan
        });
    };

    onLayoutInterest = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutInterest y: ", y)

        this.setState({
            positionInputInterest: this.state.positionParentSpecialistDoctorPractice + y - heightHeaderSpan
        });
    };

    onLayoutStatusPNS = ({ nativeEvent: { layout: { x, y, width, height } } }) => {
        console.log("onLayoutStatusPN y: ", y)

        this.setState({
            positionStatusPNS: y + heightHeaderSpan
        });
    };

    openBSDeleteAccountConfirmation = () => {
        this.onOpen();
        this.setState({ typeBottomSheet: "BottomSheetDeleteAccountConfirmation" });
    }

    onOpen = () => {
        this.menu.hide();
        this.modalizeBSRef.open();
        this.props.navigation.setParams({ isHide: true });
    };

    onClose = () => {
        this.props.navigation.setParams({ isHide: false })
        this.modalizeBSRef.close();
    };

    onPressConfirmationDeleteAccount = () => {
        this.onClose();
        this.onPressLogout()
    }

    _renderBottomSheet = () => {
        return (
            <Modalize
                withOverlay={true}
                withHandle={true}
                handleStyle={{
                    backgroundColor: "transparent",
                }}
                adjustToContentHeight={true}
                closeOnOverlayTap={true}
                panGestureEnabled={true}
                ref={(ref) => {
                    this.modalizeBSRef = ref;
                }}
                modalStyle={{
                    borderTopLeftRadius: 16,
                    borderTopRightRadius: 16,
                    paddingHorizontal: 16,
                }}
                overlayStyle={{
                    backgroundColor: "#000000CC",
                }}
                // onClose={() => onClose()}
                HeaderComponent={<View style={styles.bottomSheetHeader} />}
            >
                {this.state.typeBottomSheet == "BottomSheetDeleteAccountConfirmation" ? (
                    <BottomSheet
                        onPressCancel={() => this.onClose()}
                        onPressOk={() => {
                            this.onPressConfirmationDeleteAccount();
                        }}
                        title="Delete Account Confirmation"
                        firstDesc="Your account will be permanently deleted and the Email that you are currently using cannot be used again when registering a D2D account. Are you sure you want to delete your account?"
                        wordingCancel="CANCEL"
                        wordingOk="CONFIRMATION"
                        type="BottomSheetDeleteAccountConfirmation"
                    />
                ) : null}
            </Modalize>
        )
    }

    onTextChanged = (text) => {
        let afterRemoveEmoji = removeEmojis(text);
        let textTemp = "";
        if (this.state.country_code == "ID") {
            textTemp = validateNpaIDI(afterRemoveEmoji);
        } else {
            textTemp = onlyAlphaNumeric(afterRemoveEmoji);
        }


        if (!_.isEmpty(textTemp)) {
            this.setState({ npaIdi: textTemp });
        } else {
            this.setState({ npaIdi: "" });
        }
    }

    render() {
        let nav = this.props.navigation;
        let translateY = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight)],
            outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
            extrapolate: 'clamp',
        })
        let opacityContent = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 3],
            outputRange: [1, 0],
        });
        let opacityTitle = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 3],
            outputRange: [0, 1],
        });
        const imageOpacity = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight)],
            outputRange: [1, 0],
            extrapolate: 'clamp',
        });

        let imageAvatar = this.state.avatarSource === null ?
            { uri: 'https://s3-ap-southeast-1.amazonaws.com/static.guesehat.com/article/mums_ayo_belajar_memahami_si_kecil_1520934591.jpg' } :
            // null :
            { uri: this.state.avatarSource };

        let heightImage = this.state.avatarSource === null ? 0 : heightHeaderSpan - (heightHeaderSpan / 6);
        let heightEmptyImage = this.state.avatarSource === null ? heightHeaderSpan - (heightHeaderSpan / 6) : 0;

        let topValue = this.state.avatarSource === null ? heightHeaderSpan - (heightHeaderSpan / 2) - 100 :
            heightHeaderSpan - (heightHeaderSpan / 2) - 100;
        let iconName = 'account-circle';
        let iconType = 'MaterialIcons';
        let sizeIcon = this.state.avatarSource === null ? 70 : 60;
        console.log('this.state.avatarSource: ', this.state.avatarSource)
        let styleBtnUploadImage = this.state.avatarSource === null ? styles.btnUploadImage : styles.invisibleBtn;
        let mrgnBottomIcon = this.state.avatarSource === null ? 0 : 20;
        let valueOpacity = this.state.avatarSource === null ? 1 : 0.8;

        console.log(imageAvatar);

        const name = this.state.mProfile != null && this.state.mProfile.doctor.name ? this.state.mProfile.doctor.name : 'noname';

        let tabPosition = 0;
        let { params } = this.props.navigation.state;
        console.log("render: ", params)

        if (params != null && params.type != null) {
            if (params.from != 'pushNotifCertificate') {
                tabPosition = 0; //idk why set position to 1 yet
            }
        }

        return (
            <Fragment>
                <SafeAreaView style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }} />


                <Container style={{ backgroundColor: '#F7F8FA' }}>
                    <DateTimePicker
                        isVisible={this.state.isShowingDatePicker}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDatePicker}
                        maximumDate={moment().toDate()}
                        date={this.state.shownBornDate ? moment(this.state.shownBornDate, 'DD MMM YYYY').toDate() : moment().toDate()}
                    />


                    <Tabs
                        initialPage={tabPosition}
                        ref={c => this.tabs = c}
                        renderTabBar={(props) =>
                            <Animated.View
                                style={{ transform: [{ translateY }], top: heightHeaderSpan, zIndex: 1, width: "100%" }}>
                                <ScrollableTab {...props}
                                    style={{ borderWidth: 0 }}
                                    renderTab={(name, page, active, onPress, onLayout) => (
                                        <TouchableOpacity key={page}
                                            onPress={() => { onPress(page) }}
                                            onLayout={onLayout}
                                            activeOpacity={0.99}>
                                            <Animated.View style={styles.tabHeading}>
                                                <TabHeading
                                                    {...testID('tab_' + name)}
                                                    style={{ width: (platform.deviceWidth / 2), height: platform.toolbarHeight }}
                                                    active={active}>
                                                    <Text style={{
                                                        fontFamily: 'Roboto-Medium',
                                                        //active ? 'Nunito-Bold' : 'Nunito-Regular',
                                                        color: active ? platform.topTabBarActiveTextColor : platform.topTabBarTextColor,
                                                        fontSize: platform.tabFontSize
                                                    }}>
                                                        {name}
                                                    </Text>
                                                </TabHeading>
                                            </Animated.View>
                                        </TouchableOpacity>
                                    )}
                                    underlineStyle={{ backgroundColor: platform.topTabBarActiveBorderColor }} />
                            </Animated.View>
                        }
                        onChangeTab={(tab) => this.onChangeTab(tab)}>

                        <Tab

                            heading="PROFILE">

                            <Animated.ScrollView
                                // automaticallyAdjustContentInsets={false}
                                // keyboardShouldPersistTaps = 'always'
                                contentContainerStyle={{ paddingTop: heightHeaderSpan + 5, paddingHorizontal: 0 }}
                                scrollEventThrottle={1} // <-- Use 1 here to make sure no cmespessialist are ever missed
                                onScroll={
                                    Animated.event(
                                        [{ nativeEvent: { contentOffset: { y: this.state.animatedValue } } }],
                                        { useNativeDriver: true } // <-- Add this
                                    )}
                                ref={(c) => { this.scroll = c }}

                            >
                                <KeyboardAwareScrollView
                                    extraScrollHeight={-150}
                                >

                                    {this.state.country_code != "PH" && (

                                        <View onLayout={this.onLayoutMedicalId}>

                                            <Card noShadow transparent style={[styles.card, { paddingBottom: 0 }]} >
                                                <Label black style={styles.textLabel}  >Medical ID Number</Label>
                                                <Item noShadowElevation style={[styles.itemInput]} error={this.state.npaIdiFailed}
                                                >
                                                    <Input

                                                        newDesign
                                                        placeholder={'Enter your Medical ID Number'}
                                                        value={this.state.npaIdi}
                                                        editable={this.state.country_code == "ID" ? this.state.npaIdiFilled : true}
                                                        placeholderTextColor={platform.placeholderTextColor}
                                                        keyboardType={this.state.country_code == "ID" ? 'number-pad' : platform.platform === "ios" ? "ascii-capable" : "visible-password"}
                                                        maxLength={this.state.country_code == "SG" ? 20 : 50}
                                                        onChangeText={(txt) => this.onTextChanged(txt)}
                                                        {...testID('input_npaidi')}
                                                        ref={(ref) => { this.inputMedicalId = ref }}

                                                    />
                                                </Item>
                                                {this.state.npaIdiFailed && (
                                                    <Text textError style={styles.textError}>{this.state.errorMessageMedicalId}</Text>
                                                )}
                                            </Card>
                                        </View>
                                    )}

                                    <Card noShadow transparent style={styles.card}>
                                        <Label black style={styles.textLabel}>Full Name</Label>
                                        <Item noShadowElevation style={styles.itemInput} error={this.state.fullNameFailed} onLayout={this.onLayoutFullname}
                                        >
                                            <Input
                                                newDesign
                                                autoCapitalize="none"
                                                placeholder={'Enter your Full Name'}
                                                editable={true}
                                                value={this.state.fullName}
                                                placeholderTextColor={platform.placeholderTextColor}
                                                onChangeText={(txt) => this.setState({ fullName: removeEmojis(txt) })}
                                                maxLength={200}
                                                {...testID('input_fullname')}
                                            />
                                        </Item>
                                        {this.state.fullNameFailed && (
                                            <Text textError style={styles.textError}>{this.state.errorMessageFullname}</Text>
                                        )}
                                        {this.state.country_code != "PH" && (
                                            <View>
                                                <Label black style={styles.textLabel}>Email</Label>
                                                <Item noShadowElevation style={styles.itemInput} error={this.state.emailFailed}>
                                                    <Input
                                                        newDesign
                                                        autoCapitalize="none"
                                                        placeholder={'Enter your email'}
                                                        editable={false}
                                                        value={this.state.email}
                                                        placeholderTextColor={platform.placeholderTextColor}
                                                        onChangeText={(txt) => this.setState({ email: txt })}
                                                        {...testID('input_email')}
                                                    />
                                                </Item>
                                                <Label black style={styles.textLabel}>Phone Number</Label>
                                                <Item noShadowElevation style={styles.itemInput} error={this.state.phoneNoFailed}
                                                    onLayout={this.onLayoutPhoneNumber}
                                                >
                                                    <Input
                                                        newDesign
                                                        // {...testID('inputNumberic')}
                                                        accessibilityLabel="input_numeric"
                                                        autoCapitalize="none"
                                                        keyboardType="numeric"
                                                        placeholder={'Enter your phone number'}
                                                        value={this.state.phoneNo}
                                                        placeholderTextColor={platform.placeholderTextColor}
                                                        onChangeText={(txt) => this.onChangeTextPhoneNo(removeEmojis(txt))}
                                                    />
                                                </Item>
                                                {this.state.phoneNoFailed && (
                                                    <Text textError style={styles.textError}>{this.state.errorMessagePhoneNumber}</Text>
                                                )}
                                                <Label black style={styles.textLabel}>Date of Birth</Label>

                                                <Item
                                                    noShadowElevation
                                                    error={this.state.bornDateFailed}
                                                    style={[styles.itemInput, { height: 56, alignItems: "center" }]}
                                                    onPress={this._showDatePicker}
                                                >
                                                    <Icon name='calendar-today' type='MaterialCommunityIcons' style={styles.iconDateOfBirth} />

                                                    <Text
                                                        regular
                                                        style={{
                                                            color: this.state.shownBornDate != null && this.state.shownBornDate != '' ? '#1E1E20' : platform.placeholderTextColor,
                                                        }}>
                                                        {this.state.shownBornDate != null && this.state.shownBornDate != '' ? this.state.shownBornDate : 'Enter your birthday'}
                                                    </Text>
                                                </Item>
                                            </View>
                                        )}


                                        {this._renderCountries()}
                                        {this.state.country_code == 'ID' && (
                                            <View onLayout={this.onLayoutStatusPNS}>
                                                {this.renderHomeLocation()}
                                                {this.renderStatusPNS()}
                                            </View>
                                        )}

                                    </Card>
                                    {this.state.country_code != "PH" && (
                                        <View onLayout={this.onLayoutParentSpecialistDoctorPractice}>
                                            <View onLayout={this.onLayoutSpecialist}>
                                                <Card noShadow transparent style={[styles.card]}>
                                                    <View style={{
                                                        flex: 1,
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                    }}>
                                                        <Text titleInCard style={{ marginBottom: 8 }}>Specialization</Text>
                                                        <TouchableOpacity
                                                            accessibilityLabel="button_logout_testing"
                                                            style={styles.textInvisibleLogout}
                                                            onPress={() => this.onPressLogout()}>
                                                        </TouchableOpacity>
                                                    </View>
                                                    {this._renderSpesialization()}
                                                </Card>
                                            </View>
                                            <View onLayout={this.onLayoutInterest}>
                                                <Card noShadow transparent style={[styles.card]}>
                                                    <Text titleInCard style={{ marginBottom: 8 }}>Interest</Text>

                                                    {this._renderSubscribtions()}
                                                </Card>
                                            </View>

                                            <View
                                                onLayout={this.onLayoutEducation1}
                                            >
                                                <Card noShadow transparent style={[styles.card, { paddingBottom: 0 }]}>
                                                    <Text titleInCard style={{ marginBottom: 16 }}
                                                    >Educational Degree</Text>

                                                    {this.renderEducations()}
                                                </Card>

                                            </View>
                                            <View onLayout={this.onLayoutDoctorPractice1}>
                                                <Card noShadow transparent style={[styles.card, { paddingBottom: 0 }]}>
                                                    <Text titleInCard style={{ marginBottom: 16 }}
                                                    >Doctor Practice</Text>

                                                    <Item noShadowElevation style={styles.itemInput} error={this.state.clinicLocationFailed}>
                                                        <Input
                                                            newDesign
                                                            // {...testID('inputClinic1')}
                                                            accessibilityLabel="input_clinic1"
                                                            autoCapitalize="none"
                                                            placeholder={translate("placeholder_short_practice_location")}
                                                            value={this.state.clinicLocation}
                                                            placeholderTextColor={platform.placeholderTextColor}
                                                            onChangeText={(txt) => this.onChangeTextClinic1(removeEmojis(txt))}
                                                        />
                                                    </Item>
                                                    {this.state.clinicLocationFailed && (
                                                        <Text textError style={styles.textError}>{this.state.errorMessageDoctorPractice1}</Text>
                                                    )}
                                                    <Item noShadowElevation style={styles.itemInput} error={this.state.clinicLocation2Failed}>
                                                        <Input
                                                            newDesign
                                                            // {...testID('inputClinic2')}
                                                            accessibilityLabel="input_clinic2"
                                                            autoCapitalize="none"
                                                            placeholder={translate("placeholder_short_practice_location")}
                                                            value={this.state.clinicLocation2}
                                                            placeholderTextColor={platform.placeholderTextColor}
                                                            onChangeText={(txt) => this.onChangeTextClinic2(removeEmojis(txt))}
                                                        />
                                                    </Item>
                                                    <Item noShadowElevation style={styles.itemInput} error={this.state.clinicLocation3Failed}>
                                                        <Input
                                                            newDesign
                                                            // {...testID('inputClinic3')}
                                                            accessibilityLabel="input_clinic3"
                                                            autoCapitalize="none"
                                                            placeholder={translate("placeholder_short_practice_location")}
                                                            value={this.state.clinicLocation3}
                                                            placeholderTextColor={platform.placeholderTextColor}
                                                            onChangeText={(txt) => this.onChangeTextClinic3(removeEmojis(txt))}
                                                        />
                                                    </Item>
                                                </Card>
                                            </View>
                                        </View>)}
                                </KeyboardAwareScrollView>

                                <Card noShadow transparent style={[styles.card, { marginBottom: 0 }]
                                    //, { borderWidth: 0, borderColor: 'transparent', paddingVertical: 24, paddingHorizontal: 16 }
                                }>

                                    <Button
                                        // {...testID('buttonSave')}
                                        accessibilityLabel="button_save"
                                        style={[styles.btnSave], { paddingTop: 0 }} onPress={() => this.doValidateDataProfile()} block>
                                        <Text textButton>Save</Text>
                                    </Button>
                                </Card>
                            </Animated.ScrollView>

                        </Tab>
                        <Tab

                            heading="NOTIFICATION">
                            <AnimatedFlatList
                                contentContainerStyle={{ paddingTop: heightHeaderSpan + 5, paddingHorizontal: 10, }}
                                scrollEventThrottle={1} // <-- Use 1 here to make sure no cmespessialist are ever missed
                                onScroll={Animated.event(
                                    [{ nativeEvent: { contentOffset: { y: this.state.animatedValue } } }],
                                    { useNativeDriver: true } // <-- Add this
                                )}
                                data={this.state.notifications}
                                onEndReached={this.handleLoadMore}
                                onEndReachedThreshold={0.5}
                                renderItem={this._renderItem}
                                onRefresh={() => this.onRefreshListNotif()}
                                refreshing={this.state.isFetchingNotif}
                                ListEmptyComponent={this._renderEmptyItemNotification}
                                ListFooterComponent={this._renderItemFooter()}
                                keyExtractor={(item, index) => index.toString()} />
                        </Tab>
                    </Tabs>

                    <Animated.View style={[styles.animHeaderSpan, { transform: [{ translateY }] }]}>
                        <Animated.View style={[styles.mainViewCollapsing, { opacity: imageOpacity }]}>
                            <TouchableOpacity
                                // {...testID('buttonAvatar')}
                                accessibilityLabel="button_avatar"
                                onPress={this.selectPhotoTapped.bind(this)}>
                                <Animated.Image style={{ height: heightImage, opacity: imageOpacity, backgroundColor: '#CB1D50' }}
                                    source={imageAvatar}
                                // defaultSource={require('../assets/images/new_icon.png')}
                                />
                                <Animated.View style={{ height: heightEmptyImage, backgroundColor: '#CB1D50' }}
                                />
                            </TouchableOpacity>
                            <View style={[styles.viewInCollapsing, { top: topValue }]}>

                                <TouchableOpacity style={{ flex: 0, justifyContent: 'center', alignItems: 'center' }}
                                    {...testID('upload_profile')}
                                    onPress={this.selectPhotoTapped.bind(this)}>
                                    <Image resizeMode='contain' source={require('./../../assets/images/profile-on-icon.png')} style={{ width: 64, height: 64 }} />
                                    {/* <Icon name={iconName} type={iconType} style={[styles.iconImage, { backgroundColor: '#454F63', fontSize: sizeIcon, opacity: valueOpacity }]} /> */}
                                    <TouchableOpacity
                                        // {...testID('buttonUploadImage')}
                                        accessibilityLabel="button_up_load_image"
                                        style={styles.btnUploadImage} onPress={this.selectPhotoTapped.bind(this)}>
                                        <Text textAsButton >{this.state.avatarSource != null ? 'Change Photo' : 'Upload Photo'}</Text>
                                    </TouchableOpacity>
                                </TouchableOpacity>

                                <Animated.Text
                                    {...testID('nama_user')}
                                    bordered style={[styles.heading, { opacity: opacityContent }]}
                                    numberOfLines={1}>{this.state.fullName}</Animated.Text>
                                <Card style={styles.mainCardCollapsing}>
                                    <CardItem style={[styles.smallCardCollapsing, { borderRightWidth: 1, borderRightColor: '#F4F4F6', }]}>
                                        <TouchableOpacity
                                            {...testID('buttonBookmark')}
                                            accessibilityLabel="button_bookmark"
                                            onPress={() => this.gotoProfileBookmark()}
                                            style={styles.touchSmallCardCollapsing}>
                                            <Text style={styles.textNumber}>{this.state.totalBookmark}</Text>
                                            <Text style={styles.textCollapsingDesc}>BOOKMARK</Text>
                                        </TouchableOpacity>
                                    </CardItem>
                                    <CardItem style={[styles.smallCardCollapsing, { borderLeftWidth: 1, borderLeftColor: '#F4F4F6' }]}>
                                        <TouchableOpacity
                                            // {...testID('buttonDownload')}
                                            accessibilityLabel="button_download"
                                            onPress={() => this.gotoProfileDownload()}
                                            style={styles.touchSmallCardCollapsing}>
                                            <Text style={styles.textNumber}>{this.state.totalDownload}</Text>
                                            <Text style={styles.textCollapsingDesc}>DOWNLOAD</Text>
                                        </TouchableOpacity>
                                    </CardItem>
                                </Card>
                            </View>
                        </Animated.View>
                    </Animated.View>

                    <Animated.View style={styles.animHeader}>
                        <Header noShadow hasTabs style={{ backgroundColor: 'transparent' }}>
                            <Left style={{ flex: 0.5 }}>
                            </Left>
                            <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <AnimatedTitle style={{ opacity: opacityTitle, textAlign: 'center', fontFamily: 'Roboto-Bold' }}>{this.state.fullName}</AnimatedTitle>
                            </Body>
                            <Right style={{ flex: 0.5 }}>
                                <Menu style={{ padding: 0 }}
                                    ref={this.setMenuRef}
                                    button={<Button
                                        // {...testID('buttonMore')}
                                        accessibilityLabel="button_more"
                                        transparent onPress={() => this.menu.show() + this.sendAdjust(from.Profile_3Dots)}>
                                        <Icon name='md-more' nopadding style={styles.iconMore} />
                                    </Button>}>
                                    <MenuItem
                                        // {...testID('buttonTnC')}
                                        accessibilityLabel="button_tnc"
                                        textStyle={styles.textLogout} onPress={() => this.gotoWebView(ParamSetting.TERMS_CONDITIONS) + this.sendAdjust(from.T_AND_C)}>Terms & Conditions</MenuItem>
                                    <MenuItem
                                        // {...testID('buttonPrivacy')}
                                        accessibilityLabel="button_privacy"
                                        textStyle={styles.textLogout} onPress={() => this.gotoWebView(ParamSetting.PRIVACY_POLICY) + this.sendAdjust(from.PrivacyPolicy)}>Privacy Policy</MenuItem>
                                    <MenuItem
                                        // {...testID('buttonContactUs')}
                                        accessibilityLabel="button_contactus"
                                        textStyle={styles.textLogout} onPress={() => this.gotoWebView(ParamSetting.CONTACT_US) + this.sendAdjust(from.ContactUs)}>Contact Us</MenuItem>
                                    {/* {this.state.country_code == "MM" || this.state.country_code == "KH" || this.state.country_code == "SG" ? <MenuItem
                                        // {...testID('buttonLogOut')}
                                        accessibilityLabel="button_delete_account"
                                        textStyle={styles.textLogout} onPress={() => this.openBSDeleteAccountConfirmation()}>Delete Account</MenuItem> : null} */}
                                    <MenuItem
                                        // {...testID('buttonLogOut')}
                                        accessibilityLabel="button_logout"
                                        textStyle={styles.textLogout} onPress={() => this.onPressLogout()}>Logout</MenuItem>
                                </Menu>
                            </Right>
                        </Header>
                    </Animated.View>

                    <Loader visible={this.state.showLoader} />
                    {this.renderBottomSheetWarningChangeCountry()}
                    {this._renderBottomSheet()}
                    <Popup
                        ref={ref => {
                            this.popup = ref;
                        }}

                        onPressButtonPositive={() =>
                            this.logout() + this.sendAdjust(from.Logout)
                        }
                        title={'Do you want to logout?'}
                        message={'Your data might be lost, such as login access and webinar certificates.'}
                        type={"logout"}
                        textButtonNegative={"CANCEL"}
                        textButtonPositive={"LOGOUT"}
                    />

                </Container >
            </Fragment >

        )
    }
}


const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const AnimatedTitle = Animated.createAnimatedComponent(Title);
const heightHeaderSpan = 320;//250
const styles = StyleSheet.create({
    headerBody: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: platform.titleFontColor,
        fontFamily: platform.titleFontfamily,
        fontSize: platform.titleFontSize,
    },
    animHeader: {
        position: "absolute",
        width: "100%",
        zIndex: 2,
    },
    animHeaderSpan: {
        position: 'absolute',
        backgroundColor: platform.toolbarDefaultBg,
        height: heightHeaderSpan,
        left: 0,
        right: 0,
        zIndex: 1,
    },
    animHeaderImageSpan: {
        height: heightHeaderSpan,
        left: 0,
        right: 0,
        zIndex: 1,
        paddingHorizontal: 20,
        paddingVertical: 10,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },
    emptyItem: {
        justifyContent: 'center',
        alignItems: 'center',
        height: (platform.deviceHeight / 2) - 80
    },
    tabHeading: {
        flex: 1,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        padding: 10,
    },
    card: {
        // flex: 0, 
        paddingHorizontal: 16,
        borderColor: 'transparent',
        marginBottom: 8,
        paddingVertical: 24,
        marginTop: 0,
        // marginBottom: platform.platform == 'ios' ? platform.deviceHeight / 4 : 10,
    },
    form: {
        paddingHorizontal: 20,
    },
    viewInCollapsing: {
        position: 'absolute',
        // top : heightHeaderSpan - (heightHeaderSpan/2) - 30,
        left: 0,
        right: 0,
        paddingHorizontal: 10,
    },
    textNumber: {
        flex: 1,
        fontFamily: 'Roboto-Medium',
        color: '#1E1E20',
        fontSize: 20,

    },
    textCollapsingDesc: {
        // flex: 1,
        // justifyContent: 'flex-end',
        fontFamily: 'Roboto-Medium',
        color: '#1E1E20',
    },
    textLabel: {
        marginBottom: 12
    },
    iconMore: {
        color: '#D0D8E6',
        fontSize: 24
    },
    textLogout: {
        fontFamily: 'Nunito-Regular',
        fontSize: 13
    },
    mainViewCollapsing: {
        flex: 1,
        // height: heightHeaderSpan - platform.toolbarHeight,
        backgroundColor: '#fff'
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        fontFamily: 'Roboto-Bold',
        flexWrap: "wrap",
        textAlign: 'center',
    },
    mainCardCollapsing: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
        marginHorizontal: 20,
        borderRadius: 16
        //  borderRadius: 16
        // marginVertical:10
    },
    smallCardCollapsing: {
        flex: 1,
        height: 70,
        paddingTop: 0,
        paddingBottom: 0,
        justifyContent: 'center',
    },
    touchSmallCardCollapsing: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginVertical: 12
    },
    btnSave: {
        marginTop: 30,
        marginBottom: 10,
        borderRadius: 10,
    },
    itemViewNotif: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffff',
        borderRadius: 4
    },
    itemViewDots: {
        flex: 0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconDot: {
        fontSize: 30,
        textAlign: 'center'
    },
    itemViewText: {
        flex: 0.85,
        paddingVertical: 15,
        marginRight: 5,
        justifyContent: 'center',
        alignItems: 'flex-start',
        borderBottomWidth: 1,
        borderBottomColor: '#F4F4F6'
    },
    textItemTime: {
        color: '#959DAD',
        fontSize: 12,
    },
    textItemDesc: {
        fontFamily: 'Nunito-SemiBold',
        color: '#454F63',
        fontSize: 16
    },
    backgroundCompetence: {
        padding: 5,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginRight: 10,
        backgroundColor: '#F0F0F0',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundBtnAdd: {
        padding: 5,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginRight: 10,
        borderRadius: 5,
    },
    textGrey: {
        fontSize: 14,
        color: '#6C6C6C'
    },
    textSpecialist: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 10,
        flexWrap: 'wrap'
    },
    tagIconX: {
        fontSize: 24,
        marginLeft: 10,
    },
    iconImage: {
        color: '#FFFFFF',
        // fontSize: 45,
        justifyContent: 'center',
        // opacity : 0.8,
        alignItems: 'center',
        flexWrap: "wrap",
        textAlign: 'center',
    },
    btnUploadImage: {
        marginVertical: 16,
        borderRadius: 8,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    invisibleBtn: {
        height: 0,
    },
    textWhite: {
        color: '#fff',
        flexWrap: "wrap",
        textAlign: 'center',
        fontSize: 12,
    },

    mainTouchSp: {
        flex: 1,
        marginTop: 16,
        borderColor: '#F6F6F7',
        borderRadius: 8,
    },
    viewTopSp: {
        height: 56,
        paddingHorizontal: 16,
        paddingVertical: 10,
        borderWidth: 0,
        // borderColor: '#F0F0F0',
        backgroundColor: '#F6F6F7',
        flexDirection: 'row-reverse',
        borderRadius: 8,
        alignItems: 'center'
    },
    viewBottomSp: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: 'transparent',
        flexDirection: 'row-reverse',
    },
    textAdd: {
        textAlign: 'center',
        marginTop: 12,
        lineHeight: 48
    },
    touchRadio: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textPns: {
        fontFamily: 'Nunito-Bold',
        color: '#454F63',
        fontSize: 16
    },
    textInvisibleLogout: {
        fontFamily: 'Nunito-Bold',
        backgroundColor: 'white',
        alignSelf: 'center',
        height: 2,
        width: 2,
        margin: 10
    },
    itemInput: {
        marginBottom: 24
    },

    iconDateOfBirth: {
        color: '#454F63',
        fontSize: 24,
        marginTop: 5,
        marginBottom: platform.platform == 'ios' ? 0 : 3,
        paddingVertical: 0,
        paddingRight: 12,
    },
    textError: {
        marginTop: -16,
        marginBottom: 16
    },
    bottomSheetHeader: {
        height: 4,
        borderRadius: 2,
        width: 40,
        marginTop: 8,
        backgroundColor: "#454F6329",
        alignSelf: "center",
    },
});
