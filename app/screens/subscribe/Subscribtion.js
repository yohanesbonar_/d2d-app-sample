import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, Image, TouchableOpacity, View, FlatList, Modal, ImageBackground } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Content, Toast, Card, CardItem } from 'native-base';
import { Checklist, Loader } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { NavigationActions, StackActions } from 'react-navigation';
import { STORAGE_TABLE_NAME } from './../../libs/Common';
// import Api, {errorMessage} from './../../libs/Api';
import { getCompetence, doSendCompetence } from './../../libs/NetworkUtility';
import { SafeAreaView } from 'react-native';

export default class Subscribtion extends Component {

    SPESIALIZATION = 'spesialization';
    SUBSCRIPTION = 'subscription';

    subscribtionList = [];
    selectedSpesialist = null;
    state = {
        subscribtionList: [],
        showLoader: true,
        from: null,
        type: this.SUBSCRIPTION,
    }
    checkedSubscribtion = [];

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);

        let { params } = this.props.navigation.state

        if (params != null) {
            this.setSelectedList(params)
        } else {
            this.getLearningList();
        }
    }

    setSelectedList(params) {
        if (params.selectedSpecialist != null) {
            this.selectedSpesialist = params.selectedSpecialist;
        }
        this.setState({ from: params.from, type: params.type })
        this.getLearningList();
    }

    async getLearningList() {
        this.setState({ showLoader: true })

        try {
            let response = await getCompetence();

            if (response.isSuccess == true) {
                this.doSetSubscribtionList(response.data);
            } else {
                this.setState({ showLoader: false });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        } catch (error) {
            this.setState({ showLoader: false });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }

    doSetSubscribtionList(result) {
        for (let key in result) {
            let obj = result[key];
            let dataChecklist = null;
            let isSelected = false;

            if (this.selectedSpesialist != null && this.selectedSpesialist.length > 0) {

                this.selectedSpesialist.map(function (d, i) {
                    if (obj.id == d.id) {
                        isSelected = true;
                    }
                });

            }

            if (isSelected === true) {
                this.checkedSubscribtion.push(obj);
                dataChecklist = {
                    id: obj.id,
                    title: obj.title,
                    description: obj.meta_title,
                    isChecked: true,
                };
            } else {
                dataChecklist = {
                    id: obj.id,
                    title: obj.title,
                    description: obj.meta_title,
                    isChecked: false,
                };
            }

            this.subscribtionList.push(dataChecklist);

        }

        this.setState({ showLoader: false, subscribtionList: this.subscribtionList });
    }


    onCheckedListener = (data, check) => {
        if (check === true) {
            this.checkedSubscribtion.push(data);
        } else {
            if (this.checkedSubscribtion != null && this.checkedSubscribtion.length > 0) {
                mIndex = null;
                this.checkedSubscribtion.map(function (idList, index) {
                    if (idList.id === data.id) {
                        mIndex = index;
                    }
                })

                if (mIndex != null) {
                    this.checkedSubscribtion.splice(mIndex, 1)
                }
            }
        }
    }


    _renderItem = ({ item }) => {
        return (
            <Checklist data={item} action={this.onCheckedListener} />
        )
    }

    doSubscribe = async () => {
        if (this.checkedSubscribtion == null || this.checkedSubscribtion.length == 0) {
            Toast.show({ text: 'Select at least one topic', position: 'top', duration: 3000 });
            return;
        } else {
            if (this.state.from == 'PROFILE' || this.state.from == 'REQUEST_JOURNAL') {
                this.props.navigation.state.params.updateData(this.checkedSubscribtion);
                this.props.navigation.goBack();

            } else {
                this.setState({ showLoader: true });

                let type = this.state.type;
                let response = await doSendCompetence(this.checkedSubscribtion, type);

                this.setState({ showLoader: false });

                if (response.isSuccess == true) {
                    if (this.state.from == 'REQUEST_JOURNAL') {
                        this.doSaveProfileSpecialization();
                    } else {
                        this.doSaveProfileSubscribtion();
                    }
                } else {
                    Toast.show({ text: response.message, position: 'top', duration: 3000 })
                }


            }
        }

    }

    async doSaveProfileSubscribtion() {
        try {
            let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
            let dataProfile = JSON.parse(getJsonProfile);
            dataProfile.subscription = this.checkedSubscribtion;

            console.log(dataProfile);

            let jsonProfile = JSON.stringify(dataProfile);
            let saveProfile = await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, jsonProfile);

            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Home' })]
                })
            );
        } catch (error) {
            Toast.show({ text: error, position: 'top', duration: 3000 })
        }

    }

    async doSaveProfileSpecialization() {
        try {
            let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
            let dataProfile = JSON.parse(getJsonProfile);
            dataProfile.spesialization = this.checkedSubscribtion;

            console.log(dataProfile)

            let jsonProfile = JSON.stringify(dataProfile);
            let saveProfile = await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, jsonProfile);

            this.props.navigation.goBack();
            this.props.navigation.state.params.updateData(this.checkedSubscribtion);
        } catch (error) {
            alert(error);
        }

    }

    render() {
        let title = 'SUBSCRIPTION';
        let subTitle = 'Select Topics';
        let subDesc = 'Please select at least one topic you are interested in and will appear automatically following your interests and preferences';
        if (this.state.type == this.SPESIALIZATION) {
            title = 'SPECIALIZATION';
            subTitle = 'Select Specializations';
            subDesc = 'Please select at least one specialist';
        }

        return (
            <Container>
                {/* {this._modalResult()} */}
                <Loader visible={this.state.showLoader} />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow hasTabs>
                        <Body style={styles.bodyTitle}>
                            <Title>{title}</Title>
                        </Body>
                    </Header>
                </SafeAreaView>
                <Content>
                    <ImageBackground source={require('./../../assets/images/bg-blank.png')}
                        style={styles.imageBackground}>
                    </ImageBackground>

                    <View style={styles.mainView}>
                        <Card>
                            <CardItem>
                                <View style={styles.viewCard}>
                                    <Text style={styles.textTitleCard}>{subTitle}</Text>
                                    <Text style={styles.textDescriptionCard}>{subDesc}</Text>
                                    <FlatList
                                        contentContainerStyle={styles.flatList}
                                        scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                                        data={this.state.subscribtionList}
                                        renderItem={this._renderItem}
                                        keyExtractor={(item, index) => index.toString()} />
                                </View>

                            </CardItem>
                        </Card>
                    </View>
                </Content>
                <View style={{ flex: 1, flexDirection: 'row', width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#fff', padding: 10 }}>
                    <Button style={styles.btnLogin} onPress={() => this.doSubscribe()} success block>
                        <Text style={{ fontFamily: 'Nunito-Bold' }}>SUBMIT</Text>
                    </Button>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    bodyTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBackground: {
        width: platform.deviceWidth,
        height: platform.deviceHeight / 1.5
    },
    mainView: {
        marginTop: -platform.deviceHeight / 1.5,
        padding: 10,
        marginBottom: 50,
    },
    viewCard: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'flex-start',
        flexDirection: 'column',
        padding: 10
    },
    textTitleCard: {
        flex: 1,
        fontFamily: 'Nunito-Bold',
        fontSize: 24,
        color: '#454F63'
    },
    textDescriptionCard: {
        flex: 1,
        fontSize: 14,
        color: '#454F63',
        marginTop: 10,
    },
    flatList: {
        marginTop: 10,
    },

    btnOK: {
        fontFamily: 'Nunito-Black',
        fontSize: 16
    },

    btnLogin: {
        borderRadius: 10,
        height: 50,
        flex: 1
    },


})