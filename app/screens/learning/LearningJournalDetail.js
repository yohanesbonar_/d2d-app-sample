import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Modal, Image, TouchableOpacity, BackHandler } from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import {
    Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right,
    Grid, Row, Col, Form, Item, Input, Label, Toast, Thumbnail, Card, CardItem, Footer
} from 'native-base';
import { Loader } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { convertMonth, escapeHtml, testID, customCss } from './../../libs/Common';
import { ParamAction, ParamContent, doActionUserActivity } from './../../libs/NetworkUtility';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { AdjustTrackerConfig, AdjustTracker } from '../../libs/AdjustTracker';
//import { WebView } from 'react-native-webview';
import ViewMoreText from 'react-native-view-more-text';
import AutoHeightWebView from 'react-native-autoheight-webview'
import { SafeAreaView } from 'react-native';

let from = {
    BOOKMARK: 'BOOKMARK',
    VIEW_PDF: 'VIEW_PDF',
    BACK_BUTTON: 'BACK_BUTTON',
    SEE_MORE: 'SEE_MORE',
    SEE_LESS: 'SEE_LESS'
}

const webViewScript = `
  setTimeout(function() { 
    window.postMessage(document.documentElement.scrollHeight); 
  }, 500);
  true; // note: this is required, or you'll sometimes get silent failures
`;

export default class LearningJournalDetail extends Component {

    fromDeeplink = false;


    constructor(props) {
        super(props);

        this.state = {
            desc: '',
            webheight: 110,
            showLoader: false,
            bookmark: 0,
            readmore: true,
            key: 0,
            from: this.props.navigation.state.params ? this.props.navigation.state.params.from : '',
            isRefreshPageSearchKonten: false,
        }
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        const nav = this.props.navigation;
        const { params } = nav.state;
        if (params.data != null) {
            this.setState({ bookmark: params.data.flag_bookmark, desc: this.shortenDesc(params.data.long_description, 300) })
            this.doViewJournalDetail(params.data.id);

            if (params.fromDeeplink != null) {
                this.fromDeeplink = true;
            }

        }
        lor(this);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed()
            return true;
        });
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.BOOKMARK:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Journal_Bookmark
                else if (this.state.from == 'LearningSpecialist')
                    token = AdjustTrackerConfig.Learning_SpesialisOpen_Bookmark
                break;
            case from.VIEW_PDF:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Journal_ViewPDF
                else if (this.state.from == 'LearningSpecialist')
                    token = AdjustTrackerConfig.Learning_SpesialisOpen_ViewPDF
                break;
            case from.BACK_BUTTON:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Journal_BackButton
                break;
            case from.SEE_MORE:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Journal_SeeMore
                break;
            case from.SEE_LESS:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Journal_SeeLess
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }

    componentWillUnmount() {
        rol();
        this.backHandler.remove()
    }

    async doViewJournalDetail(journalId) {
        let viewJournal = await doActionUserActivity(ParamAction.VIEW, ParamContent.PDF_JOURNAL, journalId);
    }

    setMenuRef = (ref) => {
        this.menu = ref;
    };

    setBookmarkFrom = () => {
        if (this.props.navigation.state.params.from == "Home" || this.props.navigation.state.params.from == "SearchKonten" || this.props.navigation.state.params.from == "SearchAllByKonten") {
            this.setState({ isRefreshPageSearchKonten: true });
        }
    };

    toggleBookmark(journalId) {
        this.menu.hide();
        let actionBookmark = this.state.bookmark ? ParamAction.UNBOOKMARK : ParamAction.BOOKMARK;
        this.doBookmarkPdfJournal(actionBookmark, journalId)
    }

    async doBookmarkPdfJournal(actionBookmark, journalId) {
        let response = await doActionUserActivity(actionBookmark, ParamContent.PDF_JOURNAL, journalId);
        if (response.isSuccess == true) {
            this.setState({ bookmark: !this.state.bookmark });
            let act = this.state.bookmark == true ? 'bookmark' : 'unbookmark';
            // Toast.show({ text: 'Succeded ' + act, position: 'top', duration: 3000})
            Toast.show({ text: act + ' succesfull', position: 'top', duration: 3000 })
        } else {
            Toast.show({ text: response.message, position: 'top', duration: 3000 })
        }
    }

    onBackPressed() {
        console.log("onBackPressed from: ", this.state.from)
        this.sendAdjust(from.BACK_BUTTON)
        this.props.navigation.goBack();
        if (this.fromDeeplink == false) {
            this.props.navigation.state.params.updateDataBookmark(this.state.bookmark);
        }
        if (this.state.isRefreshPageSearchKonten == true) {
            this.props.navigation.state.params.onRefreshDataSearchKonten();
        }
    }

    _renderSeeMore(data) {
        if (data.length > 350) {
            return (
                <Col style={{ marginLeft: 10 }}>
                    <TouchableOpacity
                        // {...testID('buttonSeeMore')}
                        accessibilityLabel="button_see_more"
                        onPress={() => this.setState({ readmore: !this.state.readmore, key: this.state.key + 1 })} style={{ justifyContent: 'flex-start', alignItems: 'flex-start', paddingVertical: 10 }}>
                        <Text style={styles.readmoreText}>
                            {this.state.readmore == true ? 'see more...' : 'less'}
                        </Text>
                    </TouchableOpacity>
                </Col>
            )
        }

        return null;
    }

    gotoPdfView = (params) => {
        if (params.data == null || params.data.attachment == null) {
            Toast.show({ text: 'File not found', position: 'top', duration: 3000 })
        } else {
            let tempData = {
                from: this.state.from,
                ...params.data
            }
            const viewPdf = { type: "Navigate", routeName: "PdfView", params: tempData }
            this.props.navigation.navigate(viewPdf)
        }
        this.sendAdjust(from.VIEW_PDF)
    }

    shortenDesc(text, max) {
        return text && text.replace(/<[^>]*>/g, '').length > max ? text.slice(0, max).split(' ').slice(0, -1).join(' ') : text
    }

    pressShowMoreLess = (params) => {
        if (this.state.readmore == true) {
            this.sendAdjust(from.SEE_MORE)
        }
        else {
            this.sendAdjust(from.SEE_LESS)
        }

        if (this.state.readmore) {
            this.setState({ desc: this.shortenDesc(params.data.long_description, params.data.long_description.length) })
        }
        else {
            this.setState({ desc: this.shortenDesc(params.data.long_description, 300) })
        }
        this.setState({ readmore: !this.state.readmore })
    }

    render() {
        const nav = this.props.navigation;
        const { params } = nav.state;
        const requestJournal = { type: "Navigate", routeName: "LearningRequestJournal", params: params.data }
        const gotoPdfView = { type: "Navigate", routeName: "PdfView", params: params.data }
        const scalesPageToFit = Platform.OS === 'android';

        params.data.long_description = params.data.long_description == null ? '' : params.data.long_description;
        let isFree = true;

        // if (params.data.attachment == null || params.data.attachment == 'https://d2doss.oss-ap-southeast-5.aliyuncs.com/pdf/journal/') {
        //     isFree  = false;
        // }
        console.log(params.data.paid)
        if (params.data.paid != null && params.data.paid == 'Y') {
            isFree = false;
        } else if (params.data.paid != null && params.data.paid == 1) {
            isFree = false;
        }

        console.log(isFree);

        return (
            <Container>
                <View style={{ width: wp('200%'), height: wp('200%'), borderRadius: wp('100%'), backgroundColor: platform.toolbarDefaultBg, position: 'absolute', left: -wp('50%'), top: -wp('100%') }}></View>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >

                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Journal</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }}>
                            <Menu style={{ padding: 0 }}
                                ref={this.setMenuRef}
                                button={<Button
                                    // {...testID('buttonMore')}
                                    accessibilityLabel="button_more"
                                    onPress={() => this.menu.show()} transparent>
                                    <Icon name='md-more' nopadding style={styles.menus} />
                                </Button>}>
                                <MenuItem
                                    // {...testID('buttonToogleBookmark')}
                                    accessibilityLabel="button_toogle_bookmark"
                                    textStyle={{ fontFamily: 'Nunito-Regular', fontSize: 13 }} onPress={() => this.toggleBookmark(params.data.id) + this.setBookmarkFrom() + this.sendAdjust(from.BOOKMARK)}>{this.state.bookmark ? 'Unbookmark' : 'Bookmark'}</MenuItem>
                            </Menu>
                        </Right>
                    </Header>
                </SafeAreaView>
                <Content>

                    <View style={styles.mainView}>

                        <Card style={{ flex: 0 }}>
                            <CardItem>
                                <View style={styles.mainCardItem}>

                                    <Text
                                        {...testID('judul_journal')}
                                        style={styles.textTitle}> {params.data.title} </Text>
                                    <Col>

                                        <Text style={styles.textTitleAbstract}>Abstract</Text>
                                        <AutoHeightWebView
                                            style={{ width: wp('100%') - 60 }}
                                            source={{ html: escapeHtml(this.state.desc, '') }}
                                            bounces={false}
                                            zoomable={false}
                                            scrollEnabled={false}
                                            customStyle={customCss}
                                            scalesPageToFit={false}
                                        />

                                        <TouchableOpacity onPress={() => this.pressShowMoreLess(params)}>
                                            <Text style={styles.readmoreText}>
                                                {this.state.readmore == true ? 'see more...' : 'less'}
                                            </Text>
                                        </TouchableOpacity>

                                    </Col>
                                    <Row style={{ marginTop: 10, paddingHorizontal: 5, paddingVertical: 10, backgroundColor: '#F0F0F1' }}>
                                        <Col size={35}>
                                            <Text size={35} style={styles.textAlignLeft}>Author</Text>
                                        </Col>
                                        <Col size={65}>
                                            <Text size={65} style={styles.textAlignRight}>{params.data.author}</Text>
                                        </Col>
                                    </Row>
                                    <Row style={{ paddingHorizontal: 5, paddingVertical: 10, backgroundColor: '#FFFFFF' }}>
                                        <Col size={35}>
                                            <Text size={35} style={styles.textAlignLeft}>Year published</Text>
                                        </Col>
                                        <Col size={65}>
                                            <Text size={65} style={styles.textAlignRight}>{params.data.release_year}</Text>
                                        </Col>
                                    </Row>
                                    <Row style={{ paddingHorizontal: 5, paddingVertical: 10, backgroundColor: '#F0F0F1' }}>
                                        <Col size={35}>
                                            <Text size={35} style={styles.textAlignLeft}>Journal</Text>
                                        </Col>
                                        <Col size={65}>
                                            <Text size={65} style={styles.textAlignRight} numberOfLines={1}>{params.data.book_title}</Text>
                                        </Col>
                                    </Row>

                                </View>
                            </CardItem>

                        </Card>
                    </View>
                </Content>

                <Button
                    // {...testID('buttonDownloadPdf')}
                    accessibilityLabel="button_download_pdf"
                    block success style={{ borderRadius: 0, height: 55 }} onPress={() => isFree == true ? this.gotoPdfView(params) : this.props.navigation.navigate(requestJournal)} >
                    <Text nopadding style={styles.textWhite}>{isFree == true ? 'VIEW PDF' : 'DOWNLOAD PDF FULL VERSION'}</Text>
                </Button>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    toolbarIcon: {
        color: '#FFFFFF',
        fontSize: 25,
    },
    imageBackground: {
        width: platform.deviceWidth,
        height: platform.deviceHeight / 1.5
    },
    mainView: {
        // marginTop: -platform.deviceHeight/1.5,
        padding: 10
    },
    mainCardItem: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textDate: {
        fontSize: 12,
        fontFamily: 'Nunito-Regular',
        marginTop: 5,
        color: '#454F63',
    },
    menus: {
        color: '#D0D8E6',
        fontSize: 24
    },
    textTitle: {
        color: '#454F63',
        fontSize: 20,
        fontFamily: 'Nunito-Bold',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 5
    },
    textTitleAbstract: {
        color: '#454F63',
        fontSize: 16,
        fontFamily: 'Nunito-Bold',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 15
    },
    textDesc: {
        color: '#78849E',
        fontSize: 14,
        fontFamily: 'Nunito-Regular',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'justify',
        marginTop: 10
    },
    textAlignLeft: {
        fontSize: 14,
        fontFamily: 'Nunito-Regular',
        textAlign: 'left',
        color: '#78849E'
    },
    textAlignRight: {
        fontSize: 14,
        fontFamily: 'Nunito-Regular',
        textAlign: 'right',
        color: '#78849E'
    },
    textAlignRightBlue: {
        fontSize: 14,
        fontFamily: 'Nunito-Regular',
        textAlign: 'right',
        color: '#1D67CB'
    },
    buttonDownloadPdf: {
        backgroundColor: '#3AE194',
        flex: 1,
        justifyContent: 'center'
    },
    textWhite: {
        fontSize: 14,
        fontFamily: 'Nunito-Black',
        textAlign: 'center',
        color: '#FFFFFF'
    },
    bodyTitle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    readmoreText: {
        color: '#448AFF',
        textAlign: 'left',
        marginTop: 0
    },
});
