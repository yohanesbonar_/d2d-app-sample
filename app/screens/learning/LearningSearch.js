import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Toast,
  Item,
  Input,
} from "native-base";
import { Loader, ListItemLearning } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
// import Api, {errorMessage} from './../libs/Api';
import { getDataSpesializations } from "./../../libs/NetworkUtility";
import { testID } from "./../../libs/Common";
import { SafeAreaView } from "react-native";

export default class LearningSearch extends Component {
  page = 1;
  timer = null;
  countCompetence = [];

  state = {
    cmelist: [],
    searchValue: "",
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    isEmptyData: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    // this.getCMEList(this.page);
    this.getDataCompetence();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async getDataCompetence() {
    this.getSpecilizationSubscription(this.page);
  }

  async getSpecilizationSubscription(page) {
    if (this.state.searchValue == "") {
      this.setState({ isFetching: false, isSuccess: true });
      return;
    }

    this.setState({ isFailed: false, isFetching: true });

    let params = {
      type: "subscription",
      page: page,
      limit: 10,
      keyword: this.state.searchValue,
      nesting: true,
    };

    try {
      let response = await getDataSpesializations(params);

      if (response.isSuccess == true) {
        this.setState({
          cmelist:
            response.docs != null && response.docs.length > 0
              ? [...this.state.cmelist, ...response.docs]
              : [],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isEmptyData:
            response.docs != null && response.docs.size > 0 ? false : true,
        });
      } else {
        this.setState({ isFetching: false, isSuccess: true, isFailed: false });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMore = () => {
    if (this.state.isEmptyData == true) {
      return;
    }
    this.page = this.state.isFailed == true ? this.page : this.page + 1;
    this.getSpecilizationSubscription(this.page);
  };

  _renderItem = ({ item }) => {
    return <ListItemLearning navigation={this.props.navigation} data={item} />;
  };

  _renderItemFooter = () => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader()}
    </View>
  );

  _renderItemFooterLoader() {
    if (this.state.isFailed == true && this.page > 1) {
      return (
        <TouchableOpacity onPress={() => this.handleLoadMore()}>
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = () => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
  );

  _renderEmptyItemLoader() {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.handleLoadMore()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (this.state.isFetching == false && this.state.isSuccess == true) {
      let messageNotFound = "We are really sad we could not find anything";
      if (this.state.searchValue != "") {
        messageNotFound += " for " + this.state.searchValue;
      }
      return (
        <View style={styles.viewNotFound}>
          <Icon name="ios-search" style={styles.iconNotFound} />
          <Text style={styles.textNotFound}>{messageNotFound}</Text>
        </View>
      );
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  doSearch = (inputText) => {
    clearTimeout(this.timer);
    this.setState({ cmelist: [], searchValue: inputText, isEmptyData: false });
    if (inputText.length >= 3) {
      this.page = 1;
      this.timer = setTimeout(() => {
        this.getSpecilizationSubscription(this.page);
      }, 1000);
    }
  };

  render() {
    const nav = this.props.navigation;
    return (
      <Container>
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header noShadow searchBar rounded>
            <Item nopadding>
              <Icon name="ios-search" />
              <Input
                // {...testID('inputSearch')}
                accessibilityLabel="input_search"
                placeholder="Search specialization"
                onChangeText={(text) => this.doSearch(text)}
                value={this.state.searchValue}
                style={styles.searchField}
              />
              <Icon
                // {...testID('buttonSearch')}
                accessibilityLabel="button_search"
                name="ios-close-circle"
                onPress={() => this.doSearch("")}
              />
            </Item>
            <TouchableOpacity
              {...testID("button_close")}
              accessibilityLabel="button_close"
              transparent
              style={{ marginRight: 5, marginLeft: 10, alignSelf: "center" }}
              onPress={() => this.onBackPressed()}
            >
              <Icon
                name="md-close"
                type="Ionicons"
                style={[styles.iconClose]}
              />
            </TouchableOpacity>
            {/* <Button 
                    // {...testID('buttonClose')}
                    accessibilityLabel="button_close"
                    transparent style={{ marginRight: 5 }} onPress={() => nav.goBack()}>
                        <Icon name='ios-close' style={{ fontSize: 28 }} />
                    </Button> */}
          </Header>
        </SafeAreaView>
        <FlatList
          contentContainerStyle={{
            paddingTop: 5,
            paddingHorizontal: 10,
            paddingBottom: 5,
          }}
          scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
          data={this.state.cmelist}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.5}
          renderItem={this._renderItem}
          ListEmptyComponent={this._renderEmptyItem}
          ListFooterComponent={this._renderItemFooter}
          keyExtractor={(item, index) => index.toString()}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  searchField: {
    fontSize: 14,
    lineHeight: 15,
  },
  viewNotFound: {
    marginVertical: 20,
    marginHorizontal: 10,
  },
  iconNotFound: {
    fontSize: 80,
    textAlign: "center",
  },
  textNotFound: {
    textAlign: "center",
    fontSize: 20,
  },
  iconClose: {
    fontSize: 28,
    color: "#fff",
  },
});
