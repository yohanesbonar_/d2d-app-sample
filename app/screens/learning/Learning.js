import React, { Component, Fragment } from "react";
import {
  StatusBar,
  StyleSheet,
  Animated,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Toast,
} from "native-base";
import { Loader, ListItemLearning } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
// import Api, {errorMessage} from './../libs/Api';
import { getDataSpesializations } from "./../../libs/NetworkUtility";
import { testID } from "./../../libs/Common";
import { AdjustTrackerConfig, AdjustTracker } from "../../libs/AdjustTracker";
import { SafeAreaView } from "react-native";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";
import { ArrowBackButton } from "../../../src/components/atoms";

export default class Learning extends Component {
  page = 1;
  countCompetence = [];

  state = {
    animatedValue: new Animated.Value(0),
    cmelist: [],
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    hideFooter: true,
    isEmptyData: false,
    backButtonAvailable: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    this.getDataCompetence(this.page);
    // this.getLearningList(this.page);
    this.getDataCountryCode();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.setState({ backButtonAvailable: true });
      } else {
        this.setState({ backButtonAvailable: false });
      }
    });
  };

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async getDataCompetence(page) {
    this.getLearningList(page);
  }

  async getLearningList(page) {
    this.setState({ isFailed: false, isFetching: true, hideFooter: false });

    let params = {
      type: "subscription",
      page: page,
      limit: 10,
      keyword: "",
      nesting: true,
    };
    try {
      let response = await getDataSpesializations(params);

      if (response.isSuccess == true) {
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          hideFooter: true,
          cmelist:
            response.docs != null && response.docs.length > 0
              ? [...this.state.cmelist, ...response.docs]
              : this.state.cmelist,
          isEmptyData:
            response.docs != null && response.docs.length > 0 ? false : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: true,
          hideFooter: true,
        });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        hideFooter: false,
      });
      Toast.show({
        text: "Something went wrong! ",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMore = () => {
    if (this.state.isEmptyData == true) {
      return;
    }
    this.page = this.state.isFailed == true ? this.page : this.page + 1;
    this.getLearningList(this.page);
  };

  _renderItem = ({ item }) => {
    return <ListItemLearning navigation={this.props.navigation} data={item} />;
  };

  _renderItemFooter = () => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader()}
    </View>
  );

  _renderItemFooterLoader() {
    if (this.state.isFailed == true && this.page > 1) {
      return (
        <TouchableOpacity
          // {...testID('buttonHandleLoadMore')}
          accessibilityLabel="button_handle_load_more"
          onPress={() => this.handleLoadMore()}
        >
          <Icon type="EvilIcons" name="search" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = () => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
  );

  _renderEmptyItemLoader() {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginTop: 20,
          }}
          onPress={() => this.handleLoadMore()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (this.state.isFetching == false && this.state.isSuccess == true) {
      if (this.state.cmelist.length == 0) {
        return <Text style={{ textAlign: "center" }}>Data not found</Text>;
      }

      return null;
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  render() {
    const nav = this.props.navigation;
    let translateY = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
      extrapolate: "clamp",
    });
    let opacityContent = this.state.animatedValue.interpolate({
      inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 2],
      outputRange: [1, 0],
    });

    return (
      <Fragment>
        <SafeAreaView
          style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }}
        />
        <Container>
          <AnimatedFlatList
            contentContainerStyle={{
              paddingTop: heightHeaderSpan + 5,
              paddingHorizontal: 10,
              paddingBottom: 5,
            }}
            scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: { y: this.state.animatedValue },
                  },
                },
              ],
              { useNativeDriver: true } // <-- Add this
            )}
            data={this.state.cmelist}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            renderItem={this._renderItem}
            ListEmptyComponent={this._renderEmptyItem}
            ListFooterComponent={this._renderItemFooter}
            keyExtractor={(item, index) => index.toString()}
          />

          <Animated.View
            style={[styles.animHeaderSpan, { transform: [{ translateY }] }]}
          >
            <Animated.Text style={{ color: "#fff", opacity: opacityContent }}>
              Share & get updates on new clinic cases and health journal info
            </Animated.Text>
            <Animated.Text
              style={[styles.heading, { opacity: opacityContent }]}
            >
              Choose Specialization
            </Animated.Text>
          </Animated.View>

          <Animated.View style={styles.animHeader}>
            <Header noShadow hasTabs>
              <Body>
                {this.state.backButtonAvailable == true && (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <View style={{ marginLeft: -6 }}>
                      <ArrowBackButton onPress={() => this.onBackPressed()} />
                    </View>
                    <Title
                      style={styles.textTitle(this.state.backButtonAvailable)}
                    >
                      Literatur
                    </Title>
                  </View>
                )}
                {this.state.backButtonAvailable == false && (
                  <Title style={{ fontSize: 30 }}>Learning</Title>
                )}
              </Body>
              <Right>
                <Button
                  // {...testID('buttonLearningSearch')}
                  accessibilityLabel="button_learning_search"
                  transparent
                  onPress={() =>
                    this.props.navigation.navigate("LearningSearch") +
                    AdjustTracker(AdjustTrackerConfig.Learning_Search)
                  }
                >
                  <Icon
                    type="EvilIcons"
                    name="search"
                    style={{ fontSize: 28 }}
                  />
                </Button>
              </Right>
            </Header>
          </Animated.View>
        </Container>
      </Fragment>
    );
  }
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const heightHeaderSpan = 170;
const styles = StyleSheet.create({
  animHeader: {
    position: "absolute",
    width: "100%",
    zIndex: 2,
  },
  animHeaderSpan: {
    position: "absolute",
    paddingTop: platform.toolbarHeight,
    backgroundColor: platform.toolbarDefaultBg,
    height: heightHeaderSpan,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: "flex-end",
    alignItems: "flex-start",
  },
  emptyItem: {
    justifyContent: "center",
    alignItems: "center",
    height: platform.deviceHeight / 2 - 80,
  },
  heading: {
    fontFamily: "Nunito-Bold",
    fontSize: 14,
    color: "#fff",
    marginTop: 20,
  },
  textTitle: (backButtonAvailable) => ({
    fontSize: 30,
    marginLeft: backButtonAvailable == true ? 20 : null,
  }),
});
