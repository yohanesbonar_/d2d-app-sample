import React, { Component, Fragment } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  Animated,
  View,
  Modal,
  Image,
  TouchableOpacity,
  FlatList,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Tabs,
  Tab,
  ScrollableTab,
  TabHeading,
  Toast,
} from "native-base";
import Menu, { MenuItem } from "react-native-material-menu";
import { Loader, ListItemLearningSpecialist } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import { EnumFeedsCategory, testID } from "./../../libs/Common";
import Api, { errorMessage } from "./../../libs/Api";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import Orientation from "react-native-orientation";
import { SafeAreaView } from "react-native";

let from = {
  Search: "Search",
  All: "All",
  Journal: "Journal",
  Video: "Video",
  Guideline: "Guideline",
  RequestJurnal: "RequestJurnal",
};

export default class LearningSpecialist extends Component {
  menu = null;
  page = 1;
  pageBookmark = 1;
  state = {
    animatedValue: new Animated.Value(0),
    cmespessialist: [],
    bookmarks: [],
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    isFetchingAll: false,
    isFetchingBookmark: false,
    isEmptyDataAll: false,
    isEmptyDataBookmark: false,
    dataCompetence: null,
    filterJournal: true,
    filterVideo: true,
    filterGuideline: true,
    uid: null,
    activeTab: 0,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    StatusBar.setHidden(false);
    this.initial();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };
  async initial() {
    let getUid = await AsyncStorage.getItem("UID");

    const { params } = this.props.navigation.state;

    console.log("params: ", params);
    this.setState({
      dataCompetence: params,
      filterJournal: true,
      filterVideo: true,
      filterGuideline: true,
      uid: getUid,
    });
    this.getLearningAll(this.page, params.id);
    this.getLearningBookmark(this.pageBookmark, params.id);
  }

  setMenuRef = (ref) => {
    this.menu = ref;
  };

  async getLearningAll(page, competence) {
    this.setState({ isFailed: false, isFetching: true });

    let dataCompetence =
      competence != null ? competence : this.state.dataCompetence.id;

    try {
      let response = await Api.get(
        `feeds?page=${page}&competence=${dataCompetence}&${
          EnumFeedsCategory.VIDEO_LEARNING
        }=${this.state.filterVideo}&${EnumFeedsCategory.PDF_JOURNAL}=${
          this.state.filterJournal
        }&${EnumFeedsCategory.PDF_GUIDELINE}=${
          this.state.filterGuideline
        }&uid=${this.state.uid}`
      );

      if (errorMessage(response) == "") {
        this.setState({
          cmespessialist:
            page == 1
              ? [...response.result.data]
              : [...this.state.cmespessialist, ...response.result.data],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingAll: false,
          isEmptyDataAll:
            response.result.data != null && response.result.data.length > 0
              ? false
              : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingAll: false,
        });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        isFetchingAll: false,
      });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  async getLearningBookmark(page, competence) {
    this.setState({ isFailed: false, isFetching: true });

    let dataCompetence =
      competence != null ? competence : this.state.dataCompetence.id;

    try {
      let response = await Api.get(
        `feeds?page=${page}&competence=${dataCompetence}&${
          EnumFeedsCategory.VIDEO_LEARNING
        }=${this.state.filterVideo}&${EnumFeedsCategory.PDF_JOURNAL}=${
          this.state.filterJournal
        }&${EnumFeedsCategory.PDF_GUIDELINE}=${
          this.state.filterGuideline
        }&bookmark=true&uid=${this.state.uid}`
      );

      if (errorMessage(response) == "") {
        this.setState({
          bookmarks:
            page == 1
              ? [...response.result.data]
              : [...this.state.bookmarks, ...response.result.data],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingBookmark: false,
          isEmptyDataBookmark:
            response.result.data != null && response.result.data.length > 0
              ? false
              : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingBookmark: false,
        });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        isFetchingBookmark: false,
      });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  onRefreshAll() {
    this.page = 1;
    this.setState({
      cmespessialist: [],
      isFetchingAll: true,
      isEmptyDataAll: false,
    });
    this.getLearningAll(this.page, null);
  }

  onRefreshBookmark() {
    this.pageBookmark = 1;
    this.setState({
      bookmarks: [],
      isFetchingBookmark: true,
      isEmptyDataBookmark: false,
    });
    this.getLearningBookmark(this.pageBookmark, null);
  }

  handleLoadMore = () => {
    if (this.state.isEmptyDataAll === true) {
      return;
    }
    this.page = this.state.isFailed == true ? this.page : this.page + 1;
    this.getLearningAll(this.page, null);
  };

  handleLoadMoreBookmark = () => {
    if (this.state.isEmptyDataBookmark === true) {
      return;
    }
    this.pageBookmark =
      this.state.isFailed == true ? this.pageBookmark : this.pageBookmark + 1;
    this.getLearningBookmark(this.pageBookmark, null);
  };

  _renderItem = ({ item }, test) => {
    let tempData = {
      type: test,
      ...item,
    };
    return (
      <ListItemLearningSpecialist
        navigation={this.props.navigation}
        data={tempData}
      />
    );
  };

  _renderItemFooter = (type) => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader(type)}
    </View>
  );

  _renderItemFooterLoader(type) {
    if (
      this.state.isFailed == true &&
      ((this.page > 1 && type == "all") ||
        (this.pageBookmark > 1 && type == "bookmark"))
    ) {
      return (
        <TouchableOpacity
          // {...testID('buttonBookmark')}
          accessibilityLabel="button_bookmark"
          onPress={() => {
            if (type == "bookmark") this.handleLoadMoreBookmark();
            else this.handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = () => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
  );

  _renderEmptyItemLoader() {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.handleLoadMore()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>touch to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (this.state.isFetching == false && this.state.isSuccess == true) {
      if (
        this.state.cmespessialist.length == 0 ||
        this.state.bookmarks.length == 0
      ) {
        return <Text style={{ textAlign: "center" }}>Data not found</Text>;
      }

      return null;
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  openRequestJurnal = () => {
    this.menu.hide();
    // const requestJournal= { type: "Navigate", routeName: "LearningRequestJournal"}
    this.props.navigation.navigate("LearningRequestJournal");
  };

  doFilterAll() {
    this.menu.hide();
    this.page = 1;
    this.pageBookmark = 1;
    this.setState({
      filterJournal: true,
      filterVideo: true,
      filterGuideline: true,
    });

    setTimeout(() => {
      this.setState({ cmespessialist: [], bookmarks: [] });
      this.getLearningAll(this.page, null);
      this.getLearningBookmark(this.pageBookmark, null);
    }, 200);
  }

  doFilterJournal() {
    this.menu.hide();
    this.page = 1;
    this.pageBookmark = 1;
    this.setState({
      filterJournal: true,
      filterVideo: false,
      filterGuideline: false,
    });

    setTimeout(() => {
      this.setState({ cmespessialist: [], bookmarks: [] });
      this.getLearningAll(this.page, null);
      this.getLearningBookmark(this.pageBookmark, null);
    }, 200);
  }

  doFilterVideo() {
    this.menu.hide();
    this.page = 1;
    this.pageBookmark = 1;
    this.setState({
      filterJournal: false,
      filterVideo: true,
      filterGuideline: false,
    });

    setTimeout(() => {
      this.setState({ cmespessialist: [], bookmarks: [] });
      this.getLearningAll(this.page, null);
      this.getLearningBookmark(this.pageBookmark, null);
    }, 200);
  }

  doFilterGuideline() {
    this.menu.hide();
    this.page = 1;
    this.pageBookmark = 1;
    this.setState({
      filterJournal: false,
      filterVideo: false,
      filterGuideline: true,
    });

    setTimeout(() => {
      this.setState({ cmespessialist: [], bookmarks: [] });
      this.getLearningAll(this.page, null);
      this.getLearningBookmark(this.pageBookmark, null);
    }, 200);
  }

  autoRefresh() {
    this.onRefreshAll();
    this.onRefreshBookmark();
  }

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.Search:
        token = AdjustTrackerConfig.Learning_Spesialis_Search;
        break;
      case from.All:
        token = AdjustTrackerConfig.Learning_Spesialis_All;
        break;
      case from.Journal:
        token = AdjustTrackerConfig.Learning_Spesialis_Journal;
        break;
      case from.Video:
        token = AdjustTrackerConfig.Learning_Spesialis_Video;
        break;
      case from.Guideline:
        token = AdjustTrackerConfig.Learning_Spesialis_Guideline;
        break;
      case from.RequestJurnal:
        token = AdjustTrackerConfig.Learning_Spesialis_RequestJurnal;
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  render() {
    const nav = this.props.navigation;
    const { params } = nav.state;
    const searchLearningSpecialist = {
      type: "Navigate",
      routeName: "LearningSpecialistSearch",
      params: params,
    };

    let translateY = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
      extrapolate: "clamp",
    });
    let opacityContent = this.state.animatedValue.interpolate({
      inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 3],
      outputRange: [1, 0],
    });
    let opacityTitle = this.state.animatedValue.interpolate({
      inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 3],
      outputRange: [0, 1],
    });

    return (
      <Fragment>
        <SafeAreaView
          style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }}
        />
        <Container>
          <Animated.View style={styles.animHeader}>
            <Header noShadow hasTabs>
              <Left style={{ flex: 0.5 }}>
                <Button
                  // {...testID('buttonBack')}
                  accessibilityLabel="button_back"
                  transparent
                  onPress={() => this.onBackPressed()}
                >
                  <Icon name="md-arrow-back" style={styles.toolbarIcon} />
                </Button>
              </Left>
              <Body
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <AnimatedTitle style={{ opacity: opacityTitle }}>
                  {params.description}
                </AnimatedTitle>
              </Body>
              <Right style={[styles.headerRight, { flex: 0.5 }]}>
                <Button
                  // {...testID('buttonSearch')}
                  accessibilityLabel="button_search"
                  transparent
                  onPress={() =>
                    this.props.navigation.navigate(searchLearningSpecialist) +
                    this.sendAdjust(from.Search)
                  }
                >
                  <Icon
                    type="EvilIcons"
                    name="search"
                    style={{ fontSize: 26 }}
                  />
                </Button>
                <Menu
                  style={{ padding: 5 }}
                  ref={this.setMenuRef}
                  button={
                    <Button
                      // {...testID('buttonMore')}
                      accessibilityLabel="button_more"
                      nopadding
                      transparent
                      onPress={() => this.menu.show()}
                      style={{ paddingHorizontal: 10 }}
                    >
                      <Icon
                        name="md-more"
                        nopadding
                        style={{ color: "#FFFFFF", fontSize: 26 }}
                      />
                    </Button>
                  }
                >
                  <MenuItem
                    // {...testID('buttonAll')}
                    accessibilityLabel="button_all"
                    textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                    onPress={() =>
                      this.doFilterAll() + this.sendAdjust(from.All)
                    }
                  >
                    All
                  </MenuItem>
                  <MenuItem
                    // {...testID('buttonJournal')}
                    accessibilityLabel="button_journal"
                    extStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                    onPress={() =>
                      this.doFilterJournal() + this.sendAdjust(from.Journal)
                    }
                  >
                    Journal
                  </MenuItem>
                  <MenuItem
                    // {...testID('buttonVideo')}
                    accessibilityLabel="button_video"
                    textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                    onPress={() =>
                      this.doFilterVideo() + this.sendAdjust(from.Video)
                    }
                  >
                    Video
                  </MenuItem>
                  <MenuItem
                    // {...testID('buttonGuideline')}
                    accessibilityLabel="button_guideline"
                    textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                    onPress={() =>
                      this.doFilterGuideline() + this.sendAdjust(from.Guideline)
                    }
                  >
                    Guideline
                  </MenuItem>
                  <MenuItem
                    // {...testID('buttonRequestJournal')}
                    accessibilityLabel="button_request_journal"
                    textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                    onPress={() =>
                      this.openRequestJurnal() +
                      this.sendAdjust(from.RequestJurnal)
                    }
                  >
                    Request Journal
                  </MenuItem>
                </Menu>
              </Right>
            </Header>
          </Animated.View>

          <Animated.View
            style={[styles.animHeaderSpan, { transform: [{ translateY }] }]}
          >
            <Animated.Text
              style={[styles.heading, { opacity: opacityContent }]}
            >
              {params.description}
            </Animated.Text>
          </Animated.View>

          <Tabs
            renderTabBar={(props) => (
              <Animated.View
                style={{
                  transform: [{ translateY }],
                  top: heightHeaderSpan,
                  zIndex: 1,
                  width: "100%",
                }}
              >
                <ScrollableTab
                  {...props}
                  renderTab={(name, page, active, onPress, onLayout) => (
                    <TouchableOpacity
                      key={page}
                      onPress={() => {
                        onPress(page);
                      }}
                      onLayout={onLayout}
                      activeOpacity={0.99}
                    >
                      <Animated.View style={styles.tabHeading}>
                        <TabHeading
                          // {...testID('tabHeading')}
                          accessibilityLabel="tab_heading"
                          style={{
                            width: platform.deviceWidth / 2,
                            height: platform.toolbarHeight,
                          }}
                          active={active}
                        >
                          <Text
                            style={{
                              fontFamily: active
                                ? "Nunito-Bold"
                                : "Nunito-Regular",
                              color: active
                                ? platform.topTabBarActiveTextColor
                                : platform.topTabBarTextColor,
                              fontSize: platform.tabFontSize,
                            }}
                          >
                            {name}
                          </Text>
                        </TabHeading>
                      </Animated.View>
                    </TouchableOpacity>
                  )}
                  underlineStyle={{
                    backgroundColor: platform.topTabBarActiveBorderColor,
                  }}
                />
              </Animated.View>
            )}
            onChangeTab={() => this.autoRefresh()}
          >
            <Tab
              // {...testID('tabLatest')}
              accessibilityLabel="tab_latest"
              heading="LATEST"
            >
              <AnimatedFlatList
                contentContainerStyle={{
                  paddingTop: heightHeaderSpan + 5,
                  paddingHorizontal: 10,
                }}
                scrollEventThrottle={1} // <-- Use 1 here to make sure no cmespessialist are ever missed
                onScroll={Animated.event(
                  [
                    {
                      nativeEvent: {
                        contentOffset: { y: this.state.animatedValue },
                      },
                    },
                  ],
                  { useNativeDriver: true } // <-- Add this
                )}
                data={this.state.cmespessialist}
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={0.5}
                onRefresh={() => this.onRefreshAll()}
                refreshing={this.state.isFetchingAll}
                renderItem={(item) => this._renderItem(item, "all")}
                ListEmptyComponent={this._renderEmptyItem}
                ListFooterComponent={this._renderItemFooter("all")}
                keyExtractor={(item, index) => index.toString()}
              />
            </Tab>
            <Tab
              // {...testID('tabBookmark')}
              accessibilityLabel="tab_bookmark"
              heading="BOOKMARK"
            >
              <AnimatedFlatList
                contentContainerStyle={{
                  paddingTop: heightHeaderSpan + 5,
                  paddingHorizontal: 10,
                }}
                scrollEventThrottle={1} // <-- Use 1 here to make sure no cmespessialist are ever missed
                onScroll={Animated.event(
                  [
                    {
                      nativeEvent: {
                        contentOffset: { y: this.state.animatedValue },
                      },
                    },
                  ],
                  { useNativeDriver: true } // <-- Add this
                )}
                data={this.state.bookmarks}
                onEndReached={this.handleLoadMoreBookmark}
                onEndReachedThreshold={0.5}
                onRefresh={() => this.onRefreshBookmark()}
                refreshing={this.state.isFetchingBookmark}
                renderItem={(item) => this._renderItem(item, "bookmark")}
                ListEmptyComponent={this._renderEmptyItem}
                ListFooterComponent={this._renderItemFooter("bookmark")}
                keyExtractor={(item, index) => index.toString()}
              />
            </Tab>
          </Tabs>
        </Container>
      </Fragment>
    );
  }
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const AnimatedTitle = Animated.createAnimatedComponent(Title);
const heightHeaderSpan = 150;
const styles = StyleSheet.create({
  headerBody: {
    justifyContent: "center",
    alignItems: "center",
  },
  headerRight: {
    justifyContent: "center",
    alignItems: "flex-end",
    paddingHorizontal: platform.platform === "ios" ? 10 : 0,
  },
  title: {
    color: platform.titleFontColor,
    fontFamily: platform.titleFontfamily,
    fontSize: platform.titleFontSize,
  },
  heading: {
    color: "#fff",
    fontSize: 20,
    fontFamily: "Nunito-SemiBold",
    flexWrap: "wrap",
    textAlign: "left",
  },
  animHeader: {
    position: "absolute",
    width: "100%",
    zIndex: 2,
  },
  animHeaderSpan: {
    position: "absolute",
    paddingTop: platform.toolbarHeight,
    backgroundColor: platform.toolbarDefaultBg,
    height: heightHeaderSpan,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    justifyContent: "flex-end",
    alignItems: "flex-start",
    // flexDirection: 'row',
  },
  emptyItem: {
    justifyContent: "center",
    alignItems: "center",
    height: platform.deviceHeight / 2 - 80,
  },
  tabHeading: {
    flex: 1,
    height: 100,
    backgroundColor: platform.tabBgColor,
    justifyContent: "center",
    alignItems: "center",
  },
});
