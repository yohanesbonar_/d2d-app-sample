import React, { Component, Fragment } from "react";
import {
  StatusBar,
  StyleSheet,
  Animated,
  View,
  Modal,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Tabs,
  Tab,
  Item,
  Input,
  ScrollableTab,
  TabHeading,
  Toast,
} from "native-base";
import { Loader, ListItemLearningSpecialist } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import Api, { errorMessage } from "./../../libs/Api";
import { EnumFeedsCategory, testID } from "./../../libs/Common";
import Orientation from "react-native-orientation";
export default class LearningSpecialistSearch extends Component {
  pageVideo = 1;
  pageGuideline = 1;
  pageJournal = 1;
  timer = null;
  state = {
    competenceId: null,
    videoList: [],
    guidelineList: [],
    journalList: [],
    searchValue: "",
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    isEmptyDataVideo: false,
    isEmptyDataGuideline: false,
    isEmptyDataJournal: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    StatusBar.setHidden(false);
    const { params } = this.props.navigation.state;
    console.log(params);
    this.setState({ competenceId: params.id });

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });

    setTimeout(() => {
      this.getData();
    }, 500);
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  getData() {
    this.getVideoList(this.pageVideo);
    this.getGuidelineList(this.pageGuideline);
    this.getJournalList(this.pageJournal);
  }

  async getVideoList(page) {
    if (this.state.searchValue == "") {
      this.setState({ isFetching: false, isSuccess: true });
      return;
    }

    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await Api.get(
        `feeds?page=${page}&competence=${this.state.competenceId}&search=${
          this.state.searchValue
        }&${EnumFeedsCategory.VIDEO_LEARNING}=true`
      );

      if (errorMessage(response) == "") {
        this.setState({
          videoList: [...this.state.videoList, ...response.result.data],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isEmptyDataVideo:
            response.result.data != null && response.result.data.length > 0
              ? false
              : true,
        });
        // this.setState({ isFetching: false, isSuccess: true, isFailed: false });
      } else {
        this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  async getGuidelineList(page) {
    if (this.state.searchValue == "") {
      this.setState({ isFetching: false, isSuccess: true });
      return;
    }

    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await Api.get(
        `feeds?page=${page}&search=${this.state.searchValue}&${
          EnumFeedsCategory.PDF_GUIDELINE
        }=true`
      );

      if (errorMessage(response) == "") {
        this.setState({
          guidelineList: [...this.state.guidelineList, ...response.result.data],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isEmptyDataGuideline:
            response.result.data != null && response.result.data.length > 0
              ? false
              : true,
        });
        // this.setState({ isFetching: false, isSuccess: true, isFailed: false });
      } else {
        this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  async getJournalList(page) {
    if (this.state.searchValue == "") {
      this.setState({ isFetching: false, isSuccess: true });
      return;
    }

    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await Api.get(
        `feeds?page=${page}&search=${this.state.searchValue}&${
          EnumFeedsCategory.PDF_JOURNAL
        }=true`
      );

      if (errorMessage(response) == "") {
        this.setState({
          journalList: [...this.state.journalList, ...response.result.data],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isEmptyDataJournal:
            response.result.data != null && response.result.data.length > 0
              ? false
              : true,
        });
        // this.setState({ isFetching: false, isSuccess: true, isFailed: false });
      } else {
        this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMoreVideo = () => {
    if (this.state.isEmptyDataVideo == true) {
      return;
    }
    this.pageVideo =
      this.state.isFailed == true ? this.pageVideo : this.pageVideo + 1;
    this.getVideoList(this.pageVideo);
  };

  handleLoadMoreGuideline = () => {
    if (this.state.isEmptyDataGuideline == true) {
      return;
    }
    this.pageGuideline =
      this.state.isFailed == true ? this.pageGuideline : this.pageGuideline + 1;
    this.getGuidelineList(this.pageGuideline);
  };

  handleLoadMoreJournal = () => {
    if (this.state.isEmptyDataJournal == true) {
      return;
    }
    this.pageJournal =
      this.state.isFailed == true ? this.pageJournal : this.pageJournal + 1;
    this.getJournalList(this.pageJournal);
  };

  _renderItem = ({ item }) => {
    return (
      <ListItemLearningSpecialist
        navigation={this.props.navigation}
        data={item}
      />
    );
  };

  _renderItemFooter = (type) => (
    <View
      style={{
        height: this.state.isFetching == true || type == "journal" ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader(type)}
      {this._renderButtonRequestJournal(type == "journal" ? true : false)}
    </View>
  );

  _renderButtonRequestJournal(isShowing) {
    if (isShowing == true) {
      return (
        <Button
          style={styles.btnReqJournal}
          onPress={() => this.openRequestJurnal()}
          success
          block
        >
          <Text style={{ fontFamily: "Nunito-Bold" }}>
            Didn't found any journal?
          </Text>
        </Button>
      );
    }
  }

  openRequestJurnal = () => {
    this.props.navigation.navigate("LearningRequestJournal");
  };

  _renderItemFooterLoader(type) {
    if (
      this.state.isFailed == true &&
      ((this.pageVideo > 1 && type == "video") ||
        (this.pageGuideline > 1 && type == "guideline") ||
        (this.pageJournal > 1 && type == "journal"))
    ) {
      return (
        <TouchableOpacity
          onPress={() => {
            if (type == "video") this.handleLoadMoreVideo();
            else if (type == "guideline") this.handleLoadMoreGuideline();
            else if (type == "journal") this.handleLoadMoreJournal();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = () => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
  );

  _renderEmptyItemLoader() {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.handleLoadMore()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>touch to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (this.state.isFetching == false && this.state.isSuccess == true) {
      let messageNotFound = "We are really sad we could not find anything";
      if (this.state.searchValue != "") {
        messageNotFound += " for " + this.state.searchValue;
      }
      return (
        <View style={styles.viewNotFound}>
          <Icon name="ios-search" style={styles.iconNotFound} />
          <Text style={styles.textNotFound}>{messageNotFound}</Text>
        </View>
      );
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  doSearch = (inputText) => {
    clearTimeout(this.timer);
    this.setState({
      videoList: [],
      guidelineList: [],
      journalList: [],
      searchValue: inputText,
      isEmptyDataVideo: false,
      isEmptyDataJournal: false,
      isEmptyDataGuideline: false,
    });

    if (inputText.length >= 3) {
      this.pageVideo = 1;
      this.pageGuideline = 1;
      this.pageJournal = 1;

      this.timer = setTimeout(() => {
        this.getData();
      }, 1000);
    }
  };

  render() {
    const nav = this.props.navigation;

    return (
      <Fragment>
        <SafeAreaView
          style={{ zIndex: 1, backgroundColor: platform.toolbarDefaultBg }}
        />

        <Container>
          <Header noShadow searchBar rounded>
            <Item nopadding>
              <Icon name="ios-search" />
              <Input
                // {...testID('inputSearch')}
                accessibilityLabel="input_search"
                placeholder="Search video, guideline, or journal"
                onChangeText={(text) => this.doSearch(text)}
                value={this.state.searchValue}
                style={styles.searchField}
              />
              <Icon name="ios-close-circle" onPress={() => this.doSearch("")} />
            </Item>
            <TouchableOpacity
              {...testID("button_close")}
              accessibilityLabel="button_close"
              transparent
              style={{ marginRight: 5, marginLeft: 10, alignSelf: "center" }}
              onPress={() => this.onBackPressed()}
            >
              <Icon
                name="md-close"
                type="Ionicons"
                style={[styles.iconClose]}
              />
            </TouchableOpacity>
            {/* <Button
                        // {...testID('buttonClose')}
                        accessibilityLabel="button_close"
                        transparent style={{ marginRight: 5 }} onPress={() => nav.goBack()}>
                        <Icon name='ios-close' style={{ fontSize: 28 }} />
                    </Button> */}
          </Header>
          {/* <Content> */}
          <Tabs
            initialPage={0}
            renderTabBar={(props) => (
              <ScrollableTab
                {...props}
                renderTab={(name, page, active, onPress, onLayout) => (
                  <TouchableOpacity
                    key={page}
                    onPress={() => {
                      onPress(page);
                    }}
                    onLayout={onLayout}
                    activeOpacity={0.99}
                  >
                    <Animated.View style={styles.tabHeading}>
                      <TabHeading
                        // {...testID('tabHeading')}
                        //accessibilityLabel="tab_heading"
                        style={{
                          width: platform.deviceWidth / 3,
                          height: platform.toolbarHeight,
                        }}
                        active={active}
                      >
                        <Text
                          style={{
                            fontFamily: active
                              ? "Nunito-Bold"
                              : "Nunito-Regular",
                            color: active
                              ? platform.topTabBarActiveTextColor
                              : platform.topTabBarTextColor,
                            fontSize: platform.tabFontSize,
                          }}
                        >
                          {name}
                        </Text>
                      </TabHeading>
                    </Animated.View>
                  </TouchableOpacity>
                )}
                underlineStyle={{
                  backgroundColor: platform.topTabBarActiveBorderColor,
                }}
              />
            )}
          >
            <Tab heading="VIDEO" accessibilityLabel="tab_video">
              <View>
                <FlatList
                  style={{ marginHorizontal: 10 }}
                  data={this.state.videoList}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={this._renderItem}
                  ListEmptyComponent={this._renderEmptyItem}
                  ListFooterComponent={this._renderItemFooter("video")}
                  refreshing={true}
                  onEndReached={this.handleLoadMoreVideo}
                  onEndReachedThreshold={0.5}
                />
              </View>
            </Tab>
            <Tab heading="GUIDELINE" accessibilityLabel="tab_video">
              <View>
                <FlatList
                  style={{ marginHorizontal: 10 }}
                  data={this.state.guidelineList}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={this._renderItem}
                  ListEmptyComponent={this._renderEmptyItem}
                  ListFooterComponent={this._renderItemFooter("guideline")}
                  refreshing={true}
                  onEndReached={this.handleLoadMoreGuideline}
                  onEndReachedThreshold={0.5}
                />
              </View>
            </Tab>
            <Tab heading="JOURNAL" accessibilityLabel="tab_video">
              <View>
                <FlatList
                  style={{ marginHorizontal: 10 }}
                  data={this.state.journalList}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={this._renderItem}
                  ListEmptyComponent={this._renderEmptyItem}
                  ListFooterComponent={this._renderItemFooter("journal")}
                  refreshing={true}
                  onEndReached={this.handleLoadMoreJournal}
                  onEndReachedThreshold={0.5}
                />
              </View>
            </Tab>
          </Tabs>

          {/* </Content> */}
        </Container>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  searchField: {
    fontSize: 14,
    lineHeight: 15,
  },
  tab: {
    padding: 10,
  },
  btnNotFound: {
    backgroundColor: "#3AE194",
    flex: 1,
    justifyContent: "center",
  },
  viewNotFound: {
    marginVertical: 20,
    marginHorizontal: 10,
  },
  iconNotFound: {
    fontSize: 80,
    textAlign: "center",
  },
  textNotFound: {
    textAlign: "center",
    fontSize: 20,
  },
  btnReqJournal: {
    marginHorizontal: 10,
    marginVertical: 20,
    borderRadius: 10,
    height: 55,
  },
  tabHeading: {
    flex: 1,
    height: 100,
    backgroundColor: platform.tabBgColor,
    justifyContent: "center",
    alignItems: "center",
  },
  iconClose: {
    fontSize: 28,
    color: "#fff",
  },
});
