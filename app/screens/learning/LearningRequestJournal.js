import React, { Component } from 'react';
import { NavigationActions, StackActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Modal, ImageBackground, TouchableOpacity, Alert, Image, BackHandler } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Item, Input, Label, Toast, Card, CardItem, Col, Row } from 'native-base';
import { Loader, Popup } from './../../components';
import { validateEmail, validatePhoneNumber, STORAGE_TABLE_NAME, testID, removeEmojis } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import { doRequestJournal } from './../../libs/NetworkUtility'
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { remove } from 'lodash';
import { translate } from '../../libs/localize/LocalizeHelper'
import country from "../../libs/countries/assets/data/country.json";
import DeviceInfo from 'react-native-device-info';
import moment from "moment-timezone";
import _ from "lodash";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as lor,
    removeOrientationListener as rol,
} from "react-native-responsive-screen";
import { SafeAreaView } from 'react-native';
import { getData, getProfile, KEY_ASYNC_STORAGE, storeData, updateDataProfile } from '../../../src/utils';
import { IconCambodiaFlag, IconIndonesiaFlag, IconMyanmarFlag, IconPhilippinesFlag, IconSingaporeFlag } from '../../../src/assets';

export default class LearningRequestJournal extends Component {

    SPESIALIZATION = 'spesialization';

    state = {
        showLoader: false,
        modalVisible: false,
        name: null,
        nameFailed: false,
        address: null,
        addressFailed: false,
        cityDoctorPractice: null,
        cityDoctorPracticeFailed: false,
        spesialization: null,
        spesializationFailed: false,
        phone: null,
        phoneFailed: false,
        email: null,
        emailFailed: false,
        journal: null,
        journalFailed: false,
        messageSuccessReqJournal: 'We will send the status update through your email. Thank you.',
        isFromDeeplink: false,
        country_code: false,
        from: ""
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        const nav = this.props.navigation;
        const { params } = nav.state;
        if (params != null) {
            this.setState({ journal: params.title })
            if (typeof (params) != "undefined") {
                if (params.from != null && !_.isEmpty(params.from)) {
                    this.setState({ from: params.from });
                }
            }
        }

        this.doGetProfile();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    async doGetProfile() {

        try {
            let response = await getProfile();
            console.log("response: ", response);
            if (response.acknowledge) {
                let result = response.result;
                await storeData(KEY_ASYNC_STORAGE.PROFILE, result);
                await this.getDataProfile();
            }
        } catch (error) {
            console.log("error: ", error);
        }
    };

    async getDataProfile() {

        let isCompletedProfile = true;
        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let dataProfile = JSON.parse(getJsonProfile);

        if (dataProfile != null) {
            if (_.isEmpty(dataProfile.subscription) ||
                (dataProfile.phone == null) || (dataProfile.born_date == null) ||
                (dataProfile.clinic_location_1 == null && dataProfile.clinic_location_2 == null && dataProfile.clinic_location_3 == null) ||
                (dataProfile.education_1 == null && dataProfile.education_2 == null && dataProfile.education_3 == null)) {
                isCompletedProfile = false;
            }

            if (isCompletedProfile || dataProfile.country_code == "PH") {
                this.setState({
                    country_code: dataProfile.country_code,
                    name: dataProfile.name,
                    address: dataProfile.clinic_location_1,
                    spesialization: dataProfile.spesialization,
                    phone: dataProfile.phone,
                    cityDoctorPractice: dataProfile.home_location,
                    email: dataProfile.country_code != "PH" ? dataProfile.email : "",
                })
            } else {
                this.showAlertCompleteProfile();
            }
        }
    }

    showAlertCompleteProfile = () => {
        Alert.alert(
            'Warning',
            'Your profile is not completed, please completed profile first',
            [
                // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'OK', onPress: () => this.gotoProfile(),
                },
            ],
            { cancelable: true }
        );
    }

    gotoProfile = () => {
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'Profile',
            key: `rq_journal_profile`
        }));
    }

    async submitPress() {
        // Adjust Tracker
        AdjustTracker(AdjustTrackerConfig.RequestJurnal_Submit);

        const nav = this.props.navigation;
        const resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'RegisterVerification' })] });

        let errorMessage = this.doValidateField();
        if (errorMessage == null) {
            this.setState({ showLoader: true, modalVisible: false });
            this.doRequestJournal();
        } else {
            Toast.show({ text: errorMessage, position: 'top', duration: 3000 })
        }
    }

    doValidateField() {
        let errorMessage = null;
        let messages = [];

        if (!this.state.name) {
            this.setState({ nameFailed: true });
            messages.push('Name can not be empty');
        } else {
            this.setState({ nameFailed: false })
        }

        if (!this.state.address && this.state.country_code != "PH") {
            this.setState({ addressFailed: true });
            messages.push('Doctor practice can not be empty');
        } else {
            this.setState({ addressFailed: false })
        }

        if (!this.state.cityDoctorPractice && this.state.country_code == "ID") {
            this.setState({ cityDoctorPracticeFailed: true });
            messages.push('City can not be empty');
        } else {
            this.setState({ cityDoctorPracticeFailed: false })
        }

        if ((this.state.spesialization == null || this.state.spesialization.length <= 0) && this.state.country_code != "PH") {
            this.setState({ spesializationFailed: true });
            messages.push('Please select at least one specialization');
        } else {
            this.setState({ spesializationFailed: false });
        }

        if (!this.state.email) {
            this.setState({ emailFailed: true });
            messages.push('Email address can not be empty');
        } else {
            if (validateEmail(this.state.email) === false) {
                this.setState({ emailFailed: true })
                messages.push('Email address is invalid');
            } else {
                this.setState({ emailFailed: false })
            }
        }

        if (!this.state.phone && this.state.country_code != "PH") {
            this.setState({ phoneFailed: true });
            messages.push('Phone number can not be empty');
        } else {
            if (validatePhoneNumber(this.state.phone) === false && this.state.country_code != "PH") {
                this.setState({ phoneFailed: true })
                messages.push('Phone number is invalid');
            } else {
                this.setState({ phoneFailed: false })
            }
        }

        if (!this.state.journal) {
            this.setState({ journalFailed: true });
            messages.push('Journal can not be empty');
        } else {
            this.setState({ journalFailed: false })
        }

        if (messages != null && messages.length > 0) {
            errorMessage = messages.join(', ');
        }


        return errorMessage;
    }

    async doRequestJournal() {
        try {
            let getUid = await AsyncStorage.getItem('UID');

            let spesialistId = [];
            let spesialistTitle = [];
            let competenceId = ""
            let competenceTitle = ""

            if (this.state.country_code != "PH") {
                this.state.spesialization.map(function (d, i) {
                    spesialistId.push(d.id);
                    spesialistTitle.push(d.title);
                });

                competenceId = spesialistId.join(',');
                competenceTitle = spesialistTitle.join(',');
            }


            let params = {
                uid: getUid,
                competence_id: competenceId,
                competence_name: competenceTitle,
                phone: this.state.phone,
                email: this.state.email,
                journal_title: this.state.journal,
                address: this.state.address,
                city: this.state.cityDoctorPractice,
                fullname: this.state.name,
            }
            await this.doUpdateProfile();
            let response = await doRequestJournal(params);
            console.log("doRequestJournal response: ", response)
            if (response.isSuccess) {
                if (response.result != null && response.result.message != null) {
                    this.setState({ messageSuccessReqJournal: response.result.message });
                }

                setTimeout(() => {
                    this.setState({ showLoader: false, modalVisible: true })
                    this.showPopupSubmitReqJournal()
                }, 2000)
            } else {
                this.setState({ showLoader: false, modalVisible: false })
                Toast.show({
                    text: "Something went wrong!",
                    position: "top",
                    duration: 5000,
                });
            }
        } catch (error) {
            console.log(error)
            this.setState({ showLoader: false });
            Toast.show({
                text: "Something went wrong!" + error,
                position: "top",
                duration: 5000,
            });
        }

    }

    async doUpdateProfile() {
        let fcm_token = await getData(KEY_ASYNC_STORAGE.FCM_TOKEN)


        let profile = await getData(KEY_ASYNC_STORAGE.PROFILE)
        console.log("data profile", profile);

        let npa_idi = profile.npa_idi

        let params = {
            country_code: profile.country_code,
            npa_idi: npa_idi,
            name: profile.name,
            email: this.state.email,
            phone: this.state.phone,
            born_date: moment(profile.born_date, 'DD MMM YYYY').format('YYYY-MM-DD'),
            //this.state.bornDate,
            education_1: profile.education_1,
            education_2: _.isEmpty(profile.education_2) ? '' : profile.education_2,
            education_3: _.isEmpty(profile.education_3) ? '' : profile.education_3,
            home_location: this.state.cityDoctorPractice,
            clinic_location_1: this.state.address,
            clinic_location_2: _.isEmpty(profile.clinic_location_2) ? '' : profile.clinic_location_2,
            clinic_location_3: _.isEmpty(profile.clinic_location_3) ? '' : profile.clinic_location_3,
            pns: this.state.pns == "pns" ? true : false,

            clinic_type_1: profile.clinic_type_1,
            clinic_type_2: profile.clinic_type_1,
            clinic_type_3: profile.clinic_type_1,

            //device info
            app_version: DeviceInfo.getVersion(),
            device_unique_id: DeviceInfo.getUniqueId(),
            fcm_token: fcm_token,
            device_platform: DeviceInfo.getSystemName(),
            system_version: DeviceInfo.getSystemVersion(),
            os_build_version: await DeviceInfo.getBuildId(), //should be await, because it was promise,
            device_brand: DeviceInfo.getBrand()
        }

        console.log("updateDataProfile params: ", params)

        let response = await updateDataProfile(params);
        console.log('doUpdateProfile response', response)

        if (response.acknowledge == true) {
        } else {
            Toast.show({ text: response.message, position: 'bottom', duration: 3000 });
        }

    }


    _showJournalTitle() {
        const { params } = this.props.navigation.state;
        console.log('_showJournalTitle params: ', params)


        if (params == null || (params != null && params.isFromDeeplink == true) || params.isFromPushNotification == true || params.sbcChannel != "") {
            return (
                <View>
                    {/* <Label style={{ marginTop: 15, marginBottom: 5 }}>Need another journal?</Label>
                    <Item error={this.state.journalFailed}>
                        <Input
                            // {...testID('title_journal')}
                            accessibilityLabel="title_journal"
                            placeholder={this.state.journal == null ? 'Write the title and description of journal here' : this.state.journal}
                            placeholderTextColor={platform.placeholderTextColor}
                            multiline={true}
                            numberOfLines={4}
                            value={this.state.journal}
                            style={{ height: 100 }}
                            onChangeText={(txt) => this.setState({ journal: removeEmojis(txt) })}
                            onTouchStart={() => AdjustTracker(AdjustTrackerConfig.Explore_ReqJournal_Title)} />
                    </Item> */}
                    <Label black style={styles.textLabel}>Description of Journal</Label>
                    <Item noShadowElevation style={[styles.itemInput, { marginBottom: 0 }]} error={this.state.journalFailed}>
                        <Input
                            newDesign
                            {...testID('title_journal')}
                            accessibilityLabel="title_journal"
                            placeholder={this.state.journal == null ? 'Write the description of the journal that you would like to request or you can also drop the source in here.' : this.state.journal}
                            placeholderTextColor={platform.placeholderTextColor}
                            multiline={true}
                            numberOfLines={4}
                            value={this.state.journal}
                            style={{ height: 150, paddingTop: 16 }}
                            onChangeText={(txt) => this.setState({ journal: removeEmojis(txt) })}
                            onTouchStart={() => { this.state.from == "Forum" ? AdjustTracker(AdjustTrackerConfig.RequestJurnal_Description) : AdjustTracker(AdjustTrackerConfig.Explore_ReqJournal_Title) }}
                        />
                    </Item>
                </View>
            )
        }

        return null;
    }

    // gotoSpesialization() {
    // this.props.navigation.navigate('Subscribtion', {
    //     from: 'REQUEST_JOURNAL',
    //     type: this.SPESIALIZATION,
    //     updateData: this.updateDataSpesialis,
    //     selectedSpecialist: this.state.spesialization
    // })
    // }


    gotoSelectSpecialist = () => {
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSpecialist',
            key: `gotoSelectSpecialistFromReqJournal`,
            params: {
                specialistList: this.state.spesialization,
                // saveDataSp: this.updateDataSp,
                saveDataSp: this.updateDataSpesialis,
                from: 'SPECIALIST_REQ_JOURNAL'
            }
            ,
        }));
    }


    updateDataSpesialis = (dataSpesialization) => {
        console.log(dataSpesialization);
        this.setState({ spesialization: dataSpesialization })

    };

    _renderSpesialization = () => {
        let keyAdd = this.state.spesialization != null ? this.state.spesialization.length : 1;

        return (
            <View style={styles.textSpecialist}>
                {this._renderTagSpecialist()}

                <TouchableOpacity
                    // {...testID('inputGoToSpecialization')}
                    accessibilityLabel="input_go_to_specialization"
                    key={keyAdd} onPress={() => this.gotoSelectSpecialist()} activeOpacity={0.7}>
                    <View style={styles.backgroundBtnAdd}>
                        <Text style={styles.textGrey}>+ Add</Text>
                    </View>
                </TouchableOpacity>
            </View>);

    }

    _renderTagSpecialist = () => {
        let items = [];
        if (this.state.spesialization != null && this.state.spesialization.length > 0) {
            let self = this;
            this.state.spesialization.map(function (d, i) {
                items.push(
                    <TouchableOpacity key={i} onPress={() => self.doDeletedSpesialization(self, i)} activeOpacity={0.7}>
                        <View style={styles.backgroundCompetence}>
                            <Text style={styles.textGrey}>{d.title}</Text>
                            <Icon name='ios-close' style={styles.tagIconX} />
                        </View>
                    </TouchableOpacity>
                )
            });

        }

        return items
    }

    doDeletedSpesialization = (self, index) => {
        self.state.spesialization.splice(index, 1);
        self.setState({ spesialization: self.state.spesialization })
    }

    doChooseCity = () => {
        AdjustTracker(AdjustTrackerConfig.Explore_ReqJournal_City);
        this.props.navigation.navigate('SearchCity', {
            onSelectedCity: this.onSelectedCity,
        })
    }

    onSelectedCity = (city) => {
        this.setState({ cityDoctorPractice: city, cityDoctorPracticeFailed: false })
    }

    _renderSelectedFlag = () => {
        let country_code = this.state.country_code
        if (country_code == "KH") {
            return (
                // <Image
                //     resizeMode={"contain"}
                //     source={require("./../../assets/images/flags/Cambodia.png")}
                //     style={{ width: 24, height: 24, marginRight: 12 }}
                // />
                <IconCambodiaFlag />
            );
        } else if (country_code == "MM") {
            return (
                <IconMyanmarFlag />
            );
        } else if (country_code == "PH") {
            return (
                <IconPhilippinesFlag />
            );
        } else if (country_code == "SG") {
            return (
                <IconSingaporeFlag />
            );
        } else {
            return (
                <IconIndonesiaFlag />
            );
        }
    };

    _renderCountries = () => {
        let resultCountry = country.filter((obj) => {
            return this.state.country_code == obj.country_code;
        });
        console.log('resultCountry: ', resultCountry)
        let countryName = resultCountry.length > 0 ? resultCountry[0].name : ''
        return (
            <Col>
                <Label black style={{ marginBottom: 12 }}>Country</Label>

                <Item
                    noShadowElevation
                    {...testID('item_select_country')}

                    error={this.state.emailFailed}
                    style={{ height: 56, alignItems: "center", borderColor: "#D7D7D7", backgroundColor: "#D7D7D7", marginBottom: 24 }}
                >

                    <View style={{ marginRight: 12 }}>{this._renderSelectedFlag()}</View>

                    <Text regular>{countryName}</Text>

                </Item>

            </Col >
        );
    };

    showPopupSubmitReqJournal = () => {
        const { params } = this.props.navigation.state;

        this.popup.togglePopup();

        setTimeout(() => {
            this.setState({ modalVisible: false })
            if (this.popup) {
                this.popup.togglePopup();
            }
            if (typeof (params) != "undefined") {
                if (params.sbcChannel != "") {
                    this.props.navigation.goBack();
                } else {
                    this.props.navigation.popToTop();
                }
            } else {
                this.props.navigation.popToTop();
            }

        }, 5000)

    }

    render() {
        let title = 'Request Journal'

        return (
            <Container>
                <View style={{ width: wp('200%'), height: wp('200%'), borderRadius: wp('100%'), backgroundColor: platform.toolbarDefaultBg, position: 'absolute', left: -wp('50%'), top: -wp('100%') }}></View>

                <Modal animationType="slide" transparent={true} visible={this.state.showLoader} onRequestClose={() => console.log('loader closed')}>
                    <Loader visible={this.state.showLoader} />
                </Modal>
                <Popup
                    ref={ref => {
                        this.popup = ref;
                    }}
                    title={'Submitted'}
                    message={this.state.messageSuccessReqJournal}
                    type={"successInformation"}
                    image={require("./../../assets/images/success-icon.png")}
                />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >

                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' style={{ color: '#fff' }} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>{title}</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <Content>

                    <View style={styles.content}>
                        <Card style={styles.card}>

                            <View style={styles.form}>
                                {/* <Label style={{ marginTop: 15, marginBottom: 5 }}>Fullname</Label>
                                <Item error={this.state.nameFailed}>
                                    <Input
                                        // {...testID('inputFullName')}
                                        accessibilityLabel="input_fullname"
                                        autoCapitalize="none"
                                        placeholder={'Fullname'}
                                        value={this.state.name}
                                        editable={false}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ name: removeEmojis(txt) })} />
                                </Item> */}
                                <Label black style={styles.textLabel}>Full Name</Label>
                                <Item noShadowElevation style={styles.itemInput} error={this.state.nameFailed}
                                >
                                    <Input
                                        newDesign
                                        autoCapitalize="none"
                                        placeholder={'Enter your Full Name'}
                                        editable={false}
                                        value={this.state.name}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ fullName: removeEmojis(txt) })}
                                        maxLength={200}
                                        {...testID('input_fullname')}
                                    />
                                </Item>

                                {this.state.country_code == "PH" && (
                                    this._renderCountries()
                                )}
                                {this.state.country_code != "PH" && (
                                    <View>
                                        <Label black style={styles.textLabel}>Doctor practice</Label>
                                        <Item noShadowElevation style={styles.itemInput} error={this.state.addressFailed}>
                                            <Input
                                                newDesign
                                                // {...testID('inputAddress')}
                                                accessibilityLabel="input_address"
                                                autoCapitalize="none"
                                                placeholder={translate("placeholder_short_practice_location")}
                                                value={this.state.address}
                                                placeholderTextColor={platform.placeholderTextColor}
                                                onChangeText={(txt) => this.setState({ address: removeEmojis(txt) })} />
                                        </Item>
                                    </View>
                                )}
                                {this.state.country_code == "ID" && (
                                    <View>
                                        <Label black style={styles.textLabel}>City</Label>
                                        <Item noShadowElevation style={styles.itemInput} error={this.state.cityDoctorPracticeFailed} onPress={() => this.doChooseCity()}>
                                            <Input
                                                newDesign
                                                // {...testID('inputCity')}
                                                accessibilityLabel="input_city"
                                                autoCapitalize="none"
                                                placeholder={'Choose City'}
                                                // onTouchStart={this.doChooseCity}
                                                editable={false}
                                                pointerEvents="none"
                                                value={this.state.cityDoctorPractice}
                                                placeholderTextColor={platform.placeholderTextColor}
                                                onChangeText={(txt) => this.setState({ cityDoctorPractice: txt })} />
                                        </Item>
                                    </View>
                                )}
                                {this.state.country_code != "PH" && (
                                    <View>
                                        <Label black style={styles.textLabel}>Specialist</Label>
                                        {this._renderSpesialization()}
                                        <Label black style={styles.textLabel}>Phone number</Label>
                                        <Item noShadowElevation style={styles.itemInput} error={this.state.phoneFailed}>
                                            <Input
                                                newDesign
                                                // {...testID('inputPhone')}
                                                accessibilityLabel="input_phone"
                                                autoCapitalize="none"
                                                keyboardType="numeric"
                                                placeholder={'ex: 0896xxxxxxxx'}
                                                value={this.state.phone}
                                                placeholderTextColor={platform.placeholderTextColor}
                                                onChangeText={(txt) => this.setState({ phone: txt })} />
                                        </Item>
                                    </View>
                                )}
                                {/* <Label style={{ marginTop: 15, marginBottom: 5 }}>Email address</Label>
                                <Item error={this.state.emailFailed}>
                                    <Input
                                        // {...testID('inputEmail')}
                                        accessibilityLabel="input_email"
                                        autoCapitalize="none"
                                        keyboardType="email-address"
                                        placeholder={'e-mail'}
                                        value={this.state.email}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ email: txt })} />
                                </Item> */}
                                <Label black style={styles.textLabel}>Email</Label>
                                <Item noShadowElevation style={styles.itemInput} error={this.state.emailFailed}>
                                    <Input
                                        newDesign
                                        autoCapitalize="none"
                                        keyboardType="email-address"
                                        placeholder={'Enter your email'}
                                        editable={this.state.country_code == "PH" ? true : false}
                                        value={this.state.email}
                                        placeholderTextColor={platform.placeholderTextColor}
                                        onChangeText={(txt) => this.setState({ email: removeEmojis(txt) })}
                                        {...testID('input_email')}
                                    />
                                </Item>
                                {this._showJournalTitle()}
                                <Button
                                    // {...testID('buttonSubmit')}
                                    accessibilityLabel="button_submit"
                                    style={styles.button} onPress={() => this.submitPress()} block>
                                    <Text textButton>SUBMIT</Text>
                                </Button>
                            </View>
                        </Card>
                    </View>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        padding: 10,
    },
    title: {
        fontFamily: 'Nunito-Bold',
        fontSize: 30,
        textAlign: 'center',
        marginTop: 30,
        marginBottom: 10,
        color: '#fff'
    },
    card: {
        flex: 0,
        paddingVertical: 24,
    },
    centerAlign: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    form: {
        paddingHorizontal: 20,
    },
    button: {
        height: 48,
        marginTop: 24
    },
    modalBadge: {
        width: 90,
        height: 90,
        marginBottom: 20,
        backgroundColor: '#3AE194',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50
    },
    modalImageCheck: {
        color: '#FFFFFF',
        fontSize: 35,
        textAlign: 'center'
    },
    modalText: {
        fontSize: 20,
        color: '#37474F',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center'
    },
    backgroundCompetence: {
        padding: 5,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginRight: 10,
        backgroundColor: '#F0F0F0',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundBtnAdd: {
        padding: 5,
        paddingHorizontal: 10,
        marginBottom: 5,
        marginRight: 10,
        borderRadius: 5,
    },
    textGrey: {
        fontSize: 14,
        color: '#6C6C6C'
    },
    textSpecialist: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'wrap',
        marginBottom: 12
    },
    tagIconX: {
        fontSize: 18,
        marginLeft: 10,
    },
    itemInput: {
        marginBottom: 24
    },
    textLabel: {
        marginBottom: 12
    },
});
