import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, BackHandler, ImageBackground, TouchableOpacity, Platform, ScrollView, FlatList } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Button, Icon, Text, Form, Item, Input, Label, Toast, Card, CardItem } from 'native-base';
import { Loader, ChecklistSpecialist } from './../../components';
import { validateEmail, STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import platform from '../../../theme/variables/d2dColor';
import guelogin from './../../libs/GueLogin';
const gueloginAuth = firebase.app('guelogin');
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { translate } from './../../libs/localize/LocalizeHelper'
import { SafeAreaView } from 'react-native';

export default class SelectSubSpecialist extends Component {

    state = {
        subspecialist: [],
        searchValue: '',
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        let { data } = this.props.navigation.state.params;
        this.initSubSpesialist(data.subspecialist)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    initSubSpesialist = (subspecialist) => {
        if (subspecialist != null && subspecialist.length > 0) {
            let pureSubSp = []

            if (subspecialist[0].id != 0) {
                let notSubSp = {
                    id: 0,
                    title: '',
                    description: translate('no_subspecialist'),
                    isChecked: true,
                }
                pureSubSp.push(notSubSp);
            }

            let isEmptySubSP = true;

            subspecialist.map(function (d, i) {
                if (d.id > 0 && d.isChecked == true) {
                    isEmptySubSP = false;
                }

                let subSp = {
                    id: d.id,
                    title: d.title,
                    description: d.description,
                    isChecked: d.isChecked
                }

                pureSubSp.push(subSp)
            })

            if (isEmptySubSP == false) {
                pureSubSp[0].isChecked = false;
            }

            this.setState({ subspecialist: pureSubSp })
        }
    }

    saveDataSubSp = (subSpList) => {
        let isCheckedSubSp = false;
        subSpList.map(function (d, i) {
            if (d.isChecked == true) {
                isCheckedSubSp = true;
            }
        })

        this.props.navigation.state.params.data.isChecked = isCheckedSubSp;
        this.props.navigation.state.params.data.subspecialist = subSpList;
        this.props.navigation.goBack();
        this.props.navigation.state.params.updateDataSp();
    }

    _renderItem = ({ item }) => {
        return (
            <ChecklistSpecialist data={item} action={this.onCheckedListener}
                unCheckAll={this.onCheckNotSubSelected}
                isSubSelected={this.isSubSelected} />
        )
    }

    isSubSelected = () => {
        if (this.state.subspecialist[0].isChecked == true) {
            let resultList = [];

            this.state.subspecialist.map(function (d, i) {
                let dataSub = {
                    id: d.id,
                    title: d.title,
                    description: d.description,
                    isChecked: d.id == 0 ? false : d.isChecked,
                }
                resultList.push(dataSub);
            });

            console.log('sub selected', resultList)
            this.setState({ subspecialist: resultList })

        }

    }

    onCheckNotSubSelected = () => {
        let resultList = [];

        this.state.subspecialist.map(function (d, i) {
            let dataSub = {
                id: d.id,
                title: d.title,
                description: d.description,
                isChecked: d.id == 0 ? true : false,
            }
            resultList.push(dataSub);
        });

        console.log('onUnchek', resultList)
        this.setState({ subspecialist: resultList })
    }

    checkMatchesSpList = (searchValue, spList) => {
        let resultList = [];
        spList.map(function (d, i) {
            if ((d.description.toLowerCase()).includes(searchValue.toLowerCase())) {
                resultList.push(d)
            }
        });

        return resultList;
    }


    renderNotFound = () => {
        let messageNotFound = "We are really sad we could not find anything";
        if (this.state.searchValue != '') {
            messageNotFound += ' for ' + this.state.searchValue;
        }

        return (
            <View style={styles.viewNotFound}>
                <Icon name="ios-search" style={styles.iconNotFound} />
                <Text style={styles.textNotFound}>{messageNotFound}</Text>
            </View>

        );
    }

    render() {
        let shownSubSpList = this.state.subspecialist;

        if (this.state.searchValue != '') {
            shownSubSpList = this.checkMatchesSpList(this.state.searchValue, this.state.subspecialist)
        }


        return (
            <Container style={styles.backgroundColor}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>

                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Select Sub Specialization</Title>
                        </Body>
                        <Right style={{ flex: 0.5, paddingRight: 10 }}>
                            <TouchableOpacity onPress={() => this.saveDataSubSp(this.state.subspecialist)}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 16,
                                    fontFamily: 'Nunito-Bold',
                                }}>Save</Text>
                            </TouchableOpacity>
                        </Right>
                    </Header>
                </SafeAreaView>

                <View style={styles.viewSearch}>
                    <Item onPress={() => null}
                        last style={styles.itemInput}>
                        <Input
                            value={this.state.searchValue}
                            autoCapitalize="none"
                            placeholder={'Search...'}
                            placeholderTextColor={platform.placeholderTextColor}
                            onChangeText={(txt) => this.setState({ searchValue: txt })} />
                        <TouchableOpacity onPress={() => this.setState({ searchValue: '' })}>
                            <Icon name={'ios-close-circle'} style={{ color: '#D7DEE6' }} />
                        </TouchableOpacity>
                    </Item>
                </View>
                {shownSubSpList.length <= 0 && this.renderNotFound()}
                <FlatList
                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                    data={shownSubSpList}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()} />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: 'white'
    },
    contentColor: {
        color: '#78849E'
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
    },
    touchBtnRegister: {
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: {
        width: '20%',
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    },
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
    },
    viewEmpty: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 25,
        marginTop: 10
    },
    textSpesialis: {
        color: '#78849E',
        margin: 10,
        fontSize: 16
    },
    itemInput: {
        flex: 1,
        backgroundColor: 'white',
    },
    textAdd: {
        color: '#0EA2DD',
        textAlign: 'right'
    },
    iconSpesialist: {
        color: '#78849E',
        marginHorizontal: 15,
        fontSize: 45
    },
    viewTextNoborder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewTextBorderTop: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#D7DEE6'
    },
    iconCancel: {
        color: '#D7DEE6',
        marginRight: 10,
    },
    viewSearch: {
        backgroundColor: '#D7DEE6',
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    }
});
