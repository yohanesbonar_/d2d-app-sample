import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { StatusBar, StyleSheet, TouchableOpacity, FlatList, BackHandler } from 'react-native';
import { Container, Header, Left, Body, Right, Title, View, Button, Icon, Text, Form, Item, Input, Label, Toast, Card, CardItem } from 'native-base';
import { Loader, ChecklistSpecialist } from './../../components';
import { testID } from './../../libs/Common';
import { getDataSpesializations } from './../../libs/NetworkUtility';
import platform from '../../../theme/variables/d2dColor';
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { SafeAreaView } from 'react-native';
import { getData, KEY_ASYNC_STORAGE } from '../../../src/utils';

export default class SelectSpecialist extends Component {

    state = {
        titleToolbar: 'Select Specializations',
        specialistList: [],
        searchValue: '',
        showLoader: false,
    }

    constructor(props) {
        super(props);
    }

    profileSpecialist = [];
    from = '';

    componentDidMount() {
        StatusBar.setHidden(false);
        // console.log(this.props.navigation.state.params)
        // this.setDummySpecilist();
        this.initProfileSptList();
        this.getDataMaster();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    initProfileSptList = () => {
        let { params } = this.props.navigation.state;
        console.log("params: ", params)
        if (params != null) {
            this.profileSpecialist = params.specialistList;
            this.from = params.from;

            if (this.from == 'SUBSCRIBTIONS') {
                getData(KEY_ASYNC_STORAGE.PROFILE).then(res => {
                    if (res.country_code == "ID") {
                        this.setState({ titleToolbar: 'Pilih Peminatan' })
                    }
                    else {
                        this.setState({ titleToolbar: 'Select Interest' })
                    }
                })
            }
        }
    }

    getDataMaster = async () => {
        let masterDataSp = [];


        this.setState({ showLoader: true })

        let params = {
            type: this.from,
            page: 1,
            limit: 100,
            keyword: '',
            nesting: true
        }

        try {
            let response = await getDataSpesializations(params);
            console.log("getDataSpesializations response: ", response)

            this.setState({ showLoader: false });

            if (response.isSuccess == true) {
                masterDataSp = response.docs
            } else {
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        } catch (error) {
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }

        if (masterDataSp != null && masterDataSp.length > 0) {
            if (this.profileSpecialist.length > 0) {
                this.intiChecklist(masterDataSp, this.profileSpecialist)
            } else {
                this.setState({ specialistList: masterDataSp })
            }
        }
    }


    intiChecklist = (masterSpList, checkedSpList) => {
        let resultList = [];
        masterSpList.map(function (dMaster, iMaster) {
            // let tempData = dMaster;


            let tempData = {
                id: dMaster.id,
                title: dMaster.title,
                description: dMaster.description,
                subspecialist: dMaster.subspecialist,
                isChecked: false
            }

            checkedSpList.map(function (dChecked, iChecked) {



                if (dMaster.id == dChecked.id) {
                    tempData.isChecked = true;

                    if (dChecked.subspecialist != null && dChecked.subspecialist.length > 0) {
                        if (tempData.subspecialist != null && tempData.subspecialist.length > 0) {
                            dChecked.subspecialist.map(function (dSub, iSub) {
                                dMaster.subspecialist.map(function (dMas, iMas) {
                                    if (dMas.id == dSub.id) {
                                        tempData.subspecialist[iMas].isChecked = true;
                                    }
                                })
                            })
                        }
                    }
                }
            })

            resultList.push(tempData);
        })

        this.setState({ specialistList: resultList })
    }

    setDummySpecilist = () => {
        let resultList = [];
        for (i = 1; i <= 10; i++) {

            dummySpList = [];
            for (a = 11; a <= 15; a++) {
                let dummySubSp = {
                    id: a,
                    title: 'Sub Specialist ' + a,
                    description: 'Specialist ' + a,
                    sp_id: i,
                    // isChecked : true
                };

                dummySpList.push(dummySubSp);
            }

            let dummySpecialist = {
                id: i,
                title: 'Specialist ' + i,
                description: 'Specialist ' + i,
                subspecialist: i < 5 ? [] : dummySpList,
                // isChecked : true
            };

            resultList.push(dummySpecialist);
        }

        return resultList;
    }

    _renderItem = ({ item }) => {
        return (
            <ChecklistSpecialist data={item} from={this.from}
                gotoSubSpesialis={this.gotoSubSpesialis}
                doCheckAll={this.onCheckAll}
                isSpSelected={this.isSpSelected}
            />
        )
    }


    isSpSelected = () => {
        if (this.state.specialistList[0].isChecked == true) {
            let resultList = [];

            this.state.specialistList.map(function (d, i) {
                let dataSub = {
                    id: d.id,
                    title: d.title,
                    description: d.description,
                    isChecked: d.id == 0 ? false : d.isChecked,
                }
                resultList.push(dataSub);
            });

            console.log('sub selected', resultList)
            this.setState({ specialistList: resultList })

        }

    }

    onCheckAll = (isChecked) => {
        // if subscribe only
        let resultList = [];

        this.state.specialistList.map(function (d, i) {
            let data = {
                id: d.id,
                title: d.title,
                description: d.description,
                subspecialist: d.subspecialist,
                isChecked: isChecked
            }

            resultList.push(data);
        })

        this.setState({ specialistList: resultList })
    }

    gotoSubSpesialis = (data) => {
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSubSpecialist',
            key: `gotoSelectSubSpecialist`,
            params: {
                data: data,
                updateDataSp: this.updateDataSp,
            }
            ,
        }));
    }

    updateDataSp = () => {
        let pureSp = [];

        console.log('before update', this.state.specialistList);

        this.state.specialistList.map(function (d, i) {
            let datasp = {
                id: d.id,
                title: d.title,
                description: d.description,
                subspecialist: d.subspecialist,
                isChecked: d.isChecked
            }

            pureSp.push(datasp)
        })

        console.log('after update', pureSp);

        this.setState({ specialistList: pureSp })
    }

    saveSpecialist = () => {
        if (this.from == 'SUBSCRIBTIONS') {
            AdjustTracker(AdjustTrackerConfig.Onboarding_Subs_Save);
        } else {
            AdjustTracker(AdjustTrackerConfig.Onboarding_Spec_Save);
        }

        let dataSpChecked = [];

        this.state.specialistList.map(function (dSp, iSp) {
            if (dSp.isChecked) {
                let dataSp = {
                    id: dSp.id,
                    title: dSp.title,
                    description: dSp.description,
                    isChecked: dSp.isChecked,
                    subspecialist: []
                }

                if (dSp.subspecialist != null && dSp.subspecialist.length > 0) {
                    let dataSubSpChecked = [];

                    dSp.subspecialist.map(function (dSubSp, iSubSp) {
                        if (dSubSp.isChecked == true) {
                            dataSubSpChecked.push(dSubSp);
                        }
                    });

                    dataSp.subspecialist = dataSubSpChecked;
                }

                dataSpChecked.push(dataSp)
            }
        });

        console.log('save spesialist', dataSpChecked);

        getData(KEY_ASYNC_STORAGE.PROFILE).then(res => {
            if (res.country_code == "ID") {
                let maxSelected = 2
                let postfixMessage = 'spesialis'
                if (this.props.navigation.state.params.isPeminatanStudi) {
                    maxSelected = 3
                    postfixMessage = 'peminatan'
                }
                if (this.from == "FILTER" && postfixMessage == 'spesialis') {
                    this.props.navigation.goBack()
                    this.props.navigation.state.params.saveDataSp(dataSpChecked);
                } else
                    if (dataSpChecked.length > maxSelected) {
                        Toast.show({ text: `Maksimal hanya ${maxSelected} ${postfixMessage}`, position: 'top', duration: 3000 })
                    }
                    else {
                        this.props.navigation.goBack()
                        this.props.navigation.state.params.saveDataSp(dataSpChecked);
                    }
            }
            else {
                this.props.navigation.goBack()
                this.props.navigation.state.params.saveDataSp(dataSpChecked);
            }
        })

    }

    checkMatchesSpList = (searchValue, spList) => {
        let resultList = [];

        spList.map(function (d, i) {
            if ((d.description.toLowerCase()).includes(searchValue.toLowerCase())) {
                resultList.push(d)
            }
        });

        return resultList;
    }

    renderNotFound = () => {
        let messageNotFound = "We are really sad we could not find anything";
        if (this.state.searchValue != '') {
            messageNotFound += ' for ' + this.state.searchValue;
        }

        return (
            <View style={styles.viewNotFound}>
                <Icon name="ios-search" style={styles.iconNotFound} />
                <Text style={styles.textNotFound}>{messageNotFound}</Text>
            </View>

        );
    }

    onFocus = () => {
        if (this.from == 'SUBSCRIBTIONS') {
            AdjustTracker(AdjustTrackerConfig.Onboarding_Subs_Search)
        } else {
            AdjustTracker(AdjustTrackerConfig.Onboarding_Spec_Search);
        }
    }


    render() {
        let shownSpList = this.state.specialistList;

        if (this.state.searchValue != '') {
            shownSpList = this.checkMatchesSpList(this.state.searchValue, this.state.specialistList)
        }

        console.log("shownSpList: ", shownSpList)
        return (
            <Container style={styles.backgroundColor}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>

                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>{this.state.titleToolbar}</Title>
                        </Body>
                        <Right style={{ flex: 0.5, paddingRight: 10 }}>
                            <TouchableOpacity onPress={() => this.saveSpecialist()}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 16,
                                    fontFamily: 'Nunito-Bold',
                                }}>Save</Text>
                            </TouchableOpacity>
                        </Right>
                    </Header>
                </SafeAreaView>

                <Loader visible={this.state.showLoader} />
                <View style={styles.viewSearch}>
                    <Item onPress={() => null}
                        last style={styles.itemInput}>
                        <Input
                            value={this.state.searchValue}
                            autoCapitalize="none"
                            placeholder={'Search...'}
                            placeholderTextColor={platform.placeholderTextColor}
                            onChangeText={(txt) => this.setState({ searchValue: txt })}
                            onFocus={() => this.onFocus()} />
                        <TouchableOpacity onPress={() => this.setState({ searchValue: '' })}>
                            <Icon name={'ios-close-circle'} style={{ color: '#D7DEE6' }} />
                        </TouchableOpacity>
                    </Item>
                </View>
                {this.state.showLoader == false && shownSpList.length <= 0 && this.renderNotFound()}
                <FlatList
                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                    data={shownSpList}
                    renderItem={this._renderItem}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    keyExtractor={(item, index) => index.toString()} />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    backgroundColor: {
        backgroundColor: 'white'
    },
    contentColor: {
        color: '#78849E'
    },
    btnNextRegister: {
        backgroundColor: '#F7F7FA',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
    },
    touchBtnRegister: {
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 15,
    },
    textNext: {
        fontFamily: 'Nunito-Bold',
        color: 'white',
        fontSize: 18
    },
    viewGrey: {
        backgroundColor: '#EAEAEA',
        height: 12,
        borderRadius: 15
    },
    viewGreen: {
        width: '20%',
        top: -12,
        backgroundColor: '#3AE194',
        height: 12,
        borderRadius: 15
    },
    textTitle: {
        color: '#78849E',
        fontSize: 30,
        fontFamily: 'Nunito-Bold',
    },
    viewEmpty: {
        justifyContent: 'flex-end',
        flex: 1,
        marginBottom: 25,
        marginTop: 10
    },
    textSpesialis: {
        color: '#78849E',
        margin: 10,
        fontSize: 16
    },
    itemInput: {
        flex: 1,
        backgroundColor: 'white',
    },
    textAdd: {
        color: '#0EA2DD',
        textAlign: 'right'
    },
    iconSpesialist: {
        color: '#78849E',
        marginHorizontal: 15,
        fontSize: 45
    },
    viewTextNoborder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewTextBorderTop: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: 2,
        borderTopColor: '#D7DEE6'
    },
    iconCancel: {
        color: '#D7DEE6',
        marginRight: 10,
    },
    viewSearch: {
        backgroundColor: '#D7DEE6',
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    }
});
