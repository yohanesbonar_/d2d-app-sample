import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Image, TouchableHighlight, Dimensions, Alert, BackHandler, SafeAreaView } from 'react-native';
import Pdf from 'react-native-pdf';
import { Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right, Toast } from 'native-base';
import { Loader } from './../components';
import platform from '../../theme/variables/d2dColor';
import { EnumFeedsCategory, downloadFile, testID } from './../libs/Common';
import { ParamAction, ParamContent, doActionUserActivity } from './../libs/NetworkUtility';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import { NavigationActions } from 'react-navigation';
import { AdjustTracker, AdjustTrackerConfig } from '../libs/AdjustTracker';
import { WebView } from 'react-native-webview';

let from = {
    DOWNLOAD: 'DOWNLOAD',
    BACK_BUTTON: 'BACK_BUTTON'
}

export default class PdfView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            numberOfPages: 0,
            showLoader: false,
            from: this.props.navigation.state.params ? this.props.navigation.state.params.from : '',
        }

        this.isCanDownload = true;
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        console.log(params)
        if (params != null) {
            // this.params = params
            if (params.category == EnumFeedsCategory.PDF_GUIDELINE &&
                params.id != null) {
                this.doViewGuideline(params.id);
            }
        }
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed()
            return true;
        });
        lor(this);
    }

    onBackPressed = () => {
        this.sendAdjust(from.BACK_BUTTON)
        this.props.navigation.goBack()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    sendAdjust = (action) => {
        const { params } = this.props.navigation.state;

        let category = ''
        if (params != null) {
            if (params.category == EnumFeedsCategory.PDF_GUIDELINE) {
                category = EnumFeedsCategory.PDF_GUIDELINE
            }
        }


        let token = ''
        switch (action) {
            case from.DOWNLOAD:
                if (this.state.from == 'LearningSpecialist') {
                    token = AdjustTrackerConfig.Learning_SpesialisOpen_ViewPDFDownload
                }
                else if (this.state.from == 'Feeds') {
                    if (category == EnumFeedsCategory.PDF_GUIDELINE) {
                        token = AdjustTrackerConfig.Feeds_Guideline_Download
                    }
                }
                break;
            case from.BACK_BUTTON:
                if (this.state.from == 'Feeds') {
                    if (category == EnumFeedsCategory.PDF_GUIDELINE) {
                        token = AdjustTrackerConfig.Feeds_Guideline_Close
                    }
                }

                break;
            default: break;
        }

        if (token != '') {
            AdjustTracker(token);
        }
    }

    componentWillUnMount() {
        rol();
    }

    async doViewGuideline(guidelineId) {
        let viewGuideline = await doActionUserActivity(ParamAction.VIEW, ParamContent.PDF_GUIDELINE, guidelineId);
    }

    async doDownloadFile(id, category, url) {
        let actionDownload = await doActionUserActivity(ParamAction.DOWNLOAD, category, id);

        // if (category != ParamContent.PDF_MATERIS && category != ParamContent.VIDEO_MATERIS) {
        if (category != ParamContent.PDF_MATERIS) {
            downloadFile(url);
        } else if (this.isCanDownload == true && actionDownload.isSuccess) {
            if (category == ParamContent.PDF_MATERIS) {
                this.showAlertSuccess();
            } else {
                Toast.show({ text: 'Download succesfull', position: 'top', duration: 3000 })
            }
        }

    }

    showAlertSuccess = () => {
        Alert.alert(
            'Information',
            'File successfully downloaded. You can access this file in Profile Menu',
            [
                // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.gotoProfileDownload() },
            ],
            { cancelable: true }
        );
    }

    // gotoProfile = () => {
    //     this.props.navigation.dispatch(NavigationActions.navigate({
    //         routeName: 'Profile',
    //         key: `profile-1234`,
    //         params : {isGotoDownload : true},
    //     }));
    // }

    gotoProfileDownload = () => {
        this.props.navigation.dispatch(
            NavigationActions.navigate({
                routeName: 'ProfileDownload',
                key: `pdfProfileDownload`
            }));
    }

    _renderPdf(attachment) {
        if (platform.platform == 'android') {
            const source = { uri: attachment, cache: false };
            return (
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages, filePath) => {
                        // this.state.page = numberOfPages;
                        console.log(numberOfPages)
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        // this.state.page = page;
                        if (page && numberOfPages)
                            Toast.show({ text: `Page : ${page}/${numberOfPages}`, position: 'bottom', duration: 1000 })
                    }}
                    onError={(error) => {
                        if (error.message != 'cancelled')
                            Toast.show({ text: error.message, position: 'top', duration: 3000 })
                    }}
                    style={{ flex: 1, height: hp('100%') - platform.toolbarHeight, width: wp('100%') }} />
            );
        } else {
            const jsCode = "window.postMessage(document.getElementByTagName('body').innerHTML)"
            return (
                <WebView
                    javaScriptEnabled={true}
                    injectedJavaScript={jsCode}
                    onMessage={event => console.log('Received: ', event.nativeEvent.data)}
                    source={{ uri: attachment }}
                    onLoadStart={this.onloadStart}
                    onLoadEnd={this.onloadEnd}
                    style={{ flex: 1, flexDirection: 'column', width: wp('100%'), backgroundColor: '#F3F3F3' }} />
            )
        }
    }

    onloadStart = () => {
        this.setState({ showLoader: true })
    }

    onloadEnd = () => {
        this.setState({ showLoader: false })
    }

    render() {
        const nav = this.props.navigation;
        const { params } = nav.state;

        if (typeof params.isCanDownload != 'undefined') {
            this.isCanDownload = params.isCanDownload;
        }

        let nameIconLeftHeader = typeof (params.type) != 'undefined' && params.type != null && (params.type == 'materi_posttest' || params.type == "paymentInstructions") ? 'md-arrow-back' : 'ios-close'
        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonClose')}
                                accessibilityLabel="button_close"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name={nameIconLeftHeader} style={{ color: '#fff', fontSize: nameIconLeftHeader == 'md-arrow-back' ? 25 : 32 }} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>{params.title}</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }}>
                            {this.isCanDownload == true && (<Button
                                // {...testID('buttonDownlaod')}
                                accessibilityLabel="button_download"
                                transparent onPress={() => this.doDownloadFile(params.id, params.category, params.attachment) + this.sendAdjust(from.DOWNLOAD)}>
                                <Icon name='md-download' nopadding style={styles.iconDownload} />
                            </Button>)}
                        </Right>
                    </Header>
                </SafeAreaView>
                <View style={styles.content}>
                    {this._renderPdf(params.attachment)}
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex: 1,
        height: hp('100%') - platform.toolbarHeight,
        width: wp('100%')
    },
    pagingWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        zIndex: 1000,
        minHeight: 40,
        width: Dimensions.get('window').width,
        padding: 10,
        backgroundColor: 'rgba(36, 41, 46, 0.7)',
    },
    paging: {
        color: '#fff',
        textAlign: 'center'
    },
    iconDownload: {
        flex: 0,
        color: '#FFFFFF',
        fontSize: 22
    },
});
