import React, { Component } from "react";
import firebase from "react-native-firebase";
import { NavigationActions, StackActions } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  ImageBackground,
  Image,
  Linking,
  Platform,
  Alert,
  SafeAreaView,
} from "react-native";
import { Grid, Col, Row, Text, Toast, Button } from "native-base";
import { Loader } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import guelogin from "./../../libs/GueLogin";
import {
  validateEmail,
  STORAGE_TABLE_NAME,
  openOtherApp,
} from "./../../libs/Common";
import {
  doLoginApi,
  ParamSetting,
  getSetting,
  getPopupShow,
  doCheckVersion,
} from "./../../libs/NetworkUtility";
import _ from "lodash";

const gueloginAuth = firebase.app("guelogin");
import { Swiper } from "./../../components";
import DeviceInfo from "react-native-device-info";
import { setI18nConfig } from "../../libs/localize/LocalizeHelper";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";
import { postLogin } from "../../../src/utils";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
export default class Splash extends Component {
  CURRENT_VERSION_ANDROID = DeviceInfo.getVersion();
  CURRENT_VERSION_IOS = DeviceInfo.getVersion();
  CURRENT_BUILD_NUMBER = DeviceInfo.getVersion();

  resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: "ChooseCountry" })],
  });

  constructor(props) {
    super(props);
    this.getDataCountryCode();
    this.state = {
      idToken: null,
      urlDeepLink: null,
      showOnBoarding: false,
      showUpdateApk: false,
      mandatoryUpdate: false,
      country_code: "",
    };
    this.UID = null;
    this.IS_LOGIN = "N";
    this.IS_REGISTER = "N";
    this.ONBOARDING = null;
    this.FIRST_LOGIN = "N";
  }

  componentDidMount = () => {
    this.doCheckVersion();
  };

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code != null) {
        this.setState({ country_code: profile.country_code });
      }
    });
  };

  async doCheckVersion() {
    let isLogin = await AsyncStorage.getItem(STORAGE_TABLE_NAME.IS_LOGIN);
    let uid = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);

    console.log("isLogin: ", isLogin);
    console.log("uid: ", uid);
    try {
      // let params = platform.platform == 'ios' ? ParamSetting.VERSION_IOS : ParamSetting.VERSION_ANDROID;
      // let response = await getSetting(params);
      // -- api v3 --
      let deviceBuildNumber = DeviceInfo.getBuildNumber();
      let deviceVersion = DeviceInfo.getVersion();
      let paramsData = {
        version: deviceVersion,
        build_number: deviceBuildNumber,
      };
      let response = await doCheckVersion(paramsData);

      console.log("docheckVersion apiv3 response", response.docs.version);
      console.log("doCheckVersion response: ", response);
      if (response.isSuccess === true && response.docs != null) {
        let currentVersion =
          platform.platform == "ios"
            ? this.CURRENT_VERSION_IOS
            : this.CURRENT_VERSION_ANDROID;
        console.log("doCheckVersion currentVersion: ", currentVersion);
        // console.log('doCheckVersion response.data[0].content: ', response.data[0].content)

        if (response.docs.show == true) {
          let mandatoryUpdate = response.docs.force_update;
          console.log("cek mandatory update", mandatoryUpdate);
          this.showDialogUpdate(response.docs.force_update);
          return;
        } else {
          this.initialize();
        }
      } else {
        this.initialize();
      }
    } catch (error) {
      this.initialize();
    }
  }

  async checkMandatoryUpdate() {
    let response = await getSetting(ParamSetting.MANDATORY_UPDATE);
    let result = false;

    if (response.isSuccess === true && response.data != null) {
      if (response.data[0].content === "true") {
        result = true;
      }
    }
    console.log("checkMandatoryUpdate result: ", result);
    return result;
  }

  showDialogUpdate(mandatoryUpdate) {
    this.setState({ showUpdateApk: true, mandatoryUpdate });
    //this.setState({ showUpdateApk: true, mandatoryUpdate: false })
  }

  doUpdateApps() {
    this.doCheckVersion();
    let url =
      platform.platform == "ios"
        ? "https://itunes.apple.com/id/app/d2d/id1411171451?mt=8"
        : "https://play.google.com/store/apps/details?id=com.d2d.android";
    openOtherApp(url, {});
  }

  async initialize() {
    this.setState({ showUpdateApk: false });
    try {
      let onBoarding = await AsyncStorage.getItem("ONBOARDING");
      // let onBoarding = "Y";

      if (onBoarding == "Y") {
        this.checkUser();
      } else {
        AdjustTracker(AdjustTrackerConfig.Onboarding_Start)
        this.setState({ showOnBoarding: true });
      }
    } catch (error) {
      console.log(error);
    }
  }

  checkUser = () => {
    this.createAuthListener();
  };

  createAuthListener = () => {
    authListener = gueloginAuth.auth().onAuthStateChanged((user) => {
      console.log("createAuthListener", user);

      if (user) {
        if (
          user.emailVerified === true ||
          (user.emailVerified === false &&
            (user.providerData[0].providerId == "facebook.com" ||
              user.providerData[0].providerId == "gmail"))
        ) {
          this.doLogin(user.email);
        } else {
          this.props.navigation.dispatch(this.resetAction);
        }
      } else {
        this.props.navigation.dispatch(this.resetAction);
      }
    });
    //remove listener
    authListener();
  };

  async checkModalPopup() {
    try {
      let user = gueloginAuth.auth().currentUser;
      if (!_.isEmpty(user)) {
        await AsyncStorage.setItem("showPopup", "true");
        let response = await getPopupShow();
        if (response.isSuccess == true) {
          let res = response.data;
          res.uid = user.uid;
          let popup = JSON.parse(await AsyncStorage.getItem("totalShowPopup"));

          if (res.id != popup.id || popup.uid != user.uid) {
            if (popup.uid == user.uid) res.total_show = popup.total_show;
            await AsyncStorage.setItem("totalShowPopup", JSON.stringify(res));
            console.log("set", res);
          }
        } else {
          console.log("gagal", res);
        }
      }
    } catch (error) {
      // Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 });
    }
  }

  setGotoHomeNavigation = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code != "ID") {
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Home" })],
        });
      } else {
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: "HomeNavigation" }),
          ],
        });
      }

      this.props.navigation.dispatch(this.resetAction);
    });
  };

  async doLogin(email) {
    console.log("doLogin splash");
    try {
      let user = gueloginAuth.auth().currentUser;
      let response = null;

      if (typeof user.email != "undefined") {
        if (this.state.country_code == "ID") {
          response = await postLogin(email);
        } else {
          response = await doLoginApi(email);
        }
      }
      console.log("doLogin splash response", response);
      if (
        (response.acknowledge && this.state.country_code == "ID") ||
        (response.isSuccess == true && this.state.country_code != "ID")
      ) {
        let uid = null;
        if (this.state.country_code == "ID") {
          console.warn(response.result.uid);
          uid = response.result.uid;
        } else {
          console.warn(response.data.uid);
          uid = response.data.uid;
        }
        console.log("response doLogin splash: ", response);

        if (typeof uid != "undefined") {
          let profile = null;
          if (this.state.country_code == "ID") {
            profile = response.result;
          } else {
            profile = response.data;
          }
          let jsonProfile = JSON.stringify(profile);
          let saveProfile = await AsyncStorage.setItem(
            STORAGE_TABLE_NAME.PROFILE,
            jsonProfile
          );

          setI18nConfig(); // set initial config

          prevUid = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);
          console.log("profile.uid ", profile.uid);
          console.log("prevUid ", prevUid);

          if (profile.uid != prevUid) {
            console.log("remove async storage SUBMIT_CME_QUIZ ");
            await AsyncStorage.removeItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ);
            await AsyncStorage.removeItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ);
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_EVENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_WEBINAR
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_PAY_NOW
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_JOIN
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.COACHMARK_EVENT_REPLAY
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED
            );
            await AsyncStorage.removeItem(STORAGE_TABLE_NAME.COUNTRY);
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CONTENT
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL
            );
            await AsyncStorage.removeItem(
              STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL
            );
          }

          let uid = await AsyncStorage.setItem(
            STORAGE_TABLE_NAME.UID,
            profile.uid
          );

          console.warn("login", "login api berhasil");

          if (profile.country_code == "PH") {
            this.setGotoHomeNavigation();
          } else {
            if (
              profile.subscription == null ||
              profile.subscription.length == 0
            ) {
              let country = await AsyncStorage.getItem(
                STORAGE_TABLE_NAME.COUNTRY
              );
              country = JSON.parse(country);
              console.log("country", country);

              this.resetAction = StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: "RegisterHello",
                    params: { profile, country },
                  }),
                ],
              });
              this.props.navigation.dispatch(this.resetAction);
            } else if (profile.subscription.length > 0) {
              this.setGotoHomeNavigation();
              //this.resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Home' })] });
              // this.checkModalPopup();
            }
          }
        } else {
          console.warn("login", "login api ga ada data");
          if (this.state.country_code == "ID") {
            if (response.message != "Login Success") {
              Toast.show({
                text: response.message,
                position: "top",
                duration: 3000,
              });
            }
          } else {
            if (response.message != "Success") {
              Toast.show({
                text: response.message,
                position: "top",
                duration: 3000,
              });
            }
          }

          await gueloginAuth.auth().signOut();
          this.resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: "ChooseCountry",
                params: user,
              }),
            ],
          });
          this.props.navigation.dispatch(this.resetAction);
          //this.resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login', params: user })] });
        }
      } else {
        if (this.state.country_code == "ID") {
          if (response.message != "Login Success") {
            Toast.show({
              text: response.message,
              position: "top",
              duration: 3000,
            });
          }
        } else {
          if (response.message != "Success") {
            Toast.show({
              text: response.message,
              position: "top",
              duration: 3000,
            });
          }
        }
        this.resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: "ChooseCountry",
              params: user,
            }),
          ],
        });
        this.props.navigation.dispatch(this.resetAction);
        // this.resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login', params: user })] });
      }

      //  this.props.navigation.dispatch(this.resetAction);
    } catch (error) {
      console.warn(error);
    }
  }

  gotoLoginWithClearData() {
    const user = gueloginAuth.auth().currentUser;

    if (user) {
      gueloginAuth
        .auth()
        .signOut()
        .then(() => {
          let setLogin = async () =>
            await AsyncStorage.setItem(STORAGE_TABLE_NAME.IS_LOGIN, "N");
          this.resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: "ChooseCountry",
                params: user,
              }),
            ],
          });
          //    this.resetAction = StackActions.reset({ index: 0, actions: [NavigationActions.navigate({ routeName: 'Login', params: user })] });
          this.props.navigation.dispatch(this.resetAction);
        })
        .catch((error) => {
          Toast.show({ text: error, position: "top", duration: 3000 });
        });
    }

    this.props.navigation.dispatch(this.resetAction);
  }

  async startApp() {
    let setOnBoard = await AsyncStorage.setItem("ONBOARDING", "Y");
    this.checkUser();
  }

  testCrash() {
    firebase.crashlytics().log("Test fajar!");
    firebase.crashlytics().crash();
  }

  render() {
    //this.testCrash();
    if (this.state.showOnBoarding == true) {
      return (
        <ImageBackground
          source={require("./../../assets/images/bg-splash.png")}
          style={{ width: platform.deviceWidth, height: platform.deviceHeight }}
        >
          <Swiper onPress={() => this.startApp()}>
            {/* First screen */}
            <View style={[styles.slide]}>
              <Image
                source={require("./../../assets/images/onboarding/CME-on-big-icon-onboarding.png")}
                style={[styles.image]}
                resizeMode="contain"
              />
              <Text style={styles.titleApp}>CME</Text>
            </View>
            {/* Second screen */}
            <View style={[styles.slide]}>
              <Image
                source={require("./../../assets/images/onboarding/webinar-onboarding.png")}
                style={[styles.image]}
                resizeMode="contain"
              />
              <Text style={styles.titleApp}>Webinar</Text>
            </View>
            {/* Third screen */}
            <View style={[styles.slide]}>
              <Image
                source={require("./../../assets/images/onboarding/request-journal-onboarding.png")}
                style={styles.image}
                resizeMode="contain"
              />
              <Text style={styles.titleApp}>Request Journal</Text>
            </View>
          </Swiper>
        </ImageBackground>
      );
    } else if (this.state.showUpdateApk == true) {
      return (
        <SafeAreaView style={{ flex: 1 }}>
          <View style={styles.viewUpdate}>
            <Text style={styles.titleUpdateApps}>Update D2D application ?</Text>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Image
                source={require("./../../assets/images/new_icon.png")}
                style={styles.imageD2D}
                resizeMode="contain"
              />
            </View>
            <Text style={styles.contentUpdateApps}>
              The latest version of the D2D application is available. For the
              best experience, please update your D2D application immediately
            </Text>
            <View style={styles.viewButtonUpdate}>
              {this.state.mandatoryUpdate === false && (
                <Button
                  bordered
                  success
                  style={styles.btnUpdate}
                  onPress={() => this.initialize()}
                >
                  <Text style={{ fontSize: 17, fontFamily: "Nunito-Bold" }}>
                    Later
                  </Text>
                </Button>
              )}
              <Button
                bordered
                success
                style={[styles.btnUpdate, { backgroundColor: "#3AE194" }]}
                onPress={() => this.doUpdateApps()}
              >
                <Text
                  style={{
                    fontSize: 17,
                    color: "#fff",
                    fontFamily: "Nunito-Bold",
                  }}
                >
                  Update
                </Text>
              </Button>
            </View>
          </View>
        </SafeAreaView>
      );
    } else {
      return (
        <ImageBackground
          source={require("./../../assets/images/bg-splash.png")}
          style={styles.container}
        >
          <Image
            source={require("./../../assets/images/logo.png")}
            style={styles.imageSplash}
            resizeMode="contain"
          />
          <Text style={styles.titleAppSplash}>Doctor to Doctor</Text>
        </ImageBackground>
      );
    }
  }
}

const styles = StyleSheet.create({
  // container styles
  container: {
    width: platform.deviceWidth,
    height: platform.deviceHeight,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },

  imageSplash: {
    width: 72,
    height: 72,
  },
  titleAppSplash: {
    fontFamily: "Nunito-Bold",
    fontSize: 20,
    textAlign: "center",
    color: "#fff",
    padding: 20,
  },
  viewUpdate: {
    paddingHorizontal: 20,
    //  paddingVertical: 30,
    backgroundColor: "#fff",
    // justifyContent: 'center',
    flex: 1,
  },
  imageD2D: {
    width: 100,
    height: 100,
  },
  titleUpdateApps: {
    fontFamily: "Nunito-Bold",
    fontSize: 20,
    marginVertical: 20,
  },
  contentUpdateApps: {
    fontFamily: "Nunito-Regular",
    fontSize: 16,
    marginVertical: 16,
  },
  viewButtonUpdate: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 15,
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
  },
  btnUpdate: {
    marginHorizontal: 10,
    flex: 1,
    justifyContent: "center",
  },
  // Slide styles
  slide: {
    flex: 1, // Take up all screen
    justifyContent: "center", // Center vertically
    alignItems: "center", // Center horizontally
  },
  image: {
    width: 120,
    height: 95,
  },
  titleApp: {
    fontFamily: "Roboto-Bold",
    fontSize: 24,
    textAlign: "center",
    color: "#fff",
    marginTop: 32,
  },
});
