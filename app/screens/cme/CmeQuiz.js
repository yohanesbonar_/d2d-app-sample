import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  FlatList,
  Modal,
  Alert,
  BackHandler,
  AppState,
  TouchableOpacity,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Content,
  Card,
  CardItem,
  Toast,
  Row,
  Col,
} from "native-base";
import { CardItemQuiz } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import {
  getQuestionList,
  submitAnswerQuiz,
  getPretestWebinar,
  getPosttestWebinar,
  submitPretestPostest,
} from "./../../libs/NetworkUtility";
import { NavigationActions } from "react-navigation";
import { Loader, Popup } from "./../../components";
import {
  STORAGE_TABLE_NAME,
  testID,
  checkNpaIdiEmpty,
  sendTopicCertificateWebinarSubmit,
  sendTopicCertificate,
} from "./../../libs/Common";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import ProgressBarAnimated from "./../../libs/AnimatedProgressBar";
import _ from "lodash";
import { translate } from "../../libs/localize/LocalizeHelper";
import { SafeAreaView } from "react-native";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";

let from = {
  SUBMIT: "SUBMIT",
  OK: "OK",
};

export default class CmeQuiz extends Component {
  countTimer = null;
  timestampBackgroundInSeconds = 0;
  timestampForegroundInSeconds = 0;

  state = {
    appState: AppState.currentState,
    questionList: [],
    showResult: false,
    resultSuccess: false,
    isTrueQuiz: false,
    messageSubmitQuiz: "",
    certificateCme: null,
    showLoader: false,
    from: this.props.navigation.state.params
      ? this.props.navigation.state.params.from
      : "",
    result_time: "00:00",
    countAnsweredQuestion: 0,
    valuePerStep: 0,
    currentValueProgressExam: 0,
    isButtonSubmitDisabled:
      this.props.navigation.state.params.from == "Webinar" ||
        this.props.navigation.state.params.from == "Feeds"
        ? true
        : false,
    dataWebinarDetail: null,
    typePretestPosttest: null,
    isShowMateri: false,
    idAttempt: null,
    materiPostTest: null,
    flagToMateri: false,
  };
  dataAnswer = [];
  totalSeconds = 0;
  countTimer = null;
  quizData = null;
  isNpaIdiEmpty = true;

  constructor(props) {
    super(props);
    this.state = this.state;
    this.closeScreen = this.closeScreen.bind(this); //handle props navigation undefined
  }

  showPopupWarning = () => {
    if (this.popup) {
      this.popup.togglePopup();
    }
  };

  countUpTimer = () => {
    ++this.totalSeconds;
    let hour = Math.floor(this.totalSeconds / 3600);
    let minute = Math.floor((this.totalSeconds - hour * 3600) / 60);
    let seconds = this.totalSeconds - (hour * 3600 + minute * 60);
    if (hour < 10) hour = "0" + hour;
    if (minute < 10) minute = "0" + minute;
    if (seconds < 10) seconds = "0" + seconds;

    this.setState({
      result_time: minute + ":" + seconds,
    });
    console.log("countUpTimer totalSeconds: ", this.totalSeconds);
  };

  componentDidMount() {
    StatusBar.setHidden(false);
    this.didiFocusListener = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        if (!this.state.flagToMateri) {
          this.initData();
        } else {
          this.setState({
            flagToMateri: false,
          });
        }
      }
    );

    AppState.addEventListener("change", this._handleAppStateChange);

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  _handleAppStateChange = (nextAppState) => {
    const { appState } = this.state;
    console.log("_handleAppStateChange appState: ", appState);
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground!");
      this.timestampForegroundInSeconds = Date.parse(new Date()) / 1000;
      console.log(
        "timestampForegroundInSeconds ",
        this.timestampForegroundInSeconds
      );
      //  if (this.timestampForegroundInSeconds != 0 && this.timestampBackgroundInSeconds != 0) {
      let diffTimestamp =
        this.timestampForegroundInSeconds - this.timestampBackgroundInSeconds;
      console.log("diffTimestamp ", diffTimestamp);
      console.log("totalSeconds ", this.totalSeconds);

      if (this.totalSeconds != 0) {
        this.totalSeconds = this.totalSeconds + diffTimestamp;
        this.countTimer = setInterval(this.countUpTimer, 1000);
      } else {
        clearInterval(this.countTimer);
      }
    } else {
      this.timestampBackgroundInSeconds = Date.parse(new Date()) / 1000;
      console.log(
        "timestampBackgroundInSeconds ",
        this.timestampBackgroundInSeconds
      );
      clearInterval(this.countTimer);
    }

    this.setState({ appState: nextAppState });
  };

  onBackPressed = () =>
    this.state.from == "Webinar" || this.state.from == "Feeds"
      ? this.showPopupWarning()
      : this.props.navigation.goBack();

  closeScreen() {
    this.deletedLocalAnswer();

    if (this.state.typePretestPosttest == "pretest") {
      getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
        if (profile.country_code == "ID") {
          this.props.navigation.pop(2);
        } else {
          this.props.navigation.popToTop();
        }
      });

    } else {
      this.props.navigation.goBack();
    }
  }

  clearStatePrePostest = async () => {
    await this.setState({
      result_time: "00:00",
      countAnsweredQuestion: 0,
      valuePerStep: 0,
      currentValueProgressExam: 0,
      isButtonSubmitDisabled: true,
    });
  };

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.SUBMIT:
        if (this.state.from == "Cme")
          token = AdjustTrackerConfig.CME_SKP_Submit;
        break;
      case from.OK:
        if (this.state.from == "Cme") token = AdjustTrackerConfig.CME_ADD_OK;
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  componentWillUnmount() {
    this.backHandler.remove();
    this.didiFocusListener.remove();
    AppState.removeEventListener("change", this._handleAppStateChange);
    clearInterval(this.countTimer);
  }

  saveProgressTime = async () => {
    await AsyncStorage.setItem(
      STORAGE_TABLE_NAME.PROGRESS_TIME_PREPOSTTEST,
      this.totalSeconds.toString()
    );
  };

  async initData() {
    let { params } = this.props.navigation.state;
    console.log("initData params: ", params);
    if (params.id == undefined) {
      params.id = params.quiz_id;
    }
    if (
      params.done == "true" ||
      params.is_done == 1 ||
      params.from == "CmeHistory" ||
      params.from == "pushNotifCertificate"
    ) {
      this.setState({
        showResult: true,
        isTrueQuiz: true,
        certificateCme: params.certificate,
        messageSubmitQuiz: `You have successfully taken this quiz and get ${params.skp
          } SKP`,
      });
    }

    this.quizData = params;

    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    this.isNpaIdiEmpty = checkNpaIdiEmpty(profile);

    this.setState({
      questionList: [],
    });

    let dataLocal = await this.getLocalAnswer();
    if (this.state.from == "Webinar" || this.state.from == "Feeds") {
      this.clearStatePrePostest();
      this.setState({
        typePretestPosttest: params.typePretestPosttest,
        dataWebinarDetail: this.props.navigation.state.params,
      });

      this.deletedLocalAnswer();
      this.getQestionPretestPostest(params.id);
    } else {
      if (dataLocal != null && dataLocal.id == params.id) {
        console.log("dari local");
        this.setState({
          questionList: dataLocal.answerList,
        });

        this.dataAnswer = dataLocal.answerList;
      } else {
        //deleted local db
        this.deletedLocalAnswer();
        this.getDataQuestions(params.id);
      }
    }
  }

  getQestionPretestPostest = async (id) => {
    this.setState({ showLoader: true });

    try {
      let response = "";
      if (this.state.typePretestPosttest == "pretest") {
        response = await getPretestWebinar(id);
      } else if (this.state.typePretestPosttest == "posttest") {
        response = await getPosttestWebinar(id);
      }

      console.log("getQestionPretestPostest response: ", response);
      if (response.isSuccess == true) {
        this.countTimer = setInterval(this.countUpTimer, 1000);

        this.setState({
          questionList: this.initEmptyAnswer(response.docs.questions),
          //valuePerStep: 100 / response.data.questions.length
          valuePerStep: Math.ceil(100 / response.docs.questions.length),
          idAttempt: response.docs.id_attempt,
          materiPostTest: response.docs.detail.materi,
          isShowMateri: _.isEmpty(response.docs.detail.materi) ? false : true,
        });

        this.dataAnswer = this.initEmptyAnswer(response.docs.questions);
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }

      this.setState({ showLoader: false });
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({
        text: "Something went wrong!, " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  async getDataQuestions(quizId) {
    this.setState({ showLoader: true });

    try {
      let response = await getQuestionList(quizId);

      if (response.isSuccess == true) {
        this.setState({
          // questionList: response.data,this.initEmptyAnswer(response.data)
          questionList: this.initEmptyAnswer(response.data),
        });

        this.dataAnswer = this.initEmptyAnswer(response.data);
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }

      this.setState({ showLoader: false });
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({
        text: "Something went wrong!, " + error,
        position: "top",
        duration: 3000,
      });
    }
  }

  async getLocalAnswer() {
    let result = null;

    let jsonAnswer = await AsyncStorage.getItem(STORAGE_TABLE_NAME.QUIZ_ANSWER);

    if (jsonAnswer != null) {
      let objJsonAnswer = await JSON.parse(jsonAnswer);
      result = objJsonAnswer;
    }

    return result;
  }

  deletedLocalAnswer = async () => {
    let deleted = await AsyncStorage.setItem(
      STORAGE_TABLE_NAME.QUIZ_ANSWER,
      ""
    );
  };

  doSaveQuizAnswer = async () => {
    if (
      this.quizData != null &&
      this.dataAnswer != null &&
      this.dataAnswer.length > 0
    ) {
      let dataQuiz = {
        id: this.quizData.id,
        answerList: this.dataAnswer,
      };
      let jsonAnswer = await JSON.stringify(dataQuiz);
      let saveQuizAnswer = await AsyncStorage.setItem(
        STORAGE_TABLE_NAME.QUIZ_ANSWER,
        jsonAnswer
      );
    }
  };

  initEmptyAnswer = (questionList) => {
    let result = [];
    if (questionList != null && questionList.length > 0) {
      questionList.map(function (d, i) {
        result.push({
          id: d.id,
          question: d.question,
          answer: "",
          choices: d.choices,
        });
      });
    }

    return result;
  };

  inputAnswerData = (questionData) => {
    if (this.dataAnswer != null && this.dataAnswer.length > 0) {
      this.dataAnswer.map(function (d, i) {
        if (d.id == questionData.id) {
          d.answer = questionData.answer;
        }
      });

      //handle answer prepostest
      if (this.state.from == "Webinar" || this.state.from == "Feeds") {
        let answeredQuestion = this.dataAnswer.filter(function (element) {
          console.log("element: ", element);
          return element.answer != "" || typeof element.answer == "number";
        });

        let curentProgress = Math.ceil(
          this.state.valuePerStep * answeredQuestion.length
        );
        // console.log('answeredQuestion: ', answeredQuestion.length)
        // console.log('curentProgress: ', curentProgress)
        // console.log('valuePerStep: ', this.state.valuePerStep)
        // console.log('currentValueProgressExam: ', this.state.currentValueProgressExam)
        // console.log('questionList: ', this.state.questionList)

        if (this.state.countAnsweredQuestion < this.state.questionList.length) {
          this.setState({
            countAnsweredQuestion: answeredQuestion.length,
            currentValueProgressExam:
              curentProgress > 100 ? 100 : curentProgress,
          });
        }

        if (answeredQuestion.length == this.state.questionList.length) {
          this.setState({
            isButtonSubmitDisabled: false,
          });
        }
      }

      this.doSaveQuizAnswer();
    }
  };

  onPressSubmitQuiz = () => {
    if (this.state.from == "Webinar" || this.state.from == "Feeds") {
      this.submitPrePostest();
    } else {
      this.submitQuiz();
    }
  };

  submitPrePostest = async () => {
    this.setState({ showLoader: true });

    // Cancel the timer when you are done with it

    let paramAnswer = [];
    console.log("dataAnswer: ", this.dataAnswer);
    this.dataAnswer.map(function (value, index) {
      paramAnswer.push({
        id: value.id,
        answer: value.answer,
      });
    });

    let requestParams = {
      type: this.state.typePretestPosttest,
      webinarId: this.state.dataWebinarDetail.id,
      id_attempt: this.state.idAttempt,
      answers: JSON.stringify(paramAnswer),
      time: this.state.result_time,
    };

    console.log(requestParams);
    try {
      let response = await submitPretestPostest(requestParams);
      console.log(response.docs);
      console.log(this.state.dataWebinarDetail);
      if (response.isSuccess == true) {
        clearInterval(this.countTimer);
        this.deletedLocalAnswer();
        let dataParams = {
          ...this.state.dataWebinarDetail,
          result_time: this.state.result_time,
          ...response.docs,
        };

        this.gotoResultPrePostTest(dataParams);
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }

      this.setState({ showLoader: false });
    } catch (error) {
      console.log(error);
      this.setState({ showLoader: false });
      Toast.show({
        text: "Something went wrong!, " + error,
        position: "top",
        duration: 3000,
      });
    }
  };

  async submitQuiz() {
    if (this.dataAnswer != null && this.dataAnswer.length > 0) {
      let uid = await AsyncStorage.getItem("UID");

      if (this.quizData != null && uid != null) {
        console.log(JSON.stringify(this.dataAnswer));
        this.setState({ showLoader: true });

        let requestParams = {
          quizid: this.quizData.id,
          uid: uid,
          skp: this.quizData.skp,
          source: this.quizData.source,
          answers: JSON.stringify(this.dataAnswer),
        };

        console.log(requestParams);

        let response = await submitAnswerQuiz(requestParams);
        console.log("submitAnswerQuiz response: ", response);

        if (
          (this.state.from == "Cme" || this.state.from == "SearchKonten") &&
          response.isSuccess
        ) {
          sendTopicCertificate("cme", this.quizData.id);
          if (this.isNpaIdiEmpty) {
            let dataSubmitCmeQuiz = await AsyncStorage.getItem(
              STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ
            );
            dataSubmitCmeQuiz = JSON.parse(dataSubmitCmeQuiz);
            console.log("dataSubmitCmeQuiz: ", dataSubmitCmeQuiz);

            await AsyncStorage.setItem(
              STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ,
              JSON.stringify(requestParams)
            );
          }
        }

        this.deletedLocalAnswer();

        this.setState({
          showResult: true,
          messageSubmitQuiz: response.message,
          isTrueQuiz: response.isSuccess,
          certificateCme: response.isSuccess ? response.data.certificate : null,
          showLoader: false,
        });
      } else {
        Toast.show({
          text: "Something went wrong!",
          position: "top",
          duration: 3000,
        });
      }
    } else {
      Toast.show({
        text: "Please answer all questions",
        position: "top",
        duration: 3000,
      });
    }
  }

  goToCme() {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.props.navigation.pop(2);
      } else {
        this.props.navigation.popToTop();
      }
    });
  }

  goToCmeCertificate() {
    this.setState({ showResult: false });
    this.props.navigation.navigate("CmeCertificate");
  }

  gotoProfile = (paramCertificate) => {
    if (paramCertificate.from != "pushNotifCertificate") {
      if (paramCertificate.from != "SearchKonten") {
        paramCertificate.from = "CmeQuiz";
      }
    }
    paramCertificate.resubmitQuiz = true;
    paramCertificate.done = "true";

    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      let routeName = "Profile";
      if (profile.country_code == "ID") {
        routeName = "ChangeProfile";
      }
      this.props.navigation.dispatch(
        NavigationActions.navigate({
          routeName: routeName,
          params: paramCertificate,
        })
      );
    });

    // this.props.navigation.navigate('Profile', {
    //     dataObat: item
    // })
  };

  gotoResultPrePostTest = async (params) => {
    this.totalSeconds = 0;
    console.log(params);
    let maxAttempt =
      this.state.typePretestPosttest == "pretest"
        ? params.pretest_max_attempt
        : params.posttest_max_attempt;

    if (this.state.typePretestPosttest == "posttest" && params.is_pass) {
      sendTopicCertificateWebinarSubmit(this.state.dataWebinarDetail.slug_hash);
    }

    if (params.is_pass || params.count_attempt == maxAttempt) {
      this.props.navigation.replace("TestResult", {
        //dataResult: params,
        ...params,
      });
    } else {
      this.props.navigation.navigate("TestResult", {
        //dataResult: params,
        ...params,
      });
    }
  };

  renderItem = (item, index) => {
    let from = "";
    if (this.state.from == "Webinar" || this.state.from == "Feeds") {
      from = "Webinar";
    }

    return (
      <CardItemQuiz
        from={from}
        data={item}
        length={this.state.questionList.length}
        index={index}
        resultChoices={this.inputAnswerData}
      />
    );
  };

  renderMenuSeeMateri = () => { };
  // showPdf = () => {
  //     let { params } = this.props.navigation.state;

  //     if (params != null) {
  //         let dataPdf = {
  //             title: params.title,
  //             pdf: params.filename,
  //         };

  //         this.props.navigation.dispatch(NavigationActions.navigate({
  //             routeName: 'GeneralPdfView',
  //             params: dataPdf, key: `showpdf-${params.id}`
  //         }));
  //     }
  // }

  onClickBtnOk = () => {
    this.setState({ showResult: false });
    let { params } = this.props.navigation.state;
    console.log("onClickBtnOk params: ", params);
    if (
      (this.state.isTrueQuiz && this.state.certificateCme != null) ||
      params.from == "pushNotifCertificate"
    ) {
      if (this.state.isTrueQuiz && this.state.certificateCme != null) {
        if (params != null) {
          params.certificate = this.state.certificateCme;

          if (this.isNpaIdiEmpty == true) {
            if (params.from == "SearchKonten") {
              //flag for back button in certifcate
              params.popCounter = 3;
            }
            //fill npa idi
            this.gotoProfile(params);
          } else if (this.isNpaIdiEmpty == false) {
            if (params.from == "SearchKonten") {
              //flag for back button in certifcate
              params.popCounter = 3;
            }
            // show Certificate
            this.props.navigation.dispatch(
              NavigationActions.navigate({
                routeName: "CmeCertificate",
                params: params,
                key: `cmeCertificate-${this.quizData.id}`,
              })
            );
          }
        }
      } else {
        if (this.state.from == "Webinar" || this.state.from == "Feeds") {
          this.deletedLocalAnswer();
          this.props.navigation.goBack();
        } else {
          this.goToCme();
        }
      }
    } else {
      this.goToCme();
    }
  };

  _modalResult() {
    let messageWarningNpaIdi =
      this.isNpaIdiEmpty == true && this.state.isTrueQuiz == true
        ? ". Please complete your profile page and Medical ID to get your certificate."
        : "";

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showResult}
        onRequestClose={() => {
          this.setState({ showResult: false });
          this.goToCme();
        }}
      >
        <View
          style={{
            backgroundColor: "#00000080",
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card style={styles.cardModalQuiz}>
            <CardItem style={{ justifyContent: "center", flex: 0 }}>
              <View style={styles.viewModalQuiz}>
                <View
                  style={[
                    styles.modalBadge,
                    {
                      backgroundColor: this.state.isTrueQuiz
                        ? platform.brandSuccess
                        : platform.brandDanger,
                    },
                  ]}
                >
                  <Icon
                    style={styles.modalImageCheck}
                    name={this.state.isTrueQuiz ? "md-checkmark" : "md-close"}
                  />
                </View>
                <Text style={styles.modalTextTitle}>
                  {this.state.isTrueQuiz ? "Success" : "Failed"}
                </Text>
                <Text style={styles.modalText}>
                  {this.state.messageSubmitQuiz + messageWarningNpaIdi}
                </Text>

                {/* <Button success block onPress={() => this.goToCmeCertificate()}><Text style={styles.btnOK}>OK</Text></Button> */}
                <Button
                  // {...testID('buttonOK')}
                  accessibilityLabel="button_ok"
                  style={styles.btnOK}
                  success
                  block
                  onPress={() => this.onClickBtnOk() + this.sendAdjust(from.OK)}
                >
                  <Text style={styles.textBtnOK}>OK</Text>
                </Button>
              </View>
            </CardItem>
          </Card>
        </View>
      </Modal>
    );
  }

  renderProgress = () => {
    return (
      <View style={styles.containerProgress}>
        <Row
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            flex: 0,
            marginHorizontal: 20,
            marginBottom: 10,
          }}
        >
          <Text
            style={{
              color: "#CE174E",
              fontSize: 12,
              fontFamily: "Nunito-Regular",
            }}
          >
            {translate("message_progress_answer", {
              answeredQuestion: this.state.countAnsweredQuestion,
              totalQuestion: this.state.questionList.length,
            })}
          </Text>

          <Text
            style={{
              color: "#CE174E",
              fontSize: 12,
              fontFamily: "Nunito-Regular",
            }}
          >
            {this.state.result_time}
          </Text>
        </Row>
        <ProgressBarAnimated
          width={platform.deviceWidth - 80}
          height={6}
          marginHorizontal={20}
          value={this.state.currentValueProgressExam}
          maxValue={100}
          backgroundColorOnComplete="#CE174E"
          backgroundColor="#CE174E"
          underlyingColor="#D63A6933"
          borderColor="transparent"
        />
      </View>
    );
  };

  gotoPostTestMateri = () => {
    let params = {
      id: this.state.materiPostTest.id,
      attachment: this.state.materiPostTest.url,
      isCanDownload: false,
      category: "pdf_materi",
      type: "materi_posttest",
    };
    this.setState({
      flagToMateri: true,
    });
    const showMateri = {
      type: "Navigate",
      routeName: "PdfView",
      params: params,
    };
    this.props.navigation.navigate(showMateri);
  };

  render() {
    let title = "Questions";
    if (this.state.from == "Webinar" || this.state.from == "Feeds") {
      title = "";
      if (this.state.typePretestPosttest == "pretest") {
        title = "Pretest";
      } else {
        title = "Posttest";
      }
    }
    return (
      <Container style={{ backgroundColor: "#F3F3F3" }}>
        {this._modalResult()}
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header nopadding>
            <Left style={{ flex: 0.75 }}>
              <Button
                // {...testID('buttonBack')}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={styles.toolbarIcon} />
              </Button>
            </Left>
            <Body
              style={{
                flex: 1,
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Title>{title}</Title>
            </Body>
            <Right style={{ flex: 0.75 }}>
              {(this.state.from == "Webinar" || this.state.from == "Feeds") &&
                this.state.isShowMateri && (
                  <TouchableOpacity
                    {...testID("button_materi")}
                    style={{ backgroundColor: "#F7F7FA", borderRadius: 12 }}
                    transparent
                    onPress={() => {
                      this.gotoPostTestMateri();
                    }}
                  >
                    <Text style={styles.textMateriPosttest}>
                      {translate("button_lihat_materi")}
                    </Text>
                  </TouchableOpacity>
                )}
            </Right>
          </Header>
        </SafeAreaView>

        <View
          style={{
            flex: 1,
          }}
        >
          {(this.state.from == "Webinar" || this.state.from == "Feeds") &&
            this.state.questionList.length > 0 &&
            this.renderProgress()}
          <FlatList
            contentContainerStyle={{ padding: 10, paddingBottom: 55 }}
            scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
            data={this.state.questionList}
            // renderItem={this._renderItem}
            renderItem={({ item, index }) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />

          <Popup
            ref={(ref) => {
              this.popup = ref;
            }}
            closeScreen={this.closeScreen}
            title={translate("webinar_title_popup_pretest_posttest_cancel")}
            message={translate("webinar_message_popup_pretest_posttest_cancel")}
          />
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            width: "100%",
            position: "absolute",
            bottom: 0,
            backgroundColor: "#fff",
          }}
        >
          {/* <Button block success onPress={() => nav.goBack()} style={{ flex: 1, height: 55, borderRadius: 0, backgroundColor: '#9B9B9B' }}>
                        <Text nopadding style={{ color: '#fff' }}>BATAL</Text>
                    </Button> */}
          <Button
            // {...testID('buttonSubmit')}
            disabled={this.state.isButtonSubmitDisabled}
            accessibilityLabel="button_submit"
            block
            success
            onPress={() =>
              this.onPressSubmitQuiz() + this.sendAdjust(from.SUBMIT)
            }
            style={{
              flex: 1,
              height: 55,
              borderRadius: 0,
              backgroundColor: this.state.isButtonSubmitDisabled
                ? "#7DB099"
                : platform.brandSuccess,
            }}
          >
            <Text nopadding style={{ color: "#fff", fontWeight: "bold" }}>
              SUBMIT
            </Text>
          </Button>
        </View>

        <Loader visible={this.state.showLoader} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerBody: {
    justifyContent: "center",
    alignItems: "center",
  },
  modalBadge: {
    width: 90,
    height: 90,
    paddingTop: 5,
    marginTop: 20,
    marginBottom: 10,
    backgroundColor: "#3AE194",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
  },
  modalImageCheck: {
    color: "#FFFFFF",
    fontSize: 50,
    textAlign: "center",
  },
  modalTextTitle: {
    fontSize: 35,
    color: "#37474F",
    fontFamily: "Nunito-Black",
    textAlign: "center",
    marginBottom: 10,
  },
  cardModalQuiz: {
    flex: 0,
    marginTop: 22,
    width: platform.deviceWidth - 70,
  },
  viewModalQuiz: {
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    flex: 1,
  },
  modalText: {
    fontSize: 18,
    color: "#37474F",
    fontFamily: "Nunito-SemiBold",
    textAlign: "center",
    marginBottom: 10,
  },
  textBtnOK: {
    fontFamily: "Nunito-Black",
    fontSize: 16,
  },
  btnOK: {
    marginTop: 10,
    marginHorizontal: 5,
  },
  containerProgress: {
    marginTop: 20,
    marginBottom: 10,
    paddingVertical: 15,
    backgroundColor: "white",
    marginHorizontal: 16,
    borderRadius: 12,
    shadowColor: "#455B6314",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 4,
    zIndex: 1,
    flex: 0,
  },
  toolbarIcon: {
    color: "#FFFFFF",
    fontSize: 25,
  },
  textMateriPosttest: {
    fontFamily: "Nunito-Bold",
    fontSize: 14,
    marginVertical: 4,
    marginHorizontal: 8,
    color: "#CE174E",
  },
});
