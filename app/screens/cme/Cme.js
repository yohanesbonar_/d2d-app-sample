import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  Animated,
  Modal,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Content,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Body,
  Right,
  Tabs,
  Tab,
  ScrollableTab,
  TabHeading,
  Toast,
  Card,
  CardItem,
  Col,
} from "native-base";
import { Loader, CardItemWebinar, CardItemCme } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import {
  getQuizList,
  ValueTypeQuiz,
  resetSkp,
} from "./../../libs/NetworkUtility";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import { NavigationActions } from "react-navigation";
import { testID, STORAGE_TABLE_NAME } from "./../../libs/Common";
import _ from "lodash";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import AsyncStorage from "@react-native-community/async-storage";
import { Fragment } from "react";
import { ArrowBackButton } from "../../../src/components/atoms";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";

let from = {
  Reset: "Reset",
  Reset_Now: "Reset_Now",
};

export default class CME extends Component {
  PAGE_NEW = "new";
  typeDataChanged = null;
  pageNew = 0;
  uid = null;

  state = {
    animatedValue: new Animated.Value(0),
    cmeNew: [],
    isFetching: false,
    isFetchingNew: false,
    isSuccess: false,
    isFailed: false,
    isEmptyDataNew: false,
    showDialogSkp: false,
    skp: 0,
    isInternationalMode: false,
    backButtonAvailable: false,
  };

  constructor(props) {
    super(props);
    this.getDataCountryCode();
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        this.getData(false);
      }
    );
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.setState({ backButtonAvailable: true });
      } else {
        this.setState({ backButtonAvailable: false });
      }
    });
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  setMenuRef = (ref) => {
    this.menu = ref;
  };

  async getData(isDefaultEmpty) {
    let dataProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let countryCode = JSON.parse(dataProfile).country_code;
    if (countryCode != "ID") {
      this.setState({
        isInternationalMode: true,
      });
    }
    this.onRefreshNew(isDefaultEmpty);
  }

  onRefreshNew(isDefaultEmpty) {
    if (isDefaultEmpty) {
      this.setState({
        cmeNew: [],
      });
    }
    this.pageNew = 1;
    this.setState({ isFetchingNew: true, isEmptyDataNew: false });
    this.getCmeNew(this.pageNew);
  }

  async getCmeNew(page) {
    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await getQuizList(page, ValueTypeQuiz.ALL);

      if (response.isSuccess == true) {
        let skp =
          response.data.skp != null && response.data.skp.skp != null
            ? response.data.skp.skp
            : 0;
        this.setState({
          skp: skp,
          cmeNew:
            page == 1
              ? [...response.data.quiz]
              : [...this.state.cmeNew, ...response.data.quiz],
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingNew: false,
          isEmptyDataNew:
            response.data != null && response.data.quiz.length > 0
              ? false
              : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: false,
          isFailed: true,
          isFetchingNew: false,
        });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        isFetchingNew: false,
      });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMoreNew = () => {
    if (this.state.isEmptyDataNew == true) {
      return;
    }

    this.pageNew =
      this.state.isFailed == true ? this.pageNew : this.pageNew + 1;
    this.getCmeNew(this.pageNew);
  };

  _renderItemNew = ({ item }) => {
    console.log("item cme", item);
    return <CardItemCme navigation={this.props.navigation} data={item} />;
  };

  _renderItemFooter = () => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader()}
    </View>
  );

  _renderItemFooterLoader() {
    if (this.state.isFailed == true && this.pageNew > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            this.handleLoadMoreNew();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = () => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader()}</View>
  );

  _renderEmptyItemLoader() {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.getData(false)}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (
      this.state.isFetching == false &&
      this.state.isSuccess == true &&
      this.state.isEmptyDataNew == true
    ) {
      return <Text style={{ textAlign: "center" }}>Data not found</Text>;
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  gotoCmeHistory = () => {
    AdjustTracker(AdjustTrackerConfig.CME_History);
    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: "CmeHistory",
        key: `cme_history`,
      })
    );
  };

  navigateTo = (to) => {
    switch (to) {
      case "CmeKursus":
        AdjustTracker(AdjustTrackerConfig.CME_ADD_Serti);
    }

    this.props.navigation.navigate(to);
  };

  doResetSkp = async () => {
    this.dissmisDialogResetSkp();

    try {
      let response = await resetSkp();

      Toast.show({ text: response.message, position: "top", duration: 3000 });

      if (response.isSuccess == true) {
        this.getData(false);
      }
    } catch (error) {
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  };

  dissmisDialogResetSkp = () => {
    this.setState({ showDialogSkp: false });
  };

  _initModalResetSkp() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showDialogSkp}
        onRequestClose={() => {
          console.log("alert closed");
        }}
      >
        <TouchableOpacity
          // {...testID('buttonDissmisDialog')}
          accessibilityLabel="button_dismis_dialog"
          onPress={this.dissmisDialogResetSkp}
          style={{
            backgroundColor: "#00000080",
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card style={styles.cardResetSkp}>
            <CardItem
              style={{ justifyContent: "center", flex: 1, paddingVertical: 10 }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 10,
                }}
              >
                <View style={styles.modalBadge}>
                  <Icon
                    style={styles.modalImageReset}
                    name="restore"
                    type="MaterialIcons"
                  />
                </View>
                <Text style={[styles.modalText, styles.modalTextTitle]}>
                  RESET
                </Text>
                <Text style={[styles.modalText, { fontSize: 16 }]}>
                  System will reset your SKP Point become 0. If your Manual SKP
                  are not yet approved, it will be rejected automatically. Are
                  you sure?
                </Text>
                <Button
                  // {...testID('buttonResetNow')}
                  accessibilityLabel="button_reset_now"
                  style={styles.btnResetSkp}
                  onPress={() =>
                    this.doResetSkp() + this.sendAdjust(from.Reset_Now)
                  }
                  success
                  block
                >
                  <Text style={{ fontFamily: "Nunito-Bold", fontSize: 18 }}>
                    Reset Now
                  </Text>
                </Button>
              </View>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </Modal>
    );
  }

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.Reset:
        token = AdjustTrackerConfig.CME_Reset;
        break;
      case from.Reset_Now:
        token = AdjustTrackerConfig.CME_Reset_Now;
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  _renderCmeInternational() {
    return (
      <Container>
        <SafeAreaView>
          <Header
            newDesign
            androidStatusBarColor={"#fff"}
            iosBarStyle={"dark-content"}
            noBackButton
          >
            <Body newDesign>
              <Title newDesign>Medical Education</Title>
            </Body>
          </Header>
        </SafeAreaView>

        <View
          style={{
            flex: 1,
          }}
        >
          <Col
            style={{
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              resizeMode="contain"
              style={{
                height: 95,
                width: 120,
              }}
              source={require("../../assets/images/CME-on-big-icon.png")}
            />
            <Text
              bold
              style={{
                marginTop: 48,
                textAlign: "center",
                fontSize: 20,
              }}
            >
              CME Coming Soon
            </Text>
            <Text
              regular
              style={{
                marginTop: 16,
                marginHorizontal: 32,
                textAlign: "center",
              }}
            >
              We are still preparing the right CME contents for you. Stay tuned!
            </Text>
          </Col>
        </View>
      </Container>
    );
  }

  render() {
    const nav = this.props.navigation;
    let translateY = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
      extrapolate: "clamp",
    });
    let opacityContent = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [1, 0],
    });

    return this.state.isInternationalMode ? (
      this._renderCmeInternational()
    ) : (
      <Fragment>
        <SafeAreaView
          style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }}
        />
        <Container>
          {this._initModalResetSkp()}
          <View
            style={{
              width: wp("200%"),
              height: wp("200%"),
              borderRadius: wp("100%"),
              backgroundColor: platform.toolbarDefaultBg,
              position: "absolute",
              left: -wp("50%"),
              top: -wp("100%"),
            }}
          />

          <AnimatedFlatList
            contentContainerStyle={{
              paddingTop: heightHeaderSpan + 5,
              paddingHorizontal: 10,
            }}
            scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: { y: this.state.animatedValue },
                  },
                },
              ],
              { useNativeDriver: true } // <-- Add this
            )}
            data={this.state.cmeNew}
            onEndReached={this.handleLoadMoreNew}
            onEndReachedThreshold={0.5}
            onRefresh={() => this.onRefreshNew(true)}
            refreshing={this.state.isFetchingNew}
            renderItem={this._renderItemNew}
            ListEmptyComponent={this._renderEmptyItem()}
            ListFooterComponent={this._renderItemFooter()}
            keyExtractor={(item, index) => index.toString()}
          />

          <Animated.View
            style={[styles.animHeaderSpan, { transform: [{ translateY }] }]}
          >
            <Animated.View
              style={[styles.wrapperHeader, { opacity: opacityContent }]}
            >
              <Text style={{ color: "#fff" }}>
                Share & get updates on new clinic cases and health journal info
              </Text>

              <Card style={{ flex: 1 }}>
                <View
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                    paddingHorizontal: 5,
                    paddingTop: 5,
                  }}
                >
                  <TouchableOpacity
                    // {...testID('buttonReset')}
                    accessibilityLabel="button_reset"
                    style={styles.backgroundReset}
                    onPress={() =>
                      this.setState({ showDialogSkp: true }) +
                      this.sendAdjust(from.Reset)
                    }
                  >
                    {/* <Icon name='md-refresh-circle' style={{ fontSize: 28, color: '#CB1D50' }} /> */}
                    <Text
                      style={styles.textReset}
                      onPress={() =>
                        this.setState({ showDialogSkp: true }) +
                        this.sendAdjust(from.Reset)
                      }
                    >
                      RESET
                    </Text>
                  </TouchableOpacity>
                </View>
                <CardItem
                  nopadding
                  style={{ flex: 1, flexDirection: "column" }}
                >
                  <Text style={styles.introTitle}>SKP This Year</Text>
                </CardItem>
                <CardItem
                  nopadding
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <Text style={styles.introPoint}>{this.state.skp}</Text>
                </CardItem>
                <CardItem
                  nopadding
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <Button
                    {...testID("buttonCmeKursus")}
                    bordered
                    success
                    small
                    style={styles.introBtn}
                    onPress={() => this.navigateTo("CmeKursus")}
                  >
                    <Text>Add SKP Courses</Text>
                  </Button>
                </CardItem>
              </Card>
            </Animated.View>
          </Animated.View>

          <Animated.View style={[styles.animHeader]}>
            <Header noShadow hasTabs>
              <Body style={{ flexDirection: "row", alignItems: "center" }}>
                {this.state.backButtonAvailable == true && (
                  <View style={{ marginLeft: -6 }}>
                    <ArrowBackButton onPress={() => this.onBackPressed()} />
                  </View>
                )}
                <Title style={styles.textTitle(this.state.backButtonAvailable)}>
                  CME
                </Title>
              </Body>
              <Right>
                <Button
                  // {...testID('buttonCmeHistory')}
                  accessibilityLabel="button_cme_history"
                  transparent
                  onPress={() => this.gotoCmeHistory()}
                >
                  <Icon
                    name="clipboard-notes"
                    type="Foundation"
                    style={{ fontSize: 30, width: 20, marginHorizontal: 10 }}
                  />
                  {/* <Icon name='md-clipboard' style={{ fontSize: 28, marginHorizontal: 10, }} /> */}
                </Button>
              </Right>
            </Header>
          </Animated.View>
        </Container>
      </Fragment>
    );
  }
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const heightHeaderSpan = 310;
const styles = StyleSheet.create({
  animHeader: {
    position: "absolute",
    width: "100%",
    zIndex: 2,
  },
  animHeaderSpan: {
    position: "absolute",
    paddingTop: platform.toolbarHeight,
    backgroundColor: platform.toolbarDefaultBg,
    height: heightHeaderSpan,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: "flex-end",
    alignItems: "flex-start",
  },
  tabHeading: {
    flex: 1,
    height: 100,
    backgroundColor: platform.tabBgColor,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapperHeader: {
    position: "absolute",
    left: 0,
    right: 0,
    paddingHorizontal: 10,
    paddingVertical: 10,
    zIndex: 3,
  },
  introTitle: {
    fontFamily: "Nunito-Bold",
    fontSize: 18,
    textAlign: "center",
  },
  introPoint: {
    fontFamily: "Nunito-Black",
    fontSize: 40,
    textAlign: "center",
  },
  introBtn: {
    marginTop: 10,
    marginBottom: 15,
  },
  emptyItem: {
    justifyContent: "center",
    alignItems: "center",
    height: platform.deviceHeight / 2 - 80,
  },
  heading: {
    fontFamily: "Nunito-Bold",
    fontSize: 14,
    color: "#fff",
    marginTop: 20,
  },
  modalBadge: {
    width: 95,
    height: 95,
    marginBottom: 20,
    backgroundColor: "#CB1D50",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
  },
  modalImageReset: {
    color: "#FFFFFF",
    fontSize: 37,
    // textAlign: 'center'
  },
  modalText: {
    // fontSize: 20,
    color: "#37474F",
    fontFamily: "Nunito-SemiBold",
    textAlign: "center",
  },
  btnResetSkp: {
    marginHorizontal: 5,
    marginTop: 20,
    borderRadius: 10,
    height: 55,
    backgroundColor: "#D82059",
  },
  backgroundReset: {
    // borderWidth: 2,
    // borderColor:'#CB1D50',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    justifyContent: "center",
  },
  textReset: {
    fontFamily: "Nunito-Bold",
    fontSize: 14,
    color: "#CB1D50",
    textDecorationLine: "underline",
  },
  modalTextTitle: {
    fontWeight: "bold",
    marginBottom: 5,
    fontSize: 20,
  },
  cardResetSkp: {
    flex: 0,
    marginTop: 22,
    width: platform.deviceWidth - 50,
    height:
      platform.platform == "ios"
        ? platform.deviceHeight / 1.5
        : platform.deviceHeight / 1.8,
  },
  textTitle: (backButtonAvailable) => ({
    fontSize: 30,
    marginLeft: backButtonAvailable == true ? 20 : null,
  }),
});
