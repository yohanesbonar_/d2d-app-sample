import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  Modal,
  TouchableOpacity,
  Keyboard,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Form,
  Item,
  Input,
  Label,
  Toast,
  Card,
  CardItem,
  Footer,
} from "native-base";
import { Loader } from "./../../components";
import { convertMonth } from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
import DocumentPicker from "react-native-document-picker";
import ImagePicker from "react-native-image-picker";
import DateTimePicker from "react-native-modal-datetime-picker";
import { uploadSkpManual } from "./../../libs/NetworkUtility";
import moment from "moment-timezone";
import { NavigationActions } from "react-navigation";
import { testID, sendTopicCertificate } from "./../../libs/Common";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { SafeAreaView } from "react-native";

let from = {
  Upload: "Upload",
};

export default class CmeKursus extends Component {
  state = {
    showLoader: false,
    modalVisible: false,
    showChoices: false,
    isShowingDatePicker: false,
    kursusTitle: null,
    kursusTitleFailed: false,
    sumber: null,
    sumberFailed: false,
    poinSkp: null,
    poinSkpFailed: false,
    mDate: null,
    mDateRequest: null,
    dateFailed: false,
    pathCertificate: null,
    typeCertyficate: null,
    certificateFailed: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(false);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.props.navigation.goBack()
  }

  async doValidate() {
    let isSuccess = true;
    let message = "";
    if (
      !this.state.kursusTitle ||
      !this.state.sumber ||
      !this.state.poinSkp ||
      !this.state.mDateRequest ||
      !this.state.pathCertificate
    ) {
      isSuccess = false;

      if (!this.state.kursusTitle) {
        this.setState({ kursusTitleFailed: true });
      } else {
        this.setState({ kursusTitleFailed: false });
      }

      if (!this.state.sumber) {
        this.setState({ sumberFailed: true });
      } else {
        this.setState({ sumberFailed: false });
      }

      if (!this.state.poinSkp) {
        this.setState({ poinSkpFailed: true });
      } else {
        this.setState({ poinSkpFailed: false });
      }

      if (!this.state.mDateRequest) {
        this.setState({ dateFailed: true });
      } else {
        this.setState({ dateFailed: false });
      }

      if (!this.state.pathCertificate) {
        this.setState({ certificateFailed: true });
      } else {
        this.setState({ certificateFailed: false });
      }

      message = "Please fill in all required fields";
    } else {
      isSuccess = true;
      this.setState({
        kursusTitleFailed: false,
        sumberFailed: false,
        poinSkpFailed: false,
        dateFailed: false,
        certificateFailed: false,
      });
    }

    if (isSuccess == true) {
      this.uploadSkp();
    } else {
      Toast.show({ text: message, position: "top", duration: 3000 });
    }
  }

  async uploadSkp() {
    this.setState({ showLoader: true });
    try {
      let isPdf = this.state.typeCertyficate == "pdf" ? true : false;

      let response = await uploadSkpManual(
        this.state.poinSkp,
        this.state.kursusTitle,
        this.state.sumber,
        this.state.mDateRequest,
        this.state.pathCertificate,
        isPdf
      );

      if (response.isSuccess == true) {
        this.showSertificate(response.data.certificate);
        //sendTopicCertificate('offline', )
      } else {
        console.log(response);
        Toast.show({
          text: "Something went wrong!",
          position: "top",
          duration: 3000,
        });
        // console.warn(response)
        // Toast.show({ text: response.message, position: 'top', duration: 3000 });
      }

      this.setState({ showLoader: false });
    } catch (error) {
      console.log(error);
      Toast.show({ text: error, position: "top", duration: 3000 });
      this.setState({ showLoader: false });
    }
  }

  showSertificate = (certificate) => {
    if (certificate != null) {
      let paramCertificate = {
        title: this.state.kursusTitle,
        certificate: certificate,
        from: "cmekursus",
      };

      this.props.navigation.dispatch(
        NavigationActions.navigate({
          routeName: "CmeCertificate",
          params: paramCertificate,
          key: `cmeCertificate-${this.state.kursusTitle}`,
        })
      );
    } else {
      this.props.navigation.popToTop();
    }
  };

  showCamera() {
    const options = {
      quality: 1.0,
      maxWidth: 1500,
      maxHeight: 1500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.launchCamera(options, (response) => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        //   let source = { uri: response.uri };
        let source = response.uri;

        // You can also display the image using data:
        //   let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({ pathCertificate: source, typeCertyficate: "image" });

        this.dismissChoices();
      }
    });
  }

  selectFileImages() {
    // iPhone/Android
    // DocumentPicker.show({
    //     filetype: [DocumentPickerUtil.images()],
    // }, (error, res) => {
    //     // Android
    //     if (res != null && res.uri != null) {
    //         console.log(res.uri, res.type, res.fileName, res.fileSize);

    //         this.dismissChoices()
    //         this.setState({ pathCertificate: res.uri, typeCertyficate: 'image' })
    //     }
    // });
    const options = {
      quality: 1.0,
      maxWidth: 1500,
      maxHeight: 1500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        //   let source = { uri: response.uri };
        let source = response.uri;

        // You can also display the image using data:
        //   let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({ pathCertificate: source, typeCertyficate: "image" });

        this.dismissChoices();
      }
    });
  }

  selectFilePdf = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });

      if (res != null && res.uri != null) {
        console.log(
          res.uri,
          res.type, // mime type
          res.name,
          res.size
        );
        this.dismissChoices();
        this.setState({ pathCertificate: res.uri, typeCertyficate: "pdf" });
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        console.warn(err);
        throw err;
      }
    }
  };

  showChoices() {
    Keyboard.dismiss();
    this.setState({ showChoices: true });
  }

  dismissChoices() {
    this.setState({ showChoices: false });
  }

  _initChoicePicker() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showChoices}
        onRequestClose={() => {
          this.setState({ showChoices: false });
        }}
      >
        <View
          // {...testID('buttonDismissChoices')}
          accessibilityLabel="button_dismis_choices"
          onPress={() => this.dismissChoices()}
          style={styles.viewModal}
        >
          <Card style={styles.cardModal}>
            <Text style={[styles.textSelectChoice, { textAlign: "left" }]}>
              Choose file :
            </Text>
            <CardItem style={{ justifyContent: "center" }}>
              <TouchableOpacity
                // {...testID('buttonCamera')}
                accessibilityLabel="button_camera"
                onPress={() => this.showCamera()}
                style={styles.touchModal}
              >
                <Icon
                  style={styles.imageChoicePicker}
                  name="camera"
                  type="FontAwesome"
                />
                <Text style={styles.modalText}>Camera</Text>
              </TouchableOpacity>
              <TouchableOpacity
                // {...testID('buttonGallery')}
                accessibilityLabel="button_gallery"
                onPress={() => this.selectFileImages()}
                style={styles.touchModal}
              >
                <Icon
                  style={styles.imageChoicePicker}
                  name="file-image-o"
                  type="FontAwesome"
                />
                <Text style={styles.modalText}>Gallery</Text>
              </TouchableOpacity>
              <TouchableOpacity
                // {...testID('buttonPdf')}
                accessibilityLabel="button_pdf"
                onPress={() => this.selectFilePdf()}
                style={styles.touchModal}
              >
                <Icon
                  style={styles.imageChoicePicker}
                  name="file-pdf-o"
                  type="FontAwesome"
                />
                <Text style={styles.modalText}>Pdf</Text>
              </TouchableOpacity>
            </CardItem>
            <Text
              // {...testID('buttonDismissText')}
              accessibilityLabel="button_dismis_text"
              style={styles.textModalCancel}
              onPress={() => this.dismissChoices()}
            >
              Batal
            </Text>
          </Card>
        </View>
      </Modal>
    );
  }

  _initDatePicker() {
    <DateTimePicker
      isVisible={this.state.isShowingDatePicker}
      onConfirm={this._handleDatePicked}
      onCancel={this._hideDatePicker}
    />;
  }

  _showDatePicker = () => {
    Keyboard.dismiss();
    this.setState({ isShowingDatePicker: true });
  };

  _hideDatePicker = () => this.setState({ isShowingDatePicker: false });

  _handleDatePicked = (date) => {
    console.log("A date has been picked: ", date);
    let shownDate =
      date.getDate() +
      "-" +
      convertMonth(date.getMonth() + 1) +
      "-" +
      date.getFullYear();
    let dateFormat = moment(date).format("YYYY-MM-DD HH:mm:ss");

    this.setState({ mDate: shownDate, mDateRequest: dateFormat });
    this._hideDatePicker();
  };

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.Upload:
        token = AdjustTrackerConfig.CME_Add_Upload;
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  render() {
    const nav = this.props.navigation;
    const { params } = nav.state;

    const pathCertificate =
      this.state.pathCertificate != null && this.state.typeCertyficate
        ? this.state.typeCertyficate + " : " + this.state.pathCertificate
        : "Upload Certificate";

    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showLoader}
          onRequestClose={() => console.log("loader closed")}
        >
          <Loader visible={this.state.showLoader} />
        </Modal>
        {this._initChoicePicker()}
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header noShadow>
            <Left style={{ flex: 0.15 }}>
              <Button
                // {...testID('buttonBack')}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={{ color: "#fff" }} />
              </Button>
            </Left>
            <Body style={[styles.headerBody, { flex: 0.7 }]}>
              <Title>CME Course</Title>
            </Body>
            <Right style={{ flex: 0.15 }} />
          </Header>
        </SafeAreaView>
        <Content>
          <View style={styles.content}>
            <DateTimePicker
              isVisible={this.state.isShowingDatePicker}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDatePicker}
              maximumDate={moment().toDate()}
            />
            <Label style={styles.labelTextInput}>Course Title</Label>
            <Item error={this.state.kursusTitleFailed}>
              <Input
                autoCapitalize="none"
                // {...testID('inputTitle')}
                accessibilityLabel="input_title"
                style={styles.textInput}
                placeholder={"Input Course Title"}
                placeholderTextColor={platform.placeholderTextColor}
                onChangeText={(txt) => this.setState({ kursusTitle: txt })}
              />
            </Item>
            <Label style={styles.labelTextInput}>Source</Label>
            <Item error={this.state.sumberFailed}>
              <Input
                autoCapitalize="none"
                // {...testID('inputCourse')}
                accessibilityLabel="input_course"
                style={styles.textInput}
                placeholder={"Input Course"}
                placeholderTextColor={platform.placeholderTextColor}
                onChangeText={(txt) => this.setState({ sumber: txt })}
              />
            </Item>
            <Label style={styles.labelTextInput}>SKP Point</Label>
            <Item error={this.state.poinSkpFailed}>
              <Input
                autoCapitalize="none"
                // {...testID('inputSkpPoint')}
                accessibilityLabel="input_skp_point"
                style={styles.textInput}
                keyboardType="numeric"
                placeholder={"Input SKP Point"}
                placeholderTextColor={platform.placeholderTextColor}
                onChangeText={(txt) => this.setState({ poinSkp: txt })}
              />
            </Item>
            <Label style={styles.labelTextInput}>Certificate Date</Label>
            <Item
              error={this.state.dateFailed}
              style={[{ justifyContent: "flex-start" }]}
              onPress={this._showDatePicker}
            >
              <Input
                autoCapitalize="none"
                style={styles.textInput}
                // onTouchStart={this._showDatePicker}
                // {...testID('inputDate')}
                accessibilityLabel="input_date"
                editable={false}
                pointerEvents="none"
                value={this.state.mDate}
                placeholder={"Choose Certificate Date"}
                placeholderTextColor={platform.placeholderTextColor}
                onChangeText={(txt) => this.setState({ date: txt })}
              />
              <Icon
                name="date-range"
                type="MaterialIcons"
                style={{ color: "#C3D1E8" }}
              />
            </Item>
            <Label style={styles.labelTextInput}>Certificate</Label>
            <Item error={this.state.certificateFailed}>
              <TouchableOpacity
                // {...testID('buttonShowChoices')}
                accessibilityLabel="button_show_choices"
                style={styles.touchUploadCertificate}
                onPress={() => this.showChoices()}
              >
                <View style={styles.viewUploadCertf}>
                  <Icon name="md-images" style={styles.imageUpload} />
                  <Text style={styles.textPathUpload}>{pathCertificate}</Text>
                </View>
              </TouchableOpacity>
            </Item>
          </View>
        </Content>
        <Footer>
          <Button
            style={styles.btnSubmit}
            onPress={() => this.doValidate() + this.sendAdjust(from.Upload)}
            success
            block
          >
            <Text style={{ fontFamily: "Nunito-Bold" }}>Upload</Text>
          </Button>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerBody: {
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    padding: 20,
  },
  title: {
    fontFamily: "Nunito-Bold",
    fontSize: 30,
    textAlign: "center",
    marginTop: 30,
    marginBottom: 10,
    color: "#fff",
  },
  card: {
    flex: 0,
    paddingVertical: 20,
    paddingBottom: 30,
  },
  form: {
    paddingHorizontal: 20,
  },
  btnSubmit: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 55,
  },
  textSelectChoice: {
    marginHorizontal: 10,
    fontSize: 20,
    color: "#37474F",
    fontFamily: "Nunito-SemiBold",
  },
  imageChoicePicker: {
    color: "#CB1D50",
    fontSize: 30,
    textAlign: "center",
    margin: 10,
  },
  modalText: {
    fontSize: 16,
    color: "#37474F",
    fontFamily: "Nunito-Regular",
    textAlign: "center",
  },
  itemField: {
    backgroundColor: "#FFFFFF",
  },
  textInput: {
    // fontSize:15,
  },
  viewModal: {
    backgroundColor: "#00000080",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  cardModal: {
    marginTop: 22,
    width: platform.deviceWidth - 70,
    flex: 0,
    paddingVertical: 10,
  },
  touchModal: {
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    flex: 0.5,
  },
  textModalCancel: {
    marginHorizontal: 20,
    fontSize: 15,
    color: "#37474F",
    fontFamily: "Nunito-Regular",
    textAlign: "right",
  },
  labelTextInput: {
    marginTop: 15,
    marginBottom: 5,
  },
  touchUploadCertificate: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  viewUploadCertf: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  imageUpload: {
    color: "#C3D1E8",
    margin: 5,
    fontSize: 30,
  },
  textPathUpload: {
    fontFamily: "Nunito-Regular",
    fontSize: 15,
    color: "#BEBEBE",
  },
});
