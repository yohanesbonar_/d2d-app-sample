import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, ScrollView, WebView } from 'react-native';
import Menu from 'react-native-material-menu';
import { 
    Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right, Item, Toast
} from 'native-base';
import { Loader } from './../../components';
import { escapeHtml } from './../../libs/Common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import MyWebView from 'react-native-webview';
import { ParamSetting, getSetting, getCourseMedicinus } from './../../libs/NetworkUtility';
import Api, { errorMessage } from './../../libs/Api';

export default class GeneralWebview extends Component {

    webView = {
        canGoBack: false,
        ref: null,
      }

    state = {
        showLoader: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        canGoBack: true,
        urlLogin : null,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        this.getMedicinusCek();
    }

    async getMedicinusCek() {
        this.setState({ showLoader : true,});
        try {
            let response = await getCourseMedicinus();
            if(response != null && response.data != null && response.data.length >0){
                this.setState({
                    urlLogin : response.data[0].login_url
                })
            }

        } catch(error) {
            this.setState({ showLoader : false});
            // Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000})
        }
    }

    onNavigationStateChange(navState) {
        this.setState({
          canGoBack: navState.canGoBack
        });
    }


  onBackPress = () => {
    if (this.webView.canGoBack && this.webView.ref) {
      this.webView.ref.goBack();
      return true;
    }else{
        this.props.navigation.goBack();
    }
    // return false;
  }

    onloadStart = () => {
        this.setState({showLoader: true})
    }

    onloadEnd = () => {
        setTimeout(() => {
            this.setState({showLoader: false})
        }, 1000);
    }

    render() {
        const nav = this.props.navigation;
        const { params } = nav.state;

        let loginUrl = this.state.loginUrl == null ? params.loginUrl : this.state.loginUrl;
        let url = loginUrl + '&wantsurl=http://cme.medicinus.co/cme/course/view.php?id='+ params.courseId;
        console.log(url)

        let jsCode =`
        $(document).ready(function() {
            $('#header, .header-main').hide();
            $('#page-header, #footer, #theme_switch_link').remove();
            $('#yui3-css-stamp').next('a').remove();
        });
        `;
        
        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                <Header noShadow>
                    <Left style={{flex:0.15}}>
                        <Button 
                        // disabled={!this.state.canGoBack} 
                        transparent onPress={() => this.onBackPress()}>
                            <Icon name='md-arrow-back' style={styles.toolbarIcon}/>
                        </Button>   
                    </Left>
                    <Body style={[styles.headerBody, {flex:0.7}]}  >
                        <Title>{this.state.title}</Title>
                    </Body>
                    <Right style={{flex:0.15}}/>
                </Header>
                    <View style={{flex : 1, flexDirection: 'column'}}>
                        <WebView 
                            userAgent={'Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)'}
                            ref={(webView) => { this.webView.ref = webView; }}
                            onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack; }}
                            injectedJavaScript={jsCode}
                            javaScriptEnabledAndroid={true}
                            source={{uri: url}} 
                            onLoadStart={this.onloadStart}
                            onLoadEnd={this.onloadEnd}
                            style={{flex: 1, flexDirection: 'column', width: wp('100%'), backgroundColor: '#F3F3F3'}} />
                    </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody:{
        justifyContent: 'center',
        alignItems: 'center'
    },
});
