import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, Animated, View, Alert, BackHandler, Dimensions } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Toast, } from 'native-base';
import { Loader, } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { escapeHtml, STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import { MyWebView, WebView } from 'react-native-webview';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import moment from 'moment-timezone';
import { NavigationActions } from 'react-navigation';
import Pdf from 'react-native-pdf';
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { SafeAreaView } from 'react-native';

let from = {
    START: 'START',
}

export default class CmeDetail extends Component {

    state = {
        animatedValue: new Animated.Value(0),
        widthWebView: wp('100%') - 20,
        width: Dimensions.get('window').width,
        scale: 1,
        from: this.props.navigation.state.params ? this.props.navigation.state.params.from : '',
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.showWarningBack();
            return true;
        });
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.START:
                if (this.state.from == 'Cme')
                    token = AdjustTrackerConfig.CME_SKP_StartQuiz
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }

    async getLocalAnswer() {
        let result = null;

        let jsonAnswer = await AsyncStorage.getItem(STORAGE_TABLE_NAME.QUIZ_ANSWER);

        if (jsonAnswer != null) {
            let objJsonAnswer = await JSON.parse(jsonAnswer);
            result = objJsonAnswer;
        }

        return result;
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    showWarningBack = async () => {
        let dataLocal = await this.getLocalAnswer();
        if (dataLocal != null) {
            Alert.alert(
                'Warning',
                'All your answers will be lost. Are you sure you want to exit this quiz?',
                [
                    { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    { text: 'OK', onPress: () => this.deletedLocalAnswer() },
                ],
                { cancelable: true }
            )
        } else {
            const { params } = this.props.navigation.state;

            if (params != null && params.from == 'profile') {
                this.props.navigation.navigate('Cme')
            }
            else {
                this.props.navigation.goBack()
            }
        }
    }


    deletedLocalAnswer = async () => {
        let deleted = await AsyncStorage.setItem(STORAGE_TABLE_NAME.QUIZ_ANSWER, '');
        const { params } = this.props.navigation.state;

        if (params != null && params.from == 'profile') {
            this.props.navigation.navigate('Cme')
        }
        else {
            this.props.navigation.goBack()
        }
    }


    startQuiz = () => {
        const { params } = this.props.navigation.state;
        if (params != null) {
            // const cmeQuiz = { type: "Navigate", routeName: "CmeQuiz", params: params }
            // this.props.navigation.navigate(cmeQuiz);

            this.props.navigation.dispatch(NavigationActions.navigate({
                routeName: 'CmeQuiz',
                params: params, key: `cmeQuiz-${params.id}`
            }));
        }
    }

    renderWebView = (summary) => {
        if (summary == null || summary == '') {
            return (<Text style={styles.textNotFound}>Data Summary Not Found</Text>);
        } else {
            return (<MyWebView
                source={{ html: escapeHtml(summary, '') }}
                startInLoadingState={true}
                width={wp('100%') - 40}
                defaultHeight={100} />);
        }
    }

    _renderPdf(attachment) {
        if (platform.platform == 'android') {
            const source = { uri: attachment, cache: false };
            return (
                <Pdf
                    scale={this.state.scale}
                    fitWidth={true}
                    source={source}
                    onLoadComplete={(numberOfPages, filePath) => {
                        console.log(numberOfPages)
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        // this.state.page = page;
                        // if (page && numberOfPages)
                        //     Toast.show({ text: `Page : ${page}/${numberOfPages}`, position: 'bottom', duration: 1000 })
                    }}
                    onError={(error) => {
                        console.log(error.message)
                        if (error.message != 'Canceled')
                            Toast.show({ text: error.message, position: 'top', duration: 3000 })
                    }}
                    // style={{ flex: 1, height: hp('100%') - platform.toolbarHeight, width: wp('100%') - 20 }} />
                    style={[styles.pdf, { width: Dimensions.get('window').width }]} />
            );
        } else {
            const jsCode = "window.postMessage(document.getElementByTagName('body').innerHTML)"
            return (
                <WebView
                    javaScriptEnabled={true}
                    injectedJavaScript={jsCode}
                    onMessage={event => console.log('Received: ', event.nativeEvent.data)}
                    source={{ uri: attachment }}
                    onLoadStart={this.onloadStart}
                    onLoadEnd={this.onloadEnd}
                    style={[styles.styleWebView, { width: this.state.widthWebView }]} />
            )
        }
    }

    onLayout = (e) => {
        this.setState({
            widthWebView: wp('100%') - 20,
        })

        if (this.state.width < Dimensions.get('window').width) {
            this.setState({ scale: 2 });
        } else {
            this.setState({ scale: 1 });
        }
    }

    render() {
        const { params } = this.props.navigation.state;
        let translateY = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight)],
            outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
            extrapolate: 'clamp',
        })

        let formatDate = 'YYYY-MM-DD HH:mm:ss';
        let expired = moment(params.available_end, formatDate).format('DD MMM YYYY');
        let pdfDesc = params.filename;
        // let pdfDesc = 'https://d2doss.oss-ap-southeast-5.aliyuncs.com/pdf/quiz/11e0f54c7918cd5f319f19a8c02f4930.pdf';

        return (
            <Container>
                <Loader visible={this.state.showLoader} />
                {/* <View style={{ width: wp('200%'), height: wp('200%'), borderRadius: wp('100%'), backgroundColor: platform.toolbarDefaultBg, position: 'absolute', left: -wp('50%'), top: -wp('100%') }}></View> */}
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>

                    <Header nopadding noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.showWarningBack()}>
                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>{params.title}</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <View
                    onLayout={this.onLayout}
                    style={styles.contentPdf} bordered>
                    {/* <Text style={styles.introTitle}>{params.title}</Text> */}
                    <Text style={styles.introSKP}>{params.skp} SKP</Text>
                    <Text style={styles.introExpired}>Available until {expired}</Text>
                </View>
                <View
                    onLayout={this.onLayout}
                    style={styles.viewPdf}>
                    {this._renderPdf(pdfDesc)}
                    <Button
                        // {...testID('buttonStartQuiz')}
                        accessibilityLabel="button_start_quiz"
                        block success onPress={() => this.startQuiz() + this.sendAdjust(from.START)} style={{ height: 50, borderRadius: 0 }}>
                        <Text nopadding style={{ color: '#fff' }}>START QUIZ</Text>
                    </Button>
                </View>
            </Container>
        )
    }
}

const heightHeaderSpan = 250;
const styles = StyleSheet.create({
    contentPdf: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        flexDirection: 'column',
        backgroundColor: platform.toolbarDefaultBg,
    },
    animHeader: {
        position: "absolute",
        width: "100%",
        zIndex: 2
    },
    introTitle: {
        fontFamily: 'Nunito-Bold',
        fontSize: 16,
        textAlign: 'center',
        color: '#fff'
    },
    introSKP: {
        fontFamily: 'Nunito-Bold',
        fontSize: 14,
        textAlign: 'center',
        color: '#fff'
    },
    introExpired: {
        fontFamily: 'Nunito-Light',
        fontSize: 12,
        color: '#fff',
        marginTop: 10,
    },
    textNotFound: {
        fontSize: 16,
        margin: 10,
        textAlign: 'center',
        flexWrap: 'wrap'
    },
    viewPdf: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    styleWebView: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        padding: 10,
    },
    pdf: {
        flex: 1,
        // width: Dimensions.get('window').width,
    },
});
