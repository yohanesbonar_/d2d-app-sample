import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Image, TouchableOpacity, FlatList, Animated, BackHandler } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Tabs, Tab, Toast, ScrollableTab, TabHeading } from 'native-base';
import { Loader, CardItemCme } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { ValueStatusSKPHistory, getSKPHistory, ValueTypeSKPHistory } from './../../libs/NetworkUtility';
import { testID } from './../../libs/Common';
import Orientation from "react-native-orientation"; 
import { AdjustTrackerConfig, AdjustTracker } from './../../libs/AdjustTracker';
import { SafeAreaView } from 'react-native';

let from = {
    D2D: 'D2D',
    MANUAL: 'MANUAL',
}

export default class CmeHistory extends Component {

    pageManual = 1;
    pageD2DSkp = 1;
    timer = null;
    state = {
        competenceId: null,
        cmeManualList: [],
        cmeDoneList: [],
        searchValue: '',
        isFetching: false,
        isFetchingDone: false,
        isSuccess: false,
        isFailed: false,
        isEmptyDataManual: false,
        isEmptyDataDone: false,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        StatusBar.setHidden(false);
        this.getData();
        this.sendAdjust(from.D2D)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed = () => {
        let { params } = this.props.navigation.state

        if (typeof params != 'undefined') {
            let { isFlowNpaIdiProfileCertificate } = params
            if (typeof isFlowNpaIdiProfileCertificate != 'undefined') {
                this.props.navigation.navigate('Cme')
            }
        }
        else {
            this.props.navigation.goBack()
        }

    }

    getData() {
        this.getDataCmeHistory(this.pageD2DSkp)
        this.getSkpManual(this.pageManual);
    }

    async getDataCmeHistory(page) {

        this.setState({ isFailed: false, isFetchingDone: true });

        try {
            let response = await getSKPHistory(page, ValueTypeSKPHistory.ALL, ValueStatusSKPHistory.VALID);
            console.log('getDataCmeHistory response: ', response)
            if (response.isSuccess == true) {

                this.setState({
                    cmeDoneList: [...this.state.cmeDoneList, ...response.docs],
                    isFetchingDone: false, isSuccess: true, isFailed: false,
                    isEmptyDataDone: response.docs != null && response.docs.length > 0 ? false : true,
                });

            } else {
                this.setState({ isFetchingDone: false, isSuccess: false, isFailed: true });
            }
        } catch (error) {
            this.setState({ isFetchingDone: false, isSuccess: false, isFailed: true });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }


    async getSkpManual(page) {

        this.setState({ isFailed: false, isFetching: true });

        try {
            let response = await getSKPHistory(page, ValueTypeSKPHistory.OFFLINE);

            if (response.isSuccess == true) {

                this.setState({
                    cmeManualList: [...this.state.cmeManualList, ...response.docs],
                    isFetching: false, isSuccess: true, isFailed: false,
                    isEmptyDataManual: response.docs != null && response.docs.length > 0 ? false : true,
                });

            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            }
        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }

    handleLoadMoreDone = () => {
        if (this.state.isEmptyDataDone == true) {
            return;
        }
        this.pageD2DSkp = this.state.isFailed == true ? this.pageD2DSkp : this.pageD2DSkp + 1;
        this.getDataCmeHistory(this.pageD2DSkp)
    }

    handleLoadMoreManual = () => {
        if (this.state.isEmptyDataManual == true) {
            return;
        }
        this.pageManual = this.state.isFailed == true ? this.pageManual : this.pageManual + 1;
        this.getSkpManual(this.pageManual);
    }


    _renderItemDone = ({ item }) => {
        return (
            <CardItemCme navigation={this.props.navigation} data={item} isFromHistory={true} />
        )
    }

    _renderItemManual = ({ item }) => {
        return (
            <CardItemCme navigation={this.props.navigation} data={item} isManualSkp={true} isFromHistory={true} />
        )
    }

    _renderItemFooter = (type) => (
        <View style={{
            height: (type == 'manual' && this.state.isFetching == true) ||
                (type == 'done' && this.state.isFetchingDone == true) ? 80 : 0,
            justifyContent: 'center', alignItems: 'center'
        }}>
            {this._renderItemFooterLoader(type)}
        </View>
    )

    _renderItemFooterLoader(type) {
        if (this.state.isFailed == true &&
            ((this.pageManual > 1 && type == 'manual') ||
                (this.pageD2DSkp > 1 && type == 'done'))
        ) {
            return (<TouchableOpacity onPress={() => {
                if (type == 'manual')
                    this.handleLoadMoreManual()
                else if (type == 'done')
                    this.handleLoadMoreDone()
            }}>
                <Icon name='ios-sync' style={{ fontSize: 42 }} />
            </TouchableOpacity>
            )
        }

        return (<Loader visible={type == 'manual' ? this.state.isFetching : this.state.isFetchingDone} transparent />);
    }

    _renderEmptyItem = (type) => (
        <View style={styles.emptyItem}>
            {this._renderEmptyItemLoader(type)}
        </View>
    )

    _renderEmptyItemLoader = (type) => {
        if (((this.state.isFetching == false && type == 'manual') ||
            (this.state.isFetchingDone == false && type == 'done')) && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.getData()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>touch to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (((this.state.isFetching == false && type == 'manual') ||
            (this.state.isFetchingDone == false && type == 'done')) && this.state.isSuccess == true) {
            let messageNotFound = "Not data found";
            return (
                <View style={styles.viewNotFound}>
                    {/* <Icon name="ios-search" style={styles.iconNotFound} /> */}
                    <Text style={styles.textNotFound}>{messageNotFound}</Text>
                </View>

            );
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.D2D:
                token = AdjustTrackerConfig.CME_History_D2DSKP_1
                break;
            case from.MANUAL:
                token = AdjustTrackerConfig.CME_History_Manual_SKP
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }

    onChangeTab = (event) => {
        if (event && event.i == 0) {
            this.sendAdjust(from.D2D)
        } else if (event && event.i == 1) {
            this.sendAdjust(from.MANUAL)
        }
    }

    render() {
        const nav = this.props.navigation;

        return (
            <Container>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>History</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                {/* <Content> */}
                <Tabs
                    initialPage={0}
                    renderTabBar={(props) =>
                        <ScrollableTab {...props}
                            renderTab={(name, page, active, onPress, onLayout) => (
                                <TouchableOpacity key={page}
                                    onPress={() => { onPress(page) }}
                                    onLayout={onLayout}
                                    activeOpacity={0.99}>
                                    <Animated.View style={styles.tabHeading}>
                                        <TabHeading
                                            // {...testID('tabHeading')}
                                            accessibilityLabel="tab_heading"
                                            style={{ width: (platform.deviceWidth / 2), height: platform.toolbarHeight }}
                                            active={active}>
                                            <Text style={{
                                                fontFamily: active ? 'Nunito-Bold' : 'Nunito-Regular',
                                                color: active ? platform.topTabBarActiveTextColor : platform.topTabBarTextColor,
                                                fontSize: platform.tabFontSize
                                            }}>
                                                {name}
                                            </Text>
                                        </TabHeading>
                                    </Animated.View>
                                </TouchableOpacity>
                            )}
                            underlineStyle={{ backgroundColor: platform.topTabBarActiveBorderColor }} />
                    }
                    onChangeTab={(event) => this.onChangeTab(event)} >
                    <Tab heading="D2D SKP">
                        <View>
                            <FlatList
                                style={{ paddingHorizontal: 10 }}
                                data={this.state.cmeDoneList}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={this._renderItemDone}
                                ListEmptyComponent={this._renderEmptyItem('done')}
                                ListFooterComponent={this._renderItemFooter('done')}
                                refreshing={true}
                                onEndReached={this.handleLoadMoreDone}
                                onEndReachedThreshold={0.5} />
                        </View>
                    </Tab>
                    <Tab heading="MANUAL SKP">
                        <View>
                            <FlatList
                                style={{ paddingHorizontal: 10 }}
                                data={this.state.cmeManualList}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={this._renderItemManual}
                                ListEmptyComponent={this._renderEmptyItem('manual')}
                                ListFooterComponent={this._renderItemFooter('manual')}
                                refreshing={true}
                                onEndReached={this.handleLoadMoreManual}
                                onEndReachedThreshold={0.5} />
                        </View>
                    </Tab>
                </Tabs>

                {/* </Content> */}
            </Container >
        )
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    searchField: {
        fontSize: 14,
        lineHeight: 15,
    },
    tab: {
        padding: 10
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    },
    tabHeading: {
        flex: 1,
        height: 100,
        backgroundColor: platform.tabBgColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
