import React, { Component, PropTypes } from "react";
import { WebView } from "react-native-webview";
import {
  StatusBar,
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Toast,
  Content,
  Col,
} from "native-base";
import { Loader, Popup } from "./../../components";
import Pdf from "react-native-pdf";
import {
  downloadFile,
  testID,
  getCountryCodeProfile,
} from "./../../libs/Common";
import {
  sendEmailCertificate,
  readNotification,
  sendEmailEventCertificate,
  sendEmailWebinarCertificate,
} from "./../../libs/NetworkUtility";
import platform from "../../../theme/variables/d2dColor";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import { AdjustTrackerConfig, AdjustTracker } from "./../../libs/AdjustTracker";
import { NavigationActions, StackActions } from "react-navigation";
import { translate } from "../../libs/localize/LocalizeHelper";
import { SafeAreaView } from "react-native";
import PopupInputCertificate from "../../components/PopupInputCertificate";
import Modal from "react-native-modal";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";
import AsyncStorage from "@react-native-community/async-storage";
import _ from "lodash";
let from = {
  DOWNLOAD: "DOWNLOAD",
  SEND_EMAIL: "SEND_EMAIL",
};

export default class CmeCertificate extends Component {
  state = {
    page: 1,
    numberOfPages: 0,
    bookmark: false,
    showLoader: false,
    convertWidthImage: wp("85%"),
    widthImage: 0,
    heightImage: 0,
    scale: 1,
    width: Dimensions.get("window").width,
    flex: 0.75,
    from: this.props.navigation.state.params
      ? this.props.navigation.state.params.from
      : "",
    isManualSkp: this.props.navigation.state.params
      ? this.props.navigation.state.params.isManualSkp
      : "",
    isCertificateAvailable: true,
    modalVisible: false,
    emailAddress: "",
    dataCertificate: this.props.navigation.state.params,
    countryCode: "",
  };

  constructor(props) {
    super(props);
    this.closeScreen = this.closeScreen.bind(this); //handle props navigation undefined
    this.gotoFeeds = this.gotoFeeds.bind(this);
    this.gotoHome = this.gotoHome.bind(this);
    this.handlerStatusSubmitEmail = this.handlerStatusSubmitEmail.bind(this);
  }

  isFromHistory = false;
  isFromEventDetail = false;
  isFromProfile = false;
  backFromCertificate = null;

  componentDidMount() {
    console.log(this.state.from, this.state.isManualSkp);
    StatusBar.setHidden(false);
    let { params } = this.props.navigation.state;
    console.log("CmeCertificate params:", params);
    if (params != null) {
      if (params.typeCertificate == "webinar") {
        this.setState({
          isCertificateAvailable: params.isCertificateAvailable,
        });
      }

      if (platform.platform == "ios") {
        let source = {
          uri: params.type != null ? params.slug : params.certificate,
        };
        Image.getSize(
          source,
          (width, height) =>
            this.setState({ widthImage: width, heightImage: height }),
          console.log
        );
      }

      if (params.isFromListNotifProfile) {
        if (params.dataNotif.status == "unread") {
          this.readNotification(params.dataNotif);
        }
      }

      if (params.isRead) {
        this._readNotif(params.title, params.certificate);
      }

      this.isFromHistory =
        params.isFromHistory != null ? params.isFromHistory : false;
      this.isFromEventDetail =
        params.isFromEventDetail != null ? params.isFromEventDetail : false;
      //this.isFromProfile = params.from != null ? params.from : false;

      if (params.from != null) {
        if (params.from == "profile") {
          this.isFromProfile = true;
          this.backFromCertificate = params.backFromCertificate;
        }
      }
    }

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  closeScreen = () => {
    this.props.navigation.goBack();
  };

  gotoFeeds = () => {
    this.props.navigation.popToTop();
    this.props.navigation.navigate("Feeds");
  };

  gotoHome = () => {
    this.props.navigation.popToTop();
  };

  async showPopupWarning() {
    let countryCode = await getCountryCodeProfile();
    console.log("Country code ->", countryCode);
    this.setState({
      countryCode: countryCode,
    });
    if (this.popup) {
      this.popup.togglePopup();
    }

  };

  renderCertificateNotAvailable = () => {
    return (
      <Content>
        <View
          style={{
            marginVertical: 50,
            paddingVertical: 45,
            backgroundColor: "white",
            marginHorizontal: 16,
            borderRadius: 12,
            shadowColor: "#455B6314",
            shadowOffset: { width: 0, height: 4 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 4,
            paddingHorizontal: 30,
          }}
        >
          <Col
            style={{
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              style={{ width: 52, height: 58 }}
              source={require("../../assets/images/icon-certificate-orion.png")}
              resizeMode="contain"
            />
            <Text
              style={{
                marginTop: 40,
                color: "#6C6C6C",
                fontSize: 18,
                fontFamily: "Nunito-Regular",
                textAlign: "center",
              }}
            >
              {translate("message_certificate_not_ready")}
            </Text>
          </Col>
        </View>
      </Content>
    );
  };

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.DOWNLOAD:
        if (this.state.from == "Cme") {
          token = AdjustTrackerConfig.CME_SKP_Certidownload;
        } else if (this.state.from == "CmeHistory") {
          if (this.state.isManualSkp)
            token = AdjustTrackerConfig.CME_History_ManualSKP_Download;
          else token = AdjustTrackerConfig.CME_History_D2DSKP_Download;
        }
        break;
      case from.SEND_EMAIL:
        if (this.state.from == "Cme") {
          token = AdjustTrackerConfig.CME_SKP_SentEmail;
        } else if (this.state.from == "CmeHistory") {
          if (this.state.isManualSkp)
            token = AdjustTrackerConfig.CME_History_ManualSKP_SentEmail;
          else token = AdjustTrackerConfig.CME_History_D2DSKP_SentEmail;
        }
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async sendEmail(paramCertificate) {
    this.setState({ showLoader: true });
    this.sendAdjust(from.SEND_EMAIL);
    console.warn("typeCertificate", paramCertificate.typeCertificate);

    try {
      let response = "";
      if (paramCertificate.typeCertificate === "event") {
        response = await sendEmailEventCertificate(paramCertificate);
      } else if (paramCertificate.typeCertificate == "webinar") {
        response = await sendEmailWebinarCertificate(paramCertificate);
      } else {
        response = await sendEmailCertificate(paramCertificate);
      }
      console.log(response);
      this.setState({ showLoader: false });
      if (
        paramCertificate.email != null &&
        this.state.countryCode == "PH" &&
        response.isSuccess == true
      ) {
        Toast.show({
          text: "Certificate has been sent to your email",
          textStyle: { fontSize: 14 },
          position: "bottom",
          buttonText: "CLOSE",
        });
      } else {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "top", duration: 3000 });
    }
  }

  _renderPdf(attachment, isImage) {
    if (platform.platform == "android" && !isImage) {
      console.log("attachment", attachment);
      const source = { uri: attachment, cache: false };
      return (
        <Pdf
          scale={this.state.scale}
          fitWidth={true}
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(numberOfPages);
          }}
          onPageChanged={(page, numberOfPages) => {
            // this.state.page = page;
            // if (page && numberOfPages)
            //     Toast.show({ text: `Page : ${page}/${numberOfPages}`, position: 'bottom', duration: 1000 })
          }}
          onError={(error) => {
            // if (error.message != 'cancelled')
            //     Toast.show({ text: error.message, position: 'top', duration: 3000 })
          }}
          // style={{ flex: 1, height: hp('100%') - platform.toolbarHeight, width: wp('100%') }} />
          style={[styles.pdf, { width: Dimensions.get("window").width - 60 }]}
        />
      );
    } else {
      const jsCode =
        "window.postMessage(document.getElementByTagName('body').innerHTML)";

      // let htmlImage = `
      //     <html>
      //         <head>
      //             <meta content="width=device-width, initial-scale=1" name="viewport">
      //             <style>.img-responsive { display: block; max-width: 100%; height: auto; }</style>
      //         </head>
      //         <body>
      //             <img src="${attachment}" alt="img" class="img-responsive">
      //         </body>
      //     </html>`;

      return (
        <WebView
          javaScriptEnabled={true}
          injectedJavaScript={jsCode}
          onMessage={(event) =>
            console.log("Received: ", event.nativeEvent.data)
          }
          source={{ uri: attachment }}
          // source={{html : htmlImage }}
          onLoadStart={this.onloadStart}
          onLoadEnd={this.onloadEnd}
          // style={{ flex: 1, flexDirection: 'column', width: wp('80%'), backgroundColor: '#ffffff', }} />
          style={[
            styles.pdf,
            { width: Dimensions.get("window").width - 60 },
            { backgroundColor: "#ffffff" },
          ]}
        />
      );
    }
  }

  async readNotification(data) {
    try {
      let response = await readNotification(data);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  }

  async _readNotif(title, slug) {
    // this.setState({ showLoader: true });

    try {
      let response = await readNotification(title, slug);
      console.log(response);
      // this.setState({ showLoader: false });
      // Toast.show({ text: response.message, position: 'top', duration: 3000 })
    } catch (error) {
      // this.setState({ showLoader: false });
      // Toast.show({ text: error.message, position: 'top', duration: 3000 })
    }
  }

  onLayout = (e) => {
    this.setState({
      convertWidthImage: wp("85%"),
    });
    if (this.state.width < Dimensions.get("window").width) {
      this.setState({ scale: 2 });
      this.setState({ flex: 1 });
    } else {
      this.setState({ scale: 1 });
      this.setState({ flex: 0.75 });
    }
  };

  onBackPressed = () => {
    if (
      this.isFromHistory ||
      this.isFromEventDetail ||
      this.state.from == "TestResult" ||
      this.state.from == "Webinar" ||
      this.state.from == "Feeds"
    ) {
      this.state.isCertificateAvailable
        ? this.props.navigation.goBack()
        : this.showPopupWarning();
    } else if (this.isFromProfile == true) {
      this.backToRoot(this.backFromCertificate);
    } else {
      getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
        let popCounter = this.props.navigation.state.params.popCounter;

        if (profile.country_code == "ID") {
          if (this.state.from == "cmekursus") {
            this.props.navigation.pop(2);
          } else if (this.state.from == "Cme") {
            if (typeof popCounter != "undefined") {
              if (popCounter == 1) {
                //enter directly from list cme - cme detail - cme quiz
                this.props.navigation.goBack()
              }
            }
            else {
              //enter from list cme - cme detail - cme quiz
              this.props.navigation.pop(3);
            }
          } else if (this.state.from == "SearchKonten") {
            // console.log("popCounter: ", popCounter);
            if (typeof popCounter != "undefined") {
              //enter from list search content - cme - cme quiz
              this.props.navigation.pop(popCounter);
              this.props.navigation.state.params.onRefreshDataSearchKonten();
            } else {
              //enter from list search content
              this.props.navigation.goBack();
            }
          } else {
            this.props.navigation.goBack();
          }
        } else {
          this.props.navigation.popToTop();
        }
      });
    }
  };

  backToRoot(root) {
    //root is cme list or cme history list
    this.props.navigation.navigate(root, {
      isFlowNpaIdiProfileCertificate: true,
    });
  }

  renderCertificate = () => {
    let { params } = this.props.navigation.state;
    let regex = /.pdf/;
    let isImage = regex.test(params.certificate) ? false : true;

    console.log("params", regex.test(params.certificate));
    console.log("isImage", isImage);

    let convertHeightImage = 100;
    (this.state.convertWidthImage * this.state.heightImage) /
      this.state.widthImage;

    if (isImage) {
      return (
        <ScrollView>
          <View onLayout={this.onLayout} style={styles.mainContent}>
            <Card style={styles.cardImage}>
              <CardItem bordered>
                {platform.platform == "ios" && (
                  <Image
                    //style={{ width: this.state.convertWidthImage, height: convertHeightImage }}
                    style={{ width: wp("85%"), height: wp("100%") }}
                    source={{ uri: params.certificate }}
                    resizeMode="contain"
                  />
                )}
                {platform.platform == "android" && (
                  <Image
                    style={{ width: wp("85%"), height: wp("100%") }}
                    source={{ uri: params.certificate }}
                    resizeMode="contain"
                  />
                )}
              </CardItem>
              <CardItem>
                <View style={styles.viewBottom}>
                  <View style={styles.backgroundTotalSkp}>
                    <Text nopadding style={styles.textTotalSkp}>
                      Certificate
                    </Text>
                  </View>
                  <View style={{ flex: 1, alignItems: "flex-end" }}>
                    <TouchableOpacity
                      // {...testID('buttonSendEmail')}
                      accessibilityLabel="button_send_email"
                      full
                      small
                      transparent
                      onPress={() => this.onTapSendEmail(params)}
                      style={{
                        flex: 1,
                        alignItems: "center",
                        flexDirection: "row",
                      }}
                    >
                      <Icon
                        nopadding
                        style={styles.listItemIconBookmarked}
                        type="MaterialIcons"
                        name="email"
                      />
                      <Text nopadding style={styles.itemTextEmail}>
                        Sent Email
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </CardItem>
            </Card>
          </View>
        </ScrollView>
      );
    } else if (!isImage) {
      return (
        <View onLayout={this.onLayout} style={styles.mainContent}>
          <Card style={[styles.content, { flex: this.state.flex }]}>
            <View style={styles.contentPdf} bordered>
              {this._renderPdf(params.certificate, isImage)}
            </View>
            <CardItem>
              <View style={styles.viewBottom}>
                <View style={styles.backgroundTotalSkp}>
                  <Text nopadding style={styles.textTotalSkp}>
                    Certificate
                  </Text>
                </View>
                <View style={{ flex: 1, alignItems: "flex-end" }}>
                  <TouchableOpacity
                    accessibilityLabel="button_send_email"
                    full
                    small
                    transparent
                    onPress={() => this.onTapSendEmail(params)}
                    style={{
                      flex: 1,
                      alignItems: "center",
                      flexDirection: "row",
                    }}
                  >
                    <Icon
                      nopadding
                      style={styles.listItemIconBookmarked}
                      type="MaterialIcons"
                      name="email"
                    />
                    <Text nopadding style={styles.itemTextEmail}>
                      Sent Email
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </CardItem>
          </Card>
        </View>
      );
    }
  };

  onTapSendEmail = async (params) => {
    let countryCode = await getCountryCodeProfile();
    console.log("Country code ->", countryCode);
    this.setState({
      countryCode: countryCode,
    });
    if (countryCode != "PH") {
      this.sendEmail(params);
    } else {
      console.log("Open popup email");
      this.setState({
        modalVisible: true,
      });
    }
  };

  toggleModal = (visible) => {
    this.setState({
      modalVisible: visible,
    });
  };

  renderInputEmail() {
    console.log("this.state.modalVisible", this.state.modalVisible);
    return (
      <Modal
        hasBackdrop={true}
        avoidKeyboard={true}
        backdropOpacity={0.8}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}
        isVisible={this.state.modalVisible}
      >
        <PopupInputCertificate
          type={"sendEmail"}
          handlerStatusSubmitEmail={this.handlerStatusSubmitEmail}
          onVisibleModal={this.toggleModal}
        />
      </Modal>
    );
  }

  async handlerStatusSubmitEmail(email) {
    this.state.dataCertificate.email = email;
    this.sendEmail(this.state.dataCertificate);
  }

  render() {
    let { params } = this.props.navigation.state;

    return (
      <Container style={{ backgroundColor: "#F3F3F3" }}>
        <Loader visible={this.state.showLoader} />
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header>
            <Left style={{ flex: 0.5 }}>
              <Button
                // {...testID('buttonBack')}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={styles.toolbarIcon} />
              </Button>
            </Left>
            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Title>Certificate</Title>
            </Body>
            <Right style={{ flex: 0.5 }}>
              {this.state.isCertificateAvailable && (
                <Button
                  // {...testID('buttonDownload')}
                  accessibilityLabel="button_download"
                  transparent
                  onPress={() =>
                    downloadFile(params.certificate) +
                    this.sendAdjust(from.DOWNLOAD)
                  }
                >
                  <Icon
                    name="md-download"
                    nopadding
                    style={styles.iconDownload}
                  />
                </Button>
              )}
            </Right>
          </Header>
        </SafeAreaView>
        {!this.state.isCertificateAvailable && this.state.countryCode != "ID" && (
          <Popup
            ref={(ref) => {
              this.popup = ref;
            }}
            closeScreen={this.closeScreen}
            gotoFeeds={this.gotoFeeds}
            title={"Move to"}
            message={""}
            type={"navigation"}
          />
        )}

        {!this.state.isCertificateAvailable && this.state.countryCode == "ID" && (
          <Popup
            ref={(ref) => {
              this.popup = ref;
            }}
            closeScreen={this.closeScreen}
            gotoHome={this.gotoHome}
            title={"Move to"}
            message={""}
            type={"navigationID"}
          />
        )}

        {this.state.isCertificateAvailable
          ? this.renderCertificate()
          : this.renderCertificateNotAvailable()}
        {this.renderInputEmail()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentPdf: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // marginVertical: 5,
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 20,
  },
  content: {
    // flex: 0.6,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    // paddingHorizontal: 10,
    backgroundColor: "#fff",
  },
  cardImage: {
    flex: 0,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    // paddingHorizontal: 10,
    backgroundColor: "#fff",
  },
  headerBody: {
    justifyContent: "center",
    alignItems: "center",
  },
  mainContent: {
    flex: 1,
    padding: 10,
    // backgroundColor: '#ffff'
  },
  pdf: {
    flex: 1,
    // width: Dimensions.get('window').width,
  },
  backgroundCmeType: {
    backgroundColor: "#F0F0F0",
    borderRadius: 25,
    paddingHorizontal: 15,
    // justifyContent: 'center'
  },
  listitemText: {
    fontSize: 12,
    color: "#78849E",
  },
  itemTextEmail: {
    fontSize: 14,
    color: "#78849E",
  },
  listItemIconBookmarked: {
    color: "#78849E",
    fontSize: 20,
  },
  iconDownload: {
    flex: 0,
    color: "#FFFFFF",
    fontSize: 22,
  },
  cardItemPdf: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#F4F4F6",
    marginHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: "#ffff",
  },
  colType: {
    justifyContent: "flex-start",
    flexDirection: "row",
    marginHorizontal: 10,
  },
  colShare: {
    justifyContent: "center",
    alignContent: "center",
  },
  pdf: {
    flex: 1,
    height: hp("100%") - platform.toolbarHeight,
    width: wp("100%"),
  },
  pagingWrapper: {
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: 1000,
    minHeight: 40,
    width: Dimensions.get("window").width,
    padding: 10,
    backgroundColor: "rgba(36, 41, 46, 0.7)",
  },
  paging: {
    color: "#fff",
    textAlign: "center",
  },
  iconDownload: {
    flex: 0,
    color: "#FFFFFF",
    fontSize: 22,
  },
  viewBottom: {
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  backgroundTotalSkp: {
    backgroundColor: "#F0F0F0",
    borderRadius: 25,
    paddingHorizontal: 20,
    paddingVertical: 5,
    justifyContent: "center",
  },
  textTotalSkp: {
    fontSize: 13,
    color: "#78849E",
  },
});
