import React, { Component } from 'react'
import { BackHandler, ImageBackground, StyleSheet, ScrollView, Image } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Thumbnail, Card, Label, Item, Toast, CardItem, Col, Row, View, Switch, Content } from 'native-base';
import { testID } from '../../libs/Common'
import platform from '../../../theme/variables/d2dColor';
import { getDetailDataPatient, readNotification } from './../../libs/PMM';
import { Loader } from './../../components'
import moment from 'moment-timezone';
import { getInitialName } from '../../libs/GetInitialName'
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';

export default class FinishPostTest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            result: true
        }
    }

    componentDidMount() {

    }
    renderAreaMessageFinish = () => {
        return <ImageBackground
            imageStyle={{ borderRadius: 5 }}
            style={{
                marginTop: 50,
                width: platform.deviceWidth * 0.92,
                height: platform.deviceWidth * 1,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center'
            }}
            resizeMode='stretch'
            source={require('./../../assets/images/bg-finish.png')}
        >

            <Col style={{
                position: 'absolute',
                top: '30%',
                alignItems: 'center',
                marginHorizontal: 50,
            }}>
                <View style={{
                    width: 80,
                    height: 80,
                    borderRadius: 40,
                    backgroundColor: this.state.result ? '#1CA8ED' : '#FD6A6A',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                    <Icon
                        type='Ionicons'
                        name={this.state.result ? 'md-checkmark' : 'md-close'}
                        style={{
                            fontSize: 37,
                            color: '#FFFFFF'
                        }} />
                </View>

                <Text style={{
                    marginVertical: 35,
                    color: '#FFFFFF',
                    fontSize: 18,
                    fontFamily: 'Nunito-Regular',
                    textAlign: 'center'
                }}>Anda Telah Menyelesaikan Post test. Silakan isi kuisioner untuk mendapatkan sertifikat Anda</Text>

            </Col>
        </ImageBackground>
    }

    render() {

        return (
            <Container style={{ backgroundColor: '#F3F3F3' }}>
                <Header noShadow >
                    <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>Finish</Title>
                    </Body>
                </Header>
                <Content>
                    {this.renderAreaMessageFinish()}
                </Content>


                <Button
                    // {...testID('buttonSave')}
                    accessibilityLabel="button_back_to_webinar"
                    style={styles.btnQuisioner} onPress={() => this.props.navigation.goBack()} success block>
                    <Text style={{ fontFamily: 'Nunito-Bold' }}>ISI QUISIONER</Text>
                </Button>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    containerAreaProcessingTime: {
        marginVertical: 20,
        paddingVertical: 20,
        backgroundColor: 'white',
        marginHorizontal: 16,
        borderRadius: 12,
        shadowColor: '#455B6314',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 4,
    },

    btnQuisioner: {
        position: 'absolute',
        borderRadius: 0,
        flex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 0
    },

})