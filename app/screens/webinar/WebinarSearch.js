import React, { Component, Fragment } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, Animated, View, Modal, Image, TouchableOpacity, FlatList, BackHandler, SafeAreaView } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Tabs, Tab, Item, Input, Row, Toast, Col } from 'native-base';
import { Loader, CardItemWebinar, HistorySearch } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { getListWebinar, ValueTypeWebinar } from './../../libs/NetworkUtility';
import moment from 'moment-timezone';
import { testID, STORAGE_TABLE_NAME, removeHistorySearch, EnumTypeHistorySearch } from './../../libs/Common';
import { getData, KEY_ASYNC_STORAGE, sendTopicWebinarAttendee } from '../../../src/utils';

export default class WebinarSearch extends Component {

    page = 1;
    timer = null;
    state = {
        competenceId: null,
        webinarList: [],
        searchValue: '',
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        isEmptyData: false,
        isFocusSearch: false,
        historySearchList: [],
        serverDate: '',
        country_code: ""
    }

    constructor(props) {
        super(props);
        this.getDataCountryCode()
    }

    getDataCountryCode = () => {
        getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
            this.setState({
                country_code: profile.country_code
            })
        });
    };

    componentDidMount() {
        StatusBar.setHidden(false);
        const { params } = this.props.navigation.state;
        console.log(params)
        // this.setState({competenceId : params.id});

        setTimeout(() => {
            this.getData();
        }, 500);

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack()
            return true;
        });

        this.didFocusListener = this.props.navigation.addListener(
            'didFocus',
            async () => {
                console.log('didFocusListener')
                this.loadHistorySearch()
            }
        )
        this.loadHistorySearch()
    }

    componentWillUnmount() {
        this.backHandler.remove()
        this.didFocusListener.remove()
    }

    getData() {
        this.getDataListWebinar(this.page);
    }

    loadHistorySearch = async () => {
        let dataHistorySearch = await AsyncStorage.getItem(STORAGE_TABLE_NAME.HISTORY_SEARCH_WEBINAR)
        dataHistorySearch = JSON.parse(dataHistorySearch) != null ? JSON.parse(dataHistorySearch).historySearch : []
        this.setState({
            historySearchList: dataHistorySearch
        })
    }

    doShownData = async (dataProfile, webinarList) => {
        let result = [];

        if (webinarList != null && webinarList.length > 0) {
            webinarList.forEach(element => {
                if (element.exclusive == 1) {
                    if (dataProfile != null && dataProfile.webinar != null && dataProfile.webinar.length > 0) {
                        let isShow = false
                        dataProfile.webinar.forEach(slug_hash => {
                            sendTopicWebinarAttendee(slug_hash)
                            if (slug_hash == element.slug_hash) {
                                isShow = true
                            }
                        })

                        if (isShow) {
                            result.push(element)
                        }
                    }
                } else {
                    result.push(element)
                }
            });
        }

        return result;
    }

    async getDataListWebinar(page) {
        if (this.state.searchValue == '') {
            this.setState({ isFetching: false, isSuccess: true })
            return;
        }

        this.setState({ isFailed: false, isFetching: true });

        try {
            let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
            let dataProfile = JSON.parse(getJsonProfile);

            let listIdSpecialist = ''
            let isSkp = false


            let response = await getListWebinar(page, isSkp, listIdSpecialist, this.state.searchValue);
            console.log("cek response webSearch", response);
            if (response.isSuccess == true) {
                let shownData = await this.doShownData(dataProfile, response.docs);
                if (page == 1 || (page > 1 && response.server_date != "")) {
                    this.setState({ serverDate: response.server_date });
                }
                this.setState({
                    webinarList: [...this.state.webinarList, ...shownData],
                    isFetching: false, isSuccess: true, isFailed: false,
                    isEmptyData: response.docs != null && response.docs.length > 0 ? false : true
                });
            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        } catch (error) {
            console.log(error)
            this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }

    handleLoadMore = () => {
        if (this.state.isEmptyData == true) {
            return;
        }
        this.page = this.state.isFailed == true ? this.page : this.page + 1;
        this.getDataListWebinar(this.page);
    }

    _renderItem = ({ item }) => {
        item.server_date = this.state.serverDate;
        return (
            // <CardItemWebinar navigation={this.props.navigation} data={item} isLive={true} />
            <CardItemWebinar valueSearchHistory={this.state.searchValue} navigation={this.props.navigation} data={item} country_code={this.state.country_code}
                isLive={moment().isSameOrAfter(moment(item.end_date, 'YYYY-MM-DD HH:mm:ss')) ? false : true} />
        )
    }

    _renderItemDone = ({ item }) => {
        return (
            <CardItemWebinar navigation={this.props.navigation} data={item} isLive={false} country_code={this.state.country_code} />
        )
    }

    _renderItemFooter = () => (
        <View style={{
            height: this.state.isFetching == true ? 80 : 0,
            justifyContent: 'center', alignItems: 'center'
        }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true &&
            (this.page > 1)
        ) {
            return (<TouchableOpacity onPress={() => {
                this.handleLoadMore()

            }}>
                <Icon name='ios-sync' style={{ fontSize: 42 }} />
            </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItem = () => (
        <View style={styles.emptyItem}>

            {this.state.isFocusSearch && this.state.searchValue.trim().length < 3 ?
                null
                :
                this._renderEmptyItemLoader()
            }
        </View>
    )

    _renderEmptyItemLoader = () => {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.getData()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>touch to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            let messageNotFound = "We are really sad we could not find anything";
            if (this.state.searchValue != '') {
                messageNotFound += ' for ' + this.state.searchValue;
            }
            return (
                <View style={styles.viewNotFound}>
                    <Icon name="ios-search" style={styles.iconNotFound} />
                    <Text style={styles.textNotFound}>{messageNotFound}</Text>
                </View>

            );
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }

    doSearch = (inputText) => {
        clearTimeout(this.timer);
        this.setState({
            webinarList: [],
            searchValue: inputText,
            isEmptyData: false,
        });

        if (inputText.trim().length >= 3) {

            this.page = 1;

            this.timer = setTimeout(() => {
                this.getData();
            }, 1000);
        }
    }

    onPressRemoveItemHistory = async (item) => {
        await removeHistorySearch(EnumTypeHistorySearch.WEBINAR, item.index)
        this.loadHistorySearch()
    }

    render() {
        const nav = this.props.navigation;
        const inlineStyleIconClose = { marginTop: 7 }

        return (
            <Container>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow searchBar rounded>
                        <Item nopadding>
                            <Icon name="ios-search" />
                            <Input
                                onFocus={() => this.setState({ isFocusSearch: true })}
                                // {...testID('inputSearch')}
                                accessibilityLabel="input_search"
                                placeholder="Search webinar video"
                                onChangeText={(text) => this.doSearch(text)}
                                value={this.state.searchValue} style={styles.searchField} />
                            <Icon
                                // {...testID('buttonSearch')}
                                accessibilityLabel="button_search"
                                name="ios-close-circle" onPress={() => this.doSearch('')} />
                        </Item>
                        <TouchableOpacity
                            {...testID('button_close')}
                            accessibilityLabel="button_close"
                            transparent style={{ marginRight: 5, marginLeft: 10, alignSelf: 'center' }} onPress={() => nav.goBack()}>
                            <Icon name='md-close' type="Ionicons"
                                style={[styles.iconClose]}
                            />
                        </TouchableOpacity>
                    </Header>
                </SafeAreaView>
                {/* <Content> */}
                {/* <Tabs initialPage={0}>
                        <Tab heading="TERBARU">
                            <View>
                                <FlatList 
                                    style={{marginHorizontal:10}}
                                    data={this.state.webinarNewList}
                                    keyExtractor={(item, index) => index.toString() }
                                    renderItem={this._renderItemNew}
                                    ListEmptyComponent={this._renderEmptyItem('new')}
                                    ListFooterComponent={this._renderItemFooter('new')}
                                    refreshing={true}
                                    onEndReached={this.handleLoadMoreNew}
                                    onEndReachedThreshold={0.5} />
                            </View>
                        </Tab>
                        <Tab heading="SELESAI">
                            <View>
                                <FlatList 
                                    style={{marginHorizontal:10}}
                                    data={this.state.webinarDoneList}
                                    keyExtractor={(item, index) => index.toString() }
                                    renderItem={this._renderItemDone}
                                    ListEmptyComponent={this._renderEmptyItem('done')}
                                    ListFooterComponent={this._renderItemFooter('done')}
                                    refreshing={true}
                                    onEndReached={this.handleLoadMoreDone}
                                    onEndReachedThreshold={0.5} />
                            </View>
                        </Tab>
                    </Tabs> */}

                <View style={{ flex: 1, flexDirection: 'column' }}>

                    {this.state.isFocusSearch && this.state.searchValue.trim().length < 3 && this.state.historySearchList.length > 0 && (
                        <HistorySearch
                            onPressRemoveItem={(item) => this.onPressRemoveItemHistory(item)}
                            onPressItem={(txt) => this.doSearch(txt)}
                            data={this.state.historySearchList}>

                        </HistorySearch>
                    )}

                    <FlatList
                        style={{ padding: 10 }}
                        data={this.state.webinarList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderItem}
                        ListEmptyComponent={this._renderEmptyItem()}
                        ListFooterComponent={this._renderItemFooter()}
                        refreshing={true}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5} />
                </View>

                {/* </Content> */}
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    searchField: {
        fontSize: 14,
        lineHeight: 15,
    },
    tab: {
        padding: 10
    },
    btnNotFound: {
        backgroundColor: '#3AE194',
        flex: 1,
        justifyContent: 'center'
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    },
    btnReqJournal: {
        marginHorizontal: 10,
        marginVertical: 20,
        borderRadius: 10,
        height: 55
    },
    iconClose: {
        fontSize: 28,
        color: '#fff',
    },
});
