import AsyncStorage from '@react-native-community/async-storage';
import _ from 'lodash';
import { Body, Button, Col, Container, Content, Header, Icon, Row, Text, Title, View } from 'native-base';
import React, { Component } from 'react';
import { BackHandler, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import platform from '../../../theme/variables/d2dColor';
import { PopupInputCertificate } from '../../components';
import Config from 'react-native-config'
import { ScrollView } from 'react-native-gesture-handler';
import { translate } from '../../libs/localize/LocalizeHelper'
import { SafeAreaView } from 'react-native';

export default class TestResult extends Component {

    dataResult = null
    constructor(props) {
        super(props);
        this.state = {
            result_time: '00:00',
            isPass: false,
            score: 0,
            maxAttempt: 0,
            currentAttempt: 0,
            remainAttempt: 0,
            typePretestPosttest: '',
            skp: 0,
            modalVisible: false,
            isQuisionerPass: false,
            dataCertificate: null,
            quisionerLink: null,
            isSubmit: false
            // dataWebinar: null
        }

        this.handlerStatusSubmit = this.handlerStatusSubmit.bind(this);
        this.closeGoToCertificate = this.closeGoToCertificate.bind(this);
    }

    handlerStatusSubmit(dataCertificate) {
        this.setState({
            isSubmitted: true,
            dataCertificate: dataCertificate
        })
    }

    closeGoToCertificate = async () => {
        //check certificate is availabe or not
        let isCertificateAvailable = ''
        let env =  Config.ENV == "production" ? '' : 'dev/';
        let uid = await AsyncStorage.getItem('UID');

        let data = null

        if (_.isEmpty(this.state.dataCertificate)) {
            //console.log()
            console.log('certificate belum ada ', this.state.dataCertificate)
            data = {
                title: '',
                typeCertificate: 'webinar',
                certificate: '',
                isCertificateAvailable: false,
                from: 'TestResult'
            }
        }

        else {
            isCertificateAvailable = true
            console.log('certificate sudah ada', this.state.dataCertificate)
            data = {
                title: this.state.dataCertificate.event_title,
                typeCertificate: 'webinar',
                certificate: this.state.dataCertificate.filename,
                isCertificateAvailable: true,
                from: 'TestResult'
            }
        }

        this.props.navigation.replace('CmeCertificate', { ...data })

    }

    toggleModal = (visible) => {
        this.setState({
            modalVisible: visible,
        });
    }

    renderPopupInputCertificate(data) {

        return (
            <Modal
                hasBackdrop={true}
                avoidKeyboard={true}
                backdropOpacity={0.8}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={600}
                animationOutTiming={600}
                backdropTransitionInTiming={600}
                backdropTransitionOutTiming={600}
                isVisible={this.state.modalVisible}
            >
                <PopupInputCertificate type={'webinar'} closeGoToCertificate={this.closeGoToCertificate} handlerStatusSubmit={this.handlerStatusSubmit} onVisibleModal={this.toggleModal} data={data} />
            </Modal >
        )
    }

    componentDidMount() {
        this.initData()
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            //this.onBackPressed()
            return true;
        });
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    initData = async () => {
        // let { dataResult } = this.props.navigation.state.params
        let { params } = this.props.navigation.state
        this.dataResult = params
        console.log('props: ', this.props)

        let maxAttempt = 0;
        let currentAttempt = this.dataResult.count_attempt
        let typePrePost = ''
        if (this.dataResult.pretest > 0 && this.dataResult.pretest_passed == 0) {
            typePrePost = 'Pretest'
            maxAttempt = this.dataResult.pretest_max_attempt
        }
        else if (this.dataResult.posttest > 0 && this.dataResult.posttest_passed == 0) {
            typePrePost = 'Posttest'
            maxAttempt = this.dataResult.posttest_max_attempt
        }

        // if (this.dataResult.is_pass) {
        //     await AsyncStorage.removeItem(STORAGE_TABLE_NAME.PROGRESS_TIME_PREPOSTTEST);
        // }

        this.setState({
            result_time: this.dataResult.result_time,
            isPass: this.dataResult.is_pass,
            score: this.dataResult.score,
            remainAttempt: maxAttempt - currentAttempt,
            currentAttempt: currentAttempt,
            maxAttempt: maxAttempt,
            isQuisionerPass: this.dataResult.quisioner_passed,
            typePretestPosttest: typePrePost,
            skp: this.dataResult.skp_reward,
            quisionerLink: this.dataResult.quisioner_link
        })
    }

    renderResult = () => {
        let messageResult = ''
        let isPassMessage = ''
        if (this.state.isPass) {
            messageResult = `Dengan ${this.state.currentAttempt}x percobaan, hasil anda adalah:`
            if (this.state.typePretestPosttest == 'Pretest') {
                isPassMessage = 'BERHASIL'
            }
            else {
                isPassMessage = 'LULUS'
            }
        }
        else {
            if (this.state.currentAttempt == this.state.maxAttempt) {
                messageResult = `Anda sudah melewati max ${this.state.maxAttempt}x percobaan`
            }
            else {
                messageResult = `Tersisa ${this.state.remainAttempt}x Percobaan ${this.state.typePretestPosttest}`
            }

            if (this.state.typePretestPosttest == 'Pretest') {
                isPassMessage = 'TIDAK BERHASIL'
            }
            else {
                isPassMessage = 'TIDAK LULUS'
            }
        }
        return <View style={{
            marginTop: 20,
            paddingVertical: 28,
            backgroundColor: 'white',
            marginHorizontal: 16,
            borderRadius: 12,
            shadowColor: '#455B6314',
            shadowOffset: { width: 0, height: 4 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 4,
        }}>

            <Col style={{
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                {this.state.currentAttempt != -1 &&
                    <Text style={{
                        fontFamily: 'Nunito-SemiBold',
                        color: '#444E64',
                        fontSize: 14,
                        textAlign: 'center'
                    }}>{messageResult}</Text>
                }

                <View style={{
                    marginTop: 26,
                    width: 80,
                    height: 80,
                    borderRadius: 40,
                    backgroundColor: this.state.isPass ? '#1CA8ED' : '#FD6A6A',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                    <Icon
                        type='Ionicons'
                        name={this.state.isPass ? 'md-checkmark' : 'md-close'}
                        style={{
                            fontSize: 37,
                            color: '#FFFFFF'
                        }} />
                </View>

                <Text style={{
                    marginTop: 26,
                    color: '#454F63',
                    fontSize: 24,
                    fontFamily: 'Nunito-SemiBold'
                }}>{isPassMessage}</Text>

                {this.state.typePretestPosttest == 'Posttest' &&
                    <Text style={{
                        marginTop: 4,
                        color: '#78849E',
                        fontSize: 18,
                        fontFamily: 'Nunito-Regular'
                    }}>{this.state.isPass && this.state.skp > 0 ? `Mendapatkan ${this.state.skp} SKP` : ''}</Text>
                }

            </Col>
        </View>

    }

    renderAreaPorcessingTime = () => {
        return <View style={styles.containerAreaProcessingTime}>

            <Row style={{
                marginHorizontal: 20,
                justifyContent: 'space-between',
                alignItems: 'center'
            }}>
                <Row style={{
                    alignItems: 'center'
                }}>
                    <Icon
                        type='MaterialIcons'
                        name={'watch-later'}
                        style={{
                            fontSize: 20,
                            color: '#454F63'
                        }} />

                    <Text style={{
                        marginLeft: 12,
                        color: '#454F63',
                        fontSize: 16,
                        fontFamily: 'Nunito-SemiBold'
                    }}>{translate("waktu_pengerjaan")}</Text>
                </Row>

                <Text style={{
                    color: '#78849E',
                    fontSize: 18,
                    fontFamily: 'Nunito-Regular'
                }}>{this.state.result_time}</Text>

            </Row>
        </View>

    }

    onPressGetCertificateOrQuisioner = () => {
        if (this.state.isQuisionerPass || (this.state.isQuisionerPass == 0 && (this.state.quisionerLink == null || this.state.quisionerLink == ''))) {
            if (this.dataResult.template_certificate == 1) {
                this.setState({
                    modalVisible: true
                })
            }
            else {
                let data = {
                    title: '',
                    typeCertificate: 'webinar',
                    certificate: '',
                    isCertificateAvailable: false,
                    from: 'TestResult'
                }
                this.props.navigation.replace('CmeCertificate', { ...data })

            }
        }
        else {
            this.props.navigation.replace('Quisioner', { ...this.dataResult })
        }
    }


    backToWebinarDetail = () => {
        this.state.currentAttempt == this.state.maxAttempt ? this.props.navigation.goBack() : this.props.navigation.pop(2)
    }

    renderButtonRetryWebinar = () => {
        if (this.state.isPass) {
            if (this.state.typePretestPosttest == 'Pretest') {
                return (
                    <Button
                        // {...testID('buttonSave')}
                        accessibilityLabel="button_back_webinar_or_get_certificate"
                        style={styles.btnBackToWebinar} onPress={() => this.props.navigation.goBack()} success block>
                        <Text style={{
                            fontSize: 14,
                            fontFamily: 'Nunito-SemiBold',
                            textAlign: 'center'
                        }}>LANJUTKAN WEBINAR</Text>
                    </Button>
                )
            }
            else {
                return (
                    <Button
                        // {...testID('buttonSave')}
                        accessibilityLabel="button_back_webinar_or_get_certificate"
                        style={styles.btnBackToWebinar} onPress={() => this.onPressGetCertificateOrQuisioner()} success block>
                        <Text style={{
                            fontSize: 14,
                            fontFamily: 'Nunito-SemiBold',
                            textAlign: 'center'
                        }}>{this.state.isQuisionerPass == 1 || (this.state.isQuisionerPass == 0 && (this.state.quisionerLink == null || this.state.quisionerLink == '')) ? 'DAPATKAN SERTIFIKAT' : 'KUESIONER'}</Text>
                    </Button>
                )
            }
        }

        else {
            return (
                <Row style={{
                    position: 'absolute',
                    bottom: 0
                }}>
                    <Button
                        accessibilityLabel="button_kembali_webinar"
                        style={{
                            borderRadius: 0,
                            width: this.state.currentAttempt < this.state.maxAttempt ? '50%' : '100%',
                            backgroundColor: this.state.currentAttempt == this.state.maxAttempt ? platform.brandSuccess : '#7DB199'
                        }} onPress={() =>
                            this.state.typePretestPosttest == 'Pretest' ?
                                this.props.navigation.popToTop() : this.backToWebinarDetail()

                        }
                        block>
                        <Text style={{
                            fontSize: 14,
                            fontFamily: 'Nunito-SemiBold',
                            textAlign: 'center'
                        }}>{this.state.typePretestPosttest == 'Pretest' ? 'WEBINAR LAINNYA' : 'KEMBALI KE WEBINAR'}</Text>
                    </Button>
                    {this.state.currentAttempt < this.state.maxAttempt && (
                        <Button
                            accessibilityLabel="button_retry"
                            style={{
                                borderRadius: 0,
                                width: '50%',
                            }} onPress={() =>
                                this.props.navigation.goBack()
                            } success block>
                            <Text style={{
                                fontSize: 14,
                                fontFamily: 'Nunito-SemiBold',
                                textAlign: 'center'
                            }}>{translate("button_ulangi")}</Text>
                        </Button>
                    )}
                </Row>
            )
        }
    }

    render() {
        let prefixTitleMessage = this.state.typePretestPosttest == "Pretest" ? '' : ' untuk mendapatkan eCertificate'

        let messageSubTitle = ''

        if (this.state.isPass) {
            if (this.state.typePretestPosttest == 'Pretest') {
                messageSubTitle = `Silakan klik button Lanjutkan Webinar untuk mengikut webinar. Selamat menonton!`
            }

        }
        else if (!this.state.isPass) {
            if (this.state.remainAttempt == 0) {
                messageSubTitle = `Anda sudah mencapai batas maksimal pengerjaan soal dari yang diperbolehkan`
            }
            else if (this.state.currentAttempt == -1) {
                messageSubTitle = `Berikut hasil test anda`

            }
            else {
                messageSubTitle = translate("subtitle_info_result_current_attempt", {
                    currentAttempt: this.state.currentAttempt,
                    remainAttempt: this.state.remainAttempt
                })
            }
        }

        return (
            <Container style={{ backgroundColor: '#F3F3F3' }}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>

                <Header noShadow >
                    <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>{translate("header_result", {
                            type: this.state.typePretestPosttest == 'Pretest' ? 'Pre Test' : 'Post Test'
                        })}</Title>
                    </Body>
                </Header>
                </SafeAreaView>


                <Content style={{
                    marginBottom: 48
                }}>

                    <Text style={{
                        marginTop: 30,
                        marginHorizontal: 60,
                        fontFamily: 'Nunito-Bold',
                        color: this.state.isPass ? '#00A6F3' : '#FD6A6A',
                        fontSize: 20,
                        textAlign: 'center'
                    }}
                    >{this.state.isPass ? translate("webinar_finish_test_pass", { type: this.state.typePretestPosttest }) :
                        translate("webinar_finish_test_not_pass")}</Text>
                    {this.state.isPass && this.state.typePretestPosttest == 'Posttest' ? null :
                        (
                            <Text style={{
                                marginTop: 16,
                                marginHorizontal: 20,
                                fontFamily: 'Nunito-Regular',
                                color: '#6C6C6C',
                                fontSize: 14,
                                textAlign: 'center'
                            }}>{messageSubTitle}</Text>
                        )}

                    {this.renderResult()}
                    {this.renderAreaPorcessingTime()}
                    {this.renderPopupInputCertificate(this.dataResult)}
                </Content>

                {this.renderButtonRetryWebinar()}

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    containerAreaProcessingTime: {
        marginTop: 20,
        marginBottom: 150,
        paddingVertical: 20,
        backgroundColor: 'white',
        marginHorizontal: 16,
        borderRadius: 12,
        shadowColor: '#455B6314',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 4,
    },

    btnBackToWebinar: {
        position: 'absolute',
        borderRadius: 0,
        flex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 0
    }

})
