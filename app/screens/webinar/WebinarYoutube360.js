import React, { Component } from "react";
import { WebView } from "react-native-webview";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Toast,
} from "native-base";
import Orientation from "react-native-orientation";
import {
  BackHandler,
  PixelRatio,
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Image,
  NativeModules,
} from "react-native";
import { testID } from "./../../libs/Common";
import platform from "../../../theme/variables/d2dColor";
import { translate } from "../../libs/localize/LocalizeHelper";
import FullScreen from "../../libs/FullScreen";
import { replace } from "lodash";
const { WebinarModule } = NativeModules;

export default class WebinarYoutube360 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fullscreen: false,
      webinarId: null,
      idYoutube: "",
      isLive: false,
      isUpcoming: false,
      containerMounted: false,
      containerWidth: null,
      isVideo360: false,
    };
  }

  componentDidMount() {
    // Toast.show({
    //   text:
    //   "componentdidmount youtube 360",
    //   position: "top",
    //   duration: 3000,
    // });

    // this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
    //   this.onBackPressed();
    //   return true;
    // });

    //Orientation.lockToLandscape()
    this.doInitParams();
  }

  componentWillUnmount() {
    //this.backHandler.remove();
  }

  componentDidUpdate(prevProps) {
    // Toast.show({
    //   text:
    //   "component did update" + this.props.isTriggerButtonPip,
    //   position: "top",
    //   duration: 3000,
    // });

    // this.webview.injectJavaScript('window.alert("test")');
    if (
      prevProps.isTriggerButtonPip != this.props.isTriggerButtonPip &&
      this.props.isTriggerButtonPip
    ) {
      this.webview.injectJavaScript("window.getTime();");
    }
    if (prevProps.seekTo != this.props.seekTo) {
      this.webview.injectJavaScript(
        "window.seekToSeconds(" + this.props.seekTo + ")"
      );
    }
  }

  onBackPressed() {
    Orientation.lockToPortrait();
    this.props.navigation.goBack();
  }

  doInitParams = () => {
    let params = this.props.data;

    params.isLive = this.props.isLive;
    params.isUpcoming = this.props.isUpcoming;
    console.log("this.props: ", this.props);
    console.log("doInitParams: ", params);
    if (params != null) {
      this.setState({
        webinarId: params.id,
        idYoutube: params.youtube_id,
        // idYoutube: 'rQHwK8_DXsM',
        isLive: params.isLive == true ? true : false,
        isUpcoming: params.isUpcoming == true ? true : false,
        isVideo360: params.video_360 == true ? true : false,
      });
      // this.doViewEvent(params.id);
    }

    this.checkIOS360();
  };

  checkIOS360 = () => {
    if (this.state.isVideo360 == true && platform.platform == "ios") {
      // this.setState({typeContentBottomSheet: 'ios_360' });
      // this.modalizeBottomSheet.open();
    }
  };

  onShouldStartLoadWithRequest(request) {
    // HACK: allow some urls to be embedded, and reject others
    // https://github.com/react-native-community/react-native-webview/issues/381
    return false;
  }

  doChangeFullScreen = (isFullScreen) => {
    console.log("doChangeFullScreen: ", isFullScreen);
    this.props.handlerFullscreenMode(isFullScreen);
    this.setState(
      { fullscreen: isFullScreen, isChangeScreen: true },
      function() {
        if (isFullScreen == false) {
          Orientation.lockToPortrait();
          if (platform.platform == "android") {
            FullScreen.disable();
          }
        } else if (isFullScreen == true) {
          Orientation.lockToLandscape();
          if (platform.platform == "android") {
            // FullScreen.enable();
          }
        }
      }
    );
  };

  minimizeWebinar = (eventData) => {
    var time = eventData.replace("type=minimize", "");
    console.log("time", time);
    time = parseInt(Math.floor(parseFloat(time)));
    console.log("time", time);
    let youtubeId = this.props.data.youtube_id;
    this.props.setTriggerButtonPip(false);
    WebinarModule.minimizeWebinar(youtubeId, "", time, "", (param) => {});
  };

  render() {
    const disableZoom =
      "const meta = document.createElement('meta'); meta.setAttribute('content', 'initial-scale=1.0, maximum-scale=1.0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta);";

    const WIDTH = Dimensions.get("window").width;
    const HEIGHT = !this.state.fullscreen
      ? PixelRatio.roundToNearestPixel(WIDTH / (16 / 9))
      : Dimensions.get("window").height - StatusBar.currentHeight;
    let widthVideo = this.state.containerWidth;
    console.log("log props html", this.props.html);
    return (
      <View
        onLayout={({
          nativeEvent: {
            layout: { width, height },
          },
        }) => {
          console.log("onLayout height: ", height);
          console.log("onLayout containerWidth: ", width);
          if (!this.state.containerMounted)
            this.setState({ containerMounted: true });
          if (this.state.containerWidth !== width)
            this.setState({ containerWidth: width });
        }}
      >
        {this.state.isVideo360 == true && platform.platform != "ios" && (
          <View
            style={{
              height: HEIGHT,
              width: widthVideo,
            }}
          >
            {/* {this.state.fullscreen && (
          <Header noShadow hasTabs>
            <Left style={{ flex: 0.5 }}>
              <Button
                {...testID('button_back')}
                accessibilityLabel="button_back"
                transparent onPress={() => this.onBackPressed()}>
                <Icon name='md-arrow-back' />
              </Button>
            </Left>
            <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Title>Webinar 360</Title>
            </Body>
            <Right style={{ flex: 0.5 }}>

            </Right>
          </Header>
        )} */}

            <WebView
              // userAgent={'Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)'}
              // onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack; }}
              injectedJavaScript={disableZoom}
              javaScriptEnabledAndroid={true}
              //source={{ uri: "https://www.youtube.com/watch?v=sPyAQQklc1s" }}
              source={{
                html: this.props.html,
                baseUrl: "https://www.youtube.com",
              }}
              allowsInlineMediaPlayback={true}
              onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
              scalesPageToFit={true}
              ref={(ref) => (this.webview = ref)}
              // onLoadStart={this.onloadStart}
              // onLoadEnd={this.onloadEnd}
              style={{ flex: 1, backgroundColor: "black" }}
              onMessage={(event) => {
                console.log("event", event.nativeEvent.data);
                if (event.nativeEvent.data.includes("type=minimize")) {
                  this.minimizeWebinar(event.nativeEvent.data);
                }
              }}
            />
          </View>
        )}

        {this.state.isVideo360 == true && platform.platform == "ios" && (
          <View
            style={{
              height: HEIGHT,
              width: widthVideo,
              backgroundColor: "#D7D7D7",
              alignItems: "center",
            }}
          >
            <Image
              resizeMode="contain"
              style={{
                height: 48,
                width: 55,
                marginTop: 71,
                marginBottom: 24,
              }}
              source={require("../../assets/images/icon-detail-360.png")}
            />
            <Text
              style={{
                marginBottom: 16,
                textAlign: "center",
                fontSize: 16,
                color: "#1E1E20",
                fontFamily: "Roboto-Regular",
                lineHeight: 32,
                letterSpacing: 0.26,
              }}
            >
              {translate("video_360_hanya_tersedia_di_android")}
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
