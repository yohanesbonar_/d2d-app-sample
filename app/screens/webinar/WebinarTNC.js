import React, { Component } from 'react'
import { View, BackHandler, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Container, Header, Left, Right, Button, Icon, Body, Title, Card, Text, Row, Col, Toast } from 'native-base'
import platform from "../../../theme/variables/d2dColor";
import { testID } from "../../libs/Common"
import { ScrollView } from 'react-native-gesture-handler';
import { Modalize } from 'react-native-modalize';
import { getWebinarTNC, sendAgreementWebinarTNC } from '../../libs/NetworkUtility'
import { Loader } from '../../components'
import HTML from 'react-native-render-html';
import { result } from 'lodash';
import { getData, KEY_ASYNC_STORAGE } from '../../../src/utils';

export default class WebinarTNC extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showLoader: false,
            dataTNC: '',
            webinarId: this.props.navigation.state.params ? this.props.navigation.state.params.id : "",
            isPaid: this.props.navigation.state.params.isPaid != undefined ? this.props.navigation.state.params.isPaid : false,
            country_code: ""
        }
        this.getDataCountryCode()
    }

    getDataCountryCode = () => {
        getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
            this.setState({
                country_code: profile.country_code
            })
        });
    };

    componentDidMount() {
        console.log(this.props)
        this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
            this.onBackPressed();
            return true;
        });
        console.log("log webinarTNC this.props.navigation", this.props.navigation.state.params);
        console.log("log webinarTNC isPaid", this.state.isPaid);
        this.getDataWebinarTNC()
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    onBackPressed = () => {
        this.props.navigation.goBack();
    }


    renderContentCancelConfirmationTNC = () => {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <Col style={{
                    alignItems: 'center',
                    marginBottom: 16
                }}>
                    <Image
                        resizeMode='contain'
                        style={styles.iconWarningBottomSheetDecline}
                        source={require("../../assets/images/confirmation-warning-icon.png")}>

                    </Image>
                    <Text bold style={styles.titleBottomSheetDecline}>Don’t want to get an E-Certificate?</Text>
                    <Text regular style={styles.messageBottomSheetDecline}>If you don’t agree, you can watch the webinar but you cannot claim the E-Certificate which will be given at the end of the webinar session</Text>

                    <Row style={{
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginTop: 24
                    }}>


                        <TouchableOpacity
                            style={[styles.button, {
                                backgroundColor: "#F6F6F6",
                                marginRight: 16
                            }
                            ]} onPress={() => this.onPressDontWantTNC()} >
                            <Text style={[styles.textButtonNegative]}>Don't Want</Text>
                        </TouchableOpacity>


                        <TouchableOpacity
                            style={[styles.button, {
                                backgroundColor: "#D01E53",
                            }]} onPress={() => this.modalizeBottomSheet.close()} >
                            <Text style={[styles.textButtonPositive]}>I Want It</Text>
                        </TouchableOpacity>
                    </Row>
                </Col>
            </SafeAreaView>

        )
    }


    _renderBottomSheet = () => {
        return (
            <Modalize
                withOverlay={true}
                withHandle={false}
                adjustToContentHeight={true}
                closeOnOverlayTap={false}
                tapGestureEnabled={false}
                panGestureEnabled={false}
                closeSnapPointStraightEnabled={false}
                onBackButtonPress={() => null}

                ref={(ref) => {
                    this.modalizeBottomSheet = ref;
                }}

                overlayStyle={{
                    backgroundColor: "#000000CC",
                }}
                modalStyle={styles.modalizeContainer}
            >

                {this.renderContentCancelConfirmationTNC()}

            </Modalize>
        )
    }

    onPressButtonDecline = () => {
        if (this.state.country_code != "ID") {
            this.onBackPressed()
        }
        else {
            this.modalizeBottomSheet.open()
        }
    }

    onPressButtonAgree = () => {
        this.modalizeBottomSheet.close()
        let params = {
            webinar_id: this.state.webinarId,
            isAgree: true,
            agreement_id: this.state.dataTNC.agreement_id
        }

        this.doSendAgreementWebinarTNC(params)
    }

    doSendAgreementWebinarTNC = async (params) => {

        try {
            this.setState({
                showLoader: true,
            })
            let response = await sendAgreementWebinarTNC(params);
            console.log('sendAgreementWebinarTNC response', response)

            if (response.isSuccess) {

                this.gotoWebinarDetail()
            }
            else {
                this.setState({
                    showLoader: false
                })
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }



        } catch (error) {
            console.log(error)
            this.setState({
                showLoader: false
            })
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }

    }

    onPressDontWantTNC = () => {
        this.modalizeBottomSheet.close()
        let params = {
            webinar_id: this.state.webinarId,
            isAgree: false,
            agreement_id: this.state.dataTNC.agreement_id
        }

        this.doSendAgreementWebinarTNC(params)
    }

    getDataWebinarTNC = async () => {
        try {
            this.setState({
                showLoader: true
            })
            let params = {
                webinar_id: this.state.webinarId
            }
            let response = await getWebinarTNC(params);

            console.log('getDataWebinarTNC', response.docs)

            if (response.isSuccess == true) {
                let result = response.docs;
                this.checkUserSubmit(result);
            } else {
                this.setState({
                    showLoader: false
                })
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        } catch (error) {
            console.log(error)
            this.setState({
                showLoader: false
            })
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }

    checkUserSubmit = (result) => {
        if (result.user_submit != null) {
            this.gotoWebinarDetail();
        } else {
            this.setState({
                dataTNC: result,
                showLoader: false
            })
        }
    }

    gotoWebinarDetail = () => {
        let tempData = this.props.navigation.state.params
        console.log("gotoWebinarDetail: ", tempData)
        this.props.navigation.replace('WebinarVideo', { ...tempData })
    }

    render() {
        return (

            <Container>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >

                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                // {...testID('buttonBack')}
                                {...testID("button_back")}
                                accessibilityLabel="button_back"
                                transparent
                                onPress={() => this.onBackPressed()}
                            >
                                <Icon name="md-arrow-back" style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 2, justifyContent: "center", alignItems: "center" }}>
                            {this.state.showLoader == false && (<Title>Webinar Terms & Conditions</Title>)}
                        </Body>
                        <Right style={{ flex: 0.5 }}>

                        </Right>
                    </Header>
                </SafeAreaView>

                {this._renderBottomSheet()}

                <SafeAreaView style={{
                    flex: 1,
                    backgroundColor: "transparent"
                }}>

                    {this.state.showLoader == false && (

                        <View style={{ flex: 1, padding: 16 }}>

                            <View style={styles.cardContentTncContainer}>
                                <ScrollView
                                    style={styles.scrollViewContentTNC}
                                >
                                    <View style={styles.contentTNCContainer}>
                                        <Text {...testID("title_tnc")}
                                            style={styles.titleTNC}
                                        >
                                            {this.state.dataTNC.title}
                                        </Text>

                                        <HTML
                                            {...testID("content_tnc")}
                                            baseFontStyle={{
                                                fontFamily: 'Nunito-Regular',
                                                color: '#1E1E20',
                                                fontSize: 16
                                            }}

                                            html={this.state.dataTNC.agreement}
                                        />
                                    </View>
                                </ScrollView>
                            </View>

                            <View style={{
                                flex: 0
                            }}>
                                <Text style={styles.confirmationTNC}>{this.state.dataTNC.confirmation}</Text>
                                <Row style={styles.rowButtonContainer}>

                                    {this.state.isPaid == false && (<TouchableOpacity
                                        style={[styles.button, {
                                            backgroundColor: "#F6F6F6",
                                            marginRight: 16
                                        }
                                        ]} onPress={() => this.onPressButtonDecline()} >
                                        <Text style={[styles.textButtonNegative]}>Decline</Text>
                                    </TouchableOpacity>)}


                                    <TouchableOpacity
                                        style={[styles.button, {
                                            backgroundColor: "#D01E53",
                                        }]} onPress={() => this.onPressButtonAgree()} >
                                        <Text style={[styles.textButtonPositive]}>I Agree</Text>
                                    </TouchableOpacity>

                                </Row>
                            </View>

                        </View>

                    )}


                </SafeAreaView>

                <Loader visible={this.state.showLoader} />

            </Container >
        )
    }
}

const styles = StyleSheet.create({
    modalizeContainer: {
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        paddingHorizontal: 16,
    },
    cardContentTncContainer: {
        flex: 1,
        borderRadius: 8,
        borderColor: "#D7D7D7",
        borderWidth: 1
    },
    toolbarIcon: {
        color: "#FFFFFF",
        fontSize: 25,
    },
    button: {
        height: 48,
        flex: 1,
        borderRadius: 8,
        justifyContent: 'center',
    },
    iconWarningBottomSheetDecline: {
        height: 82,
        width: 82,
        marginVertical: 32,
    },
    titleBottomSheetDecline: {
        lineHeight: 32,
        marginBottom: 16,
        textAlign: 'center',
        fontSize: 20,
        fontFamily: "Nunito-Bold"
    },
    messageBottomSheetDecline: {
        textAlign: 'center',
        fontSize: 16,
        fontFamily: "Nunito-Regular",
        lineHeight: 26,
        color: "#1E1E20"
    },
    buttonInBottomSheetDecline: {
        flex: 1,
        height: 48,
        borderRadius: 12
    },
    confirmationTNC: {
        lineHeight: 26,
        marginVertical: 16,
        fontFamily: "Nunito-Regular",
        fontSize: 16,
        color: "#1E1E20"
    },
    textButtonPositive: {
        color: "#FFFFFF",
        fontSize: 16,
        fontFamily: "Nunito-Bold",
        lineHeight: 26,
        textAlign: 'center'
    },
    textButtonNegative: {
        color: "#1E1E20",
        fontSize: 16,
        fontFamily: "Nunito-Bold",
        lineHeight: 26,
        textAlign: 'center'
    },
    rowButtonContainer: {
        flex: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    contentTNCContainer: {
        flex: 1,
        marginRight: 16
    },
    titleTNC: {
        fontSize: 20,
        fontFamily: "Nunito-Bold",
        color: "#1E1E20",
        lineHeight: 28,
        marginBottom: 16
    },
    scrollViewContentTNC: {
        marginTop: 16,
        marginLeft: 16
    }
})