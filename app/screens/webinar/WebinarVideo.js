import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Animated,
  FlatList,
  KeyboardAvoidingView,
  BackHandler,
  Keyboard,
  Alert,
  AppState,
  Image,
  Linking,
  NativeModules,
} from "react-native";
import {
  Col,
  Button,
  Input,
  Container,
  Icon,
  Toast,
  Header,
  Title,
  Text,
  Left,
  Body,
  Right,
  Card,
  Thumbnail,
  Row,
} from "native-base";
import platform from "../../../theme/variables/d2dColor";
//import MyWebView from 'react-native-webview';
import AutoHeightWebView from "react-native-autoheight-webview";
import { Modalize } from "react-native-modalize";
import {
  ParamAction,
  ParamContent,
  doActionUserActivity,
} from "../../libs/NetworkUtility";
import {
  share,
  escapeHtml,
  testID,
  customCss,
  isToday,
  isAvailable,
  isShowInfoAvailable,
  isFinished,
  sendTopicWebinar,
  sendTopicCertificateWebinarSubmit,
  isUpcoming,
  getCountryCodeProfile,
  STORAGE_TABLE_NAME,
} from "../../libs/Common";
import _ from "lodash";
import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import RocketChat from "../../libs/rocketchat";
import Moment from "moment";
import ViewMoreText from "react-native-view-more-text";
import {
  loginRocketChatApi,
  getWebinarDetail,
} from "../../libs/NetworkUtility";
import { Loader, PopupInputCertificate } from "./../../components";
import momentTimezone from "moment-timezone";
import WebinarYoutube from "./WebinarYoutube";
import moment from "moment";
import Config from "react-native-config";
import Orientation from "react-native-orientation";
import { NavigationActions } from "react-navigation";
import { warning } from "react-native-gifted-chat/lib/utils";
import { act } from "react-test-renderer";
import Modal from "react-native-modal";
import AsyncStorage from "@react-native-community/async-storage";
import { SafeAreaView } from "react-native";
import { translate } from "../../libs/localize/LocalizeHelper";
import WebinarYoutube360 from "./WebinarYoutube360";
import Coachmark from "../../libs/coachmark/Coachmark";
const { WebinarModule } = NativeModules;
let from = {
  FastForward: "FastForward",
  Fullscreen: "Fullscreen",
  Like: "Like",
  Share: "Share",
  Question: "Question",
  BACK_BUTTON: "BACK_BUTTON",
  SHOW_DESC: "SHOW_DESC",
  HIDE_DESC: "HIDE_DESC",
  POST_TEST: "POST_TEST",
};

export default class WebinarVideo extends Component {
  // url = 'http://rahmat1929.net/livechat.php?id=';
  baseUrlAvatar = Config.BASE_URL_ROCKET_CHAT + "profile/avatar/";
  url = "https://d2d.co.id/livechat.php?id=";
  validateUrl = "https://www.youtube.com/watch?v=";
  defaultImageSpeaker =
    "https://s3-ap-southeast-1.amazonaws.com/static.guesehat.com/directories_thumb/93df4a25ec23629e3377296e62132386.jpg";
  _refWebview;
  offset = 0;
  count = 10;
  countryCode = null;
  constructor(props) {
    super(props);
    this.state = {
      fullscreen: false,
      containerMounted: false,
      containerWidth: null,
      isShowChat: false,
      showDescription: false,
      webinarId: null,
      idYoutube: "",
      title: "",
      description: "",
      photoSpeaker: null,
      nameSpeaker: "",
      totalView: "",
      shareLink: "",
      isLive: false,
      isLike: false,
      totalLike: 0,
      urlWebview: "",
      from: this.props.navigation.state.params
        ? this.props.navigation.state.params.from
        : "",
      textComment: "",
      webinarCommentId: this.props.navigation.state.params
        ? this.props.navigation.state.params.group_cid
        : "",
      isQuote: false,
      dataForQuote: {},
      isShowCardDesc: false,
      messages: [],
      isFetchingComment: false,
      isSuccessLoadComment: false,
      isFailedLoadComment: false,
      showLoaderComment: false,
      isLastPagingComment: false,
      isSuccessLoginRocketChatApi: false,
      isStartedFromNotif: false,
      isRefreshLoadChat: true,
      skpReward: null,
      pretest: 0,
      posttest: 0,
      showLoader: false,
      dataWebinarDetail: null,
      availableDateTime: null,
      isFromClickButtonGetPretest: false,
      isPretestAvailable: false,
      isPostTestAvailable: false,
      isShowInfoAvailable: false,
      isShowButtonPostestQuisionerCertificate: false,
      typeButtonPosttestQuisionerCertificate: null,
      isQuisionerDone: false,
      slug_hash: null,
      modalVisible: false,
      isSubmitted: false,
      dataCertificate: null,
      isFullscreenMode: false,
      appState: AppState.currentState,
      startDate: null,
      endDate: null,
      isComment: false,
      serverDate: null,
      typeWebinar: null,
      isPlay: true,
      dataTnC: {},
      minHeightBottomSheet: 0,
      isOpenTnc: false,
      isCheckTNC: false,
      typeContentBottomSheet: "agreement",
      isVideo360: false,
      html360: "",
      typeContentBottomSheet: "",
      seekTo: null,
      isTriggerButtonPip: false,
      isAutoShowCoachmarkFloating: false,
    };
    this.handlerStatusSubmit = this.handlerStatusSubmit.bind(this);
    this.closeGoToCertificate = this.closeGoToCertificate.bind(this);
    this.handlerFullscreenMode = this.handlerFullscreenMode.bind(this);
    countHandlePopup = 0;
  }

  setHtml360 = () => {
    this.setState({
      html360: (html = `<style>
			.current-values {
			  color: #666;
			  font-size: 12px;
			}
		  </style>
		  <!-- The player is inserted in the following div element -->
		  <div id="spherical-video-player"></div>
		  
		  <script type="text/javascript">
			var tag = document.createElement('script');
			tag.id = 'iframe-demo';
			tag.rel = 0
			tag.src = 'https://www.youtube.com/iframe_api';
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		  
			var PROPERTIES = ['yaw', 'pitch', 'roll', 'fov'];
			var updateButton = document.getElementById('spherical-properties-button');
			// Create the YouTube Player.
			var ytplayer;
			function onYouTubeIframeAPIReady() {
			  ytplayer = new YT.Player('spherical-video-player', {
				playerVars: { 'autoplay': 1, 'fs': 0, 'cc': 0, 'rel': 0, loop: 1, enablejsapi:1 },
				  height: '100%',
				  width: '100%',
				  videoId: '${this.state.dataWebinarDetail.youtube_id}',

				  events: {
					'onStateChange' : function (event) {
					  switch(event.data) {
					  case 0:
					  ytplayer.stopVideo();
					  break;
					  default:
						break;
					  }
					}
				}
			  });
			}

			function getTime() {
				var time =
				ytplayer.playerInfo.currentTime != null
					? ytplayer.playerInfo.currentTime
					: 0;
				window.ReactNativeWebView.postMessage("type=minimize" + time);
			}

			function seekToSeconds(time){
				ytplayer.seekTo(time, true);
			}
		  
			// Don't display current spherical settings because there aren't any.
			function hideCurrentSettings() {
			  for (var p = 0; p < PROPERTIES.length; p++) {
				document.getElementById(PROPERTIES[p] + '-current-value').innerHTML = '';
			  }
			}
		  
			// Retrieve current spherical property values from the API and display them.
			function updateSetting() {
			  if (!ytplayer || !ytplayer.getSphericalProperties) {
				hideCurrentSettings();
			  } else {
				let newSettings = ytplayer.getSphericalProperties();
				if (Object.keys(newSettings).length === 0) {
				  hideCurrentSettings();
				} else {
				  for (var p = 0; p < PROPERTIES.length; p++) {
					if (newSettings.hasOwnProperty(PROPERTIES[p])) {
					  currentValueNode = document.getElementById(PROPERTIES[p] +
																 '-current-value');
					  currentValueNode.innerHTML = ('current: ' +
						  newSettings[PROPERTIES[p]].toFixed(4));
					}
				  }
				}
			  }
			  requestAnimationFrame(updateSetting);
			}
			updateSetting();
		  
			// Call the API to update spherical property values.
			updateButton.onclick = function() {
			  var sphericalProperties = {};
			  for (var p = 0; p < PROPERTIES.length; p++) {
				var propertyInput = document.getElementById(PROPERTIES[p] + '-property');
				sphericalProperties[PROPERTIES[p]] = parseFloat(propertyInput.value);
			  }
			  ytplayer.setSphericalProperties(sphericalProperties);
			}
		  </script>`),
    });
  };

  handlerFullscreenMode(fullscreenMode) {
    console.log("handlerFullscreenMode: ", fullscreenMode);
    this.setState({
      isFullscreenMode: fullscreenMode,
    });
  }

  handlerStatusSubmit(dataCertificate) {
    this.setState({
      isSubmitted: true,
      dataCertificate: dataCertificate,
    });
  }

  closeGoToCertificate = async () => {
    AppState.removeEventListener("change", this._handleAppStateChange);

    let data = null;
    if (!_.isEmpty(this.state.dataCertificate)) {
      data = {
        title: this.state.dataCertificate.event_title,
        typeCertificate: "webinar",
        certificate: this.state.dataCertificate.filename,
        isCertificateAvailable: true,
      };
      this.gotoCertificated(data);
    }
  };

  toggleModal = (visible) => {
    this.setState({
      modalVisible: visible,
    });
  };

  animated = new Animated.Value(0);
  translateY = this.animated.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });

  isConnectRocketChat = async () => {
    return await RocketChat.connect();
  };

  onPullRefresh = async () => {
    let tempYoutubeId = this.state.dataWebinarDetail.youtube_id;

    if (this.state.messages.length == 0) {
      this.onRefreshLoadChat();
    }
    await this.getDataWebinarDetail(this.state.webinarId);

    if (tempYoutubeId != this.state.dataWebinarDetail.youtube_id) {
      this.refWebinarYoutube.onRefreshDataYoutubeVideo(
        this.state.dataWebinarDetail.youtube_id
      );
    }

    this.refWebinarYoutube.onRefreshWebinarDetail();
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    let newState = this.state.dataWebinarDetail;

    let isFromNotifWebinarSubmit = false;

    let flagParamTargetNotif = this.props.navigation.state.params
      .typeTargetNotif;

    if (
      flagParamTargetNotif != null &&
      typeof flagParamTargetNotif != "undefined"
    ) {
      console.log("flagParamTargetNotif: ", flagParamTargetNotif);
      isFromNotifWebinarSubmit = flagParamTargetNotif.includes("webinar_submit")
        ? true
        : false;
    }

    if (newState != null) {
      if (isFromNotifWebinarSubmit) {
        if (newState.posttest_passed == 1) {
          if (
            !newState.quisioner_passed &&
            newState.quisioner_link != null &&
            newState.quisioner_link != ""
          ) {
            this.gotoQuisioner();
          } else {
            countHandlePopup++;
            if (countHandlePopup == 1) {
              this.toggleModal(true);
            }
          }
        }
      }
    }
  }

  componentDidMount() {
    // console.log('state param -- ', this.props.navigation.state.params);
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
    this.didBlurListener = this.props.navigation.addListener(
      "didBlur",
      (payload) => {
        //to stop webinar video
        this.setState({
          dataWebinarDetail: null,
        });
        this.props.navigation.state.params.typeTargetNotif = undefined;
      }
    );

    let numbOnceExecute = 0;

    this.didFocusListener = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        if (numbOnceExecute != 0) {
          this.doInitParams();
        }
      }
    );

    // this.doInitParams();

    numbOnceExecute = 1;
    this.loginRocketChat();
    this.startOrNot();
    this.checkCoachmarkFloating();

    AppState.addEventListener("change", this._handleAppStateChange);
    Orientation.unlockAllOrientations();
  }

  onBackPressed = () => {
    console.log("onBackPressed");

    if (this.state.isFullscreenMode) {
      if (!this.state.isVideo360) {
        this.refWebinarYoutube.doChangeFullScreen(false);
      } else {
        this.refWebinarYoutube360.doChangeFullScreen(false);
      }
    } else {
      this.sendAdjust(from.BACK_BUTTON);
      this.props.navigation.goBack();
      RocketChat.logout();
      Orientation.lockToPortrait();
    }
  };

  componentWillUnmount() {
    this.backHandler.remove();
    this.didFocusListener.remove();
    this.didBlurListener.remove();

    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    let that = this;
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      if (!this.state.isVideo360) {
        console.log("App has come to the foreground!");
        this.refWebinarYoutube.onResumePlay();
      }
      if (WebinarModule != null) {
        WebinarModule.stopAndBackToAppWebinar((type, time, videoId) => {
          // Toast.show({
          //   text:
          // 	"callback : " + type + " --- " + time + " --- " + videoId,
          //   position: "top",
          //   duration: 3000,
          // });
          console.log("------callback", time);
          that.setState({ seekTo: time });
        });
      }
    }
    this.setState({ appState: nextAppState });
  };

  async loginRocketChat() {
    let groupChatId = this.state.webinarCommentId;
    console.log(this.state.webinarCommentId);
    this.setState({
      isFailedLoadComment: false,
      isFetchingComment: true,
    });

    try {
      let response = null;

      response = await loginRocketChatApi(groupChatId);

      console.log("loginRocketChat response", response);
      if (response.isSuccess) {
        this.initRocketChat(response.docs.roomId, response.docs.username);
        this.setState({
          isSuccessLoginRocketChatApi: true,
          isRefreshLoadChat: false,
        });
      } else {
        console.log(response.message);
        this.setState({
          isSuccessLoginRocketChatApi: false,
          isRefreshLoadChat: false,
          showLoaderComment: false,
          isFetchingComment: false,
          isFailedLoadComment: true,
        });
      }
    } catch (error) {
      console.log(error);
      this.setState({
        isSuccessLoginRocketChatApi: false,
        showLoaderComment: false,
        isFetchingComment: false,
        isFailedLoadComment: true,
        isRefreshLoadChat: true,
      });
    }
  }

  sendAdjust = (action) => {
    let token = "";

    switch (action) {
      case from.FastForward:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Webinar_FastForward;
        else if (this.state.from == "Webinar")
          token = AdjustTrackerConfig.Webinar_Video_FF;
        break;
      case from.Fullscreen:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Webinar_FullScreen;
        else if (this.state.from == "Webinar")
          token = AdjustTrackerConfig.Webinar_Video_Fullscreen;
        break;
      case from.Like:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Webinar_Like;
        else if (this.state.from == "Webinar")
          token = AdjustTrackerConfig.Webinar_Video_Like;
        break;
      case from.Question:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Webinar_Question;
        else if (this.state.from == "Webinar")
          token = AdjustTrackerConfig.Webinar_Video_Question;
        break;
      case from.Share:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Webinar_Share;
        else if (this.state.from == "Webinar")
          token = AdjustTrackerConfig.Webinar_Video_Share;
        break;
      case from.BACK_BUTTON:
        token = AdjustTrackerConfig.Webinar_Video_BackButton;
        break;
      case from.SHOW_DESC:
        token = AdjustTrackerConfig.Webinar_Video_ShowDesc;
        break;
      case from.HIDE_DESC:
        token = AdjustTrackerConfig.Webinar_Video_HideDesc;
        break;
      case from.POST_TEST:
        token = AdjustTrackerConfig.Webinar_Video_Posttest;
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  async doViewEvent(webinarId) {
    let viewEvent = await doActionUserActivity(
      ParamAction.VIEW,
      ParamContent.WEBINAR,
      webinarId
    );
  }
  actionLike = async (isLike, webinarId) => {
    // Adjust Tracker
    // AdjustTracker(AdjustTrackerConfig.Webinar_Video_Like);
    this.sendAdjust(from.Like);
    if (!this.isSynchronize) {
      this.isSynchronize = true;
      this.setState({
        isLike: isLike,
        totalLike: isLike ? this.state.totalLike + 1 : this.state.totalLike - 1,
      });
      let response = await doActionUserActivity(
        isLike ? ParamAction.LIKE : ParamAction.UNLIKE,
        ParamContent.WEBINAR,
        webinarId
      );
      if (response.isSuccess == false) {
        Toast.show({ text: response.message, position: "top", duration: 3000 });
        this.setState({
          isLike: !isLike,
          totalLike: !isLike
            ? this.state.totalLike + 1
            : this.state.totalLike - 1,
        });
      }
      this.isSynchronize = false;
    }
  };
  isSynchronize = false;

  async roomDetails() {
    let roomName = this.state.webinarCommentId;
    let { messages } = this.state;

    try {
      let result = await RocketChat.getRoomDetails(
        roomName,
        this.offset,
        this.count
      );
      console.log("result: ", result);

      let { success } = result;
      if (success) {
        messages = _.unionWith(messages, result.messages, _.isEqual);
        messages = messages.filter((data) => !("t" in data));
        console.log("messages filtered: ", messages);

        this.setState({
          messages,
          showLoaderComment: false,
          isFetchingComment: false,
          isFailedLoadComment: false,
          isLastPagingComment:
            result.messages.length < this.count ? true : false,
        });
      } else {
        this.setState({
          messages,
          showLoaderComment: false,
          isFetchingComment: false,
          isFailedLoadComment: true,
          isLastPagingComment: false,
        });
      }
    } catch (error) {
      console.log(error);
      //Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
      this.setState({
        showLoaderComment: false,
        isFetchingComment: false,
        isFailedLoadComment: true,
      });
    }
  }

  async resultUpdateSubscribeRoom(ddpMessage) {
    let roomName = this.state.webinarCommentId;
    let { messages } = this.state;

    try {
      let newMessage = ddpMessage.fields.args[0];
      let ts = new Date(ddpMessage.fields.args[0].ts.$date);
      console.log("ts: ", ts);
      newMessage.ts = ts;
      messages.unshift(newMessage);

      await this.setState({
        messages: messages,
      });

      console.log("resultUpdateSubscribeRoom stateMessage: ", messages);
    } catch (error) {
      console.log(error);
    }
  }

  async initRocketChat(roomId, username) {
    let resultConnectRocketChat = await this.isConnectRocketChat();
    if (resultConnectRocketChat) {
      this.setState({
        isRefreshLoadChat: false,
      });
      console.log("resultConnectRocketChat: ", resultConnectRocketChat);
      await RocketChat.login(username);
      this.roomDetails();
      await RocketChat.subscribeRoom({ rid: roomId }, this);
    } else {
      this.setState({
        isRefreshLoadChat: true,
      });
    }
  }

  onLongPressMessageItem(dataForQuote) {
    console.log("onLongPressMessageItem: ", dataForQuote);
    this.setState({
      isQuote: true,
      dataForQuote: dataForQuote,
    });
  }

  async onPressSendMessage(channel, text) {
    const permalink = `https://staging-rocketchat.guesehat.com/group/${channel}?msg=${
      this.state.dataForQuote._id
    }`;

    let { isQuote } = this.state;

    if (isQuote) {
      text = `[ ](${permalink}) ${text}`;
    }

    if (!_.isEmpty(text.trim())) {
      console.log("kirim");
      this.setState({
        textComment: "",
        isQuote: false,
      });

      let resultSendMessage = await RocketChat.sendMessage(channel, text);
      console.log("resultSendMessage: ", resultSendMessage);
      if (resultSendMessage.success) {
        this.offset = 0;
        Keyboard.dismiss();
      }
      // await this.sendMessageToRocketChatApi(channel, text)

      this.scrollToTop();
    } else {
      console.log("tidak boleh kosong");
    }
  }

  scrollToTop = () => {
    this.flatListRef.scrollToIndex({ animated: true, index: 0 });
  };

  doInitParams = async () => {
    let { params } = this.props.navigation.state;
    console.log("params: ", params);

    this.countryCode = await getCountryCodeProfile();

    if (params != null) {
      // this.setState({
      //   isLive: params.isLive == true ? true : false,
      // })

      this.doViewEvent(params.id);
      await this.getDataWebinarDetail(params.id);
      AdjustTracker(
        AdjustTrackerConfig.Webinar_Onload_Page,
        params.title,
        params.id
      );
    }
  };

  async getDataWebinarDetail(idWebinar) {
    this.setState({ isFailed: false, isFetching: true, showLoader: true });

    try {
      let params = {
        id: idWebinar,
      };
      let response = await getWebinarDetail(params);
      let getUid = await AsyncStorage.getItem("UID");

      console.log("getDataWebinarDetail response", response);

      if (response.isSuccess == true) {
        let result = response.docs;

        await this.setState({
          serverDate: result.server_date,
        });
        console.log("this.state.serverDate: ", this.state.serverDate);

        sendTopicWebinar(result.slug_hash);

        let authorName = _.isEmpty(_.trim(result.author))
          ? "D2D"
          : result.author;
        let authorPhoto =
          result.author_photo != null && result.author_photo != ""
            ? result.speaker_photo
            : this.defaultImageSpeaker;
        let typeButtonPosttestQuisionerCertificate = "";
        let formatDate = "YYYY-MM-DD HH:mm:ss";

        resultAvailableDate = momentTimezone(
          result.posttest_start,
          formatDate
        ).format("DD MMM YYYY HH:mm");

        if (result.posttest_passed == 0 && result.posttest == 1) {
          await this.setState({
            availableDateTime: isToday(result.posttest_start)
              ? result.posttest_start.substr(11, 5)
              : resultAvailableDate,
          });
        }

        await this.setState({
          typeWebinar: isUpcoming(result.server_date, result.start_date)
            ? "upcoming"
            : null,
        });

        let typePretestPosttest = null;
        if (
          result.pretest_passed == 0 &&
          result.pretest == 1 &&
          isAvailable(
            this.state.serverDate,
            result.pretest_start,
            result.pretest_end
          )
        ) {
          typeButtonPosttestQuisionerCertificate = "pretest";
          typePretestPosttest = "pretest";
        } else if (
          result.posttest_passed == 0 &&
          result.posttest == 1 &&
          result.posttest_count_attempt < result.posttest_max_attempt
        ) {
          typeButtonPosttestQuisionerCertificate = "posttest";
          typePretestPosttest = "posttest";
        } else if (
          !result.quisioner_passed &&
          result.quisioner_link != null &&
          result.quisioner_link != ""
        ) {
          typeButtonPosttestQuisionerCertificate = "quisioner";
        } else {
          typeButtonPosttestQuisionerCertificate = "certificated";
        }

        result.typePretestPosttest = typePretestPosttest;
        let isShowButton = false;
        //if (result.skp_reward != null) {
        if (this.countryCode != "ID" && result.ecertificate_active == true) {
          isShowButton = true;
          typeButtonPosttestQuisionerCertificate = "certificated";
        } else {
          if (result.posttest_passed == 0) {
            if (result.posttest_count_attempt < result.posttest_max_attempt) {
              if (!isFinished(this.state.serverDate, result.posttest_end)) {
                isShowButton = true;
              }
            } else if (
              result.posttest_count_attempt >= result.posttest_max_attempt
            ) {
              if (result.quisioner_passed == 0) {
                if (
                  result.quisioner_link != null &&
                  result.quisioner_link != ""
                ) {
                  isShowButton = true;
                }
              }
            }
          } else if (result.posttest_passed == 1) {
            isShowButton = true;
          }
        }

        //}

        result.quisioner_link =
          result.quisioner_link != null && result.quisioner_link != ""
            ? result.quisioner_link + `?uid=${getUid}`
            : "";

        await this.setState({
          isLive: isAvailable(
            this.state.serverDate,
            result.start_date,
            result.end_date
          ),
          dataWebinarDetail: result,
          startDate: result.start_date,
          endDate: result.end_date,
          isComment: result.isComment,
          webinarId: result.id,
          isShowInfoAvailable:
            isShowInfoAvailable(this.state.serverDate, result.posttest_end) &&
            (result.posttest_passed == 0 &&
              result.posttest_count_attempt < result.posttest_max_attempt),
          isShowButtonPostestQuisionerCertificate: isShowButton,
          // && (result.posttest_passed == 0 && result.posttest_count_attempt < result.posttest_max_attempt)) ||result.quisioner_passed==0 || ,
          isPostTestAvailable: isAvailable(
            this.state.serverDate,
            result.posttest_start,
            result.posttest_end
          ),
          isPretestAvailable: isAvailable(
            this.state.serverDate,
            result.pretest_start,
            result.pretest_end
          ),
          title: result.title,
          description: result.description,
          photoSpeaker: authorPhoto,
          nameSpeaker:
            isAvailable(
              this.state.serverDate,
              result.start_date,
              result.end_date
            ) == true
              ? authorName + " . Live"
              : this.state.typeWebinar == "upcoming"
              ? authorName + " . Upcoming"
              : authorName + " . Recorded",
          shareLink: result.url[0].app_link,
          // totalView: params.flag_view,
          // totalLike: params.flag_like,
          totalView: result.total_view,
          totalLike: result.total_like,
          isLike: result.flag_like > 0 ? true : false,
          skpReward: result.skp_reward,
          pretest: result.pretest,
          posttest: result.posttest,
          typeButtonPosttestQuisionerCertificate: typeButtonPosttestQuisionerCertificate,
          isQuisionerDone: result.quisioner_passed,
          slug_hash: result.slug_hash,
          isVideo360: result.video_360,
        });
        this.setHtml360();

        this.checkIOS360();

        if (result.posttest_passed == 1) {
          await sendTopicCertificateWebinarSubmit(result.slug_hash);
        }

        if (
          result.pretest_count_attempt >= result.pretest_max_attempt &&
          result.pretest_passed == 0
        ) {
          //to stop webinar youtube
          await this.setState({
            isPlay: false,
          });
          this.gotoResult();
        }

        //move to pretest is pretest is on
        else if (
          isAvailable(
            this.state.serverDate,
            result.pretest_start,
            result.pretest_end
          )
        ) {
          if (
            result.pretest == 1 &&
            result.pretest_count_attempt < result.pretest_max_attempt &&
            result.pretest_passed == 0 &&
            this.state.isFromClickButtonGetPretest == false
          ) {
            //to stop webinar youtube
            await this.setState({
              isPlay: false,
            });
            this.gotoQuiz();
          }
        }

        this.setState({ showLoader: false });
      } else {
        this.setState({ showLoader: false });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      console.log(error);
      this.setState({ showLoader: false });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  checkIOS360 = () => {
    if (this.state.isVideo360 == true && platform.platform == "ios") {
      this.setState({ typeContentBottomSheet: "ios_360" });
      this.modalizeBottomSheet.open();
    }
  };
  renderBoxMessage() {
    let roomName = this.state.webinarCommentId;
    // console.log('renderBoxMessage isSuccessLoginRocketChatApi: ', this.state.isSuccessLoginRocketChatApi)
    // console.log('renderBoxMessage this.state.isLive: ', this.state.isLive)
    // console.log('renderBoxMessage !_.isEmpty(roomName): ', !_.isEmpty(roomName))

    return (this.state.isLive == true || this.state.isComment == true) &&
      this.state.isSuccessLoginRocketChatApi &&
      !this.state.isFullscreenMode ? (
      <View style={styles.messageBoxContainer}>
        <Input
          style={{ paddingLeft: 10, backgroundColor: "white" }}
          value={this.state.textComment}
          onChangeText={(text) => this.setState({ textComment: text })}
          onSubmitEditing={(event) =>
            this.onPressSendMessage(roomName, this.state.textComment)
          }
          editable={true}
          multiline={false}
          placeholder="Write Your Message Here"
        />
        <TouchableOpacity
          {...testID("button_send_comment")}
          style={{
            backgroundColor: "#D40349",
            alignSelf: "stretch",
            justifyContent: "center",
          }}
          onPress={() =>
            this.sendAdjust(from.Question) +
            this.onPressSendMessage(roomName, this.state.textComment)
          }
        >
          <Icon
            name="send"
            type="MaterialIcons"
            style={{ marginHorizontal: 20, color: "white" }}
          />
        </TouchableOpacity>
      </View>
    ) : null;
  }

  rendertItemCommentSeparator() {
    return (
      <View
        style={{
          marginTop: 10,
          height: 0.5,
          width: "100%",
          backgroundColor: "#00000029",
        }}
      />
    );
  }

  renderViewMore(onPress) {
    return (
      <Text
        style={{ fontFamily: "Nunito-Bold", textDecorationLine: "underline" }}
        onPress={onPress}
      >
        View more
      </Text>
    );
  }

  renderViewLess(onPress) {
    return (
      <Text
        style={{ fontFamily: "Nunito-Bold", textDecorationLine: "underline" }}
        onPress={onPress}
      >
        View less
      </Text>
    );
  }

  renderEmptyItem = () => {
    //console.log('renderEmptyItem: ', this.state.messages.length)
    if (this.state.messages.length == 0) {
      if (this.state.isSuccessLoginRocketChatApi) {
      }
      let messageEmptyCommentPage = "";
      if (this.state.isSuccessLoginRocketChatApi != true) {
        //   messageEmptyCommentPage = 'Gagal memuat komentar'
        messageEmptyCommentPage = translate("komentar_tidak_tersedia");
      } else {
        if (
          this.state.isLive == true &&
          isAvailable(
            this.state.serverDate,
            this.state.startDate,
            this.state.endDate
          )
        ) {
          messageEmptyCommentPage = translate("yuk_tulis_komentar");
        } else {
          messageEmptyCommentPage = translate("komentar_tidak_tersedia");
        }
      }
      // this.state.isSuccessLoginRocketChatApi && this.state.isLive ? 'Yuk, tulis komentar' : 'Komentar tidak tersedia'
      //console.log('messageEmptyCommentPage: ', messageEmptyCommentPage)
      return (
        <View style={styles.emptyItem}>
          <Text>
            {this.state.isFetchingComment == true
              ? "Loading..."
              : messageEmptyCommentPage}
          </Text>
        </View>
      );
    }
  };

  renderMessageForQuote() {
    {
      return this.state.isQuote ? (
        <View style={styles.messageForQuoteContainer}>
          <Col style={{ paddingLeft: 10 }}>
            <Text>{this.state.dataForQuote.u.username} said: </Text>
            <Text style={styles.replyText} numberOfLines={2}>
              {this.convertToMessage(this.state.dataForQuote.msg)}
            </Text>
          </Col>
          <TouchableOpacity
            style={{
              backgroundColor: "transparent",
              alignSelf: "stretch",
              justifyContent: "center",
            }}
            onPress={() => this.setState({ isQuote: false })}
          >
            <Icon
              name="md-close"
              type="Ionicons"
              style={{ marginHorizontal: 20, color: "white" }}
            />
          </TouchableOpacity>
        </View>
      ) : null;
    }
  }

  renderItemWebinarComment(item) {
    // let avatar = this.baseUrlAvatar + item.u.username + '?' + momentTimezone().unix();
    //console.log('avatar: ', avatar)
    return "t" in item ? null : (
      <View
        style={{
          backgroundColor: "#F6F6FA",
          marginVertical: 10,
          marginHorizontal: 20,
        }}
      >
        <Row>
          {/* <Thumbnail
            style={{ height: 28, width: 28, backgroundColor: '#DDDDDD' }}
            source={{ uri: avatar }}
          //onLoad={(event) => console.log('onLoad: ', event)}

          /> */}

          <Col style={{ marginLeft: 10 }}>
            <Text style={{ fontSize: 18, fontWeight: "bold" }}>
              {item.u.name ? item.u.name : item.u.username}
            </Text>

            {this.renderAttachments(item)}

            <ViewMoreText
              numberOfLines={2}
              renderViewMore={this.renderViewMore}
              renderViewLess={this.renderViewLess}
            >
              <Text
                style={{
                  fontSize: 14,
                  marginTop: 5,
                  fontFamily: "Nunito-Regular",
                  color: "#111111",
                  opacity: 10,
                }}
              >
                {this.convertToMessage(item.msg)}
              </Text>
            </ViewMoreText>
          </Col>
          <Text note>
            {Moment(momentTimezone.tz(item.ts, "Asia/Jakarta")).fromNow()}
          </Text>
        </Row>
        {this.rendertItemCommentSeparator()}
      </View>
    );
  }

  convertToMessage = (item) => {
    let message = item;
    message = message.replace(/^\[([\s]]*)\]\(([^)]*)\)\s/, "").trim();
    return message;
  };

  //render teks yang diquote/file attachments
  renderAttachments = (item) => {
    if (item.attachments) {
      // console.log('item.attachments', item.attachments);
      // return null;
      if (item.attachments[0]) {
        if (item.attachments[0].text) {
          // console.log(
          //     'item.attachments[0].text',
          //     item.attachments[0].text,
          // );
          return (
            // <Card style={{ backgroundColor: 'white' }}>
            //   <Text style={{ marginHorizontal: 10, marginTop: 5, fontWeight: 'bold' }}>
            //     {item.attachments[0].author_name}
            //   </Text>
            //   <View style={{ margin: 15 }}>

            //     <Text>

            //       {this.convertToMessage(
            //         item.attachments[0].text,
            //       )}

            //     </Text>
            //   </View>
            // </Card>

            null
          );
        }
      }
    }
    // console.log('item.msg', item.msg);
  };

  startOrNot() {
    let { params } = this.props.navigation.state;
    let startDate = params.start_date;
    // console.log('startOrNot startDate: ', startDate)
    let month = 0;
    if (new Date().getMonth() < 10) {
      month = new Date().getMonth() + 1;
      month = "0" + month;
    } else {
      month = new Date().getMonth();
    }

    let minutes = 0;
    if (new Date().getMinutes() < 10) {
      minutes = new Date().getMinutes();
      minutes = "0" + minutes;
    } else {
      minutes = new Date().getMinutes();
    }

    let hour = 0;
    if (new Date().getHours() < 10) {
      hour = new Date().getHours();
      hour = "0" + hour;
    } else {
      hour = new Date().getHours();
    }

    let date = 0;
    if (new Date().getDate() < 10) {
      date = new Date().getDate();
      date = "0" + date;
    } else {
      date = new Date().getDate();
    }
    let currentDate =
      new Date().getFullYear() +
      "-" +
      month +
      "-" +
      new Date().getDate() +
      " " +
      new Date().getHours() +
      ":" +
      new Date().getMinutes();

    console.log(
      "startOrNot  currentDate: ",
      new Date().getFullYear() +
        "-" +
        month +
        "-" +
        date +
        " " +
        hour +
        ":" +
        minutes
    );

    let timestampStartDate = new Date(startDate).getTime() / 1000;

    let timestampcurrentDate = new Date(currentDate).getTime() / 1000;

    // console.log('timestampStartDate: ', timestampStartDate)
    // console.log('timestampCurrentDate: ', timestampcurrentDate)
    // console.log('this.state.isLive: ', this.state.isLive)
    let isStarted =
      timestampcurrentDate > timestampStartDate && this.state.isLive == true
        ? true
        : false;
    console.log("isStarted: ", isStarted);
    this.setState({
      isStartedFromNotif: isStarted,
    });
  }

  async loadMoreWebinarComment() {
    // console.log('loadMoreWebinarComment offset: ', this.offset);
    // console.log('loadMoreWebinarComment this.state.isLastPagingComment: ', this.state.isLastPagingComment);
    if (this.state.isLastPagingComment == true) {
      return;
    }
    this.offset =
      this.state.isFailedLoadComment == true
        ? this.offset
        : this.offset + this.count;
    this.roomDetails();
  }

  _renderItemFooter = () => (
    <View
      style={{
        height: this.state.isFetchingComment == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader()}
    </View>
  );

  _renderItemFooterLoader() {
    if (this.state.isFailedLoadComment == true) {
      return (
        <TouchableOpacity onPress={() => this.loadMoreWebinarComment()}>
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetchingComment} transparent />;
  }

  renderWebinarComment() {
    // console.log('this.state.messages.length : ', this.state.messages.length)
    // console.log('this.state.isRefreshLoadChat: ', this.state.isRefreshLoadChat)
    return (
      <FlatList
        ref={(ref) => {
          this.flatListRef = ref;
        }}
        inverted={false}
        data={this.state.messages}
        keyExtractor={(item) => item._id}
        onEndReachedThreshold={0.5}
        onEndReached={() => this.loadMoreWebinarComment()}
        ListHeaderComponent={this.renderCardDescription}
        ListFooterComponent={this._renderItemFooter}
        ListEmptyComponent={this.renderEmptyItem}
        renderItem={({ item }) => this.renderItemWebinarComment(item)}
        onRefresh={() => this.onPullRefresh()}
        refreshing={this.state.isRefreshLoadChat}
      />
    );
  }

  async onRefreshLoadChat() {
    console.log("onRefreshLoadChat");
    this.setState({
      isRefreshLoadChat: true,
    });

    await this.loginRocketChat();
  }

  onPressButtonPostTest = async () => {
    //check is there pretest or still not.
    await this.setState({
      isFromClickButtonGetPretest: true,
    });

    if (this.state.typeButtonPosttestQuisionerCertificate == "posttest") {
      this.sendAdjust(from.POST_TEST);

      let data = {
        title: "Warning",
        message: "Silakan kerjakan pretest terlebih dahulu",
        onPress: "goToQuiz",
      };

      await this.getDataWebinarDetail(this.state.webinarId);

      if (this.state.typeButtonPosttestQuisionerCertificate == "posttest") {
        if (
          this.state.dataWebinarDetail.posttest_count_attempt >=
          this.state.dataWebinarDetail.posttest_max_attempt
        ) {
          data.message = `Anda sudah mencapai limit mengerjakan Posttest`;
          data.onPress = "dismiss";
          this.showAlert(data);
        } else if (this.state.isPostTestAvailable) {
          this.gotoQuiz();
        } else if (
          isFinished(
            this.state.serverDate,
            this.state.dataWebinarDetail.posttest_end
          )
        ) {
          data.message = `Posttest telah berakhir`;
          data.onPress = "dismiss";
          this.showAlert(data);
        } else {
          data.message = `Posttest akan tersedia pada ${
            isToday(this.state.dataWebinarDetail.posttest_start) ? "pukul " : ""
          } ${this.state.availableDateTime} WIB`;
          data.onPress = "dismiss";
          this.showAlert(data);
        }
      } else {
        this.showAlert(data);
      }
    } else if (
      this.state.typeButtonPosttestQuisionerCertificate == "quisioner"
    ) {
      this.gotoQuisioner();
    } else if (
      this.state.typeButtonPosttestQuisionerCertificate == "certificated"
    ) {
      let data = "";
      if (this.state.dataWebinarDetail.template_certificate == 0) {
        data = {
          title: "",
          typeCertificate: "webinar",
          certificate: "",
          isCertificateAvailable: false,
        };
        this.gotoCertificated(data);
      } else {
        if (this.state.dataWebinarDetail.certificate != null) {
          data = {
            typeCertificate: "webinar",
            isCertificateAvailable: true,
            ...this.state.dataWebinarDetail,
          };
          this.gotoCertificated(data);
        } else {
          //show popup
          this.setState({
            modalVisible: true,
          });
        }
      }
    }
  };

  gotoResult = () => {
    AppState.removeEventListener("change", this._handleAppStateChange);

    let params = {
      ...this.state.dataWebinarDetail,
    };
    params.from = "Webinar";

    params.result_time = params.pretest_result_time;
    params.is_pass = false;
    params.count_attempt = params.pretest_count_attempt;

    this.props.navigation.replace("TestResult", { ...params });
  };

  gotoQuisioner = () => {
    AppState.removeEventListener("change", this._handleAppStateChange);

    let params = {
      ...this.state.dataWebinarDetail,
    };
    params.from = "Webinar";

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: "Quisioner",
        params: params,
      })
    );
  };

  gotoCertificated = (data) => {
    AppState.removeEventListener("change", this._handleAppStateChange);

    data.from = "Webinar";
    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: "CmeCertificate",
        params: data,
      })
    );
  };

  gotoQuiz = () => {
    AppState.removeEventListener("change", this._handleAppStateChange);

    let params = {
      ...this.state.dataWebinarDetail,
    };
    params.from = "Webinar";

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: "CmeQuiz",
        params: params,
      })
    );
  };

  onPressOkAlert = (action) => {
    if (action == "dismiss") {
      null;
    } else if (action == "goToQuiz") {
      if (this.state.isPretestAvailable) {
        this.gotoQuiz();
      }
    }
  };

  showAlert = (data) => {
    Alert.alert(
      data.title,
      data.message,
      [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: "OK",
          onPress: () => this.onPressOkAlert(data.onPress),
        },
      ],
      { cancelable: false }
    );
  };

  renderInfoRewardSkp = () => {
    return (
      <Text
        style={{
          color: "#1690D4",
          fontFamily: "Nunito-SemiBold",
          fontSize: 11,
          marginLeft: 16,
        }}
      >
        E-Certificate {this.state.skpReward > 0 ? this.state.skpReward : ""} SKP
      </Text>
    );
  };

  renderLabelAvailable = () => {
    return (
      this.state.isShowInfoAvailable && (
        <View
          style={{
            flex: 1,
            alignSelf: "center",
          }}
        >
          <Row
            style={{
              backgroundColor: "#B1A5A9",
              alignSelf: "baseline",
              justifyContent: "space-between",
              paddingVertical: 5,
              paddingHorizontal: 16,
              borderRadius: 8,
              alignItems: "center",
              marginBottom: 16,
            }}
          >
            <Icon
              type="MaterialCommunityIcons"
              name={"information"}
              style={{
                fontSize: 16,
                color: "#FFFFFF",
              }}
            />

            <Text
              style={{
                fontFamily: "Nunito-Regular",
                textAlign: "center",
                color: "#FFFFFF",
              }}
            >
              {`Available at ${this.state.availableDateTime} WIB`}
            </Text>
          </Row>
        </View>
      )
    );
  };

  renderAreaGetPretestPostest = () => {
    let labelButton = "";

    if (this.state.typeButtonPosttestQuisionerCertificate == "posttest") {
      labelButton = "GET POSTTEST";
    } else if (
      this.state.typeButtonPosttestQuisionerCertificate == "quisioner"
    ) {
      labelButton = "Kuesioner";
    } else if (
      this.state.typeButtonPosttestQuisionerCertificate == "certificated"
    ) {
      labelButton = "Certificated";
    }

    return (
      this.state.isShowButtonPostestQuisionerCertificate && (
        <Col
          style={{
            paddingHorizontal: 12,
          }}
        >
          <Button
            // {...testID('buttonSave')}
            accessibilityLabel="button_pretest_postest"
            style={{
              borderRadius: 8,
              marginBottom: 16,
            }}
            onPress={() => this.onPressButtonPostTest()}
            success={this.countryCode != "ID" ? false : true}
            block
          >
            <Text textButton>{labelButton}</Text>
          </Button>

          {this.renderLabelAvailable()}
        </Col>
      )
    );
  };

  onPressShowHideDesc = () => {
    if (this.state.showDescription) {
      this.sendAdjust(from.HIDE_DESC);
    } else {
      this.sendAdjust(from.SHOW_DESC);
    }
    this.setState((s) => ({ showDescription: !s.showDescription }));
  };

  renderCardDescription = () => {
    let uriImageSpeaker =
      this.state.photoSpeaker != null && this.state.photoSpeaker != ""
        ? { uri: this.state.photoSpeaker }
        : { uri: this.defaultImageSpeaker };

    return (
      <View style={styles.containterCardDesc}>
        <Card>
          <View style={styles.viewTitleDesc}>
            {this.state.photoSpeaker != null && (
              <Thumbnail style={styles.roundedImage} source={uriImageSpeaker} />
            )}
            <View style={styles.viewTitleDesc2}>
              <Text
                {...testID("judul_webinar")}
                style={styles.textTitle}
                numberOfLines={2}
              >
                {this.state.title}
              </Text>
              <Row
                style={{
                  alignItems: "center",
                }}
              >
                <Text style={styles.textDesc} numberOfLines={1}>
                  {this.state.nameSpeaker}
                </Text>

                {this.state.skpReward != null && this.renderInfoRewardSkp()}
              </Row>
            </View>
          </View>
          <View style={styles.viewMain3Icon}>
            <View style={styles.viewPerIcon}>
              <Icon name="md-eye" style={styles.iconShare} />
              <Text style={styles.descIcon}>{this.state.totalView}</Text>
            </View>
            <TouchableOpacity
              // {...testID('buttonLike')}
              accessibilityLabel="button_like"
              onPress={() =>
                this.actionLike(!this.state.isLike, this.state.webinarId)
              }
              style={[styles.viewPerIcon, styles.viewCenterPerIcon]}
            >
              <Icon
                name="ios-heart"
                style={[
                  styles.iconLike,
                  { color: this.state.isLike ? "#CB1D50" : "#C3D1E8" },
                ]}
              />
              <Text style={styles.descIcon}>{this.state.totalLike}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              // {...testID('buttonShare')}
              accessibilityLabel="button_share"
              onPress={() =>
                share(this.state.shareLink) + this.sendAdjust(from.Share)
              }
              style={styles.viewPerIcon}
            >
              <Icon name="md-share" style={styles.iconShare} />
              <Text style={styles.descIcon}>SHARE</Text>
            </TouchableOpacity>
          </View>
          {this.renderAreaGetPretestPostest()}
          {this.state.showDescription && (
            // <Text style={styles.textFullDesc}>{this.state.description}</Text>
            // <MyWebView
            //   source={{ html: escapeHtml(this.state.description, '') }}
            //   startInLoadingState={true}
            //   width={platform.deviceWidth - 30}
            //   defaultHeight={100} />
            <AutoHeightWebView
              style={{
                flex: 1,
                width: wp("100%") - 60,
                marginLeft: 15,
              }}
              ref={(ref) => (this.webview = ref)}
              source={{ html: escapeHtml(this.state.description, "") }}
              bounces={false}
              zoomable={false}
              scrollEnabled={false}
              customStyle={customCss}
              onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
            />
          )}
          {this.state.description != "" && (
            <Text style={[styles.textShowDesc, styles.descIcon]}>
              {this.state.showDescription
                ? "Hide Description"
                : "Show Description"}
            </Text>
          )}
          {this.state.description != "" && (
            <TouchableOpacity
              // {...testID('buttonShowDescription')}
              accessibilityLabel="button_show_desc"
              onPress={() => this.onPressShowHideDesc()}
            >
              <Icon
                name={
                  this.state.showDescription ? "ios-arrow-up" : "ios-arrow-down"
                }
                style={styles.iconShare}
              />
            </TouchableOpacity>
          )}
        </Card>
      </View>
    );
  };

  onShouldStartLoadWithRequest(request) {
    // HACK: allow some urls to be embedded, and reject others
    // https://github.com/react-native-community/react-native-webview/issues/381

    if (request.url.includes("http")) {
      Linking.openURL(request.url).catch((err) =>
        console.error(`Could not open URL ${request.url}`, err)
      );
      return false;
    } else {
      return true;
    }
  }

  renderPopupInputCertificate(data) {
    return (
      <Modal
        hasBackdrop={true}
        avoidKeyboard={true}
        backdropOpacity={0.8}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}
        isVisible={this.state.modalVisible}
      >
        <PopupInputCertificate
          type={"webinar"}
          closeGoToCertificate={this.closeGoToCertificate}
          handlerStatusSubmit={this.handlerStatusSubmit}
          onVisibleModal={this.toggleModal}
          data={data}
        />
      </Modal>
    );
  }

  onPressFullscreen360 = () => {
    this.refWebinarYoutube360.doChangeFullScreen(true);
  };

  renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={false}
        panGestureEnabled={false}
        ref={(ref) => {
          this.modalizeBottomSheet = ref;
        }}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        // onClose={async () => this.state.typeContentBottomSheet == 'event_expired' ? await this.setFlagStatusShowBottomSheet(this.state.mEvent) : null}
        HeaderComponent={
          this.state.typeContentBottomSheet == "event_expired" ? (
            <View
              style={{
                height: 4,
                borderRadius: 2,
                width: 40,
                marginTop: 8,
                backgroundColor: "#454F6329",
                alignSelf: "center",
              }}
            />
          ) : null
        }
      >
        {this.state.typeContentBottomSheet == "ios_360" &&
          this._renderBottomSheetIOS360()}
      </Modalize>
    );
  };

  _renderBottomSheetIOS360 = () => {
    return (
      <Col
        style={{
          alignItems: "center",
          marginBottom:
            platform.platform == "ios" &&
            platform.deviceHeight >= 812 &&
            platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <Image
          resizeMode="contain"
          style={{
            height: 80,
            width: 126,
            marginTop: 32,
            marginBottom: 24,
          }}
          source={require("../../assets/images/icon-warning-360-video.png")}
        />
        <Text
          bold
          style={{
            marginBottom: 16,
            textAlign: "center",
            fontSize: 20,
            color: "#1E1E20",
            fontFamily: "Roboto-Medium",
            lineHeight: 32,
            letterSpacing: 0.26,
          }}
        >
          {translate("ios_belum_mendukung_360_video")}
        </Text>
        <Text
          regular
          style={{
            textAlign: "center",
            color: "#1E1E20",
            fontFamily: "Roboto-Regular",
            fontSize: 16,
            lineHeight: 26,
          }}
        >
          {translate("gunakan_android_untuk_menggunakannya")}
        </Text>

        <Row
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            marginTop: 24,
          }}
        >
          <Button
            {...testID("button_ok_kembali")}
            block
            style={{
              flex: 1,
              marginLeft: 8,
              height: 48,
              backgroundColor: "#D01E53",
              borderRadius: 8,
            }}
            onPress={() => {
              this.onBackPressed();
              this.modalizeBottomSheet.close();
            }}
          >
            <Text
              style={{
                color: "#FFFFFF",
                fontSize: 14,
                fontFamily: "Roboto-Medium",
                lineHeight: 26,
              }}
            >
              {translate("oke_kembali")}
            </Text>
          </Button>
        </Row>
      </Col>
    );
  };

  getCurrentTimeForNavigating = () => {
    this.setState({ isTriggerButtonPip: true });
  };

  onPressButtonPositiveCoachmarkFloating = async () => {
    await AsyncStorage.setItem(
      STORAGE_TABLE_NAME.COACHMARK_WEBINAR_FLOATING,
      "Y"
    );
    this.setState({
      isAutoShowCoachmarkFloating: false,
    });
    this.coachmarkChatSupport.hide();
  };

  checkCoachmarkFloating = async () => {
    let dataCoachmarkChatSupport = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.COACHMARK_WEBINAR_FLOATING
    );
    console.log(" ----", dataCoachmarkChatSupport);
    if (dataCoachmarkChatSupport == null) {
      this.setState({
        isAutoShowCoachmarkFloating: true,
      });
    }
  };

  render() {
    let { params } = this.props.navigation.state;
    let title = "";

    if (this.state.isLive) {
      title = "Webinar Live";
    } else {
      if (this.state.typeWebinar == "upcoming") {
        title = "Webinar Upcoming";
      } else {
        title = "Webinar Recorded";
      }
    }

    return (
      <Container>
        {this.renderPopupInputCertificate(this.state.dataWebinarDetail)}
        {this.renderBottomSheet()}
        {!this.state.isFullscreenMode && (
          <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
            <Header noShadow>
              <Left style={{ flex: 0.5 }}>
                <Button
                  // {...testID('buttonBack')}
                  accessibilityLabel="button_back"
                  transparent
                  onPress={() => this.onBackPressed()}
                >
                  <Icon name="md-arrow-back" style={styles.toolbarIcon} />
                </Button>
              </Left>
              <Body
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Title
                // {...testID('title')}
                // accessibilityLabel="title"
                >
                  {title}
                </Title>
              </Body>
              <Right style={{ flex: 0.5 }} />
            </Header>
          </SafeAreaView>
        )}
        <Loader visible={this.state.showLoader} />
        <View
          style={styles.container}
          onLayout={({
            nativeEvent: {
              layout: { width },
            },
          }) => {
            if (!this.state.containerMounted)
              this.setState({ containerMounted: true });
            if (this.state.containerWidth !== width)
              this.setState({ containerWidth: width });
          }}
        >
          {this.state.dataWebinarDetail &&
            this.state.isVideo360 &&
            this.state.html360 != "" && (
              <View>
                <WebinarYoutube360
                  data={this.state.dataWebinarDetail}
                  html={this.state.html360}
                  ref={(ref) => {
                    this.refWebinarYoutube360 = ref;
                  }}
                  from={this.state.from}
                  isReload={this.state.reloadWebviewVideo360}
                  handlerFullscreenMode={this.handlerFullscreenMode}
                  isLive={this.state.isLive}
                  isPlay={this.state.isPlay}
                  isUpcoming={
                    this.state.isLive
                      ? false
                      : this.state.typeWebinar == "upcoming"
                      ? true
                      : false
                  }
                  isTriggerButtonPip={this.state.isTriggerButtonPip}
                  setTriggerButtonPip={(isTriggered) =>
                    this.setState({ isTriggerButtonPip: isTriggered })
                  }
                  seekTo={this.state.seekTo}
                />

                <View style={{ flexDirection: "row", alignSelf: "flex-end" }}>
                  {platform.platform == "android" && (
                    <Coachmark
                      ref={(ref) => {
                        this.coachmarkChatSupport = ref;
                      }}
                      autoShow={this.state.isAutoShowCoachmarkFloating}
                      title={translate("coachmark_title_webinar_in_background")}
                      message={translate(
                        "coachmark_desc_webinar_in_background"
                      )}
                      onPressButtonPositive={() =>
                        this.onPressButtonPositiveCoachmarkFloating()
                      }
                      buttonTextPositive={translate("close")}
                      dotLength={0}
                      dotActivePosition={1}
                    >
                      <TouchableOpacity
                        accessibilityLabel="button_pip"
                        onPress={() => this.getCurrentTimeForNavigating()}
                      >
                        <Icon
                          type="MaterialIcons"
                          name="picture-in-picture"
                          style={styles.iconPip}
                        />
                      </TouchableOpacity>
                    </Coachmark>
                  )}
                  <TouchableOpacity
                    // {...testID('buttonFullScreenAndroid')}
                    accessibilityLabel="button_fullscreen"
                    onPress={() =>
                      this.onPressFullscreen360() +
                      this.sendAdjust(from.Fullscreen)
                    }
                  >
                    <Icon
                      type="MaterialIcons"
                      name="fullscreen"
                      style={styles.iconFullscreen}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            )}

          {this.state.dataWebinarDetail && !this.state.isVideo360 && (
            <WebinarYoutube
              ref={(ref) => {
                this.refWebinarYoutube = ref;
              }}
              from={this.state.from}
              data={this.state.dataWebinarDetail}
              handlerFullscreenMode={this.handlerFullscreenMode}
              isLive={this.state.isLive}
              isPlay={this.state.isPlay}
              isUpcoming={
                this.state.isLive
                  ? false
                  : this.state.typeWebinar == "upcoming"
                  ? true
                  : false
              }
              startAt={
                this.props.navigation.state.params.startAt
                  ? this.props.navigation.state.params.startAt
                  : 0
              }
              seekTo={this.state.seekTo}
              isAutoShowCoachmarkFloating={
                this.state.isAutoShowCoachmarkFloating
              }
              onPressButtonPositiveCoachmarkFloating={() =>
                this.onPressButtonPositiveCoachmarkFloating()
              }
            />
          )}

          {this.state.isFullscreenMode ? null : this.renderWebinarComment()}
          {this.state.isFullscreenMode ? null : this.renderMessageForQuote()}
        </View>

        {this.state.isFullscreenMode ? null : platform.platform == "android" ? (
          this.renderBoxMessage()
        ) : (
          <KeyboardAvoidingView behavior="padding" enabled>
            {this.renderBoxMessage()}
          </KeyboardAvoidingView>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6FA",
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  buttonGroup: {
    flexDirection: "row",
    alignSelf: "center",
  },
  button: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    alignSelf: "center",
  },
  buttonText: {
    fontSize: 18,
    color: "blue",
  },
  buttonTextSmall: {
    fontSize: 15,
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5,
  },
  player: {
    alignSelf: "stretch",
    marginBottom: 5,
  },
  viewBottomChat: {
    flex: 1,
    flexDirection: "column",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
  },
  viewPlayer: {
    flex: 1,
    position: "absolute",
    top: 0,
    width: platform.deviceWidth,
    justifyContent: "center",
    alignItems: "center",
  },
  viewOpacityPlayer: {
    opacity: 0.5,
    backgroundColor: "#79887C",
  },
  iconPlay: {
    color: "#FFFFFF",
    fontSize: 55,
    justifyContent: "center",
    alignItems: "center",
  },
  iconPip: {
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    padding: 12,
  },
  iconFullscreen: {
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    padding: 12,
    marginRight: 8,
  },
  iconFullscreenAndroid: {
    fontSize: 35,
    // justifyContent: 'center',
    // alignItems: 'center',
    marginLeft: 5,
  },
  textTimeWhite: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "bold",
  },
  textTime: {
    fontSize: 16,
    fontWeight: "bold",
  },
  viewBottomPlayer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    marginHorizontal: 10,
    marginVertical: 5,
    width: platform.width,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  viewPlayerAndroid: {
    marginHorizontal: 10,
    width: platform.width,
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  viewThumbnailVideo: {
    // width: platform.deviceWidth - 21,
    flex: 1,
    // borderRadius:3,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  viewTitleDesc: {
    flexDirection: "row",
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent: "center",
  },
  viewMain3Icon: {
    // flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 10,
  },
  viewPerIcon: {
    paddingVertical: 5,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  iconLike: {
    fontSize: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    textAlign: "center",
    // flex: 1
  },
  iconShare: {
    color: "#C3D1E8",
    fontSize: 30,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    textAlign: "center",
    //flex: 1
  },
  arrowLiveChat: {
    color: "white",
    fontSize: 20,
  },
  descIcon: {
    color: "#78849E",
    textAlign: "center",
    marginTop: 5,
  },
  textShowDesc: {
    textAlign: "center",
    // flex: 1,
    marginTop: 10,
  },
  viewCenterPerIcon: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderLeftColor: "#C3D1E8",
    borderRightColor: "#C3D1E8",
  },
  viewTitleDesc2: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
  roundedImage: {
    width: 60,
    height: 60,
    borderWidth: 2,
    borderColor: "#FFFFFF",
    borderRadius: 25,
    marginRight: 15,
  },
  textTitle: {
    fontSize: 20,
    color: "#454545",
    fontWeight: "bold",
  },
  textDesc: {
    fontSize: 14,
    color: "#78849E",
  },
  textFullDesc: {
    color: "#78849E",
    margin: 10,
    fontSize: 16,
  },
  btnLiveChat: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    // backgroundColor: '#AAAAAA',
    backgroundColor: "#1F8DCD",
    //paddingHorizontal: 10,
  },

  containterCardDesc: {
    marginHorizontal: 10,
    width: platform.width,
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    // padding: 10, marginBottom: 20
  },

  messageBoxContainer: {
    minHeight: 50,
    maxHeight: 80,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "white",
  },

  messageForQuoteContainer: {
    minHeight: 50,
    maxHeight: 80,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "lightgray",
  },

  emptyItem: {
    marginTop: 20,
    backgroundColor: "transparent",
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    justifyContent: "center",
    alignItems: "center",
  },
});
