import React, { Component, Fragment } from 'react'
import { BackHandler, TouchableOpacity, StyleSheet, Image, Modal } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Content, Card, Label, Item, Toast, CardItem, Col, Row, View, Switch } from 'native-base';
import { testID, STORAGE_TABLE_NAME } from '../../libs/Common'
import platform from '../../../theme/variables/d2dColor';
import { Loader } from './../../components'
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import { translate } from '../../libs/localize/LocalizeHelper'
import { SafeAreaView } from 'react-native';


export default class WebinarFilter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isSkp: 0,
            spesialization: [],
        }
    }

    componentDidMount() {
        this.initialSetting()
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        this.props.navigation.goBack();
    }

    initialSetting = async () => {
        let filterWebinar = await AsyncStorage.getItem(STORAGE_TABLE_NAME.FILTER_WEBINAR)
        filterWebinar = JSON.parse(filterWebinar)
        console.log('initialSetting: ', filterWebinar)
        if (filterWebinar != null && filterWebinar != '') {
            this.setState({
                isSkp: filterWebinar.isSkp,
                spesialization: filterWebinar.specialistList
            })
        }

    }

    toggleSwitchSkp = (value) => {
        //onValueChange of the switch this function will be called
        this.setState({ isSkp: value })
        //state changes according to switch
        //which will result in re-render the text
    }

    renderSpesialisasi = () => {
        let items = [];
        if (this.state.spesialization != null && this.state.spesialization.length > 0) {
            let self = this;
            console.log('renderTagSpecialist this.state.spesialization: ', this.state.spesialization)
            this.state.spesialization.map(function (d, i) {
                items.push(
                    <TouchableOpacity key={i} style={styles.mainTouchSp}>
                        <View style={styles.viewTopSp}>
                            <TouchableOpacity
                                // {...testID('buttonX')}
                                accessibilityLabel="buttonX"
                                onPress={() => self.doDeletedSpesialization(self, i)}>
                                <Icon name='ios-close' style={styles.tagIconX} />
                            </TouchableOpacity>
                            <Text style={{ flex: 1, textAlign: 'left' }}>{d.description}</Text>
                        </View>
                    </TouchableOpacity>
                )
            });

            items = <Card style={[styles.card, { paddingVertical: 20 }]}>
                {items}
            </Card>
        }


        return items
    }

    deleteSubSpecialist = (self, indexSp, index) => {
        self.state.spesialization[indexSp].subspecialist.splice(index, 1);
        self.setState({ spesialization: self.state.spesialization, needSendSpesialzation: true })
    }

    doDeletedSpesialization = (self, index) => {
        self.state.spesialization.splice(index, 1);
        self.setState({ spesialization: self.state.spesialization, needSendSpesialzation: true })
    }

    doUpdateDataSpesialization = (spesializationList) => {
        console.log('data specialist', spesializationList)
        this.setState({ spesialization: spesializationList, needSendSpesialzation: true });
    };

    gotoSelectSpecialist = () => {
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'SelectSpecialist',
            key: `gotoSelectSpecialist`,
            params: {
                specialistList: this.state.spesialization,
                saveDataSp: this.doUpdateDataSpesialization,
                from: 'FILTER'
            }

        }));
    }
    renderButtonSpesialisasi = () => {

        return <Item style={{
            backgroundColor: '#FFFFFF',
            marginTop: 12,
            paddingHorizontal: 24
        }} onPress={() => this.gotoSelectSpecialist()} >

            <Row style={{
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingVertical: 16,
            }}>
                <Text style={{
                    color: '#78849E',
                    fontSize: 15,
                    fontFamily: 'Nunito-SemiBold'
                }}>{translate("text_label_specialist")}</Text>
                <Icon name='arrow-drop-down' type='MaterialIcons' style={{ color: '#78849E', margin: 0, paddingHorizontal: 0 }} />

            </Row>
        </Item>

    }

    applyFilter = async () => {
        let filterWebinar = {
            isSkp: this.state.isSkp,
            specialistList: this.state.spesialization
        }
        filterWebinar = JSON.stringify(filterWebinar)
        await AsyncStorage.setItem(STORAGE_TABLE_NAME.FILTER_WEBINAR, filterWebinar)
        console.log('filterWebinar: ', filterWebinar)
    }

    resetFilter = async () => {
        this.setState({
            isSkp: false,
            spesialization: []
        })
        await AsyncStorage.removeItem(STORAGE_TABLE_NAME.FILTER_WEBINAR)
    }

    render() {

        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }} />

                <Container style={{ backgroundColor: '#F3F3F3' }}>
                    <Header noShadow >
                        <Left style={{ flex: 0.25 }}>
                            <Button
                                {...testID('button_back')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Filter</Title>
                        </Body>
                        <Right style={{ flex: 0.25 }}>
                        </Right>
                    </Header>


                    <Content style={{
                        marginBottom: 48
                    }}>
                        <Col style={{ marginHorizontal: 20, marginTop: 40, }}>
                            <Row style={{ flex: 0, alignItems: 'center', justifyContent: 'space-between' }}>
                                <Text style={{ fontFamily: 'Nunito-SemiBold', color: '#11243D', fontSize: 16 }}>{translate('skp_included')}</Text>

                                <Row style={{
                                    flex: 0,
                                    fontFamily: 'Nunito-Regular',
                                    fontSize: 14,
                                    color: '#11243D'
                                }}>
                                    <Text style={{
                                        fontFamily: 'Nunito-SemiBold',
                                        color: '#11243D',
                                        fontSize: 16,
                                        marginRight: 5
                                    }}>{this.state.isSkp ? 'On' : 'Off'}</Text>

                                    <Switch
                                        trackColor={{ true: '#3AE194', false: 'grey' }}
                                        onValueChange={this.toggleSwitchSkp}
                                        value={this.state.isSkp}
                                        style={{ transform: [{ scaleX: .8 }, { scaleY: .7 }] }}

                                    ></Switch>
                                </Row>

                            </Row>
                            <Text style={{ marginTop: 30, fontFamily: 'Nunito-SemiBold', color: '#11243D', fontSize: 16 }}>{translate("text_label_specialist")}</Text>

                            {this.renderButtonSpesialisasi()}

                            {this.renderSpesialisasi()}

                        </Col>
                    </Content>


                    <Row style={{
                        position: 'absolute',
                        bottom: 0
                    }}>
                        <Button
                            // {...testID('buttonSave')}i
                            accessibilityLabel="button_save"
                            style={{
                                borderRadius: 0,
                                width: '50%',
                                backgroundColor: '#7DB199'
                            }} onPress={() =>
                                this.resetFilter()
                                //+
                                //this.props.navigation.navigate('FinishPostTest')
                            }
                            block>
                            <Text style={{ fontFamily: 'Nunito-Bold', textAlign: 'center' }}>RESET</Text>
                        </Button>
                        <Button
                            // {...testID('buttonSave')}i
                            accessibilityLabel="button_save"
                            style={{
                                borderRadius: 0,
                                width: '50%'

                            }} onPress={() =>
                                this.applyFilter()
                                + this.props.navigation.goBack()
                            }
                            success block>
                            <Text style={{ fontFamily: 'Nunito-Bold' }}>{translate("text_button_terapkan")}</Text>
                        </Button>
                    </Row>


                </Container>
            </Fragment>

        )
    }
}

const styles = StyleSheet.create({
    containerCard: {
        position: 'absolute',
        flex: 1,
        top: 230,
        left: 0,
        right: 0,
        paddingHorizontal: 16,
        justifyContent: 'space-between',
        zIndex: 1
    },
    card: {
        flex: 1,
        justifyContent: 'space-between',
        marginVertical: 20,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    textHeaderInCard: {
        fontSize: 15,
        fontFamily: 'Nunito-Black',
        fontWeight: 'bold',
        color: '#454F63',
        alignSelf: 'center',
        textAlign: 'center'
    },
    textInCard: {
        fontSize: 11,
        fontFamily: 'Nunito-Bold',
        color: '#78849E',
        alignSelf: 'center'

    },
    btnSave: {
        position: 'absolute',
        flex: 1,
        left: 24,
        right: 24,
        bottom: 35,
        height: 55,
        zIndex: 0
    },
    initialStyle: {
        alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 80 / 2,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "white",
        backgroundColor: '#B5B5B5',
        justifyContent: 'center'
    },
    textStyle: {
        fontFamily: 'Roboto-Regular',
        color: '#424242',
        backgroundColor: 'transparent',
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    mainTouchSp: {
        flex: 1,
        marginTop: 10,
        borderWidth: 2,
        borderColor: '#F0F0F0',
        borderRadius: 5,
    },
    viewTopSp: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderWidth: 2,
        borderColor: '#F0F0F0',
        backgroundColor: '#F0F0F0',
        flexDirection: 'row-reverse',
        borderRadius: 5,
    },
    viewBottomSp: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: 'white',
        flexDirection: 'row-reverse',
    },
    tagIconX: {
        fontSize: 18,
        marginLeft: 10,
    },
})
