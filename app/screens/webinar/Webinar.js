import React, { Component, Fragment } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  Animated,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Body,
  Right,
  Tabs,
  Tab,
  ScrollableTab,
  TabHeading,
  Toast,
} from "native-base";
import { Loader, CardItemWebinar } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import {
  getListWebinar,
  ValueTypeWebinar,
  getDataWebinar,
  getProfileUser,
} from "./../../libs/NetworkUtility";
import { testID, STORAGE_TABLE_NAME } from "./../../libs/Common";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";

import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import WebinarFilter from "./WebinarFilter";
import moment from "moment-timezone";
import { ArrowBackButton } from "../../../src/components/atoms";
import { sendTopicWebinarAttendee } from "../../../src/utils";

export default class Webinar extends Component {
  page = 0;

  state = {
    animatedValue: new Animated.Value(0),
    listWebinar: [],
    isFetching: false,
    isSuccess: false,
    isFailed: false,
    isEmptyData: false,
    isSkp: false,
    spesialization: [],
    server_date: "",
    backButtonAvailable: false,
    country_code: "",
  };
  constructor(props) {
    super(props);
    this.getDataCountryCode();
  }

  componentDidMount() {
    StatusBar.setHidden(false);

    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        AdjustTracker(AdjustTrackerConfig.Webinar_Latest);
        this.initData();
      }
    );
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      this.setState({
        country_code: profile.country_code,
      });
      if (profile.country_code == "ID") {
        this.setState({ backButtonAvailable: true });
      } else {
        this.setState({ backButtonAvailable: false });
      }
    });
  };

  onBackPressed = () => {
    this.props.navigation.goBack();
  };

  getProfileDetail = async () => {
    try {
      let response = await getProfileUser();
      if (response.isSuccess) {
        this.doSetProfile(response);
      }
    } catch (error) {
      console.log("getProfileDetail error: ", error);
      Toast.show({ text: "Something went wrong!", position: "top" });
    }
  };

  doSetProfile(response) {
    if (response.data != null) {
      let profile = response.data;
      console.log("doSetProfile: ", profile);
      if (profile != null) {
        let shownBornDate = null;

        if (profile.born_date != null && profile.born_date != "") {
          shownBornDate = moment(profile.born_date, "YYYY-MM-DD").format(
            "DD MMM YYYY"
          );

          profile.born_date = shownBornDate;
        }

        this.doSaveProfileUser(profile);
      }
    }
  }

  async doSaveProfileUser(profile) {
    try {
      let jsonProfile = JSON.stringify(profile);
      let saveProfile = await AsyncStorage.setItem(
        STORAGE_TABLE_NAME.PROFILE,
        jsonProfile
      );

      console.log("saveProfile ", JSON.parse(jsonProfile).country_code);
    } catch (error) {
      console.log("failed update db local profile");
    }
  }

  setMenuRef = (ref) => {
    this.menu = ref;
  };

  initData = async () => {
    this.setState({
      isSkp: false,
      spesialization: [],
    });

    await this.setupFilter();
    this.getData();
  };

  setupFilter = async () => {
    let filterWebinar = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.FILTER_WEBINAR
    );
    filterWebinar = JSON.parse(filterWebinar);
    console.log("setupFilter: ", filterWebinar);
    if (filterWebinar != null && filterWebinar != "") {
      this.setState({
        isSkp: filterWebinar.isSkp,
        spesialization: filterWebinar.specialistList,
      });
    }
  };

  async getData(tab) {
    if (typeof tab != "undefined") {
      switch (tab.i) {
        case 0:
          AdjustTracker(AdjustTrackerConfig.Webinar_Latest);
          break;
        case 1:
          AdjustTracker(AdjustTrackerConfig.Webinar_Recorded);
          break;
      }
    }
    this.onRefresh();
  }

  onRefresh = async () => {
    this.page = 1;
    this.setState({ listWebinar: [], isFetching: true, isEmptyData: false });
    await this.getProfileDetail();
    this.getDataListWebinar(this.page);
  };

  doShownData = async (dataProfile, webinarList) => {
    let result = [];
    console.log("doShownData webinarList: ", webinarList);

    console.log("data profile", dataProfile);

    if (webinarList != null && webinarList.length > 0) {
      webinarList.forEach((element) => {
        if (element.exclusive == 1) {
          if (
            dataProfile != null &&
            dataProfile.webinar != null &&
            dataProfile.webinar.length > 0
          ) {
            let isShow = false;
            dataProfile.webinar.forEach((slug_hash) => {
              sendTopicWebinarAttendee(slug_hash);
              if (slug_hash == element.slug_hash) {
                isShow = true;
              }
            });

            if (isShow) {
              result.push(element);
            }
          }
        } else {
          result.push(element);
        }
      });
      console.log("webinar list", webinarList);
    }

    if (result.length == 0 && typeof webinarList != "undefined") {
      this.page = this.page + 1;
      this.setState({ isFetching: true });
      this.getDataListWebinar(this.page);
    }

    return result;
  };

  async getDataListWebinar(page) {
    this.setState({ isFailed: false, isFetching: true });
    try {
      let getJsonProfile = await AsyncStorage.getItem(
        STORAGE_TABLE_NAME.PROFILE
      );
      let dataProfile = JSON.parse(getJsonProfile);

      let idSpecialist = [];
      let listIdSpecialist = "";
      if (
        this.state.spesialization != null &&
        this.state.spesialization.length > 0
      ) {
        this.state.spesialization.map(function(value, index) {
          idSpecialist.push(value.id);
        });
        listIdSpecialist = idSpecialist.join(",");
      }

      let response = await getListWebinar(
        page,
        this.state.isSkp,
        listIdSpecialist
      );
      console.log("response: ", response);
      if (response.isSuccess == true) {
        let shownData = await this.doShownData(dataProfile, response.docs);
        this.setState({
          listWebinar:
            page == 1
              ? [...shownData]
              : [...this.state.listWebinar, ...shownData],
        });
        if (response.server_date != "") {
          this.setState({
            server_date: response.server_date,
          });
        }
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isEmptyData:
            response.docs != null && response.docs.length > 0 ? false : true,
        });
      } else {
        this.setState({ isFetching: false, isSuccess: false, isFailed: true });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({ isFetching: false, isSuccess: false, isFailed: true });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMore = () => {
    if (this.state.isEmptyData == true) {
      return;
    }

    //temp comment
    this.page = this.state.isFailed == true ? this.page : this.page + 1;
    this.getDataListWebinar(this.page);
  };

  _renderItem = ({ item }) => {
    item.server_date = this.state.server_date;
    return (
      <CardItemWebinar
        navigation={this.props.navigation}
        data={item}
        isLive={true}
        country_code={this.state.country_code}
      />
    );
  };

  _renderItemFooter = (type) => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader(type)}
    </View>
  );

  _renderItemFooterLoader(type) {
    if (this.state.isFailed == true && this.page > 1) {
      return (
        <TouchableOpacity
          onPress={() => {
            this.handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = (type) => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader(type)}</View>
  );

  _renderEmptyItemLoader(type) {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.getData()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (
      this.state.isFetching == false &&
      this.state.isSuccess == true &&
      this.state.isEmptyData == true
    ) {
      return <Text style={{ textAlign: "center" }}>Data not found</Text>;
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  render() {
    const nav = this.props.navigation;
    let translateY = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
      extrapolate: "clamp",
    });
    let opacityContent = this.state.animatedValue.interpolate({
      inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 2],
      outputRange: [1, 0],
    });

    return (
      <Fragment>
        <SafeAreaView
          style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }}
        />

        <Container>
          <AnimatedFlatList
            contentContainerStyle={{
              paddingTop: heightHeaderSpan + 5,
              paddingHorizontal: 10,
            }}
            scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: { y: this.state.animatedValue },
                  },
                },
              ],
              { useNativeDriver: true } // <-- Add this
            )}
            data={this.state.listWebinar}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isFetching}
            renderItem={this._renderItem}
            ListEmptyComponent={this._renderEmptyItemLoader()}
            ListFooterComponent={this._renderItemFooter()}
            keyExtractor={(item, index) => index.toString()}
          />

          <Animated.View
            style={[
              styles.animHeaderSpan,
              {
                transform: [{ translateY }],
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-start",
              },
            ]}
          >
            <Animated.Text style={{ color: "#fff", opacity: opacityContent }}>
              Share & get updates on new clinic cases and health journal info
            </Animated.Text>
          </Animated.View>

          <Animated.View style={styles.animHeader}>
            <Header noShadow hasTabs>
              <Body style={{ flexDirection: "row", alignItems: "center" }}>
                {this.state.backButtonAvailable == true && (
                  <View style={{ marginLeft: -6 }}>
                    <ArrowBackButton onPress={() => this.onBackPressed()} />
                  </View>
                )}
                <Title style={styles.textTitle(this.state.backButtonAvailable)}>
                  Webinar
                </Title>
              </Body>
              <Right>
                <Button
                  // {...testID('buttonWebinarSearch')}
                  accessibilityLabel="button_webinar_filter"
                  transparent
                  onPress={() => {
                    // AdjustTracker(AdjustTrackerConfig.Webinar_Video_Search);
                    this.props.navigation.navigate("WebinarFilter");
                    AdjustTracker(AdjustTrackerConfig.Webinar_Video_Filter);
                  }}
                >
                  <Icon
                    type="FontAwesome"
                    name="sliders"
                    style={{ fontSize: 20 }}
                  />
                </Button>

                <Button
                  // {...testID('buttonWebinarSearch')}
                  accessibilityLabel="button_webinar_search"
                  transparent
                  onPress={() => {
                    AdjustTracker(AdjustTrackerConfig.Webinar_Video_Search);
                    this.props.navigation.navigate("WebinarSearch");
                  }}
                >
                  <Icon
                    type="EvilIcons"
                    name="search"
                    style={{ fontSize: 28 }}
                  />
                </Button>
              </Right>
            </Header>
          </Animated.View>
        </Container>
      </Fragment>
    );
  }
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const heightHeaderSpan = 150;
const styles = StyleSheet.create({
  animHeader: {
    position: "absolute",
    width: "100%",
    zIndex: 2,
  },
  animHeaderSpan: {
    position: "absolute",
    paddingTop: platform.toolbarHeight,
    backgroundColor: platform.toolbarDefaultBg,
    height: heightHeaderSpan,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: 10,
    paddingVertical: 30,
    justifyContent: "flex-end",
    alignItems: "flex-end",
  },
  emptyItem: {
    justifyContent: "center",
    alignItems: "center",
    height: platform.deviceHeight / 2 - 80,
  },
  tabHeading: {
    flex: 1,
    height: 100,
    backgroundColor: platform.tabBgColor,
    justifyContent: "center",
    alignItems: "center",
  },
  textTitle: (backButtonAvailable) => ({
    fontSize: 30,
    marginLeft: backButtonAvailable == true ? 20 : null,
  }),
});
