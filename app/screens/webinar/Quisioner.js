import React, { Component } from 'react'
import { BackHandler, Dimensions, KeyboardAvoidingView, DeviceEventEmitter, ScrollView, View } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right } from 'native-base';
import { WebView } from 'react-native-webview';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import { STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import Config from 'react-native-config'
import Orientation from 'react-native-orientation';
import { userActivity } from '../../libs/NetworkUtility'
import PopupInputCertificate from '../../components/PopupInputCertificate';
import Modal from 'react-native-modal'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import platform from '../../../theme/variables/d2dColor';
import _ from 'lodash';
import { SafeAreaView } from 'react-native';

export default class Quisioner extends Component {

    numberToPreventExecuteMultipleTimes = 0

    constructor(props) {
        super(props);

        this.state = {
            urlQuisioner: this.props.navigation.state.params.quisioner_link,
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            webinarId: this.props.navigation.state.params.id,
            template_certificate: this.props.navigation.state.params.template_certificate,
            modalVisible: false,
            data: this.props.navigation.state.params,
            isSubmitted: false,
            dataCertificate: null
        }

        this.handlerStatusSubmit = this.handlerStatusSubmit.bind(this);
        this.closeGoToCertificate = this.closeGoToCertificate.bind(this);
    }

    handlerStatusSubmit(dataCertificate) {
        this.setState({
            isSubmitted: true,
            dataCertificate: dataCertificate
        })
    }

    closeGoToCertificate = async () => {
        //check certificate is availabe or not
        let isCertificateAvailable = ''
        let env = Config.ENV == 'production' ? '' : 'dev/';
        let uid = await AsyncStorage.getItem('UID');

        let data = null

        if (_.isEmpty(this.state.dataCertificate)) {
            //console.log()
            console.log('certificate belum ada ', this.state.dataCertificate)
            isCertificateAvailable = false
            data = {
                title: '',
                typeCertificate: 'webinar',
                certificate: '',
                isCertificateAvailable: isCertificateAvailable,
                from: 'TestResult'
            }
        }

        else {
            isCertificateAvailable = true
            console.log('certificate sudah ada', this.state.dataCertificate)
            data = {
                title: this.state.dataCertificate.event_title,
                typeCertificate: 'webinar',
                certificate: this.state.dataCertificate.filename,
                isCertificateAvailable: isCertificateAvailable,
                from: 'TestResult'
            }
        }

        this.props.navigation.replace('CmeCertificate', { ...data })

    }

    toggleModal = (visible) => {
        this.setState({
            modalVisible: visible,
        });
    }

    // onLayout(e) {
    //     console.log('onLayout: ', e)
    //     this.setState({
    //         width: Dimensions.get('window').width,
    //         height: Dimensions.get('window').height,
    //     });
    // }

    componentDidMount() {
        console.log(this.props)
        console.log(this.props.navigation.state.params.id)
        console.log(this.state.data)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Orientation.lockToPortrait();
        this.props.navigation.goBack();
    }

    _onMessage = event => {
        console.log('_onMessage', JSON.parse(event.nativeEvent.data));
        const res = JSON.parse(event.nativeEvent.data);
        this.numberToPreventExecuteMultipleTimes = this.numberToPreventExecuteMultipleTimes + 1
        if (this.numberToPreventExecuteMultipleTimes == 1) {
            if (res.message === 'quisionerDone') {
                //alert('button clicked ok');
                this.setStatusQuisioner()

            }
        }

    };

    async setStatusQuisioner() {
        console.log(this.state.webinarId)
        let dataParams = {
            activity: 'quisioner',
            content: 'webinar',
            content_id: this.state.webinarId
        }

        let response = await userActivity(dataParams);
        console.log(response)
        if (response.isSuccess) {
            //Toast.show({ text: action + ' succesfull', position: 'top', duration: 3000 })
            if (this.state.data.is_pass == true || this.state.data.posttest_passed == 1) {

                if (this.state.template_certificate == 1) {
                    //show popup
                    this.setState({
                        modalVisible: true,
                    });
                }
                else {
                    this.gotoCertificateIsNotAvailable()
                }
            }
        } else {
            Toast.show({ text: response.message, position: 'top', duration: 3000 })
        }
    }

    gotoCertificateIsNotAvailable = async () => {
        let isCertificateAvailable = ''
        let data = null

        if (this.state.template_certificate == 0) {
            //console.log()
            console.log('certificate belum ada ', this.state.dataCertificate)
            isCertificateAvailable = false
            data = {
                title: '',
                typeCertificate: 'webinar',
                certificate: '',
                isCertificateAvailable: isCertificateAvailable,
                from: 'TestResult'
            }
            this.props.navigation.replace('CmeCertificate', { ...data })
        }
    }

    renderPopupInputCertificate(data) {

        return (
            <Modal
                hasBackdrop={true}
                avoidKeyboard={true}
                backdropOpacity={0.8}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={600}
                animationOutTiming={600}
                backdropTransitionInTiming={600}
                backdropTransitionOutTiming={600}
                isVisible={this.state.modalVisible}
            >
                <PopupInputCertificate type={'webinar'} closeGoToCertificate={this.closeGoToCertificate} handlerStatusSubmit={this.handlerStatusSubmit} onVisibleModal={this.toggleModal} data={data} />
            </Modal >
        )
    }

    render() {
        const jsCode = `
        document.body.addEventListener('DOMSubtreeModified', function () {

            let isDoneSubmit = document.querySelector('.lf-confirmation-screen') !== null;
            if (isDoneSubmit) {
                window.ReactNativeWebView.postMessage(JSON.stringify({type: "click", message : "quisionerDone"}));
            } 
        });
        `;

        return (
            <Container style={{ backgroundColor: '#F3F3F3' }}>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow hasTabs>
                        <Left style={{ flex: 0.5 }}>
                            <Button
                                {...testID('button_back')}
                                accessibilityLabel="button_back"
                                transparent onPress={() => this.onBackPressed()}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Kuesioner</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }}>

                        </Right>
                    </Header>
                </SafeAreaView>

                <View style={{ flex: 1, flexDirection: 'column' }}>

                    <WebView
                        userAgent={"Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3714.0 Mobile Safari/537.36"}
                        onMessage={this._onMessage}
                        injectedJavaScript={jsCode}
                        startInLoadingState={true}
                        javaScriptEnabled={true}
                        //source={{ uri: 'https://docs.google.com/forms/d/e/1FAIpQLSf75jbG6jduHokT0gtK0p6X11FVnxNjHwSGb6paocvhwea72Q/viewform' }}
                        source={{ uri: this.state.urlQuisioner }}
                        style={{ flex: 1, flexDirection: 'column', width: this.state.width, backgroundColor: '#F3F3F3' }} />

                </View>
                {this.renderPopupInputCertificate(this.state.data)}

            </Container>
        )
    }

}