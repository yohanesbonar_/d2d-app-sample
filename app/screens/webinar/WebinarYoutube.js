import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    PixelRatio,
    PanResponder,
    Dimensions,
} from 'react-native';
import { Icon, Toast, Text, Row } from 'native-base';
import platform from '../../../theme/variables/d2dColor';

import _ from 'lodash';
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import Slider from '@react-native-community/slider';
import { ParamAction, ParamContent, doActionUserActivity } from '../../libs/NetworkUtility';
import Orientation from 'react-native-orientation';
import YoutubePlayer from 'react-native-youtube-iframe';

let from = {
    FastForward: 'FastForward',
    Fullscreen: 'Fullscreen',
    Like: 'Like',
    Share: 'Share',
    Question: 'Question',
    PAUSE: 'PAUSE',
    ONWARD: 'ONWARD',
    BACKWARD: 'BACKWARD',
    FULLSCREEN_PAUSE: 'FULLSCREEN_PAUSE'
}
import FullScreen from '../../libs/FullScreen'
import WebinarModule from '../../libs/WebinarModule'
import Coachmark from '../../libs/coachmark/Coachmark';
import AsyncStorage from '@react-native-community/async-storage';
import { STORAGE_TABLE_NAME } from '../../libs/Common';
import { translate } from '../../libs/localize/LocalizeHelper';
export default class WebinarYoutube extends Component {
    intervalGetCurrentTime = 0
    constructor(props) {
        super(props);

        this.state = {
            comments: null,
            isReady: false,
            quality: null,
            error: null,
            isPlaying: this.props.isPlay,
            isLooping: true,
            duration: 0,
            currentTime: 0,
            fullscreen: false,
            containerMounted: false,
            containerWidth: null,
            active: false,
            isShowChat: false,
            showOpacityPlayer: false,
            webinarId: null,
            idYoutube: '',
            isLive: false,
            urlWebview: '',
            sliderValue: 50,
            from: this.props.from ? this.props.from : '',
            tempCurrentTime: 0,
            isChangeScreen: false,
            inactive: true,
            isUpcoming: false,
            isFromRefresh: false,
        }

    }

    offset = 0;

    componentDidMount() {
        this.doInitParams()
        Orientation.lockToPortrait()
    }
    componentDidUpdate(prevProps){
        if(this.props.seekTo!=null && this.props.seekTo != prevProps.seekTo){
            this._youTubeRef.seekTo(this.props.seekTo);
        }
        if(this.props.isAutoShowCoachmarkFloating!= prevProps.isAutoShowCoachmarkFloating){
            if(!this.props.isAutoShowCoachmarkFloating){
                this.coachmarkChatSupport.hide();
            }
        }
    }

    onRefreshWebinarDetail = () => {
        this.setState({
            isLive: this.props.isLive
        })
    }

    onRefreshDataYoutubeVideo = (idYoutube) => {
        clearInterval(this.intervalGetCurrentTime)
        this.setState({
            isPlaying: false,
            isFromRefresh: true,
            idYoutube: idYoutube,
        })
    }

    onResumePlay = () => {
        console.log('onResumePlay')
        this.setState({ isPlaying: true })
    }

    getCurrentTimeForNavigating = () => {
        let time = Math.floor(this.state.currentTime);
        let dataWebinar = this.props.data;
        let youtubeId = dataWebinar.youtube_id;
        let webinarLink = dataWebinar.url!=null && dataWebinar.url.length && dataWebinar.url[0].app_link!=null? dataWebinar.url[0].app_link: "https://d2d.co.id/";
        WebinarModule.minimizeWebinar(
            youtubeId,
            "",
            time,
            webinarLink,
            (param) => {}
        )
    }

    getCurrentTimeVideo = () => {
        if (this.state.duration == 0) {
            this.setDurationTimer()
        }

        this._youTubeRef.getCurrentTime()
            .then(currentTime =>
                this.setState({
                    currentTime: currentTime
                })
            );
    }

    onPressPlayPause = () => {
        if (this.state.isPlaying == true) {
            if (this.state.fullscreen) {
                this.sendAdjust(from.FULLSCREEN_PAUSE)
            } else {
                this.sendAdjust(from.PAUSE)
            }
        }
        this.setState({ isPlaying: !this.state.isPlaying })
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponderCapture: () => {
                clearTimeout(this.timeout);

                this.setState(state => {
                    if (state.inactive === false) return null;
                    return {
                        inactive: false,
                    };
                });

                this.timeout = setTimeout(() => {
                    this.setState({
                        inactive: true,
                    });
                }, 3000);
                return false;
            },

        });

    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
        clearInterval(this.intervalGetCurrentTime);
    }

    doInitParams = () => {
        let params = this.props.data;
        params.isLive = this.props.isLive
        params.isUpcoming = this.props.isUpcoming

        if (params != null) {
            this.setState({
                webinarId: params.id,
                idYoutube: params.youtube_id,
                // idYoutube: 'rQHwK8_DXsM',
                isLive: params.isLive == true ? true : false,
                isUpcoming: params.isUpcoming == true ? true : false
            });
            this.doViewEvent(params.id);
        }
    }

    async doViewEvent(webinarId) {
        let viewEvent = await doActionUserActivity(ParamAction.VIEW, ParamContent.WEBINAR, webinarId);
    }

    doChangeFullScreen = (isFullScreen) => {
        console.log('doChangeFullScreen: ', isFullScreen)
        this.props.handlerFullscreenMode(isFullScreen)
        this.setState({ fullscreen: isFullScreen, isChangeScreen: true }, function () {
            if (isFullScreen == false) {
                Orientation.lockToPortrait()
                if (platform.platform == 'android') {
                    FullScreen.disable();
                }

            }
            else if (isFullScreen == true) {
                Orientation.lockToLandscape()
                if (platform.platform == 'android') {
                    FullScreen.enable();
                }
            }
        });

    }

    shownTimeVideos = (time) => {
        let result = '0.00';
        let hour = Math.trunc((time / 60 / 60) % 60) > 0 ? Math.trunc((time / 60 / 60) % 60) + ':' : '';
        let minutes = Math.trunc((time / 60) % 60) < 10 && hour > 0 ? '0' + Math.trunc((time / 60) % 60) : Math.trunc((time / 60) % 60);
        let second = Math.trunc(time % 60) < 10 ? '0' + Math.trunc(time % 60) : Math.trunc(time % 60);
        result = hour + minutes + ':' + second;
        return result;
    }

    onChangedProgress = (value) => {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            let currentTime = value / 100 * this.state.duration;
            this._youTubeRef && this._youTubeRef.seekTo(currentTime);
            this.setCurrentTime(currentTime)
        }, 500);
    }

    timer = null;

    setCurrentTime = (value) => {
        this.setState({ currentTime: value, tempCurrentTime: value });
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.FastForward:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Webinar_FastForward
                else if (this.state.from == 'Webinar')
                    token = AdjustTrackerConfig.Webinar_Video_FF
                break;
            case from.Fullscreen:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Webinar_FullScreen
                else if (this.state.from == 'Webinar')
                    token = AdjustTrackerConfig.Webinar_Video_Fullscreen
                break;
            case from.Like:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Webinar_Like
                else if (this.state.from == 'Webinar')
                    token = AdjustTrackerConfig.Webinar_Video_Like
                break;
            case from.Question:
                if (this.state.from == 'Feeds')
                    token = AdjustTrackerConfig.Feeds_Webinar_Question
                else if (this.state.from == 'Webinar')
                    token = AdjustTrackerConfig.Webinar_Video_Question
                break;
            case from.PAUSE:
                token = AdjustTrackerConfig.Webinar_Video_Pause;
                break;
            case from.FULLSCREEN_PAUSE:
                token = AdjustTrackerConfig.Webinar_Video_FullScreen_Pause;
                break;
            case from.ONWARD:
                token = AdjustTrackerConfig.Webinar_Video_FullScreen_Onward;
                break;
            case from.BACKWARD:
                token = AdjustTrackerConfig.Webinar_Video_FullScreen_Backward;
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }



    onReady = () => {
        console.log('on ready');
        this.setDurationTimer()
        this.intervalGetCurrentTime = setInterval(this.getCurrentTimeVideo, 1000)
        if (this.state.isFromRefresh) {
            this.setState({
                isPlaying: true,
                isFromRefresh: false
            })
        }
    }

    setDurationTimer = () => {
        this._youTubeRef.getDuration()
            .then(getDuration =>
                this.setState({
                    duration: getDuration
                })
            );
    }

    forwardNextTenSecond = () => {
        this._youTubeRef.seekTo(this.state.currentTime + 10)
    }

    backwardPreviousTenSecond = () => {
        this._youTubeRef.seekTo(this.state.currentTime - 10)
    }

    render() {
        console.log("props: ", this.props)
        //  let params = this.props.data;
        const WIDTH = Dimensions.get('window').width;
        const HEIGHT = !this.state.fullscreen ? PixelRatio.roundToNearestPixel(WIDTH / (16 / 9)) : Dimensions.get('window').height;

        let heightVideo = HEIGHT
        //this.state.fullscreen ? PixelRatio.roundToNearestPixel(456 / (9 / 16)) : PixelRatio.roundToNearestPixel(this.state.containerWidth / (16 / 9));

        let widthVideo = this.state.containerWidth

        let currentTime = this.shownTimeVideos(this.state.currentTime);

        let maxTime = this.shownTimeVideos(this.state.duration);

        let valueSlider = isNaN(this.state.currentTime / this.state.duration * 100) == true || (this.state.currentTime / this.state.duration * 100) === Infinity ? 0 : (this.state.currentTime / this.state.duration * 100);

        return (
            <View
                onLayout={({ nativeEvent: { layout: { width, height } } }) => {
                    if (!this.state.containerMounted) this.setState({ containerMounted: true });
                    if (this.state.containerWidth !== width) this.setState({ containerWidth: width });
                }}
            >
                {this.state.containerMounted && (

                    <YoutubePlayer
                        ref={component => {
                            this._youTubeRef = component;
                        }}
                        height={heightVideo}
                        width={widthVideo}
                        videoId={this.state.idYoutube}
                        play={this.state.isPlaying}

                        onChangeState={state => {
                            console.log('onChangeState: ', state);
                            if (state === 'ended') {
                                this._youTubeRef.seekTo(0) //force to loop
                            }
                            if (state == 'paused') {
                                this.setState({ isPlaying: false })
                            }
                        }}

                        onReady={() => console.log("onReady") + this.onReady()}
                        onError={e => console.log(e)}
                        onPlaybackQualityChange={q => console.log(q)}
                        playbackRate={1}
                        initialPlayerParams={{
                            showClosedCaptions: false,
                            modestbranding: false,
                            rel: false,
                            loop: true,
                            controls: false,
                            showInfo: false,
                            start:this.props.startAt? this.props.startAt : 0
                        }}

                        //the way to fit video when landscape
                        //based on https://github.com/LonelyCpp/react-native-youtube-iframe/issues/13
                        webViewProps={{
                            startInLoadingState: true,
                            injectedJavaScript: `
                        var element = document.getElementsByClassName('container')[0];
                        element.style.position = 'unset';
                        true;
                      `,
                        }}
                    />
                )}
                <View style={[{
                    height: heightVideo,
                    width: widthVideo,
                    backgroundColor: this.state.fullscreen && (!this.state.inactive || !this.state.isPlaying) ? '#000000' : 'transparent',
                    opacity: this.state.fullscreen && (!this.state.inactive || !this.state.isPlaying) ? 0.8 : 1
                }, styles.viewPlayer]}
                    {...this._panResponder.panHandlers}
                >
                    {this.state.fullscreen && (!this.state.inactive || !this.state.isPlaying) && (

                        <Row style={{ width: '100%', alignItems: 'center', justifyContent: 'space-evenly' }}>

                            {!this.state.isLive && !this.state.isUpcoming && (
                                <TouchableOpacity
                                    // {...testID('buttonFullScreenAndroid')}
                                    accessibilityLabel="button_fullscreen_backward"
                                    onPress={() => this.backwardPreviousTenSecond() + this.sendAdjust(from.BACKWARD)}>
                                    <Icon type='MaterialIcons' name='replay-10' style={styles.iconForwardBackward} />
                                </TouchableOpacity>

                            )}
                            <TouchableOpacity
                                accessibilityLabel="button_play_pause"
                                onPress={() => this.onPressPlayPause()}>
                                <Icon type='MaterialIcons' name={this.state.isPlaying ? 'pause' : 'play-arrow'} style={styles.iconPlayPauseFullScreen} />
                            </TouchableOpacity>

                            {!this.state.isLive && !this.state.isUpcoming && (
                                <TouchableOpacity
                                    // {...testID('buttonFullScreenAndroid')}
                                    accessibilityLabel="button_fullscreen_forward"
                                    onPress={() => this.forwardNextTenSecond() + this.sendAdjust(from.ONWARD)}>
                                    <Icon type='MaterialIcons' name='forward-10' style={styles.iconForwardBackward} />
                                </TouchableOpacity>
                            )}
                        </Row>
                    )}

                    {this.state.fullscreen && (!this.state.inactive || !this.state.isPlaying) && !this.state.isUpcoming && (
                        <View style={styles.viewFullscreenPlayer}>

                            {!this.state.isLive && !this.state.isUpcoming && (<Text style={[styles.textTime, { color: '#FFFFFF' }]}>{currentTime + '/' + maxTime}</Text>)}

                            <Row style={{
                                marginHorizontal: 16,
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}>
                                {this.state.isLive && !this.state.isUpcoming && (
                                    <View style={{ opacity: 0.8, borderRadius: 5, backgroundColor: '#FF0000', paddingHorizontal: 5 }}>
                                        <Text style={{ color: '#fff', fontSize: 14 }}>LIVE</Text>
                                    </View>
                                )}

                                {!this.state.isLive && !this.state.isUpcoming && (

                                    <Slider
                                        step={1}
                                        //value={this.state.currentTime / this.state.duration * 100}
                                        value={valueSlider}
                                        minimumValue={0}
                                        maximumValue={100}
                                        minimumTrackTintColor="#CB1D50"
                                        maximumTrackTintColor="#FFFFFF"
                                        thumbImage={require('./../../assets/images/reddot.png')}
                                        onValueChange={(changeValue) =>
                                            // this.setState({ sliderValue: ChangedValue })
                                            this.onChangedProgress(changeValue)
                                            // console.warn(changeValue)
                                        }
                                        style={{
                                            width: '90%',

                                        }}
                                        onSlidingComplete={() => {
                                            // AdjustTracker(AdjustTrackerConfig.Webinar_Video_FF)
                                            this.sendAdjust(from.FastForward)
                                        }}
                                    />
                                )}

                                <View style={{}}>
                                    <TouchableOpacity
                                        // {...testID('buttonFullScreenAndroid')}
                                        accessibilityLabel="button_fullscreen_android"
                                        onPress={() => this.doChangeFullScreen(false) + this.sendAdjust(from.Fullscreen)}>
                                        <Icon type='MaterialIcons' name='fullscreen-exit' style={styles.iconMinimizeFullscreen} />
                                    </TouchableOpacity>
                                </View>
                            </Row>
                        </View>
                    )}
                </View>

                {!this.state.isLive && !this.state.isUpcoming && !this.state.fullscreen && (
                    <Slider
                        step={1}
                        //value={this.state.currentTime / this.state.duration * 100}
                        value={valueSlider}
                        minimumValue={0}
                        maximumValue={100}
                        minimumTrackTintColor="#CB1D50"
                        thumbImage={require('./../../assets/images/reddot.png')}
                        onValueChange={(changeValue) =>
                            // this.setState({ sliderValue: ChangedValue })
                            this.onChangedProgress(changeValue)
                            // console.warn(changeValue)
                            //this._youTubeRef.seekTo(0)
                        }
                        style={styles.slider}
                        onSlidingComplete={() => {
                            // AdjustTracker(AdjustTrackerConfig.Webinar_Video_FF)
                            this.sendAdjust(from.FastForward)
                        }}
                    />
                )}

                {!this.state.fullscreen && !this.state.isUpcoming && (
                    <View style={styles.viewPlayerAndroid}>
                        <TouchableOpacity
                            accessibilityLabel="button_play_pause"
                            onPress={() => this.onPressPlayPause()}>
                            <Icon type='MaterialIcons' name={this.state.isPlaying ? 'pause' : 'play-arrow'} style={styles.iconPlay} />
                        </TouchableOpacity>

                        {!this.state.isLive && !this.state.isUpcoming && (<Text style={styles.textTime}>{currentTime + '/' + maxTime}</Text>)}
                        {this.state.isLive &&
                            (<View style={{ opacity: 0.8, borderRadius: 5, backgroundColor: '#FF0000', paddingHorizontal: 5 }}>
                                <Text style={{ color: '#fff', fontSize: 14 }}>LIVE</Text>
                            </View>)}
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            { platform.platform == 'android' && 
                                <Coachmark
                                    ref={(ref) => {
                                    this.coachmarkChatSupport = ref;
                                    }}
                                    autoShow={this.props.isAutoShowCoachmarkFloating}
                                    title={translate("coachmark_title_webinar_in_background")}
									message={translate("coachmark_desc_webinar_in_background")}
                                    onPressButtonPositive={() =>
                                    this.props.onPressButtonPositiveCoachmarkFloating()
                                    }
                                    buttonTextPositive={translate("close")}
                                    dotLength={0}
                                    dotActivePosition={1}
                                >
                                    <TouchableOpacity
                                        accessibilityLabel="button_pip"
                                        onPress={() => 
                                            this.getCurrentTimeForNavigating()
                                        //     {console.log('webinarmodule ', this.props.data.youtube_id );
                                        // console.log('webinarmodule ', this.getCurrentTime());}
                                            
                                        }>
                                        <Icon type='MaterialIcons' name='picture-in-picture' style={styles.iconPip} />
                                    </TouchableOpacity>
                            </Coachmark>
                            }
                            <TouchableOpacity
                                // {...testID('buttonFullScreenAndroid')}
                                accessibilityLabel="button_fullscreen"
                                onPress={() => this.doChangeFullScreen(true) + this.sendAdjust(from.Fullscreen)}>
                                <Icon type='MaterialIcons' name='fullscreen' style={styles.iconFullscreen} />
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F6FA'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    buttonGroup: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    button: {
        paddingVertical: 4,
        paddingHorizontal: 8,
        alignSelf: 'center',
    },
    buttonText: {
        fontSize: 18,
        color: 'blue',
    },
    buttonTextSmall: {
        fontSize: 15,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    player: {
        alignSelf: 'stretch',
        marginBottom: 5,
    },
    viewBottomChat: {
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },
    viewPlayer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOpacityPlayer: {
        opacity: 0.5,
        backgroundColor: '#79887C',
    },
    iconPlayPauseFullScreen: {
        alignItems: 'center',
        fontSize: 75,
        color: '#FFFFFF',
        opacity: 1
    },
    iconPlay: {
        alignItems: 'center',
        fontSize: 24,
        padding: 12,
    },
    iconPip: {
        fontSize: 24,
        alignItems: 'center',
        padding: 12,
    },
    iconFullscreen: {
        fontSize: 24,
        alignItems: 'center',
        padding: 12,
    },

    iconForwardBackward: {
        fontSize: 35,
        color: '#FFFFFF',
    },
    iconMinimizeFullscreen: {
        fontSize: 35,
        color: '#FFFFFF',
        marginLeft: 5,
    },
    textTimeWhite: {
        fontSize: 16,
        color: '#fff',
        fontWeight: 'bold',
    },
    textTime: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    viewBottomPlayer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        marginHorizontal: 10,
        marginVertical: 5,
        width: platform.width,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewPlayerAndroid: {
        marginHorizontal: 10,
        width: platform.width,
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    viewThumbnailVideo: {
        // width: platform.deviceWidth - 21, 
        flex: 1,
        // borderRadius:3,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    viewTitleDesc: {
        flexDirection: 'row',
        marginVertical: 10,
        marginHorizontal: 20,
        justifyContent: 'center',
    },
    viewMain3Icon: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
    },
    viewPerIcon: {
        paddingVertical: 5,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconLike: {
        fontSize: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        textAlign: 'center',
        // flex: 1
    },
    iconShare: {
        color: '#C3D1E8',
        fontSize: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        textAlign: 'center',
        //flex: 1
    },
    arrowLiveChat: {
        color: 'white',
        fontSize: 20,
    },
    descIcon: {
        color: '#78849E',
        textAlign: 'center',
        marginTop: 5,
    },
    textShowDesc: {
        textAlign: 'center',
        // flex: 1,
        marginTop: 10
    },
    viewCenterPerIcon: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderLeftColor: '#C3D1E8',
        borderRightColor: '#C3D1E8'
    },
    viewTitleDesc2: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    roundedImage: {
        width: 60,
        height: 60,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 25,
        marginRight: 15,
    },
    textTitle: {
        fontSize: 20,
        color: '#454545',
        fontWeight: 'bold',
    },
    textDesc: {
        fontSize: 14,
        color: '#78849E'
    },
    textFullDesc: {
        color: '#78849E',
        margin: 10,
        fontSize: 16,
    },
    btnLiveChat: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        // backgroundColor: '#AAAAAA',
        backgroundColor: '#1F8DCD',
        //paddingHorizontal: 10,
    },
    slider: {
        height: 10,
        width: platform.platform == 'ios' ? '100%' : '104%',
        left: platform.platform == 'ios' ? 0 : '-2%',
        // height: 30,
    },
    containterCardDesc: {
        marginHorizontal: 10,
        width: platform.width,
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // padding: 10, marginBottom: 20
    },

    messageBoxContainer: {
        minHeight: 50,
        maxHeight: 80,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
    },

    messageForQuoteContainer: {
        minHeight: 50,
        maxHeight: 80,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'lightgray'
    },
    viewFullscreenPlayer: {
        flex: 1,
        position: 'absolute',
        bottom: platform.platform == 'ios' ? 0 : 20,
        left: 0,
        marginBottom: 8,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 10,
        justifyContent: 'space-between',
    },

});
