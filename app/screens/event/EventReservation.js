import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Modal, Image, TouchableOpacity, ImageBackground, TouchableHighlight } from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import {
    Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right,
    Grid, Row, Col, Form, Item, Input, Label, Toast, Thumbnail, Card, CardItem, Badge
} from 'native-base';
import { Loader } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import { convertMonth, convertShownDate, openOtherApp, testID } from '../../libs/Common';
import { doReservationEvent } from './../../libs/NetworkUtility';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';

export default class EventReservation extends Component {

    isGotoMain = true;

    constructor(props) {
        super(props);

        this.state = {
            showLoader: false,
            modalVisible: false,
            showLoader: false
        }
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        lor(this);
        const nav = this.props.navigation;
        const { params } = nav.state;
        this.isGotoMain = params.manualUpdateReserve == null ? true : !params.manualUpdateReserve;
    }

    componentWillUnMount() {
        rol();
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    setMenuRef = (ref) => {
        this.menu = ref;
    };

    render() {
        const nav = this.props.navigation;
        const { params } = nav.state;

        return (
            <Container>
                {this._initModal()}
                <Modal animationType="slide" transparent={true} visible={this.state.showLoader} onRequestClose={() => console.log('loader closed')}>
                    <Loader visible={this.state.showLoader} />
                </Modal>
                <View style={{ width: wp('200%'), height: wp('200%'), borderRadius: wp('100%'), backgroundColor: platform.toolbarDefaultBg, position: 'absolute', left: -wp('50%'), top: -wp('100%') }}></View>
                <Header noShadow>
                    <Left>
                        <Button
                            // {...testID('buttonBack')}
                            accessibilityLabel="button_back"
                            transparent onPress={() => nav.goBack()}>
                            <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                        </Button>
                    </Left>
                    <Body />
                    <Right />
                </Header>
                <Content>
                    {/* <ImageBackground source={require('./../../assets/images/bg-blank.png')} style={{width: platform.deviceWidth, height: platform.deviceHeight/1.5}}>
                    </ImageBackground> */}
                    <View style={{ padding: 10 }}>
                        <Card style={{ flex: 0 }}>
                            {/* {this._renderMoreParticipant()} */}
                            <CardItem style={{ justifyContent: 'center', flex: 1 }}>
                                <Text style={styles.textMediumGreyCenter}>Contact the following Contact Person for a formal reservation</Text>
                            </CardItem>
                            {this._renderContactPerson(params.data)}

                            {this._renderButtonReservation(params.data.flag_reserve == 1 ? true : false, params.data.id, params.data.title)}
                        </Card>

                    </View>
                </Content>
            </Container>
        )
    }

    _initModal() {
        return (<Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                console.log('alert closed')
            }}>

            <TouchableOpacity
                onPress={this.onModalDismiss}
                style={{
                    backgroundColor: '#00000080',
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Card style={{ flex: 0, marginTop: 22, width: platform.deviceWidth - 70, height: 300 }}>
                    <CardItem style={{ justifyContent: 'center', flex: 1 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', padding: 10 }}>
                            <View style={styles.modalBadge}>
                                <Icon style={styles.modalImageCheck} name='md-checkmark' />
                            </View>
                            <Text style={styles.modalText}>Thank you.</Text>
                            <Text style={styles.modalText}>For further event registration, please contact the contact person listed</Text>
                        </View>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        </Modal>
        );
    }

    _doShowModal() {
        this.setModalVisible(true)
        this.setState({ showLoader: false });

        setTimeout(() => {
            this.onModalDismiss();
        }, 7000);
    }

    onModalDismiss = () => {
        this.setModalVisible(false);
        if (this.isGotoMain == true) {
            this.goToMainEvent();
        } else {
            this.props.navigation.goBack();
            this.props.navigation.state.params.isReserved();
        }
    }


    goToMainEvent() {
        this.props.navigation.popToTop();
    }


    _renderButtonReservation = (reserved, eventId, title) => {
        if (reserved == false) {
            return (
                <CardItem nopadding>
                    <Button
                        // {...testID('buttonDoYouWantToAttend')}
                        accessibilityLabel="button_do_you_want_to_attend"
                        block success onPress={() => { this.doReservation(eventId, title) }} style={{ flex: 1, borderRadius: 0, height: 55 }}>
                        <Text nopadding style={styles.textButtonWhite}>DO YOU WANT TO ATTEND?</Text>
                    </Button>
                </CardItem>
            );
        } else {
            return (
                <View>
                    <CardItem bordered />
                    <CardItem nopadding>
                        <Button
                            // {...testID('buttonAttend')}
                            accessibilityLabel="button_attend"
                            block disabled transparent onPress={() => null} style={{ flex: 1, borderRadius: 0, height: 55 }}>
                            <Text nopadding style={styles.textButtonRed}>Yes, I'm coming</Text>
                        </Button>
                    </CardItem>
                </View>
            );
        }
    }

    doReservation = async (eventId, eventName) => {
        this.setState({ showLoader: true })
        let response = await doReservationEvent(eventId, eventName);

        setTimeout(() => {
            if (response.isSuccess == true) {
                this._doShowModal();
            } else {
                this.setState({ showLoader: false })
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        }, 1000)
    }


    async doBookmarkContent(actionBookmark, contentType, contentId) {
        let response = await doActionUserActivity(actionBookmark, contentType, contentId);
        if (response.isSuccess == true) {
            this.setState({ bookmark: !this.state.bookmark })
        } else {
            Toast.show({ text: response.message, position: 'top', duration: 3000 })
        }
    }

    _renderMoreParticipant() {
        return (
            <CardItem style={{ marginBottom: 10 }}>
                <Left>
                    <Body style={{ justifyContent: 'center' }}>
                        <Text style={styles.textMediumBlack}>Participants of other doctors</Text>
                        <Text style={styles.textMediumGrey}>Immediately reserve your place</Text>
                    </Body>
                    {this._renderImageParticipant()}
                </Left>
            </CardItem>

        );
    }

    _renderImageParticipant() {
        let items = [];
        for (i = 2; i >= 0; i--) {
            items.push(
                <View key={i} style={{ position: 'absolute', top: 0, right: i * 30 }}>
                    <Thumbnail style={styles.roundedImage} source={{ uri: 'https://s3-ap-southeast-1.amazonaws.com/static.guesehat.com/directories_thumb/93df4a25ec23629e3377296e62132386.jpg' }} />
                </View>
            )
        }

        return items
    }

    _renderContactPerson = (params) => {
        return (
            <CardItem style={styles.cardGrey} noPadding>
                <Row>
                    <Col>
                        <Text style={styles.textEventTitle}>
                            {params.title}
                        </Text>

                        {this._renderEmail(params.cp_email)}
                        {this._renderPhone(params.cp_number)}
                        {this._renderCPName(params.cp_name)}
                    </Col>
                </Row>
            </CardItem>
        );
    }

    _renderEmail = (email) => {
        if (email) {
            return (
                <TouchableOpacity onPress={() => openOtherApp(`mailto:${email}`, {})} activeOpacity={0.7}>
                    <CardItem style={styles.backgroundCardCPDetail}>
                        <Left>
                            <Icon style={styles.mediumIconSize} name='md-mail-open' />
                            <Body>
                                <Text style={styles.textSmallGrey}>Email</Text>
                                <Text style={styles.textSmallRed}>{email}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </TouchableOpacity>
            );
        }

        return null;
    }

    _renderPhone = (phone) => {
        if (phone) {
            return (
                <TouchableOpacity onPress={() => openOtherApp(`tel:${phone}`, {})} activeOpacity={0.7}>
                    <CardItem style={styles.backgroundCardCPDetail}>
                        <Left>
                            <Icon style={styles.mediumIconSize} type='MaterialIcons' name='local-phone' />
                            <Body>
                                <Text style={styles.textSmallGrey}>Phone</Text>
                                <Text style={styles.textSmallRed}>{phone}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </TouchableOpacity>
            );
        }

        return null;
    }

    _renderCPName = (cpName) => {
        if (cpName) {
            return (
                <CardItem style={styles.backgroundCardCPDetail}>
                    <Left>
                        <Icon style={styles.mediumIconSize} type='MaterialIcons' name='person' />
                        <Body>
                            <Text style={styles.textSmallGrey}>Contact Person</Text>
                            <Text style={styles.textSmallRed}>{cpName}</Text>
                        </Body>
                    </Left>
                </CardItem>
            );
        }

        return null;
    }

}

const styles = StyleSheet.create({
    textMediumBlack: {
        fontSize: 14,
        color: '#000000',
        marginVertical: 5
    },
    textMediumGrey: {
        fontSize: 14,
        color: '#788697'
    },
    textMediumGreyCenter: {
        fontSize: 14,
        color: '#788697',
        marginHorizontal: 20,
        textAlign: 'center',
    },
    textEventTitle: {
        fontSize: 20,
        color: '#37474F',
        fontFamily: 'Nunito-SemiBold',
    },
    cardGrey: {
        backgroundColor: '#F3F3F3',
        padding: 5,
        marginHorizontal: 20,
        marginBottom: 15,
        flex: 1
    },
    textSmallGrey: {
        fontSize: 10,
        color: '#6C6C6C'
    },
    textSmallRed: {
        fontSize: 16,
        fontFamily: 'Nunito-SemiBold',
        color: '#CB1D50'
    },
    mediumIconSize: {
        color: '#BBBBBB',
        fontSize: 23,
    },
    backgroundCardCPDetail: {
        backgroundColor: '#F3F3F3',
        marginTop: 5,
        marginHorizontal: 0,
        padding: 0
    },
    imageContainer: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    roundedImage: {
        width: 50,
        height: 50,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 25
    },
    textButtonRed: {
        fontSize: 17,
        fontFamily: 'Nunito-SemiBold',
        color: '#CB1D50'
    },
    textButtonWhite: {
        fontSize: 17,
        fontFamily: 'Nunito-SemiBold',
        color: '#FFFFFF'
    },
    modalBadge: {
        width: 90,
        height: 90,
        marginBottom: 20,
        backgroundColor: '#3AE194',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50
    },
    modalImageCheck: {
        color: '#FFFFFF',
        fontSize: 35,
        textAlign: 'center'
    },
    modalText: {
        fontSize: 20,
        color: '#37474F',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center'
    }
});
