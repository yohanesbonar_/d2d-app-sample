import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Platform } from 'react-native';

import platform from '../../../theme/variables/d2dColor';
import {
    Content, Button, Text, Col, Item, Input, Label, Icon, Toast
} from 'native-base';
import { testID, sendTopicCertificate } from '../../libs/Common';
import AsyncStorage from '@react-native-community/async-storage';
import Api, { errorMessage } from '../../libs/Api';
import { Loader } from './../../components';
import { translate } from '../../libs/localize/LocalizeHelper'
export default class InputCertificate extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isInputName: false,
            namaGelar: '',
            typeModal: 'inputName',
            showLoader: false,
            isLoading: false,
            isSuccesSubmit: false,
        }

    }

    handleVisibleModalChange = (isShow) => {
        this.props.onVisibleModal(isShow);
    }

    closeThanksDialog() {
        this.props.onVisibleModal(false);
        this.props.closeGoToCertificate();
    }

    onChangeTextName = (value) => {

        if (value.trim().length > 0) {
            this.setState({
                isInputName: true,
                namaGelar: value
            })
        }
        else {
            this.setState({
                isInputName: false,
                namaGelar: value
            })
        }
    }

    onPressSubmit = (typeOnPress) => {
        if (typeOnPress === 'submitInput') {
            //this.isContainEmoji(this.state.namaGelar)
            if (this.state.namaGelar.trim().length > 0) {
                this.setState({
                    typeModal: 'confirmation'
                })
            }
        } else if (typeOnPress === 'submitConfirmation') {
            if (this.state.namaGelar.trim().length > 0) {
                this.setState({
                    isLoading: true,
                    typeModal: 'messageThanks'
                })
                this.doSubmitCertificate();
            }
        }

    }

    async doSubmitCertificate() {
        console.warn(this.state.namaGelar)
        this.setState({ showLoader: true })

        let getUid = await AsyncStorage.getItem('UID');
        console.warn(getUid)
        let params = {
            uid: getUid,
            event_id: this.props.dataEvent.id,
            fullname: this.state.namaGelar
        }

        console.warn(params)
        let response = await Api.post('event/ecertificate/', params);
        console.warn(response)
        console.warn('errorMessage response', errorMessage(response))

        if (errorMessage(response) == '') {
            this.setState({
                typeModal: 'messageThanks',
                namaGelar: '',
                isInputName: false,
            });

            this.props.handlerStatusSubmit(response.result.data)
            sendTopicCertificate('event', this.props.dataEvent.slug_hash)
            this.setState({ showLoader: false, isLoading: false })

        } else {
            this.handleVisibleModalChange(false)
            Toast.show({ text: errorMessage(response), position: 'top', duration: 3000 })
            this.setState({ showLoader: false, isLoading: false })
        }

    }

    render() {
        return (
            <Content
                contentContainerStyle={{ justifyContent: 'center', flex: 1 }}
                centerContent={true}
            >
                <Loader transparent={true} visible={this.state.showLoader} />

                <View style={{ padding: 10, position: 'absolute', left: 0, right: 0 }}>
                    {this.state.isLoading ? (
                        <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>
                        </View>
                    ) :
                        this.renderPopup()
                    }

                    {this.state.isLoading === false && this.state.typeModal != 'confirmation' ? (

                        <TouchableOpacity style={styles.popupClose} onPress={() => this.onPressClosePopup()}>
                            <Text style={{ color: '#FFFFFF', marginTop: -2 }}>x</Text>
                        </TouchableOpacity>
                    ) : null}

                </View>

            </Content>
        )
    }

    onPressClosePopup() {
        console.warn('onPressClosePopup')
        if (this.state.typeModal === 'inputName' || this.state.typeModal === 'confirmation') {
            this.handleVisibleModalChange(false);
        }
        else {
            this.closeThanksDialog();
        }
    }

    renderPopup() {
        if (this.state.typeModal === 'inputName') {
            return this.renderInputCertificate();
        }
        else if (this.state.typeModal === 'confirmation') {
            return this.renderConfirmation();
        }
        else if (this.state.typeModal === 'messageThanks') {
            return this.renderMessageThanks();
        }
    }

    isContainEmoji(name) {
        let regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
        console.warn(regex.test(name))
        return regex.test(name);
    }

    renderInputCertificate() {
        if (this.state.isLoading === false) {
            return (
                <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>

                    <Col>
                        <Text style={styles.textMessageInputCertificate}>{translate("lengkapi_nama_certificate")}</Text>

                        <Label style={styles.textLabel}>{translate("nama_dan_gelar")}</Label>

                        <Item >
                            <Input autoCapitalize="none"
                                placeholder={'dr. Susilo Joko Soekarno S.pr'}
                                editable={true}
                                {...testID('input_nama_gelar')}
                                onChangeText={(txt) => this.onChangeTextName(txt)}
                                value={this.state.namaGelar}
                                keyboardType={Platform.OS === 'android' ? 'visible-password' : 'ascii-capable'} //disable emoji
                            />
                        </Item>

                        <Button
                            {...testID('button_submit')}
                            accessibilityLabel="button_submit"
                            style={[styles.btnSubmit, { backgroundColor: this.state.isInputName == true ? '#3AE194' : '#C4C4C4' }]}
                            onPress={() => this.onPressSubmit('submitInput')} block>
                            <Text style={{ color: 'white', fontFamily: 'Nunito-Bold' }}>SUBMIT</Text>
                        </Button>

                    </Col>
                </View>
            )
        }
        return null;
    }

    renderConfirmation() {
        if (this.state.isLoading === false) {
            return (
                <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>

                    <Col>
                        <Text style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            marginTop: 15,
                            marginHorizontal: 25
                        }}>Are you sure to use this name to your certificated?</Text>

                        <Text style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            marginTop: 15,
                            marginHorizontal: 25
                        }}>{this.state.namaGelar}</Text>
                        <Text style={{
                            fontSize: 14,
                            marginTop: 15,
                            color: '#6C6C6C',
                            fontFamily: 'Nunito-Regular'
                        }}>* The name you entered can not be changed once you submited</Text>

                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>

                            <Button
                                {...testID('button_no')}
                                accessibilityLabel="button_no"
                                style={[styles.btnSubmit, { backgroundColor: 'white', borderColor: '#3AE194', borderWidth: 2, flex: 1, marginRight: 10 }]} onPress={() => this.setState({ typeModal: 'inputName' })} block>
                                <Text style={{ textDecorationLine: 'underline', color: '#3AE194', fontFamily: 'Nunito-Bold' }}>NO</Text>
                            </Button>

                            <Button
                                {...testID('button_yes')}
                                accessibilityLabel="button_yes"
                                style={[styles.btnSubmit, { backgroundColor: '#3AE194', flex: 1, marginLeft: 10 }]} onPress={() => this.onPressSubmit('submitConfirmation')} block>
                                <Text style={{ color: 'white', fontFamily: 'Nunito-Bold' }}>YES</Text>
                            </Button>

                        </View>
                    </Col>
                </View>
            )
        }
        return null;
    }

    renderMessageThanks() {
        if (this.state.isLoading === false) {
            return (
                <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>

                    <Col>
                        <View style={{
                            marginTop: 40,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <View style={styles.circleIcon}>
                                <Icon
                                    name={'ios-checkmark'}
                                    style={{
                                        fontSize: 100,
                                        color: '#FFFFFF'
                                    }} />
                            </View>
                        </View>

                        <Text style={styles.textMessageTerimakasih}>{translate("message_after_complete_name_certificate")}</Text>
                    </Col>
                </View>
            )
        }
        return null;
    }
}

const styles = StyleSheet.create({
    ///style modal
    popupClose: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#78849E',
        width: 24,
        height: 24,
        borderRadius: 12,
        position: 'absolute',
        right: Platform.OS == 'ios' ? 3 : 3,
        top: Platform.OS == 'ios' ? 3 : 3,
        zIndex: 2,
    },

    textMessageInputCertificate: {
        color: '#454F63',
        fontSize: 16,
        fontFamily: 'Nunito-Bold',
        textAlign: 'center',
        marginTop: 15,
        marginHorizontal: 25
    },

    textMessageTerimakasih: {
        color: '#454F63',
        fontSize: 18,
        fontFamily: 'Nunito-Bold',
        textAlign: 'center',
        marginTop: 10,
        marginHorizontal: 10,
    },

    textLabel: {
        marginTop: 25,
        marginBottom: 5
    },
    btnSubmit: {
        marginTop: 15,
        marginBottom: 10,
        borderRadius: 10,
        height: 55
    },

    circleIcon: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 1,
        backgroundColor: platform.brandSuccess,
        borderColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        shadowColor: "#00000029",
        shadowOpacity: 0.8,
        shadowRadius: 1,
        shadowOffset: {
            height: 1,
            width: 0
        },

    }
})
