import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Image, TouchableOpacity, FlatList, ImageBackground, BackHandler, SafeAreaView } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Toast, Item, Input } from 'native-base';
import { Loader, CardItemEvent, HistorySearch } from './../../components';
import platform from '../../../theme/variables/d2dColor';
import Api, { errorMessage } from './../../libs/Api';
import { STORAGE_TABLE_NAME, EnumFeedsCategory, testID, removeHistorySearch, EnumTypeHistorySearch } from './../../libs/Common';
import AsyncStorage from '@react-native-community/async-storage';

export default class EventSearch extends Component {

    page = 1;
    timer = null;
    state = {
        events: [],
        searchValue: '',
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        isEmptyData: false,
        isFocusSearch: false,
        historySearchList: []
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        // this.getEvents(this.page);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

        this.didFocusListener = this.props.navigation.addListener(
            'didFocus',
            async () => {
                console.log('didFocusListener')
                this.loadHistorySearch()
            }
        )
        this.loadHistorySearch()
    }

    onBackPressed() {
        this.props.navigation.goBack();
    }

    componentWillUnmount() {
        this.backHandler.remove();
        this.didFocusListener.remove()
    }

    loadHistorySearch = async () => {
        let dataHistorySearch = await AsyncStorage.getItem(STORAGE_TABLE_NAME.HISTORY_SEARCH_EVENT)
        dataHistorySearch = JSON.parse(dataHistorySearch) != null ? JSON.parse(dataHistorySearch).historySearch : []
        this.setState({
            historySearchList: dataHistorySearch
        })
    }

    async getEvents(page) {
        if (this.state.searchValue == '') {
            this.setState({ isFetching: false, isSuccess: true })
            return;
        }

        this.setState({ isFailed: false, isFetching: true });

        try {
            let response = await Api.get(`feeds?page=${page}&${EnumFeedsCategory.EVENT}=true&search=${this.state.searchValue}`);
            if (errorMessage(response) == '') {
                this.setState({
                    events: [...this.state.events, ...response.result.data],
                    isFetching: false, isSuccess: true, isFailed: false,
                    isEmptyData: response.result.data != null && response.result.size > 0 ? false : true,
                });
                // this.setState({ isFetching: false, isSuccess: true, isFailed: false });
            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            }
        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }
    }

    handleLoadMore = () => {
        if (this.state.isEmptyData == true) {
            return;
        }
        this.page = this.state.isFailed == true ? this.page : this.page + 1;
        this.getEvents(this.page);
    }

    _renderItem = ({ item }) => {
        return (
            <CardItemEvent valueSearchHistory={this.state.searchValue} navigation={this.props.navigation} data={item} />
        )
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetching == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true) {
            return (
                <TouchableOpacity onPress={() => this.handleLoadMore()}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItem = () => (
        <View style={styles.emptyItem}>
            {this.state.isFocusSearch && this.state.searchValue.trim().length < 3 ?
                null
                :
                this._renderEmptyItemLoader()
            }
        </View>
    )

    _renderEmptyItemLoader() {
        let messageNotFound = "We are really sad we could not find anything";

        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            if (this.state.searchValue != '') {
                messageNotFound += ' for ' + this.state.searchValue;
            }
            return (
                <View style={styles.viewNotFound}>
                    <Icon name="ios-search" style={styles.iconNotFound} />
                    <Text style={styles.textNotFound}>{messageNotFound}</Text>
                </View>

            );
        }
    }

    doSearch = (inputText) => {
        clearTimeout(this.timer);
        this.setState({ events: [], searchValue: inputText, isEmptyData: false });
        if (inputText.trim().length >= 3) {
            this.page = 1;
            this.timer = setTimeout(() => {
                this.getEvents(this.page);
            }, 1000);
        }
    }

    renderSearchNotFound() {
        return (
            <View>
                <Icon name="ios-search" />
                <Text style={{ textAlign: 'center' }}>Data not found</Text>
            </View>

        );
    }

    onPressRemoveItemHistory = async (item) => {
        await removeHistorySearch(EnumTypeHistorySearch.EVENT, item.index)
        this.loadHistorySearch()
    }

    render() {
        const nav = this.props.navigation;

        return (
            <Container>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }} >
                    <Header noShadow searchBar rounded>
                        <Item nopadding>
                            <Icon name="ios-search" />
                            <Input
                                // {...testID('inputSearch')}
                                onFocus={() => this.setState({ isFocusSearch: true })}
                                accessibilityLabel="input_search"
                                placeholder="Search for an event or location name"
                                onChangeText={(text) => this.doSearch(text)}
                                value={this.state.searchValue}
                                style={styles.searchField} />
                            <Icon name="ios-close-circle"
                                onPress={() => this.doSearch('')}
                            />
                        </Item>
                        <TouchableOpacity
                            {...testID('button_close')}
                            accessibilityLabel="button_close"
                            transparent style={{ marginRight: 5, marginLeft: 10, alignSelf: 'center', justifyContent: 'center' }} onPress={() => nav.goBack()}>
                            <Icon name='md-close' type="Ionicons"
                                style={[styles.iconClose]}
                            />
                        </TouchableOpacity>
                        {/* <Button
                        // {...testID('buttonClose')}
                        accessibilityLabel="button_close"
                        transparent style={{ marginRight: 5 }}
                        onPress={() => console.warn('backhandler')}
                    //  onPress={() => nav.goBack()}
                    >
                        <Icon name='ios-close' style={{ fontSize: 28 }} />
                    </Button> */}
                    </Header>
                </SafeAreaView>
                {this.state.isFocusSearch && this.state.searchValue.trim().length < 3 && this.state.historySearchList.length > 0 && (
                    <HistorySearch
                        onPressRemoveItem={(item) => this.onPressRemoveItemHistory(item)}
                        onPressItem={(txt) => this.doSearch(txt)}
                        data={this.state.historySearchList}>

                    </HistorySearch>
                )}

                <FlatList
                    contentContainerStyle={{ paddingTop: 5, paddingHorizontal: 10, paddingBottom: 5, }}
                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                    data={this.state.events}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.5}
                    renderItem={this._renderItem}
                    ListEmptyComponent={this._renderEmptyItem}
                    ListFooterComponent={this._renderItemFooter}
                    keyExtractor={(item, index) => index.toString()} />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    searchField: {
        fontSize: 14,
        lineHeight: 15,
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    },
    iconClose: {
        fontSize: 28,
        color: '#fff',
        textAlignVertical: 'center'
    },
});
