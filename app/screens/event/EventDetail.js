import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
  StatusBar,
  StyleSheet,
  View,
  TouchableOpacity,
  Alert,
  Linking,
  Image,
  ScrollView,
  RefreshControl,
  BackHandler,
  AppState,
} from "react-native";
import { showLocation } from "react-native-map-link";
import {
  Container,
  Content,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Grid,
  Row,
  Col,
  Toast,
  Card,
  CardItem,
} from "native-base";
import { Loader, GoogleStaticMap, Tag, BottomSheet } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import {
  convertMonth,
  convertShownDate,
  openOtherApp,
  downloadFile,
  share,
  escapeHtml,
  customCss,
  testID,
  doSplitUrl,
  STORAGE_TABLE_NAME,
  numberCurrencyFormat,
  isExpired,
  isUpcoming,
  isStarted,
  isUpcomingTime,
  isFinishedTime,
} from "../../libs/Common";

import AutoHeightWebView from "react-native-autoheight-webview";
import * as AddCalendarEvent from "react-native-add-calendar-event";
import {
  ParamAction,
  ParamContent,
  doActionUserActivity,
  getDetailSlug,
  getEventDetail,
  getProfileUser,
  startPayment,
  EnumTypePayment,
  readNotification,
  cancelPayment,
} from "./../../libs/NetworkUtility";
import moment from "moment-timezone";
import { NavigationActions } from "react-navigation";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import { AdjustTrackerConfig, AdjustTracker } from "../../libs/AdjustTracker";
import Modal from "react-native-modal";
import ModalCertificate from "./ModalCertificate";
import Config from "react-native-config";
import Coachmark from "../../libs/coachmark/Coachmark";
let from = {
  LOC: "LOC",
  SHARE: "SHARE",
  BOOKMARK: "BOOKMARK",
  CALENDAR: "CALENDAR",
  MATERI: "MATERI",
  CERTIFICATE: "CERTIFICATE",
  BACK_BUTTON: "BACK_BUTTON",
};
import ZendeskChat from "react-native-zendesk-chat";
import { SafeAreaView } from "react-native";
import { Modalize } from "react-native-modalize";
import momentTimezone from "moment-timezone";
import firebase, { database } from "react-native-firebase";
import {
  sendTopicEventAttendee,
  sendTopicWebinarAttendee,
} from "../../../src/utils";
import {
  IconExpandMoreWhite,
  IconNavigateNextDisabled,
  IconShare,
  IconVirtualExhibition,
  IconVirtualExhibitionDisabled,
} from "../../../src/assets";
import { translate } from "../../libs/localize/LocalizeHelper";
const gueloginAuth = firebase.app("guelogin");
export default class EventDetail extends Component {
  shareLink = null;
  reserved = 0;
  fromDeeplink = false;
  uid = null;

  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      showLoader: false,
      bookmark: 0,
      mEvent: null,
      isShowMateri: false,
      from: this.props.navigation.state.params
        ? this.props.navigation.state.params.from
        : "",
      isSubmitted: false,
      dataCertificate: null,
      isShowCertificate: false,
      isShowRegistrasi: false,
      isShowBayarSekarang: false,
      isShowJoin: false,
      isRegistered: false,
      isWaitingPayment: false,
      isSuccessPayment: false,
      isEventEnded: false,
      priceEvent: 0,
      isShowReplay: false,
      isAlreadyPaid: false,
      isPaid: this.props.navigation.state.params.data.paid,
      isAutoShowCoachmarCS: false,
      isAutoShowCoachmarkPayNow: false,
      isAutoShowCoachmarkReplay: false,
      isAutoShowCoachmarkJoin: false,
      isButtonRegistrationDisable: false,
      isButtonPayNowDisable: false,
      isButtonReplayDisable: false,
      isButtonJoinDisable: false,
      isShowingBottomSheet: false,
      webinarID: 0,
      isExpiredPayment: false,
      typeContentBottomSheet: "",
      appState: AppState.currentState,
      isButtonNavigation: true,
      isTriggerChangeBookmark: false,
      isVirtualExhibitionEnabledType: 0, //0 hide, 1 disable, 2 enable
      isTypeVirtualExhibition: false,
      exhibitionData: null,
    };
    this.handlerStatusSubmit = this.handlerStatusSubmit.bind(this);
    this.closeGoToCertificate = this.closeGoToCertificate.bind(this);
  }

  handlerStatusSubmit(dataCertificate) {
    this.setState({
      isSubmitted: true,
      dataCertificate: dataCertificate,
    });
  }

  closeGoToCertificate() {
    this.navigateToCertificate();
  }

  toggleModal = (visible) => {
    this.setState({
      modalVisible: visible,
    });
  };

  componentDidMount() {
    this.init();
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
    this.didFocusListener = this.props.navigation.addListener(
      "didFocus",
      (obj) => {
        console.log("HomeScreen didFocus start");
        this.handlerPushNotif();
        this.onRefresh();
        AppState.addEventListener("change", this._handleAppStateChange);
      }
    );

    this.didBlurListener = this.props.navigation.addListener(
      "didBlur",
      (obj) => {
        console.log("didBlurListener");
        this.removeNotificationListener();
      }
    );
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/background/) && nextAppState === "active") {
      console.log("App has come to the foreground!");
      this.onRefresh();
    }
    this.setState({ appState: nextAppState });
  };

  handlerPushNotif = () => {
    console.log("handlerPushNotif");
    //case open apps
    this.notificationListener = firebase
      .notifications()
      .onNotification((notification) => {
        console.log("notificationListener eventDetail: ", notification);
        if (notification != null && notification.data != null) {
          if (
            notification.data.type == "event" &&
            typeof notification.data.order_code != "undefined" &&
            this.state.mEvent.order != null
          ) {
            if (
              this.state.mEvent.order.order_id == notification.data.order_code
            ) {
              this.onRefresh();
            } else {
              this.gotoAnotherEventDetailFromPushNotif(notification.data);
            }
          }
        }
      });
  };

  gotoAnotherEventDetailFromPushNotif = async (dataNotif) => {
    console.log("gotoEventDetailFromPushNotif dataNotif: ", dataNotif);
    dataNotif.id = dataNotif.content_id;
    this.props.navigation.push("EventDetail", {
      data: dataNotif,
    });
  };

  removeNotificationListener = () => {
    console.log(
      "removeNotificationListener notificationListener: ",
      this.notificationListener
    );
    this.notificationListener();
  };

  componentWillUnmount() {
    this.backHandler.remove();
    this.didFocusListener.remove();
    this.didBlurListener.remove();
    // this.removeNotificationListener();
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  init = () => {
    ZendeskChat.init("FGCBnVOZwPaLWVzARuActOQmZSq2HRtU");
    StatusBar.setHidden(false);
    //this.getParams();
    lor(this);

    let { params } = this.props.navigation.state;

    if (params != null) {
      console.log("params: ", params);
      if (params.data.isFromListNotifProfile == true) {
        this.readNotification(params.dataNotif);
      }
    }
  };

  readNotification = async (data) => {
    try {
      let response = await readNotification(data);
      console.log("readNotification response: ", response);
    } catch (error) {
      console.log(error);
    }
  };

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.LOC:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Events_Loc;
        else if (this.state.from == "Event")
          token = AdjustTrackerConfig.Event_Open_GPS;
        break;
      case from.SHARE:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Events_Share;
        else if (this.state.from == "Event")
          token = AdjustTrackerConfig.Event_Open_Share;
        else if (this.state.from == "bookmark")
          token = AdjustTrackerConfig.Event_Bookmark_Detail_Share;
        break;
      case from.BOOKMARK:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Events_Bookmarks;
        else if (this.state.from == "Event")
          token = AdjustTrackerConfig.Event_Open_Bookmark;
        else if (this.state.from == "bookmark")
          token = AdjustTrackerConfig.Event_Bookmark_Detail_Bookmark;
        break;
      case from.CALENDAR:
        if (this.state.from == "Feeds")
          token = AdjustTrackerConfig.Feeds_Events_Calendar;
        else if (this.state.from == "Event")
          token = AdjustTrackerConfig.Event_Open_Calendar;
        else if (this.state.from == "bookmark")
          token = AdjustTrackerConfig.Event_Bookmark_Detail_Calendar;
        break;
      case from.MATERI:
        token = AdjustTrackerConfig.Event_Download_Materi;
        break;
      case from.CERTIFICATE:
        token = AdjustTrackerConfig.Event_Download_Certificate;
        break;
      case from.BACK_BUTTON:
        if (this.state.from == "Event") {
          token = AdjustTrackerConfig.Event_Event_Detail_BackButton;
        } else if (this.state.from == "bookmark") {
          token = AdjustTrackerConfig.Event_Bookmark_Detail_BackButton;
        }
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  getParams = () => {
    let { params } = this.props.navigation.state;
    if (params != null) {
      console.warn("params.data", params.data);
      this.reserved = params.data.flag_reserve;
      this.setState({ bookmark: params.data.flag_bookmark });
      this.doViewEvent(params.data.id);
      // this.checkStatusPayment(params);
      // if (params.data.url.length > 0 || params.data ) {
      //     let self = this;
      //     let shareUrl = null;
      //     params.data.url.map(function (d, i) {
      //         if (d.app_name == "D2D") {
      //             self.shareLink = d.app_link;
      //             shareUrl = d.app_link;
      //         }
      //     });

      //     if (shareUrl != null) {
      //         let urlData = doSplitUrl(this.shareLink);
      //         this.doGetEventDetail(urlData);
      //     }
      // }

      this.doGetEventDetail();
      if (params.fromDeeplink != null) {
        this.fromDeeplink = true;
      }

      // this.checkEndDateEvent(params.data.end_date)
      // this.checkShowMateris(params.data.materi, params.data.slug_hash)
    }
  };

  checkAutoLaunchBottomSheet = async (params) => {
    if (
      typeof params.order != "undefined" &&
      params.order.status != "success"
    ) {
      let isEventExpired = isExpired(params.current_time, params.end_date);
      let isOrderExpired = isExpired(params.current_time, params.order.expired);

      if (isEventExpired || isOrderExpired) {
        if (isEventExpired) {
          this.setState({
            isRegistered: true,
            typeContentBottomSheet: "event_expired",
          });
        } else if (isOrderExpired) {
          this.setState({
            isSuccessPayment: false,
            typeContentBottomSheet: "event_reregist",
          });
        }

        let dataFlagBottomSheetEventHasEnded = await AsyncStorage.getItem(
          STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED
        );

        console.log(
          "checkAutoLaunchBottomSheet dataFlagBottomSheetEventHasEnded: ",
          dataFlagBottomSheetEventHasEnded
        );
        if (dataFlagBottomSheetEventHasEnded == null) {
          this.modalizeBottomSheet.open();
          if (isOrderExpired) {
            this.setState({
              isWaitingPayment: true,
              isRegistered: true,
            });
          }
        } else {
          dataFlagBottomSheetEventHasEnded = JSON.parse(
            dataFlagBottomSheetEventHasEnded
          );
          //check dataFlagBottomSheetEventHasEnded isExist in asnyc storage .
          if (!dataFlagBottomSheetEventHasEnded.event_id.includes(params.id)) {
            this.modalizeBottomSheet.open();
            if (isOrderExpired) {
              this.setState({
                isWaitingPayment: true,
                isRegistered: true,
              });
            }
          } else if (isOrderExpired) {
            if (
              params.order.status == null ||
              params.order.status == "failure"
            ) {
              this.setState({
                isWaitingPayment: false,
                isRegistered: false,
              });
            }
          }
        }
      }
    }
  };

  checkStatusPayment = async (params) => {
    if (params.order != null) {
      if (params.order.status != null) {
        if (params.order.status == "failure") {
          this.setState({
            isSuccessPayment: false,
            isWaitingPayment: true,
            isRegistered: true,
          });

          if (this.state.isEventEnded == true) {
            this.checkShowCoachmarkChatSupport(params);
          } else {
            this.checkShowCoachmarkPayNow(params);
          }
        } else if (params.order.status == "success") {
          this.setState({
            isSuccessPayment: true,
            isWaitingPayment: false,
            isRegistered: true,
          });
          if (isExpired(params.current_time, params.end_date)) {
            this.checkShowCoachmarkReplay(params);
          } else {
            this.checkShowCoachmarkJoin(params);
          }
        } else if (params.order.status == "pending") {
          this.setState({
            isSuccessPayment: false,
            isWaitingPayment: true,
            isRegistered: true,
          });
          this.checkShowCoachmarkPayNow(params);
        }
      } else {
        this.setState({
          isRegistered: false,
          isWaitingPayment: false,
          isSuccessPayment: false,
        });
        this.checkShowCoachmarkChatSupport(params);
      }
    } else {
      this.checkShowCoachmarkChatSupport(params);
    }
  };

  checkShowButtonNavigation = (params) => {
    if (
      this.state.priceEvent > 0 &&
      this.state.isEventEnded == true &&
      (typeof this.state.mEvent.order == "undefined" ||
        (typeof this.state.mEvent.order != "undefined" &&
          params.order.status != "success"))
    ) {
      this.setState({ isButtonNavigation: false });
    } else {
      this.setState({ isButtonNavigation: true });
    }
  };

  checkStatusEventTransaction = (params) => {
    console.log("checkStatusEventTransaction");
    let showBottomSheet = false;
    let endDate = params.end_date;
    let isShowBottomSheetExpiredEventHasEnded =
      typeof params.order != "undefined"
        ? isExpired(params.current_time, endDate) &&
          params.order.status != "success" &&
          params.order.status != "failure"
        : false;
    let isShowBottomSheetExpiredReRegist = false;

    if (isShowBottomSheetExpiredEventHasEnded) {
      this.setState({
        typeContentBottomSheet: "event_expired",
      });
      this.modalizeBottomSheet.open();
      showBottomSheet = true;
    } else {
      isShowBottomSheetExpiredReRegist =
        typeof params.order != "undefined"
          ? isExpired(params.current_time, params.order.expired) &&
            params.order.status != "success" &&
            params.order.status != "failure"
          : false;
      if (isShowBottomSheetExpiredReRegist) {
        this.setState({
          typeContentBottomSheet: "event_reregist",
        });
        this.modalizeBottomSheet.open();
        showBottomSheet = true;
      }
    }
    return showBottomSheet;
  };

  checkWebinar = (params) => {
    if (params.webinar != null) {
      this.setState({ webinarID: params.webinar.id });
    }
  };

  checkShowMateris = async (responseDocs) => {
    let materis = responseDocs.materi;
    let slug_hash = responseDocs.slug_hash;
    // if (materis != null && ((materis.pdf != null && materis.pdf.length > 0) ||
    //     (materis.video != null && materis.video.length > 0))) {
    console.log("checkShowMateris slug_hash: ", slug_hash);

    try {
      let getJsonProfile = await AsyncStorage.getItem(
        STORAGE_TABLE_NAME.PROFILE
      );
      let dataProfile = JSON.parse(getJsonProfile);
      console.log("checkShowMateris dataProfile: ", dataProfile);

      if (this.fromDeeplink === true) {
        //only insert if data in asnyc storage not exist.
        if (!dataProfile.event_attendee.includes(slug_hash)) {
          dataProfile.event_attendee.push(slug_hash);
          dataProfile = JSON.stringify(dataProfile);

          await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, dataProfile);
          dataProfile = JSON.parse(dataProfile);
        }
      }

      let isShowMateri = responseDocs.show_materi ? true : false;
      if (materis != null && materis.length > 0) {
        this.setState({ isShowMateri: isShowMateri });
      }
      if (dataProfile != null) {
        let isShowCertificate = false;
        let eventAttendee = dataProfile.event_attendee;
        this.uid = dataProfile.uid;

        if (eventAttendee != null && eventAttendee.length > 0) {
          eventAttendee.forEach((element) => {
            console.log("checkShowMateris slug_hash: ", slug_hash);
            console.log("checkShowMateris element: ", element);
            sendTopicEventAttendee(element);
            if (slug_hash == element) {
              // isShowMateri = true;
              isShowCertificate = true;
            }
          });
        }
        isShowCertificate =
          this.state.mEvent.show_certificate == 1 && isShowCertificate == true;
        //this.state.mEvent.showCertificate == 1 ? isShowCertificate = true : isShowCertificate = false;
        this.setState({
          isShowCertificate: isShowCertificate,
        });
      }
    } catch (error) {
      console.warn("checkShowMateris", error);
    }
  };

  doGetEventDetail = async () => {
    // let isJoined = this.props.navigation.state.params.data.flag_join;
    let id = this.props.navigation.state.params.data.id;
    // console.log("isi response is_joined", response)
    // if (isJoined == 1) {
    //     this.getProfileDetail();
    // }

    try {
      this.setState({ showLoader: true });
      // let response = await getDetailSlug(urlData.category, urlData.slug, urlData.hash);
      let params = {
        id: id,
      };
      let response = await getEventDetail(params);
      console.log("doGetEventDetail response", response);
      if (response.isSuccess && response.docs != null) {
        // temporary hide exhibition
        // if (response.docs.exhibition) {
        //   if (response.docs.exhibition.id) {
        //     this.setState({
        //       isTypeVirtualExhibition: true,
        //       exhibitionData: response.docs.exhibition,
        //     });

        //     let responseExhibition = response.docs.exhibition;
        //     var virtualExhibitionStatus =
        //       response.docs != null && response.docs.flag_join
        //         ? 1
        //         : !response.docs.nominal || response.docs.nominal === 0
        //         ? 1
        //         : 0;
        //     if (virtualExhibitionStatus != 0) {
        //       virtualExhibitionStatus =
        //         !isUpcoming(
        //           response.docs.current_time,
        //           responseExhibition.start_date
        //         ) &&
        //         !isExpired(
        //           response.docs.current_time,
        //           responseExhibition.end_date
        //         ) &&
        //         !isUpcomingTime(
        //           response.docs.current_time,
        //           responseExhibition.start_date
        //         ) &&
        //         !isFinishedTime(
        //           response.docs.current_time,
        //           responseExhibition.end_date
        //         )
        //           ? 2
        //           : 1;
        //     }
        //     this.setState({
        //       isVirtualExhibitionEnabledType: virtualExhibitionStatus,
        //     });
        //   }
        // }
        if (response.docs.flag_join == 1) {
          await this.getProfileDetail();
        } else {
          this.setState({ showLoader: false });
        }

        this.setState({
          mEvent: response.docs,
          priceEvent: response.docs.nominal ? response.docs.nominal : 0,
          bookmark: response.docs.flag_bookmark,
          isExpiredPayment:
            typeof response.docs.order != "undefined"
              ? isExpired(
                  response.docs.current_time,
                  response.docs.order.expired
                )
              : false,
        });
        this.checkEndDateEvent(response.docs.end_date);
        this.checkShowMateris(response.docs);
        this.checkStatusPayment(response.docs);
        this.checkWebinar(response.docs);
        this.checkShowButtonNavigation(response.docs);
        this.shareLink = response.docs.url.app_link;

        //this.checkStatusEventTransaction(response.docs)
        // this.setState({
        //     isShowingBottomSheet: true
        // })
      }
    } catch (error) {
      console.log("doGetEventDetail error", error);
      this.setState({ showLoader: false });
    }
  };

  checkShowCoachmarkChatSupport = async (params) => {
    let dataCoachmarkChatSupport = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER
    );

    if (dataCoachmarkChatSupport == null) {
      this.setState({
        isAutoShowCoachmarCS: true,
      });
    } else {
      this.checkAutoLaunchBottomSheet(params);
    }
    console.log(
      "checkShowCoachmark dataCoachmarkChatSupport ",
      dataCoachmarkChatSupport
    );
  };

  checkShowCoachmarkPayNow = async (params) => {
    let dataCoachmarkPayNow = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.COACHMARK_EVENT_PAY_NOW
    );
    console.log("checkShowCoachmark dataCoachmarkPayNow ", dataCoachmarkPayNow);

    if (dataCoachmarkPayNow == null) {
      // this.coachmarkPayNow.show()
      this.setState({
        isAutoShowCoachmarkPayNow: true,
      });
    } else {
      this.checkAutoLaunchBottomSheet(params);
    }
  };

  checkShowCoachmarkJoin = async (params) => {
    let dataCoachmarkJoin = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.COACHMARK_EVENT_JOIN
    );

    if (dataCoachmarkJoin == null) {
      // this.setState({
      //     isAutoShowCoachmarkJoin: true
      // })
      this.coachmarkJoin.show();
    } else {
      this.checkAutoLaunchBottomSheet(params);
    }
  };

  checkShowCoachmarkReplay = async (params) => {
    let dataCoachmarkReplay = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.COACHMARK_EVENT_REPLAY
    );

    if (dataCoachmarkReplay == null) {
      // this.setState({
      //     isAutoShowCoachmarkReplay: true
      // })
      this.coachmarkReplay.show();
    } else {
      this.checkAutoLaunchBottomSheet(params);
    }
  };

  doSetProfile = async (response) => {
    // console.log("log  dosetprofile response", response);
    // console.log("log getprofile dosetprofile response.data.event_attendee", response.data.event_attendee);
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    response.data.event_attendee.map(async (slug_hash, i) => {
      let valueSlugHash = (element) => element === slug_hash;
      // console.log("log  dosetprofile  dataProfile event attende", dataProfile);
      // console.log("log  dosetprofile  event attende if slug_hash", slug_hash);
      // console.log("log getprofile dosetprofile valueSlugHash", valueSlugHash);
      //only insert if data in asnyc storage not exist.
      if (!dataProfile.event_attendee.some(valueSlugHash)) {
        dataProfile.event_attendee.push(slug_hash);
        dataProfile = JSON.stringify(dataProfile);
        // console.log("log  dosetprofile event attendee after if slug_hash", slug_hash);
        await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, dataProfile);
      }
    });

    getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    dataProfile = JSON.parse(getJsonProfile);

    response.data.webinar.map(async (slug_hash, i) => {
      sendTopicWebinarAttendee(slug_hash);

      let valueSlugHash = (element) => element === slug_hash;
      // console.log("log  dosetprofile webinar dataProfile Webinar", dataProfile);
      // console.log("log  dosetprofile webinar before if slug_hash", slug_hash);
      //only insert if data in asnyc storage not exist.
      if (!dataProfile.webinar.some(valueSlugHash)) {
        dataProfile.webinar.push(slug_hash);
        dataProfile = JSON.stringify(dataProfile);
        // console.log("log  dosetprofile webinar after if slug_hash", slug_hash);
        await AsyncStorage.setItem(STORAGE_TABLE_NAME.PROFILE, dataProfile);
      }
    });
  };

  async getProfileDetail() {
    try {
      let response = await getProfileUser();
      this.setState({ showLoader: false });
      await this.doSetProfile(response);
    } catch (error) {
      //this.setState({ isFetching: false, isSuccess: false, isFailed: true, showLoader: false });
      Toast.show({ text: "Something went wrong!", position: "top" });
      this.setState({ showLoader: false });
    }
  }

  checkEndDateEvent = (endDate) => {
    if (endDate != null) {
      if (isExpired(this.state.mEvent.current_time, endDate)) {
        this.setState({
          isEventEnded: true,
        });
        Toast.show({
          text: "Event was expired",
          position: "top",
          duration: 3000,
        });
        // this.checkShowCoachmarkReplay()
      } else {
        this.setState({
          isEventEnded: false,
        });
        // this.checkShowCoachmarkChatSupport()
      }
    }
  };

  componentWillUnMount() {
    rol();
  }

  setMenuRef = (ref) => {
    this.menu = ref;
  };

  _renderDate = (eventStartDate, eventEndDate) => {
    if (eventStartDate != null && eventEndDate != null) {
      const shownDate = convertShownDate(eventStartDate, eventEndDate);

      return (
        <CardItem>
          <Left style={{ paddingHorizontal: 16 }}>
            {/* <Icon style={styles.mediumIconSize} type='FontAwesome' name='calendar-o' /> */}
            <Image
              style={styles.mediumImageSize}
              source={require("./../../assets/images/icons-dateEvent.png")}
            />
            <Body>
              <Text style={styles.textLabelMedium}>Date</Text>
              <Text style={styles.textMediumRed}>{shownDate}</Text>
            </Body>
          </Left>
        </CardItem>
      );
    }
    return null;
  };

  _renderPrice = () => {
    let currency =
      this.state.mEvent.currency == "IDR" ? "Rp" : this.state.mEvent.currency;
    let price = currency + numberCurrencyFormat(this.state.priceEvent);
    console.log("meEvent: ", this.state.mEvent);
    return (
      <CardItem>
        <Left style={{ paddingHorizontal: 16 }}>
          {/* <Icon style={styles.mediumIconSize} type='FontAwesome' name='calendar-o' /> */}
          <Image
            style={styles.mediumImageSize}
            source={require("./../../assets/images/icons-price.png")}
          />
          <Body>
            <Text style={styles.textLabelMedium}>Price</Text>
            <Text style={styles.textMediumRed}>{price}</Text>
          </Body>
        </Left>
      </CardItem>
    );
  };

  _renderDownloadReceipt = (params) => {
    console.warn("linkPdf", params.receipt);
    if (
      this.state.isRegistered == true &&
      this.state.isSuccessPayment == true
    ) {
      if (params.receipt != null) {
        // if (linkPdf.replace("https://d2doss.oss-ap-southeast-5.aliyuncs.com/pdf/event/", "") != "") {
        return (
          <TouchableOpacity
            // {...testID('buttonDownloadFile')}
            accessibilityLabel="button_down_load_file"
            onPress={() => downloadFile(params.receipt)}
            activeOpacity={0.7}
          >
            <CardItem>
              <Left style={{ paddingHorizontal: 16 }}>
                {/* <Icon style={styles.mediumIconSize} name='md-download' /> */}
                <Image
                  style={styles.mediumImageSize}
                  source={require("./../../assets/images/icons-downloadreceipt.png")}
                />
                <Body>
                  <Text style={styles.textLabelMedium}>Download Receipt</Text>
                  <Text style={styles.textMediumRed}>Receipt.pdf</Text>
                </Body>
              </Left>
            </CardItem>
          </TouchableOpacity>
        );
        // }
      }

      return null;
    }
  };

  openMap = (locationName, latitude, longitude) => {
    if (latitude && longitude) {
      showLocation({
        latitude: latitude,
        longitude: longitude,
        title: locationName,
      });
    }
  };

  _renderLocation = (locationName, latitude, longitude) => {
    if (locationName) {
      return (
        <TouchableOpacity
          // {...testID('buttonOpenLocation')}
          accessibilityLabel="button_open_location"
          onPress={() =>
            this.openMap(locationName, latitude, longitude) +
            this.sendAdjust(from.LOC)
          }
          activeOpacity={0.7}
        >
          <CardItem>
            <Left style={{ paddingHorizontal: 16 }}>
              {/* <Icon style={styles.mediumIconSize} type='MaterialIcons' name='place' /> */}
              <Image
                style={styles.mediumImageSize}
                source={require("./../../assets/images/icons_locationEvent.png")}
              />
              <Body>
                <Text style={styles.textLabelMedium}>Location</Text>
                <Text style={styles.textMediumRed}>{locationName}</Text>
              </Body>
            </Left>
          </CardItem>
          {this._renderMapsInLocation(locationName, latitude, longitude)}
        </TouchableOpacity>
      );
    }

    return null;
  };

  _renderWebsite = (url) => {
    if (url) {
      return (
        <TouchableOpacity
          // {...testID('buttonWebsite')}
          accessibilityLabel="button_website"
          onPress={() =>
            openOtherApp(url, {}) +
            AdjustTracker(AdjustTrackerConfig.Event_Open_Website)
          }
          activeOpacity={0.7}
        >
          <CardItem>
            <Left style={{ paddingHorizontal: 16 }}>
              {/* <Icon style={styles.mediumIconSize} name='ios-link' /> */}
              <Image
                style={styles.mediumImageSize}
                source={require("./../../assets/images/icons_websiteEvent.png")}
              />
              <Body>
                <Text style={styles.textLabelMedium}>Website</Text>
                <Text style={styles.textMediumRed}>{url}</Text>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      );
    }

    return null;
  };

  _renderDownloadPdf = (linkPdf, filename) => {
    console.warn("linkPdf", linkPdf);
    if (linkPdf != null) {
      let env = Config.ENV === "production" ? "" : "dev/";

      if (
        linkPdf.replace(
          `https://d2doss.oss-ap-southeast-5.aliyuncs.com/${env}pdf/event/`,
          ""
        ) != ""
      ) {
        return (
          <TouchableOpacity
            // {...testID('buttonDownloadFile')}
            accessibilityLabel="button_down_load_file"
            onPress={() =>
              downloadFile(linkPdf) +
              AdjustTracker(AdjustTrackerConfig.Event_Open_Announcement)
            }
            activeOpacity={0.7}
          >
            <CardItem>
              <Left style={{ paddingHorizontal: 16 }}>
                {/* <Icon style={styles.mediumIconSize} name='md-download' /> */}
                <Image
                  style={styles.mediumImageSize}
                  source={require("./../../assets/images/icons_downloadEvent.png")}
                />
                <Body>
                  <Text style={styles.textLabelMedium}>
                    Download Announcement
                  </Text>
                  <Text style={styles.textMediumRed}>Announcement File</Text>
                </Body>
              </Left>
            </CardItem>
          </TouchableOpacity>
        );
      }
    }

    return null;
  };

  _renderCompetence = (subscription) => {
    return (
      <CardItem>
        <View style={{ paddingHorizontal: 16 }}>
          <Text style={styles.textLabelBold}>Specialist</Text>
          <View style={styles.viewCompetence}>
            {this._renderListSpecialist(subscription)}
          </View>
        </View>
      </CardItem>
    );
  };

  _renderListSpecialist = (subscription) => {
    let items = [];

    if (subscription != null && subscription.length > 0) {
      let self = this;
      subscription.map(function(d, i) {
        items.push(<Tag key={i} value={d.title} />);
      });
    }

    return items;
  };

  _renderMapsInLocation = (locationName, latitude, longitude) => {
    if ((latitude, longitude)) {
      const locationProps = {
        latitude: `${latitude}`,
        longitude: `${longitude}`,
        zoom: 15,
        scale: 2,
        size: {
          width: 300,
          height: 200,
        },
        style: {
          height: 106,
          width: "100%",
          borderRadius: 7,
        },
      };

      return (
        <TouchableOpacity
          // {...testID('buttonMap')}
          accessibilityLabel="button_map"
          onPress={() => this.openMap(locationName, latitude, longitude)}
          activeOpacity={0.9}
        >
          <CardItem>
            <Body style={{ paddingRight: 6, paddingLeft: 52 }}>
              <GoogleStaticMap {...locationProps} />
            </Body>
          </CardItem>
        </TouchableOpacity>
      );
    }

    return null;
  };

  _renderMaps = (locationName, latitude, longitude) => {
    if ((latitude, longitude)) {
      const locationProps = {
        latitude: `${latitude}`,
        longitude: `${longitude}`,
        zoom: 15,
        scale: 2,
        size: {
          width: 300,
          height: 200,
        },
        style: { height: 200, width: "100%" },
      };

      return (
        <TouchableOpacity
          // {...testID('buttonMap')}
          accessibilityLabel="button_map"
          onPress={() => this.openMap(locationName, latitude, longitude)}
          activeOpacity={0.9}
        >
          <CardItem>
            <Body style={{ paddingHorizontal: 16 }}>
              <Text style={styles.textGrey}>Location</Text>
              <GoogleStaticMap {...locationProps} />
            </Body>
          </CardItem>
        </TouchableOpacity>
      );
    }

    return null;
  };

  _renderAttendee = (attendee, id) => {
    if (attendee != null && attendee == true) {
      return (
        <TouchableOpacity
          onPress={() => this.navigateToEventMateris(id)}
          activeOpacity={0.7}
        >
          <CardItem>
            <Left style={{ paddingHorizontal: 16 }}>
              <Icon style={styles.mediumIconSize} name="md-book" />
              <Body>
                <Text style={styles.textGrey}>Download Materi Event</Text>
                <Text style={styles.textMediumRed}>Material File</Text>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      );
    }

    return null;
  };

  _showDownloadMateris = () => {
    if (this.state.isShowMateri == true) {
      return (
        <TouchableOpacity
          onPress={() => this.navigateToEventMateris()}
          activeOpacity={0.7}
        >
          <CardItem>
            <Left style={{ paddingHorizontal: 16 }}>
              <Icon style={styles.mediumIconSize} name="md-book" />
              <Body>
                <Text style={styles.textGrey}>Download Materi Event</Text>
                <Text style={styles.textMediumRed}>Material File</Text>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      );
    }

    return null;
  };

  _renderLine = () => {
    return (
      <View
        style={{
          backgroundColor: "#E4E6E9",
          height: 1,
          flex: 1,
          marginHorizontal: 16,
        }}
      />
    );
  };
  _renderDescription = (description) => {
    if (description) {
      console.warn("description", escapeHtml(description, ""));
      return (
        <CardItem>
          <Body style={{ paddingHorizontal: 16 }}>
            <Text style={styles.textLabelBold}>Description</Text>
            <View style={{ height: 12 }} />
            <AutoHeightWebView
              ref={(ref) => (this.webview = ref)}
              style={{ width: wp("100%") - 60 }}
              source={{ html: escapeHtml(description, "") }}
              bounces={false}
              zoomable={false}
              scrollEnabled={false}
              customStyle={customCss}
              // onNavigationStateChange={(event) => {
              //     // this.webview.stopLoading();
              //     //comment code
              //     //because show toast "Tidak dapat menampilkan PDF (web tidak dapat dibuka)"
              //     //on Oppo f9 Device
              //     //Linking.openURL(event.url);
              //     if (event.url) {
              //         this.webview.stopLoading();
              //         Linking.openURL(event.url);
              //     }
              // }}
              onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
            />
          </Body>
        </CardItem>
      );
    }

    return null;
  };

  onShouldStartLoadWithRequest(request) {
    // HACK: allow some urls to be embedded, and reject others
    // https://github.com/react-native-community/react-native-webview/issues/381

    if (request.url.includes("http")) {
      Linking.openURL(request.url).catch((err) =>
        console.error(`Could not open URL ${request.url}`, err)
      );
      return false;
    } else {
      return true;
    }
  }

  _renderEmail = (email) => {
    if (email) {
      if (email instanceof Array) {
        if (email.length != 0) {
          return (
            <CardItem style={styles.backgroundCardCPDetail}>
              <Left style={{ paddingHorizontal: 16 }}>
                {/* <Icon style={styles.mediumIconSize} name="md-mail-open" /> */}
                <Image
                  style={styles.mediumImageSize}
                  source={require("./../../assets/images/icons_emailEvent.png")}
                />
                <Body>
                  <Text style={styles.textGrey}>Email</Text>
                  {email.map((value, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() => openOtherApp(`mailto:${value}`, {})}
                        activeOpacity={0.7}
                      >
                        <Text
                          style={styles.textMediumRed}
                          numberOfLines={1}
                          ellipsizeMode="tail"
                        >
                          {value}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                </Body>
              </Left>
            </CardItem>
          );
        }
      }

      //data lama
      else {
        console.log("_renderEmail data lama non array: ");
        if (!email.includes("[") && !email.includes("]")) {
          return (
            <CardItem style={styles.backgroundCardCPDetail}>
              <Left style={{ paddingHorizontal: 16 }}>
                {/* <Icon style={styles.mediumIconSize} name="md-mail-open" /> */}
                <Image
                  style={styles.mediumImageSize}
                  source={require("./../../assets/images/icons_emailEvent.png")}
                />
                <Body>
                  <Text style={styles.textGrey}>Email</Text>

                  <TouchableOpacity
                    onPress={() => openOtherApp(`mailto:${email}`, {})}
                    activeOpacity={0.7}
                  >
                    <Text
                      style={styles.textMediumRed}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {email}
                    </Text>
                  </TouchableOpacity>
                </Body>
              </Left>
            </CardItem>
          );
        }
      }
    }

    return null;
  };

  _renderPhone = (phone, name) => {
    console.log("_renderPhone phone: ", phone);

    if (phone) {
      if (phone instanceof Array && name instanceof Array) {
        console.log("_renderPhone length: ", phone.length);

        if (phone.length != 0) {
          return (
            <CardItem style={styles.backgroundCardCPDetail}>
              <Left style={{ paddingHorizontal: 16 }}>
                {/* <Icon style={styles.mediumIconSize} type="MaterialIcons" name="local-phone" /> */}
                <Image
                  style={styles.mediumImageSize}
                  source={require("./../../assets/images/icons_phoneEvent.png")}
                />
                <Body>
                  <Text style={styles.textGrey}>Phone</Text>
                  {phone.map((value, index) => {
                    if (value.trim() != "") {
                      return (
                        <TouchableOpacity
                          key={index}
                          onPress={() => openOtherApp(`tel:${value}`, {})}
                          activeOpacity={0.7}
                        >
                          <Text style={styles.textMediumRed}>
                            {value}
                            {name == "" || name == null
                              ? ""
                              : ` - ${name[index]}`}
                          </Text>
                        </TouchableOpacity>
                      );
                    }
                  })}
                </Body>
              </Left>
            </CardItem>
          );
        }
      } else {
        console.log("_renderPhone data lama non array: ");
        if (!phone.includes("[") && !phone.includes("]")) {
          return (
            <CardItem style={styles.backgroundCardCPDetail}>
              <Left style={{ paddingHorizontal: 16 }}>
                {/* <Icon style={styles.mediumIconSize} type="MaterialIcons" name="local-phone" /> */}
                <Image
                  style={styles.mediumImageSize}
                  source={require("./../../assets/images/icons_phoneEvent.png")}
                />
                <Body>
                  <Text style={styles.textGrey}>Phone</Text>

                  <TouchableOpacity
                    onPress={() => openOtherApp(`tel:${phone}`, {})}
                    activeOpacity={0.7}
                  >
                    <Text style={styles.textMediumRed}>
                      {phone}
                      {name == "" || name == null ? "" : ` - ${name}`}
                    </Text>
                  </TouchableOpacity>
                </Body>
              </Left>
            </CardItem>
          );
        }
      }
    }

    return null;
  };

  navigateToEventMateris() {
    let materis = this.state.mEvent.materi;
    AppState.removeEventListener("change", this._handleAppStateChange);

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: "EventMateris",
        params: { materis },
        key: `event-materis-${this.state.mEvent.id}`,
      })
    );
  }

  navigateToCertificate() {
    let event = this.state.mEvent;

    let filename =
      this.state.isSubmitted === true
        ? this.state.dataCertificate.filename
        : event.certificate.filename;
    let env = Config.ENV == "production" ? "" : "dev/";
    let data = {
      title: event.title,
      certificate: `https://static.d2d.co.id/${env}certificate/${
        this.uid
      }/${filename}`,
      typeCertificate: "event",
      isFromEventDetail: true,
    };
    console.log("certificate", data.certificate);
    let cmeCertificate = {
      type: "Navigate",
      routeName: "CmeCertificate",
      params: data,
    };
    this.props.navigation.navigate(cmeCertificate);
  }

  toggleBookmark(eventId) {
    // this.menu.hide();
    let actionBookmark = this.state.bookmark
      ? ParamAction.UNBOOKMARK
      : ParamAction.BOOKMARK;
    this.doBookmarkEvent(actionBookmark, eventId, false);
    this.setState({
      isTriggerChangeBookmark: true,
    });
  }

  async doBookmarkEvent(actionBookmark, eventId, isFromAddCalendar) {
    this.setState({ showLoader: true });
    let response = await doActionUserActivity(
      actionBookmark,
      ParamContent.EVENT,
      eventId
    );
    this.setState({ showLoader: false });
    if (response.isSuccess == true) {
      this.setState({ bookmark: !this.state.bookmark }, () => {
        let act = this.state.bookmark == true ? "bookmark" : "unbookmark";
        Toast.show({
          text: act + " succesfull",
          position: "top",
          duration: 3000,
        });

        if (
          this.state.bookmark == true &&
          this.state.mEvent != null &&
          !isFromAddCalendar
        ) {
          this.showAlertAddToCalendar();
        }
      });
    } else {
      Toast.show({ text: response.message, position: "top", duration: 3000 });
    }
  }

  showAlertAddToCalendar = () => {
    Alert.alert(
      "Information",
      "You can add this event to your calendar. Do you want to add this event to your calendar now?",
      [
        {
          text: "Later",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => this.addCalendar(this.state.mEvent),
        },
      ],
      { cancelable: true }
    );
  };

  async addCalendar(data) {
    // https://github.com/vonovak/react-native-add-calendar-event
    // this.menu.hide();
    // var start = moment.tz(data.start_date, "Asia/Jakarta");
    // var end = moment.tz(data.end_date, "Asia/Jakarta");
    var start = moment
      .utc(data.start_date)
      .format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
    var end = moment.utc(data.end_date).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");

    const eventConfig = {
      title: data.title,
      // startDate: data.start_date + "T05:00:00.000Z",
      // endDate: data.end_date + "T05:00:00.000Z",
      startDate: start,
      endDate: end,
    };

    AddCalendarEvent.presentEventCreatingDialog(eventConfig)
      .then((eventInfo) => {
        // handle success - receives an object with `calendarItemIdentifier` and `eventIdentifier` keys, both of type string.
        // These are two different identifiers on iOS.
        // On Android, where they are both equal and represent the event id, also strings.
        // when false is returned, the dialog was dismissed
        if (eventInfo) {
          // console.warn(JSON.stringify(eventInfo));
          this.autoBookmarkEvent(data.id);
        } else {
          console.warn("dismissed");
        }
      })
      .catch((error) => {
        console.warn("eventConfig", eventConfig);
        // handle error such as when user rejected permissions
        console.warn("error", error);
      });
  }

  autoBookmarkEvent = (eventId) => {
    if (this.state.bookmark <= 0) {
      this.doBookmarkEvent(ParamAction.BOOKMARK, eventId, true);
    }
  };

  async doViewEvent(eventId) {
    let viewEvent = await doActionUserActivity(
      ParamAction.VIEW,
      ParamContent.EVENT,
      eventId
    );
  }

  onBackPressed = async () => {
    //buat testing
    // await AsyncStorage.removeItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER)
    // await AsyncStorage.removeItem(STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED)
    // await AsyncStorage.removeItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_PAY_NOW)
    // await AsyncStorage.removeItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER)
    // await AsyncStorage.removeItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_JOIN)
    // await AsyncStorage.removeItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_REPLAY)

    this.sendAdjust(from.BACK_BUTTON);
    this.props.navigation.goBack();
    if (this.fromDeeplink == false) {
      if (
        this.state.from == "home" ||
        this.state.from == "SearchKonten" ||
        this.state.from == "SearchAllByKonten"
      ) {
        if (this.state.isTriggerChangeBookmark) {
          this.props.navigation.state.params.onRefreshDataSearchKonten();
          this.props.navigation.state.params.triggerRefreshHome();
        }
      } else {
        this.props.navigation.state.params.updateDataBookmark(
          this.state.bookmark
        );
      }
      // if (this.reserved) {
      //     this.props.navigation.state.params.updateDataReserve();
      // }
    }
  };

  navigateToEventReservation(dataEvent, manualUpdateReserve) {
    dataEvent.flag_reserve = this.reserved;
    AppState.removeEventListener("change", this._handleAppStateChange);

    this.props.navigation.navigate("EventReservation", {
      isReserved: this.isReserved,
      data: dataEvent,
      manualUpdateReserve: manualUpdateReserve,
    });
  }

  isReserved = () => {
    this.reserved = true;
    this.props.navigation.goBack();
    this.props.navigation.state.params.updateDataReserve();
  };

  renderBottomSheet = () => {
    return (
      <Modalize
        withOverlay={true}
        withHandle={true}
        handleStyle={{
          backgroundColor: "transparent",
        }}
        adjustToContentHeight={true}
        closeOnOverlayTap={false}
        panGestureEnabled={
          this.state.typeContentBottomSheet == "event_expired" ? true : false
        }
        ref={(ref) => {
          this.modalizeBottomSheet = ref;
        }}
        modalStyle={{
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingHorizontal: 16,
        }}
        overlayStyle={{
          backgroundColor: "#000000CC",
        }}
        onClose={async () =>
          this.state.typeContentBottomSheet == "event_expired"
            ? await this.setFlagStatusShowBottomSheet(this.state.mEvent)
            : null
        }
        HeaderComponent={
          this.state.typeContentBottomSheet == "event_expired" ? (
            <View
              style={{
                height: 4,
                borderRadius: 2,
                width: 40,
                marginTop: 8,
                backgroundColor: "#454F6329",
                alignSelf: "center",
              }}
            />
          ) : null
        }
      >
        {this.state.typeContentBottomSheet == "event_expired" &&
          this._renderContentBottomSheetEventEnded()}

        {this.state.typeContentBottomSheet == "event_reregist" &&
          this._renderContentBottomSheetReRegist()}
      </Modalize>
    );
  };

  setFlagStatusShowBottomSheet = async (params) => {
    let dataFlagBottomSheetEventHasEnded = await AsyncStorage.getItem(
      STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED
    );

    console.log(
      "checkAutoLaunchBottomSheet dataFlagBottomSheetEventHasEnded: ",
      dataFlagBottomSheetEventHasEnded
    );
    if (dataFlagBottomSheetEventHasEnded == null) {
      let event_id = [];
      event_id.push(params.id);
      dataFlagBottomSheetEventHasEnded = {
        event_id: event_id,
      };
      dataFlagBottomSheetEventHasEnded = JSON.stringify(
        dataFlagBottomSheetEventHasEnded
      );
      await AsyncStorage.setItem(
        STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED,
        dataFlagBottomSheetEventHasEnded
      );
    } else {
      dataFlagBottomSheetEventHasEnded = JSON.parse(
        dataFlagBottomSheetEventHasEnded
      );

      //only insert if data in asnyc storage not exist.
      if (!dataFlagBottomSheetEventHasEnded.event_id.includes(params.id)) {
        dataFlagBottomSheetEventHasEnded.event_id.push(params.id);
        dataFlagBottomSheetEventHasEnded = JSON.stringify(
          dataFlagBottomSheetEventHasEnded
        );
        await AsyncStorage.setItem(
          STORAGE_TABLE_NAME.BOTTOMSHEET_EVENT_HAS_ENDED,
          dataFlagBottomSheetEventHasEnded
        );
      }
    }
  };

  onPressButtonOkBottomSheeEventEnded = async () => {
    await this.setFlagStatusShowBottomSheet(this.state.mEvent);
    console.log("onPressButtonOkBottomSheeEventEnded");
    this.modalizeBottomSheet.close();
  };

  onPressButtonCancelBottomSheetOrderExpired = async () => {
    this.modalizeBottomSheet.close();

    if (
      this.state.mEvent.order.status != null &&
      this.state.mEvent.order.status == "pending"
    ) {
      try {
        this.setState({ showLoader: true });
        let params = {
          order_id: this.state.mEvent.order.order_id,
        };
        let response = await cancelPayment(params);
        console.log(response);

        if (response.isSuccess) {
          await this.setFlagStatusShowBottomSheet(this.state.mEvent);
          this.onRefresh();
        } else {
          this.modalizeBottomSheet.open();
          this.setState({ showLoader: false });
          Toast.show({
            text: response.message,
            position: "top",
            duration: 3000,
          });
        }
      } catch (error) {
        this.modalizeBottomSheet.open();
        this.setState({ showLoader: false });
        Toast.show({ text: error, position: "top", duration: 3000 });
      }
    } else {
      await this.setFlagStatusShowBottomSheet(this.state.mEvent);
      this.setState({
        isWaitingPayment: false,
        isRegistered: false,
      });
    }
  };

  _renderContentBottomSheetEventEnded = () => {
    return (
      <Col
        style={{
          alignItems: "center",
          marginBottom:
            platform.platform == "ios" &&
            platform.deviceHeight >= 812 &&
            platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <Image
          resizeMode="contain"
          style={{
            height: 92,
            width: 119,
            marginVertical: 32,
          }}
          source={require("../../assets/images/icon-expired.png")}
        />
        <Text
          bold
          style={{
            marginBottom: 16,
            textAlign: "center",
            fontSize: 20,
            color: "#1E1E20",
            fontFamily: "Nunito-Bold",
            lineHeight: 32,
            letterSpacing: 0.26,
          }}
        >
          Transaction Has Expired
        </Text>
        <Text
          regular
          style={{
            textAlign: "center",
            color: "#1E1E20",
            fontFamily: "Nunito-Regular",
            fontSize: 16,
            lineHeight: 26,
          }}
        >
          Because event has ended
        </Text>

        <Row
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            marginTop: 24,
          }}
        >
          <Button
            {...testID("button_ok_event_has_ended")}
            block
            style={{
              flex: 1,
              marginLeft: 8,
              height: 48,
              backgroundColor: "#D01E53",
              borderRadius: 8,
            }}
            onPress={() => this.onPressButtonOkBottomSheeEventEnded()}
          >
            <Text
              style={{
                color: "#FFFFFF",
                fontSize: 16,
                fontFamily: "Nunito-Regular",
                lineHeight: 26,
              }}
            >
              Ok
            </Text>
          </Button>
        </Row>
      </Col>
    );
  };
  _renderContentBottomSheetReRegist = () => {
    return (
      <Col
        style={{
          alignItems: "center",
          marginBottom:
            platform.platform == "ios" &&
            platform.deviceHeight >= 812 &&
            platform.deviceWidth >= 375
              ? 40
              : 16,
        }}
      >
        <Image
          resizeMode="contain"
          style={{
            height: 92,
            width: 119,
            marginVertical: 32,
          }}
          source={require("../../assets/images/icon-expired.png")}
        />
        <Text
          style={{
            marginBottom: 16,
            textAlign: "center",
            fontSize: 20,
            fontFamily: "Nunito-Bold",
          }}
        >
          Transaction Has Expired
        </Text>
        <Text
          regular
          style={{
            textAlign: "center",
            color: "#1E1E20",
            fontFamily: "Nunito-Regular",
            fontSize: 16,
            lineHeight: 26,
          }}
        >
          This payment of event has expired, please make a new event
          registration with click ‘Registration’ button below
        </Text>

        <Row
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            marginTop: 24,
          }}
        >
          <Button
            {...testID("button_cancel_change_country")}
            block
            style={{
              flex: 1,
              marginRight: 12,
              height: 48,
              backgroundColor: "#F6F6F7",
              borderRadius: 8,
            }}
            onPress={() => this.onPressButtonCancelBottomSheetOrderExpired()}
          >
            <Text
              style={{
                color: "#1E1E20",
                fontSize: 16,
                fontFamily: "Nunito-Regular",
                lineHeight: 26,
              }}
            >
              Cancel
            </Text>
          </Button>
          <Button
            {...testID("button_send_change_country")}
            block
            style={{
              flex: 1,
              marginLeft: 12,
              height: 48,
              backgroundColor: "#D01E53",
              borderRadius: 8,
            }}
            onPress={() => this.onPressRegistrationBottomSheet()}
          >
            <Text
              style={{
                color: "#FFFFFF",
                fontSize: 16,
                fontFamily: "Nunito-Regular",
                lineHeight: 26,
              }}
            >
              Registration
            </Text>
          </Button>
        </Row>
      </Col>
    );
  };

  renderModal(dataEvent) {
    return (
      <Modal
        hasBackdrop={true}
        avoidKeyboard={true}
        backdropOpacity={0.8}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}
        isVisible={this.state.modalVisible}
      >
        <ModalCertificate
          closeGoToCertificate={this.closeGoToCertificate}
          handlerStatusSubmit={this.handlerStatusSubmit}
          onVisibleModal={this.toggleModal}
          dataEvent={dataEvent}
        />
      </Modal>
    );
  }

  onPressMenuCertificate() {
    if (
      this.state.mEvent.is_close == false ||
      (this.state.mEvent.is_close && this.state.mEvent.is_verified)
    ) {
      if (this.state.mEvent.certificate === undefined) {
        if (this.state.isSubmitted === false) {
          this.toggleModal(true);
        } else {
          this.navigateToCertificate();
        }
      } else {
        this.navigateToCertificate();
      }
    }
  }

  onRefresh = () => {
    this.getParams();
  };

  onPressButtonPayNow = async () => {
    console.log("this.state.mEvent ", this.state.mEvent);

    if (this.checkStatusEventTransaction(this.state.mEvent) == false) {
      let data = {
        //url buat testing dapet dari sini
        //https://app.sandbox.midtrans.com/snap/v1/transactions
        urlSelectPayment: this.state.mEvent.order.redirect_url,
        order_id: this.state.mEvent.order.order_id,
        id_event: this.state.mEvent.id,
        type: EnumTypePayment.EVENT,
        amount: this.state.priceEvent,
      };
      AppState.removeEventListener("change", this._handleAppStateChange);

      let selectPayment = {
        type: "Navigate",
        routeName: "SelectPayment",
        params: data,
      };
      this.props.navigation.navigate(selectPayment);
    }
  };

  createOrder = async () => {
    let isFailed = false;
    let params = {
      id: this.state.mEvent.id,
      type: EnumTypePayment.EVENT,
      amount: this.state.priceEvent,
    };

    try {
      this.setState({ showLoader: true });

      let response = await startPayment(params);
      console.log(response);

      if (response.isSuccess) {
        let data = {
          //url buat testing dapet dari sini
          //https://app.sandbox.midtrans.com/snap/v1/transactions
          urlSelectPayment: response.docs.redirect_url + "#/select-payment",
          order_id: response.docs.order_id,
          id_event: this.state.mEvent.id,
          type: EnumTypePayment.EVENT,
          amount: this.state.priceEvent,
        };

        AppState.removeEventListener("change", this._handleAppStateChange);
        let selectPayment = {
          type: "Navigate",
          routeName: "SelectPayment",
          params: data,
        };
        this.props.navigation.navigate(selectPayment);
      } else {
        isFailed = true;
        this.setState({ showLoader: false });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      isFailed = true;
      this.setState({ showLoader: false });
      Toast.show({ text: error, position: "top", duration: 3000 });
    }
    return isFailed;
  };

  onPressRegistrationBottomSheet = async () => {
    this.modalizeBottomSheet.close();
    let params = {
      order_id: this.state.mEvent.order.order_id,
    };

    if (
      this.state.mEvent.order.status != null &&
      this.state.mEvent.order.status == "pending"
    ) {
      this.setState({ showLoader: true });

      try {
        let response = await cancelPayment(params);
        console.log(response);

        if (response.isSuccess) {
          if (this.createOrder() == false) {
            this.modalizeBottomSheet.open();
          } else {
            await this.setFlagStatusShowBottomSheet(this.state.mEvent);
          }
        } else {
          this.modalizeBottomSheet.open();
          this.setState({ showLoader: false });
          Toast.show({
            text: response.message,
            position: "top",
            duration: 3000,
          });
        }
      } catch (error) {
        this.modalizeBottomSheet.open();
        this.setState({ showLoader: false });
        Toast.show({ text: error, position: "top", duration: 3000 });
      }
    } else {
      if (this.createOrder() == false) {
        this.modalizeBottomSheet.open();
      } else {
        await this.setFlagStatusShowBottomSheet(this.state.mEvent);
      }
    }
  };

  onPressRegistration = async () => {
    let orderStatus =
      typeof this.state.mEvent.order != "undefined"
        ? this.state.mEvent.order.status
        : null;
    let expiredDate =
      typeof this.state.mEvent.order != "undefined"
        ? this.state.mEvent.order.expired
        : null;

    if (
      typeof this.state.mEvent.order == "undefined" ||
      (orderStatus == null &&
        isExpired(this.state.mEvent.current_time, expiredDate))
    ) {
      console.log("log masuk start payment");
      this.setState({ showLoader: true });

      let params = {
        id: this.state.mEvent.id,
        type: EnumTypePayment.EVENT,
        amount: this.state.priceEvent,
      };
      try {
        let response = await startPayment(params);
        console.log(response);

        if (response.isSuccess) {
          this.setState({ showLoader: false });
          let data = {
            //url buat testing dapet dari sini
            //https://app.sandbox.midtrans.com/snap/v1/transactions
            urlSelectPayment: response.docs.redirect_url + "#/select-payment",
            order_id: response.docs.order_id,
            id_event: this.state.mEvent.id,
            type: EnumTypePayment.EVENT,
            amount: this.state.priceEvent,
          };
          console.log("LOG ISI URL", response.docs.redirect_url);
          AppState.removeEventListener("change", this._handleAppStateChange);

          let selectPayment = {
            type: "Navigate",
            routeName: "SelectPayment",
            params: data,
          };
          this.props.navigation.navigate(selectPayment);
        } else {
          this.setState({ showLoader: false });
          Toast.show({
            text: response.message,
            position: "top",
            duration: 3000,
          });
        }
      } catch (error) {
        this.setState({ showLoader: false });
        Toast.show({ text: error, position: "top", duration: 3000 });
      }
    } else {
      if (this.checkStatusEventTransaction(this.state.mEvent) == false) {
        console.log("log tidak masuk start payment");
        if (this.state.mEvent.order.status == "failure") {
          this.createOrder();
        } else {
          let redirectUrl = this.state.mEvent.order.redirect_url;
          let url =
            this.state.mEvent.order.status == null
              ? redirectUrl + "#/select-payment"
              : redirectUrl;
          let data = {
            //url buat testing dapet dari sini
            //https://app.sandbox.midtrans.com/snap/v1/transactions
            urlSelectPayment: url,
            order_id: this.state.mEvent.order.order_id,
            id_event: this.state.mEvent.id,
            type: EnumTypePayment.EVENT,
            amount: this.state.priceEvent,
          };
          console.log("LOG ISI URL", url);
          AppState.removeEventListener("change", this._handleAppStateChange);
          let selectPayment = {
            type: "Navigate",
            routeName: "SelectPayment",
            params: data,
          };
          this.props.navigation.navigate(selectPayment);
        }
      }
    }
  };

  onPressChatSupport = async () => {
    AdjustTracker(AdjustTrackerConfig.Event_Open_Contact_Us);
    let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    profile = JSON.parse(profile);
    // On button press, when you want to show chat:
    ZendeskChat.startChat({
      name: profile.name,
      email: profile.email,
      phone: profile.phone,
      tags: [this.state.mEvent.title, "D2D"],
      department: "D2D",
      // The behaviorFlags are optional, and each default to 'true' if omitted
      behaviorFlags: {
        showAgentAvailability: true,
        showChatTranscriptPrompt: true,
        showPreChatForm: true,
        showOfflineForm: true,
      },
      // The preChatFormOptions are optional & each defaults to "optional" if omitted
      preChatFormOptions: {
        name: "required",
        email: "optional",
        phone: "optional",
        department: "required",
      },
      localizedDismissButtonTitle: "Dismiss",
    });
  };

  onPressButtonPositiveCoachmarkCS = async () => {
    this.coachmarkChatSupport.hide();

    if (this.coachmarkRegistration != null) {
      this.coachmarkRegistration.show();
    } else {
      AsyncStorage.setItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER, "Y");
    }
  };

  onPressButtonPositiveCoachmarkRegistration = async () => {
    this.coachmarkRegistration.hide();
    AsyncStorage.setItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_CS_REGISTER, "Y");
  };

  onPressButtonPositiveCoachmarkPayNow = async () => {
    this.setState({
      isAutoShowCoachmarkPayNow: false,
    });
    this.coachmarkPayNow.hide();
    AsyncStorage.setItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_PAY_NOW, "Y");
  };

  onPressButtonPositiveCoachmarkJoin = async () => {
    this.setState({
      isAutoShowCoachmarkJoin: false,
    });
    this.coachmarkJoin.hide();
    AsyncStorage.setItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_JOIN, "Y");
  };

  onPressButtonPositiveCoachmarkReplay = async () => {
    console.log(
      "isAutoShowCoachmarkReplay ",
      this.state.isAutoShowCoachmarkReplay
    );
    this.setState({
      isAutoShowCoachmarkReplay: false,
    });
    this.coachmarkReplay.hide();
    AsyncStorage.setItem(STORAGE_TABLE_NAME.COACHMARK_EVENT_REPLAY, "Y");
  };

  onPressReplay = () => {
    if (this.state.webinarID != 0) {
      this.gotoWebinarDetail();
    } else {
      Toast.show({
        text: `Something went wrong ,please contact us !`,
        position: "bottom",
        duration: 3000,
        style: {
          alignSelf: "center",
          marginBottom: 78,
          flex: 0,
          borderRadius: 8,
        },
        textStyle: {
          textAlign: "center",
          justifyContent: "center",
          flex: 0,
          alignSelf: "center",
        },
      });
    }
  };

  gotoWebinarDetail = () => {
    let tempData = {
      id: this.state.webinarID,
      group_cid: this.state.mEvent.webinar.group_cid,
      isPaid: this.state.priceEvent != 0 ? true : false,
    };

    console.log("LOG TempData Webinar", tempData, "--", this.state.priceEvent);
    AppState.removeEventListener("change", this._handleAppStateChange);

    let data = this.state.mEvent;
    data.server_date = data.current_time;

    let dateShowTNC = moment(data.webinar.start_date)
      .add(-1, "hours")
      .format("YYYY-MM-DD HH:mm:ss");

    let isStartShowTNC = isStarted(data.server_date, dateShowTNC);

    let routeNameDestination =
      data.webinar.has_tnc == true && isStartShowTNC == true
        ? "WebinarTNC"
        : "WebinarVideo";

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: routeNameDestination,
        params: tempData,
      })
    );
  };

  onPressButtonJoin = () => {
    console.log("LOG WebinarID", this.state.webinarID);
    if (this.state.webinarID != 0) {
      this.gotoWebinarDetail();
    } else {
      let formatDate = "YYYY-MM-DD HH:mm:ss";
      let eventStartDate = momentTimezone(
        this.state.mEvent.start_date,
        formatDate
      ).format("DD MMMM YYYY");
      Toast.show({
        text: `Available on ${eventStartDate}`,
        position: "bottom",
        duration: 3000,
        style: {
          alignSelf: "center",
          marginBottom: 78,
          flex: 0,
          borderRadius: 8,
        },
        textStyle: {
          textAlign: "center",
          justifyContent: "center",
          flex: 0,
          alignSelf: "center",
        },
      });
    }
  };

  _renderButtonNavigation = () => {
    let nav = this.props.navigation;
    let {
      params: { data, manualUpdateReserve },
    } = nav.state;

    let dataEvent = this.state.mEvent != null ? this.state.mEvent : data;

    let marginHorizontalPayNow = 0;
    let marginHorizontalJoin = 0;
    let marginHorizontalReplay = 0;
    if (
      this.state.isShowCertificate == true ||
      this.state.isShowMateri == true
    ) {
      marHorRegistration = 0;
    } else {
      marHorRegistration = 29;
      marginHorizontalPayNow = 29;
      marginHorizontalJoin = 29;
      marginHorizontalReplay = 29;
    }

    let widthSpaceButtonReplay = 3;
    let widthSpaceButtonJoin = 3;

    if (this.state.isShowMateri && this.state.isShowCertificate) {
      widthSpaceButtonReplay = 5;
      widthSpaceButtonJoin = 5;
    } else if (
      (this.state.isShowMateri == true &&
        this.state.isShowCertificate == false) ||
      (this.state.isShowMateri == false && this.state.isShowCertificate == true)
    ) {
      widthSpaceButtonReplay = 4;
      widthSpaceButtonJoin = 4;
    }

    if (this.state.isButtonNavigation) {
      return (
        <View style={styles.viewBottom}>
          <TouchableOpacity
            {...testID("button_bookmark")}
            onPress={() =>
              this.toggleBookmark(dataEvent.id) + this.sendAdjust(from.BOOKMARK)
            }
            style={styles.touchItem}
          >
            {this.state.bookmark ? (
              <Image
                style={styles.mediumImageSize}
                source={require("./../../assets/images/icons-bookmarked.png")}
              />
            ) : (
              <Image
                style={styles.mediumImageSize}
                source={require("./../../assets/images/icons-bookmark.png")}
              />
            )}
            <Text style={[styles.textMenuBottom, { marginTop: 5 }]}>
              Bookmark
            </Text>
          </TouchableOpacity>

          {((this.state.priceEvent != 0 &&
            this.state.isWaitingPayment == false &&
            this.state.isEventEnded == false &&
            this.state.isSuccessPayment == false) ||
            (this.state.mEvent != null &&
              this.state.mEvent.order != null &&
              this.state.mEvent.order.status == null &&
              isExpired(
                this.state.mEvent.current_time,
                this.state.mEvent.order.expired
              ))) && (
            <View style={styles.containerCoachmark}>
              <Coachmark
                ref={(ref) => {
                  this.coachmarkRegistration = ref;
                }}
                // autoShow={false}

                title="Event Registration"
                message="Tekan registration untuk daftar event."
                type="event_registration"
                onPressButtonNegative={() =>
                  this.coachmarkRegistration.hide() +
                  this.coachmarkChatSupport.show()
                }
                onShow={() => {
                  this.setState({
                    isButtonRegistrationDisable: true,
                  });
                }}
                onHide={() => {
                  this.setState({
                    isButtonRegistrationDisable: false,
                  });
                }}
                onPressButtonPositive={() =>
                  this.onPressButtonPositiveCoachmarkRegistration()
                }
                buttonTextNegative="Kembali"
                buttonTextPositive="Selesai"
                dotActivePosition={2}
                dotLength={2}
              >
                <TouchableOpacity
                  {...testID("button_registrasi")}
                  disabled={this.state.isButtonRegistrationDisable}
                  onPress={() => this.onPressRegistration()}
                  style={[
                    styles.touchItem,
                    { width: platform.deviceWidth / 3 },
                  ]}
                >
                  <Image
                    style={styles.mediumImageSize}
                    source={require("./../../assets/images/icons-registration.png")}
                  />
                  <Text style={[styles.textMenuBottom, { marginTop: 5 }]}>
                    {translate("registration")}
                  </Text>
                </TouchableOpacity>
              </Coachmark>
            </View>
          )}

          {this.state.isRegistered == true &&
            this.state.isSuccessPayment == true &&
            this.state.isEventEnded != true && (
              <View style={styles.containerCoachmark}>
                <Coachmark
                  ref={(ref) => {
                    this.coachmarkJoin = ref;
                  }}
                  autoShow={this.state.isAutoShowCoachmarkJoin}
                  title="Pembayaran Berhasil"
                  message="Anda sudah bisa masuk event sekarang"
                  type="event_join"
                  onPressButtonPositive={() =>
                    this.onPressButtonPositiveCoachmarkJoin()
                  }
                  buttonTextPositive="Tutup"
                  onShow={() => {
                    this.setState({
                      isButtonJoinDisable: true,
                    });
                  }}
                  onHide={() => {
                    this.setState({
                      isButtonJoinDisable: false,
                    });
                  }}
                >
                  <TouchableOpacity
                    {...testID("button_join")}
                    disabled={this.state.isButtonJoinDisable}
                    onPress={() => this.onPressButtonJoin()}
                    style={[
                      styles.touchItem,
                      { width: platform.deviceWidth / widthSpaceButtonJoin },
                    ]}
                  >
                    <Image
                      style={[styles.mediumImageSize]}
                      source={require("./../../assets/images/icons-join.png")}
                    />
                    <Text
                      style={[
                        styles.textMenuBottom,
                        {
                          marginTop: 5,
                          marginHorizontal: marginHorizontalJoin,
                        },
                      ]}
                    >
                      Join
                    </Text>
                  </TouchableOpacity>
                </Coachmark>
              </View>
            )}

          {this.state.priceEvent != 0 &&
            this.state.isWaitingPayment == true &&
            this.state.isAlreadyPaid == false && (
              <View style={styles.containerCoachmark}>
                <Coachmark
                  ref={(ref) => {
                    this.coachmarkPayNow = ref;
                  }}
                  autoShow={this.state.isAutoShowCoachmarkPayNow}
                  title="Selesaikan Pembayaran"
                  message="Registrasi akan hangus jika belum dibayar"
                  type="event_paynow"
                  onPressButtonPositive={() =>
                    this.onPressButtonPositiveCoachmarkPayNow()
                  }
                  onShow={() => {
                    this.setState({
                      isButtonPayNowDisable: true,
                    });
                  }}
                  onHide={() => {
                    this.setState({
                      isButtonPayNowDisable: false,
                    });
                  }}
                  buttonTextPositive="Tutup"
                >
                  <TouchableOpacity
                    {...testID("button_bayarsekarang")}
                    disabled={this.state.isButtonPayNowDisable}
                    onPress={() => this.onPressButtonPayNow()}
                    style={[
                      styles.touchItem,
                      { width: platform.deviceWidth / 3 },
                    ]}
                  >
                    <Image
                      style={styles.mediumImageSize}
                      source={require("./../../assets/images/icons-bayarsekarang.png")}
                    />
                    <Text style={[styles.textMenuBottom, { marginTop: 5 }]}>
                      Pay Now
                    </Text>
                  </TouchableOpacity>
                </Coachmark>
              </View>
            )}

          {this.state.isRegistered == true &&
            this.state.isSuccessPayment == true &&
            this.state.isEventEnded != false && (
              <View style={styles.containerCoachmark}>
                <Coachmark
                  ref={(ref) => {
                    this.coachmarkReplay = ref;
                  }}
                  autoShow={this.state.isAutoShowCoachmarkReplay}
                  title="Webinar sudah tidak live"
                  message="Anda masih bisa menonton recorded webinar, namun sesi tanya jawab sudah tidak tersedia."
                  type="event_paynow"
                  onPressButtonPositive={() =>
                    this.onPressButtonPositiveCoachmarkReplay()
                  }
                  buttonTextPositive="Tutup"
                  onShow={() => {
                    this.setState({
                      isButtonReplayDisable: true,
                    });
                  }}
                  onHide={() => {
                    this.setState({
                      isButtonReplayDisable: false,
                    });
                  }}
                >
                  <TouchableOpacity
                    {...testID("button_replay")}
                    disabled={this.state.isButtonReplayDisable}
                    onPress={() => this.onPressReplay()}
                    style={[
                      styles.touchItem,
                      { width: platform.deviceWidth / widthSpaceButtonReplay },
                    ]}
                  >
                    <Image
                      style={styles.mediumImageSize}
                      source={require("./../../assets/images/icons-replay.png")}
                    />
                    <Text
                      style={[
                        styles.textMenuBottom,
                        {
                          marginTop: 5,
                          marginHorizontal: marginHorizontalReplay,
                        },
                      ]}
                    >
                      Replay
                    </Text>
                  </TouchableOpacity>
                </Coachmark>
              </View>
            )}

          <TouchableOpacity
            {...testID("button_calendar")}
            onPress={() =>
              this.addCalendar(dataEvent) + this.sendAdjust(from.CALENDAR)
            }
            style={styles.touchItem}
          >
            <Image
              style={styles.mediumImageSize}
              source={require("./../../assets/images/icons-calendar.png")}
            />
            <Text style={[styles.textMenuBottom, { marginTop: 5 }]}>
              {translate("calendar")}
            </Text>
          </TouchableOpacity>

          {this.state.isShowMateri == true && (
            <TouchableOpacity
              {...testID("button_materi")}
              onPress={() =>
                this.navigateToEventMateris() + this.sendAdjust(from.MATERI)
              }
              style={styles.touchItem}
            >
              <Image
                style={styles.mediumImageSize}
                source={require("./../../assets/images/icons-material.png")}
              />
              <Text style={[styles.textMenuBottom, { marginTop: 5 }]}>
                Material
              </Text>
            </TouchableOpacity>
          )}

          {this.state.isShowCertificate == true && (
            <TouchableOpacity
              {...testID("button_e_certificate")}
              onPress={() =>
                this.onPressMenuCertificate() +
                this.sendAdjust(from.CERTIFICATE)
              }
              style={styles.touchItem}
            >
              <Image
                style={styles.mediumImageSize}
                source={
                  this.state.mEvent.is_close == false ||
                  (this.state.mEvent.is_close && this.state.mEvent.is_verified)
                    ? require("./../../assets/images/icons-certificate-new.png")
                    : require("./../../assets/images/icons-certificate-disable-new.png")
                }
              />
              <Text
                style={[
                  styles.textMenuBottom,
                  {
                    color:
                      this.state.mEvent.is_close == false ||
                      (this.state.mEvent.is_close &&
                        this.state.mEvent.is_verified)
                        ? "#1E201F"
                        : "#8F8F90",
                  },
                  { marginTop: 5 },
                ]}
              >
                Certificate
              </Text>
            </TouchableOpacity>
          )}
        </View>
      );
    } else {
      return null;
    }
  };

  navigateToBooth = () => {
    // temporary hide exhibition
    // let paramsData = {
    //   exhibitionId:
    //     this.state.exhibitionData != null ? this.state.exhibitionData.id : null,
    // };
    // this.props.navigation.navigate("Booth", paramsData);
  };

  render() {
    let nav = this.props.navigation;
    let {
      params: { data, manualUpdateReserve },
    } = nav.state;

    let dataEvent = this.state.mEvent != null ? this.state.mEvent : data;

    let marginHorizontalPayNow = 0;
    let marginHorizontalJoin = 0;
    let marginHorizontalReplay = 0;
    if (
      this.state.isShowCertificate == true ||
      this.state.isShowMateri == true
    ) {
      marHorRegistration = 0;
    } else {
      marHorRegistration = 29;
      marginHorizontalPayNow = 29;
      marginHorizontalJoin = 29;
      marginHorizontalReplay = 29;
    }

    return (
      <Container>
        {this.renderBottomSheet()}
        {/* <BottomSheet
                    title={"Transaction Has Expired"}
                    message={"Please try again to make a new registration"}
                    show={this.state.isShowingBottomSheet}
                ></BottomSheet> */}
        <View
          style={{
            width: wp("200%"),
            height: wp("200%"),
            borderRadius: wp("100%"),
            // backgroundColor: platform.toolbarDefaultBg,
            backgroundColor: "white",
            position: "absolute",
            left: -wp("50%"),
            top: -wp("100%"),
          }}
        />
        <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
          <Header noShadow>
            <Left style={{ flex: 0.5 }}>
              <Button
                // {...testID('buttonBack')}
                {...testID("button_back")}
                accessibilityLabel="button_back"
                transparent
                onPress={() => this.onBackPressed()}
              >
                <Icon name="md-arrow-back" style={styles.toolbarIcon} />
              </Button>
            </Left>
            <Body
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Title>Event</Title>
            </Body>
            <Right style={{ flex: 0.5, flexDirection: "row" }}>
              {/*
                // temporary hide exhibition
               {this.state.priceEvent != 0 &&
                this.state.isTypeVirtualExhibition && (
                  <TouchableOpacity onPress={() => this.onPressChatSupport()}>
                    <Image
                      style={styles.iconHeader}
                      source={require("./../../assets/images/icons-helpwithredcircle.png")}
                    />
                  </TouchableOpacity>
                )} */}

              <TouchableOpacity
                {...testID("button_share")}
                onPress={() =>
                  share(
                    this.shareLink != null
                      ? this.shareLink
                      : dataEvent.url[0].app_link
                  ) + this.sendAdjust(from.SHARE)
                }
                transparent
              >
                <IconShare style={styles.iconHeader} />
                {/* <Icon name="md-share" style={{ fontSize: 28, marginVertical:10 }} /> */}
              </TouchableOpacity>
            </Right>
          </Header>
        </SafeAreaView>
        <View style={{ flex: 1, flexDirection: "column-reverse" }}>
          {this.state.priceEvent != 0 && (
            // termporary hide exhibition
            // && !this.state.isTypeVirtualExhibition && (
            <TouchableOpacity
              onPress={() =>
                //this.coachmarkChatSupport.show()
                this.onPressChatSupport()
              }
              style={{
                zIndex: 1000,
                position: "absolute",
                bottom:
                  platform.platform == "ios" &&
                  platform.deviceHeight >= 812 &&
                  platform.deviceWidth >= 375
                    ? 106
                    : 72,
                right: 12,
              }}
            >
              <Coachmark
                ref={(ref) => {
                  this.coachmarkChatSupport = ref;
                }}
                autoShow={this.state.isAutoShowCoachmarCS}
                title="D2D Support"
                message="Jika ada pertanyaan, tekan tombol icon ini untuk mendapatkan bantuan."
                onPressButtonPositive={() =>
                  this.onPressButtonPositiveCoachmarkCS()
                }
                buttonTextPositive={
                  this.coachmarkRegistration != null ? "Lanjut" : "Tutup"
                }
                type="event_chatsupport"
                dotLength={this.coachmarkRegistration != null ? 2 : 0}
                dotActivePosition={1}
              >
                <Image
                  style={styles.imageHelp}
                  source={require("./../../assets/images/icons-helpwithredcircle.png")}
                />
              </Coachmark>
            </TouchableOpacity>
          )}

          <SafeAreaView>{this._renderButtonNavigation()}</SafeAreaView>

          {/* 
          // temporary hide exhibition  
        {this.state.isTypeVirtualExhibition &&
          this.state.isVirtualExhibitionEnabledType != 0 ? (
            this.state.isVirtualExhibitionEnabledType == 2 ? (
              <TouchableOpacity
                style={styles.virtualExhibitionContainer}
                onPress={() => this.navigateToBooth()}
              >
                <IconVirtualExhibition height={48} />
                <Text style={styles.virtualExhibitionText}>
                  PAMERAN VIRTUAL
                </Text>
                <IconExpandMoreWhite
                  width={24}
                  height={24}
                  style={{ marginRight: 12 }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.virtualExhibitionContainerDisabled}
                disabled={true}
              >
                <IconVirtualExhibitionDisabled height={48} />
                <Text style={styles.virtualExhibitionTextDisabled}>
                  PAMERAN VIRTUAL
                </Text>
                <IconNavigateNextDisabled
                  width={24}
                  height={24}
                  style={{ marginRight: 12 }}
                />
              </TouchableOpacity>
            )
          ) : null} */}

          <ScrollView
            refreshControl={
              <RefreshControl refreshing={false} onRefresh={this.onRefresh} />
            }
          >
            <Content>
              <View style={{ padding: 10 }}>
                <Card
                  style={{
                    flex: 0,
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                  }}
                >
                  {(this.state.isRegistered == true ||
                    (this.state.priceEvent != 0 &&
                      this.state.isEventEnded == true)) && (
                    <CardItem
                      style={{
                        height: 56,
                        backgroundColor: "#F7F8FA",
                        justifyContent: "center",
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                      }}
                    >
                      {this.state.isWaitingPayment &&
                        this.state.isEventEnded == false && (
                          <Text
                            {...testID("waitingforpayment")}
                            style={{
                              color: "#1E1E20",
                              fontSize: 20,
                              fontWeight: "bold",
                              fontFamily: "Nunito",
                            }}
                          >
                            Waiting for Payment
                          </Text>
                        )}
                      {this.state.isSuccessPayment &&
                        this.state.isEventEnded == false && (
                          <Text
                            {...testID("paymentsuccess")}
                            style={{
                              color: "#1E1E20",
                              fontSize: 20,
                              fontWeight: "bold",
                              fontFamily: "Nunito",
                            }}
                          >
                            Payment Success
                          </Text>
                        )}
                      {this.state.isEventEnded && (
                        <Text
                          {...testID("eventhasended")}
                          style={{
                            color: "#1E1E20",
                            fontSize: 20,
                            fontWeight: "bold",
                            fontFamily: "Nunito",
                          }}
                        >
                          Event has ended
                        </Text>
                      )}
                    </CardItem>
                  )}
                  <CardItem
                    style={{
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                    }}
                  >
                    <Row>
                      <Col size={95}>
                        <View style={{ marginTop: 12, paddingHorizontal: 16 }}>
                          {this.state.isRegistered &&
                            this.state.isSuccessPayment && (
                              <Text
                                {...testID("youareregister")}
                                style={{
                                  color: "#1E1E20",
                                  fontSize: 14,
                                  fontFamily: "Nunito",
                                  marginBottom: 12,
                                }}
                              >
                                You are registered as a attendee of :
                              </Text>
                            )}
                          <Text
                            {...testID("judul_event")}
                            style={styles.textTitle}
                          >
                            {dataEvent.title}
                          </Text>
                        </View>
                      </Col>
                      <Col size={5} />
                    </Row>
                  </CardItem>

                  {this._renderDate(dataEvent.start_date, dataEvent.end_date)}
                  {this.state.priceEvent != null &&
                    this.state.priceEvent != 0 &&
                    this._renderPrice()}
                  {dataEvent.order != null &&
                    this._renderDownloadReceipt(dataEvent.order)}
                  {this._renderLocation(
                    dataEvent.location,
                    dataEvent.latitude,
                    dataEvent.longitude
                  )}
                  {this._renderWebsite(dataEvent.web)}
                  {/* {this._renderAttendee(dataEvent.attendee, dataEvent.id)} */}
                  {this._renderDownloadPdf(
                    dataEvent.attachment,
                    dataEvent.title
                  )}
                  {this._renderPhone(dataEvent.cp_number, dataEvent.cp_name)}
                  {this._renderEmail(dataEvent.cp_email)}
                  {/* {this._showDownloadMateris()} */}
                  {this._renderLine()}
                  {this._renderCompetence(dataEvent.subscription)}
                  {/* {this._renderMaps(dataEvent.location, dataEvent.latitude, dataEvent.longitude)} */}
                  {this._renderDescription(dataEvent.long_description)}
                  <CardItem nopadding bordered />
                  {/* <CardItem nopadding>
                                <Button
                                    {...testID('buttonRSVP')}
                                    block transparent onPress={() => this.navigateToEventReservation(dataEvent, manualUpdateReserve)} style={{ flex: 1, height: 55 }} >
                                    <Text nopadding style={styles.textMediumRed}>RSVP Information</Text>
                                </Button>
                            </CardItem> */}
                </Card>
              </View>
              {/* 
              //temporary hide exhibition
              {this.state.isTypeVirtualExhibition && (
                <View style={{ height: 50 }} />
              )} */}
              {this.renderModal(dataEvent)}
            </Content>
          </ScrollView>
        </View>
        <Loader visible={this.state.showLoader} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  textTitle: {
    fontSize: 16,
    fontFamily: "Nunito-Bold",
    color: "#1E1E20",
  },
  textTerimakasih: {
    fontSize: 20,
    fontFamily: "Nunito-Bold",
  },
  textGrey: {
    fontSize: 14,
    fontFamily: "Nunito-Regular",
    color: "#1E1E20",
  },
  textLabelMedium: {
    fontSize: 14,
    fontFamily: "Nunito-Regular",
    color: "#1E1E20",
    fontWeight: "500",
  },
  textLabelBold: {
    fontSize: 14,
    fontFamily: "Nunito-Regular",
    color: "#1E1E20",
    fontWeight: "bold",
  },
  textMenuBottom: {
    fontSize: 12,
    color: "#1E201F",
    fontFamily: "Nunito-Regular",
  },
  textMediumRed: {
    fontSize: 17,
    fontFamily: "Nunito-SemiBold",
    color: "#CB1D50",
  },
  mediumIconSize: {
    color: "#BBBBBB",
    fontSize: 23,
    height: 25,
    width: 25,
    textAlign: "center",
  },
  mediumImageSize: {
    height: 25,
    width: 25,
  },
  toolbarIcon: {
    color: "#FFFFFF",
    fontSize: 25,
  },
  backgroundCompetence: {
    padding: 5,
    paddingHorizontal: 16,
    marginTop: 5,
    marginRight: 10,
    backgroundColor: "#F0F0F0",
    borderRadius: 5,
  },
  textDescription: {
    fontSize: 16,
    color: "#6C6C6C",
  },
  viewCompetence: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 10,
    flexWrap: "wrap",
  },
  viewBottom: {
    // flex: 1,
    flexDirection: "row",
    // position: 'absolute',
    // bottom: 0,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderTopColor: "#E4E6E9",
  },
  touchItem: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: 5,
    margin: 0,
    height: 56,
  },
  containerCoachmark: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    margin: 0,
    paddingHorizontal: 8,
    height: 56,
  },
  coverIconHelp: {
    marginRight: 16,
    marginBottom: 0,
    backgroundColor: "#D01E53",
    padding: 5,
    width: 56,
    height: 56,
    borderRadius: 30,
    alignItems: "center",
    alignContent: "center",
  },
  imageHelp: {
    height: 56,
    width: 56,
    margin: 4,
  },
  iconHeader: {
    height: 48,
    width: 48,
  },
  virtualExhibitionContainer: {
    zIndex: 1000,
    position: "absolute",
    bottom:
      platform.platform == "ios" &&
      platform.deviceHeight >= 812 &&
      platform.deviceWidth >= 375
        ? 106
        : 72,
    right: 16,
    left: 16,
    backgroundColor: "#0771CD",
    borderRadius: 4,
    opacity: 1,
    position: "absolute",
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  virtualExhibitionContainerDisabled: {
    zIndex: 1000,
    position: "absolute",
    bottom:
      platform.platform == "ios" &&
      platform.deviceHeight >= 812 &&
      platform.deviceWidth >= 375
        ? 106
        : 72,
    right: 16,
    left: 16,
    backgroundColor: "#D7D7D7",
    borderRadius: 4,
    opacity: 1,
    position: "absolute",
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  virtualExhibitionText: {
    color: "#FFFFFF",
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    textTransform: "uppercase",
    letterSpacing: 0.9,
    lineHeight: 16,
    flexGrow: 1,
    paddingLeft: 22,
  },
  virtualExhibitionTextDisabled: {
    color: "#989898",
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    textTransform: "uppercase",
    letterSpacing: 0.9,
    lineHeight: 16,
    flexGrow: 1,
    paddingLeft: 22,
  },
});
