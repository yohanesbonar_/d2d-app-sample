import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { StatusBar, StyleSheet, View, Image, TouchableOpacity, FlatList, BackHandler } from 'react-native';
import Menu from 'react-native-material-menu';
import {
    Container, Content, Header, Title, Button, Icon, Text, Left, Body, Right,
    Row, Col, Form, Item, Toast, Card
} from 'native-base';
import { Loader, ListItemLearningSpecialist, CardItemEvent } from './../../components';

import platform from '../../../theme/variables/platform';
import { EnumFeedsCategory, STORAGE_TABLE_NAME, testID } from './../../libs/Common';

import { getEventMateri, ParamContent } from './../../libs/NetworkUtility';
import { SafeAreaView } from 'react-native';

export default class EventMateris extends Component {

    page = 1;

    state = {
        showLoader: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        onRefresh: false,
        emptyDataDownload: false,
        downloadList: [],
        subscription: null,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        const { params } = this.props.navigation.state;
        // if (params != null && params.id != null) {
        //     this.getDataMateri(params.id);
        // }
        if (params != null && params.materis != null) {
            console.log('materis', params.materis)
            let downloadList = []

            // if (params.materis.pdf != null && params.materis.pdf.length > 0) {
            //     // downloadList = params.materis.pdf
            //     params.materis.pdf.forEach(element => {
            //         let pdf = element;
            //         pdf.type = 'pdf_materi';

            //         downloadList.push(pdf)
            //     });
            // }

            // if (params.materis.video != null && params.materis.video.length > 0) {
            //     // downloadList = [...downloadList, ...params.materis.video]
            //     // video_materi
            //     params.materis.video.forEach(element => {
            //         let vid = element;
            //         vid.type = 'video_materi';

            //         downloadList.push(vid)
            //     });
            // }

            if (params.materis.length > 0) {
                params.materis.forEach(element => {
                    let vid = element;

                    downloadList.push(vid)
                });

            }

            this.setState({ downloadList: downloadList })
        }
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed()
            return true;
        });
    }

    onBackPressed = () => {
        this.props.navigation.goBack()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getDataMateri(id) {
        this.setState({ isFailed: false, isFetching: true, });

        try {
            let response = await getEventMateri(id);

            if (response.isSuccess === true) {
                this.setState({
                    downloadList: [...this.state.downloadList, ...response.data],
                    isFetching: false, isSuccess: true, isFailed: false, onRefresh: false,
                    emptyDataDownload: response.data != null && response.data.length > 0 ? true : false
                })
            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true, onRefresh: false });
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
            }
        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true, onRefresh: false });
            Toast.show({ text: 'error', position: 'top', duration: 3000 })
        }
    }


    handleLoadMore = () => {
        // if (this.state.emptyDataDownload === true) {
        //     return;
        // }
        // this.page = this.state.isFailed == true ? this.page : this.page + 1;
        // this.getDataDownload(this.page);
    }

    onRefreshBookmarks() {
        // this.page = 1
        // this.setState({onRefresh : true, emptyDataDownload: false, downloadList: []})
        // this.getDataDownload(this.page)
    }

    renderItem = (item, index) => {
        let iconType = item.type == ParamContent.PDF_MATERIS ? 'FontAwesome' : 'MaterialIcons';
        let iconName = item.type == ParamContent.PDF_MATERIS ? 'file-pdf-o' : 'play-circle-filled';
        let title = item.title != null ? item.title : '';

        return (
            <TouchableOpacity
                {...testID('list_materi_event')}
                accessibilityLabel={'list_materi_event'}

                activeOpacity={0.7} onPress={() => this.showDetail(item)}>
                <View style={styles.mainItemView}>
                    <View size={15} style={styles.leftItemView}>
                        <View style={styles.leftItemViewInside}>
                            <Icon name={iconName} type={iconType} style={styles.iconItem} />
                        </View>
                    </View>
                    <View size={85} style={styles.centerItemView}>
                        {/* <Text style={styles.textTitleItem}>Materi {index}</Text> */}
                        <Text style={styles.textTitleItem}>{title}</Text>
                    </View>
                    <View size={15} style={styles.rightItemView}>
                        <Icon
                            {...testID('list_materi_event')}
                            accessibilityLabel={'list_materi_event'}

                            name={'chevron-thin-right'} type='Entypo' style={styles.iconArrowRight} />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    showDetail(data) {
        let params = {
            id: data.id,
            attachment: data.filename,
            isCanDownload: true,
            category: data.type == 'pdf_materi' ? ParamContent.PDF_MATERIS : ParamContent.VIDEO_MATERIS
        }
        if (data.type == 'pdf_materi') {
            const showDetail = { type: "Navigate", routeName: 'PdfView', params: params }
            this.props.navigation.navigate(showDetail);
        } else if (data.type == 'video_materi') {
            const showDetail = { type: "Navigate", routeName: 'VideoView', params: params }
            this.props.navigation.navigate(showDetail);
        }
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetching == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true && (
            (this.page > 1))
        ) {
            return (
                <TouchableOpacity onPress={() => {
                    this.handleLoadMore()
                }}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItemLoader() {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true) {
            if (this.state.downloadList.length == 0) {
                return (
                    <Text style={{ textAlign: 'center' }}>Data not found</Text>
                )
            }

            return null;
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }


    render() {
        const nav = this.props.navigation;
        const DownloadDetail = { type: "Navigate", routeName: "DownloadDetail" }

        return (
            <Container>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow>
                        <Left style={{ flex: 0.5 }}>
                            <Button transparent onPress={() => this.onBackPressed()}
                                accessibilityLabel="button_back"
                            >

                                <Icon name='md-arrow-back' style={styles.toolbarIcon} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Title>Event Material</Title>
                        </Body>
                        <Right style={{ flex: 0.5 }} />
                    </Header>
                </SafeAreaView>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <FlatList
                        style={styles.flatlist}
                        data={this.state.downloadList}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        // onRefresh={() => this.onRefreshBookmarks()}
                        refreshing={this.state.onRefresh}
                        // renderItem={this._renderItem}
                        renderItem={({ item, index }) => this.renderItem(item, index)}
                        ListEmptyComponent={this._renderEmptyItem}
                        ListFooterComponent={this._renderItemFooter}
                        keyExtractor={(item, index) => index.toString()
                        } />
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    headerBody: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContent: {
    },
    flatlist: {
        padding: 10,
        backgroundColor: '#ffff'
    },
    mainItemView: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffff',
        borderBottomWidth: 2,
        borderBottomColor: '#F4F4F6',
        paddingVertical: 5
    },
    leftItemView: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    leftItemViewInside: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D31F55',
        padding: 10,
        borderRadius: 10
    },
    iconItem: {
        fontSize: 20,
        textAlign: 'center',
        color: '#F4F4F6'
    },
    centerItemView: {
        flex: 0.8,
        padding: 10,
        marginRight: 3,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    textTitleItem: {
        fontFamily: 'Nunito-Bold',
        color: '#454F63',
        fontSize: 16
    },
    textTimeItem: {
        color: '#959DAD',
        fontSize: 12
    },
    rightItemView: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    iconArrowRight: {
        fontSize: 20,
        textAlign: 'center',
        color: '#454F63'
    },
});

