import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {
  Platform,
  StatusBar,
  StyleSheet,
  Animated,
  View,
  Modal,
  Image,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  BackHandler,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
  Tabs,
  Tab,
  ScrollableTab,
  TabHeading,
  Toast,
} from "native-base";
import { Loader, CardItemEvent, MenuItemChecklist } from "./../../components";
import platform from "../../../theme/variables/d2dColor";
import { getApiFeeds } from "./../../libs/NetworkUtility";
import {
  STORAGE_TABLE_NAME,
  EnumFeedsCategory,
  testID,
} from "./../../libs/Common";
import Menu from "react-native-material-menu";
import Orientation from "react-native-orientation";
import { Fragment } from "react";

import { AdjustTracker, AdjustTrackerConfig } from "../../libs/AdjustTracker";
import { ArrowBackButton } from "../../../src/components/atoms";
import { getData, KEY_ASYNC_STORAGE } from "../../../src/utils/localStorage";

let from = {
  Search: "Search",
  Filter: "Filter",
};

export default class Event extends Component {
  PAGE_ALL = "all";
  PAGE_BOOKMARK = "bookmark";
  PAGE_RESERVE = "reserve";
  typeDataChanged = null;
  page = 1;
  pageB = 1;
  pageR = 1;
  uid = null;
  menu = null;
  monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  requestFilterMonth = "1,2,3,4,5,6,7,8,9,10,11,12";

  state = {
    animatedValue: new Animated.Value(0),
    events: [],
    bookmarks: [],
    reservation: [],
    isFetching: false,
    isFetchingAll: false,
    isFetchingBookmark: false,
    isFetchingReserve: false,
    isSuccess: false,
    isFailed: false,
    isEmptyDataAll: false,
    isEmptyDataBookmark: false,
    isEmptyDataReserve: false,
    filterMonthValue: [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
    ],
    backButtonAvailable: false,
  };

  constructor(props) {
    super(props);
    this.getDataCountryCode();
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    StatusBar.setHidden(false);
    AdjustTracker(AdjustTrackerConfig.Event_Event);
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.onBackPressed();
      return true;
    });
    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      (payload) => {
        this.getData();
      }
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed = () => {
    this.sendAdjust(from.BACK_BUTTON);
    this.props.navigation.goBack();
  };

  setMenuRef = (ref) => {
    this.menu = ref;
  };

  async getData() {
    this.page = 1;
    this.pageB = 1;
    this.pageR = 1;
    this.setState({ events: [], bookmarks: [], reservation: [] });

    this.getEvents(this.page);
    this.getBookmark(this.pageB);
    this.getReservation(this.pageR);
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.setState({ backButtonAvailable: true });
      } else {
        this.setState({ backButtonAvailable: false });
      }
    });
  };

  onRefreshAll() {
    this.page = 1;
    this.setState({ events: [], isFetchingAll: true, isEmptyDataAll: false });
    this.getEvents(this.page);
  }

  onRefreshBookmark() {
    this.pageB = 1;
    this.setState({
      bookmarks: [],
      isFetchingBookmark: true,
      isEmptyDataBookmark: false,
    });
    this.getBookmark(this.pageB);
  }

  onRefreshReservation() {
    this.pageR = 1;
    this.setState({
      reservation: [],
      isFetchingReserve: true,
      isEmptyDataReserve: false,
    });
    this.getReservation(this.pageR);
  }

  async getEvents(page) {
    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await getApiFeeds(
        page,
        null,
        false,
        false,
        false,
        true,
        false,
        false,
        false,
        this.requestFilterMonth,
        false,
        false
      );
      console.log('event response', response);

      if (response.isSuccess == true) {
        this.setState({
          events:
            page == 1
              ? [...response.data]
              : [...this.state.events, ...response.data],
        });
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingAll: false,
          isEmptyDataAll:
            response.data != null && response.data.length > 0 ? false : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: false,
          isFailed: true,
          isFetchingAll: false,
        });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        isFetchingAll: false,
      });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  async getBookmark(page) {
    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await getApiFeeds(
        page,
        null,
        false,
        false,
        false,
        true,
        true,
        false,
        false,
        this.requestFilterMonth,
        false,
        false
      );

      if (response.isSuccess == true) {
        this.setState({
          bookmarks:
            page == 1
              ? [...response.data]
              : [...this.state.bookmarks, ...response.data],
        });
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingBookmark: false,
          isEmptyDataBookmark:
            response.data != null && response.data.length > 0 ? false : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: false,
          isFailed: true,
          isFetchingBookmark: false,
        });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        isFetchingBookmark: false,
      });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  async getReservation(page) {
    this.setState({ isFailed: false, isFetching: true });

    try {
      let response = await getApiFeeds(
        page,
        null,
        false,
        false,
        false,
        true,
        false,
        false,
        true,
        this.requestFilterMonth,
        false,
        false
      );

      if (response.isSuccess == true) {
        this.setState({
          reservation:
            page == 1
              ? [...response.data]
              : [...this.state.reservation, ...response.data],
        });
        this.setState({
          isFetching: false,
          isSuccess: true,
          isFailed: false,
          isFetchingReserve: false,
          isEmptyDataReserve:
            response.data != null && response.data.length > 0 ? false : true,
        });
      } else {
        this.setState({
          isFetching: false,
          isSuccess: false,
          isFailed: true,
          isFetchingReserve: false,
        });
        Toast.show({ text: response.message, position: "top", duration: 3000 });
      }
    } catch (error) {
      this.setState({
        isFetching: false,
        isSuccess: false,
        isFailed: true,
        isFetchingReserve: false,
      });
      Toast.show({
        text: "Something went wrong!",
        position: "top",
        duration: 3000,
      });
    }
  }

  handleLoadMore = () => {
    if (this.state.isEmptyDataAll == true) {
      return;
    }

    this.page = this.state.isFailed == true ? this.page : this.page + 1;
    this.getEvents(this.page);
  };

  handleLoadMoreBookmark = () => {
    if (this.state.isEmptyDataBookmark == true) {
      return;
    }

    this.pageB = this.state.isFailed == true ? this.pageB : this.pageB + 1;
    this.getBookmark(this.pageB);
  };

  handleLoadMoreReserve = () => {
    if (this.state.isEmptyDataReserve == true) {
      return;
    }

    this.pageR = this.state.isFailed == true ? this.pageR : this.pageR + 1;
    this.getReservation(this.pageR);
  };

  _renderItem = ({ item }) => {
    return (
      <CardItemEvent
        navigation={this.props.navigation}
        data={item}
        activeType={this.PAGE_ALL}
        action={this.onChangeBookmarked}
      />
    );
  };

  _renderItemBookmark = ({ item }) => {
    return (
      <CardItemEvent
        navigation={this.props.navigation}
        data={item}
        activeType={this.PAGE_BOOKMARK}
        action={this.onChangeBookmarked}
      />
    );
  };

  _renderItemReserve = ({ item }) => {
    return (
      <CardItemEvent
        navigation={this.props.navigation}
        data={item}
        activeType={this.PAGE_RESERVE}
        action={this.onChangeBookmarked}
      />
    );
  };

  onChangeBookmarked = (type) => {
    this.typeDataChanged = type;
  };

  _renderItemFooter = (type) => (
    <View
      style={{
        height: this.state.isFetching == true ? 80 : 0,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {this._renderItemFooterLoader(type)}
    </View>
  );

  _renderItemFooterLoader(type) {
    if (
      this.state.isFailed == true &&
      ((this.page > 1 && type == this.PAGE_ALL) ||
        (this.pageB > 1 && type == this.PAGE_BOOKMARK) ||
        (this.pageR > 1 && type == this.PAGE_RESERVE))
    ) {
      return (
        <TouchableOpacity
          // {...testID('buttonBookmark')}
          accessibilityLabel="button_bookmark"
          onPress={() => {
            if (type == this.PAGE_BOOKMARK) this.handleLoadMoreBookmark();
            else if (type == this.PAGE_RESERVE) this.handleLoadMoreReserve();
            else this.handleLoadMore();
          }}
        >
          <Icon name="ios-sync" style={{ fontSize: 42 }} />
        </TouchableOpacity>
      );
    }

    return <Loader visible={this.state.isFetching} transparent />;
  }

  _renderEmptyItem = (type) => (
    <View style={styles.emptyItem}>{this._renderEmptyItemLoader(type)}</View>
  );

  _renderEmptyItemLoader(type) {
    if (this.state.isFetching == false && this.state.isFailed == true) {
      return (
        <TouchableOpacity
          style={{ justifyContent: "center", alignItems: "center" }}
          onPress={() => this.handleLoadMore()}
        >
          <Image
            source={require("./../../assets/images/noinet.png")}
            style={{ width: 72, height: 72 }}
          />
          <Text style={{ textAlign: "center" }}>
            Something went wrong,{" "}
            <Text style={{ color: platform.brandInfo }}>tap to reload</Text>
          </Text>
        </TouchableOpacity>
      );
    } else if (
      this.state.isFetching == false &&
      this.state.isSuccess == true &&
      ((type == this.PAGE_ALL && this.state.isEmptyDataAll == true) ||
        (type == this.PAGE_BOOKMARK &&
          this.state.isEmptyDataBookmark == true) ||
        (type == this.PAGE_RESERVE && this.state.isEmptyDataReserve == true))
    ) {
      return <Text style={{ textAlign: "center" }}>Data not found</Text>;
    }

    return <Text style={{ textAlign: "center" }}>Loading...</Text>;
  }

  autoRefresh(tab) {
    switch (tab.i) {
      case 0:
        AdjustTracker(AdjustTrackerConfig.Event_Event);
        break;
      case 1:
        AdjustTracker(AdjustTrackerConfig.Event_Bookmark);
        break;
      case 2:
        AdjustTracker(AdjustTrackerConfig.Event_Rsvp);
        break;
    }

    if (this.typeDataChanged != null) {
      if (this.typeDataChanged == this.PAGE_ALL) {
        this.onRefreshBookmark();
        this.onRefreshReservation();
      } else if (this.typeDataChanged == this.PAGE_BOOKMARK) {
        this.onRefreshAll();
        this.onRefreshReservation();
        this.onRefreshBookmark();
      } else if (this.typeDataChanged == this.PAGE_RESERVE) {
        this.onRefreshAll();
        this.onRefreshBookmark();
      }

      this.typeDataChanged == null;
    }
  }

  doFilterEvent(index, value) {
    let filter = this.state.filterMonthValue;
    filter[index] = value;
    this.setState({ filterMonthValue: filter });

    let tempReqFilter = [];
    this.state.filterMonthValue.map(function(d, i) {
      if (d == true) {
        tempReqFilter.push(i + 1);
      }
    });

    this.requestFilterMonth = tempReqFilter.join(",");
    console.log(this.requestFilterMonth);

    this.getData();
  }

  sendAdjust = (action) => {
    let token = "";
    switch (action) {
      case from.Filter:
        token = AdjustTrackerConfig.Event_Event_Filter;
        break;
      case from.Search:
        token = AdjustTrackerConfig.Event_Event_Search;
        break;
      default:
        break;
    }
    if (token != "") {
      AdjustTracker(token);
    }
  };

  render() {
    const nav = this.props.navigation;
    let translateY = this.state.animatedValue.interpolate({
      inputRange: [0, heightHeaderSpan - platform.toolbarHeight],
      outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
      extrapolate: "clamp",
    });
    let opacityContent = this.state.animatedValue.interpolate({
      inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 2],
      outputRange: [1, 0],
    });

    return (
      <Fragment>
        <SafeAreaView
          style={{
            flex: 0,
            zIndex: 1000,
            backgroundColor: platform.toolbarDefaultBg,
          }}
        />
        <Container>
          <Tabs
            renderTabBar={(props) => (
              <Animated.View
                style={{
                  transform: [{ translateY }],
                  top: heightHeaderSpan,
                  zIndex: 1,
                  width: "100%",
                }}
              >
                <ScrollableTab
                  {...props}
                  renderTab={(name, page, active, onPress, onLayout) => (
                    <TouchableOpacity
                      key={page}
                      onPress={() => {
                        onPress(page);
                      }}
                      onLayout={onLayout}
                      activeOpacity={0.99}
                    >
                      <Animated.View style={styles.tabHeading}>
                        <TabHeading
                          // {...testID('tabHeading')}
                          //accessibilityLabel="tab_heading"
                          style={{
                            width: platform.deviceWidth / 3,
                            height: platform.toolbarHeight,
                          }}
                          active={active}
                        >
                          <Text
                            style={{
                              fontSize: 16,
                              fontFamily: active
                                ? "Nunito-Bold"
                                : "Nunito-SemiBold",
                              color: active
                                ? platform.topTabBarActiveTextColor
                                : platform.topTabBarTextColor,
                              // fontSize: platform.tabFontSize
                            }}
                          >
                            {name}
                          </Text>
                        </TabHeading>
                      </Animated.View>
                    </TouchableOpacity>
                  )}
                  underlineStyle={{
                    backgroundColor: platform.topTabBarActiveBorderColor,
                  }}
                />
              </Animated.View>
            )}
            onChangeTab={(tab) => this.autoRefresh(tab)}
          >
            <Tab
              // {...testID('tabEvent')}
              accessibilityLabel="tab_event"
              heading="Event"
            >
              <View>
                <AnimatedFlatList
                  contentContainerStyle={{
                    paddingTop: heightHeaderSpan + 5,
                    paddingHorizontal: 10,
                  }}
                  scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                  onScroll={Animated.event(
                    [
                      {
                        nativeEvent: {
                          contentOffset: { y: this.state.animatedValue },
                        },
                      },
                    ],
                    { useNativeDriver: true } // <-- Add this
                  )}
                  data={this.state.events}
                  onEndReached={this.handleLoadMore}
                  onEndReachedThreshold={0.5}
                  onRefresh={() => this.onRefreshAll()}
                  refreshing={this.state.isFetchingAll}
                  renderItem={this._renderItem}
                  ListEmptyComponent={this._renderEmptyItem(this.PAGE_ALL)}
                  ListFooterComponent={this._renderItemFooter(this.PAGE_ALL)}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </Tab>
            <Tab
              // {...testID('inputBookmark')}
              accessibilityLabel="tab_bookmark"
              heading="Bookmark"
            >
              <View>
                <AnimatedFlatList
                  contentContainerStyle={{
                    paddingTop: heightHeaderSpan + 5,
                    paddingHorizontal: 10,
                  }}
                  scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                  onScroll={Animated.event(
                    [
                      {
                        nativeEvent: {
                          contentOffset: { y: this.state.animatedValue },
                        },
                      },
                    ],
                    { useNativeDriver: true } // <-- Add this
                  )}
                  data={this.state.bookmarks}
                  onEndReached={this.handleLoadMoreBookmark}
                  onEndReachedThreshold={0.5}
                  onRefresh={() => this.onRefreshBookmark()}
                  refreshing={this.state.isFetchingBookmark}
                  renderItem={this._renderItemBookmark}
                  ListEmptyComponent={this._renderEmptyItem(this.PAGE_BOOKMARK)}
                  ListFooterComponent={this._renderItemFooter(
                    this.PAGE_BOOKMARK
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </Tab>
            <Tab
              // {...testID('inputReservation')}
              accessibilityLabel="tab_joined"
              heading="Joined"
            >
              <View>
                <AnimatedFlatList
                  contentContainerStyle={{
                    paddingTop: heightHeaderSpan + 5,
                    paddingHorizontal: 10,
                    paddingBottom: 5,
                  }}
                  scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                  onScroll={Animated.event(
                    [
                      {
                        nativeEvent: {
                          contentOffset: { y: this.state.animatedValue },
                        },
                      },
                    ],
                    { useNativeDriver: true } // <-- Add this
                  )}
                  data={this.state.reservation}
                  onEndReached={this.handleLoadMoreReserve}
                  onEndReachedThreshold={0.5}
                  onRefresh={() => this.onRefreshReservation()}
                  refreshing={this.state.isFetchingReserve}
                  renderItem={this._renderItemReserve}
                  ListEmptyComponent={this._renderEmptyItem(this.PAGE_RESERVE)}
                  ListFooterComponent={this._renderItemFooter(
                    this.PAGE_RESERVE
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </Tab>
          </Tabs>

          <Animated.View
            style={[
              styles.animHeaderSpan,
              {
                transform: [{ translateY }],
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-start",
              },
            ]}
          >
            <Animated.Text
              style={{
                color: "#fff",
                opacity: opacityContent,
                fontFamily: "Nunito",
                fontSize: 16,
              }}
            >
              Share & get updates on new clinic cases and health journal info
            </Animated.Text>
          </Animated.View>

          <Animated.View style={styles.animHeader}>
            <Header noShadow hasTabs>
              <Body
                style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
              >
                {this.state.backButtonAvailable == true && (
                  <View style={{ marginLeft: -6 }}>
                    <ArrowBackButton onPress={() => this.onBackPressed()} />
                  </View>
                )}
                <Title style={styles.textTitle(this.state.backButtonAvailable)}>
                  Event
                </Title>
              </Body>
              <Right>
                <Button
                  // {...testID('buttonEventSearch')}
                  accessibilityLabel="button_search"
                  transparent
                  onPress={() =>
                    this.props.navigation.navigate("EventSearch") +
                    this.sendAdjust(from.Search)
                  }
                >
                  <Icon
                    type="EvilIcons"
                    name="search"
                    style={{ fontSize: 28 }}
                  />
                </Button>
                <Menu
                  scrollEventThrottle={1}
                  style={{ padding: 5, height: platform.deviceHeight / 1.6 }}
                  ref={this.setMenuRef}
                  button={
                    <Button
                      // {...testID('buttonFilter')}
                      accessibilityLabel="button_filter"
                      nopadding
                      transparent
                      onPress={() =>
                        this.menu.show() + this.sendAdjust(from.Filter)
                      }
                      style={{ paddingHorizontal: 10 }}
                    >
                      <Icon
                        type="MaterialCommunityIcons"
                        name="filter"
                        nopadding
                        style={{ color: "#FFFFFF", fontSize: 25 }}
                      />
                    </Button>
                  }
                >
                  <ScrollView>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[0]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(0, !this.state.filterMonthValue[0])
                      }
                    >
                      {this.monthNames[0]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[1]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(1, !this.state.filterMonthValue[1])
                      }
                    >
                      {this.monthNames[1]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[2]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(2, !this.state.filterMonthValue[2])
                      }
                    >
                      {this.monthNames[2]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[3]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(3, !this.state.filterMonthValue[3])
                      }
                    >
                      {this.monthNames[3]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[4]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(4, !this.state.filterMonthValue[4])
                      }
                    >
                      {this.monthNames[4]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[5]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(5, !this.state.filterMonthValue[5])
                      }
                    >
                      {this.monthNames[5]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[6]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(6, !this.state.filterMonthValue[6])
                      }
                    >
                      {this.monthNames[6]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[7]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(7, !this.state.filterMonthValue[7])
                      }
                    >
                      {this.monthNames[7]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[8]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(8, !this.state.filterMonthValue[8])
                      }
                    >
                      {this.monthNames[8]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[9]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(9, !this.state.filterMonthValue[9])
                      }
                    >
                      {this.monthNames[9]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[10]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(10, !this.state.filterMonthValue[10])
                      }
                    >
                      {this.monthNames[10]}
                    </MenuItemChecklist>
                    <MenuItemChecklist
                      isChecked={this.state.filterMonthValue[11]}
                      textStyle={{ fontFamily: "Nunito-Regular", fontSize: 13 }}
                      onPress={() =>
                        this.doFilterEvent(11, !this.state.filterMonthValue[11])
                      }
                    >
                      {this.monthNames[11]}
                    </MenuItemChecklist>
                  </ScrollView>
                </Menu>
              </Right>
            </Header>
          </Animated.View>
        </Container>
      </Fragment>
    );
  }
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const heightHeaderSpan = 150;
const styles = StyleSheet.create({
  animHeader: {
    position: "absolute",
    width: "100%",
    zIndex: 2,
  },
  animHeaderSpan: {
    position: "absolute",
    paddingTop: platform.toolbarHeight,
    backgroundColor: platform.toolbarDefaultBg,
    height: heightHeaderSpan,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: "flex-end",
    alignItems: "flex-start",
  },
  emptyItem: {
    justifyContent: "center",
    alignItems: "center",
    height: platform.deviceHeight / 2 - 80,
  },
  tabHeading: {
    flex: 1,
    height: 100,
    backgroundColor: platform.tabBgColor,
    justifyContent: "center",
    alignItems: "center",
  },
  textTitle: (backButtonAvailable) => ({
    fontSize: 24,
    fontFamily: "Nunito",
    fontWeight: "bold",

    marginLeft: backButtonAvailable == true ? 20 : null,
  }),
});
