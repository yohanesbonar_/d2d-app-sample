import React, { Component } from 'react'
import { BackHandler, Dimensions } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right } from 'native-base';
import { WebView } from 'react-native-webview';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import { STORAGE_TABLE_NAME, testID } from './../../libs/Common';
import Config from "react-native-config";
import Orientation from 'react-native-orientation';

export default class LaporanKesehatan extends Component {


    constructor(props) {
        super(props);

        this.state = {
            urlLinkLaporanKesehatan: '',
            uidPasien: this.props.navigation.state.params.uid,
            lastUpdated: this.props.navigation.state.params.updated,
            created: this.props.navigation.state.params.created,
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        }

        this.onLayout = this.onLayout.bind(this);

    }


    onLayout(e) {
        this.setState({
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        });
    }

    componentDidMount() {
        console.log('LaporanKesehatan this.props.navigation.state.params: ', this.props.navigation.state.params)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

        Orientation.lockToLandscape()
        this.getlinkLaporanKesehatan()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Orientation.lockToPortrait();
        this.props.navigation.goBack();
    }

    async getlinkLaporanKesehatan() {
        let created = this.state.created
        let uidPasien = this.state.uidPasien
        let lastUpdated = this.state.lastUpdated
        let uidDoctor = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);

        let url = Config.ENV == "production" ? 'https://api.temandiabetes.com/get-link-d2dQR/' :
            'https://staging-api.temandiabetes.com/get-link-d2dQR/';

        url += uidPasien + '/' + uidDoctor


        fetch(url.replace(" ", "T"))
            .then((response) => response.json())
            .then((data) => {
                console.log('url link webview: ', data.link)
                console.log('lastUpdate: ', lastUpdated)
                console.log('created: ', created)
                this.setState({
                    urlLinkLaporanKesehatan: data.link + '?pmm=true'
                })
            })
            .catch((error) => {
                console.error(error);
            });
    }


    render() {
        console.log('urlLinkLaporanKesehatan: ', this.state.urlLinkLaporanKesehatan)
        console.log('width: ', Dimensions.get('window').width)
        console.log('height: ', Dimensions.get('window').height)
        return (
            <Container style={{ backgroundColor: '#F3F3F3' }}>
                <Header noShadow hasTabs>
                    <Left style={{ flex: 0.5 }}>
                        <Button
                            {...testID('button_back')}
                            accessibilityLabel="button_back"
                            transparent onPress={() => this.onBackPressed()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Title>Laporan Kesehatan</Title>
                    </Body>
                    <Right style={{ flex: 0.5 }}>

                    </Right>
                </Header>

                <WebView
                    onLayout={this.onLayout}
                    startInLoadingState={true}
                    javaScriptEnabled={true}
                    onMessage={event => console.log('onMessage webView: ', event.nativeEvent.data)}
                    source={{ uri: this.state.urlLinkLaporanKesehatan }}
                    style={{ flex: 1, flexDirection: 'column', width: this.state.width, backgroundColor: '#F3F3F3' }} />

            </Container>
        )
    }

}