import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Platform, Dimensions, ScrollView, Image } from 'react-native';

import platform from '../../../theme/variables/d2dColor';
import {
    Content, Button, Text, Col, Item, Input, Label, Icon, Toast
} from 'native-base';
import { testID } from '../../libs/Common';
import HTML from 'react-native-render-html';
import QRCode from 'react-native-qrcode-svg';

export default class PopupQRCode extends Component {

    constructor(props) {
        super(props);

        this.state = {

        }

    }

    componentDidMount() {

    }

    render() {
        const { data } = this.props;
        return (
            <View
                style={{ justifyContent: 'center' }}
                centerContent={true}
            >

                <View style={{ marginHorizontal: 10, padding: 0, position: 'absolute', left: 0, right: 0 }}>

                    <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, borderRadius: 10, zIndex: 1, }}>
                        <Text style={{
                            textAlign: 'center',
                            marginTop: 65, fontSize: 18,
                            fontWeight: 'bold',
                            color: '#262626D6',
                            marginBottom: 5
                        }}>Pairing to Patient</Text>
                        <Text style={{
                            textAlign: 'center',
                            fontSize: 16,
                            color: '#262626D6'
                        }}>Gunakan QR Code ini untuk terhubung dengan saya</Text>
                        <View style={styles.viewBarcode}>
                            <QRCode
                                style={{
                                    textAlign: 'center'
                                }}
                                value={"PMM:" + data}
                                size={200}
                            />
                        </View>

                    </View>

                    <TouchableOpacity style={styles.popupClose} onPress={() => this.props.onPressClosePopup()}>
                        <Text style={{ color: '#FFFFFF', marginTop: -2, fontSize: 18 }}>x</Text>
                    </TouchableOpacity>


                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    ///style modal
    popupClose: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#78849E',
        width: 35,
        height: 35,
        borderRadius: 17.5,
        position: 'absolute',
        right: 5,
        top: 5,
        zIndex: 2,
    },

    viewBarcode: {
        // flex: 1,
        marginVertical: 20,
        paddingHorizontal: 10,
        paddingVertical: 25,
        backgroundColor: '#fff',
        justifyContent: 'center',
        borderRadius: 5,
        alignItems: 'center'
    }
})
