import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Image, TouchableOpacity, FlatList, SectionList } from 'react-native';
import { Container, Header, Row, Col, Thumbnail, Icon, Text, Item, Input } from 'native-base';
import _ from 'lodash';
import platform from '../../../theme/variables/d2dColor';
import { Loader } from './../../components'
import { getListPatients } from './../../libs/PMM';
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { testID } from '../../libs/Common'
import { SafeAreaView } from 'react-native';

export default class SearchPasien extends Component {

    page = 1;
    timer = null;
    state = {
        searchValue: '',

        patient: [],
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        isEmptyData: false,
        isFirstTime: true,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        AdjustTracker(AdjustTrackerConfig.Kelola_Search_Start)
        StatusBar.setHidden(false);
        this.getPatient(this.page)
        this.setState({
            isFirstTime: false
        })
    }

    doSearch = (inputText) => {
        clearTimeout(this.timer);
        this.setState({ patient: [], searchValue: inputText, isEmptyData: false });
        if (inputText.length >= 3) {
            this.page = 1;
            this.timer = setTimeout(() => {
                this.getPatient(this.page);
            }, 1000);
        }
    }

    async getPatient(page) {

        if (this.state.searchValue == '') {
            this.setState({ isFetching: false, isSuccess: true })
            return;
        }

        this.setState({ isFailed: false, isFetching: true });

        let response = await getListPatients(page, limit = 10, sortStatus = 'status ASC', this.state.searchValue, sort = 'updated DESC');

        try {
            if (response.isSuccess) {
                this.setState({
                    //data: [...this.state.data, response],
                    patient: [...this.state.patient, ...response.data.data],
                    isFetching: false, isSuccess: true, isFailed: false,
                    isEmptyData: response.data.data != null && response.data.data.length > 0 ? false : true,
                });

            } else {
                this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            }
        } catch (error) {
            this.setState({ isFetching: false, isSuccess: false, isFailed: true });
            Toast.show({ text: 'Something went wrong!', position: 'top', duration: 3000 })
        }

    }

    handleLoadMore = () => {

        if (this.state.isEmptyData == true) {
            return;
        }
        this.page = this.state.isFailed == true ? this.page : this.page + 1;
        this.getPatient(this.page);
    }

    renderItemPatient = ({ item }) => {
        let patient_other_info = item.patient_other_info != null ? JSON.parse(item.patient_other_info) : null

        return (

            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.containerListPasien}
                onPress={() => this.gotoPerkenalanFitur(item, true)}
            >
                <View >
                    <Row>
                        <Thumbnail style={{
                            alignSelf: 'center',
                            width: 40,
                            height: 40,
                            borderRadius: 20,
                            marginRight: 10
                        }} source={{ uri: item.patient_photo }} />

                        <Col style={{ alignSelf: 'center' }}>
                            <Text style={styles.pasienName}>{item.patient_name}</Text>
                            <Text style={styles.pasienType}>{
                                patient_other_info == null ? '-' : "tipe_diabetes"
                                    in patient_other_info ? patient_other_info.tipe_diabetes : '-'
                            }</Text>
                        </Col>
                        {item.status == "U" && (
                            <Image resizeMode="contain" source={require('./../../assets/images/unpaired.png')} style={{
                                width: 100, height: 20, alignSelf: 'center'
                            }} />
                        )}

                    </Row>

                </View>

            </TouchableOpacity>
        )
    }

    gotoPerkenalanFitur(item) {
        let data = item

        let navaPerkenalanFitur = {
            type: "Navigate",
            routeName: 'PerkenalanFitur',
            params: { data: data }
        };
        AdjustTracker(AdjustTrackerConfig.Kelola_Search_View_Pasien)
        this.props.navigation.navigate(navaPerkenalanFitur)
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetching == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true) {
            return (
                <TouchableOpacity onPress={() => this.handleLoadMore()}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderEmptyItem = () => (
        <View style={styles.emptyItem}>
            {this._renderEmptyItemLoader()}
        </View>
    )

    _renderEmptyItemLoader() {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMore()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFirstTime == true || (this.state.isFetching == false && this.state.isSuccess == true)) {
            let messageNotFound = "We are really sad we could not find anything";
            if (this.state.searchValue != '') {
                messageNotFound += ' for ' + this.state.searchValue;
            }
            return (
                <View style={styles.viewNotFound}>
                    <Icon name="ios-search" style={styles.iconNotFound} />
                    <Text style={styles.textNotFound}>{messageNotFound}</Text>
                </View>

            );
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }

    renderList() {
        return (
            <FlatList
                contentContainerStyle={{ paddingTop: 5, paddingBottom: 5, }}
                scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                data={this.state.patient}
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={0.01}
                renderItem={this.renderItemPatient}
                ListEmptyComponent={this._renderEmptyItem}
                ListFooterComponent={this._renderItemFooter}
                keyExtractor={(item, index) => index.toString()} />
        )
    }

    render() {
        const nav = this.props.navigation;
        const inlineStyleIconClose = { marginTop: 7 }

        return (
            <Container>
                <SafeAreaView style={{ backgroundColor: platform.toolbarDefaultBg }}>
                    <Header noShadow searchBar>
                        <Item nopadding  >
                            <Icon
                                name="ios-search"
                                style={{ color: '#D7DEE6' }} />
                            <Input
                                {...testID('input_search')}
                                accessibilityLabel="input_search"
                                placeholder="Ketik Nama Pasien"
                                onChangeText={(text) => this.doSearch(text)}
                                //  onTouchStart={() => AdjustTracker(AdjustTrackerConfig.Obat_Search)}
                                value={this.state.searchValue}
                                style={styles.searchField} />
                            <Icon
                                {...testID('button_clear_input')}
                                style={{ color: '#D7DEE6' }} name="ios-close-circle" onPress={() => this.doSearch('')} />
                        </Item>
                        <TouchableOpacity
                            {...testID('button_close')}
                            accessibilityLabel="button_close"
                            transparent style={{ marginRight: 5, marginLeft: 10, alignSelf: 'center' }} onPress={() => nav.goBack()}>
                            <Icon name='md-close' type="Ionicons"
                                style={[styles.iconClose]}
                            />
                        </TouchableOpacity>
                    </Header>
                </SafeAreaView>
                {/* <View style={styles.containerTabs}>
                    {this.renderButtonTab()}

                </View> */}
                {this.renderList()}
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    searchField: {
        fontSize: 14,
        lineHeight: 15,
    },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    },

    pasienName: {
        fontFamily: 'Nunito-Regular',
        fontSize: 16,
        color: '#454F63',

    },
    pasienType: {
        fontFamily: 'Nunito-Regular',
        fontSize: 12,
        color: '#959DAD',
    },
    containerListPasien: {
        backgroundColor: '#fff',
        marginHorizontal: 10,
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#EAF4FF'
    },

    buttonTab: {
        width: 100 / 3 + '%',
        borderRadius: 0,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        borderBottomWidth: 3,
        borderBottomColor: 'transparent'
    },
    textButtonTab: {
        fontFamily: 'Nunito-Bold',
        fontSize: 13,
        color: '#959DAD'
    },
    containerTabs: {
        flexDirection: "row",
        width: '100%'
    },
    iconClose: {
        fontSize: 28,
        color: '#fff',
    },
    // emptyItem: {
    //     backgroundColor: '#fff',
    //     borderBottomLeftRadius: 7,
    //     borderBottomRightRadius: 7,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     height: (platform.deviceHeight / 2) - 80
    // },
    viewNotFound: {
        marginVertical: 20,
        marginHorizontal: 10
    },
    iconNotFound: {
        fontSize: 80,
        textAlign: 'center'
    },
    textNotFound: {
        textAlign: 'center',
        fontSize: 20
    }
});
