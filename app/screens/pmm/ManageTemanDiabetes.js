import React, { Component } from 'react'
import { Platform, StatusBar, StyleSheet, Animated, View, Modal, Image, TouchableOpacity, FlatList, ImageBackground, BackHandler } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Tabs, Tab, ScrollableTab, TabHeading, Thumbnail, Badge, Col, Row, Toast } from 'native-base';
import platform from '../../../theme/variables/d2dColor';
import { getApiFeeds } from './../../libs/NetworkUtility';
import { testID, STORAGE_TABLE_NAME } from '../../libs/Common'
import { widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as lor, removeOrientationListener as rol } from 'react-native-responsive-screen';
import PopupQrCode from './PopupQrCode'
import ReactModal from 'react-native-modal'
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const heightHeaderSpan = 150;
const urlDataDummyPasient = 'http://www.mocky.io/v2/5e5f4a17310000b838afda2f'
const urlDataDummyNotifikasi = 'http://www.mocky.io/v2/5e6f2edc330000a11df07754'
import { Loader } from './../../components'
import { getListPatients, getPMMListNotification } from './../../libs/PMM';
import AsyncStorage from '@react-native-community/async-storage';
import Moment from 'moment';
import momentTimezone from 'moment-timezone';
import { getInitialName } from '../../libs/GetInitialName'
import firebase from 'react-native-firebase';
const gueloginAuth = firebase.app('guelogin');
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { SafeAreaView } from 'react-native';
import Orientation from "react-native-orientation";
import { Fragment } from 'react'
export default class ManageTemanDiabetes extends Component {
    page = 1;
    pageNotif = 1;
    uidDoctor;

    constructor(props) {
        super(props);
    }

    state = {
        animatedValue: new Animated.Value(0),
        listPasien: [],
        listNotifikasi: [],
        isShowPopupQR: false,
        isEmptyData: false,
        isFetching: false,
        isSuccess: false,
        isFailed: false,
        isEmptyDataNotif: false,
        isFetchingNotif: false,
        isSuccessNotif: false,
        isFailedNotif: false,
        unreadNotif: 0
    }

    componentDidMount() {
        AdjustTracker(AdjustTrackerConfig.Kelola_Start)

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        this.handlerPushNotif()
        Orientation.lockToPortrait();
        this.props.navigation.addListener(
            'didFocus',
            payload => {
                console.log('didFocus')
                this.onRefreshListPatient()
                this.onRefreshListNotif()
                this.getUidDoctor()
            }
        );

    }

    async handlerPushNotif() {
        console.log('handlerPushNotif')
        //case open apps
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('notificationListener ManageTemanDiabetes: ', notification)
            if (notification != null && notification.data != null) {
                if (notification.data.type == 'pmm_patient') {
                    this.onRefreshListNotif()
                    this.onRefreshListPatient()
                }
            }
        });
    }
    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        this.props.navigation.goBack();
    }

    async getUidDoctor() {
        this.uidDoctor = await AsyncStorage.getItem(STORAGE_TABLE_NAME.UID);
    }


    onRefreshListPatient() {
        this.page = 1;
        this.setState({ listPasien: [] })
        this.getDataListPasienPMM(this.page);
    }

    async getDataListPasienPMM(page, limit = 10, sortStatus = 'status ASC', keyword = '', sort = 'updated DESC') {

        this.setState({
            isFailed: false,
            isFetching: true,
        })

        try {
            let response = await getListPatients(page, limit, sortStatus, keyword, sort);
            console.log('getDataListPasienPMM response: ', response)

            if (response.isSuccess) {
                this.setState({
                    listPasien: [...this.state.listPasien, ...response.data.data],
                    isEmptyData: response.data.data != null && response.data.data.length > 0 ? false : true,
                    isFetching: false, isSuccess: true, isFailed: false,
                })
            } else {
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
                this.setState({
                    isFetching: false, isSuccess: false, isFailed: true
                })
            }
        } catch (error) {
            Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
            this.setState({
                isFetching: false, isSuccess: false, isFailed: true
            })
        }

    }


    onRefreshListNotif() {
        this.pageNotif = 1;
        this.setState({ listNotifikasi: [] })
        this.getDataPMMListNotification(this.pageNotif);
    }

    async getDataPMMListNotification(pageNotif, limit = 10, type = 'pmm_patient') {

        this.setState({
            isFailedNotif: false,
            isFetchingNotif: true
        })

        try {
            let response = await getPMMListNotification(pageNotif, limit, type);
            console.log('getDataPMMListNotification response: ', response)
            if (response.isSuccess) {
                this.setState({
                    listNotifikasi: [...this.state.listNotifikasi, ...response.data.data.docs],
                    isEmptyDataNotif: response.data.data.docs != null && response.data.data.docs.length > 0 ? false : true,
                    isFetchingNotif: false, isSuccessNotif: true, isFailedNotif: false,
                    unreadNotif: response.data.data.unread
                })
            } else {
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
                this.setState({
                    isFetchingNotif: false, isSuccessNotif: false, isFailedNotif: true
                })
            }
        } catch (error) {
            Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
            this.setState({
                isFetchingNotif: false, isSuccessNotif: false, isFailedNotif: true
            })
        }

    }

    gotoPerkenalanFitur(item, isFromListNotif) {
        if (isFromListNotif == true) {
            AdjustTracker(AdjustTrackerConfig.Kelola_Notifikasi_Klik)
        }
        else if (isFromListNotif == false) {
            AdjustTracker(AdjustTrackerConfig.Kelola_Pasien_View)
        }
        let data = item
        data.isFromListNotif = isFromListNotif

        let navaPerkenalanFitur = {
            type: "Navigate",
            routeName: 'PerkenalanFitur',
            params: { data: data }
        };
        console.log("gotoPerkenalanFitur data: ", data)
        this.props.navigation.navigate(navaPerkenalanFitur)
    }

    renderListPasien = ({ item }) => {
        let patient_other_info = item.patient_other_info != null ? JSON.parse(item.patient_other_info) : null
        const isImageExist = !!item.patient_photo && item.patient_photo !== ''

        return (
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.containerListPasien}
                onPress={() => this.gotoPerkenalanFitur(item, false)}
            >
                <View >
                    <Row>
                        {/* <Thumbnail style={{
                            alignSelf: 'center',
                            width: 40,
                            height: 40,
                            borderRadius: 20,
                            marginRight: 10,
                            backgroundColor: '#B5B5B5'
                        }} source={{ uri: item.patient_photo }} /> */}
                        {isImageExist &&
                            <Thumbnail style={{
                                alignSelf: 'center',
                                width: 40,
                                height: 40,
                                borderRadius: 20,
                                marginRight: 10,
                                backgroundColor: '#B5B5B5'
                            }} source={{ uri: item.patient_photo }} />
                        }
                        {!isImageExist &&
                            <View
                                style={[styles.initialStyle]}
                            >
                                <Text style={[styles.textStyle]}>{getInitialName(item.patient_name) || ''}</Text>
                            </View>
                        }

                        <Col style={{ alignSelf: 'center' }}>
                            <Text style={styles.pasienName}>{item.patient_name}</Text>
                            <Text style={styles.pasienType}>{
                                patient_other_info == null ? '-' : "tipe_diabetes"
                                    in patient_other_info ? patient_other_info.tipe_diabetes : '-'
                            }</Text>
                        </Col>
                        {item.status == "U" && (
                            <Image resizeMode="contain" source={require('./../../assets/images/unpaired.png')} style={{
                                width: 100, height: 20, alignSelf: 'center'
                            }} />
                        )}

                    </Row>

                </View>

            </TouchableOpacity>
        )
    }

    renderListNotifikasi = ({ item }) => {
        let data = typeof item.data == 'undefined' ? null : item.data != null ? JSON.parse(item.data) : null
        console.log('renderListNotifikasi data: ', data)
        let sourceImageIsConnect = data.status == "U" ? require('./../../assets/images/disconnect-pairing.png') : require('./../../assets/images/connect-pairing.png')

        return (
            <TouchableOpacity
                activeOpacity={0.8}

                onPress={() => this.gotoPerkenalanFitur(item, true)}
            >
                <View
                    style={[styles.containerListNotifikasi, {
                        backgroundColor: item.status == 'unread' ? '#F2F2F2' : 'white',
                        // opacity: item.status == 'unread' ? 0.32 : 1
                    }]}
                >
                    <Row>
                        <View style={{
                            height: 50,
                            width: 50,
                            backgroundColor: '#EBEBEB',
                            //opacity: item.status == 'read' ? 0.22 : 1,
                            borderRadius: 15,
                            alignSelf: 'center',
                            alignItems: 'center',
                        }} >
                            <Image resizeMode="contain" source={sourceImageIsConnect} style={{
                                alignSelf: 'center', height: 20, width: 30, flex: 1
                            }} />
                        </View>
                        <Col style={{ marginLeft: 20 }}>
                            <Text style={{ fontFamily: 'Nunito-Bold', color: '#454F63', fontSize: 16 }}>{item.title}</Text>
                            <Text style={{ fontFamily: 'Nunito-Regular', color: '#959DAD', fontSize: 12 }}>
                                {Moment(momentTimezone.tz(item.created, 'Asia/Jakarta')).fromNow()}
                            </Text>
                        </Col>
                    </Row>

                </View>

            </TouchableOpacity>
        )
    }

    closePopupQR = async () => {
        AdjustTracker(AdjustTrackerConfig.Kelola_ShowQR_Close)
        this.setState({
            isShowPopupQR: false,
        });

    }

    renderPopupQR() {
        return (
            <ReactModal
                hasBackdrop={true}
                avoidKeyboard={true}
                backdropOpacity={0.8}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={600}
                animationOutTiming={600}
                backdropTransitionInTiming={600}
                backdropTransitionOutTiming={600}
                isVisible={this.state.isShowPopupQR}
            >
                <PopupQrCode
                    onPressClosePopup={this.closePopupQR}
                    data={this.uidDoctor}
                />
            </ReactModal >
        )
    }

    _renderItemFooter = () => (
        <View style={{ height: this.state.isFetching == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemFooterLoader()}
        </View>
    )

    _renderItemFooterNotif = () => (
        <View style={{ height: this.state.isFetchingNotif == true ? 80 : 0, justifyContent: 'center', alignItems: 'center' }}>
            {this._renderItemNotifFooterLoader()}
        </View>
    )

    _renderItemFooterLoader() {
        if (this.state.isFailed == true) {
            return (
                <TouchableOpacity onPress={() => this.handleLoadMoreListPasient()}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetching} transparent />);
    }

    _renderItemNotifFooterLoader() {
        if (this.state.isFailedNotif == true) {
            return (
                <TouchableOpacity onPress={() => this.handleLoadMoreListNotif()}>
                    <Icon name='ios-sync' style={{ fontSize: 42 }} />
                </TouchableOpacity>
            )
        }

        return (<Loader visible={this.state.isFetchingNotif} transparent />);
    }
    renderEmptyItemPatient = () => {
        if (this.state.isFetching == false && this.state.isFailed == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMoreListPasient()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetching == false && this.state.isSuccess == true &&
            this.state.isEmptyData == true) {
            return (
                <View style={{ paddingVertical: 40, justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                    <Image resizeMode='contain' source={require('./../../assets/images/ic_empty_data.png')} style={{ width: 72, height: 72 }} />

                    <Text style={styles.textEmtpyData}>Saat ini belum ada pasien dalam Program Kelola</Text>

                </View>
            )
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }

    renderEmptyItemNotif = () => {
        if (this.state.isFetchingNotif == false && this.state.isFailedNotif == true) {
            return (
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.handleLoadMoreListNotif()}>
                    <Image source={require('./../../assets/images/noinet.png')} style={{ width: 72, height: 72 }} />
                    <Text style={{ textAlign: 'center' }}>Something went wrong, <Text style={{ color: platform.brandInfo }}>tap to reload</Text></Text>
                </TouchableOpacity>
            )
        } else if (this.state.isFetchingNotif == false && this.state.isSuccessNotif == true &&
            this.state.isEmptyDataNotif == true) {
            return (
                <View style={{ paddingVertical: 40, justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                    <Image resizeMode='contain' source={require('./../../assets/images/ic_empty_data.png')} style={{ width: 72, height: 72 }} />
                    <Text style={styles.textEmtpyData}>Notifikasi untuk Program Kelola belum tersedia</Text>


                </View>)
        }

        return (<Text style={{ textAlign: 'center' }}>Loading...</Text>);
    }

    handleLoadMoreListPasient = () => {
        if (this.state.isEmptyData == true) {
            return;
        }

        this.page = this.state.isFailed == true ? this.page : this.page + 1;

        this.getDataListPasienPMM(this.page);
    }

    handleLoadMoreListNotif = () => {
        if (this.state.isEmptyDataNotif == true) {
            return;
        }

        this.pageNotif = this.state.isFailedNotif == true ? this.pageNotif : this.pageNotif + 1;

        this.getDataPMMListNotification(this.pageNotif);
    }

    renderBadgeNotif() {
        return (
            this.state.unreadNotif != 0 ?
                <Badge style={styles.badgeNotif}>
                    <Text style={styles.numberNotif}>{this.state.unreadNotif}</Text>
                </Badge>
                : null
        )
    }

    gotoSearchPatient() {
        AdjustTracker(AdjustTrackerConfig.Kelola_Search)
        this.props.navigation.navigate('SearchPasien')
    }

    showQR() {
        AdjustTracker(AdjustTrackerConfig.Kelola_ShowQR)
        this.setState({
            isShowPopupQR: true
        })
    }

    pressTab(page) {
        console.log('onPressTab page: ', page)
        page == 0 ? AdjustTracker(AdjustTrackerConfig.Kelola_Pasien) : AdjustTracker(AdjustTrackerConfig.Kelola_Notifikasi)
    }

    render() {
        const nav = this.props.navigation;
        let translateY = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight)],
            outputRange: [0, -(heightHeaderSpan - platform.toolbarHeight)],
            extrapolate: 'clamp',
        })
        let opacityContent = this.state.animatedValue.interpolate({
            inputRange: [0, (heightHeaderSpan - platform.toolbarHeight) / 2],
            outputRange: [1, 0],
        });

        return (
            <Fragment >
                <SafeAreaView style={{ zIndex: 1000, backgroundColor: platform.toolbarDefaultBg }} />
                <Container style={{ backgroundColor: '#F3F3F3' }}>

                    <Tabs renderTabBar={(props) =>
                        <Animated.View
                            style={{
                                transform: [{ translateY }], top: heightHeaderSpan,
                                zIndex: 1,
                                width: "100%"
                            }}>
                            <ScrollableTab style={{}} {...props}
                                renderTab={(name, page, active, onPress, onLayout) => (
                                    <TouchableOpacity key={page}
                                        onPress={() => { onPress(page) + this.pressTab(page) }}
                                        onLayout={onLayout}
                                        activeOpacity={0.99}>
                                        <Animated.View style={styles.tabHeading}>
                                            <TabHeading

                                                style={{ width: (platform.deviceWidth / 2), height: platform.toolbarHeight }}
                                                active={active}>
                                                <Text style={{
                                                    fontFamily: active ? 'Nunito-Bold' : 'Nunito-Regular',
                                                    color: active ? platform.topTabBarActiveTextColor : platform.topTabBarTextColor,
                                                    fontSize: platform.tabFontSize
                                                }}>
                                                    {name}
                                                </Text>
                                                {name == "NOTIFIKASI" ? this.renderBadgeNotif() : null}
                                            </TabHeading>
                                        </Animated.View>
                                    </TouchableOpacity>
                                )}
                                underlineStyle={{ backgroundColor: platform.topTabBarActiveBorderColor }} />
                        </Animated.View>
                    }
                        onChangeTab={(tab) =>
                            null
                            // this.autoRefresh(tab)
                        }
                    >

                        <Tab
                            // {...testID('tabEvent')} 
                            accessibilityLabel="tab_pasien"
                            heading="PASIEN">
                            <View >

                                <AnimatedFlatList
                                    //  contentContainerStyle={{ backgroundColor: 'yellow', paddingTop: 0 + 5, paddingHorizontal: 10 }}
                                    contentContainerStyle={{ marginTop: 10, paddingBottom: 0, backgroundColor: '#F3F3F3', paddingTop: heightHeaderSpan + 5, paddingHorizontal: 5, }}
                                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                                    onScroll={Animated.event(
                                        [{ nativeEvent: { contentOffset: { y: this.state.animatedValue } } }],
                                        { useNativeDriver: true } // <-- Add this
                                    )}
                                    data={this.state.listPasien}
                                    onEndReachedThreshold={0.5}
                                    renderItem={this.renderListPasien}
                                    ListFooterComponent={this._renderItemFooter}
                                    onRefresh={() => this.onRefreshListPatient()}
                                    refreshing={this.state.isFetching}
                                    onEndReached={this.handleLoadMoreListPasient}
                                    ListEmptyComponent={this.renderEmptyItemPatient}
                                    keyExtractor={(item, index) => index.toString()} />

                            </View>

                        </Tab>

                        <Tab
                            // {...testID('inputBookmark')}
                            accessibilityLabel="tab_notifikasi"
                            heading="NOTIFIKASI">

                            <View>
                                <AnimatedFlatList
                                    //  contentContainerStyle={{ backgroundColor: 'yellow', paddingTop: 0 + 5, paddingHorizontal: 10 }}
                                    contentContainerStyle={{ marginTop: 10, paddingBottom: 0, backgroundColor: '#F3F3F3', paddingTop: heightHeaderSpan + 5, paddingHorizontal: 5, }}
                                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                                    onScroll={Animated.event(
                                        [{ nativeEvent: { contentOffset: { y: this.state.animatedValue } } }],
                                        { useNativeDriver: true } // <-- Add this
                                    )}
                                    data={this.state.listNotifikasi}
                                    onEndReachedThreshold={0.5}
                                    renderItem={this.renderListNotifikasi}
                                    renderEmptyItem={this.renderEmptyItemNotif}
                                    ListFooterComponent={this._renderItemFooterNotif}
                                    onRefresh={() => this.onRefreshListNotif()}
                                    refreshing={this.state.isFetchingNotif}
                                    onEndReached={this.handleLoadMoreListNotif}
                                    ListEmptyComponent={this.renderEmptyItemNotif}
                                    keyExtractor={(item, index) => index.toString()} />
                            </View>
                        </Tab>


                    </Tabs>


                    <Animated.View style={[styles.animHeaderSpan, { transform: [{ translateY }], height: heightHeaderSpan }]}>
                        {/* <Animated.Text style={{ color: '#fff', opacity: opacityContent }}>Share & get updates on new clinic cases and health journal info</Animated.Text> */}
                        <Animated.View style={[styles.wrapperHeader, { opacity: opacityContent }]}>

                            <TouchableOpacity
                                {...testID('button_search_manage_td')}
                                onPress={() => this.gotoSearchPatient()}
                                activeOpacity={0.8}
                                style={{ flexDirection: 'row', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', paddingHorizontal: 7, paddingVertical: 10, marginVertical: 15 }}>
                                <Icon name='ios-search' style={styles.iconSearch} />
                                <Text style={{ flex: 0.9, }}>
                                    <Text style={{ color: '#B5B5B5', fontSize: 16, fontFamily: 'Nunito-SemiBold' }}>provided by</Text>
                                    <Text style={{ color: '#B5B5B5', fontSize: 21, fontFamily: 'zAristaLight' }}> Teman Diabetes</Text>

                                </Text>

                                <Icon name='ios-close-circle' style={styles.iconClose} />
                            </TouchableOpacity>
                        </Animated.View>
                    </Animated.View>


                    <Animated.View style={styles.animHeader}>
                        <Header noShadow hasTabs>
                            <Left style={{ flex: 0.25 }}>
                                <Button
                                    {...testID('button_back')}
                                    accessibilityLabel="button_back"
                                    transparent onPress={() => this.onBackPressed()}>
                                    <Icon name='md-arrow-back' />
                                </Button>
                            </Left>
                            <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Title>Kelola Teman Diabetes</Title>
                            </Body>
                            <Right style={{ flex: 0.25 }}>
                            </Right>
                        </Header>
                    </Animated.View>


                    <Button rounded
                        {...testID('button_show_qr')}
                        style={styles.buttonQRCode}
                        onPress={() => this.showQR()}>
                        <Icon type='MaterialCommunityIcons' name='qrcode' />
                        <Text style={{ marginLeft: -20, color: 'white', fontFamily: 'Nunito-Bold' }}>Show QR</Text>
                    </Button>

                    {this.renderPopupQR()}
                </Container>
            </Fragment>
        )
    }
}
const styles = StyleSheet.create({
    animHeader: {
        position: "absolute",
        width: "100%",
        zIndex: 2,
    },
    animHeaderSpan: {
        position: 'absolute',
        paddingTop: platform.toolbarHeight,
        backgroundColor: platform.toolbarDefaultBg,
        height: heightHeaderSpan,
        left: 0,
        right: 0,
        zIndex: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },
    emptyItem: {
        justifyContent: 'center',
        alignItems: 'center',
        height: (platform.deviceHeight / 2) - 80
    },
    tabHeading: {
        flex: 1,
        height: 100,
        backgroundColor: platform.tabBgColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconSearch: {
        fontSize: 20,
        marginLeft: -7,
        marginRight: 10,
        color: '#D7DEE6'
    },

    iconClose: {
        fontSize: 20,
        marginLeft: 10,
        marginRight: -7,
        color: '#D7DEE6'
    },

    wrapperHeader: {
        position: 'absolute',
        left: 0,
        right: 0,
        paddingHorizontal: 10,
        paddingVertical: 10,
    },

    pasienName: {
        fontFamily: 'Nunito-Regular',
        fontSize: 16,
        color: '#454F63',

    },
    pasienType: {
        fontFamily: 'Nunito-Regular',
        fontSize: 12,
        color: '#959DAD',
    },
    containerListPasien: {
        backgroundColor: '#fff',
        marginHorizontal: 10,
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#78849E14'
    },
    containerListNotifikasi: {
        // marginHorizontal: 10,
        padding: 10,
        borderBottomWidth: 1.5,
        borderBottomColor: '#EBEBEB'
    },
    buttonQRCode: {
        position: "absolute",
        zIndex: 5,
        bottom: 20,
        backgroundColor: platform.toolbarDefaultBg,
        alignSelf: 'center',
        height: 40,
        paddingHorizontal: 20,
    },
    badgeNotif: {
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#CB1D50',

    },
    numberNotif: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 13,
        fontWeight: 'bold'
    },
    initialStyle: {
        alignSelf: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
        marginRight: 10,
        backgroundColor: '#B5B5B5',
        justifyContent: 'center'
    },
    textStyle: {
        fontFamily: 'Roboto-Regular',
        color: '#424242',
        backgroundColor: 'transparent',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    textEmtpyData: {
        fontFamily: 'Nunito-Regular',
        color: '#707A89',
        fontSize: 14,
        textAlign: 'center',
        marginTop: 20
    }
});
