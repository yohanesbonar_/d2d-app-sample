import React, { Component } from 'react'
import { BackHandler, ImageBackground, StyleSheet, Image, Modal } from 'react-native';
import { Container, Header, Title, Button, Icon, Text, Left, Body, Right, Thumbnail, Card, Label, Item, Toast, CardItem, Col, Row, View } from 'native-base';
import { testID } from '../../libs/Common'
import platform from '../../../theme/variables/d2dColor';
import { getDetailDataPatient, readNotification } from './../../libs/PMM';
import { Loader } from './../../components'
import moment from 'moment-timezone';
import { getInitialName } from '../../libs/GetInitialName'
import { AdjustTracker, AdjustTrackerConfig } from '../../libs/AdjustTracker';
import { SafeAreaView } from 'react-native';

export default class PerkenalanFitur extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataPatient: null,
            statusPairing: null
        }
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

        this.getParams()
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        this.props.navigation.goBack();
    }

    getParams = () => {
        let { params } = this.props.navigation.state;
        console.log('params: ', params)
        if (params != null) {

            let patient_uid = typeof params.data.patient_uid != 'undefined' ? params.data.patient_uid : typeof params.data.data != 'undefined' ?
                JSON.parse(params.data.data).uid : params.data.uid
            //params.data.uid

            console.log('getParams patient_uid', patient_uid)

            this.getDataDetailPasienPMM(patient_uid)


            // if (params.data.isFromListNotif == false || params.isFromPushNotification == true) {
            //     console.log('set status pairing: ', params.data.status)
            //     this.setState({
            //         statusPairing: params.data.status
            //     })
            // }
            // else 
            if (params.data.isFromListNotif == true) {
                if (params.data.status == "unread") {
                    this.readNotificationPMM(params.data)
                }

                //let data = JSON.parse(params.data.data)
                // this.setState({
                //     statusPairing: data.status
                // })
            }
        }
    }

    async readNotificationPMM(data) {
        console.log('readNotificationPMM params: ', data)

        try {
            let response = await readNotification(data);
            console.log('readNotificationPMM response: ', response)

        } catch (error) {
            console.log('readNotificationPMM catch error: ', error)
        }

    }

    gotoLaporanKesehatan() {
        AdjustTracker(AdjustTrackerConfig.Kelola_Profile_Report)
        let data = this.state.dataPatient
        let navLaporanKesehatan = {
            type: "Navigate",
            routeName: 'LaporanKesehatan',
            params: data
        };
        this.props.navigation.navigate(navLaporanKesehatan)
    }

    async getDataDetailPasienPMM(patient_uid) {

        this.setState({
            showLoader: true
        })

        try {
            let response = await getDetailDataPatient(patient_uid);
            console.log('getDataDetailPasienPMM response: ', response)
            if (response.isSuccess) {
                this.setState({
                    dataPatient: response.data.data,
                    statusPairing: response.data.data.status,
                    showLoader: false,
                })

                if (response.data.data.length == 0) {
                    Toast.show({ text: response.data.message, position: 'top', duration: 3000 })
                }

            } else {
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
                this.setState({
                    showLoader: false,
                })
            }
        } catch (error) {
            Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
            this.setState({
                showLoader: false,
            })
        }

    }

    renderStatusPairing() {
        return (

            <View style={{
                flex: 1,
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 20,
                marginBottom: 10
            }}>

                <Col>

                    <Image source={require('./../../assets/images/warning.png')} style={{ width: 60, height: 60, alignSelf: 'center' }} />
                    <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 18, color: '#11243D', textAlign: 'center' }}>Data Terbaru Tidak Tersedia</Text>
                    <Text style={{ fontFamily: 'Nunito-Regular', color: '#11243D', textAlign: 'center' }}>Maaf, anda sudah terputus dengan pasien ini dan tidak lagi terdaftar dalam list anda.</Text>

                </Col>
            </View>
        )
    }

    render() {

        let nav = this.props.navigation;
        let { params: { data } } = nav.state;
        let dataPatient = this.state.dataPatient != null ? this.state.dataPatient : data;
        console.log('render dataPatient: ', dataPatient)

        let dataOtherInfo = typeof dataPatient.other_info != 'undefined' ? dataPatient.other_info : dataPatient.patient_other_info

        let other_info = typeof dataOtherInfo == 'undefined' ? null : dataOtherInfo != null ? JSON.parse(dataOtherInfo) : null
        let firstname = typeof dataPatient.firstname == 'undefined' ? '' : dataPatient.firstname
        let lastname = typeof dataPatient.lastname == 'undefined' ? '' : dataPatient.lastname
        const isImageExist = !!dataPatient.photo && dataPatient.photo !== ''
        console.log('isImageExist: ', isImageExist)

        return (

            <Container style={{ backgroundColor: '#F3F3F3' }}>

                <ImageBackground source={require('./../../assets/images/bg-gradient-red.png')} style={{ width: platform.deviceWidth, height: 270 }} >
                    <SafeAreaView style={{ backgroundColor: 'transparent' }} >
                        <Header androidStatusBarColor={"#D64D75"} noShadow style={{ backgroundColor: 'transparent' }} >
                            <Left style={{ flex: 0.25 }}>
                                <Button
                                    {...testID('button_back')}
                                    accessibilityLabel="button_back"
                                    transparent onPress={() => this.onBackPressed()}>
                                    <Icon name='md-arrow-back' />
                                </Button>
                            </Left>
                            <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Title>Kelola Teman Diabetes</Title>
                            </Body>
                            <Right style={{ flex: 0.25 }}>
                            </Right>
                        </Header>
                    </SafeAreaView>


                    <View style={{ alignSelf: 'center', alignItems: 'center' }}>
                        {/* <Avatar avatarSize="Medium" userName={`${firstname} ${lastname}`} imageSource={dataPatient.photo} /> */}
                        {/* <Thumbnail style={{
                            width: 80,
                            height: 80,
                            borderRadius: 80 / 2,
                            overflow: "hidden",
                            borderWidth: 3,
                            borderColor: "white",
                            backgroundColor: '#B5B5B5'
                        }} source={{ uri: dataPatient.photo }}

                        /> */}
                        {isImageExist &&
                            <Thumbnail style={{
                                width: 80,
                                height: 80,
                                borderRadius: 80 / 2,
                                overflow: "hidden",
                                borderWidth: 3,
                                borderColor: "white",
                                backgroundColor: '#B5B5B5'
                            }} source={{ uri: dataPatient.photo }}
                            />
                        }
                        {!isImageExist &&
                            <View
                                style={[styles.initialStyle]}
                            >
                                <Text style={[styles.textStyle]}>{getInitialName(dataPatient.display_name) || ''}</Text>
                            </View>
                        }

                        <Text style={{ marginTop: 10, fontFamily: 'Nunito-Bold', fontSize: 20, color: 'white' }}>{dataPatient.display_name}</Text>
                        <Text style={{ marginTop: 10, fontFamily: 'Nunito-Regular', fontSize: 14, color: '#FFFF00' }}>{
                            other_info == null ? '-' : "tipe_diabetes"
                                in other_info ? other_info.tipe_diabetes : '-'
                        }</Text>
                    </View>


                </ImageBackground>


                <View style={styles.containerCard}>
                    <Card style={styles.card}>
                        <Row style={{
                            flex: 1, justifyContent: 'space-between',
                        }}>
                            <CardItem style={{
                                flex: 1,
                            }}>
                                <Col style={{}}>
                                    <Text style={styles.textHeaderInCard}>BB</Text>
                                    <Text style={styles.textInCard}>{
                                        other_info == null ? '-' : "weight"
                                            in other_info ? other_info.weight : '-'
                                    }</Text>
                                </Col>

                            </CardItem>
                            <CardItem style={{
                                flex: 1,
                                borderLeftWidth: 1,
                                borderLeftColor: '#F4F4F6',
                                borderRightWidth: 1,
                                borderRightColor: '#F4F4F6'
                            }}>
                                <Col>
                                    <Text style={styles.textHeaderInCard}>Usia</Text>
                                    <Text style={styles.textInCard} >{dataPatient.birthday != null ? moment().diff(dataPatient.birthday, 'years', false) : '-'}</Text>
                                </Col>

                            </CardItem>

                            <CardItem style={{
                                flex: 1,
                            }}>
                                <Col>
                                    <Text style={styles.textHeaderInCard}>Jenis Kelamin</Text>
                                    <Text style={styles.textInCard} >{dataPatient.gender != null ? dataPatient.gender : '-'}</Text>
                                </Col>
                            </CardItem>

                        </Row>


                        <Row style={{
                            flex: 1,
                            justifyContent: 'space-between',
                            marginTop: 20
                        }}>
                            <CardItem style={{
                                flex: 1,
                            }}>
                                <Col>
                                    <Text style={styles.textHeaderInCard}>HbA1c</Text>
                                    <Text style={styles.textInCard}>{
                                        other_info == null ? '-' : "hba1c"
                                            in other_info ? other_info.hba1c : '-'
                                    }</Text>
                                </Col>

                            </CardItem>
                            <CardItem style={{
                                flex: 1,
                                borderLeftWidth: 1,
                                borderLeftColor: '#F4F4F6',
                                borderRightWidth: 1,
                                borderRightColor: '#F4F4F6'
                            }}>
                                <Col>
                                    <Text style={styles.textHeaderInCard}>Gula Darah</Text>
                                    <Text style={styles.textInCard} >{
                                        other_info == null ? '-' : "blood_glucose"
                                            in other_info ? other_info.blood_glucose : '-'
                                    }</Text>
                                </Col>

                            </CardItem>

                            <CardItem style={{
                                flex: 1,
                            }}>
                                <Col>
                                    <Text style={styles.textHeaderInCard}>Tensi</Text>
                                    <Text style={styles.textInCard} >{
                                        other_info == null ? '-' : "blood_pressure"
                                            in other_info ? other_info.blood_pressure : '-'
                                    }</Text>
                                </Col>
                            </CardItem>

                        </Row>
                    </Card>

                    {this.state.statusPairing == "U"
                        ? this.renderStatusPairing() : null}
                </View>

                <Button
                    // {...testID('buttonSave')}i
                    accessibilityLabel="button_save"
                    style={styles.btnSave} onPress={() => this.gotoLaporanKesehatan()} success block>
                    <Text style={{ fontFamily: 'Nunito-Bold' }}>LAPORAN KESEHATAN</Text>
                </Button>

                <Modal animationType="slide" transparent={true} visible={this.state.showLoader} onRequestClose={() => console.log('loader closed')}>
                    <Loader visible={this.state.showLoader} />
                </Modal>

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    containerCard: {
        position: 'absolute',
        flex: 1,
        top: 230,
        left: 0,
        right: 0,
        paddingHorizontal: 16,
        justifyContent: 'space-between',
        zIndex: 1
    },
    card: {
        flex: 1,
        justifyContent: 'space-between',
        marginVertical: 20,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    textHeaderInCard: {
        fontSize: 15,
        fontFamily: 'Nunito-Black',
        fontWeight: 'bold',
        color: '#454F63',
        alignSelf: 'center',
        textAlign: 'center'
    },
    textInCard: {
        fontSize: 11,
        fontFamily: 'Nunito-Bold',
        color: '#78849E',
        alignSelf: 'center'

    },
    btnSave: {
        position: 'absolute',
        flex: 1,
        left: 24,
        right: 24,
        bottom: 35,
        borderRadius: 10,
        height: 55,
        zIndex: 0
    },
    initialStyle: {
        alignSelf: 'center',
        width: 80,
        height: 80,
        borderRadius: 80 / 2,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "white",
        backgroundColor: '#B5B5B5',
        justifyContent: 'center'
    },
    textStyle: {
        fontFamily: 'Roboto-Regular',
        color: '#424242',
        backgroundColor: 'transparent',
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center'
    }
})