import React from "react";
import { createBottomTabNavigator, BottomTabBar } from "react-navigation-tabs";

import { createStackNavigator, createAppContainer } from "react-navigation";
import { FooterMenu } from "./components";
import { Platform } from "react-native";
import {
  AtoZ,
  DetailAtoZ,
  SearchAtoZ,
  Splash,
  Onboarding,
  Login,
  Subscribtion,
  Register,
  RegisterHello,
  RegisterSuccess,
  RegisterVerification,
  RegisterSpecialist,
  RegisterSubscribtions,
  RegisterHomeLocation,
  RegisterPracticeLocation,
  InputPracticeLocations,
  SelectSpecialist,
  SelectSubSpecialist,
  ForgotPasswordConfirmation,
  ForgotPassword,
  Feeds,
  ScanScreen,
  Event,
  EventDetail,
  EventSearch,
  EventReservation,
  Cme,
  CmeDetail,
  CmeHistory,
  CmeQuiz,
  CmeCertificate,
  CmeKursus,
  CmeMedicinus,
  Learning,
  LearningSearch,
  LearningSpecialist,
  LearningSpecialistSearch,
  LearningJournalDetail,
  LearningRequestJournal,
  Profile,
  ProfileDownload,
  ProfileBookmark,
  SearchCity,
  PdfView,
  VideoView,
  GeneralWebview,
  GeneralPdfView,
  Webinar,
  WebinarSearch,
  WebinarVideo,
  Chat,
  Playground,
  EventMateris,
  WebviewGrafik,
  ManageTemanDiabetes,
  LaporanKesehatan,
  PerkenalanFitur,
  SearchPasien,
  WebinarFilter,
  TestResult,
  FinishPostTest,
  Quisioner,
  SetNewPassword,
  WebinarYoutube360,
  ChooseCountry,
  SelectPayment,
  WebinarTNC,
  RegisterMedicalID,
} from "./screens";
import {
  Home,
  Feed,
  AlbumP2KB,
  SearchKonten,
  ProfileNew,
  Notification,
  SearchAllByKonten,
  Biodata,
  FilterSearchKonten,
  ChangeProfile,
  CalendarWebinar,
  Channel,
  ChannelDetail,
  Job,
  ChannelKonferensi,
  JobDetail,
  JobTulisArtikel,
  JobReviewArtikel,
  ChannelSeeAll,
  ComingSoon,
  JobFilter,
  JobListSpecialization,
  ChangeDetailProfile,
  Forum,
  ForumDetail,
  ReplyComment,
  ContactUs,
  NonSpecialist,
  BannerProductDetail,
  ConferenceRoom,
  WebviewPage,
  Booth,
  BoothDetail,
  WebinarDetail,
  FeedsNew,
} from "./../src/screens";
import { SafeAreaView } from "react-native";
import { BottomNavigator } from "./../src/components";
const TabBarComponent = (props) => <BottomTabBar {...props} />;

const MainNavigation = createBottomTabNavigator(
  {
    Feeds: { screen: Feeds },
    Cme: { screen: Cme },
    Event: { screen: Event },
    Webinar: { screen: Webinar },
    Profile: { screen: Profile },
    Learning: { screen: Learning },
    AtoZ: { screen: AtoZ },
  },
  {
    tabBarPosition: "bottom",
    lazy: true,
    swipeEnabled: false,
    animationEnabled: true,
    tabBarComponent: (props) => {
      let params =
        props.navigation.state.routes[props.navigation.state.index].params;
      let isHide = false;
      if (typeof params != "undefined") {
        isHide = params.isHide;
      }

      return (
        <SafeAreaView>
          <FooterMenu
            navigation={props.navigation}
            state={props.navigation.state.index}
            isHide={isHide}
          />
        </SafeAreaView>
      );
    },
  }
);

const HomeNavigation = createBottomTabNavigator(
  {
    Home: { screen: Home },
    Feed: { screen: FeedsNew },
    AlbumP2KB: { screen: ComingSoon },
  },
  {
    tabBarPosition: "bottom",
    lazy: true,
    swipeEnabled: false,
    animationEnabled: true,
    tabBarComponent: (props) => {
      return (
        <SafeAreaView>
          <BottomNavigator {...props} />
        </SafeAreaView>
      );
    },
  }
);
//Root Navigation
//const AppNavigator = createStackNavigator({
const StackNavigator = createStackNavigator(
  {
    // WebviewGrafik: {
    //     screen: WebviewGrafik,
    //     navigationOptions: ({ navigation }) => ({ header: null }),
    // },
    // Playground: {
    //     screen: Playground,
    //     navigationOptions: ({ navigation }) => ({ header: null }),
    // },

    Splash: {
      screen: Splash,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebviewPage: {
      screen: WebviewPage,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ConferenceRoom: {
      screen: ConferenceRoom,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    NonSpecialist: {
      screen: NonSpecialist,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ContactUs: {
      screen: ContactUs,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ChannelKonferensi: {
      screen: ChannelKonferensi,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ChangeDetailProfile: {
      screen: ChangeDetailProfile,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ChangeProfile: {
      screen: ChangeProfile,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Biodata: {
      screen: Biodata,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Notification: {
      screen: Notification,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Event: {
      screen: Event,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Webinar: {
      screen: Webinar,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Learning: {
      screen: Learning,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Cme: {
      screen: Cme,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SearchKonten: {
      screen: SearchKonten,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    HomeNavigation: {
      screen: HomeNavigation,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    AtoZ: {
      screen: AtoZ,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    DetailAtoZ: {
      screen: DetailAtoZ,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SearchAtoZ: {
      screen: SearchAtoZ,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Onboarding: {
      screen: Onboarding,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Login: {
      screen: Login,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Subscribtion: {
      screen: Subscribtion,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SelectSpecialist: {
      screen: SelectSpecialist,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SelectSubSpecialist: {
      screen: SelectSubSpecialist,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Register: {
      screen: Register,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterHello: {
      screen: RegisterHello,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterSuccess: {
      screen: RegisterSuccess,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterSpecialist: {
      screen: RegisterSpecialist,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterSubscribtions: {
      screen: RegisterSubscribtions,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterHomeLocation: {
      screen: RegisterHomeLocation,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterPracticeLocation: {
      screen: RegisterPracticeLocation,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    InputPracticeLocations: {
      screen: InputPracticeLocations,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterVerification: {
      screen: RegisterVerification,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ForgotPasswordConfirmation: {
      screen: ForgotPasswordConfirmation,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Home: {
      screen: MainNavigation,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ScanScreen: {
      screen: ScanScreen,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Learning: {
      screen: Learning,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    LearningSearch: {
      screen: LearningSearch,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    LearningSpecialist: {
      screen: LearningSpecialist,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    LearningSpecialistSearch: {
      screen: LearningSpecialistSearch,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    LearningJournalDetail: {
      screen: LearningJournalDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    LearningRequestJournal: {
      screen: LearningRequestJournal,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    EventDetail: {
      screen: EventDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    EventSearch: {
      screen: EventSearch,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    EventReservation: {
      screen: EventReservation,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ProfileDownload: {
      screen: ProfileDownload,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SearchCity: {
      screen: SearchCity,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    CmeDetail: {
      screen: CmeDetail,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    CmeHistory: {
      screen: CmeHistory,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    CmeQuiz: {
      screen: CmeQuiz,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    CmeCertificate: {
      screen: CmeCertificate,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    CmeKursus: {
      screen: CmeKursus,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    CmeMedicinus: {
      screen: CmeMedicinus,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    ProfileBookmark: {
      screen: ProfileBookmark,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    PdfView: {
      screen: PdfView,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    VideoView: {
      screen: VideoView,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    // Webinar: {
    //     screen: Webinar,
    //     navigationOptions: ({ navigation }) => ({ header: null }),
    // },
    WebinarVideo: {
      screen: WebinarVideo,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebinarSearch: {
      screen: WebinarSearch,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Chat: {
      screen: Chat,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ManageTemanDiabetes: {
      screen: ManageTemanDiabetes,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SearchPasien: {
      screen: SearchPasien,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    LaporanKesehatan: {
      screen: LaporanKesehatan,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    PerkenalanFitur: {
      screen: PerkenalanFitur,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    GeneralWebview: {
      screen: GeneralWebview,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebviewGrafik: {
      screen: WebviewGrafik,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    GeneralPdfView: {
      screen: GeneralPdfView,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    EventMateris: {
      screen: EventMateris,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebinarFilter: {
      screen: WebinarFilter,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    TestResult: {
      screen: TestResult,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    FinishPostTest: {
      screen: FinishPostTest,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Quisioner: {
      screen: Quisioner,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SetNewPassword: {
      screen: SetNewPassword,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebinarYoutube360: {
      screen: WebinarYoutube360,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ChooseCountry: {
      screen: ChooseCountry,
      navigationOptions: ({ navigation }) => ({
        header: null,
        gesturesEnabled: false,
      }),
    },
    SelectPayment: {
      screen: SelectPayment,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebinarTNC: {
      screen: WebinarTNC,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ProfileNew: {
      screen: ProfileNew,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    SearchAllByKonten: {
      screen: SearchAllByKonten,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    FilterSearchKonten: {
      screen: FilterSearchKonten,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    CalendarWebinar: {
      screen: CalendarWebinar,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Channel: {
      screen: Channel,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ChannelDetail: {
      screen: ChannelDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Job: {
      screen: Job,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    JobDetail: {
      screen: JobDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    JobTulisArtikel: {
      screen: JobTulisArtikel,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    JobReviewArtikel: {
      screen: JobReviewArtikel,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ChannelSeeAll: {
      screen: ChannelSeeAll,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ComingSoon: {
      screen: ComingSoon,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    JobFilter: {
      screen: JobFilter,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    JobListSpecialization: {
      screen: JobListSpecialization,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Forum: {
      screen: Forum,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ForumDetail: {
      screen: ForumDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    ReplyComment: {
      screen: ReplyComment,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    BannerProductDetail: {
      screen: BannerProductDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    Booth: {
      screen: Booth,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    BoothDetail: {
      screen: BoothDetail,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    RegisterMedicalID: {
      screen: RegisterMedicalID,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    WebinarDetail: {
      screen: WebinarDetail,
      screen: FeedsNew,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
    FeedsNew: {
      screen: FeedsNew,
      navigationOptions: ({ navigation }) => ({ header: null }),
    },
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: Platform.OS === "ios" ? false : true,
    },
  }
);

export default createAppContainer(StackNavigator);
