import AsyncStorage from '@react-native-community/async-storage';
import { STORAGE_TABLE_NAME } from './Common';
import Api, { errorMessage } from './Api';


var responseBase = (isSuccess, message, data) => {
    return {
        isSuccess: isSuccess,
        message: message,
        data: data,
    };
}

export var ParamObatAtoZ = {
    PREFIX: 'prefix',
    KEYWORD: "keyword",
    PAGE: 'page',
    LIMIT: "limit",
    OBAT_ID: 'obat_id'
};


export var getDataAtoZ = async (prefix, keyword, page, limit) => {
    let result = responseBase(false, 'Something went wrong!', null);

    try {
        //let paramRequest = `obatatoz?page=${page}&${ParamObatAtoZ.KEYWORD}=${keyword}&${ParamObatAtoZ.PREFIX}=${prefix}`;
        let paramRequest = `obatatoz?apps=d2d&${ParamObatAtoZ.PREFIX}=${prefix.toLowerCase()}&${ParamObatAtoZ.KEYWORD}=${keyword}&${ParamObatAtoZ.PAGE}=${page}&${ParamObatAtoZ.LIMIT}=${limit}`;

        let response = await Api.get(paramRequest);

        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }


    return result;
}

export var getDetailDataAtoZ = async (obat_id) => {
    let result = responseBase(false, 'Something went wrong!', null);

    try {
        let paramRequest = `obatatoz/detail?apps=d2d&${ParamObatAtoZ.OBAT_ID}=${obat_id}`;

        let response = await Api.get(paramRequest);

        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }


    return result;
}

export var getDataBannerObatAtoZ = async () => {
    let result = responseBase(false, 'Something went wrong!', null);

    try {
        let paramRequest = `obatatoz/banner`;

        let response = await Api.get(paramRequest);

        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }


    return result;
}