import AsyncStorage from "@react-native-community/async-storage";
import { STORAGE_TABLE_NAME, isAvailable } from "./Common";
import Api, { errorMessage } from "./Api";
import ApiVersion3, { errorMessageVersion3 } from "./ApiVersion3";
import ApiPayment from "./ApiPayment";
//--- Authentication ----/

export var doLoginApi = async (email) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let params = { email: email };
    let response = await ApiVersion3.post(`login`, params);
    if (response.status == 200) {
      result = responseBase(true, "Success", null);
      console.log("doLoginApi result: ", result);
      if (response.result != null) {
        if (response.acknowledge == true) {
          result = responseBase(true, "Success", response.result);
        } else {
          result = responseBase(false, response.message, null);
        }
      }
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var doRegisterApi = async (paramRequest) => {
  console.log("doRegisterApi paramRequest: ", paramRequest);
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let by_device =
      typeof paramRequest.device_unique_id != "undefined" ? "/by_device" : "";
    let response = await ApiVersion3.post(`register${by_device}`, paramRequest);
    console.log("doRegisterApi response: ", response);
    if (response.acknowledge == false) {
      //result = convertResponseInvalidPost(response)
      result = responseBaseVersion3(
        false,
        response.message,
        null,
        null,
        response.headers ? response.headers : null
      );
    } else {
      result = convertResponseBaseResultVersion3(response);
    }
  } catch (error) {
    console.log("doRegisterApi error: ", error);

    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doSendVerification = async (paramRequest) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.post(
      `register/resend_verification`,
      paramRequest
    );
    console.log("doSendVerification response: ", response);
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var activateRegisterVerification = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let paramRequest = `register/verify/${params.uid}/${params.code}`;

    let response = await ApiVersion3.get(paramRequest);
    console.log("doSendVerification response: ", response);
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doForgotPassword = async (paramRequest) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.post(`forgotpassword`, paramRequest);
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doResendForgotPassword = async (paramRequest) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.post(
      `forgotpassword/resend`,
      paramRequest
    );
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doSetNewPassword = async (paramRequest) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.post(
      `forgotpassword/change`,
      paramRequest
    );
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

//--- End Of Authentication ----//

//-- Feeds ---//

export var ParamFeeds = {
  COMPETENCE: "competence",
  PDF_JOURNAL: "pdf_journal",
  PDF_GUIDELINE: "pdf_guideline",
  VIDEO_LEARNING: "video_learning",
  EVENT: "event",
  UID: "uid",
  BOOKMARK: "bookmark",
  DOWNLOAD: "download",
  RESERVE: "reserve",
  JOIN: "join",
  FILTER_MONTH: "month",
  WEBINAR: "webinar",
  SEARCH: "search",
  PDF_MATERI: "pdf_materi",
  VIDEO_MATERI: "video_materi",
};

export var ValueTypeWebinar = {
  LATEST: "latest",
  RECORDED: "recorded",
  ALL: "all",
};

export var getDataWebinar = async (page, type, search) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let paramRequest = `feeds?page=${page}&${ParamFeeds.UID}=${getUid}&${
      ParamFeeds.WEBINAR
    }=${type}`;

    if (search != null && search != "") {
      paramRequest += `&${ParamFeeds.SEARCH}=${search}`;
    }

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getPopupShow = async () => {
  let result = responseBase(false, "Something went wrong!", null);
  try {
    let getUid = await AsyncStorage.getItem("UID");
    let url = `agreement/active`;
    let headers = await Api.headers();
    headers.uid = getUid;
    let response = await Api.xhrCustom(headers, url, null, "GET");
    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    console.log(error);
    result = responseBase(false, error, null);
  }

  return result;
};

export var cappingPopup = async (params) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let url = `agreement/cancel`;
    let headers = await Api.headers();
    headers.uid = getUid;
    let response = await Api.xhrCustom(headers, url, params, "POST");

    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var submitPopup = async (params) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let url = `agreement`;
    let headers = await Api.headers();
    headers.uid = getUid;
    let response = await Api.xhrCustom(headers, url, params, "POST");

    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getNewApiFeeds = async (page, competence) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let paramRequest = `feeds?page=${page}&${ParamFeeds.UID}=${getUid}`;

    if (competence != null) {
      paramRequest += `&${ParamFeeds.COMPETENCE}=${competence}`;
    }

    paramRequest += `&${ParamFeeds.PDF_JOURNAL}=true&${
      ParamFeeds.PDF_GUIDELINE
    }=true&${ParamFeeds.VIDEO_LEARNING}=true&${ParamFeeds.EVENT}=true&${
      ParamFeeds.WEBINAR
    }=${ValueTypeWebinar.LATEST}`;
    // paramRequest += `&${ParamFeeds.WEBINAR}=true&${ParamFeeds.VIDEO_LEARNING}=true`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(
        true,
        "Success",
        response.result.data,
        response.server_date
      );
      result.announcement = null;

      if (
        response.result.announcement != null &&
        response.result.announcement.webinar != null
      ) {
        let dataWebinar = response.result.announcement.webinar;

        if (dataWebinar.now != null && dataWebinar.now.length > 0) {
          result.announcement = dataWebinar.now[0];
          result.announcement.isLive = true;
        } else if (
          page == 1 &&
          dataWebinar.next != null &&
          dataWebinar.next.length > 0
        ) {
          let comingLive = dataWebinar.next[0];
          comingLive.isAnnouncement = true;
          comingLive.isLive = true;
          result.data = [comingLive, ...result.data];
        }

        console.log("result.data feeds ", result.data);
      }
    }

    if (response.status == 403 && response.message == "forbidden") {
      result = responseBase(false, "logout", null);
    }
  } catch (error) {
    console.log(error);
    result = responseBase(false, error, null);
  }

  return result;
};

export var getApiFeeds = async (
  page,
  competence,
  isPdfJournal,
  isPdfGuideline,
  isVideoLearning,
  isEvent,
  isBookmark,
  isDownload,
  isReserve,
  filterMonth,
  isPdfMateri,
  isVideoMateri
) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let paramRequest = `feeds?page=${page}&${ParamFeeds.UID}=${getUid}`;

    if (competence != null) {
      paramRequest += `&${ParamFeeds.COMPETENCE}=${competence}`;
    }

    if (isPdfJournal === true) {
      paramRequest += `&${ParamFeeds.PDF_JOURNAL}=true`;
    }

    if (isPdfGuideline === true) {
      paramRequest += `&${ParamFeeds.PDF_GUIDELINE}=true`;
    }

    if (isVideoLearning === true) {
      paramRequest += `&${ParamFeeds.VIDEO_LEARNING}=true`;
    }

    if (isEvent === true) {
      paramRequest += `&${ParamFeeds.EVENT}=true`;
    }

    if (isBookmark === true) {
      paramRequest += `&${ParamFeeds.BOOKMARK}=true`;
    }

    if (isDownload === true) {
      paramRequest += `&${ParamFeeds.DOWNLOAD}=true`;
    }

    if (isReserve === true) {
      // paramRequest += `&${ParamFeeds.RESERVE}=true`;
      paramRequest += `&${ParamFeeds.JOIN}=true`;
    }

    if (filterMonth != null) {
      paramRequest += `&${ParamFeeds.FILTER_MONTH}=${filterMonth}`;
    }

    if (isPdfMateri === true) {
      paramRequest += `&${ParamFeeds.PDF_MATERI}=true`;
    }

    if (isVideoMateri === true) {
      paramRequest += `&${ParamFeeds.VIDEO_MATERI}=true`;
    }

    let response = await Api.get(paramRequest);

    // let response = await Api.get(`feeds?page=${page}&${ParamFeeds.COMPETENCE}=${competence}&${ParamFeeds.PDF_JOURNAL}=${isPdfJournal}&${ParamFeeds.PDF_GUIDELINE}=${isPdfGuideline}&${ParamFeeds.VIDEO_LEARNING}=${isVideoLearning}&${ParamFeeds.EVENT}=${isEvent}&${ParamFeeds.BOOKMARK}=${isBookmark}&${ParamFeeds.DOWNLOAD}=${isDownload}&${ParamFeeds.RESERVE}=${isReserve}&${ParamFeeds.UID}=${getUid}`);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }

    if (response.status == 403 && response.message == "forbidden") {
      result = responseBase(false, "logout", null);
    }
  } catch (error) {
    console.log(error);
    result = responseBase(false, error, null);
  }

  return result;
};

export var sendFIrebaseToken = async (deviceToken) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");

    let params = {
      uid: uid,
      deviceToken: deviceToken,
    };

    let response = await Api.post("fcm/save", params);

    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var deleteFIrebaseToken = async () => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");
    let deviceToken = await AsyncStorage.getItem("FCM_TOKEN");

    // console.log("value deviceToken -> ", deviceToken);
    try {
      deviceToken = JSON.parse(deviceToken);
    } catch (e) {
      console.log("error json parse -> ", e);
    }


    let params = {
      uid: uid,
      deviceToken: deviceToken,
    };
    console.log("deleteFIrebaseToken params: ", params);
    let response = await Api.post("fcm/remove", params);

    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End Of Feeds ---//

//--- CME Quiz ---//

export var ValueTypeQuiz = {
  AVAILABLE: "available",
  DONE: "done",
  ALL: "all",
  OFFLINE: "offline",
};

export var getQuizList = async (page, type) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let paramRequest = `cme/quiz-list/${type}/${getUid}?page=${page}`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getQuestionList = async (quizId) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    // let getUid = await AsyncStorage.getItem('UID');
    let paramRequest = `cme/quiz-detail/${quizId}`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var submitAnswerQuiz = async (params) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await Api.post("cme/submit-quiz", params);
    console.log("submitAnswerQuiz response: ", response);
    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var uploadSkpManual = async (
  skp,
  title,
  source,
  takenOn,
  uri,
  isPdf
) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");

    const data = new FormData();
    data.append("uid", uid); // you can append anyone.
    data.append("skp", skp); // you can append anyone.
    data.append("title", title); // you can append anyone.
    data.append("source", source);
    data.append("taken_on", takenOn); // you can append anyone.
    data.append("filename", {
      uri: uri,
      //   type: 'image/jpeg', // or photo.type
      type: isPdf == true ? "application/pdf" : "multipart/form-data",
      name: isPdf == true ? ".pdf" : ".jpg",
    });

    let response = await Api.postMultipart("cme/submit-offline", data);

    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var resetSkp = async () => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");

    let requestParams = {
      uid: uid,
    };

    let response = await Api.post("cme/reset-skp", requestParams);

    result = convertResponseBaseWithMessage(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var sendEmailCertificate = async (paramCertificate) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    if (dataProfile != null) {
      if (paramCertificate.email == null) {
        paramCertificate.email = dataProfile.email;
      }
      requestParams = {
        title: paramCertificate.title,
        email: paramCertificate.email,
        displayName: dataProfile.name,
        certificate: paramCertificate.certificate,
      };

      let response = await ApiVersion3.post(
        "cme/email-certificate",
        requestParams
      );

      result = convertResponseBaseWithMessage(response);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var sendEmailEventCertificate = async (paramCertificate) => {
  let result = responseBase(false, "Something went wrong!", null);
  console.log("log paramCertificate", paramCertificate);
  try {
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);
    let requestParams = {};
    console.log("log param dataProfile", paramCertificate);
    if (dataProfile != null) {
      if (dataProfile != null) {
        if (paramCertificate.email == null) {
          paramCertificate.email = dataProfile.email;
        }
        requestParams = {
          title: paramCertificate.title,
          email: paramCertificate.email,
          displayName: dataProfile.name,
          certificate: paramCertificate.certificate,
        };
      }
      let response = await ApiVersion3.post(
        "event/email-certificate",
        requestParams
      );

      result = convertResponseBaseWithMessage(response);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getDetailCme = async (id) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let paramRequest = `redirect/cme/false/${id}/${getUid}`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End Of Cme Quiz ---//

//--- Detail Slug ---//

export var getDetailSlug = async (category, slug, hash) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);
    let paramRequest = `redirect/${category}/${slug}/${hash}/${getUid}/${
      dataProfile.email
    }`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getEventDetail = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let paramRequest = "";
    if (params.type === "deeplink") {
      paramRequest = `event/hash/${params.hash}`;
    } else {
      paramRequest = `event/${params.id}`;
    }
    let response = await ApiVersion3.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBaseVersion3(true, "Success", response.result);
    }
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doRegisterEvent = async (slug_hash) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    if (dataProfile != null) {
      let requestParams = {
        email: dataProfile.email,
        slug_hash: slug_hash,
      };

      let response = await Api.post(
        "event/signin/" + dataProfile.uid,
        requestParams
      );

      result = convertResponseBaseWithMessage(response);
      console.log("result ", result);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End of detail slug ---//

// --- Competence --//

export var ParamCompetence = {
  SPESIALIZATION: "spesialization",
  SUBSCRIPTION: "subscription",
  NON_SPESIALIZATION: "nonspesialization",
};

export var getCompetence = async (params) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await Api.get(`competence?`);

    result = convertResponseBase(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getAllSubscribtions = async () => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await Api.get(`competence/noauth`);

    result = convertResponseBase(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var paramSpecialization = {
  NESTING: "nesting",
  KEYWORD: "keyword",
  PAGE: "page",
  LIMIT: "limit",
  TYPE: "type",
};

export var getDataSpesializations = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  if (params.type == "nonspecialist") {
    params.type == params.type;
  } else {
    params.type =
      params.type == "SPECIALIST" ? "specialization" : "subscription";
  }

  console.log("getDataSpesializations params.type ", params.type);

  let paramsRequest = `specialist?${paramSpecialization.PAGE}=${params.page}&${
    paramSpecialization.LIMIT
  }=${params.limit}&${paramSpecialization.KEYWORD}=${params.keyword}&${
    paramSpecialization.TYPE
  }=${params.type}&${paramSpecialization.NESTING}=${params.nesting}`;

  try {
    let response = await ApiVersion3.get(paramsRequest);
    console.log("getDataSpesializations response ", response);

    result = convertResponseBaseVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doSendCompetence = async (competenceList, type) => {
  let result = responseBase(false, "Something went wrong!", null);

  if (competenceList != null && competenceList.length > 0) {
    try {
      let getUid = await AsyncStorage.getItem("UID");

      let competenceId = [];
      let competenceTitle = [];
      let competenceDesc = [];
      let subParentId = [];
      let subCompetenceId = [];
      let subCompetenceTitle = [];
      let subCompetenceDesc = [];

      competenceList.map(function(d, i) {
        competenceId.push(d.id);
        competenceTitle.push(d.title);
        competenceDesc.push(d.description);

        if (d.subspecialist != null && d.subspecialist.length > 0) {
          d.subspecialist.map(function(da, i) {
            if (da.id > 0) {
              subParentId.push(d.id);
              subCompetenceId.push(da.id);
              subCompetenceTitle.push(da.title);
              subCompetenceDesc.push(da.description);
            }
          });
        }
      });

      let params = {
        uid: getUid,
        competence_id: competenceId.join(","),
        competence_name: competenceTitle.join(","),
        competence_description: competenceDesc.join(","),
        parent_id: subParentId.length > 0 ? subParentId.join(",") : "",
        subcompetence_id:
          subCompetenceId.length > 0 ? subCompetenceId.join(",") : "",
        subcompetence_name:
          subCompetenceTitle.length > 0 ? subCompetenceTitle.join(",") : "",
        subcompetence_description:
          subCompetenceDesc.length > 0 ? subCompetenceDesc.join(",") : "",
      };
      console.log("doSendCompetence params: ", params);
      console.log("doSendCompetence type: ", type);
      let response = await Api.post("competence/subscribe/" + type, params);

      result = convertResponseBase(response);
    } catch (error) {
      result = responseBase(false, error, null);
    }
  } else {
    result = responseBase(false, "Empty Data", null);
  }

  return result;
};

export var doSendCompetenceOld = async (competenceList, type) => {
  let result = responseBase(false, "Something went wrong!", null);

  if (competenceList != null && competenceList.length > 0) {
    try {
      let getUid = await AsyncStorage.getItem("UID");

      let subscriptionId = [];
      let subscriptionTitle = [];

      competenceList.map(function(d, i) {
        subscriptionId.push(d.id);
        subscriptionTitle.push(d.title);
      });

      let params = {
        uid: getUid,
        competence_id: subscriptionId.join(","),
        competence_name: subscriptionTitle.join(","),
      };

      let response = await Api.post("competence/subscribe/" + type, params);

      result = convertResponseBase(response);
    } catch (error) {
      result = responseBase(false, error, null);
    }
  } else {
    result = responseBase(false, "Empty Data", null);
  }

  return result;
};

// --- End Of Competence ---//

//--- User Action ---//

export var ParamAction = {
  VIEW: "view",
  BOOKMARK: "bookmark",
  UNBOOKMARK: "unbookmark",
  DOWNLOAD: "download",
  DELETE_DOWNLOAD: "delete_download",
  RESERVE: "reserve",
  LIKE: "like",
  UNLIKE: "unlike",
};

export var ParamContent = {
  PDF_MATERIS: "pdf_materi",
  VIDEO_MATERIS: "video_materi",
  PDF_JOURNAL: "pdf_journal",
  PDF_GUIDELINE: "pdf_guideline",
  VIDEO_LEARNING: "video_learning",
  EVENT: "event",
  WEBINAR: "webinar",
};

export var doActionUserActivity = async (action, content, contentId) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let params = {
      act: action,
      content: content,
      contentId: contentId,
      uid: getUid,
    };
    let response = await Api.post("activity/", params);

    console.log("doActionUserActivity response: ", response);

    result = convertResponseBase(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  console.log(result);
  return result;
};

export var doReservationEvent = async (eventId, eventName) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let params = {
      uid: getUid,
      event_id: eventId,
      event_name: eventName,
    };
    let response = await Api.post("event/reserve/add", params);

    result = convertResponseBase(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End of User Action ---//

//--- Profile -----//

export var getProfileUser = async () => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.get(`profile/`);
    console.log("NetworkUtility getProfileUser response: ", response);

    result = responseBase(true, "Success", response.result);
    console.log("NetworkUtility getProfileUser result: ", result);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var updateDataProfile = async (params) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.put("profile/", params);
    // console.log('NetworkUtility updateDataProfile response: ', response)
    // result = responseBase(true, 'Success', response);
    // console.log('NetworkUtility updateDataProfile result: ', result)
    result = convertResponseBaseWithMessageVersion3(response);
    console.log("NetworkUtility updateDataProfile result: ", result);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var paramNotif = {
  SORT: "sort",
  KEYWORD: "keyword",
  PAGE: "page",
  LIMIT: "limit",
  TYPE: "type",
};

export var readNotification = async (dataNotif) => {
  let result = responseBase(false, "Something went wrong!", null);
  try {
    let params = {
      id: dataNotif.id,
      title: dataNotif.title,
      body: dataNotif.body,
      type: dataNotif.type,
      data: dataNotif.data,
      status: "read",
    };

    let response = await Api.post(`notification/`, params);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};
export var getListNotification = async (page, limit, type) => {
  let result = responseBase(false, "Something went wrong!", null);
  let paramRequest = `notification?${paramNotif.PAGE}=${page}&${
    paramNotif.LIMIT
  }=${limit}&${paramNotif.TYPE}=${type}`;

  try {
    let response = await Api.get(paramRequest);
    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- Upload Image Profile ----//

export var doUploadProfileImage = async (uri, nameFile) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");

    const data = new FormData();
    data.append("uid", getUid); // you can append anyone.
    data.append("filename", {
      uri: uri,
      //   type: 'image/jpeg', // or photo.type
      type: "multipart/form-data",
      name: ".jpg",
    });

    let response = await Api.postMultipart("profile/picture/", data);

    if (response != null && response.url != null) {
      result = responseBase(
        true,
        "Success",
        response.url.replace("http", "https")
      );
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- end of upload image profile ---//

//--- Detail Slug ---//

export var getDataCity = async () => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await Api.get(`kota`);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End of detail slug ---//

//--- Get Detail T&C ---//

export var ParamSetting = {
  VERSION: "version",
  VERSION_ANDROID: "version_android",
  VERSION_IOS: "version_ios",
  MANDATORY_UPDATE: "force_update",
  CONTACT_US: "contact_us",
  PRIVACY_POLICY: "privacy_policy",
  TERMS_CONDITIONS: "terms_conditions",
};

export var getSetting = async (type) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await Api.get(`settings/${type}`);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End Get Detail T&C ---//

//--- Mddicinus ---//
export var getCourseMedicinus = async () => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");
    let getProfile = await AsyncStorage.getItem("PROFILE");
    let profile = JSON.parse(getProfile);

    if (uid != null && profile != null) {
      let params = {
        uid: uid,
        idnumber: profile.npa_idi,
      };

      let response = await Api.post(`medicinus/cek`, params);

      if (response != null && response.result != null) {
        result = responseBase(true, "Success", response.result.data);
      }
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var cek = async (id) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let response = await Api.get(`medicinus/get-course/${id}`);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//--- End of medicinus ---//

//--- Base Response ---//

var responseBase = (isSuccess, message, data, server_date = "") => {
  return {
    isSuccess: isSuccess,
    message: message,
    data: data,
    server_date: server_date,
  };
};

var convertResponseBase = (response) => {
  let result = responseBase(false, "Something went wrong!", null);

  if (response.status == 200) {
    result = responseBase(true, "Success", null);

    if (response.result != null) {
      if (response.result.acknowledge == true) {
        result = responseBase(true, "Success", response.result.data);
      } else {
        result = responseBase(false, response.result.message, null);
      }
    }
  } else {
    result = responseBase(false, response.message, null);
  }

  return result;
};

var convertResponseBaseWithMessage = (response) => {
  let result = responseBase(false, "Something went wrong!", null);

  if (response.status == 200) {
    result = responseBase(true, "Success", null);

    if (response.result != null) {
      if (response.result.acknowledge == true) {
        result = responseBase(
          true,
          response.result.message,
          response.result.data
        );
      } else {
        result = responseBase(false, response.result.message, null);
      }
    }
  } else {
    result = responseBase(false, response.message, null);
  }

  return result;
};
//--- End of Response ---//

export var getEventMateri = async (id) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let paramRequest = `materi/${id}`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getLinkGraphTD = async (userIdTd) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let paramRequest = `redirect/temandiabetes/${userIdTd}/${getUid}`;

    let response = await Api.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result.data);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//login rocket chat api
export var loginRocketChatApi = async (groupChatID) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    // let params = { user: user, groupChatID: groupChatID, name: name };

    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    console.log("getJsonProfile: ", getJsonProfile);

    let dataProfile = JSON.parse(getJsonProfile);
    console.log("dataProfile: ", dataProfile);

    if (dataProfile != null) {
      let requestParams = {
        user: dataProfile.email,
        groupchatID: groupChatID,
        name: dataProfile.name,
        photo:
          "https://d2doss.oss-ap-southeast-5.aliyuncs.com/image/profile/" +
          dataProfile.photo,
      };
      console.log("requestParams: ", requestParams);
      let response = await ApiVersion3.postRocketChat(``, requestParams);

      console.log("response: ", response);
      result = convertResponseBaseResultVersion3(response);
      //result = responseBaseVersion3(true, 'Success', response.result);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

//API V3
//--- Base Response ---//

var responseBaseVersion3 = (
  isSuccess,
  message,
  docs,
  server_date = "",
  headers
) => {
  return {
    isSuccess: isSuccess,
    message: message,
    docs: docs,
    server_date: server_date,
    headers: headers,
  };
};

var convertResponseBaseVersion3 = (response) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  if (response.status == 200) {
    result = responseBaseVersion3(true, "Success", null);

    if (response != null) {
      if (response.acknowledge == true) {
        result = responseBaseVersion3(
          true,
          "Success",
          response.result.docs,
          response.result.server_date
        );
      } else {
        result = responseBaseVersion3(false, response.message, null);
      }
    }
  } else {
    result = responseBaseVersion3(false, response.message, null);
  }

  return result;
};

var convertResponseBaseWithMessageVersion3 = (response) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  if (response.status == 200) {
    result = responseBaseVersion3(true, "Success", null);

    if (response.acknowledge == true) {
      result = responseBaseVersion3(true, response.message, response.result);
    } else {
      result = responseBaseVersion3(false, response.message, null);
    }
  } else {
    result = responseBaseVersion3(false, response.message, null);
  }

  return result;
};

export var convertResponseInvalidPost = (response) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  let message = "";

  message = response.errors.validation.body.map(function(value, index) {
    message += value.messages;
  });

  result = responseBaseVersion3(false, message, null);

  return result;
};

var convertResponseBaseResultVersion3 = (response) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  if (response.status == 200) {
    result = responseBaseVersion3(true, "Success", null);

    if (response != null) {
      if (response.acknowledge == true) {
        result = responseBaseVersion3(true, "Success", response.result);
      } else {
        result = responseBaseVersion3(true, response.message, null);
      }
    }
  } else {
    result = responseBaseVersion3(false, response.message, null);
  }

  return result;
};

export var ValueTypeSKPHistory = {
  ALL: "all",
  CME: "cme",
  OFFLINE: "offline",
  EVENT: "event",
  WEBINAR: "webinar",
};

export var ValueStatusSKPHistory = {
  VALID: "valid",
  INVALID: "invalid",
  REJECTED: "rejected",
};

export var getSKPHistory = async (page, type, status = null) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  let limit = 10;

  try {
    let paramRequest = `skp/${type}?page=${page}&limit=${limit}`;
    if (status != null) {
      paramRequest = paramRequest + `&status=${status}`;
    }
    let response = await ApiVersion3.get(paramRequest);
    result = convertResponseBaseVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doDetailCertificate = async (data) => {
  let result = responseBase(false, "Something went wrong!", null);
  try {
    let params = {
      content_id: data.content_id,
    };
    // let paramRequest = `skp/${type}?page=${page}&limit=${limit}`;
    let type = data.type.replace("certificate_", "");

    if (type == "offline") {
      params.certificate = data.certificate;
    }

    let response = await ApiVersion3.post(`skp/${type}?`, params);

    if (response != null && response.result != null) {
      result = responseBase(true, "Success", response.result);
    }
  } catch (error) {
    result = responseBase(false, error, null);
  }

  return result;
};

export var getListWebinar = async (
  page,
  skp = false,
  specialist = "",
  keyword = ""
) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  let limit = 10;

  try {
    // let uid = await AsyncStorage.getItem('UID');
    // let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    // let dataProfile = JSON.parse(getJsonProfile);
    // let email = dataProfile.email

    let response = await ApiVersion3.get(
      `webinar?page=${page}&limit=${limit}&skp=${skp}&specialist=${specialist}&keyword=${keyword}`
    );

    result = convertResponseBaseVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var getWebinarDetail = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  let limit = 10;

  try {
    let paramRequest = "";
    if (params.type === "deeplink") {
      paramRequest = `webinar/hash/${params.hash}`;
    } else {
      paramRequest = `webinar/${params.id}`;
    }
    let response = await ApiVersion3.get(paramRequest);

    if (response != null && response.result != null) {
      result = responseBaseVersion3(true, "Success", response.result);
    }
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var getPretestWebinar = async (id) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);
    let email = dataProfile.email;

    let response = await ApiVersion3.get(`webinar/pretest/${id}`);
    result = responseBaseVersion3(true, "Success", response.result);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var getPosttestWebinar = async (id) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    // let uid = await AsyncStorage.getItem('UID');
    // let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    // let dataProfile = JSON.parse(getJsonProfile);
    // let email = dataProfile.email

    let response = await ApiVersion3.get(`webinar/posttest/${id}`);
    result = responseBaseVersion3(true, "Success", response.result);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var submitPretestPostest = async (dataParams) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);
  try {
    //let uid = await AsyncStorage.getItem('UID');
    // let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    // let dataProfile = JSON.parse(getJsonProfile);
    // let email = dataProfile.email

    let type = dataParams.type;
    let id = dataParams.webinarId;
    let params = {
      id_attempt: dataParams.id_attempt,
      answers: dataParams.answers,
      time: dataParams.time,
      //uid: uid,
      //email: email
    };
    console.log("params: ", params);
    let response = await ApiVersion3.post(
      `webinar/${type}/${id}/answers`,
      params
    );
    console.log("response: ", response);
    if (response != null && response.result != null) {
      result = responseBaseVersion3(true, "Success", response.result);
    }
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var submitWebinarCertificate = async (dataParams) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);
  try {
    //let uid = await AsyncStorage.getItem('UID');

    let params = {
      webinar_id: dataParams.webinar_id,
      fullname: dataParams.fullname,
      //uid: uid
    };
    console.log(params);

    let response = await ApiVersion3.post(`webinar/ecertificate`, params);
    console.log(response);
    result = convertResponseBaseResultVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var userActivity = async (dataParams) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getUid = await AsyncStorage.getItem("UID");
    let paramsBody = {
      app: 1,
      activity: dataParams.activity,
      content: dataParams.content,
      content_id: dataParams.content_id,
    };
    let response = await ApiVersion3.post("useractivity/", paramsBody);

    console.log("userActivity response: ", response);

    result = convertResponseBaseResultVersion3(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  console.log(result);
  return result;
};

export var submitSKP = async (type, dataParams) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let paramsBody = {
      content_id: dataParams.content_id,
    };

    let response = await ApiVersion3.post(`skp/${type}`, paramsBody);

    result = convertResponseBaseResultVersion3(response);
  } catch (error) {
    result = responseBase(false, error, null);
  }

  console.log(result);
  return result;
};

export var sendEmailWebinarCertificate = async (paramCertificate) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    if (dataProfile != null) {
      if (paramCertificate.email == null) {
        paramCertificate.email = dataProfile.email;
      }
      requestParams = {
        title: paramCertificate.title,
        email: paramCertificate.email,
        displayName: dataProfile.name,
        certificate: paramCertificate.certificate,
      };

      let response = await ApiVersion3.post(
        "webinar/email-certificate",
        requestParams
      );

      result = convertResponseBaseWithMessageVersion3(response);
    }
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var EnumTypePayment = {
  WEBINAR: "webinar",
  EVENT: "event",
  JOURNAL: "journal",
};

export var startPayment = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let paramsBody = {
      id: params.id,
      amount: params.amount,
      type: params.type,
    };

    let response = await ApiPayment.post("payment/start", paramsBody);
    console.log("startPayment request: ", paramsBody);
    console.log("startPayment response: ", response);
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var cancelPayment = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let paramsBody = {
      order_id: params.order_id,
    };

    let response = await ApiPayment.post("payment/cancel", paramsBody);
    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doCheckVersion = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let paramsBody = {
      version: params.version,
      build_number: params.build_number,
    };
    let response = await ApiVersion3.post("version", paramsBody);

    console.log("doCheckVersion api v3 response: ", response, params);

    result = convertResponseBaseResultVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  console.log(result);
  return result;
};

export var getWebinarTNC = async (params) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let uid = await AsyncStorage.getItem("UID");
    let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let dataProfile = JSON.parse(getJsonProfile);

    let country_code = dataProfile.country_code;
    let webinar_id = params.webinar_id;

    let paramRequest = `agreement/webinartnc?country_code=${country_code}&webinar_id=${webinar_id}`;

    let response = await ApiVersion3.get(paramRequest);
    result = responseBaseVersion3(true, "Success", response.result);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var sendAgreementWebinarTNC = async (params) => {
  let result = responseBase(false, "Something went wrong!", null);

  try {
    let requestParams = {
      webinar_id: params.webinar_id,
      isAgree: params.isAgree ? 1 : 0,
      agreement_id: params.agreement_id,
    };

    let response = await ApiVersion3.post(
      "agreement/webinartnc",
      requestParams
    );

    result = convertResponseBaseWithMessageVersion3(response);
  } catch (error) {
    result = responseBaseVersion3(false, error, null);
  }

  return result;
};

export var doRequestJournal = async (paramRequest) => {
  let result = responseBaseVersion3(false, "Something went wrong!", null);

  try {
    let response = await ApiVersion3.post(`journal/request`, paramRequest);
    console.log("doRequestJournal response: ", response);
    if (response.acknowledge == false) {
      result = responseBaseVersion3(false, response.message, null);
    } else {
      result = convertResponseBaseWithMessageVersion3(response);
    }
  } catch (error) {
    console.log("doRequestJournal error: ", error);

    result = responseBaseVersion3(false, error, null);
  }

  return result;
};
