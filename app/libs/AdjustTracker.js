import { AdjustConfig, AdjustEvent, Adjust } from "react-native-adjust";
import firebase from "react-native-firebase";
import { BASE_URL, URL } from "./Api";
import _ from "lodash";
import Config from "react-native-config";

const gueloginAuth = firebase.app("guelogin");

export const AdjustTrackerConfig = {
  Environment:
    Config.ENV == "production"
      ? AdjustConfig.EnvironmentProduction
      : AdjustConfig.EnvironmentSandbox,
  AppToken: "lo1ag6utq6tc",
  // PushToken            : getToken(),

  Login_Login: "9joaxx",
  Login_Facebook: "rgdsit",
  Login_Google: "8k71xu",
  Login_Register: "1yisjw",
  Login_Start: "zfe43y",
  Login_Forgot_Password: "reguvz",

  Forgot_Password_Start: "2l9hat",
  Forgot_Password_Finish: "gyq11s",

  Register_Register: "kpkq4i",

  Onboarding_Next: "tsavfe",

  Onboarding_Spec: "evah63",
  Onboarding_Spec_Next: "qyf1oy",
  Onboarding_Spec_Search: "9tmy3d",
  Onboarding_Spec_Save: "8hgz49",
  Onboarding_Spec_Add: "i18ksb",

  Onboarding_Subs: "9mcxha",
  Onboarding_Subs_Search: "cwuw0b",
  Onboarding_Subs_Save: "3474y0",
  Onboarding_Subs_Add: "e6pifp",
  Onboarding_Subs_Next: "eadj77",

  Onboarding_Practiceloc: "nu7bxg",
  Onboarding_Practiceloc_Freetext: "ag301g",
  Onboarding_Practiceloc_Add: "107hg6",
  Onboarding_Practiceloc_Save: "orzgmm",

  Onboarding_Finish: "fg43ev",

  Feeds_Feeds: "abcckb",
  ScanQR: "t7u4sa",

  CME_CME: "tdhewj",
  CME_SKP_Quiz: "ghgdlv",
  CME_ADD_Serti: "cnuhyq",
  CME_History: "8s087y",

  Explore_Explore: "h69zu5",
  Explore_Event: "25p0el",
  Explore_Learning: "q8sj86",
  Explore_ReqJournal: "4xfc07",

  RequestJurnal_Submit: "76tdze",

  Event_Event: "u1y5a0",
  Event_Bookmark: "oaougp",
  Event_Rsvp: "iconrm",
  Event_Download_Materi: "qij87v",
  Event_Download_Certificate: "g976eg",

  Webinar_Latest: "6el2mo",
  Webinar_Recorded: "k94qfp", // not used
  Webinar_Video_Search: "jhia0d",
  Webinar_Video_View: "h70a2n",
  Webinar_Video_FF: "xb2rml",
  Webinar_Video_Fullscreen: "an7ete",
  Webinar_Video_Like: "kchixu",
  Webinar_Video_Question: "6y0o2s",
  Webinar_Webinar_Live: "ql3ym9",
  Webinar_Onload_Page: "s2z68z",
  Webinar_Video_BackButton: "xepmql",
  Webinar_Video_Pause: "3yf5cb",
  Webinar_Video_Share: "brhwki",
  Webinar_Video_ShowDesc: "dcr36h",
  Webinar_Video_HideDesc: "9uvrod",
  Webinar_Video_Posttest: "qmax9y",
  Webinar_Video_FullScreen_Onward: "yf348i",
  Webinar_Video_FullScreen_Backward: "32gz5s",
  Webinar_Video_FullScreen_Pause: "wt28mb",
  Webinar_Video_Filter: "lroy3i",

  Profile_Bookmark: "empyjt",
  Profile_Download: "oehy9j",
  Profile_Profile: "h325v2",
  Profile_Notif: "pgffez",
  Profile_Save: "zaz2uj",

  Specialist: {
    0: "7c6lfe", // Learning_GP
    1: "jw521k", // Learning_Anak
    2: "s8vpd7", // Learning_Anestesi
    5: "hvl4qs", // Learning_Bedah
    9: "g6t4fw", // Learning_Bedahsaraf
    14: "49minj", // Learning_Jantung
    27: "7lkfc1", // Learning_Obgyn
    32: "l4n9d2", // Learning_Pulmo
    36: "ahoh0h", // Learning_Penyakitdalam
    42: "qpp4vs", // Learning_Saraf
    43: "wohq0d", // Learning_THT
  },

  //tambahan per 22 Mei
  CME_History_D2DSKP_1: "mj29yc", //done
  CME_History_Manual_SKP: "84ukrb", //done

  Feeds_Events: "ku7r7c", //done
  Feeds_Share: "m8rydx", //done
  Feeds_Bookmarks: "wsx2gl", //done
  Feeds_Events_Loc: "411e5c", //done
  Feeds_Events_Bookmarks: "zby8jb", //done
  Feeds_Events_Share: "9u3s1w", //done
  Feeds_Events_Calendar: "yofutg", //done
  Feeds_Journal: "rzcyog", //done
  Feeds_Journal_Bookmark: "e7mcuf", //done
  Feeds_Journal_ViewPDF: "69pdzf", //done
  Feeds_Webinar: "kvqoks", //done
  Feeds_Webinar_FastForward: "e4zeb0", //done
  Feeds_Webinar_FullScreen: "q0z7wo", //done
  Feeds_Webinar_Like: "ym2a0b", //done
  Feeds_Webinar_Share: "xsb0ng", //done
  Feeds_Webinar_Question: "aemu9y", //done
  Feeds_Live: "lqhewj",

  Feeds_Pitakuning_Profile: "1nbjxp",
  Feeds_Journal_BackButton: "7iiadw",
  Feeds_Journal_SeeMore: "xeup6x",
  Feeds_Journal_SeeLess: "kk4z8n",
  Feeds_Guideline: "k53kol",
  Feeds_Guideline_Share: "ovps1v",
  Feeds_Guideline_Bookmark: "nfrndf",
  Feeds_Guideline_Close: "n00370",
  Feeds_Guideline_Download: "5vvu9t",

  CME_Reset: "ws0srn", //done
  CME_Reset_Now: "rvge4g", //done
  CME_Add_Upload: "24idum", //done
  CME_SKP_Certidownload: "ufdkfz", //done
  CME_SKP_SentEmail: "3zdyp7", //done
  CME_History_D2DSKP_Download: "qycuwb", //done
  CME_History_D2DSKP_SentEmail: "z3tcv7", //done
  CME_History_ManualSKP_Download: "wd3nja", //done
  CME_History_ManualSKP_SentEmail: "oz1mrb", //done

  Event_Event_Search: "e3rc6a", //done
  Event_Event_Filter: "l00i5m", //done
  Event_Event_Share: "6tvxt6", //done
  Event_Event_Bookmark: "toscqh", //done
  Event_Bookmark_Share: "19ae9n", //done
  Event_Bookmark_Bookmark: "ufiwxz", //done
  Event_Joined_Share: "3ui5zw", //done
  Event_Joined_Bookmark: "q3aihv", //done
  Event_Open_GPS: "z1duzb", //done
  Event_Open_Bookmark: "mh91mz", //done
  Event_Open_Share: "ed8re6", //done
  Event_Open_Calendar: "mls3do", //done
  Event_Event_Detail_BackButton: "69kgje",
  Event_Bookmark_Detail_BackButton: "o3hzrr",
  Event_Bookmark_Detail_Share: "k2gs5m",
  Event_Bookmark_Detail_Bookmark: "k1sa43",
  Event_Bookmark_Detail_Calendar: "tyl6c1",

  Learning_Search: "97mqb4", //done
  Learning_Spesialis_Search: "cg87sw", //done
  Learning_Spesialis_LatestShare: "pgy77l", //done
  Learning_Spesialis_LatestBookmark: "51ccut", //done
  Learning_Spesialis_BookmarkShare: "99zkbl", //done
  Learning_Spesialis_BookmarkBookmark: "vqv63d", //done
  Learning_Spesialis_All: "hfd4yi", //done
  Learning_Spesialis_Journal: "7zhmhd", //done
  Learning_Spesialis_Video: "clcoxm", //done
  Learning_Spesialis_Guideline: "lmlb7c", //done
  Learning_Spesialis_RequestJurnal: "jsu9ze", //done
  Learning_SpesialisOpen_Bookmark: "jtjh0h", //done
  Learning_SpesialisOpen_ViewPDF: "kq3np8", //done
  Learning_SpesialisOpen_ViewPDFDownload: "b1dqdh", //done

  Profile_T_AND_C: "5lsiqh", //done
  Profile_PrivacyPolicy: "swhrwh", //done
  Profile_ContactUs: "a5pddu", //done
  Profile_Logout: "te46f6", //done
  Profile_UploadPhoto: "nioax2",
  Profile_3Dots: "2bvpag",
  Profile_ChangePhoto: "h7g5l5",

  CME_SKP_StartQuiz: "bu5u5i", //done
  CME_SKP_Submit: "ljecno", //done
  CME_ADD_OK: "hkbxku", //done
  CME_History_ManualSKP_1: "5u150r",
  CME_SKP_Quiz_NonTaken: "tf52x2",

  //tambahan per 19 Juni
  Event_Event_Detail: "56wqxv", //done
  Event_Bookmark_Detail: "2pxrl1", //done
  Event_Joined_Detail: "l5hxm9", //done

  Learning_Latest_Details: "qknfw0", //done
  Learning_Bookmark_Details: "v20aoi", //done
  Learning_SpesialisOpen_Details: "7girt8", //not used

  CME_History_D2DSKP_Details: "co1gmm", //done

  // tambahan obat AtoZ 10 Okt 2019
  ObatAZ_ObatAZ: "8itu4o",
  ObatAZ_Search: "7fwdaf",
  ObatAZ_Banner: "2hqb0h",
  ObatAZ_Alfabet: "j8ros4",
  ObatAZ_Alfabet_Obat: "lytet7",
  Obat_Infodetail: "ydrd9f",
  Obat_related: "e71e6a",
  Obat_Search: "7fwdaf",
  Obat_Banner: "pl8lde", // not used
  ObatAZ_Detail_BackButton: "vwticy",
  ObatAZ_Detail_Link: "90it7b",

  // Tambahan 10 Okt 2019
  Explore_ReqJournal_City: "ufaacg",
  Explore_ReqJournal_Title: "l33xfl",
  Explore_ReqJournal_Submit: "4j7f7n",

  //PMM Pairing Kelola
  Feeds_Kelola: "sp0ljn",
  Kelola_Search: "5nccj6",
  Kelola_Pasien: "kj7tpg",
  Kelola_Pasien_View: "9eyxx1",
  Kelola_ShowQR: "qxo65d",
  Kelola_Notifikasi: "fvpw34",
  Kelola_Notifikasi_Klik: "6cnyur",
  Kelola_ShowQR_Close: "o0r3bo",
  Kelola_Profile_Report: "55u05u",

  //revamp
  Homepage_Cari_Konten: "xkvhv8",
  Homepage_Kelola: "pwvyw0",
  Homepage_Scan_QR: "p5yd7p",
  Homepage_Notification: "6pgkao",
  Homepage_Lihat_Profil: "cegqn5",

  Homepage_Event: "mft57z",
  Homepage_Webinar: "cjncm7",
  Homepage_CME: "bpvxyf",
  Homepage_Job: "xyg6jk",
  Homepage_Channel: "nqv6pb",
  Homepage_Literatur: "wkrb0k",
  Homepage_ObatAZ: "703xg4",
  Homepage_Polling: "60q36g",

  Homepage_Sliding_Banner: "t5ivoc",

  Homepage_RekomendasiChannel_Lihat_Semua: "7pvmmz",
  Homepage_Rekomendasi_Channel: "k98151",

  Homepage_Home: "26096j",
  Homepage_Feed: "7l8xkk",
  Homepage_Album_P2KB: "xzgqtf",

  Homepage_Popup_TnC_Close: "jlf08c",
  Homepage_Popup_TnC_Submit: "d81gi3",
  Homepage_Popup_TnC_tidak_tertarik: "rox2mp",

  Onboarding_Start: "rsq43a",
  Onboarding_Finish: "fg43ev",

  Onboarding_Select_Country_Start: "ppvai7",
  Onboarding_Select_Country_Finish: "f246ei",

  Kelola_Start: "4450nl",
  Kelola_QR_Pairing_Show: "3my2kk",

  Kelola_Search_Start: "1yyq52",
  Kelola_Search_View_Pasien: "iwt8j0",

  Scan_QR_Start: "sivuki",
  Scan_QR_Finish: "2oc0be",

  Profile_Lihat_Profil_Saya: "d8cqot",
  Profile_Syarat_Ketentuan: "lo9pms",
  Profile_Kebijakan_Privasi: "h7b0ng",
  Profile_Kontak_Kami: "d8tv9h",
  Profile_Beri_Rating_Review: "f23fs8",
  Profile_Keluar: "l6fpob",

  Profile_Lihat_Profil_Saya_Edit_Profil_Medical: "drj61m",
  Profile_Lihat_Profil_Saya_Edit_Informasi_Akun: "ukv5fr",

  Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Start: "odwf18",
  Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Ubah_Foto: "bz9dig",
  Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Nama_Lengkap: "obcwrb",
  Profile_Lihat_Profil_Saya_Edit_Profil_Medical_ID: "faf2gu",
  Profile_Lihat_Profil_Saya_Edit_Profil_Medical_Finish: "a68zhs",

  Notification_Select: "1ff1h0",

  Event_Open_Website: "44a483",
  Event_Open_Announcement: "dayava",
  Event_Open_Contact_Us: "kzwcmx",
  Event_Open_Materi: "tu0skd",
  Event_Open_Certificate: "2apq3t",

  //forum - channel yg diganti forum
  Forum_Start: "mvg4sr",
  Forum_Saya_Open: "q3gujg",
  Forum_Cari: "it8ht0",
  Forum_Masuk: "ppdp9s",
  Forum_Banner: "nx1zdm",
  Forum_Gabung_Start: "knzvma",
  Forum_Gabung_Finish: "ezd0na",

  // forum detail
  Forum_Tentang: "fhlfa5",
  Forum_Diskusi: "56dp0h",
  Forum_Konferensi: "mr1b7m",
  Forum_Request_Jurnal: "irv6f9",
  Forum_Job: "cbz9na",
  Forum_Tinggalkan_Forum: "6991or",
  Forum_Tinggalkan_Forum_Batal: "9x92dz",

  Forum_Cari_Start: "2pramq",
  Forum_Cari_Terakhir: "515gpf",

  Forum_Cari_Masuk_Forum: "reyup7",

  //forum - diskusi
  Forum_Diskusi_Start: "uz1lgz",
  Forum_Diskusi_Open: "bf87ex",
  Forum_Diskusi_Like: "magf4e",
  Forum_Diskusi_Komentar: "oo98v7",
  Forum_Diskusi_Komentar_Like: "iagscg",
  Forum_Diskusi_Komentar_Reply: "2vatpa",
  Forum_Diskusi_Komentar_Reply_Like: "q84mrv",

  // forum - konferensi
  Forum_Konferensi_Start: "p7xt33",
  Forum_Konferensi_Tanggal: "olui3v",
  Forum_Konferensi_Pilih: "76x3us",

  // forum - request journal
  RequestJurnal_Description: "6qg2j3",

  // forum - job
  Forum_Job_Start: "v55len",
  Forum_Job_Filter: "3cfob2",
  Forum_Job_Tab_Semua: "uv9vjw",
  Forum_Job_Tab_Speaker: "fjihos",
  Forum_Job_Tab_Moderator: "6tb9gv",
  Forum_Job_Open: "cyhgvh",
  Forum_Job_Filter_Start: "sqy57v",
  Forum_Job_Filter_Reset: "tou8ml",
  Forum_Job_Filter_Terapkan: "xeam2w",

  // banner produk - forum
  Forum_Produk_Dkonsul_Registration_Start: "p4ld7c",
  Forum_Produk_Dkonsul_Registration_Register: "uyp4nm",
  Forum_Produk_Dkonsul_Registration_Form_Start: "an1rt8",
  Forum_Produk_Teman_Bumil_Registration_Start: "prw5hz",
  Forum_Produk_Teman_Bumil_Registration__Register: "5s61f7",
  Forum_Produk_Teman_Diabetes_Registration_Start: "3s778p",
  Forum_Produk_Teman_Diabetes_Registration_Register: "v8vt25",
  Forum_Produk_Teman_Diabetes_Registration_Form_Start: "12zses",

  //infinite scroll
  Infinite_Scroll_Onload: "a9b20d",

  //Feed
  Feed_Onload: "tx1fap",
};

AdjustTrackerConfig.PushToken = async () => {
  let token = await firebase.messaging().getToken();
  console.log("FCM Token " + token);
  return token;
};

export const AdjustTracker = (token, content = null, content_id = null) => {
  let user = gueloginAuth.auth().currentUser;
  let adjustEvent = new AdjustEvent(token);
  console.warn(token);
  let memberId = user != null && user.uid != null ? user.uid : null;

  adjustEvent.addCallbackParameter("member_id", memberId);

  //tracking webinar
  // if (!_.isEmpty(content)) {
  adjustEvent.addCallbackParameter("content", content);
  adjustEvent.addCallbackParameter("content_id", content_id);
  // }

  console.log(
    "==================== ADJUST TOKEN ->",
    token,
    " ====== MEMBER ID ->",
    memberId,
    " ====== CONTENT ->",
    content,
    " ====== CONTENT ID ->",
    content_id,
    " ====================="
  );

  Adjust.trackEvent(adjustEvent);
};
