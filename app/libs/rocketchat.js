import { AsyncStorage, InteractionManager } from 'react-native';
import { Rocketchat as RocketchatClient, settings as RocketChatSettings } from '@rocket.chat/sdk';

// import callJitsi from './methods/callJitsi';
// const TOKEN_KEY = 'reactnativemeteor_usertoken';

// RocketChatSettings.customHeaders = headers;
import Config from 'react-native-config'

const RocketChat = {
    // TOKEN_KEY,
    // callJitsi

    subscribeRoom({ rid }, context) {

        let message = {}
        const handleMessageReceived = (ddpMessage => {

            message = ddpMessage.fields.args[0];
            // context.setState(context.state.messages.concat(message));
            //context.roomDetails();
            context.resultUpdateSubscribeRoom(ddpMessage)
            console.log('subscribeRoom ddpMessage: ', ddpMessage);
            return message;
        });

        messageReceivedListener = this.sdk.onStreamData(
            'stream-room-messages',
            handleMessageReceived
        );
        result = this.sdk.subscribeRoom(rid);
        console.log('subscribeRoom result: ', result)

    },

    async sendMessage(channel, text) {
        console.log('sendMessage: ', channel)
        let sent = await this.sdk.post('chat.postMessage', { channel, text });
        console.log('sent', sent);
        return sent;
    },

    async login(username) {
        const staticPassRocketChatApi = 'bhE4V8qHmmg9J9Fv2pv4MCW'

        let params = { username: username, password: staticPassRocketChatApi };

        let resultLogin = await this.sdk.login(params);
    },

    async logout() {
        try {
            // RC 0.60.0
            let logout = await this.sdk.logout();
            console.log('logout rocketchat:', logout)
        } catch (error) {
            console.log('​logout -> api logout -> catch -> error', error);
        }
    },
    async getCurrentLogin(username) {
        const staticPassRocketChatApi = 'bhE4V8qHmmg9J9Fv2pv4MCW'
        await this.login(username, staticPassRocketChatApi);
        const { result } = await this.sdk.currentLogin;

        const user = {
            id: result.userId,
            token: result.authToken,
            username: result.me.username,
            name: result.me.name,
            language: result.me.language,
            status: result.me.status,
            customFields: result.me.customFields,
            emails: result.me.emails,
            roles: result.me.roles
        };
        console.log('user: ', user)

        return user;
    },

    async getRoomDetails(roomName, offset, count) {
        query = `{"t": { "$exists": false }}`

        console.log('count: ', count)
        console.log('offset: ', offset)

        let room = await this.sdk.get(`groups.messages`, { roomName, query, count, offset })
        //  let room = await this.sdk.get(`groups.history`, { roomName, count, offset })

        //console.log('room', room);
        return room;
    },

    async getRooms(userId) {
        let rooms = await this.sdk.get('rooms.get', userId)
        console.log('rooms: ', rooms)
        return rooms;
    },

    async connect() {
        const server = Config.URL_SERVER_ROCKET_CHAT
        // Use useSsl: false only if server url starts with http://
        const useSsl = !/http:\/\//.test(server);
        console.log('server: ', server)
        this.sdk = new RocketchatClient({ host: server, protocol: 'ddp', useSsl });

        let isConnected = false;

        await this.sdk.connect().then((value) => {
            isConnected = value.connected;
            return isConnected;
        })
            .catch((err) => {
                isConnected = false;
                console.log('connect error', err);
                // when `connect` raises an error, we try again in 10 seconds
                // this.connectTimeout = setTimeout(() => {
                //     this.connect({ server, user });
                // }, 10000);

            });
        return isConnected;

    }

};

export default RocketChat;
