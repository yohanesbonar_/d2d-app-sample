import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Col, Right, Row, Button } from 'native-base'
import { TouchableOpacity } from 'react-native';
export default class CoachmarkContent extends Component {

  _pressButtonPositive = () => {
    this.props.onPressButtonPositive()
  };

  _pressButtonNegative = () => {
    this.props.onPressButtonNegative()
  };

  _renderDot = () => {
    let itemsDot = [];
    let dotActivePosition = this.props.dotActivePosition

    for (let index = 1; index <= this.props.dotLength; index++) {
      itemsDot.push(
        <View
          key={index}
          style={{
            height: 8,
            width: 8,
            borderRadius: 4,
            backgroundColor: dotActivePosition == index ? "#454F63" : "#E4E6E9",
            marginRight: 8
          }}>

        </View>
      )
    }

    return itemsDot
  }

  renderButton = () => {
    return (
      <Row>
        {this.props.buttonTextNegative != null && (
          <TouchableOpacity
            style={{
              backgroundColor: "#F6F6F7",
              justifyContent: 'center',
              paddingHorizontal: 24,
              borderRadius: 8,
              height: 48,
              marginRight: 16
            }} onPress={() => this._pressButtonNegative()} >
            <Text style={{ fontFamily: 'Nunito-Bold', color: "#1E1E20", fontSize: 16, lineHeight: 26 }}>{this.props.buttonTextNegative}</Text>
          </TouchableOpacity>

        )}
        <TouchableOpacity
          style={{
            backgroundColor: "#D01E53",
            justifyContent: 'center',
            paddingHorizontal: 24,
            borderRadius: 8,
            height: 48,
          }} onPress={() => this._pressButtonPositive()} >
          <Text style={{ fontFamily: 'Nunito-Bold', color: "#FFFFFF", fontSize: 16, lineHeight: 26 }}>{this.props.buttonTextPositive}</Text>
        </TouchableOpacity>
      </Row>
    )
  }

  render() {

    return (
      <View style={styles.container}>
        <Col style={{
          margin: 16
        }}>
          <Text style={{
            fontSize: 20,
            fontFamily: "Nunito-Bold",
            color: "#1E201F"
          }}>{this.props.title}</Text>
          <Text style={{
            fontSize: 16,
            fontFamily: "Nunito-Regular",
            color: "#1E201F"
          }}>{this.props.message}</Text>

          <View style={{
            flex: 1
          }}>
            <Row style={{
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: 16
            }}>
              <Row
                style={{
                  justifyContent: "flex-start"
                }}
              >
                {this._renderDot()}
              </Row>

              <Right>
                {this.renderButton()}
              </Right>
            </Row>
          </View>
        </Col>

      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    marginHorizontal: 16,
    backgroundColor: '#FFF',
    flex: 1,
  },
  message: {
    paddingHorizontal: 8,
    paddingVertical: 16,
    flex: 1,
  },
  messageText: {
    fontSize: 14,
    lineHeight: 22,
    letterSpacing: -0.15,
  },
  button: {
    width: 48,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(246,246,246)',
  },
  buttonText: {
    fontSize: 14,
    lineHeight: 22,
    color: 'rgb(7, 112, 205)',
    fontWeight: 'bold',
  },
});
