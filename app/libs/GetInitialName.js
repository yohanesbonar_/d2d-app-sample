export const getInitialName = (name, defaultVal = 'NA') => {
    if (!name) return '';
    if (name.trim() === '') return defaultVal;
    const split = name.split(' ');
    const initialName = split[0][0] + (split[1] ? split[1][0] : '');
    return initialName.toUpperCase();
};