import { Platform } from 'react-native';
import Config from 'react-native-config';
import firebase from 'react-native-firebase';

const iosConfig = {
    clientId: '127784416281-4dik7n346f48opmidta6ueh8k7n42fvd.apps.googleusercontent.com',
    appId: '1:434697670779:ios:4066eed5cff1a069',
    databaseURL: 'x',
    storageBucket: 'x',
    messagingSenderId: 'x',
    projectId: 'gue-login',

    // enable persistence by adding the below flag
    persistence: true,
};

const iosConfigProd = {
    ...iosConfig,
    apiKey: 'AIzaSyBxM-dtG8Da222-8D2K03lZWLMNPtyq0u0'
};

const iosConfigStaging = {
    ...iosConfig,
    apiKey: 'AIzaSyBxM-dtG8Da222-8D2K03lZWLMNPtyq0u0'
};

const androidConfig = {
    clientId: '127784416281-epnvn2n12te5amjekdoeki5rbn4vkc4m.apps.googleusercontent.com',
    appId: '1:127784416281:android:feace3a7c1886f1a',
    databaseURL: 'x',
    storageBucket: 'x',
    messagingSenderId: 'x',
    projectId: 'gue-login',

    // enable persistence by adding the below flag
    persistence: true,
};

// pluck values from your `google-services.json` file you created on the firebase console
const androidConfigProd = {
    ...androidConfig,
    apiKey: 'AIzaSyAx9Dv2gOk-lBB-zhTd4aTkvPmkVkd21MU'
};

const androidConfigStaging = {
    ...androidConfig,
    apiKey: 'AIzaSyAx9Dv2gOk-lBB-zhTd4aTkvPmkVkd21MU'
};

// Managing Environment firebase
const managingEnv = () => {
    switch (Config.ENV) {
        case "production":
            return firebase.initializeApp(
                // use platform specific firebase config
                Platform.OS === "ios" ? iosConfigProd : androidConfigProd,
                // name of this app
                "guelogin"
            );
        case "staging":
            return firebase.initializeApp(
                // use platform specific firebase config
                Platform.OS === 'ios' ? iosConfigStaging : androidConfigStaging,
                // name of this app
                "guelogin"
            );
        default:
            break;
    }
};

export const guelogin = managingEnv();