import { I18nManager } from "react-native";
import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize"; // Use for caching/memoize for better performance
import AsyncStorage from "@react-native-community/async-storage";
import { STORAGE_TABLE_NAME } from '../../libs/Common'
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require("./en.json"),
  id: () => require("./id.json")
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

const setI18nConfig = async () => {
  // fallback if no available language fits
  //const fallback = { languageTag: "en", isRTL: false };


  // const { languageTag, isRTL } =
  //   RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
  //   fallback;

  let languageTag = "en"

  AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE).then(value => {
    if (value != null) {
      value = JSON.parse(value)
      if (value.country_code == "ID" || value.country_code == null) {
        languageTag = "id";
      }
      else {
        languageTag = "en";
      }
    }


    console.log('value: ', value.country_code)
    console.log('languageTag: ', languageTag)
    // clear translation cache
    translate.cache.clear();
    // update layout direction
    //I18nManager.forceRTL(isRTL);
    // set i18n-js config
    i18n.translations = { [languageTag]: translationGetters[languageTag]() };
    i18n.locale = languageTag;
  });
};


export { setI18nConfig, translate };