import {
  Linking,
  Platform,
  Share,
  PermissionsAndroid,
  Alert,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { NavigationActions } from "react-navigation";
import RNFetchBlob from "rn-fetch-blob";
import firebase from "react-native-firebase";
import { Toast } from "native-base";
import DeviceInfo from "react-native-device-info";
import _ from "lodash";
import moment from "moment-timezone";

import Config from "react-native-config";
const gueloginAuth = firebase.app("guelogin");

export function convertMonth(month) {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  return monthNames[month - 1];
}

export async function subscribeFcm() {
  let subscribeFcm =
    Config.ENV == "production" ? "all_users" : "all_users_development";

  let country_code = await getCountryCodeProfile();
  if (country_code != null && country_code != "ID") {
    subscribeFcm = subscribeFcm + "_" + country_code;
  }
  return subscribeFcm;
}

export const subscribeFcmSubscription =
  Config.ENV == "production" ? "specialist_" : "dev_specialist_";
export const subscribeFcmWebinarId =
  Config.ENV == "production" ? "webinar_" : "dev_webinar_";
export const prefix_env_topic = Config.ENV == "production" ? "" : "dev_";

export var getCountryCodeProfile = async () => {
  let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
  let country_code = JSON.parse(profile).country_code;

  return country_code;
};

//--- check FCM topics ---//
export const checkTopicsBySubscription = async (subscriptions) => {
  let localFcmTopics = await AsyncStorage.getItem(
    STORAGE_TABLE_NAME.FCM_TOPICS_SUBSCRIBTIONS
  );
  if (localFcmTopics != null) {
    //unsubscibre topics
    let fcmTopics = JSON.parse(localFcmTopics);
    if (fcmTopics != null) {
      if (fcmTopics.subscriptions != null) {
        await sendUnsubscribeTopics(fcmTopics.subscriptions, subscriptions);
      }
    }
  }

  if (subscriptions != null && subscriptions.length > 0) {
    await sendSubscribtionsTopics(subscriptions);
  }
};

//--- check FCM topics webinar ---//
export const sendTopicsWebinar = async (profileWebinar) => {
  // unsubscribe
  let localFcmTopics = await AsyncStorage.getItem(
    STORAGE_TABLE_NAME.FCM_TOPICS_WEBINAR
  );
  if (localFcmTopics != null) {
    //unsubscibre topics
    let fcmTopics = JSON.parse(localFcmTopics);
    if (fcmTopics != null && fcmTopics.length > 0) {
      for (let i = 0; i < fcmTopics.length; i++) {
        await firebase.messaging().unsubscribeFromTopic(fcmTopics[i]);
      }
    }
  }

  //subscribe
  let topicsFcmWebinars = [];
  let country_code = await getCountryCodeProfile();
  if (profileWebinar != null && profileWebinar.length > 0) {
    for (let i = 0; i < profileWebinar.length; i++) {
      if (country_code != null && country_code != "ID") {
        topicsFcmWebinars.push(
          subscribeFcmWebinarId + profileWebinar[i] + "_" + country_code
        );
        await firebase
          .messaging()
          .subscribeToTopic(
            subscribeFcmWebinarId + profileWebinar[i] + "_" + country_code
          );
      } else {
        topicsFcmWebinars.push(subscribeFcmWebinarId + profileWebinar[i]);
        await firebase
          .messaging()
          .subscribeToTopic(subscribeFcmWebinarId + profileWebinar[i]);
      }
    }
  }
  let jsonSave = JSON.stringify(topicsFcmWebinars);
  let save = await AsyncStorage.setItem(
    STORAGE_TABLE_NAME.FCM_TOPICS_WEBINAR,
    jsonSave
  );
};

export const sendSubscribtionsTopics = async (subscriptions) => {
  let dataSubs = [];
  let country_code = await getCountryCodeProfile();
  for (let i = 0; i < subscriptions.length; i++) {
    dataSubs.push(subscriptions[i].id);
    if (country_code != null && country_code != "ID") {
      await firebase
        .messaging()
        .subscribeToTopic(
          subscribeFcmSubscription + subscriptions[i].id + "_" + country_code
        );
    } else {
      await firebase
        .messaging()
        .subscribeToTopic(subscribeFcmSubscription + subscriptions[i].id);
    }
  }

  //save to local
  if (dataSubs != null && dataSubs.length > 0) {
    let saveFcmTopicsSubs = {
      subscriptions: dataSubs,
    };
    let jsonSave = JSON.stringify(saveFcmTopicsSubs);
    let save = await AsyncStorage.setItem(
      STORAGE_TABLE_NAME.FCM_TOPICS_SUBSCRIBTIONS,
      jsonSave
    );
    let needClear = await AsyncStorage.setItem(
      STORAGE_TABLE_NAME.IS_EMPTY_FCM_TOPICS,
      "false"
    );
  }
};

export const sendUnsubscribeTopics = async (oldTopics, currentTopics) => {
  if (oldTopics != null) {
    for (let i = 0; i < oldTopics.length; i++) {
      let isDeleted = true;
      if (currentTopics != null) {
        currentTopics.forEach((current) => {
          if (oldTopics[i] == current.id) {
            isDeleted = false;
          }
        });
      }

      if (isDeleted == true) {
        await clearSubscribeFcm(oldTopics[i]);
      }
    }
  }
};

export const clearSubscribeFcm = async (id) => {
  let country_code = await getCountryCodeProfile();
  if (country_code != null && country_code != "ID") {
    await firebase
      .messaging()
      .unsubscribeFromTopic(subscribeFcmSubscription + id + "_" + country_code);
  } else {
    await firebase
      .messaging()
      .unsubscribeFromTopic(subscribeFcmSubscription + id);
  }
};

//--- end of check FCM topics ---//

//--- split url ---//

export const doSplitUrl = (url) => {
  let result = null;

  if (url != null) {
    let replaceUrl = url.replace("http://d2d.co.id/?", "");
    replaceUrl = url.replace("https://d2d.co.id/?", "");
    let splitUrl = replaceUrl.split("&");
    if (splitUrl != null && splitUrl.length == 3) {
      let category = splitUrl[0].replace("type=", "");
      let slug = splitUrl[1].replace("title=", "");
      let hash = splitUrl[2].replace("hash=", "");

      result = {
        category: category,
        slug: slug,
        hash: hash,
      };
    }
  }

  return result;
};

//--- end of split url --//

export var STORAGE_TABLE_NAME = {
  UID: "UID",
  PROFILE: "PROFILE",
  IS_LOGIN: "IS_LOGIN",
  IS_REGISTER: "IS_REGISTER",
  AUTHORIZATION: "AUTHORIZATION",
  QUIZ_ANSWER: "QUIZ_ANSWER",
  FCM_TOKEN: "FCM_TOKEN",
  FCM_TOPICS_SUBSCRIBTIONS: "FCM_TOPIC_SUBSCRIBTIONS",
  FCM_TOPICS_WEBINAR: "FCM_TOPIC_WEBINAR",
  IS_EMPTY_FCM_TOPICS: "IS_EMPTY_FCM_TOPICS",
  IS_UPDATE_PROFILE: "IS_UPDATE_PROFILE",
  FILTER_WEBINAR: "FILTER_WEBINAR",
  PROGRESS_TIME_PREPOSTTEST: "PROGRESS_TIME_PREPOSTTEST",
  FCM_TOPICS_WEBINAR_SUBMIT: "FCM_TOPIC_WEBINAR_SUBMIT",
  SUBMIT_CME_QUIZ: "SUBMIT_CME_QUIZ",
  HISTORY_SEARCH_WEBINAR: "HISTORY_SEARCH_WEBINAR",
  HISTORY_SEARCH_EVENT: "HISTORY_SEARCH_EVENT",
  HISTORY_SEARCH_CONTENT: "HISTORY_SEARCH_CONTENT",
  HISTORY_SEARCH_ALL_CONTENT: "HISTORY_SEARCH_ALL_CONTENT",
  HISTORY_SEARCH_ALL_CHANNEL: "HISTORY_SEARCH_ALL_CHANNEL",
  HISTORY_SEARCH_MY_REC_CHANNEL: "HISTORY_SEARCH_MY_REC_CHANNEL",

  COACHMARK_EVENT_CS_REGISTER: "COACHMARK_EVENT_CS_REGISTER",
  COACHMARK_EVENT_JOIN: "COACHMARK_EVENT_JOIN",
  COACHMARK_EVENT_PAY_NOW: "COACHMARK_EVENT_PAY_NOW",
  COACHMARK_EVENT_REPLAY: "COACHMARK_EVENT_REPLAY",
  BOTTOMSHEET_EVENT_HAS_ENDED: "BOTTOMSHEET_EVENT_ENDED",
  COACHMARK_WEBINAR_FLOATING: "COACHMARK_WEBINAR_FLOATING",

  COUNTRY: "COUNTRY",
};

export var EnumFeedsCategory = {
  PDF_JOURNAL: "pdf_journal",
  PDF_GUIDELINE: "pdf_guideline",
  PDF_KNOWLEDGE: "pdf_knowledge", //e-det
  PDF_LITERATURE: "pdf_literature", //e-det
  PDF_PRODUCT_GROUP: "pdf_product_group", //e-det
  PDF_BROCHURE: "pdf_brochure", //e-det
  PDF_MATERI: "pdf_materi", //e-det
  VIDEO_MATERI: "video_materi",
  VIDEO_LEARNING: "video_learning",
  VIDEO_KNOWLEDGE: "video_knowledge", //e-det
  VIDEO_PRODUCT_GROUP: "video_product_group", //e-det
  VIDEO_BROCHURE: "video_brochure", //e-det
  VIDEO_GENERAL: "video_general", //e-det
  EVENT: "event",
  WEBINAR: "webinar",
  UID: "uid",
  CME_CERTIFICATE: "cme_certificate",
  PROFILE_NOTIFICATION: "profile_notification",
  CME: "cme",
  EVENT_LIST: "event_list",
  EVENT: "event",
  REQ_JOURNAL: "req_journal", //for enum notif
  JOURNAL: "journal", //for enum notif
  PMM_PATIENT: "pmm_patient",
  CME_LIST: "cme_list",
  CME_DETAIL: "cme_detail",
  ATOZ_LIST: "atoz_list",
  ATOZ_DETAIL: "atoz_detail",
  FEEDS: "feeds",

  CERTIFICATE_CME: "certificate_cme",
  CERTIFICATE_OFFLINE: "certificate_offline",
  CERTIFICATE_EVENT: "certificate_event",
  CERTIFICATE_WEBINAR: "certificate_webinar",
};

export function convertShownDate(eventStartDate, eventEndDate) {
  const startDate = eventStartDate.substring(8, 10);
  const startMonth = eventStartDate.substring(5, 7);
  const startYear = eventStartDate.substring(0, 4);

  const endDate = eventEndDate.substring(8, 10);
  const endMonth = eventEndDate.substring(5, 7);
  const endYear = eventEndDate.substring(0, 4);

  const isSameYear = startYear == endYear ? true : false;
  const isSameMonth = startMonth == endMonth && isSameYear ? true : false;

  const shownStartDate =
    startDate +
    (isSameMonth == true ? "" : " " + convertMonth(startMonth)) +
    (isSameYear == true ? "" : startYear);
  const shownEndDate = endDate + " " + convertMonth(endMonth) + " " + endYear;

  return shownStartDate + " - " + shownEndDate;
}

export const sendByEmailApp = async (type) => {
  let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
  let emailUserD2D = JSON.parse(profile).email;
  const newline = `<br>`;

  if (type == "changeCountry") {
    let destination = "cs@d2d.co.id";
    let subject = `Request Change Country ${emailUserD2D}`;
    let body = `My registered email on my D2D account is ${emailUserD2D} ${newline}Request Change Country to (Which country?)`;

    Linking.openURL(`mailto:${destination}?subject=${subject}&body=${body}`);
  }
};

export const openOtherApp = async (url, _params) => {
  let params = {
    appName: _params.appName ? _params.appName : null,
    appStoreId: _params.appStoreId ? _params.appStoreId : null,
    appStoreLocale: _params.appStoreLocale ? _params.appStoreLocale : null,
    playStoreId: _params.playStoreId ? _params.playStoreId : null,
  };
  Linking.openURL(url).catch((err) => {
    if (err.code === "EUNSPECIFIED") {
      if (Platform.OS === "ios") {
        // check if appStoreLocale is set
        const locale =
          typeof params.appStoreLocale === "undefined"
            ? "us"
            : params.appStoreLocale;

        Linking.openURL(
          `https://itunes.apple.com/${locale}/app/${params.appStoreId}`
        );
      } else {
        Linking.openURL(
          `https://play.google.com/store/apps/details?id=${params.playStoreId}`
        );
      }
    } else {
      throw new Error(`Could not open ${params.appName}. ${err.toString()}`);
    }
  });
};

export const downloadFile = async (url, isPreventOpenDocument) => {
  console.log(
    "downloadFile DeviceInfo.getSystemVersion(): ",
    DeviceInfo.getSystemVersion()
  );
  let isSuccesDownload = false;
  if (url == null || url == "") {
    Toast.show({
      text: `File not found`,
      position: "bottom",
      duration: 3000,
      type: "sucess",
    });
    return isSuccesDownload;
  }

  try {
    let allowedAccess = true;
    if (Platform.OS == "android") {
      allowedAccess = false;
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "D2D Permission",
          message: "D2D needs access to your storage for download some files.",
        }
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED || granted == true) {
        allowedAccess = true;
      }
    }
    const locationAndroid =
      DeviceInfo.getSystemVersion() != 10
        ? "/storage/emulated/0/D2D"
        : RNFetchBlob.fs.dirs.DownloadDir + "/D2D";
    const DownloadDir =
      Platform.OS == "ios" ? RNFetchBlob.fs.dirs.DocumentDir : locationAndroid;
    const filename = getFilenameUrl(url);

    if (isPreventOpenDocument != true) {
      Toast.show({
        text: `Downloading!`,
        position: "bottom",
        duration: 3000,
        type: "sucess",
      });
    }

    if (allowedAccess === true) {
      let pdfLocation = `${DownloadDir}`;
      const checkDir = await RNFetchBlob.fs.isDir(`${DownloadDir}`);
      if (!checkDir) {
        const dir = await RNFetchBlob.fs.mkdir(`${DownloadDir}`);
        pdfLocation = `${DownloadDir}`;
      }

      try {
        const res = await RNFetchBlob.config({
          fileCache: true,
          path: pdfLocation + "/" + filename,
          addAndroidDownloads: {
            useDownloadManager: true,
            mediaScannable: true,
            notification: true,
            // mime : 'application/pdf',
            title: filename,
            description: "Announcement file",
            path: pdfLocation + "/" + filename,
          },
        })
          .fetch("GET", encodeURI(url), { "Cache-Control": "no-store" })

          .then((res) => {
            if (res.path()) {
              if (isPreventOpenDocument != true) {
                RNFetchBlob.ios.openDocument(pdfLocation + "/" + filename);
              }
              isSuccesDownload = true;
              if (isPreventOpenDocument != true) {
                Toast.show({
                  text: `File downloaded!`,
                  position: "bottom",
                  duration: 3000,
                  type: "success",
                });
              }
            }
          });
      } catch (error) {
        isSuccesDownload = false;
        console.log(error);
      }

      // if (res.path()) {
      //     RNFetchBlob.ios.openDocument((Platform.OS == 'ios' ? '' : pdfLocation)  + '/' + filename);
      //     Toast.show({text: `File berhasil diunduh!`, position:'bottom', duration: 3000, type: 'success'});
      // }
    } else {
      isSuccesDownload = false;
      Toast.show({
        text: `D2D permission access storage denied!`,
        position: "bottom",
        duration: 3000,
        type: "danger",
      });
    }
  } catch (error) {
    isSuccesDownload = false;
    Toast.show({
      text: `Download file failed!`,
      position: "bottom",
      duration: 3000,
      type: "danger",
    });
  }
  return isSuccesDownload;
};

export const getFilenameUrl = (url) => {
  const regex = /([\w\d_-]*)\.?[^\\\/]*$/;
  const mathces = regex.exec(url);
  const filename = mathces[0] ? mathces[0] : url;

  return filename;
};

export const share = (message) => {
  Share.share({ message: message }, { dialogTitle: "Share to" })
    .then((res) => console.log(res))
    .catch((error) => console.log(error));
};

export const validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const validatePhoneNumber = (phone) => {
  var re = /^((\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
  return re.test(String(phone).toLowerCase());
};

export const validateFullName = (fullName) => {
  var regexEmoji = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
  return regexEmoji.test(fullName);
};

export const validateNpaIDI = (npaIDI) => {
  let regexOnlyNumber = /^[0-9]*$/gm;
  return regexOnlyNumber.test(npaIDI);
};

export const removeEmojis = (string) => {
  // emoji regex from the emoji-regex library
  const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g;

  return string.replace(regex, "");
};

export const customCss = `
    * { font-family: 'Nunito'; font-size: 13px; line-height: 20px; color: #1E1E20; zoom: reset; }
    p { text-align: left; color: #1E1E20; font-size: 13px; }
    a { color: #0770CD }
    #wrapper {  }
`;

export const escapeHtml = (str, style) => {
  if (typeof str == "string") {
    str = str.replace(/&gt;/gi, ">");
    str = str.replace(/&lt;/gi, "<");
    str = str.replace(/&#039;/g, "'");
    str = str.replace(/&quot;/gi, '"');
    str = str.replace(/&amp;/gi, "&"); /* must do &amp; last */
  }

  let html = '<div id="wrapper">' + str + "</div>";

  return html;
};

export const substringHtml = (str, len) => {
  var result = str.substr(0, len),
    lastOpening = result.lastIndexOf("<"),
    lastClosing = result.lastIndexOf(">");

  if (lastOpening !== -1 && (lastClosing === -1 || lastClosing < lastOpening)) {
    result += str.substring(len, str.indexOf(">", len) + 1);
  }

  return `${result}...`;
};

export function testID(id) {
  return Platform.OS === "android"
    ? { accessible: true, accessibilityLabel: id }
    : { accessibilityLabel: id };
}

export var NAV_CARD_EVENT = {
  FEEDS: "FEEDS",
  PROFILE: "PROFILE",
};

export const checkNpaIdiEmpty = (profile) => {
  let isNpaIdiEmpty = false;
  let dataProfile = JSON.parse(profile);
  if (!_.isEmpty(dataProfile)) {
    if (_.isEmpty(dataProfile.npa_idi)) {
      //this.showAlertCompleteProfile(keyRoute);
      isNpaIdiEmpty = true;
    }
  }

  return isNpaIdiEmpty;
};

export const sendTopicCertificate = async (type, slug_hash) => {
  let country_code = await getCountryCodeProfile();
  let topic = "";
  if (country_code != null && country_code != "ID") {
    topic = `${prefix_env_topic}certificate_${type}_${slug_hash}_${country_code}`;
  } else {
    topic = `${prefix_env_topic}certificate_${type}_${slug_hash}`;
  }
  console.log("topic: ", topic);
  const sendTopic = await firebase.messaging().subscribeToTopic(topic);
};

export const sendTopicCertificateWebinarSubmit = async (slug_hash) => {
  let country_code = await getCountryCodeProfile();
  let topic = "";
  if (country_code != null && country_code != "ID") {
    topic = `${prefix_env_topic}webinar_submit_${slug_hash}_${country_code}`;
  } else {
    topic = `${prefix_env_topic}webinar_submit_${slug_hash}`;
  }
  try {
    await firebase.messaging().subscribeToTopic(topic);
    await saveTopicWebinarSubmit(topic);
    console.log("sendTopicCertificateWebinarSubmit topic: ", topic);
  } catch (e) {
    console.log(e);
  }
};

export const sendTopicWebinar = async (slug_hash) => {
  let country_code = await getCountryCodeProfile();
  let topic = "";
  if (country_code != null && country_code != "ID") {
    topic = `${prefix_env_topic}webinar_${slug_hash}_${country_code}`;
  } else {
    topic = `${prefix_env_topic}webinar_${slug_hash}`;
  }
  console.log("topic: ", topic);
  firebase.messaging().subscribeToTopic(topic);
};

export const isToday = (date) => {
  date = new Date(date);
  console.log(date);
  let today = new Date();

  return (
    date.getDate() === today.getDate() &&
    date.getMonth() + 1 === today.getMonth() + 1 &&
    date.getFullYear() === today.getFullYear()
  );
};

export const isShowInfoAvailable = (serverDate, endDate) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";

  let isStarted = moment(serverDate, formatDate).isSameOrBefore(
    moment(endDate, formatDate)
  );

  return isStarted;
};

export const isUpcoming = (serverDate, startDate) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";

  let isUpcoming = moment(serverDate, formatDate).isBefore(
    moment(startDate, formatDate)
  );
  console.log("isUpcoming: ", isUpcoming);

  return isUpcoming;
};

export const extractTime = (date) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";
  let formatTime = "HH:mm";
  var extractedTime = moment(date, formatDate).format(formatTime);
  extractedTime = moment(extractedTime, formatTime);

  return extractedTime;
};

export const isUpcomingTime = (serverDate, startDate) => {
  let serverTime = extractTime(serverDate);
  let startTime = extractTime(startDate);

  let isUpcomingTime = serverTime.isBefore(startTime);
  return isUpcomingTime;
};

export const isFinishedTime = (serverDate, endDate) => {
  let serverTime = extractTime(serverDate);
  let endTime = extractTime(endDate);

  let isFinishedTime = serverTime.isAfter(endTime);

  return isFinishedTime;
};

export const isFinished = (serverDate, endDate) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";

  let isFinished = moment(serverDate, formatDate).isAfter(
    moment(endDate, formatDate)
  );

  return isFinished;
};

export const isAvailable = (serverDate, startDate, endDate) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";

  let isStarted =
    moment(serverDate, formatDate).isSameOrAfter(
      moment(startDate, formatDate)
    ) &&
    moment(serverDate, formatDate).isSameOrBefore(moment(endDate, formatDate));

  return isStarted;
};

export const isStarted = (serverDate, startDate) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";

  let isStarted = moment(serverDate, formatDate).isSameOrAfter(
    moment(startDate, formatDate)
  );

  return isStarted;
};

saveTopicWebinarSubmit = async (topic) => {
  let dataTopicWebinarSubmit = await AsyncStorage.getItem(
    STORAGE_TABLE_NAME.FCM_TOPICS_WEBINAR_SUBMIT
  );
  dataTopicWebinarSubmit = JSON.parse(dataTopicWebinarSubmit);
  console.log("dataTopicWebinarSubmit: ", dataTopicWebinarSubmit);

  if (dataTopicWebinarSubmit == null || _.isEmpty(dataTopicWebinarSubmit)) {
    dataTopicWebinarSubmit = [];
  } else {
    dataTopicWebinarSubmit = dataTopicWebinarSubmit.topicWebinarSubmit;
  }

  //prevent duplicate topic
  if (dataTopicWebinarSubmit.includes(topic) === false) {
    dataTopicWebinarSubmit.push(topic);
  }

  console.log("dataTopicWebinarSubmit: ", dataTopicWebinarSubmit);

  let topicWebinarSubmit = {
    topicWebinarSubmit: dataTopicWebinarSubmit,
  };
  console.log("topicWebinarSubmit: ", topicWebinarSubmit);

  topicWebinarSubmit = JSON.stringify(topicWebinarSubmit);
  await AsyncStorage.setItem(
    STORAGE_TABLE_NAME.FCM_TOPICS_WEBINAR_SUBMIT,
    topicWebinarSubmit
  );
  console.log("topicWebinarSubmit: ", topicWebinarSubmit);
};

export const unsubscribeTopicWebinarSubmit = async () => {
  dataTopicWebinarSubmit = await AsyncStorage.getItem(
    STORAGE_TABLE_NAME.FCM_TOPICS_WEBINAR_SUBMIT
  );
  dataTopicWebinarSubmit = JSON.parse(dataTopicWebinarSubmit);
  console.log("dataTopicWebinarSubmit: ", dataTopicWebinarSubmit);
  if (dataTopicWebinarSubmit == null || _.isEmpty(dataTopicWebinarSubmit)) {
    dataTopicWebinarSubmit = [];
  } else {
    dataTopicWebinarSubmit = dataTopicWebinarSubmit.topicWebinarSubmit;
  }

  dataTopicWebinarSubmit.map(async (topic, i) => {
    console.log("topic: ", topic);
    await firebase.messaging().unsubscribeFromTopic(topic);
  });
};

export var EnumTypeHistorySearch = {
  WEBINAR: "WEBINAR",
  EVENT: "EVENT",
  CONTENT: "CONTENT",
  ALL_CONTENT: "ALL_CONTENT",
  ALL_CHANNEL: "ALL_CHANNEL",
  MY_REC_CHANNEL: "MY_REC_CHANNEL",
};

export const saveHistorySearch = async (type, data) => {
  let dataSearchToSave = [];
  let existData = [];
  let existDataLowerCase = [];
  let keyAsyncStorage = "";

  if (type == EnumTypeHistorySearch.WEBINAR) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_WEBINAR;
  } else if (type == EnumTypeHistorySearch.EVENT) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_EVENT;
  } else if (type == EnumTypeHistorySearch.CONTENT) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT;
  } else if (type == EnumTypeHistorySearch.ALL_CONTENT) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CONTENT;
  } else if (type == EnumTypeHistorySearch.ALL_CHANNEL) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL;
  } else if (type == EnumTypeHistorySearch.MY_REC_CHANNEL) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL;
  }

  existData = await AsyncStorage.getItem(keyAsyncStorage);

  existData =
    JSON.parse(existData) != null ? JSON.parse(existData).historySearch : [];

  for (var i = 0; i < existData.length; i++) {
    existDataLowerCase.push(existData[i].toLowerCase());
  }

  if (!existDataLowerCase.includes(data.toLowerCase())) {
    dataSearchToSave = existData;

    if (existData.length >= 5) {
      dataSearchToSave.pop();
    }

    dataSearchToSave.unshift(data);

    let dataHistorySearch = {
      historySearch: dataSearchToSave,
    };

    dataHistorySearch = JSON.stringify(dataHistorySearch);
    await AsyncStorage.setItem(keyAsyncStorage, dataHistorySearch);
  } else {
    dataSearchToSave = existData;
    for (var i = 0; i < existDataLowerCase.length; i++) {
      if (existDataLowerCase[i] == data.toLowerCase()) {
        dataSearchToSave.splice(i, 1);
      }
    }
    dataSearchToSave.unshift(data);

    let dataHistorySearch = {
      historySearch: dataSearchToSave,
    };

    dataHistorySearch = JSON.stringify(dataHistorySearch);
    await AsyncStorage.setItem(keyAsyncStorage, dataHistorySearch);
  }
};

export const removeHistorySearch = async (type, position) => {
  let dataSearchToSave = [];
  let existData = [];
  let keyAsyncStorage = "";

  if (type == EnumTypeHistorySearch.WEBINAR) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_WEBINAR;
  } else if (type == EnumTypeHistorySearch.EVENT) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_EVENT;
  } else if (type == EnumTypeHistorySearch.CONTENT) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_CONTENT;
  } else if (type == EnumTypeHistorySearch.ALL_CONTENT) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CONTENT;
  } else if (type == EnumTypeHistorySearch.ALL_CHANNEL) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_ALL_CHANNEL;
  } else if (type == EnumTypeHistorySearch.MY_REC_CHANNEL) {
    keyAsyncStorage = STORAGE_TABLE_NAME.HISTORY_SEARCH_MY_REC_CHANNEL;
  }

  existData = await AsyncStorage.getItem(keyAsyncStorage);

  existData = JSON.parse(existData).historySearch;

  dataSearchToSave = [...existData];
  dataSearchToSave.splice(position, 1);

  let dataHistorySearch = {
    historySearch: dataSearchToSave,
  };

  dataHistorySearch = JSON.stringify(dataHistorySearch);
  await AsyncStorage.setItem(keyAsyncStorage, dataHistorySearch);
};

export const numberCurrencyFormat = (number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",00";
};

export const isExpired = (serverDate, endDate) => {
  let formatDate = "YYYY-MM-DD HH:mm:ss";

  let isExpired = moment(serverDate, formatDate).isAfter(
    moment(endDate, formatDate)
  );

  return isExpired;
};

export const onlyAlphaNumeric = (string) => {
  const regex = /[^a-z0-9]/gi;

  return string.replace(regex, "");
};

export const onlyNumeric = (string) => {
  const regex = /[^0-9]+/g;

  return string.replace(regex, "");
};

export const validatePassword = (password) => {
  let validateStatus = {};
  var regexNumber = /[0-9]/;
  var regexUpperCase = /[A-Z]/;
  var regexLowerCase = /[a-z]/;
  var regexSpecialChar = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/;

  validateStatus.charactersminimum = password.length < 8 ? false : true;
  validateStatus.lowerCase = regexLowerCase.test(password);
  validateStatus.upperCase = regexUpperCase.test(password);
  validateStatus.numbers = regexNumber.test(password);
  validateStatus.specialCharacters = regexSpecialChar.test(password);

  console.log("validateStatus return", validateStatus);
  return validateStatus;
};
