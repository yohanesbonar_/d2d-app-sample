import React from "react";
import {
  View,
  TextInput,
  Image,
  SectionList,
  TouchableOpacity,
  FlatList
} from "react-native";
import styles from "./countrySelectionStyles";
import { countries } from "./Constants";
import { Text } from "native-base";
import platform from "../../../theme/variables/d2dColor";
import { testID } from '../../libs/Common'
import { IconCambodiaFlag, IconIndonesiaFlag, IconMyanmarFlag, IconPhilippinesFlag, IconSingaporeFlag } from "../../../src/assets";

export default class CountrySelection extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      listCountry: countries,
    };
  }

  componentDidMount() {

  }

  _onPressSelectCountry = (item) => {
    this.props.action(item)
    this.props.selected = item.name
  };

  renderItemCountry = ({ item }) => {

    let countryName = item.name

    return (
      <View

        style={[
          styles.itemContainer,
          {
            backgroundColor:
              this.props.selected != null && this.props.selected == item.name
                ? "#F6F6F6"
                : "transparent",
          },
        ]}
      >
        <TouchableOpacity
          {...testID('button_select_country')}
          style={styles.itemTextContainer}
          onPress={() => this._onPressSelectCountry(item)}
        >

          <View style={{
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.45,
          }}>{this.renderFlag(item)}</View>
          <Text semiBold numberOfLines={1} style={styles.itemText}>
            {countryName}
          </Text>
          {this.renderRadioButton(item)}
        </TouchableOpacity>
      </View>
    )
  }

  renderFlag = (data) => {
    let countryName = data.name;

    if (countryName == "Cambodia") {
      return (
        // <Image
        //   resizeMode={"contain"}
        //   source={require("./../../assets/images/flags/Cambodia.png")}
        //   style={styles.imageFlag}
        // />
        <IconCambodiaFlag />
      );
    } else if (countryName == "Myanmar") {
      return (
        <IconMyanmarFlag />
      );
    } else if (countryName == "Philippines") {
      return (
        <IconPhilippinesFlag />
      );
    } else if (countryName == "Singapore") {
      return (
        <IconSingaporeFlag />
      );
    } else if (countryName == "Indonesia") {
      return (
        <IconIndonesiaFlag />
      );
    }
  };

  renderRadioButton = (item) => {

    return (
      <View
        style={[
          styles.circleRadioButton,
          {
            borderColor:
              item.name == this.props.selected
                ? platform.toolbarDefaultBg
                : "#454F63",
          }
        ]}
      >
        {item.name == this.props.selected ? (
          <View
            style={[styles.circleSelected, {
              backgroundColor: platform.toolbarDefaultBg,
            }]}
          />
        ) : null}
      </View>
    );
  };


  render() {
    const { listCountry } = this.state;

    return (
      <View style={[styles.container, {
        marginBottom:
          platform.platform == "ios" &&
            platform.deviceHeight >= 812 &&
            platform.deviceWidth >= 375
            ? 24
            : 16,
      }]}>
        <Text
          bold
          style={{ fontSize: 20, marginVertical: 24 }}
        >
          I am a doctor in...
        </Text>

        <FlatList
          scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
          data={listCountry}
          renderItem={this.renderItemCountry}
          keyExtractor={(item, index) => index.toString()} />
      </View>
    );
  }
}
