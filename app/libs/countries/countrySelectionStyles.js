import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    //marginVertical:16,
    flex: 1,
    justifyContent: "flex-end",
  },
  itemTextContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  itemText: {
    flex: 1,
    marginLeft: 16
  },
  circleSelected: {
    height: 12,
    width: 12,
    borderRadius: 6,
  },
  circleRadioButton: {
    height: 24,
    width: 24,
    borderRadius: 12,
    borderWidth: 2,
    alignItems: "center",
    justifyContent: "center",
  },
  imageFlag: {
    width: 24,
    height: 24,
    marginRight: 12
  },
  itemContainer: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderRadius: 8,
    marginBottom: 16,
  }
});
