import AsyncStorage from '@react-native-community/async-storage';
import { STORAGE_TABLE_NAME } from './Common';
import Api, { errorMessage } from './Api';


var responseBase = (isSuccess, message, data) => {
    return {
        isSuccess: isSuccess,
        message: message,
        data: data,
    };
}

export var ParamPMM = {
    SORT: 'sort',
    KEYWORD: "keyword",
    PAGE: 'page',
    LIMIT: "limit",
    TYPE: "type"
};


export var getListPatients = async (page, limit, sortStatus, keyword, sort) => {


    let result = responseBase(false, 'Something went wrong!', null);

    try {
        let paramRequest = `patient?${ParamPMM.PAGE}=${page}&${ParamPMM.LIMIT}=${limit}&${ParamPMM.SORT}=${sortStatus}&${ParamPMM.KEYWORD}=${keyword}&${ParamPMM.SORT}=${sort}`;

        let response = await Api.get(paramRequest);

        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }


    return result;
}

export var getDetailDataPatient = async (patient_uid) => {
    let result = responseBase(false, 'Something went wrong!', null);

    try {
        let paramRequest = `patient/${patient_uid}`;

        let response = await Api.get(paramRequest);

        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }


    return result;
}

export var getPMMListNotification = async (page, limit, type) => {
    let result = responseBase(false, 'Something went wrong!', null);
    let paramRequest = `notification?${ParamPMM.PAGE}=${page}&${ParamPMM.LIMIT}=${limit}&${ParamPMM.TYPE}=${type}`;

    try {
        let response = await Api.get(paramRequest);
        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }

    return result;
}

export var readNotification = async (dataNotif) => {
    let result = responseBase(false, 'Something went wrong!', null);
    try {

        let params = {
            id: dataNotif.id,
            title: dataNotif.title,
            body: dataNotif.body,
            type: dataNotif.type,
            data: dataNotif.data,
            status: 'read'
        }

        let response = await Api.post(`notification/`, params);

        if (response != null && response.result != null) {
            result = responseBase(true, 'Success', response.result.data);
        }

    } catch (error) {
        result = responseBase(false, error, null);
    }

    return result;
}