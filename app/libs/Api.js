import AsyncStorage from "@react-native-community/async-storage";
import platform from "../../theme/variables/d2dColor";
import firebase from "react-native-firebase";
import guelogin from "./../libs/GueLogin";
import Config from 'react-native-config';
const gueloginAuth = firebase.app("guelogin");


export const BASE_URL_ROCKET_CHAT = Config.BASE_URL_ROCKET_CHAT;

export const BASE_URL = Config.BASE_URL + "v2.1/"; // "development" or "production"

//URL.STAGING;

export default class API {
    static async headers() {
        let idToken = null;
        const currentUser = gueloginAuth.auth().currentUser;

        let dataProfile = await AsyncStorage.getItem("PROFILE")
        let countryCode = null;
        if (dataProfile != null) {
            countryCode = JSON.parse(dataProfile).country_code
        }

        let result = {
            // 'Accept': 'application/json',
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            // 'dataType': 'json',
            // 'X-Requested-With': 'XMLHttpRequest',
            'authorization': idToken,
            'platform': platform.platform,
            'countrycode': countryCode
        };

        try {
            result.authorization = await currentUser.getIdToken(true);
        } catch (error) {

        }

        if (result.authorization != null) {
            let auth = async () => await AsyncStorage.setItem('AUTHORIZATION', result.authorization);
        } else {
            let auth = await AsyncStorage.getItem('AUTHORIZATION');
            if (auth != null) {
                result.authorization = auth;
            }
        }

        return result;
    }


    static async headersRocketChat() {
        let uid = await AsyncStorage.getItem('UID');

        let result = {
            'aid': 'dd',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'uid': uid
        };

        return result;
    }


    static get(route) {
        return this.xhr(route, null, 'GET');
    }

    static put(route, params) {
        return this.xhr(route, params, 'PUT');
    }

    static post(route, params) {
        return this.xhr(route, params, 'POST');
    }

    static postRocketChat(route, params) {
        return this.xhrRocketChat(route, params, 'POST');
    }

    static delete(route, params) {
        return this.xhr(route, params, 'DELETE');
    }

    static async xhrRocketChat(route, params, verb) {
        const host = BASE_URL_ROCKET_CHAT;
        const url = `${host}${route}`;
        const headers = await this.headersRocketChat();

        var formBody = [];
        if (params) {
            for (var property in params) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(params[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");
        }

        let response = {};
        let options = Object.assign({ method: verb }, params ? { body: formBody } : {}, { headers: headers });
        console.log(url)
        console.log(headers)
        console.log(options)
        return fetch(url, options).then(response => {
            if (response.ok && response.status == 200) {
                let json = response.json();
                console.log(json)
                return json;

                return json.then(error => {
                    console.log(error);
                    throw error
                });
            } else {
                return this.networkError(response.status);
            }

        }).catch(error => {
            console.log(error)
            return this.networkError(0);
        })
    }

    static async xhr(route, params, verb) {
        const host = BASE_URL;
        const url = `${host}${route}`;
        const headers = await this.headers();

        var formBody = [];
        if (params) {
            for (var property in params) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(params[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");
        }

        let response = {};
        let options = Object.assign({ method: verb }, params ? { body: formBody } : {}, { headers: headers });
        console.log(url)
        console.log(headers)
        console.log(options)
        return fetch(url, options).then(response => {
            if (response.ok && response.status == 200) {
                let json = response.json();
                console.log(json)
                return json;

                return json.then(error => {
                    console.log(error);
                    throw error
                });
            } else {
                return this.networkError(response.status);
            }

        }).catch(error => {
            console.log(error)
            return this.networkError(0);
        })
    }

    static async xhrCustom(headers, route, params, verb) {
        const host = BASE_URL;
        const url = `${host}${route}`;

        var formBody = [];
        if (params) {
            for (var property in params) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(params[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");
        }

        let response = {};
        let options = Object.assign({ method: verb }, params ? { body: formBody } : {}, { headers: headers });
        console.log(url)
        console.log(headers)
        console.log(options)
        return fetch(url, options).then(response => {
            if (response.ok && response.status == 200) {
                let json = response.json();
                console.log(json)
                return json;

                return json.then(error => {
                    console.log(error);
                    throw error
                });
            } else {
                return this.networkError(response.status);
            }

        }).catch(error => {
            console.log(error)
            return this.networkError(0);
        })
    }

    static networkError(status) {
        let error = {
            code: status,
            message: status == 0 ? 'Please check your connection!' : `Internal server error ${status}`,
        };

        return error;
    }


    static postMultipart(route, data) {
        return this.xhrMultipart(route, data, 'POST');
    }

    static async xhrMultipart(route, data, verb) {
        const host = BASE_URL;
        const url = `${host}${route}`;
        const headers = await this.headersMultipart();

        let response = {};
        let options = Object.assign({ method: verb }, data ? { body: data } : {}, { headers: headers });
        console.log(url)
        console.log(options)
        return fetch(url, options).then(response => {
            let json = response.json();

            if (response.ok) {
                console.log(json)
                return json;
            }

            return json.then(error => {
                console.log(error);
                throw error
            });
        })
    }

    static async headersMultipart() {
        let idToken = null;
        const currentUser = gueloginAuth.auth().currentUser;

        let result = {
            'authorization': idToken,
            'platform': platform.platform
        };

        try {
            result.authorization = await currentUser.getIdToken(true);

            return result;
        } catch (error) {
            return result;
        }
    }
}

export function errorMessage(response) {
	result = "";

	if (response.status != 200) {
		result = response.message;
	} else if (response.result != null && response.result.acknowledge === false) {
		result = response.result.message;
	}

	return result;
}
