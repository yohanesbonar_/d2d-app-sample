import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, View } from 'react-native';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body, ListItem } from 'native-base';
import Radio from './Radio';
import platform from '../../theme/variables/d2dColor';
import { testID } from './../libs/Common';

export default class CardItemQuiz extends Component {

    state = {
        answer: null
    }

    constructor(props) {
        super(props);
    }

    changeAnswer(answer) {
        // let questionData = this.props.data;
        // this.props.resultChoices(questionData.id, answer == questionData.answer);

        let questionData = this.props.data;
        questionData.answer = answer;
        this.props.resultChoices(questionData);
        this.setState({ answer: answer });
    }

    renderChoices = (dataChoiches) => {
        let items = [];

        if (dataChoiches != null) {
            if (this.props.from == 'Webinar') {
                let choiches = dataChoiches

                if (choiches != null && choiches.length > 0) {
                    let self = this;
                    choiches.map(function (value, i) {

                        items.push(
                            <CardItem key={i} bordered>
                                <Radio
                                    {...testID('radio ' + i)}
                                    title={value.value}
                                    selected={self.state.answer == value.key || (self.props.data.answer != '' && self.props.data.answer == value.key) ? true : false}
                                    onPress={() => self.changeAnswer(value.key)} />
                            </CardItem>
                        )
                    });
                }
            }
            else {

                let choiches = JSON.parse(dataChoiches)

                if (choiches != null && choiches.length > 0) {
                    let self = this;
                    choiches.map(function (d, i) {
                        items.push(
                            <CardItem key={i} bordered>
                                <Radio
                                    {...testID('radio ' + i)}
                                    title={d} selected={self.state.answer == d || self.props.data.answer == d ? true : false} onPress={() => self.changeAnswer(d)} />
                            </CardItem>
                        )
                    });
                }

            }

        }

        return items;
    }

    render() {
        let data = this.props.data;
        let headerQuestion = this.props.from == 'Webinar' ? `Pertanyaan ${this.props.index + 1} dari ${this.props.length}` : `Question ${this.props.index + 1} of ${this.props.length}`
        return (
            <Card style={{ flex: 0 }}>
                <CardItem bordered>
                    <Text style={styles.numQuiz}>{headerQuestion}</Text>
                </CardItem>
                <CardItem bordered>
                    <Text style={styles.question}>
                        {data.question}
                    </Text>
                </CardItem>
                {this.renderChoices(data.choices)}
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    numQuiz: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 11,
        color: '#15ABED'
    },
    question: {
        fontFamily: 'Nunito-Bold',
        fontSize: 16,
        color: '#6C6C6C'
    }
});