import React, { Component } from 'react';
import {

    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Linking
} from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
import AutoHeightWebView from 'react-native-autoheight-webview'
import HTML from 'react-native-render-html';

import { escapeHtml, customCss } from './../libs/Common';
import { AdjustTrackerConfig, AdjustTracker } from '../libs/AdjustTracker';
import { testID } from '../libs/Common'

let from = {
    LINK_REFERENCE: "LINK_REFERENCE",
};
export default class ContentDetailAtoZ extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            activeTab: 1,
            yIndex: 0,
            tabActive: 0,
            componentObat: [],
            tag: [],
            related_obat: {},
            desc: '',
            readmore: true,

        }
    }

    handleStateWebview = () => {
        this.props.stateLoadWebView();
    }

    componentDidMount() {
        this.setState({
            data: this.props.dataObat,
            desc: this.shortenDesc(this.props.dataObat.description, 150),
            descAlt: this.shortenDesc(this.props.dataObat.description, 150)
        });
    }

    sendAdjust = (action) => {

        let token = "";

        switch (action) {

            case from.LINK_REFERENCE:
                token = AdjustTrackerConfig.ObatAZ_Detail_Link;
                break;
            default:
                break;
        }
        if (token != "") {
            AdjustTracker(token);
        }
    };

    shortenDesc(text, max) {
        let result = null;
        text = escapeHtml(text);

        if (text && text.replace(/<[^>]*>/g, '').length > max) {
            result = text.slice(0, max).split(' ').slice(0, -1).join(' ');
            result = result.replace(/<(\/?|\!?)(p|br)>/g, '');
        } else {
            result = text;
        }

        this.setState({ desc: result });

        return result;
    }


    pressShowMoreLess = (dataDesc) => {
        if (this.state.readmore) {
            this.setState({ desc: this.shortenDesc(dataDesc, dataDesc.length) })
        }
        else {
            this.setState({ desc: this.shortenDesc(dataDesc, 150) })
        }
        this.setState({ readmore: !this.state.readmore })
    }

    renderShowMoreLess() {
        return (
            <View>
                <TouchableOpacity
                    {...testID('show_more_less')}

                    onPress={() =>
                        this.pressShowMoreLess(this.state.data.description)
                    }
                >
                    <Text style={styles.readmoreText}>
                        {this.state.readmore == true ? 'Lebih banyak...' : 'Lebih sedikit...'}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        console.warn(escapeHtml(this.state.descAlt, '').length, escapeHtml(this.state.data.description, '').length)
        return (
            <View style={{ marginLeft: 0 }}>
                <Text style={styles.contentLabel}>{this.state.data.title}</Text>

                <HTML
                    onLinkPress={(event, href) => { Linking.openURL(href) + this.sendAdjust(from.LINK_REFERENCE) }}
                    baseFontStyle={BASE_FONT_STYLE}
                    html={escapeHtml(this.state.desc, '')}
                />

                {(escapeHtml(this.state.descAlt, '').length - escapeHtml(this.state.data.description, '').length < 0) && this.renderShowMoreLess()}

            </View >
        )
    }

}


// const CONTAINER_MAX_WIDTH = (Dimensions.get('window').width * 95) / 100;

// const CUSTOM_CONTAINER_STYLE = {
//     // maxWidth: CONTAINER_MAX_WIDTH,
//     // marginBottom: -10,
//     // marginRight: 0
// };

const BASE_FONT_STYLE = {
    fontSize: 13,
    color: '#78849E',
    textAlign: 'justify'
};


const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F3F3F3',
        alignItems: 'center',
    },
    top: {
        backgroundColor: '#CB1D50',
        height: HEIGHT * 0.6,
        width: WIDTH * 2,
        borderBottomLeftRadius: WIDTH * 4,
        borderBottomRightRadius: WIDTH * 4,
        flexDirection: 'column',
        alignItems: 'center'
        // borderRadius: WIDTH/4,
    },
    topMargin: {
        marginLeft: 0,
        width: WIDTH,
        // alignItems: 'flex-start'
    },
    back_button: {
        color: 'white',
        alignItems: 'flex-start',
        position: 'absolute',
        left: 0,
        marginLeft: 20,
        marginTop: 20,
    },
    nama_obat: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20,
        fontFamily: 'Nunito-SemiBold',
        fontSize: 18,
        color: '#FFFFFF'
    },
    contentNamaObat: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 10,
        fontFamily: 'Nunito-Bold',
        fontSize: 20,
        color: '#454F63'
    },
    contentLabel: {
        alignSelf: 'flex-start',
        justifyContent: 'center',
        //marginTop: 10,
        //marginLeft: 15,
        fontFamily: 'Nunito-Bold',
        fontSize: 16,
        color: '#454F63'
    },
    text: {
        alignSelf: 'flex-start',
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        fontFamily: 'Nunito-Regular',
        fontSize: 14,
        color: '#78849E'
    },
    containerTabs: {
        flexDirection: "row",
        width: '150%',
        backgroundColor: '#fff'
    },
    buttonTab: {
        // width: '50%',
        paddingHorizontal: 10,
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 3,
        borderBottomColor: 'transparent'
    },
    textButtonTab: {
        fontFamily: 'Nunito-Bold',
        fontSize: 13,
        color: '#959DAD'
    },
    coverWebView: {
        position: 'absolute',
        backgroundColor: 'transparent',
        width: '100%',
        height: 130,
        zIndex: 1
    },
    readmoreText: {
        color: '#78849E',
        textAlign: 'left',
        marginTop: 10,
        fontWeight: 'bold',
        marginBottom: 15
    },
})

