
import React, { Component } from 'react'
import { Text, View, Image, SafeAreaView, TouchableOpacity } from 'react-native'
import { Modalize } from 'react-native-modalize';
import { Col, Row, Button } from 'native-base'
export default class BottomSheet extends Component {

    constructor(props) {
        super(props);
        this.myRef = React.createRef()   // Create a ref object 
        this.state = {
            title: null,
            message: null,
            buttonTextPositive: null,
            buttonTextNegative: null
        }
    }

    _onPressItem = (textHistory) => {
        this.props.onPressItem(textHistory)
    };

    _onPressRemoveItem = (item) => {
        this.props.onPressRemoveItem(item)
    }

    componentDidMount = () => {
        if (this.myRef.current != null) {
            this.myRef.current.open()
        }
        console.log("BottomSheet componentDidMount props: ", this.props)
    }


    renderContent = () => {
        return (
            <Col style={{
                alignItems: 'center',
                marginBottom: 16
            }}>
                <Image
                    resizeMode='contain'
                    style={{
                        height: 82,
                        width: 82,
                        marginVertical: 32,
                    }} source={require("../assets/images/icon-expired.png")}>

                </Image>
                <Text bold style={{
                    marginBottom: 16,
                    textAlign: 'center',
                    fontSize: 20,
                }}>{this.props.title}</Text>
                <Text regular style={{
                    textAlign: 'center',
                }}>{this.props.message}</Text>

                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginTop: 24
                }}>
                    {this.props.buttonTextNegative != null && (
                        <Button secondary block style={{ flex: 1, marginRight: 8 }} onPress={() => this.modalizeBottomSheet.close()}>
                            <Text textButtonCancel>{this.props.buttonTextNegative}</Text>
                        </Button>
                    )}
                    <TouchableOpacity
                        style={{
                            backgroundColor: "#F6F6F7",
                            justifyContent: 'center',
                            paddingHorizontal: 24,
                            borderRadius: 8,
                            height: 48,
                            marginRight: 16
                        }} onPress={() => null} >
                        <Text style={{ fontFamily: 'Nunito-Bold', color: "#1E1E20", fontSize: 16, lineHeight: 26 }}>{this.props.buttonTextPositive}</Text>
                    </TouchableOpacity>
                </Row>
            </Col>
        )
    }

    saveLayout() {
        this.modalizeBottomSheet.open()
    }

    render() {
        console.log("BottomSheet props: ", this.props)
        if (this.props.show == true) {
            return (

                <Modalize
                    //onLayout={() => this.saveLayout()}
                    withOverlay={true}
                    withHandle={true}
                    handleStyle={{
                        backgroundColor: 'transparent'
                    }}

                    adjustToContentHeight={true}
                    closeOnOverlayTap={false}
                    panGestureComponentEnabled={false}
                    // ref={(ref) => {
                    //     this.modalizeBottomSheet = ref;
                    // }}
                    ref={this.myRef}
                    modalStyle={{
                        borderTopLeftRadius: 16,
                        borderTopRightRadius: 16,
                        paddingHorizontal: 16,
                    }}

                    HeaderComponent={
                        <View style={{
                            height: 4,
                            borderRadius: 2,
                            width: 40,
                            marginTop: 8,
                            backgroundColor: '#454F6329',
                            alignSelf: 'center'
                        }}>

                        </View>
                    }
                >
                    {/* {this.props.children} */}
                    {this.renderContent()}
                </Modalize>

            )
        }

        return null

    }
}
