import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, View } from 'react-native';
import ViewMoreText from 'react-native-view-more-text';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body, Col } from 'native-base';
import platform from '../../theme/variables/platform';
import { testID } from './../libs/Common';

export default class CardItemJournal extends Component {
    menu = null;

    constructor(props) {
        super(props);
    }

    _renderViewMore(onPress) {
        return (
            <Text onPress={onPress}>more</Text>
        )
    }

    _renderViewLess(onPress) {
        return (
            <Text onPress={onPress}>less</Text>
        )
    }

    showErrorMessage() {
        Toast.show({ text: 'Data not found', position: 'bottom', duration: 3000, type: 'danger' });
    }

    render() {
        const PdfView = { type: "Navigate", routeName: "PdfView", params: this.props.data }

        let availableAttachment = this.props.data.attachment != null ? true : false;

        return (
            <Card style={{ flex: 0 }}>
                <TouchableOpacity
                    {...testID('button_card_journal')}
                    activeOpacity={0.7} onPress={() => availableAttachment === true ?
                        this.props.navigation.navigate(PdfView) : this.showErrorMessage()}>
                    <CardItem bordered>
                        <Left>
                            <Icon name='ios-school' style={{ color: '#8CA7D3' }} />
                            <Body>
                                <Text style={styles.title}
                                    numberOfLines={2}>{this.props.data.title}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    <CardItem nopadding>
                        <Body style={{ paddingVertical: 5 }}>
                            {/* <ViewMoreText
                                numberOfLines={2}
                                renderViewMore={this._renderViewMore}
                                renderViewLess={this._renderViewLess}
                                textStyle={{textAlign: 'left'}}> */}
                            <Text style={styles.postText}
                                numberOfLines={2}>{this.props.data.short_description}</Text>
                            {/* </ViewMoreText> */}
                        </Body>
                    </CardItem>
                    <CardItem nopadding>
                        <Col style={styles.toolItemFirst}>
                            <Text style={styles.poweredBy}>dibaca {this.props.data.flag_view}</Text>
                        </Col>
                        <Col style={styles.toolItemCenter}>

                        </Col>
                        <Col style={styles.toolItemLast}>
                            <Text style={styles.poweredBy} numberOfLines={2}>
                                powered by <Text style={styles.poweredByName}>{this.props.data.author}</Text>
                            </Text>
                        </Col>
                    </CardItem>
                </TouchableOpacity>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 16,
        color: '#425062'
    },
    moreBtn: {
        padding: 1,
    },
    postText: {
        fontSize: 14,
        padding: 10,
        color: '#6C6C6C',
        textAlign: 'left'
    },
    postImage: {
        width: platform.deviceWidth - 25,
        height: 200,
    },
    viewsWrapper: {
        padding: 5,
        backgroundColor: '#fff',
        position: 'absolute',
        right: 0,
    },
    poweredByWrapper: {
        position: 'absolute',
        bottom: 0,
        right: 0,
    },
    poweredBy: {
        fontFamily: 'Nunito-ExtraLight',
        fontSize: 12,
        color: '#425062',
        textAlign: 'right'
    },
    poweredByName: {
        fontFamily: 'Nunito-Bold',
        fontSize: 12,
        color: '#6EC672',
        textAlign: 'right'
    },
    toolItemFirst: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
        paddingHorizontal: 10,
    },
    toolItemCenter: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
    },
    toolItemLast: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
        paddingHorizontal: 10,
    },
});