import React, { Component } from "react";
import { TouchableWithoutFeedback, Image, StyleSheet } from "react-native";
import { View, Footer, FooterTab, Button, Icon, Text } from "native-base";
import platform from "../../theme/variables/d2dColor";
import { testID, STORAGE_TABLE_NAME } from "../libs/Common";

import { AdjustTrackerConfig, AdjustTracker } from "./../libs/AdjustTracker";
import AsyncStorage from "@react-native-community/async-storage";
import { getData, KEY_ASYNC_STORAGE } from "../../src/utils/localStorage";

export default class MainMenu extends Component {
  state = {
    showMenu: false,
    isInternationalMode: false,
    footerMenuAvailable: true,
  };

  toggleMenu = () => {
    AdjustTracker(AdjustTrackerConfig.Explore_Explore);
    this.setState({ showMenu: !this.state.showMenu });
  };

  componentDidMount() {
    this.initSetup();
    this.getDataCountryCode();
  }

  initSetup = async () => {
    let dataProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
    let countryCode = JSON.parse(dataProfile).country_code;
    if (countryCode != "ID") {
      this.setState({
        isInternationalMode: true,
      });
    }
  };

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.setState({ footerMenuAvailable: false });
      } else {
        this.setState({ footerMenuAvailable: true });
      }
    });
  };

  renderMenu = () => {
    return this.state.showMenu == true ? (
      <View style={styles.expMenuMainView}>
        <TouchableWithoutFeedback onPress={() => this.toggleMenu()}>
          <View style={styles.expMenuEmptyView} />
        </TouchableWithoutFeedback>
        <View style={styles.expMenuView}>
          <Button
            {...testID("buttonEvent")}
            light
            vertical
            onPress={() => this.navigate("Event")}
            active={this.props.state === 3 && this.state.showMenu === false}
            style={styles.expMenuButton}
          >
            <Image
              source={require("./../assets/images/explore-menu/event-icon.png")}
            />
            <View style={{ flex: 1, justifyContent: "center" }}>
              <Text style={[styles.expMenuIconColor, styles.textExploreMenu]}>
                EVENT
              </Text>
            </View>
          </Button>
          <Button
            {...testID("buttonLearning")}
            light
            vertical
            onPress={() => this.navigate("Learning")}
            active={this.props.state === 3 && this.state.showMenu === false}
            style={styles.expMenuButton}
          >
            <Image
              source={require("./../assets/images/explore-menu/learning-icon.png")}
            />
            <View style={{ flex: 1, justifyContent: "center" }}>
              <Text style={[styles.expMenuIconColor, styles.textExploreMenu]}>
                LEARNING
              </Text>
            </View>
          </Button>
          <Button
            {...testID("buttonRequestJournal")}
            light
            vertical
            onPress={() => this.navigate("LearningRequestJournal")}
            active={this.props.state === 3 && this.state.showMenu === false}
            style={styles.expMenuButton}
          >
            <Image
              source={require("./../assets/images/explore-menu/request-journal-icon.png")}
            />
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={[
                  styles.expMenuIconColor,
                  styles.expMenuFontSize,
                  styles.textExploreMenu,
                ]}
              >
                REQUEST JOURNAL
              </Text>
            </View>
          </Button>

          {!this.state.isInternationalMode && (
            <Button
              {...testID("buttonAtoZ")}
              light
              vertical
              onPress={() => this.navigate("AtoZ")}
              active={this.props.state === 3 && this.state.showMenu === false}
              style={styles.expMenuButton}
            >
              <Icon
                name="ios-flask"
                style={[styles.expMenuIconColor, styles.expMenuIconSize]}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  style={[
                    styles.expMenuIconColor,
                    styles.expMenuFontSize,
                    styles.textExploreMenu,
                  ]}
                >
                  A to Z
                </Text>
              </View>
            </Button>
          )}
        </View>
      </View>
    ) : null;
  };

  navigate = (to) => {
    if (this.state.showMenu == true && this.props.state == 2) {
      this.toggleMenu();
    } else {
      this.setState({ showMenu: false });
    }

    switch (to) {
      case "Feeds":
        AdjustTracker(AdjustTrackerConfig.Feeds_Feeds);
        break;
      case "Cme":
        AdjustTracker(AdjustTrackerConfig.CME_CME);
        break;
      case "Event":
        AdjustTracker(AdjustTrackerConfig.Explore_Event);
        break;
      case "Learning":
        AdjustTracker(AdjustTrackerConfig.Explore_Learning);
        break;
      case "LearningRequestJournal":
        AdjustTracker(AdjustTrackerConfig.Explore_ReqJournal);
        break;
      case "AtoZ":
        AdjustTracker(AdjustTrackerConfig.ObatAZ_ObatAZ);
    }

    this.props.navigation.navigate(to);
  };

  render() {
    console.log("FooterMenu props: ", this.props);
    let props = this.props;

    return (
      <View>
        {this.renderMenu()}
        {this.props.isHide != true && this.state.footerMenuAvailable == true && (
          <Footer>
            <FooterTab style={{ marginTop: 6 }}>
              <Button
                {...testID("buttonFeeds")}
                vertical
                onPress={() => this.navigate("Feeds")}
                active={this.props.state === 0}
              >
                {this.props.state == 0 ? (
                  <Image
                    source={require("./../assets/images/button-navigation-action/feeds-on-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                ) : (
                  <Image
                    source={require("./../assets/images/button-navigation-action/feeds-off-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                )}
                <Text textBottomNavigation>Feeds</Text>
              </Button>
              <Button
                {...testID("buttonCme")}
                vertical
                onPress={() => this.navigate("Cme")}
                active={this.props.state === 1}
              >
                {this.props.state == 1 ? (
                  <Image
                    source={require("./../assets/images/button-navigation-action/CME-on-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                ) : (
                  <Image
                    source={require("./../assets/images/button-navigation-action/CME-off-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                )}
                <Text textBottomNavigation>CME</Text>
              </Button>
              <Button
                {...testID("buttonExplore")}
                vertical
                onPress={() => this.toggleMenu()}
                active={
                  this.props.state != 0 &&
                  this.props.state != 1 &&
                  this.props.state != 3 &&
                  this.props.state != 4 &&
                  this.state.showMenu === true
                }
              >
                <Image
                  source={require("./../assets/images/button-navigation-action/explore-on-icon.png")}
                  style={{ width: 24, height: 24 }}
                />
                <Text textBottomNavigation>Explore</Text>
              </Button>
              <Button
                {...testID("buttonWebinar")}
                vertical
                onPress={() => this.navigate("Webinar")}
                active={this.props.state === 3}
              >
                {this.props.state == 3 ? (
                  <Image
                    source={require("./../assets/images/button-navigation-action/webinar-on-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                ) : (
                  <Image
                    source={require("./../assets/images/button-navigation-action/webinar-off-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                )}
                <Text textBottomNavigation>Webinar</Text>
              </Button>
              <Button
                {...testID("buttonProfile")}
                vertical
                onPress={() => this.navigate("Profile")}
                active={this.props.state === 4}
              >
                {this.props.state == 4 ? (
                  <Image
                    source={require("./../assets/images/button-navigation-action/profile-on-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                ) : (
                  <Image
                    source={require("./../assets/images/button-navigation-action/profile-off-icon.png")}
                    style={{ width: 24, height: 24 }}
                  />
                )}
                <Text textBottomNavigation>Profile</Text>
              </Button>
            </FooterTab>
          </Footer>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  expMenuMainView: {
    position: "absolute",
    flex: 1,
    zIndex: 1000,
    top: -(platform.deviceHeight - platform.footerHeight),
    right: 0,
    bottom: 0,
    left: 0,
    height: platform.deviceHeight - platform.footerHeight,
    backgroundColor: "rgba(52, 52, 52, 0.5)",
  },
  expMenuEmptyView: {
    height: platform.deviceHeight - platform.footerHeight - 100,
    backgroundColor: "#000000",
    opacity: 0.8,
  },
  expMenuView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 0,
    width: "100%",
    // paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: "#ffffff",
    // height: platform.deviceHeight / 4 ,
    borderBottomWidth: 1,
    borderBottomColor: "#EEF0F5",
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  expMenuButton: {
    marginHorizontal: 10,
    paddingVertical: 10,
    paddingHorizontal: 2,
    // width: platform.deviceWidth / 3.5,
    flex: 1,
    height: "100%",
    backgroundColor: "#CB1D50",
  },
  expMenuIconSize: {
    fontSize: 24,
  },
  expMenuIconColor: {
    color: "white",
  },
  expMenuFontSize: {
    fontSize: 11,
  },
  footerFontSize: {
    fontSize: 9,
  },
  textExploreMenu: {
    fontFamily: "Nunito-Bold",
    fontSize: 12,
    textAlign: "center",
  },
});
