import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { testID } from './../libs/Common';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body, Row, Col } from 'native-base';

import { AdjustTracker, AdjustTrackerConfig } from '../libs/AdjustTracker';

export default class ListItemLearning extends Component {

    constructor(props) {
        super(props);

        this.state = {

        }
    }

    navigateTo(to) {
        this.props.navigation.navigate(to);
        AdjustTracker(AdjustTrackerConfig.Specialist[to.params.id]);
    }

    render() {
        const learningSpecialist = { type: "Navigate", routeName: "LearningSpecialist", params: this.props.data }
        let icon = this.props.data.image_thumbnail;
        let totalMember = this.props.data.subscribed + ' Member';

        return (
            <TouchableOpacity
                {...testID('buttonSpecialist')}
                activeOpacity={0.7} onPress={() => this.navigateTo(learningSpecialist) }>
                <Card style={{ flex: 0 }}>
                    <CardItem nopadding>
                        <Left style={styles.contentCard}>
                            <Icon style={styles.itemImage} name='md-person' />
                            <Body style={{ justifyContent: 'center' }}>
                                <Text style={styles.itemTitle}>{this.props.data.description}</Text>
                                <Text style={styles.itemTotalMember}>{totalMember}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    contentCard: {
        marginHorizontal: 15,
        marginVertical: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemImage: {
        color: '#DE215D',
        fontSize: 70,
        alignItems: 'center',
        marginHorizontal: 15,
    },
    itemTitle: {
        fontFamily: 'Nunito-Black',
        color: '#707070',
        marginBottom: 5,
        fontSize: 16,
    },
    itemTotalMember: {
        fontFamily: 'Nunito-Regular',
        color: '#6C6C6C',
        fontSize: 12,
        marginBottom: 5,
    }
})