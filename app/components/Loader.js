import React, { Component } from 'react';
import { Platform, StyleSheet, View, Animated, Easing } from 'react-native';

const widthLoader = { big: 40, middle: 25, small: 10 };

export default class Loader extends Component {
    constructor(props) {
        super(props);
        this.animatedWidthLoader = {
            big: new Animated.Value(0),
            middle: new Animated.Value(0),
            small: new Animated.Value(0),
        }
    }

    componentDidMount() {
        this.runAnimation();
    }

    runAnimation() {
        Animated.timing(this.animatedWidthLoader.small, { toValue: widthLoader.small, duration: 100, ease: Easing.bounce }).start();
        Animated.timing(this.animatedWidthLoader.middle, { toValue: widthLoader.middle, duration: 300, ease: Easing.bounce }).start()
        Animated.timing(this.animatedWidthLoader.big, { toValue: widthLoader.big, duration: 500, ease: Easing.bounce }).start();
        setTimeout(() => this.reverseAnimation(), 800);
    }

    reverseAnimation() {
        setTimeout(() =>
            Animated.timing(this.animatedWidthLoader.big, { toValue: 0, duration: 300, ease: Easing.bounce }).start()
            , 100);
        setTimeout(() =>
            Animated.timing(this.animatedWidthLoader.middle, { toValue: 0, duration: 300, ease: Easing.bounce }).start()
            , 200);
        setTimeout(() =>
            Animated.timing(this.animatedWidthLoader.small, { toValue: 0, duration: 300, ease: Easing.bounce }).start()
            , 400);

        setTimeout(() => this.runAnimation(), 900);
    }

    rendringVisibility() {
        return this.props.visible == true ?
            (<View style={[styles.container, this.props.styles]}>
                <View style={styles.wrapper}>
                    <Animated.View style={[styles.bigCircle, animatedStyles.big]} />
                    <Animated.View style={[styles.middleCircle, animatedStyles.middle]} />
                    <Animated.View style={[styles.smallCircle, animatedStyles.small]} />
                </View>
            </View>) :
            (null);
    }

    render() {
        const animatedStyles = {
            big: {
                width: this.animatedWidthLoader.big,
                height: this.animatedWidthLoader.big,
                borderRadius: this.animatedWidthLoader.big
            },
            middle: {
                width: this.animatedWidthLoader.middle,
                height: this.animatedWidthLoader.middle,
                borderRadius: this.animatedWidthLoader.middle
            },
            small: {
                width: this.animatedWidthLoader.small,
                height: this.animatedWidthLoader.small,
                borderRadius: this.animatedWidthLoader.small
            },
        }

        return this.props.visible == true ?
            (<View style={[styles.container, { backgroundColor: this.props.transparent ? 'transparent' : 'rgba(255, 255, 255, 0.9)' }]}>
                <View style={styles.wrapper}>
                    <Animated.View style={[styles.bigCircle, animatedStyles.big]} />
                    <Animated.View style={[styles.middleCircle, animatedStyles.middle]} />
                    <Animated.View style={[styles.smallCircle, animatedStyles.small]} />
                </View>
            </View>) :
            (null);
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        zIndex: 1000,
        elevation: 25
    },
    wrapper: {
        width: 80,
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    bigCircle: {
        width: widthLoader.big,
        height: widthLoader.big,
        borderRadius: widthLoader.big,
        position: 'absolute',
        backgroundColor: 'rgba(203, 29, 80, 0.25)'
    },
    middleCircle: {
        width: widthLoader.middle,
        height: widthLoader.middle,
        borderRadius: widthLoader.middle,
        position: 'absolute',
        backgroundColor: 'rgba(203, 29, 80, 0.25)'
    },
    smallCircle: {
        width: widthLoader.small,
        height: widthLoader.small,
        borderRadius: widthLoader.small,
        position: 'absolute',
        backgroundColor: 'rgba(203, 29, 80, 1)'
    },
});
