import { Button, Col, Content, Icon, Row, Text } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Image } from 'react-native';
import Modal from 'react-native-modal';
import { testID } from '../libs/Common';
import { getData, KEY_ASYNC_STORAGE } from "../../src/utils/localStorage";

export default class Popup extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isPopupShow: false,
            country_code: ""
        }
    }

    togglePopup = () => this.setState({ isPopupShow: !this.state.isPopupShow });

    onPressButtonNo = () => this.togglePopup()


    onPressButtonYes = () => {
        this.togglePopup()
        this.props.closeScreen()
    }

    async onPressButtonWebinar() {
        await this.togglePopup()
        this.props.closeScreen()
    }

    async onPressButtonFeeds() {
        await this.togglePopup()
        this.props.gotoFeeds()
    }

    async onPressButtonHome() {
        await this.togglePopup()
        this.props.gotoHome()
    }

    _onPressButtonPositive = () => {
        this.togglePopup()
        this.props.onPressButtonPositive()
    };
    renderTitle = () => {

        if (this.props.type == 'navigation' || this.props.type == 'navigationID') {
            return (
                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flex: 0,
                    marginBottom: 16
                }}>
                    <Text style={[styles.textTitleLeftAlign,]}>{this.props.title}</Text>
                    <TouchableOpacity onPress={() => this.togglePopup()}>
                        {/* <Text style={styles.textClose}>X</Text> */}
                        <Icon name='md-close' type="Ionicons"
                            style={{ fontSize: 24 }}
                        />
                    </TouchableOpacity>

                </Row>
            )
        }
        else if (this.props.type == 'logout') {
            return (
                <Text style={styles.textTitleLeftAlign}>{this.props.title}</Text>
            )
        }
        else if (this.props.type == "successInformation") {
            return (
                <Text H6 style={{ marginBottom: 16, alignSelf: 'center' }}>{this.props.title}</Text>)
        }
        else {
            return (
                <Text style={styles.textTitle}>{this.props.title}</Text>
            )
        }
    }

    renderButton = () => {
        let textButtonNegative = this.props.textButtonNegative != null ? this.props.textButtonNegative : "NO"
        let textButtonPositive = this.props.textButtonPositive != null ? this.props.textButtonPositive : "YES"
        if (this.props.type == 'navigation') {

            return (
                <View style={{
                    flex: 1,
                    flexDirection: this.props.type == 'vertical' ? 'column' : 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                    <Button
                        {...testID('button_webinar')}
                        style={[styles.buttonOptionNavigate, { marginRight: 10 }]}
                        onPress={() => this.onPressButtonWebinar()} >
                        <Icon style={styles.iconButtonNavigate} name="control-play" type="SimpleLineIcons" />
                        <Text style={styles.textButtonNavigate}>Webinar</Text>
                    </Button>

                    <Button
                        {...testID('button_feeds')}
                        style={[styles.buttonOptionNavigate, { marginLeft: 10 }]}
                        onPress={() => this.onPressButtonFeeds()} >
                        <Icon style={styles.iconButtonNavigate} name="md-globe" />
                        <Text style={styles.textButtonNavigate}>Feeds</Text>
                    </Button>
                </View>
            )
        } else if (this.props.type == 'navigationID') {

            return (
                <View style={{
                    flex: 1,
                    flexDirection: this.props.type == 'vertical' ? 'column' : 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                    <Button
                        {...testID('button_webinar')}
                        style={[styles.buttonOptionNavigate, { marginRight: 10 }]}
                        onPress={() => this.onPressButtonWebinar()} >
                        <Icon style={styles.iconButtonNavigate} name="control-play" type="SimpleLineIcons" />
                        <Text style={styles.textButtonNavigate}>Webinar</Text>
                    </Button>

                    <Button
                        {...testID('button_home')}
                        style={[styles.buttonOptionNavigate, { marginLeft: 10 }]}
                        onPress={() => this.onPressButtonHome()} >
                        <Icon style={styles.iconButtonNavigate} name="md-globe" />
                        <Text style={styles.textButtonNavigate}>Home</Text>
                    </Button>
                </View>
            )
        }
        else if (this.props.type == "logout") {
            return (
                <Row style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginTop: 24
                }}>


                    <TouchableOpacity
                        style={[styles.button, {
                            backgroundColor: "#EBEBEB",
                            marginRight: 16
                        }
                        ]} onPress={() => this.onPressButtonNo()} >
                        <Text textButtonCancel style={{ fontSize: 14 }}>{textButtonNegative}</Text>
                    </TouchableOpacity>


                    <TouchableOpacity
                        style={[styles.button, {
                            backgroundColor: "#D01E53",
                        }]} onPress={() => this._onPressButtonPositive()} >
                        <Text textButton style={{ fontSize: 14 }}>{textButtonPositive}</Text>
                    </TouchableOpacity>
                </Row>

            )
        }

        else {
            return (
                <View style={{
                    flex: 1,
                    flexDirection: this.props.type == 'vertical' ? 'column' : 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                    <Button
                        {...testID('button_no')}
                        accessibilityLabel="button_no"
                        style={styles.buttonNo}
                        onPress={() => this.onPressButtonNo()} block>
                        <Text style={{
                            textDecorationLine: 'underline',
                            color: '#3AE194',
                            fontFamily: 'Nunito-Bold'
                        }}>{textButtonNegative}</Text>
                    </Button>

                    <Button
                        {...testID('button_yes')}
                        accessibilityLabel="button_yes"
                        style={styles.buttonYes}
                        onPress={() => this.onPressButtonYes()} block>
                        <Text style={{
                            color: 'white',
                            fontFamily: 'Nunito-Bold'
                        }}>{textButtonPositive}</Text>
                    </Button>

                </View>
            )
        }
    }

    renderMessage = () => {
        let message = this.props.message
        if (this.props.type == 'navigation') {
            return null
        }
        else if (this.props.type == "logout") {
            return (
                <View>
                    <Text style={[styles.textMessageLeftAlign]}>
                        <Text regular>{'Your data might be lost, such as '}</Text>
                        <Text regular bold >{'login access'}</Text>
                        <Text regular> and </Text>
                        <Text regular bold >{'webinar certificates.'}</Text>

                    </Text>
                </View>
            )
        }
        else if (this.props.type == "successInformation") {
            return (
                <Text Body1 style={{ alignSelf: 'center' }}>{message}</Text>
            )
        }
        else {
            <Text style={styles.textMessage}>{message}</Text>
        }
    }

    render() {
        const { message } = this.props;
        console.log("props popup: ", this.props)
        return (
            <Modal
                hasBackdrop={true}
                avoidKeyboard={true}
                backdropOpacity={0.8}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={600}
                animationOutTiming={600}
                backdropTransitionInTiming={600}
                backdropTransitionOutTiming={600}
                isVisible={this.state.isPopupShow}
            >
                <Content
                    contentContainerStyle={{ justifyContent: 'center', flex: 1 }}
                    centerContent={true}
                >

                    <View style={{
                        padding: this.props.type == 'navigation' ? 16 : 10,
                        position: 'absolute',
                        left: 0,
                        right: 0
                    }}>
                        <View style={{
                            flex: 0,
                            backgroundColor: 'white',
                            paddingHorizontal: this.props.type == 'navigation' || this.props.type == "logout" ? 16 : 24,
                            //paddingVertical: this.props.type == 'navigation' && "logout" ? 16 : 20,
                            // borderRadius: this.props.type == 'navigation' && "logout" ? 12 : 10,
                            paddingVertical: 24,
                            borderRadius: 8,
                            zIndex: 1,
                        }}>

                            <View style={{ flex: 0, flexDirection: "column" }}>
                                {this.props.image != null && (
                                    <Image
                                        resizeMode='contain'
                                        source={this.props.image}
                                        style={{
                                            height: 82,
                                            width: 82,
                                            marginBottom: 24,
                                            alignSelf: 'center'
                                        }}

                                    />
                                )}

                                {this.renderTitle()}

                                {this.renderMessage()}

                                {this.props.type != "successInformation" && (
                                    this.renderButton()
                                )}

                            </View>
                        </View>

                    </View>

                </Content>
            </Modal >

        )
    }


}

const styles = StyleSheet.create({

    buttonNo: {
        marginTop: 15,
        marginBottom: 10,
        borderRadius: 10,
        height: 55,
        backgroundColor: 'white',
        borderColor: '#3AE194',
        borderWidth: 2,
        flex: 1,
        marginRight: 10
    },
    buttonYes: {
        marginTop: 15,
        marginBottom: 10,
        borderRadius: 10,
        height: 55,
        backgroundColor: '#3AE194',
        flex: 1,
        marginLeft: 10
    },

    textMessage: {
        fontSize: 14,
        marginTop: 15,
        color: '#6C6C6C',
        fontFamily: 'Nunito-Regular',
        textAlign: 'center'
    },

    textMessageLeftAlign: {
        fontSize: 16,
        marginTop: 16,
        color: '#000000',
        fontFamily: 'Roboto-Regular',
        letterSpacing: 0.15,
        lineHeight: 24
    },

    textTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 15,
        marginHorizontal: 25
    },

    textTitleLeftAlign: {
        fontSize: 20,
        lineHeight: 24,
        fontFamily: 'Roboto-Medium',
        fontWeight: 'bold',
        letterSpacing: 0.5
    },
    textClose: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        fontFamily: 'Nunito-Bold'
    },

    buttonOptionNavigate: {
        flex: 1,
        backgroundColor: '#F7F7FA',
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation: 0
    },
    textButtonNavigate: {
        color: '#454F63',
        fontFamily: 'Nunito-Bold',
        fontSize: 14,
        paddingLeft: 0,
        paddingRight: 0
    },
    iconButtonNavigate: {
        color: '#DD034E',
        fontSize: 14,
        marginLeft: 0,
        marginRight: 8
    },

    button: {
        height: 48,
        flex: 1,
        borderRadius: 4,
        justifyContent: 'center',
    },
})
