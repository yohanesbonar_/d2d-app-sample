import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Image, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body, Row, Col, Toast } from 'native-base';
import { convertMonth, share, EnumFeedsCategory, testID } from './../libs/Common';
import { ParamAction, ParamContent, doActionUserActivity } from './../libs/NetworkUtility';
import { NavigationActions } from 'react-navigation';
import moment from 'moment-timezone';
import { AdjustTracker, AdjustTrackerConfig } from '../libs/AdjustTracker';

let from = {
    NAVIGATE: 'NAVIGATE',
    SHARE: 'SHARE',
    BOOKMARK: 'BOOKMARK',
}

export default class ListItemLearningSpecialist extends Component {

    shareLink = null;
    createdDate = '';

    constructor(props) {
        super(props);

        this.state = {
            bookmark: this.props.data.flag_bookmark > 0 ? true : false,
            from: this.props.navigation && this.props.navigation.state.routeName,
            type: this.props.data ? this.props.data.type : '',
        }

        if (this.props.data.url.length > 0) {
            let self = this;
            this.props.data.url.map(function (d, i) {
                if (d.app_name == 'D2D') {
                    self.shareLink = d.app_link;
                }
            })
        }

        if (this.props.data.created != null) {
            let self = this;
            self.createdDate = this.props.data.created.substring(8, 10) + ' ' +
                convertMonth(this.props.data.created.substring(5, 7)) + ' ' +
                this.props.data.created.substring(0, 4);
        }
    }

    sendAdjust = (action) => {
        let token = ''

        switch (action) {
            case from.NAVIGATE:
                if (this.state.from == 'Feeds') {
                    if (this.props.data.category === EnumFeedsCategory.PDF_JOURNAL) {
                        token = AdjustTrackerConfig.Feeds_Journal
                    }
                    else if (this.props.data.category === EnumFeedsCategory.PDF_GUIDELINE) {
                        token = AdjustTrackerConfig.Feeds_Guideline
                    }

                }
                else if (this.state.from == 'LearningSpecialist') {
                    if (this.state.type == 'all')
                        token = AdjustTrackerConfig.Learning_Latest_Details
                    else
                        token = AdjustTrackerConfig.Learning_Bookmark_Details
                }
                break;
            case from.BOOKMARK:
                if (this.state.from == 'Feeds') {
                    if (this.props.data.category === EnumFeedsCategory.PDF_GUIDELINE) {
                        token = AdjustTrackerConfig.Feeds_Guideline_Bookmark
                    }
                }
                else if (this.state.from == 'LearningSpecialist') {
                    if (this.state.type == 'all')
                        token = AdjustTrackerConfig.Learning_Spesialis_LatestBookmark
                    else
                        token = AdjustTrackerConfig.Learning_Spesialis_BookmarkBookmark
                }

                break;
            case from.SHARE:
                if (this.state.from == 'Feeds') {
                    if (this.props.data.category === EnumFeedsCategory.PDF_GUIDELINE) {
                        token = AdjustTrackerConfig.Feeds_Guideline_Share
                    }
                }
                else if (this.state.from == 'LearningSpecialist') {
                    if (this.state.type == 'all')
                        token = AdjustTrackerConfig.Learning_Spesialis_LatestShare
                    else
                        token = AdjustTrackerConfig.Learning_Spesialis_BookmarkShare
                }
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }

    _renderBookmark() {
        let isBookmarked = this.state.bookmark == true ? 'bookmark' : 'bookmark-border';
        let colorIcon = this.state.bookmark == true ? '#CB1D50' : '#78849E';

        return (
            <Button
                {...testID('buttonBookmark')}
                full small transparent onPress={() => this._toggleBookmark() + this.sendAdjust(from.BOOKMARK)} >
                <Text nopadding style={styles.listitemText}>Bookmark</Text>
                <Icon type="MaterialIcons" {...testID('iconBookmark')} nopadding name={isBookmarked} style={[styles.listItemIconBookmarked, { color: colorIcon }]} />
            </Button>
        )
    }

    _toggleBookmark = async () => {
        let contentType = this.props.data.category == EnumFeedsCategory.VIDEO_LEARNING ? ParamContent.VIDEO_LEARNING :
            this.props.data.category == EnumFeedsCategory.PDF_GUIDELINE ? ParamContent.PDF_GUIDELINE : ParamContent.PDF_JOURNAL;

        this.doBookmarkContent(contentType, this.props.data.id);
    }


    async doBookmarkContent(contentType, contentId) {
        let action = this.state.bookmark == true ? 'unbookmark' : 'bookmark';
        let response = await doActionUserActivity(action, contentType, contentId);
        if (response.isSuccess == true) {
            this.setState({ bookmark: !this.state.bookmark })
            Toast.show({ text: action + ' succesfull', position: 'top', duration: 3000 })
        } else {
            Toast.show({ text: response.message, position: 'top', duration: 3000 })
        }
    }

    _renderGuideline() {
        return (
            <CardItem bordered>
                <Left style={styles.contentCard}>
                    <View
                        style={styles.thumbnailGuideline}>
                        <Icon name='file-pdf-o' type='FontAwesome' style={styles.iconPlay} />
                    </View>
                    <Body style={{ justifyContent: 'center' }}>
                        <Text style={styles.itemDate}>{this.createdDate}</Text>
                        <Text {...testID('titleGuideline')} style={styles.itemTitle} numberOfLines={2}>{this.props.data.title}</Text>
                        <Text style={styles.itemDesc} numberOfLines={2} >{this.props.data.short_description}</Text>
                    </Body>
                </Left>
            </CardItem>
        );
    }

    _renderJournal() {
        let isNew = this.props.data.published_on ? (moment(this.props.data.published_on, 'YYYY-MM-DD').isSame(moment(), 'day') ? true : false) : false;
        // let dummyDate = '2019-02-01';
        // let isNew = moment(dummyDate, 'YYYY-MM-DD').isSame(moment(), 'day')

        return (
            <CardItem bordered>
                <Body style={{ justifyContent: 'center', padding: 5 }}>
                    <View style={{ flex: 1, flexDirection: 'row-reverse', alignItems: 'flex-end', justifyContent: 'center' }}>
                        {isNew == true && <View style={{ backgroundColor: '#4ab839', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5 }}>
                            <Text style={{ color: 'white' }}>NEW</Text>
                        </View>}
                        <Text style={[styles.itemDate, { flex: 1 }]}>{this.createdDate}</Text>
                    </View>
                    <Text {...testID('titleJournal')} style={styles.itemTitle} numberOfLines={2}>{this.props.data.title}</Text>
                    <Text style={styles.itemDesc} numberOfLines={2} >{this.props.data.short_description}</Text>
                </Body>
            </CardItem>
        );
    }

    _renderVideo() {
        let cover = this.props.data.cover;
        return (
            <CardItem bordered>
                <Left style={styles.contentCard}>
                    <View style={styles.viewThumbnailVideo}>
                        <ImageBackground
                            imageStyle={{ borderRadius: 5 }}
                            source={{ uri: cover }}
                            defaultSource={require('../assets/images/preloader.png')}
                            style={styles.imageBackgroundVideo}>
                            <Icon type='MaterialIcons' name='play-circle-filled' style={styles.iconPlay} />
                        </ImageBackground>
                    </View>
                    <Body style={{ justifyContent: 'center' }}>
                        <Text style={styles.itemDate}>{this.createdDate}</Text>
                        <Text {...testID('titleVideo')} style={styles.itemTitle} numberOfLines={2}>{this.props.data.title}</Text>
                        <Text style={styles.itemDesc} numberOfLines={2} >{this.props.data.short_description}</Text>
                    </Body>
                </Left>

            </CardItem>
        );
    }

    _switchCard() {
        if (this.props.data.category == EnumFeedsCategory.PDF_JOURNAL) {
            return this._renderJournal();
        } else if (this.props.data.category == EnumFeedsCategory.PDF_GUIDELINE) {
            return this._renderGuideline();
        } else if (this.props.data.category == EnumFeedsCategory.VIDEO_LEARNING) { //video
            return this._renderVideo();
        }
    }

    navigateToLearningJournalDetail(data) {
        // this.props.navigation.navigate(eventDetail);
        data.flag_bookmark = this.state.bookmark;
        // this.props.navigation.navigate('LearningJournalDetail', {
        //     updateDataBookmark : this.doUpdateDataBookmark,
        //     data : data
        // });

        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'LearningJournalDetail',
            params: {
                updateDataBookmark: this.doUpdateDataBookmark,
                data: data,
                from: this.state.from
            }, key: `journal-${data.id}`
        }));

        this.sendAdjust(from.NAVIGATE)
    }

    doUpdateDataBookmark = (bookmark) => {
        this.setState({ bookmark: bookmark });
    }


    render() {
        let route = this.props.data.category == EnumFeedsCategory.PDF_GUIDELINE ? "PdfView" :
            this.props.data.category == EnumFeedsCategory.VIDEO_LEARNING ? "VideoView" : "LearningJournalDetail";
        // const navCmeDetail   = { type: "Navigate", routeName: route, params: this.props.data, key: `${this.props.data.category}-${this.props.id}` }
        let data = this.props.data;
        data.from = this.state.from
        const navCmeDetail = NavigationActions.navigate({ routeName: route, params: data, key: `${this.props.data.category}-${this.props.id}` })

        let category = this.props.data.category == EnumFeedsCategory.VIDEO_LEARNING ? 'Video' :
            this.props.data.category == EnumFeedsCategory.PDF_JOURNAL ? 'Journal' :
                this.props.data.category == EnumFeedsCategory.PDF_GUIDELINE ? 'Guideline' : this.props.data.category;

        return (
            <TouchableOpacity activeOpacity={0.7}
                {...testID('button_card_guideline')}
                onPress={() => this.props.data.category == EnumFeedsCategory.PDF_JOURNAL ?
                    this.navigateToLearningJournalDetail(this.props.data) :
                    this.props.navigation.dispatch(navCmeDetail) + this.sendAdjust(from.NAVIGATE)}>
                <Card style={{ flex: 0 }}>
                    {this._switchCard()}
                    <CardItem nopadding>
                        <Row style={{ padding: 10 }}>
                            <Col size={50} style={{ justifyContent: 'flex-start', flexDirection: 'row', marginHorizontal: 10 }}>
                                <View style={styles.backgroundCmeType}>
                                    <Text nopadding style={styles.listitemText}>{category}</Text>
                                </View>
                            </Col>
                            <Col size={25} style={{ justifyContent: 'center', alignContent: 'center' }}>
                                <Button
                                    {...testID('buttonShare')}
                                    full small transparent onPress={() => share(this.shareLink) + this.sendAdjust(from.SHARE)} >
                                    <Text nopadding style={styles.listitemText}>Share</Text>
                                    <Icon nopadding style={styles.listItemIconBookmarked} type='EvilIcons' name='share-google' />
                                </Button>
                            </Col>
                            <Col size={25}>
                                {this._renderBookmark()}
                            </Col>
                        </Row>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    listitemText: {
        fontSize: 12,
        color: '#78849E',
    },
    listItemIconBookmarked: {
        color: '#78849E',
        fontSize: 18,
    },
    contentCard: {
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemImage: {
        color: '#DE215D',
        fontSize: 70,
        alignItems: 'center',
        marginHorizontal: 15,
    },
    itemDate: {
        fontFamily: 'Nunito-Regular',
        color: '#454F63',
        fontSize: 14,
    },
    itemTitle: {
        fontFamily: 'Nunito-Black',
        color: '#454F63',
        marginVertical: 5,
        fontSize: 16,
    },
    itemDesc: {
        fontFamily: 'Nunito-Regular',
        color: '#6C6C6C',
        fontSize: 12,
    },
    backgroundCmeType: {
        backgroundColor: '#F0F0F0',
        borderRadius: 25,
        paddingHorizontal: 15,
        justifyContent: 'center'
    },
    thumbnailGuideline: {
        borderRadius: 5,
        width: 90,
        height: 90,
        borderRadius: 10,
        backgroundColor: '#CB1D50',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThumbnailVideo: {
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    thumbnailVideo: {
        width: 114,
        height: 114,
        borderRadius: 5,
    },
    buttonPlay: {
        width: 40,
        height: 40,
        backgroundColor: '#fff',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50
    },
    iconPlay: {
        color: '#FFFFFF',
        fontSize: 45,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageBackgroundVideo: {
        width: 114,
        height: 114,
        justifyContent: 'center',
        alignItems: 'center',
    }
})