import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, View, Alert } from 'react-native';
import { Card, CardItem, Text, Icon, Right, Row, } from 'native-base';
import moment from 'moment-timezone';
import { NavigationActions } from 'react-navigation';
import { testID, checkNpaIdiEmpty, STORAGE_TABLE_NAME } from './../libs/Common';
import { AdjustTrackerConfig, AdjustTracker } from './../libs/AdjustTracker';
import AsyncStorage from '@react-native-community/async-storage';
import _ from 'lodash'
import { getData, KEY_ASYNC_STORAGE } from '../../src/utils';

let from = {
    DETAIL: 'DETAIL',
}

export default class CardItemCme extends Component {
    menu = null;

    constructor(props) {
        super(props);

        this.state = {
            from: this.props.navigation && this.props.navigation.state.routeName,
        }
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.DETAIL:
                if (this.state.from == 'Cme') {
                    if (this.props.data.done == "true") {
                        token = AdjustTrackerConfig.CME_SKP_Quiz
                    }
                    else {
                        token = AdjustTrackerConfig.CME_SKP_Quiz_NonTaken
                    }

                }
                else if (this.state.from == 'CmeHistory') {
                    if (!this.props.isManualSkp) {
                        token = AdjustTrackerConfig.CME_History_D2DSKP_Details
                    }
                    else if (this.props.isManualSkp) {
                        token = AdjustTrackerConfig.CME_History_ManualSKP_1
                    }
                }
                break;
            default: break;
        }
        if (token != '') {
            AdjustTracker(token);
        }
    }

    onPressCardItemCme = async (isDone) => {
        let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE)
        let dataSubmitCmeQuiz = await AsyncStorage.getItem(STORAGE_TABLE_NAME.SUBMIT_CME_QUIZ)
        if (checkNpaIdiEmpty(profile) && !_.isEmpty(dataSubmitCmeQuiz) && !isDone) {
            //if (checkNpaIdiEmpty(profile) && !isDone) {
            this.showAlertFillNpaIDI()
        }
        else {
            this.gotoMedicinusDetail(isDone)
        }

    }

    gotoProfile = () => {
        let data = {
            from: this.state.from,
            ...this.props.data,
            backFromCertificate: this.state.from,
            resubmitQuiz: true
        }

        let routeName = "Profile"
        getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
            if (profile.country_code == "ID") {
                routeName = "ChangeProfile"
            }
            this.props.navigation.dispatch(NavigationActions.navigate({
                routeName: routeName,
                params: data
            }));
        });

    }

    showAlertFillNpaIDI = () => {
        Alert.alert(
            'Reminder',
            'Please input your Medical ID first before you attempt your next CME Quiz',
            [
                // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'OK', onPress: () => this.gotoProfile(),
                },
            ],
            { cancelable: true }
        );
    }

    gotoMedicinusDetail = async (isDone) => {
        let profile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE)
        this.sendAdjust(from.DETAIL)
        // AdjustTracker(AdjustTrackerConfig.CME_SKP_Quiz);
        let paramCertificate = {
            title: this.props.data.title,
            certificate: this.props.data.certificate,
            isRead: this.props.data.status != "invalid" ? true : false,
            isFromHistory: this.props.isFromHistory,
            from: this.state.from,
            isManualSkp: this.props.isManualSkp,
            typeCertificate: this.props.data.type,
            isCertificateAvailable: true
        }

        if (this.props.isManualSkp == true) {

            this.gotoCertificate(paramCertificate)

        } else {
            if (isDone) {
                let type = this.props.data.type

                if (type == 'event' || type == 'webinar') {
                    this.gotoCertificate(paramCertificate)
                }

                else {
                    if (checkNpaIdiEmpty(profile) == false) {
                        paramCertificate.popCounter = 1

                        this.gotoCertificate(paramCertificate)
                    }
                    else if (checkNpaIdiEmpty(profile) == true) {
                        let data = {
                            from: this.state.from,
                            ...this.props.data,
                            backFromCertificate: this.state.from
                        }
                        this.props.navigation.dispatch(NavigationActions.navigate({
                            routeName: 'CmeQuiz',
                            params: data,
                            key: 'goto'
                        }));
                    }
                }
            }
            else {
                let tempData = {
                    from: this.state.from,
                    ...this.props.data,
                    backFromCertificate: this.state.from
                }
                this.props.navigation.dispatch(NavigationActions.navigate({
                    routeName: 'CmeDetail',
                    params: tempData, key: `webinar-${this.props.data.id}`
                }));
            }

        }


    }

    gotoCertificate(paramCertificate) {
        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'CmeCertificate',
            params: paramCertificate, key: `webinar-${this.props.data.id}`
        }));
    }
    render() {
        let formatDate = 'YYYY-MM-DD HH:mm:ss';
        let isFromHistory = this.props.isFromHistory == true ? true : false;
        let expired = moment(this.props.isManualSkp == true ?
            this.props.data.taken_on : this.props.data.available_end, formatDate).format('DD MMM YYYY');
        let isDone = (this.props.isManualSkp == true && this.props.data.status != 'invalid') ||
            (this.props.isManualSkp != true && this.props.data.done == 'true') ||
            (isFromHistory == true && this.props.isManualSkp != true) ? true : false;

        let cover = this.props.data.logo;
        let textDate = this.props.isManualSkp == true ? 'Created on ' + expired :
            isFromHistory == true ? '' : 'Available until ' + expired;
        let isRejected = (this.props.isManualSkp == true && this.props.data.status == 'rejected') ? true : false;

        let type = this.props.data.type
        if (type == 'cme') {
            type = 'CME'
        }
        else if (type == 'event') {
            type = 'Event'
        }
        else if (type == 'offline') {
            type = 'Offline'
        }
        else if (type == 'webinar') {
            type = 'Webinar'
        }

        return (
            <Card style={{ padding: 5 }}>
                <TouchableOpacity
                    {...testID('card_quiz')}
                    activeOpacity={0.7} onPress={() => this.onPressCardItemCme(isDone)}>
                    <CardItem bordered>
                        <View style={{
                            padding: 5,
                            flexDirection: 'row',
                        }}>
                            {cover != null && (<Image
                                source={{ uri: cover }}
                                defaultSource={require('../assets/images/preloader.png')}
                                style={styles.imageBackgroundVideo}>
                            </Image>)}
                            <View style={{ flex: 1, alignItems: 'flex-start', marginHorizontal: 10 }}>
                                <Text /*{...testID('titleCme')}*/ style={styles.itemTitle} numberOfLines={4}>{this.props.data.title}</Text>
                            </View>
                        </View>

                    </CardItem>
                    <CardItem nopadding>
                        <View style={styles.viewBottom}>
                            <View style={styles.backgroundTotalSkp}>
                                <Text nopadding style={styles.textTotalSkp}>{this.props.data.skp} SKP</Text>
                            </View>
                            {isFromHistory &&
                                <View style={[styles.backgroundTotalSkp, { marginHorizontal: 5 }]}>
                                    <Text nopadding style={styles.textTotalSkp}>{type}</Text>
                                </View>
                            }

                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <Text nopadding style={styles.textTotalSkp}>{textDate}</Text>
                            </View>
                        </View>
                    </CardItem>
                </TouchableOpacity>

                {isDone && (<Icon
                    name={isRejected ? 'ios-close-circle' : 'ios-checkmark-circle'}
                    style={[styles.iconStatus, { color: isRejected ? '#CB1D50' : '#3AE194' }]} />)}


            </Card>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 16,
        color: '#425062'
    },
    imageBackgroundVideo: {
        width: 110,
        height: 110,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    itemTitle: {
        fontFamily: 'Nunito',
        fontWeight: 'bold',
        color: '#454F63',
        marginVertical: 10,
        fontSize: 16,
    },
    viewBottom: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundTotalSkp: {
        backgroundColor: '#F0F0F0',
        borderRadius: 25,
        paddingHorizontal: 20,
        paddingVertical: 5,
        justifyContent: 'center'
    },
    textTotalSkp: {
        fontSize: 13,
        color: '#78849E',
    },
    iconStatus: {
        // color: '#3AE194',
        position: 'absolute',
        right: 10,
        top: 7,
        fontSize: 25,
        alignItems: 'center'
    },
    labelStatus: {
        color: '#78849E',
        marginRight: 5,
    }
});