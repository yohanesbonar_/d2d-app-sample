import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Platform, Image } from 'react-native';
import platform from '../../theme/variables/d2dColor'
import {
    Content, Button, Text, Col, Item, Input, Label, Icon, Toast, Form
} from 'native-base';
import { testID, sendTopicCertificate, STORAGE_TABLE_NAME, getCountryCodeProfile, validateEmail, downloadFile } from '../libs/Common';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader, Popup } from '../components'
import { submitWebinarCertificate, submitSKP } from '../libs/NetworkUtility'
import { translate } from '../libs/localize/LocalizeHelper'


export default class PopupInputCertificate extends Component {
    timer = null
    countryCode = null
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isInputName: false,
            namaGelar: '',
            typeModal: 'inputName',
            showLoader: false,
            isLoading: false,
            isSuccesSubmit: false,
            data: this.props.data,
            type: this.props.type,
            isCertificateAvailable: false,
            emailAddress: '',
            isInputEmail: false,
            emailError: false,
            errorMessageEmail: ""
        }
    }

    componentDidMount() {
        console.log(this.state.data)
        this.initData()
    }

    initData = async () => {
        this.countryCode = await getCountryCodeProfile()
        let getJsonProfile = await AsyncStorage.getItem(STORAGE_TABLE_NAME.PROFILE);
        let dataProfile = JSON.parse(getJsonProfile);
        this.setState({
            namaGelar: dataProfile.name
        })
        if (this.state.type == "sendEmail") {
            this.setState({
                typeModal: 'sendEmail',
                modalVisible: true,
                isInputEmail: true
            })
        }
    }

    handleVisibleModalChange = (isShow) => {
        this.props.onVisibleModal(isShow);
    }

    closeThanksDialog() {
        this.props.onVisibleModal(false);
        this.props.closeGoToCertificate();
        clearTimeout(this.timer);
    }

    onChangeTextName = (value) => {
        if (value.trim().length > 0) {
            this.setState({
                isInputName: true,
                namaGelar: value
            })
        }
        else {
            this.setState({
                isInputName: false,
                namaGelar: value
            })
        }
    }

    onPressSubmit = (typeOnPress) => {
        if (typeOnPress === 'submitInput') {
            //this.isContainEmoji(this.state.namaGelar)
            if (this.state.namaGelar.trim().length > 0) {
                this.setState({
                    typeModal: 'confirmation'
                })
            }
        } else if (typeOnPress === 'submitConfirmation') {

            if (this.state.namaGelar.trim().length > 0) {
                this.setState({
                    isLoading: true,
                    typeModal: 'messageThanks'
                })
                this.doSubmitCertificateWebinar();
            }
        } else if (typeOnPress === 'submitEmail') {
            if (this.state.emailAddress == "") {
                this.setState({
                    emailError: true,
                    errorMessageEmail: "Email is required"
                })
            }
            else if (validateEmail(this.state.emailAddress) === true) {
                this.setState({
                    emailError: false,
                    errorMessageEmail: ""
                })
                this.onPressSubmitEmail();
            } else {
                this.setState({
                    emailError: true,
                    errorMessageEmail: "Invalid email format"
                })
            }
        }

    }

    onPressSubmitEmail = () => {

        this.setState({
            modalVisible: false,
        })
        this.props.onVisibleModal(false);
        this.props.handlerStatusSubmitEmail(this.state.emailAddress);
    }


    async doSubmitCertificateWebinar() {
        this.setState({ showLoader: true })

        try {
            let params = {
                webinar_id: this.state.data.id,
                fullname: this.state.namaGelar
            }
            let response = await submitWebinarCertificate(params);

            let paramsSubmitSkp = {
                content_id: this.state.data.id
            }
            let responseSubmitSKP = await submitSKP('webinar', paramsSubmitSkp)
            console.log('submitWebinarCertificate', response)
            console.log('responseSubmitSKP', responseSubmitSKP)

            if (response.isSuccess && responseSubmitSKP.isSuccess) {
                if (this.state.type == 'webinar') {
                    this.props.handlerStatusSubmit(response.docs)

                    if (this.countryCode != "ID") {
                        let isDownloaded = await downloadFile(response.docs.filename, true)
                        console.log("isDownloaded: ", isDownloaded)
                        if (isDownloaded) {
                            this.setState({
                                typeModal: 'downloaded',
                                namaGelar: '',
                                isInputName: false,
                            });

                            this.timer = setTimeout(() => {
                                this.closeThanksDialog()
                            }, 3000)
                        }
                        else {
                            this.closeThanksDialog()
                        }
                    }
                    else {
                        this.closeThanksDialog()
                    }
                }
                else {
                    this.setState({
                        typeModal: 'messageThanks',
                        namaGelar: '',
                        isInputName: false,
                    });
                    this.props.handlerStatusSubmit(response.docs)
                }

                this.setState({ showLoader: false, isLoading: false })

            } else {
                this.handleVisibleModalChange(false)
                Toast.show({ text: response.message, position: 'top', duration: 3000 })
                this.setState({ showLoader: false, isLoading: false })
            }

        } catch (error) {
            console.log(error)
            Toast.show({ text: 'Something went wrong! ' + error, position: 'top', duration: 3000 })
            this.setState({ showLoader: false, isLoading: false })
        }

    }


    renderButtonClosePopup = () => {
        if (this.state.isLoading === false && this.state.typeModal != 'confirmation' && this.countryCode == "ID") {

            return (
                <TouchableOpacity style={styles.popupClose} onPress={() => this.onPressClosePopup()}>
                    <Text style={{ color: '#FFFFFF', marginTop: -2 }}>x</Text>
                </TouchableOpacity>
            )

        }
        return null
    }

    render() {
        return (
            <Content
                contentContainerStyle={{ justifyContent: 'center', flex: 1 }}
                centerContent={true}
            >
                <Loader transparent={true} visible={this.state.showLoader} />

                <View style={{ padding: 10, position: 'absolute', left: 0, right: 0 }}>
                    {this.state.isLoading ? (
                        <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>
                        </View>
                    ) :
                        <View>

                            {this.state.typeModal == "inputName" && this.countryCode != "ID" && (
                                <TouchableOpacity style={{ alignSelf: 'flex-end', marginBottom: 10, marginRight: -8 }} onPress={() => this.onPressClosePopup()}>
                                    <Image style={{ height: 48, width: 48 }} source={require("../assets/images/close-popup.png")}></Image>
                                </TouchableOpacity>)}
                            {this.state.typeModal == "sendEmail" && this.countryCode != "ID" && (
                                <TouchableOpacity style={{ alignSelf: 'flex-end', marginBottom: 10, marginRight: -8 }} onPress={() => this.onPressClosePopup()}>
                                    <Image style={{ height: 48, width: 48 }} source={require("../assets/images/close-popup.png")}></Image>
                                </TouchableOpacity>)}

                            {this.renderPopup()}
                        </View>

                    }

                    {this.renderButtonClosePopup()}

                </View>

            </Content>
        )
    }

    onPressClosePopup() {
        console.warn('onPressClosePopup')
        if (this.state.typeModal === 'inputName' || this.state.typeModal === 'confirmation' || this.state.typeModal === 'sendEmail') {
            this.handleVisibleModalChange(false);
        }
        else {
            this.closeThanksDialog();
        }
    }

    renderDownloaded = () => {
        let title = 'Downloaded'
        let message = "Thank you for completing your full name and degree due to data certificate purposes"
        return (
            <View style={{ flex: 0, flexDirection: 'column', backgroundColor: 'white', paddingHorizontal: 24, paddingVertical: this.state.isLoading === false ? 24 : 100, borderRadius: 8, zIndex: 1, }}>

                <Image
                    resizeMode='contain'
                    source={require('../assets/images/success-icon.png')}
                    style={{
                        height: 82,
                        width: 82,
                        marginBottom: 24,
                        alignSelf: 'center'
                    }}
                />
                <Text H6 style={{ marginBottom: 16, alignSelf: 'center' }}>{title}</Text>

                <Text Body1 style={{ textAlign: 'center' }}>{message}</Text>

            </View>
        )
    }
    renderPopup() {
        if (this.state.typeModal === 'inputName') {
            return this.renderInputCertificate();
        }
        else if (this.state.typeModal === 'confirmation') {
            return this.renderConfirmation();
        }
        else if (this.state.typeModal === 'messageThanks') {
            return this.renderMessageThanks();
        }
        else if (this.state.typeModal === 'sendEmail') {
            return this.renderSendEmail();
        }
        else if (this.state.typeModal === "downloaded") {
            return this.renderDownloaded()
        }

    }

    isContainEmoji(name) {
        let regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
        console.warn(regex.test(name))
        return regex.test(name);
    }


    renderInputCertificate() {

        if (this.state.isLoading === false) {
            if (this.countryCode != "ID") {
                return (
                    <View style={{ flex: 0, backgroundColor: 'white', flexDirection: 'column', paddingHorizontal: 16, paddingVertical: this.state.isLoading === false ? 24 : 100, borderRadius: 8, zIndex: 1, }}>

                        <Label black style={{ marginBottom: 16 }}>Full Name & Degree</Label>

                        <Item noShadowElevation>
                            <Input autoCapitalize="none"
                                newDesign
                                style={{ color: '#000000' }}
                                placeholder={'dr. Susilo Joko Soekarno S.pr'}
                                editable={true}
                                {...testID('input_nama_gelar')}
                                onChangeText={(txt) => this.onChangeTextName(txt)}
                                value={this.state.namaGelar}
                                keyboardType={Platform.OS === 'android' ? 'visible-password' : 'ascii-capable'} //disable emoji
                                autoFocus={true}
                            />
                        </Item>

                        <Text Body2 style={{ marginTop: 16 }}>Make sure your name and title are correct to get a correct certificate document.</Text>
                        <Button
                            {...testID('bußtton_submit')}
                            accessibilityLabel="button_submit"
                            style={[styles.btnSubmit, { backgroundColor: this.state.isInputName == true || this.state.namaGelar.trim().length > 0 ? '#D01E53' : '#C4C4C4' }]}
                            onPress={() => this.onPressSubmit('submitInput')} block>
                            <Text textButton uppercase={true}>Download Certificate</Text>
                        </Button>

                    </View>

                )
            }
            else {

                return (
                    <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>

                        <Col>
                            <Text style={styles.textMessageInputCertificate}>{translate("lengkapi_nama_certificate")}</Text>

                            <Label style={styles.textLabel}>{translate("nama_dan_gelar")}</Label>

                            <Item >
                                <Input autoCapitalize="none"
                                    placeholder={'dr. Susilo Joko Soekarno S.pr'}
                                    editable={true}
                                    {...testID('input_nama_gelar')}
                                    onChangeText={(txt) => this.onChangeTextName(txt)}
                                    value={this.state.namaGelar}
                                    keyboardType={Platform.OS === 'android' ? 'visible-password' : 'ascii-capable'} //disable emoji
                                    autoFocus={true}
                                />
                            </Item>

                            <Button
                                {...testID('button_submit')}
                                accessibilityLabel="button_submit"
                                style={[styles.btnSubmit, { backgroundColor: this.state.isInputName == true || this.state.namaGelar.trim().length > 0 ? '#3AE194' : '#C4C4C4' }]}
                                onPress={() => this.onPressSubmit('submitInput')} block>
                                <Text style={{ color: 'white', fontFamily: 'Nunito-Bold' }}>SUBMIT</Text>
                            </Button>

                        </Col>
                    </View>
                )
            }
        }
        return null;
    }

    renderSendEmail() {
        if (this.countryCode != "ID") {

            return (
                <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 16, paddingVertical: this.state.isLoading === false ? 24 : 100, borderRadius: 8, zIndex: 1, }}>

                    <Col>
                        <Label black style={{ fontSize: 20, marginBottom: 16 }}>Email</Label>

                        <Item noShadowElevation style={{ backgroundColor: "#EBEBEB", borderColor: "#EBEBEB", marginBottom: 5 }}>
                            <Input autoCapitalize="none"
                                newDesign
                                style={{ color: '#666666', backgroundColor: "#EBEBEB" }}
                                editable={true}
                                placeholder="Enter your email"
                                {...testID('input_email_address')}
                                value={this.state.emailAddress}
                                onChangeText={(txt) => this.onChangeTextEmail(txt)}
                            />
                        </Item>
                        {this.state.emailError && (
                            <Text textError style={styles.textError}>{this.state.errorMessageEmail}</Text>
                        )}

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 24
                        }}>
                            <TouchableOpacity
                                style={[styles.button, {
                                    backgroundColor: "#D01E53",
                                }]} onPress={() => this.onPressSubmit('submitEmail')} >
                                <Text textButton style={{ fontSize: 14 }}>SUBMIT</Text>
                            </TouchableOpacity>
                        </View>

                    </Col>
                </View>
            )
        }
        return null;
    }

    onChangeTextEmail = (value) => {
        if (value.trim().length > 0) {
            this.setState({
                isInputEmail: true,
                emailAddress: value
            })
            this.setState({
                emailError: false,
                errorMessageEmail: ""
            })
        }
        else {
            this.setState({
                isInputEmail: false,
                emailAddress: value
            })
        }


    }



    renderConfirmation() {
        if (this.state.isLoading === false) {
            if (this.countryCode != "ID") {

                return (
                    <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 16, paddingVertical: this.state.isLoading === false ? 24 : 100, borderRadius: 8, zIndex: 1, }}>

                        <Col>
                            <Label black style={{ fontSize: 20, marginBottom: 16 }}>Confirmation</Label>
                            <Text regular style={{ marginBottom: 24, color: "#000000" }}>Are you sure you want to use this name?</Text>

                            <Item noShadowElevation style={{ backgroundColor: "#FFF8E1", borderColor: "#FFF8E1" }}>
                                <Input autoCapitalize="none"
                                    newDesign
                                    style={{ color: '#000000', backgroundColor: "#FFF8E1" }}
                                    editable={false}
                                    {...testID('input_nama_gelar')}
                                    value={this.state.namaGelar}
                                />
                            </Item>

                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                marginTop: 24
                            }}>

                                <TouchableOpacity
                                    style={[styles.button, {
                                        backgroundColor: "#EBEBEB",
                                        marginRight: 16
                                    }
                                    ]} onPress={() => this.setState({ typeModal: 'inputName' })} >
                                    <Text textButtonCancel style={{ fontSize: 14 }}>NO</Text>
                                </TouchableOpacity>


                                <TouchableOpacity
                                    style={[styles.button, {
                                        backgroundColor: "#D01E53",
                                    }]} onPress={() => this.onPressSubmit('submitConfirmation')} >
                                    <Text textButton style={{ fontSize: 14 }}>YES</Text>
                                </TouchableOpacity>
                            </View>

                        </Col>
                    </View>

                )
            }
            else {
                return (
                    <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>

                        <Col>
                            <Text style={{
                                fontSize: 16,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                marginTop: 15,
                                marginHorizontal: 25
                            }}>Are you sure to use this name to your certificated?</Text>

                            <Text style={{
                                fontSize: 20,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                marginTop: 15,
                                marginHorizontal: 25
                            }}>{this.state.namaGelar}</Text>
                            <Text style={{
                                fontSize: 14,
                                marginTop: 15,
                                color: '#6C6C6C',
                                fontFamily: 'Nunito-Regular'
                            }}>* The name you entered can not be changed once you submited</Text>

                            <View style={{
                                flex: 1,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>

                                <Button
                                    {...testID('button_no')}
                                    accessibilityLabel="button_no"
                                    style={[styles.btnSubmit, { backgroundColor: 'white', borderColor: '#3AE194', borderWidth: 2, flex: 1, marginRight: 10 }]} onPress={() => this.setState({ typeModal: 'inputName' })} block>
                                    <Text style={{ textDecorationLine: 'underline', color: '#3AE194', fontFamily: 'Nunito-Bold' }}>NO</Text>
                                </Button>

                                <Button
                                    {...testID('button_yes')}
                                    accessibilityLabel="button_yes"
                                    style={[styles.btnSubmit, { backgroundColor: '#3AE194', flex: 1, marginLeft: 10 }]} onPress={() => this.onPressSubmit('submitConfirmation')} block>
                                    <Text style={{ color: 'white', fontFamily: 'Nunito-Bold' }}>YES</Text>
                                </Button>

                            </View>
                        </Col>
                    </View>
                )
            }
        }
        return null;
    }

    renderMessageThanks() {
        if (this.state.isLoading === false && this.state.type != 'webinar') {
            return (
                <View style={{ flex: 0, backgroundColor: 'white', paddingHorizontal: 20, paddingVertical: this.state.isLoading === false ? 20 : 100, borderRadius: 10, zIndex: 1, }}>

                    <Col>
                        <View style={{
                            marginTop: 40,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <View style={styles.circleIcon}>
                                <Icon
                                    name={'ios-checkmark'}
                                    style={{
                                        fontSize: 100,
                                        color: '#FFFFFF'
                                    }} />
                            </View>
                        </View>

                        <Text style={styles.textMessageTerimakasih}>{translate("message_after_complete_name_certificate")}</Text>
                    </Col>
                </View>
            )
        }
        return null;
    }
}

const styles = StyleSheet.create({
    ///style modal
    popupClose: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#78849E',
        width: 24,
        height: 24,
        borderRadius: 12,
        position: 'absolute',
        right: Platform.OS == 'ios' ? 3 : 3,
        top: Platform.OS == 'ios' ? 3 : 3,
        zIndex: 2,
    },

    textMessageInputCertificate: {
        color: '#454F63',
        fontSize: 16,
        fontFamily: 'Nunito-Bold',
        textAlign: 'center',
        marginTop: 15,
        marginHorizontal: 25
    },

    textMessageTerimakasih: {
        color: '#454F63',
        fontSize: 18,
        fontFamily: 'Nunito-Bold',
        textAlign: 'center',
        marginTop: 10,
        marginHorizontal: 10,
    },

    textLabel: {
        marginTop: 25,
        marginBottom: 5
    },
    btnSubmit: {
        marginTop: 24,
    },

    circleIcon: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 1,
        backgroundColor: platform.brandSuccess,
        borderColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        shadowColor: "#00000029",
        shadowOpacity: 0.8,
        shadowRadius: 1,
        shadowOffset: {
            height: 1,
            width: 0
        },

    },

    button: {
        height: 48,
        flex: 1,
        borderRadius: 4,
        justifyContent: 'center',
    }, textError: {
        marginTop: 2,
        marginBottom: -2,
        marginLeft: 5
    }
})
