import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Grid } from 'react-native';
import { Body, Right, Icon } from 'native-base';
import platform from '../../theme/variables/d2dColor';

export default class CheckList extends Component {

    state   = {
        isChecked:null,
    }

    constructor(props) {
        super(props);           
    }



    onChecked(data, checked) {
        this.props.action(data, checked);
        this.props.data.isChecked = checked;
        this.setState({isChecked: checked});
    }

    render () {
        const data = this.props.data;

        return (
          <TouchableOpacity style={styles.container} activeOpacity={0.7} {...this.props} 
                            onPress={() => this.onChecked({id: data.id, title: data.title, meta_title : data.meta_title}, data.isChecked === true? false : true)}>
                <Text style={styles.text}>{data.description}</Text>
                {/* <Right> */}
                   <Icon 
                       name={data.isChecked === true ? 'ios-checkmark-circle' : 'ios-radio-button-off'}
                       style={[styles.iconChecklist, {color: data.isChecked === true ? '#3ACCE1' : '#9E9FA1',}]} />
                       {/* </Right> */}
           </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    text:{
        fontFamily: 'Nunito-Bold',
        fontSize: 15,
        color: '#454F63',
        flex: 0.85,
        marginHorizontal: 5
    },
    container: {
        flexDirection: 'row',
        borderColor: '#3ACCE1',
        borderWidth: 2,
        borderRadius: 10,
        marginVertical: 5,
        paddingHorizontal: 5,
        paddingVertical: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconChecklist:{
        fontSize:30, 
        flex: 0.15, 
        textAlign: 'right', 
        marginHorizontal: 5,
    }
});