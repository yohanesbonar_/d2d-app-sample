import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, View } from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import ViewMoreText from 'react-native-view-more-text';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body } from 'native-base';
import { CardButton } from './';
import platform from '../../theme/variables/platform';

export default class CardItemVideo extends Component {
    menu = null;

    constructor(props) {
        super(props);           
    }

    setMenuRef = (ref) => {
        this.menu = ref;
    };

    _renderImage = () => {
        const VideoView = { type: "Navigate", routeName: "VideoView", params: {title: this.props.data.title} }
        return this.props.data.image ? (
            <View style={styles.iconPlayWrapper}>
                <Image 
                    source={{uri: this.props.data.image}}
                    defaultSource={require('../assets/images/preloader.png')}
                    style={styles.postImage} />

                <Button light style={styles.buttonPlay} onPress={() => this.props.navigation.navigate(VideoView)}>
                    <Icon name='ios-play' style={styles.iconPlay} />
                </Button>
            </View>
        ) : null;
    }

    _renderViewMore(onPress){
        return(
          <Text style={{paddingHorizontal: 10}} onPress={onPress}>more</Text>
        )
    }

    _renderViewLess(onPress){
        return(
          <Text style={{paddingHorizontal: 10}} onPress={onPress}>less</Text>
        )
    }

    render () {
        return (
            <Card style={{flex: 0}}>
                <CardItem>
                    <Left>
                        <Thumbnail 
                            source={{uri: this.props.data.avatar}}
                            defaultSource={require('../assets/images/preloader.png')}
                            small />
                        <Body>
                            <Text style={styles.title}>{this.props.data.title}</Text>
                            <Text note>{this.props.data.date}</Text>
                        </Body>
                        <Right>
                            <Menu style={{padding: 0}}
                                ref={this.setMenuRef}
                                button={<Button style={styles.moreBtn} transparent onPress={() => this.menu.show()}>
                                            <Icon name='md-more' nopadding style={{color: '#D0D8E6', fontSize: 24}} />
                                        </Button>}>
                                <MenuItem textStyle={{fontFamily: 'Nunito-Regular', fontSize: 13}} onPress={() => this.menu.hide()}>Menu item 1</MenuItem>
                                <MenuItem textStyle={{fontFamily: 'Nunito-Regular', fontSize: 13}} onPress={() => this.menu.hide()}>Menu item 2</MenuItem>
                                <MenuItem textStyle={{fontFamily: 'Nunito-Regular', fontSize: 13}} onPress={() => this.menu.hide()}>Menu item 3</MenuItem>
                            </Menu>
                        </Right>
                    </Left>
                </CardItem>
                <TouchableOpacity activeOpacity={0.9}>
                    <CardItem nopadding>
                        <Body>
                            <ViewMoreText
                                numberOfLines={2}
                                renderViewMore={this._renderViewMore}
                                renderViewLess={this._renderViewLess}
                                textStyle={{textAlign: 'left', paddingHorizontal: 10}}>
                                    <Text style={styles.postText}>{this.props.data.text}</Text>
                            </ViewMoreText>
                            {this._renderImage()}
                        </Body>
                    </CardItem>
                </TouchableOpacity>
                <CardButton 
                    like={this.props.data.like} 
                    likes={this.props.data.likes} 
                    bookmark={this.props.data.bookmark} 
                    bookmarks={this.props.data.bookmarks} 
                    comments={this.props.data.comments} 
                    type={this.props.data.type}
                    user={this.props.data.userId} />
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 16,
        color: '#425062'
    },
    moreBtn: {
        padding: 1,
    },
    postText: {
        fontSize: 14,
        padding: 10,
        color: '#6C6C6C',
        textAlign: 'left'
    },
    postImage: {
        width: platform.deviceWidth - 25, 
        height: 200,
    },
    iconPlayWrapper: {
        width: platform.deviceWidth - 25, 
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonPlay: {
        width: 60,
        height: 60,        
        borderRadius: 50,
        position: 'absolute',
        left: (platform.deviceWidth/2) - 40,
        paddingLeft: 6,
        paddingTop: 5,
        backgroundColor: '#fff',
    },
    iconPlay: {
        color: '#26B1EF',
        fontSize: 48,
        justifyContent: 'center',
        alignItems: 'center',
    }
});