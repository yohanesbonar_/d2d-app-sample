import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, ImageBackground, Image, ScrollView } from 'react-native';
import { Card, Thumbnail, Text, Icon, Row, Right, Col, } from 'native-base';
import platform from '../../theme/variables/platform';
import moment from 'moment-timezone';
import { NavigationActions } from 'react-navigation';
import { testID, saveHistorySearch, EnumTypeHistorySearch, isStarted } from './../libs/Common';
import { Tag } from './../components';
import { translate } from '../libs/localize/LocalizeHelper';
import { AdjustTracker, AdjustTrackerConfig } from '../libs/AdjustTracker';
import AsyncStorage from '@react-native-community/async-storage';
import { getLocalize, formatTimeByOffset, getData, KEY_ASYNC_STORAGE } from '../../src/utils';
export default class CardItemWebinar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            bookmark: this.props.data.flag_bookmark == 0 ? false : true,
            reserved: this.props.data.flag_reserve == 0 ? false : true,
            from: this.props.navigation && this.props.navigation.state.routeName,
            valueSearchHistory: this.props.valueSearchHistory ? this.props.valueSearchHistory : "",
            country_code: this.props.country_code
        }
    }

    shownDate(startDate, endDate, showDiff) {
        let result = 'Coming Soon';
        let formatDate = 'YYYY-MM-DD HH:mm:ss';

        if (this.state.country_code != "ID") {
            // get device timezone eg. -> "Asia/Shanghai"
            const deviceTimeZone = getLocalize().getTimeZone();

            // force set startDate to timezone Asia/Jakarta because in cms only set date as WIB
            let startDateWIB = moment.tz(startDate, "Asia/Jakarta").format();
            let endDateWIB = moment.tz(endDate, "Asia/Jakarta").format();

            let localStartDate = moment(startDateWIB).tz(deviceTimeZone).format(formatDate)
            let localEndDate = moment(endDateWIB).tz(deviceTimeZone).format(formatDate)

            endDate = localEndDate
            startDate = localStartDate
        }

        if (startDate != null && startDate != '') {
            let postfixWIB = ''
            if (this.state.country_code == 'ID') {
                postfixWIB = ' WIB'
            }

            result = moment(startDate, formatDate).format('DD MMM YYYY HH:mm') + postfixWIB;


            if (moment().isSameOrAfter(moment(startDate, formatDate))) {
                // result = 'Live';

                if (endDate != null && endDate != '') {
                    if (moment().isAfter(moment(endDate, formatDate))) {
                        if (showDiff) {
                            result = moment(endDate, formatDate).fromNow();
                        } else {
                            result = moment(endDate, formatDate).format('DD MMM YYYY');
                            // let now = moment(new Date());
                            // let end = moment(endDate, formatDate);
                            // if(now.diff(end, 'days')){
                            //     result = moment(endDate, formatDate).format('DD MMM YYYY');
                            // }else{
                            //     result = 'Jam ' + moment(endDate, formatDate).format('HH.mm') + ' WIB';
                            // }
                        }
                    } else if (showDiff) {
                        result = 'Live';
                    }
                }
            } else {
                if (showDiff) {
                    result = moment(startDate, formatDate).fromNow();
                }
            }

        }

        return result;
    }

    sendAdjust = () => {
        let token = '';
        let dateDiff = this.shownDate(this.props.data.start_date, this.props.data.end_date, true);
        if (this.state.from && this.state.from == 'Feeds')
            token = AdjustTrackerConfig.Feeds_Webinar
        else if (dateDiff == 'Live')
            token = AdjustTrackerConfig.Webinar_Webinar_Live
        else
            token = AdjustTrackerConfig.Webinar_Video_View

        if (token != '') {
            AdjustTracker(token);
        }
    }


    gotoWebinarVideo = async (data) => {

        if (this.state.valueSearchHistory != "") {
            saveHistorySearch(EnumTypeHistorySearch.WEBINAR, this.state.valueSearchHistory)
        }

        let tempData = {
            isPaid: data.exclusive == 1 ? true : false,
            from: this.state.from,
            ...data
        }


        let dateShowTNC = moment(data.start_date).add(-1, 'hours').format('YYYY-MM-DD HH:mm:ss')

        let isStartShowTNC = isStarted(data.server_date, dateShowTNC)
        console.log("log webinar list tempData", tempData);
        console.log("log webinar list isStartShowTNC", isStartShowTNC);
        let routeNameDestination = data.has_tnc == true && isStartShowTNC == true ? "WebinarTNC" : "WebinarVideo";
        this.props.navigation.dispatch(NavigationActions.navigate({ routeName: routeNameDestination, params: tempData, key: `webinar-${data.id}` }));
    }

    renderInfoSkp = (dataWebinar) => {
        //handle layout endpoint feeds
        if (dataWebinar.skp == undefined ||
            (dataWebinar.skp == 0 && dataWebinar.pretest == 0 && dataWebinar.posttest == 0 && dataWebinar.video_360 == 0)) {
            return null
        }
        else {
            return <View style={styles.containerInfoSkp}>
                <Right>
                    <Row style={{ alignItems: 'center' }}>

                        {dataWebinar.video_360 > 0 &&
                            <Row style={{ alignItems: 'center', flex: 0 }} >
                                <Text style={styles.textInfoSkp}>360°</Text>
                                <Image source={require('./../assets/images/icon-verified.png')} style={styles.iconVerified} />
                            </Row>
                        }

                        {dataWebinar.skp > 0 &&
                            <Row style={{ alignItems: 'center', flex: 0 }} >
                                <Text style={styles.textInfoSkp}>{translate("skp_label")}</Text>
                                <Image source={require('./../assets/images/icon-verified.png')} style={styles.iconVerified} />
                            </Row>
                        }

                        {dataWebinar.pretest > 0 &&
                            <Row style={{ alignItems: 'center', flex: 0 }} >
                                <Text style={styles.textInfoSkp}>Pretest</Text>
                                <Image source={require('./../assets/images/icon-verified.png')} style={styles.iconVerified} />
                            </Row>
                        }

                        {dataWebinar.posttest > 0 &&
                            <Row style={{ alignItems: 'center', flex: 0 }} >
                                <Text style={styles.textInfoSkp}>Postest</Text>
                                <Image source={require('./../assets/images/icon-verified.png')} style={styles.iconVerified} />
                            </Row>
                        }

                    </Row>
                </Right>

            </View>
        }
    }

    render() {
        let data = this.props.data;
        data.isLive = this.props.isLive;
        let dataWebinar = this.props.data;
        let title = dataWebinar.title;
        let date = this.shownDate(dataWebinar.start_date, dataWebinar.end_date, false);
        let dateDiff = this.shownDate(dataWebinar.start_date, dataWebinar.end_date, true);
        dataWebinar.isLive = dateDiff == 'Live' ? true : false;
        //if (data.isLive == false) {
        let views = typeof (dataWebinar.views) != 'undefined' ? dataWebinar.views : dataWebinar.total_view
        date = views + ' Views . ' + date;
        //}
        let uriCover = dataWebinar.cover != null ? { uri: dataWebinar.cover } : null;

        return (
            <TouchableOpacity
                {...testID('button_card_webinar')}
                nopadding activeOpacity={0.7}
                onPress={() => {
                    this.sendAdjust();
                    // AdjustTracker(AdjustTrackerConfig.Webinar_Video_View);
                    this.gotoWebinarVideo(data)
                }}>
                <Card nopadding style={{ width: '100%' }}>

                    <View style={styles.viewThumbnailVideo}>
                        <ImageBackground
                            imageStyle={{ borderRadius: 5 }}
                            source={uriCover}
                            defaultSource={require('../assets/images/preloader.png')}
                            style={styles.imageBackgroundVideo}>
                            <View style={styles.viewOpacity} />
                            <View style={{ position: 'absolute', opacity: 0.8, bottom: 0, borderRadius: 5, right: 0, margin: 15, backgroundColor: '#CB1D50', paddingVertical: 5, paddingHorizontal: 10 }}>
                                <Text style={{ color: '#fff', fontSize: 14 }}>{dateDiff}</Text>
                            </View>
                            <Icon
                                {...testID('iconPlay_webinar')}
                                type='MaterialIcons' name='play-circle-filled' style={styles.iconPlay} />
                        </ImageBackground>


                        <View style={styles.viewBottom}>
                            {/* <Thumbnail style={styles.roundedImage} source={uriImage} /> */}
                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                <Text /*{...testID('titleWebinar')}*/ style={styles.textTitle} numberOfLines={2}>{title}</Text>
                                <Text style={styles.textDesc} numberOfLines={1}>{date}</Text>
                            </View>
                        </View>
                    </View>
                    {this.renderInfoSkp(dataWebinar)}
                </Card>
            </TouchableOpacity>

        )
    }
}

const styles = StyleSheet.create({
    imageBackgroundVideo: {
        width: '100%',
        height: platform.deviceHeight / 3.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewOpacity: {
        width: '100%',
        height: platform.deviceHeight / 3.5,
        backgroundColor: '#CB1D50',
        position: 'absolute',
        top: 0,
        borderRadius: 4,
        opacity: 0.25
    },
    viewThumbnailVideo: {
        width: '100%',
        flex: 1,
        // borderRadius:3,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    iconPlay: {
        color: '#FFFFFF',
        fontSize: 45,
        justifyContent: 'center',
        alignItems: 'center',
    },
    roundedImage: {
        width: 45,
        height: 45,
        borderWidth: 2,
        borderColor: '#FFFFFF',
        borderRadius: 25,
        marginRight: 15,
    },
    textTitle: {
        fontSize: 16,
        color: '#454545',
        fontWeight: 'bold',
    },
    textDesc: {
        fontSize: 12,
        color: '#78849E'
    },
    viewBottom: {
        flex: 1,
        flexDirection: 'row',
        margin: 15,
        justifyContent: 'center',
        // alignItems : 'center',
    },
    containerInfoSkp: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        borderTopWidth: 1.5,
        borderTopColor: '#F4F4F6FD',
        paddingVertical: 10,
        marginHorizontal: 10
    },
    textInfoSkp: {
        color: '#78849E',
        fontSize: 12,
        marginRight: 4
    },
    iconVerified: {
        width: 24,
        height: 24,
        marginRight: 10
    },
    areaSpecialist: {
        flex: 1,
        marginTop: -15,
        marginHorizontal: 15,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'center',
    }
})
