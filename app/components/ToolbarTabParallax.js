import React, { Component } from 'react';
import { Animated, Platform, TouchableOpacity, View } from "react-native";
import { Container, Header, Left, Body, Right, Button, Icon, Title, Tab, Tabs, TabHeading, ScrollableTab, Text, List, Item } from 'native-base';
import platform from '../../theme/variables/d2dColor';

const COVER_HEIGHT  = 200;
const SCROLL_HEIGHT = COVER_HEIGHT - platform.toolbarHeight;

export default class ToolbarTabParallax extends Component {
    scroll  = new Animated.Value(0);
    nScroll = new Animated.Value(0);

    static defaultProps = {
        ...Component.defaultProps,
        coverHeight: COVER_HEIGHT,
        backgroundColor: 'transparent', //platform.toolbarDefaultBg
        tabSize: 3
    }

    constructor(props) {
        super(props);
        this.nScroll.addListener(Animated.event([{value: this.scroll}], {useNativeDriver: false}));
        SCROLL_HEIGHT = this.props.coverHeight - platform.toolbarHeight;
    }
    
    heights = [500, 500];

    scrollY = this.nScroll.interpolate({
        inputRange: [0, SCROLL_HEIGHT/1, SCROLL_HEIGHT + 1],
        outputRange: [0, 0, 1]
    });

    opacityContent = this.nScroll.interpolate({
        inputRange: [0, SCROLL_HEIGHT/2],
        outputRange: [1, 0],
    });

    headerBg = this.scroll.interpolate({
        inputRange: [0, SCROLL_HEIGHT],
        outputRange: ["transparent", platform.toolbarDefaultBg],
        extrapolate: "clamp"
    });

    tabContent = (x, i) => (
        <List>
            {new Array(x).fill(null).map((_, i) => <Item key={i} style={{ paddingHorizontal: 5, paddingVertical: 10 }}><Text>Item {i}</Text></Item>)}
        </List>
    );

    render() {
        return (
            <Container>
                <Animated.View style={{position: "absolute", width: "100%", zIndex: 1, backgroundColor: this.headerBg}}>
                    {this.props.header}
                </Animated.View>
                
                <Animated.ScrollView
                    scrollEventThrottle={5}
                    showsVerticalScrollIndicator={false}
                    onScroll={Animated.event([{nativeEvent: {contentOffset: {y: this.nScroll}}}], {useNativeDriver: true})}
                    style={{zIndex: 0}}>

                    <Animated.View style={{ backgroundColor: this.props.backgroundColor, height: this.props.coverHeight, width: "100%", justifyContent: 'flex-end' }}>
                        <Animated.View style={{opacity: this.opacityContent, padding: 20, paddingVertical: 40 }}>
                            {this.props.info}
                        </Animated.View>
                    </Animated.View>

                    <Tabs
                        prerenderingSiblingsNumber={3}
                        renderTabBar={(props) => 
                        <Animated.View
                            style={{transform: [{translateY: this.scrollY}], zIndex: 1, width: "100%"}}>
                            <ScrollableTab {...props}
                                style={{ backgroundColor: platform.tabBgColor }}
                                renderTab={(name, page, active, onPress, onLayout) => (
                                    <TouchableOpacity key={page}
                                        onPress={() => onPress(page)}
                                        onLayout={onLayout}
                                        activeOpacity={0.9}>
                                        <Animated.View style={{ 
                                            flex: 1, 
                                            height: 100, 
                                            backgroundColor: platform.tabBgColor,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                            <TabHeading scrollable
                                                style={{ width: platform.deviceWidth / this.props.tabSize, height: platform.toolbarHeight }}
                                                active={active}>
                                                <Animated.Text style={{
                                                    fontFamily: active ? 'Nunito-Bold' : 'Nunito-Regular',
                                                    color: active ? platform.topTabBarActiveTextColor : platform.topTabBarTextColor,
                                                    fontSize: platform.tabFontSize
                                                }}>
                                                    {name}
                                                </Animated.Text>
                                            </TabHeading>
                                        </Animated.View>
                                    </TouchableOpacity>
                                )}
                                underlineStyle={{backgroundColor: platform.topTabBarActiveBorderColor}}/>
                        </Animated.View>
                    }>
                        {this.props.tabsContent.map((v, i) => v)}
                    </Tabs>
                </Animated.ScrollView>
            </Container>
        );
      }
}