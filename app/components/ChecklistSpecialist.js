import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Grid } from 'react-native';
import { Body, Right, Icon } from 'native-base';
import platform from '../../theme/variables/d2dColor';
import { NavigationActions } from 'react-navigation';
import { translate } from '../libs/localize/LocalizeHelper'
export default class CheckListSpecialist extends Component {

    state = {
        isChecked: null,
    }

    constructor(props) {
        super(props);
    }



    onChecked(data, checked) {
        console.log("onChecked data: ", data)
        // jangan hapus dulu
        // if (data.subSpecialistList != null && data.subSpecialistList.length > 0 &&
        //     this.props.from == 'SPECIALIST') {
        //     this.props.gotoSubSpesialis(data)
        // } else {
        //     this.props.data.isChecked = checked;
        //     this.setState({ isChecked: checked })
        // }

        if (this.props.from == 'SPECIALIST') {
            if (data.subspecialist != null && data.subspecialist.length > 0) {
                this.props.gotoSubSpesialis(data)
            } else {
                // Select Spesialist GP
                this.props.data.isChecked = checked;
                this.setState({ isChecked: checked })
            }
        } else if (this.props.from == 'SPECIALIST_REQ_JOURNAL') {
            this.props.data.isChecked = checked;
            this.setState({ isChecked: checked })
        } else if (this.props.from == 'FILTER') {
            this.props.data.isChecked = checked;
            this.setState({ isChecked: checked })
        } else {
            this.props.data.isChecked = checked;
            this.setState({ isChecked: checked })

            if (data.id == 0) {
                if (this.props.from == 'SUBSCRIBTIONS') {
                    //check all subs
                    this.props.doCheckAll(checked);
                } else if (checked == true) {
                    //sub spesialist --> tidak ada spesialist --> unCheck
                    this.props.unCheckAll();
                }
            } else if (this.props.from != 'SUBSCRIBTIONS') {
                this.props.isSubSelected();
            } else if (this.props.from == 'SUBSCRIBTIONS') {
                this.props.isSpSelected();
            }

        }
    }

    render() {
        let data = this.props.data;
        // console.log("CheckListSpecialist data: ", data)
        if (typeof (data.id) == "undefined") {
            data.id = data.id
        }

        let description = this.props.from == 'SUBSCRIBTIONS' && data.id == 0 ?
            translate("pilih_semua_specialist") : data.description;

        return (
            <TouchableOpacity style={{
                flex: 1,
                backgroundColor: 'white',
                paddingVertical: 10,
                marginHorizontal: 5,
                paddingHorizontal: 10,
                flexDirection: 'row-reverse',
                alignItems: 'center',
                borderBottomWidth: 2,
                borderBottomColor: '#EEF0F5',
            }}
                onPress={() => this.onChecked(data, data.isChecked === true ? false : true)}>
                {/* <View style={{ flex: 1, alignItems: 'flex-end' }}> */}
                <Icon name='ios-checkmark-circle'
                    style={[styles.iconChecklist, { color: data.isChecked === true ? '#3ACCE1' : 'white' }]} />
                {/* </View> */}
                <Text style={styles.text}>{description}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'Nunito-Bold',
        fontSize: 16,
        color: '#6C6C6C',
        flex: 1,
    },
    container: {
        flexDirection: 'row',
        borderColor: '#3ACCE1',
        borderWidth: 2,
        borderRadius: 10,
        marginVertical: 5,
        paddingHorizontal: 5,
        paddingVertical: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconChecklist: {
        fontSize: 30,
        marginHorizontal: 5,
        // color: '#3ACCE1'
    }
});