import React, { Component } from "react";
import { View, TouchableOpacity, FlatList, StyleSheet } from "react-native";
import { Row, Icon, Text } from "native-base";
import { getData, KEY_ASYNC_STORAGE } from "../../src/utils/localStorage";
import { IconCloseHistorySearch } from "../../src/assets";

export default class HistorySearch extends Component {
  state = {
    countryID: "",
  };

  constructor(props) {
    super(props);
    this.getDataCountryCode();
  }

  getDataCountryCode = () => {
    getData(KEY_ASYNC_STORAGE.PROFILE).then((profile) => {
      if (profile.country_code == "ID") {
        this.setState({ countryID: profile.country_code });
      } else {
        this.setState({ countryID: profile.country_code });
      }
    });
  };

  _onPressItem = (textHistory) => {
    this.props.onPressItem(textHistory);
  };

  _onPressRemoveItem = (item) => {
    this.props.onPressRemoveItem(item);
  };

  renderItemHistorySearch = (item) => {
    return (
      <TouchableOpacity
        style={styles.containerItem(this.state.countryID)}
        onPress={() => this._onPressItem(item.item)}
      >
        <Row style={styles.rowItem(this.state.countryID)}>

          {this.state.countryID == "ID"
            ? <Text
              style={styles.itemHistoryID}
            >
              {item.item}
            </Text>
            : <Text
              style={{
                textAlignVertical: "center",
              }}
            >
              {item.item}
            </Text>}
          <TouchableOpacity
            style={{
              alignItems: "center",
            }}
            onPress={() => this._onPressRemoveItem(item)}
          >
            {this.state.countryID == "ID" ? (
              <IconCloseHistorySearch />
            ) : (
              <Icon name="ios-close" />
            )}
          </TouchableOpacity>
        </Row>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View
        style={{
          padding: 16,
        }}
      >

        {this.state.countryID == "ID"
          ? <Text
            style={styles.headerListHistory}
          >Pencarian Terakhir
        </Text>
          : <Text
            style={{
              fontWeight: "bold",
              fontSize: 16,
            }}
          >
            Your Recent History
        </Text>}


        <FlatList
          style={
            this.state.countryID == "ID"
              ? styles.containerFlatListID
              : styles.containerFlatList
          }
          data={this.props.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderItemHistorySearch}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerItem: (countryID) => ({
    flex: 1,
    marginBottom: countryID == "ID" ? 8 : 16,
    borderBottomWidth: countryID == "ID" ? 0 : 1.5,
    borderBottomColor: "#F4F4F6FD",
  }),
  containerFlatList: { padding: 10 },
  containerFlatListID: {},
  rowItem: (countryID) => ({
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center",
  }),
  itemHistoryID: {
    textAlignVertical: "center",
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    lineHeight: 24,
    letterSpacing: 0.15,
    color: "#000000"
  }, headerListHistory: {
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 0.17,
    color: "#000000",
    marginBottom: 13
  }
});
