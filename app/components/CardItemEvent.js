import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Alert, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body, Row, Col, Toast } from 'native-base';
import { convertMonth, share, testID, saveHistorySearch, EnumTypeHistorySearch } from './../libs/Common';
import Api, { errorMessage } from '../libs/Api';
import { NavigationActions } from 'react-navigation';
import * as AddCalendarEvent from 'react-native-add-calendar-event';
import moment from 'moment-timezone';
import { AdjustTrackerConfig, AdjustTracker } from './../libs/AdjustTracker';

let from = {
    OPEN: 'OPEN',
    SHARE: 'SHARE',
    BOOKMARK: 'BOOKMARK',
}

export default class ListItemEvent extends Component {

    shareLink = null;
    isFetching = false;

    constructor(props) {
        super(props);

        this.state = {
            bookmark: this.props.data.flag_bookmark? true : this.props.data.bookmark,
            // reserved: this.props.data.flag_reserve == 0 ? false : true,
            reserved: this.props.data.flag_join? true : this.props.data.join,
            from: this.props.navigation && this.props.navigation.state.routeName,
            activeType: this.props.activeType,
            valueSearchHistory: this.props.valueSearchHistory ? this.props.valueSearchHistory : "",
            isPaid: this.props.data.paid,
        }

        if (this.props.data.url.length > 0) {
            let self = this;
            this.props.data.url.map(function (d, i) {
                if (d.app_name == 'D2D') {
                    self.shareLink = d.app_link;
                }
            })
        }
    }

    sendAdjust = (action) => {
        let token = ''
        switch (action) {
            case from.OPEN:
                if (this.state.from && this.state.from == 'Feeds') {
                    token = AdjustTrackerConfig.Feeds_Events
                }
                else if (this.state.from && this.state.from == 'Event') {
                    if (this.state.activeType == 'all')
                        token = AdjustTrackerConfig.Event_Event_Detail
                    else if (this.state.activeType == 'bookmark')
                        token = AdjustTrackerConfig.Event_Bookmark_Detail
                    else if (this.state.activeType == 'reserve')
                        token = AdjustTrackerConfig.Event_Joined_Detail
                }
                break;
            case from.SHARE:
                if (this.state.from && this.state.from == 'Feeds') {
                    token = AdjustTrackerConfig.Feeds_Share
                }
                else if (this.state.from && this.state.from == 'Event') {
                    if (this.state.activeType == 'all')
                        token = AdjustTrackerConfig.Event_Event_Share
                    else if (this.state.activeType == 'bookmark')
                        token = AdjustTrackerConfig.Event_Bookmark_Share
                    else if (this.state.activeType == 'reserve')
                        token = AdjustTrackerConfig.Event_Joined_Share
                }
                break;
            case from.BOOKMARK:
                if (this.state.from && this.state.from == 'Feeds') {
                    token = AdjustTrackerConfig.Feeds_Bookmarks
                }
                else if (this.state.from && this.state.from == 'Event') {
                    if (this.state.activeType == 'all')
                        token = AdjustTrackerConfig.Event_Event_Bookmark
                    else if (this.state.activeType == 'bookmark')
                        token = AdjustTrackerConfig.Event_Bookmark_Bookmark
                    else if (this.state.activeType == 'reserve')
                        token = AdjustTrackerConfig.Event_Joined_Bookmark
                }
                break;
            default: break;
        }

        if (token != '') {
            AdjustTracker(token);
        }
    }

    _renderPaid() {
        if (this.state.isPaid == "Y") {
            return (
                <View
                    {...testID('buttonBerbayar')}
                    style={styles.listItemButtonBerbayar}>
                    <Text style={styles.listitemTextBerbayar}>Berbayar</Text>
                </View>
            )
        }
        else {
            return <View />
        }

    }

    _renderBookmark() {
        let isBookmarked = this.state.bookmark == true ? 'bookmark' : 'bookmark-border';
        let colorIcon = this.state.bookmark == true ? '#CB1D50' : '#78849E';

        return (
            <Button
                {...testID('buttonBookmark')}
                full small transparent
                onPress={() => this._toggleBookmark() + this.sendAdjust(from.BOOKMARK)}
                style={styles.listItemButtonBookmarked}>
                <Text nopadding style={styles.listitemText}>Bookmark</Text>
                <Icon type="MaterialIcons" {...testID('iconBookmark')} nopadding name={isBookmarked} style={[styles.listItemIconBookmarked, { color: colorIcon }]} />
            </Button>
        )
    }

    _renderSelected() {
        // if (this.props.data.flag_reserve > 0) {
        if (this.state.reserved == true) {
            return (
                <View style={{ position: 'absolute', right: -10, top: -10 }}>
                    <View style={styles.selected} />
                    <Icon {...testID('iconCheckmark')} name='ios-checkmark' style={styles.selectedIcon} />
                </View>
            )
        } else {
            return null
        }
    }

    _toggleBookmark = async () => {
        // hit api

        if (this.isFetching == true) {
            return;
        }

        this.isFetching = true;

        let getUid = await AsyncStorage.getItem('UID');
        let params = {
            act: this.state.bookmark == true ? 'unbookmark' : 'bookmark',
            content: 'event',
            contentId: this.props.data.id,
            uid: getUid,
        }
        let response = await Api.post('activity/', params);

        this.isFetching = false;

        if (errorMessage(response) == '') {
            this.setState({ bookmark: !this.state.bookmark });

            if (this.props.activeType != null) {
                this.props.action(this.props.activeType);
            }
            Toast.show({ text: params.act + ' succesfull', position: 'top', duration: 3000 })

            if (this.state.bookmark == true) {
                this.showAlertAddToCalendar()
            }

        } else {
            Toast.show({ text: errorMessage(response), position: 'top', duration: 3000 })
        }
    }

    showAlertAddToCalendar = () => {
        Alert.alert(
            'Information',
            'You can add this event to your calendar. Do you want to add this event to your calendar now?',
            [
                { text: 'Later', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'OK', onPress: () => this.addCalendar(),
                },
            ],
            { cancelable: true }
        );
    }

    async addCalendar() {
        let data = this.props.data;
        // var start = moment.tz(data.start_date, "Asia/Jakarta");
        // var end = moment.tz(data.end_date, "Asia/Jakarta");
        var start = moment.utc(data.start_date).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
        var end = moment.utc(data.end_date).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');

        const eventConfig = {
            title: data.title,
            // startDate: data.start_date + "T05:00:00.000Z",
            // endDate: data.end_date + "T05:00:00.000Z",
            startDate: start,
            endDate: end
        };

        AddCalendarEvent.presentEventCreatingDialog(eventConfig)
            .then((eventInfo) => {
                // handle success - receives an object with `calendarItemIdentifier` and `eventIdentifier` keys, both of type string.
                // These are two different identifiers on iOS.
                // On Android, where they are both equal and represent the event id, also strings.
                // when false is returned, the dialog was dismissed
                if (eventInfo) {
                    console.warn(JSON.stringify(eventInfo));
                } else {
                    console.warn('dismissed');
                }
            })
            .catch((error) => {
                // handle error such as when user rejected permissions
                console.warn(error);
            });
    }

    navigateToEventDetail = async (data) => {
        if (this.state.valueSearchHistory != "") {
            saveHistorySearch(EnumTypeHistorySearch.EVENT, this.state.valueSearchHistory)
        }
        // this.props.navigation.navigate(eventDetail);
        data.flag_bookmark = this.state.bookmark;
        data.flag_reserve = this.state.reserved;
        // this.props.navigation.navigate('EventDetail', {
        //     updateDataBookmark : this.doUpdateDataBookmark,
        //     data : data,
        //reserve
        //     manualUpdateReserve : this.props.manualUpdateReserve == null ? false : this.props.manualUpdateReserve,
        //     updateDataReserve : this.doUpdateDataReserved,
        // })
        // this.props.navigation.navigate('EventDetail', {
        //     updateDataBookmark : this.doUpdateDataBookmark,
        //     data : data
        // })

        this.props.navigation.dispatch(NavigationActions.navigate({
            routeName: 'EventDetail',
            params: {
                updateDataBookmark: this.doUpdateDataBookmark,
                data: data,
                joined: this.state.reserved,
                //reserve
                manualUpdateReserve: this.props.manualUpdateReserve == null ? false : this.props.manualUpdateReserve,
                updateDataReserve: this.doUpdateDataReserved,
                from: this.state.activeType === 'all' ? this.state.from : this.state.activeType,
            }, key: `event-${data.id}`
        }));
    }

    doUpdateDataBookmark = (bookmark) => {
        this.setState({ bookmark: bookmark });
    }

    doUpdateDataReserved = () => {
        this.setState({ reserved: true });
    }

    render() {
        const listDate = this.props.data.start_date != null ? this.props.data.start_date.substring(8, 10) : '';
        const listMonth = this.props.data.start_date != null ? convertMonth(this.props.data.start_date.substring(5, 7)) : '';

        return (
            <TouchableOpacity
                {...testID('button_card_event')}
                style={styles.listItemWrapper} activeOpacity={0.7} onPress={() => this.navigateToEventDetail(this.props.data) + this.sendAdjust(from.OPEN)}>
                <Card style={{ flex: 0, padding: 10 }}>
                    <CardItem style={{ paddingLeft: 0, paddingRight: 0, paddingBottom: 16, paddingTop: 0 }}>
                        {this._renderSelected()}
                        <Col
                            {...testID('dateEvent')}
                            size={3} style={styles.listItemThumb}>
                            <Text style={styles.listItemDate}>{listDate}</Text>
                            <Text style={styles.listItemMonth}>{listMonth}</Text>
                        </Col>
                        <Col size={7} style={styles.listItemContent}>
                            <Text style={styles.listItemTitle}>{this.props.data.title}</Text>
                        </Col>
                    </CardItem>
                    <View style={{ backgroundColor: "#E4E6E9", height: 1, flex: 1 }}></View>
                    <CardItem nopadding>
                        <Row style={{
                            marginTop: 16,
                            justifyContent: "space-between"
                        }}>
                            {this._renderPaid()}
                            <Right>
                                <Row style={{ alignItems: 'center' }}>
                                    <Row style={{ alignItems: 'flex-end', flex: 0, marginRight: 12.5 }} >
                                        <Button
                                            {...testID('buttonShare')}
                                            full small transparent style={styles.listItemButtonBookmarked}
                                            onPress={() => share(this.shareLink) + this.sendAdjust(from.SHARE)}>
                                            <Text nopadding style={styles.listitemText}>Share</Text>
                                            <Icon nopadding type='EvilIcons' name='share-google' style={styles.listItemIconBookmarked} />
                                        </Button>
                                    </Row>
                                    <Row style={{ alignItems: 'flex-end', flex: 0 }} >
                                        {this._renderBookmark()}
                                    </Row>
                                </Row>
                            </Right>
                        </Row>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    listItemDate: {
        fontFamily: 'Nunito-Bold',
        color: '#ffffff',
        fontSize: 31,
        textAlign: 'center',
    },
    listItemMonth: {
        fontFamily: 'Nunito-Bold',
        color: '#ffffff',
        fontSize: 21,
        textAlign: 'center',
    },
    listItemWrapper: {

    },
    listItemThumb: {
        backgroundColor: '#CB1D50',
        paddingVertical: 10,
        borderRadius: 12,
    },
    listItemContent: {
        paddingHorizontal: 16,
        minHeight: 60,
    },
    listItemTitle: {
        fontSize: 16,
        color: '#454F63',
        fontFamily: "Nunito-Bold",
    },
    listItemIconBookmarked: {
        color: '#78849E',
        fontSize: 18,
    },
    listItemButtonBookmarked: {

    },
    listitemText: {
        fontSize: 12,
        color: '#78849E',
        paddingRight: 0,
        paddingLeft: 0
    },
    selected: {
        width: 0,
        height: 0,
        borderTopWidth: 40,
        borderLeftWidth: 40,
        borderRightWidth: 0,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderTopColor: '#3AE194',
    },
    selectedIcon: {
        color: '#fff',
        position: 'absolute',
        top: -3,
        right: -17
    }, listItemButtonBerbayar: {
        backgroundColor: '#F6F6F7',
        borderRadius: 5,
        justifyContent: 'center',
        paddingVertical: 6,
        paddingHorizontal: 12
    },
    listitemTextBerbayar: {
        fontSize: 14,
        color: '#696969',
        fontFamily: 'Nunito-Regular',
        textAlign: 'center'
    },
    mediumImageSize: {
        height: 24,
        width: 24,
        marginRight: 8
    },

})