import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import { AdjustTrackerConfig, AdjustTracker } from '../libs/AdjustTracker';

class Tag extends Component {
    render() {
        let dataObat = this.props

        return (
            <TouchableOpacity onPress={() => {
                dataObat.idObat != undefined ? this.props.navigation.push('DetailAtoZ', { dataObat }) +
                    AdjustTracker(AdjustTrackerConfig.Obat_related)
                    : null
            }}
                activeOpacity={0.7}>
                <View style={styles.backgroundCompetence}>
                    <Text style={styles.textGrey}>{this.props.value}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    backgroundCompetence: {
        padding: 5,
        paddingHorizontal: 10,
        marginTop: 5,
        marginRight: 10,
        backgroundColor: '#F6F6F7',
        borderRadius: 5,
    },
    textGrey: {
        fontSize: 14,
        color: '#696969'
    },
})

// withNavigation returns a component that wraps MyBackButton and passes in the
// navigation prop
export default withNavigation(Tag);