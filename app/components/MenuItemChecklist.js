import React from 'react';
import { StyleSheet, Text, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Icon, View } from 'native-base';

import PropTypes from 'prop-types';

const MenuItem = props => (
  <TouchableOpacity
    disabled={props.disabled}
    onPress={props.onPress}
    style={[styles.container, props.style]}
    underlayColor={props.underlayColor}
  >
    <Text
      numberOfLines={1}
      style={[
        styles.title,
        props.disabled && { color: props.disabledTextColor },
        props.textStyle,
      ]}
    >
      {props.children}
    </Text>
    <View style={{flex: 0.35 }}> 
      <Icon 
        type = {props.isChecked === true ? 'Ionicons' : 'MaterialIcons'}
        name={props.isChecked === true ? 'md-checkbox-outline' : 'check-box-outline-blank'}
        style={[styles.iconChecklist, {color: props.isChecked === true ? '#3ACCE1' : '#9E9FA1',}]} />
    </View>
   
  </TouchableOpacity>
);

MenuItem.Left = props => (
  <TouchableOpacity
    disabled={props.disabled}
    onPress={props.onPress}
    style={[styles.container, props.style]}
    underlayColor={props.underlayColor}
  >
    <View style={{flex: 0.35 }}> 
      <Icon 
        type = {props.isChecked === true ? 'Ionicons' : 'MaterialIcons'}
        name={props.isChecked === true ? 'md-checkbox-outline' : 'check-box-outline-blank'}
        style={[styles.iconChecklist, {color: props.isChecked === true ? '#3ACCE1' : '#9E9FA1',}]} />
    </View>
    <Text
      numberOfLines={1}
      style={[
        styles.title,
        props.disabled && { color: props.disabledTextColor },
        props.textStyle,
      ]}
    >
      {props.children}
    </Text>
   
  </TouchableOpacity>
);

MenuItem.propTypes = {
  children: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  disabledTextColor: PropTypes.string,
  onPress: PropTypes.func,
  style: TouchableHighlight.propTypes.style,
  textStyle: Text.propTypes.style,
  underlayColor: TouchableHighlight.propTypes.underlayColor,
};

MenuItem.defaultProps = {
  disabled: false,
  disabledTextColor: 'rgb(189,189,189)',
  underlayColor: 'rgb(224,224,224)',
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    justifyContent: 'flex-start',
    maxWidth: 248,
    minWidth: 124,
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    flex : 0.65,
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'left',
    paddingHorizontal: 16,
  },
  iconChecklist:{
      fontSize:25, 
      textAlign: 'left', 
      marginHorizontal: 5,
  }
});

export default MenuItem;
