import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Grid } from 'react-native';
import { Body, Right, Icon } from 'native-base';
import platform from '../../theme/variables/d2dColor';

export default class Radio extends Component {

    constructor(props) {
        super(props);           
    }
    
    render() {
        return (
            <TouchableOpacity style={styles.container} activeOpacity={0.7} {...this.props}>
                <Body style={{flex: 0.9}}>
                    <Text style={styles.text}>{this.props.title}</Text>
                </Body>
                <Right style={{flex: 0.1}}>
                    <Icon 
                        name={this.props.selected === true ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                        style={{ color: this.props.selected === true ? platform.brandInfo : '#9E9FA1' }} />
                </Right>
            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    text: {
        fontFamily: 'Nunito-Regular',
        fontSize: 14,
        color: '#6C6C6C',
    },
});