import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Right, Body, Row, Col } from 'native-base';

export default class CardButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            like: this.props.like,
            bookmark: this.props.bookmark,
        }
        this.likes = this.props.likes;        
        this.bookmarks = this.props.bookmarks;        
    }

    _toggleLike = () => {
        // hit api
        this.setState({ like: !this.state.like });
        this.likes = !this.state.like ? this.likes + 1 : this.likes - 1;
    }
    
    _toggleBookmark = () => {
        // hit api
        this.setState({ bookmark: !this.state.bookmark });
        this.bookmarks = !this.state.bookmark ? this.bookmarks + 1 : this.bookmarks - 1;
    }

    render() {
        return (
            <CardItem nopadding>
                <Col style={styles.toolItemFirst}>
                    <Button transparent full small
                        danger={this.state.like ? true : false} 
                        light={this.state.like ? false : true} 
                        onPress={this._toggleLike}>
                        <Icon name="ios-heart" nopadding style={styles.toolIcon} />
                        <Text nopadding style={styles.toolText}>{this.likes} Like</Text>
                    </Button>
                </Col>
                <Col style={styles.toolItemCenter}>
                    <Button transparent light full small>
                        <Icon name="ios-chatboxes" nopadding style={styles.toolIcon} />
                        <Text nopadding style={styles.toolText}>{this.props.comments} Comments</Text>
                    </Button>
                </Col>
                <Col style={styles.toolItemLast}>
                    <Button transparent full small
                        info={this.state.bookmark ? true : false} 
                        light={this.state.bookmark ? false : true} 
                        onPress={this._toggleBookmark}>
                        <Icon name="ios-bookmark" nopadding style={styles.toolIcon} />
                        <Text nopadding style={styles.toolText}>{this.bookmarks} Bookmark</Text>
                    </Button>
                </Col>
            </CardItem>
        )
    }
}

const styles = StyleSheet.create({
    toolItemFirst: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
    },
    toolItemCenter: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
    },
    toolItemLast: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
    },
    toolIcon: {
        fontSize: 18,
    },
    toolText: {
        fontSize: 12,
    }
})