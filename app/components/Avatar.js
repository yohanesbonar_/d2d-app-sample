import React from 'react';
import { View, Image, Text, ImageBackground } from 'react-native';
import { getInitialName } from '../libs/GetInitialName'
import PropTypes from 'prop-types';
import ViewPropTypes from '../libs/ViewPropTypes';

const Avatar = ({ avatarSize, containerStyle, imageSource, avatarStyle, textStyle, initialStyle, resizeMode, userName, customSize }) => {
    const isImageExist = !!imageSource && imageSource !== ''
    let size
    let paddingVertical = 16
    let fontSize = 32
    switch (avatarSize) {
        case 'ExtraSmall':
            size = 25
            fontSize = 10
            paddingVertical = 6
            break;
        case 'Small':
            size = 50
            fontSize = 20
            paddingVertical = 11
            break;
        case 'Medium':
            size = 75
            break;
        default:
            size = customSize
            break;
    }

    return (
        <View
            style={{
                width: size,
                height: size,
                marginHorizontal: 5,
                ...containerStyle
            }}
        >
            {
                isImageExist &&
                <Image
                    source={{ uri: imageSource || '' }}
                    style={[{ width: size, height: size, borderRadius: size / 2 }, avatarStyle]}
                    resizeMode={resizeMode}
                    accessibilityLabel={"imgProfile"}
                    onLoadStart={(event) => console.log('onLoadStart: ', event)}
                    onLoad={(event) => console.log('onLoad: ', event)}
                    onLoadEnd={(event) => console.log('onLoadEnd: ', event)}
                    onError={(event) => console.log('onError: ', event)}
                />
            }
            {
                !isImageExist &&
                <View
                    style={[styles.initialStyle, { paddingVertical }, initialStyle, avatarStyle]}
                >
                    <Text style={[styles.textStyle, { fontSize }, textStyle]} accessibilityLabel={"txtInitial"}>{getInitialName(userName) || ''}</Text>
                </View>
            }

        </View>
    )
}

Avatar.propTypes = {
    imageSource: PropTypes.string,
    avatarSize: PropTypes.string,
    avatarStyle: ViewPropTypes.style
};

const styles = {
    avatarStyle: {
        width: '100%',
        height: '100%',
        borderRadius: 50
    },
    initialStyle: {
        backgroundColor: '#E2E2E2',
        paddingVertical: 18,
        width: '100%',
        height: '100%',
        borderRadius: 100
    },
    textStyle: {
        fontFamily: 'Roboto-Regular',
        color: '#424242',
        backgroundColor: 'transparent',
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        alignSelf: 'center'
    }
};

export { Avatar };