import variable from "./../variables/platform";

export default (variables = variable) => {
  const textCommon = {
    color: "#1E1E20",
    fontSize: 16,
    fontFamily: "Roboto-Regular",
  };

  const textSmall = {
    "NativeBase.Text": {
      fontSize: 14,
    },
  };

  const textLarge = {
    fontSize: 20,
  };

  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,

    ".note": {
      color: "#a7a7a7",
      fontSize: variables.noteFontSize,
    },

    ".textLogo": {
      ...textCommon,
      fontSize: 24,
      letterSpacing: 0,
      fontWeight: "700",
      lineHeight: 38,
    },

    ".textHeader": {
      ...textCommon,
      fontSize: 20,
      letterSpacing: 0.26,
      fontWeight: "700",
      lineHeight: 32,
    },

    ".textTitle": {
      ...textCommon,
      letterSpacing: 0.15,
      fontWeight: "700",
      lineHeight: 26,
    },

    ".text2": {
      ...textCommon,
      fontSize: 14,
      letterSpacing: 0.09,
      fontWeight: "500",
      lineHeight: 24,
    },

    ".text3": {
      ...textCommon,
      fontSize: 14,
      letterSpacing: 0.09,
      fontWeight: "400",
      lineHeight: 24,
    },

    ".textLabelInput": {
      ...textCommon,
      letterSpacing: 0.49,
      fontWeight: "500",
      lineHeight: 26,
    },

    ".textError": {
      ...textCommon,
      fontSize: 12,
      letterSpacing: 0,
      color: variable.textErrorColor,
      fontWeight: "400",
      lineHeight: 14,
    },
    ".textButton": {
      ...textCommon,
      color: "#fff",
      fontFamily: "Roboto-Medium",
      textAlign: "center",
    },

    ".textButtonDisable": {
      ...textCommon,
      fontWeight: "bold",
      color: "0000003D",
    },

    ".textAsButton": {
      ...textCommon,
      color: "#0770CD",
      fontFamily: "Roboto-Medium",
      letterSpacing: 0.49,
    },

    ".textButtonCancel": {
      ...textCommon,
      fontFamily: "Roboto-Medium",
      textAlign: "center",
    },

    ".textBottomNavigation": {
      ...textCommon,
      fontSize: 10,
      fontFamily: "Roboto-Medium",
    },

    ".bold": {
      ...textCommon,
      fontFamily: "Roboto-Bold",
      fontWeight: "bold",
    },

    ".semiBold": {
      ...textCommon,
      fontWeight: "600",
    },

    ".regular": {
      ...textCommon,
      lineHeight: 26,
      fontWeight: "400",
    },

    ".textLarge": {
      fontSize: 20,
    },
    ".titleInCard": {
      ...textCommon,
      ...textLarge,
      fontFamily: "Roboto-Bold",
      fontWeight: "bold",
      letterSpacing: 0.26,
    },

    ".H6": {
      fontSize: 20,
      letterSpacing: 0.15,
      lineHeight: 24,
      fontFamily: "Roboto-Medium",
      color: "#000000",
    },
    ".H7": {
      fontSize: 18,
      fontFamily: "Roboto-Medium",
      color: "#000000",
      lineHeight: 24,
      letterSpacing: 0.17,
    },
    ".Body1": {
      fontSize: 16,
      letterSpacing: 0.5,
      lineHeight: 24,
      fontFamily: "Roboto-Regular",
      color: "#000000",
    },
    ".Body2": {
      fontSize: 14,
      letterSpacing: 0.25,
      lineHeight: 20,
      fontFamily: "Roboto-Regular",
      color: "#000000",
    },

    ".SubtitleMedium": {
      fontSize: 16,
      letterSpacing: 0.15,
      lineHeight: 24,
      fontFamily: "Roboto-Medium",
      color: "#000000",
    },
    ".Subtitle2Medium": {
      fontSize: 14,
      letterSpacing: 0.25,
      lineHeight: 20,
      fontFamily: "Roboto-Medium",
      color: "#000000",
    },

    ".textButtonMediumBlue": {
      fontSize: 14,
      letterSpacing: 0.9,
      lineHeight: 16,
      fontFamily: "Roboto-Medium",
      color: "#0771CD",
    },

    ".textButtonRegulerWhite": {
      fontFamily: "Roboto-Regular",
      fontSize: 16,
      lineHeight: 24,
      color: "#FFFFFF",
      letterSpacing: 0.15,
    },

    ".textButtonRegulerBlack": {
      fontFamily: "Roboto-Regular",
      fontSize: 16,
      lineHeight: 24,
      color: "#000000",
      letterSpacing: 0.15,
    },
  };

  return textTheme;
};
