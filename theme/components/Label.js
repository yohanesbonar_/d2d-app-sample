import variable from "./../variables/platform";

export default (variables = variable) => {
  const commonLabel = {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: '#1E1E20',
    fontWeight: "500",
    letterSpacing: 0.49,
    lineHeight: 26
  }

  const labelTheme = {
    fontSize: 16,
    fontFamily: 'Nunito-SemiBold',
    color: '#78849E',

    ".focused": {
      width: 0
    },

    ".black": {
      ...commonLabel
    },

  };

  return labelTheme;
};
