import { Platform } from "react-native";

import variable from "./../variables/platform";

export default (variables = variable) => {
  const titleTheme = {
    fontSize: variables.titleFontSize,
    fontFamily: variables.titleFontfamily,
    color: variables.titleFontColor,
    fontWeight: Platform.OS === "ios" ? "600" : undefined,
    textAlign: "center",

    '.leftTitle': {
      flex: 1,
      alignSelf: "center",
      textAlign: 'left',
      fontFamily: 'Roboto-Bold',
      color: '#1E1E20',
      fontSize: 20,
      textAlignVertical: 'center',
      marginTop: 2
    },

    '.newDesign': {
      fontFamily: 'Roboto-Bold',
      color: '#1E1E20',
      fontSize: 20,
      fontWeight: "700",
      letterSpacing: 0.26,
      lineHeight: 32
    }

  };

  return titleTheme;
};
