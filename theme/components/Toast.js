import variable from "./../variables/platform";

export default (variables = variable) => {
  const platform = variables.platform;

  const toastTheme = {
    ".danger": {
      backgroundColor: variables.brandDanger
    },
    ".warning": {
      backgroundColor: '#1E1E20',
      borderRadius: 100,
      "NativeBase.Text": {
        color: "#fff",
        flex: 1,
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center'
      },
      marginBottom: 48
    },
    ".success": {
      backgroundColor: variables.brandSuccess
    },
    backgroundColor: "#1E1E20",
    borderRadius: platform === "ios" ? 8 : 0,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
    padding: 16,

    "NativeBase.Text": {
      color: "#FFFFFF",
      flex: 1,
      fontFamily: "Roboto-Regular",
      fontSize: 16,
      lineHeight: 26
    },
    "NativeBase.Button": {
      backgroundColor: "transparent",
      marginVertical: -16,
      alignSelf: 'center',
      elevation: 0,
      padding: 0,
      "NativeBase.Text": {
        fontSize: 16,
        fontFamily: "Roboto-Medium",
        color: "#FFFFFF",
        margin: -16,
        letterSpacing: 0.49,
        lineHeight: 26
      }

    },


  };

  return toastTheme;
};
