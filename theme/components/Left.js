import variable from './../variables/platform';

export default (variables = variable) => {
	const leftTheme = {
		flex: 1,
		alignSelf: 'center',
		alignItems: 'flex-start',

		".newDesign": {
			flex: 0,
			marginHorizontal: 4
		}
	};

	return leftTheme;
};
