import variable from './../variables/platform';

export default (variables = variable) => {
	const inputTheme = {
		'.multiline': {
			height: null,
		},
		height: variables.inputHeightBase,
		color: variables.inputColor,
		paddingLeft: 0,
		paddingRight: 0,
		flex: 1,
		fontSize: variables.inputFontSize,
		//	lineHeight: variables.inputLineHeight,
		fontFamily: 'Roboto-Regular',

		'.newDesign': {
			height: 56,
			color: "#666666",
			padding: 0,
			flex: 1,
			fontSize: 16,
			top: 0,
			textAlignVertical: 'center',
			fontFamily: 'Roboto-Regular',
			fontWeight: "400",
			lineHeight: 26
		}
	};

	return inputTheme;
};
