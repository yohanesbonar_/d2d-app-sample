module.exports = api => {
  console.log("api: ", api)
  const babelEnv = api.env();
  console.log(babelEnv)

  const plugins = [];
  //change to 'production' to check if this is working in 'development' mode
  if (babelEnv !== 'development') {
    plugins.push(['transform-remove-console']);
  }
  return {
    presets: ['module:metro-react-native-babel-preset'],
    plugins,
  };
};
